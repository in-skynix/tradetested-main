# Trade Tested

## Setup with Docker
Make sure you have Docker and Docker Compose installed. 
Its also good to have dnsmasq installed.

Use the new OSX Native Docker if using OSX

Clone the repository and cd to the repository's base directory.

Log in to the gitlab docker registry

```
docker login registry.gitlab.com
```

Create a .env file in the base directory of the project,
with the following content:

```
THUMBOR_AWS_ACCESS_KEY_ID=
THUMBOR_AWS_SECRET_ACCESS_KEY=
THUMBOR_TC_AWS_LOADER_BUCKET=magento.media.staging.tradetested
RABBITMQ_USER=user
RABBITMQ_PASS=pass
MYSQL_ROOT_PASSWORD=dev_root_password
MYSQL_PASSWORD=dev_mysql_password
MAGENTO_CRYPT_KEY=
PROJECT_MOBY_ROOT=.
```

Fill in the missing details, and edit any you like.

The AWS credentials are credentials with access to the bucket 
containing the Magento media.

The MySQL and RabbitMQ credentials will be used to set up users 
in the relevant Docker containers

Create your own versions of the certificates and keys, by running:

```
./dev_setup.sh
```

Start MySQL in the background, and import the DB Dump
```
docker-compose up -d mysql
mysql -h 127.0.0.1 -P3307 -u tradetested -p -D tradetested_magento < dump.sql
```

Setup your hosts file or dnsmasq for *.dev, e.g.
```
echo 'address=/.dev/127.0.0.1' >> /usr/local/etc/dnsmasq.conf
# (and restart dnsmasq)
```

FOR iOS Only


sudo mkdir -p /etc/resolver
sudo tee /etc/resolver/dev >/dev/null <<EOF
nameserver 127.0.0.1
EOF

For info on setting up dnsmasq: https://passingcuriosity.com/2013/dnsmasq-dev-osx/

Start up all of the services
```
docker-compose up
```

**Optional**

These steps will ensure you can access any URL without SSL warnings.

* Install ./private/ssl_certs/developer.crt as a client cert in your OS/Browser
* Install ./private/ssl_certs/tradetested.ca.crt as a trusted root in your OS/Browser

## URLs

* https://mage.tradetested.dev/ - Magento Admin
* https://www.tradetested.dev/ - Magento Frontend
* https://assets.tradetested.dev/ - Gulp Asset Server
* https://thumbs.tradetested.dev/ - Thumbor Thumbnailing Server

## Assets
* All javascript/CSS/images are to be placed in the /assets directory.
* The assets will be managed by Gulp.
* There are separate stylesheets for mobile vs desktop. Look at the includes from each file. 
  To add a css file for both mobile and desktop, create it under /resources/stylesheets/all, 
  then add it to the includes under /resources/stylesheets/desktop/__index.css.sass.
  Files that begin with an underscore are included only and not compiled to their own files.

## Deployment
Via capistrano:

    * `cap staging deploy`
    * `cap production deploy`
    * `cap vagrant deploy`

## Production Database to Dev
Dump the production database, excluding some tables.
```
n98-magerun db:dump --strip="m2epro_config @development @stripped @sales @search"
```
    
Import a gzipped database dump to staging, and set config correctly
```
./tools/db_copy.sh /path/to/sql.gz
```

## Re-sync assets

```
docker-compose exec gulp /gulp/node_modules/.bin/gulp default
```

## OSX Performance workaround

It appears that there are still performance issues when sharing files from an OSX host.

d4m-nfs mounts **your** home directory as /mnt/ within the docker host, using NFS.

To use NFS for volume shares,
change the .env file line so that PROJECT_MOBY_ROOT points to the project root using /mnt/.

e.g. if my project is checked out to `~/Code/magento` on my Mac

```
PROJECT_MOBY_ROOT=/mnt/Code/magento
```

Then start with
```
./d4m-nfs.sh && docker-compose up
```

d4m-nfs is not currently integrated with inotify, so the gulp source still uses a traditional mount.
