#!/usr/bin/env bash
set -o allexport
source ./.env.production
set +o allexport
set -e

host_id="magento"
domain="staging.tradetested.co.nz"
cacert="/Users/Dane/Certificates/tradetested.ca.crt"
cakey="/Users/Dane/Certificates/ca.key"
nz_domain="staging.tradetested.co.nz"
au_domain="staging.tradetested.com.au"
#admin_pw=`openssl rand -base64 32`
#pg_pw="${PROD_DB_PW}"

echo "Creating docker-machine"
#docker-machine create --driver amazonec2 --amazonec2-vpc-id vpc-6faca30a --amazonec2-instance-type m3.large "${host_id}"
echo "Switching docker-machine"
eval $(docker-machine env "${host_id}")

echo "Generating certificates"
mkdir -p /tmp/dockerdata/ssl_certs
for domain in www.${nz_domain} mage.${nz_domain} assets.${nz_domain} media.${nz_domain} thumbs.${nz_domain} api.${nz_domain} mq.${nz_domain} ws.${nz_domain} www.${au_domain}
do
    openssl req -nodes -newkey rsa:2048 -keyout /tmp/dockerdata/ssl_certs/"${domain}".key \
        -out /tmp/dockerdata/ssl_certs/"${domain}".csr \
        -subj "/C=NZ/ST=Auckland/L=Auckland/O=Trade Tested/OU=Temp/CN=${domain}"
    openssl x509 -req -days 3650 -in /tmp/dockerdata/ssl_certs/"${domain}".csr \
        -passin pass:${CAPASS} \
        -CA "${cacert}" -CAkey "${cakey}" -set_serial 01 -out /tmp/dockerdata/ssl_certs/"${domain}".crt
done
cp ${cacert} /tmp/dockerdata/ssl_certs/tradetested.ca.crt
echo "Creating archive of /data files and removing"
tar -C /tmp/dockerdata/ -pzcf /tmp/dockerdata.tar.gz .
rm -rf /tmp/dockerdata

echo "Recreating /data (volumes) on host"
docker-machine ssh "${host_id}" sudo rm -rf /data/*
echo "Copying /data to host and extracting"
docker-machine scp /tmp/dockerdata.tar.gz "${host_id}":~
docker-machine ssh "${host_id}" "sudo mkdir -p /data"
docker-machine ssh "${host_id}" "sudo tar -xvf ~/dockerdata.tar.gz -C /data"
rm /tmp/dockerdata.tar.gz

echo "Generating (quick) dhparam on /data on host"
docker-machine ssh "${host_id}" sudo openssl dhparam -dsaparam -out /data/ssl_certs/dhparam.pem 4096

echo "stopping all containers"
docker-compose --file docker-compose.production.yml stop
echo "Firing up everything"
docker-compose --file docker-compose.production.yml up -d
