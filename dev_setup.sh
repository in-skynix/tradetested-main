#!/usr/bin/env bash
certs_dir='./private/ssl_certs/'
ca_location=${CA_LOCATION:-'./private/ssl_certs/tradetested.ca.crt'}
ca_key_location=${CA_KEY_LOCATION:-'./private/ssl_certs/tradetested.ca.key'}
nz_domain=${NZ_DOMAIN:-'tradetested.dev'}
au_domain=${AU_DOMAIN:-'tradetested.au.dev'}
passphrase=${PASSPHRASE:='V9G2onygZciiib'}
openssl_prefix=''

mkdir -p ${certs_dir}
${openssl_prefix}openssl genrsa -des3 -passout pass:$PASSPHRASE -out ${certs_dir}tradetested.co.nz.jwt.rsa 2048
${openssl_prefix}openssl rsa -in ${certs_dir}tradetested.co.nz.jwt.rsa -pubout -out ${certs_dir}tradetested.co.nz.jwt.public.rsa -passin pass:$PASSPHRASE

if [ ! -f ${ca_location} ]; then
	${openssl_prefix}openssl genrsa -des3 -passout pass:$PASSPHRASE -out ${ca_key_location} 4096
	chmod 400 ${ca_key_location}
	${openssl_prefix}openssl req -key ${ca_key_location} -passin pass:$PASSPHRASE -new -x509 -days 7300 -sha256 -extensions v3_ca -out ${ca_location} \
	  -subj "/C=NZ/ST=Auckland/L=Auckland/O=Trade Tested Ltd/CN=ca.tradetested.co.nz"
	${openssl_prefix}openssl req -nodes -new -keyout ${certs_dir}test.key -out ${certs_dir}test.csr \
		-subj "/C=NZ/ST=Auckland/L=Auckland/O=Trade Tested Ltd/CN=test"
	${openssl_prefix}openssl x509 -passin pass:$PASSPHRASE -req -days 365 -sha256 -in ${certs_dir}test.csr -CA ${ca_location} \
	  -CAkey ${ca_key_location} -CAserial ${certs_dir}tradetested.ca.srl -CAcreateserial -out ${certs_dir}test.crt
fi
for domain in www.${nz_domain} mage.${nz_domain} assets.${nz_domain} media.${nz_domain} thumbs.${nz_domain} api.${nz_domain} mq.${nz_domain} ws.${nz_domain} www.${au_domain} developer
do
	${openssl_prefix}openssl req -nodes -new -keyout ${certs_dir}${domain}.key -out ${certs_dir}${domain}.csr \
		-subj "/C=NZ/ST=Auckland/L=Auckland/O=Trade Tested Ltd/CN=${domain}"
	${openssl_prefix}openssl x509 -passin pass:$PASSPHRASE -req -days 365 -sha256 -in ${certs_dir}${domain}.csr -CA ${ca_location} \
	  -CAkey ${ca_key_location} -CAserial ${certs_dir}tradetested.ca.srl -out ${certs_dir}${domain}.crt
done

echo "
	####
	SSL Certificates have been generated for docker images
	###

	If you add the thingee to the thingee, you'll be able to browse it without warnings.
"
