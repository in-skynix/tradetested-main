<?php
/**
 * @author Dane Lowe
 */
if (extension_loaded('newrelic')) {
    newrelic_ignore_apdex();
    newrelic_ignore_transaction();
    newrelic_disable_autorum();
}
umask(0);
define('MAGENTO_ROOT', getcwd());
header('Content-Type: application/json');

if (isset($_GET['skip_mage']) && $_GET['skip_mage']) {
    require_once MAGENTO_ROOT . '/app/code/core/Mage/Core/Helper/Abstract.php';
    require_once MAGENTO_ROOT . '/app/code/local/TradeTested/AjaxBlocks/Helper/Data.php';
    $helper = new TradeTested_AjaxBlocks_Helper_Data();
    echo $helper->processSkipMagento();
} else {
    require_once MAGENTO_ROOT . '/app/Mage.php';
    if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) {
        Mage::setIsDeveloperMode(true);
    }

    $storeCode = isset($_GET['store_code']) ? $_GET['store_code'] : 1;
    Mage::app($storeCode, 'store')
        ->loadAreaPart(Mage_Core_Model_App_Area::AREA_FRONTEND, Mage_Core_Model_App_Area::PART_EVENTS);

//Load all session data. Loading just one namespace doesn't work
    Mage::getSingleton('core/session', array('name' => 'frontend'));

    $product = Mage::getModel('catalog/product')->load(Mage::app()->getRequest()->getParam('product_id'));
    Mage::register('product', $product);

    echo Mage::helper('tradetested_ajax_blocks')->getJson(Mage::app()->getRequest()->getParams());
}

