<?php
require_once 'abstract.php';

class TestShell extends Mage_Shell_Abstract
{
    protected $_helper;
    protected $_scheduler;
    protected $_queue;

    public function run()
    {
        $fh = fopen("product_guids.csv", "r");
        while (!feof($fh) ) {
            $line = fgetcsv($fh, 1024);
            $product = Mage::getModel('catalog/product')->load($line[0]);
            if ($product->getId() && $line[1]) {
                $bin = Mage::helper('tradetested_core')->guidToBin($line[1]);
                $product->setGuidBin($bin)->save();
                echo '.';
            }
        }
        fclose($fh);
    }

    public function usageHelp()
    {
        return <<<USAGE
USAGE;
    }
}

$shell = new TestShell();
$shell->run();
