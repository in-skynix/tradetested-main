<?php
//Shell script for temporary actions
require_once 'abstract.php';
class Mage_Shell_Temp extends Mage_Shell_Abstract
{
    public function run()
    {
        $helper = Mage::helper('avid_related_products');
        $from = 0;
        while (!$from || $result->count()) {
            $query = new Elastica_Query();
            $query->setSize(10000)->setFrom($from);
            $from += 10000;
            $result = $helper->getIndex()
                ->getType($helper->getDocType())
                ->search($query);
            var_dump($result->count());
            $fp = fopen('file.csv', 'a');
            fputcsv($fp, array('session_id', 'product_ids'));
            foreach ($result as $_r) {
                $ids = $_r->__get('product_ids');
                fputcsv($fp, array($_r->getId(), implode(':', $ids)));
            }
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
USAGE;
    }
}

$shell = new Mage_Shell_Temp();
$shell->run();
