<?php
require_once 'abstract.php';
class TestShell extends Mage_Shell_Abstract
{
    protected $_helper;
    protected $_scheduler;
    protected $_queue;

    public function run()
    {
        $reviews = Mage::getModel('review/review')->getCollection()
            ->addFieldToFilter('status_id', Mage_Review_Model_Review::STATUS_APPROVED)
            ->addFieldToFilter('store_id', 2);
        foreach ($reviews as $review) {
            $review = Mage::getModel('review/review')->load($review->getId());
            if ($review->getStoreId() == 2) {
                $appEmulation = Mage::getSingleton('core/app_emulation');
                $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($review->getStoreId());
                $data = Mage::helper('tradetested_messaging/extractor_review')->extractMessageData($review);
                if (isset($data['action'])) {
                    $event = Mage::getModel('messenger/event')
                        ->setName('review_update')
                        ->setData($data);
                    Mage::getSingleton('messenger/di')
                        ->newInstance('messenger/event_dispatcher')
                        ->dispatch($event);
                }
                $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
                echo ".";
            }
        }
    }

    public function usageHelp()
    {
        return <<<USAGE
USAGE;
    }
}

$shell = new TestShell();
$shell->run();
