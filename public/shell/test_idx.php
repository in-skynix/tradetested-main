<?php
require_once 'abstract.php';

class TestShell extends Mage_Shell_Abstract
{
    protected $_helper;
    protected $_scheduler;
    protected $_queue;

    public function run()
    {
        $order = Mage::getModel('sales/order')->load(50363);
//        $order = Mage::getModel('sales/order')->load(50344);
//        $order = Mage::getModel('sales/order')->load(50328);
        $event = Mage::getModel('messenger/event')
            ->setName('order_created')
            ->setData(Mage::helper('tradetested_messaging/extractor_order')->extractMessageData($order));
        Mage::getSingleton('messenger/di')
            ->newInstance('messenger/event_dispatcher')
            ->dispatch($event);
//        $rootId = Mage::app()->getStore(2)->getRootCategoryId();
//        $collection = Mage::getResourceModel('catalog/category_collection')
//            ->setStoreId(2)
//            ->addFieldToFilter('path', array('like'=> "1/{$rootId}/%"))
//            ->addAttributeToSelect([
//                'entity_id', 'name', 'is_active', 'position', 'meta_description',
//                'meta_keywords', 'meta_title', 'url_path', 'description_bottom'
//            ], 'left');
//        echo $collection->getSelect();
    }

    public function usageHelp()
    {
        return <<<USAGE
USAGE;
    }
}

$shell = new TestShell();
$shell->run();
