<?php
//Shell script for temporary actions
require_once 'abstract.php';
class Mage_Shell_Temp extends Mage_Shell_Abstract
{
    public function run()
    {
        $appEmulation = Mage::getSingleton('core/app_emulation');
        //Start environment emulation of the specified store
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation(1);
        $html = Mage::helper('avid_gravitator/mailchimp')->send();
        var_dump($html);
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
USAGE;
    }
}

$shell = new Mage_Shell_Temp();
$shell->run();
