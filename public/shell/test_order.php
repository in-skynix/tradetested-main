<?php
require_once 'abstract.php';

class TestShell extends Mage_Shell_Abstract
{
    public function run()
    {
        $orders = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('entity_id', ['gt' => 67161]);
        foreach($orders as $order) {
            $order = Mage::getModel('sales/order')->load($order->getId());
            $data = Mage::helper('tradetested_messaging/extractor_order')
                ->extractMessageData($order);
            Mage::helper('tradetested_messaging')->sendMessage('order_created', $data);
        }
    }

    public function usageHelp()
    {
        return <<<USAGE
USAGE;
    }
}

$shell = new TestShell();
$shell->run();
