<?php
require_once 'abstract.php';

class TestShell extends Mage_Shell_Abstract
{
    public function run()
    {
        $json = <<<JSON
{
  "status": "1",
  "meta_title": "",
  "stock_data": {
    "is_in_stock": true,
    "quantity": 10
  },
  "description": "Buy direct from the importer - Trade Tested\n\nComes with 1 Year warranty for peace of mind.\n\nChildren's playhouse constructed from top quality pine. Features a waterproof asphalt shingle roof, front porch with shelter plus window frames and doors. Comes ready to paint or stain.\n\nWe've intentionally chosen to provide your playhouse paint-free so you can let the little-ones choose their favourite paint or varnish, and maybe even help with putting it on!\n\nDETAILS\n- Material: Heat treated pine\n- Thickness of walls: 15mm\n- Thickness of floor: 15mm\n- Roof Material: Colourised waterproof asphalt shingle\n- Includes framed window openings and doors (unpainted)\n\nOptional perspex windows (full set) available.\n\nAssembled size: 184 (L) x 118 (W) x 123 (H) cm\n\n- Internal dimensions (room): 108 x 98cm\n- Dimensions of the front deck area: 54.5 x 98cm\n- Height of side wall at lowest point: 93cm\n- Dimensions of the door: 37 x 79cm\n\nCarton size\nCarton 1: 181 X 100 X 18cm\nCarton 2: 102.5 x 36 x 8cm\n\nNOTE: To ensure longevity the wood must be stained/painted when kept outside.",
  "weight": 75.57,
  "ebay_title_appendix": "",
  "price": 699,
  "tm_relist": "0",
  "special_price": 549,
  "package_id": "15",
  "tm_fixedend_limit": 0,
  "special_from_date": 1484388340,
  "guid": "2ba3b89e-bc59-4e58-8099-1b419df66145",
  "sku": "G000BOM-130002B",
  "tm_name_variants": "KID'S PLAY HOUSE WOODEN\r\nPlayhouse Wooden Children's\r\nCHILDRENS PLAYHOUSE WOODEN\r\nKID'S CUBBY HOUSE WOODEN\r\nPLAYHOUSE CHILDRENS WOODEN KID'S",
  "name": "Kid's Wooden Playhouse 4",
  "price_message": "",
  "stock_description": "",
  "websites": "base",
  "special_to_date": "",
  "short_description": "",
  "shipping_description": ""
}
JSON;
        $json2 = '{"sku": "000BOM-130001", "stock_data": {"is_in_stock": true, "quantity": 10}, "guid": "4ea3b89e-bc59-4e58-8099-1b419df66145", "stock_description": ""}';
        $json3 = '{"sku": "000BOM-130001", "url_key": "test-url'.uniqid().'", "guid": "4ea3b89e-bc59-4e58-8099-1b419df66145"}';
        $json4 = '{"url_key": "test-url'.uniqid().'", "guid": "e1406061-9058-40ed-b913-1a494f3907f7", "sku": "000BOM-AU-AG103M-CREAM-1-93"}';
        $json = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json);
//        var_dump(json_decode($json)); die();
        Mage::helper('tradetested_messaging/importer_product')->importProduct(json_decode($json, true));
    }

    public function usageHelp()
    {
        return <<<USAGE
USAGE;
    }
}

$shell = new TestShell();
$shell->run();
