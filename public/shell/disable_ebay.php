<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 09/03/15
 * Time: 11:01
 */
require_once 'abstract.php';
class Mage_Shell_DisableEbay extends Mage_Shell_Abstract
{
    public function run()
    {
        $nicks = array(
            'default',
            'amazon',
            'buy',
            'ebay',
            'play',
            'common',
            'configuration',
            'development',
            'logs_clearing',
            'processing',
            'servicing',
            'synchronization',
        );
        foreach ($nicks as $_nick) {
            Mage::helper('M2ePro/Module')->getConfig()->getGroupValue('/component/'.$_nick.'/', 'mode', 0);
            Mage::helper('M2ePro/Module')->getConfig()->getGroupValue('/component/'.$_nick.'/', 'allowed', 0);
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
USAGE;
    }
}

$shell = new Mage_Shell_DisableEbay();
$shell->run();

  