<?php
require_once 'abstract.php';
class Mage_Shell_Dataflow extends Mage_Shell_Abstract
{
	/**
     * Run script
     *
     */
    public function run()
    {
$userModel = Mage::getModel('admin/user');
$userModel->setUserId(0);
Mage::getSingleton('admin/session')->setUser($userModel);
$profile = Mage::getModel('dataflow/profile');
$profile->load($this->getArg('profile_id'));
$guiData = $profile->getGuiData();
$guiData['file']['path'] = $this->getArg('path');
$guiData['file']['filename'] = $this->getArg('file');
$profile->setGuiData($guiData)->save();
$profile->load($this->getArg('profile_id'));
$profile->run();
foreach($profile->getExceptions() as $exception) {
	echo $exception->getMessage()."\n";
}
$batchModel = Mage::getSingleton('dataflow/batch')->load($profile->getBatchId());
$batchImportModel = $batchModel->getBatchImportModel();
$importIds = $batchImportModel->getIdCollection();

$adapter = Mage::getModel($batchModel->getAdapter());
$adapter->setBatchParams($batchModel->getParams());

$errors = array();
$saved  = 0;
$totalRows = count($importIds);
foreach ($importIds as $importId) {
	$saved ++;
    $batchImportModel->load($importId);
    if (!$batchImportModel->getId()) {
        echo Mage::helper('dataflow')->__('Skip undefined row.')."\n";
        continue;
    }
    try {
        $importData = $batchImportModel->getBatchData();
        $adapter->saveRow($importData);
		$percent = round($saved/$totalRows*100);
		fwrite(STDOUT, "\r{$saved}/{$totalRows} ({$percent}%)");
    }
    catch (Exception $e) {
        echo "{$saved}:{$importId}:". $e->getMessage()."\n";
        continue;
    }
}


	}
	/**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage instructions here
USAGE;
    }
}
$shell = new Mage_Shell_Dataflow();
$shell->run();
