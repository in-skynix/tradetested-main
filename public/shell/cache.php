<?php
/**
 * Avid Cache Cleaning Utility
 */
require_once 'abstract.php';

class Mage_Shell_Cache extends Mage_Shell_Abstract
{
    public function run()
    {
        if ($this->getArg('clean')) {
            $tags = $this->getArg('tag') ? array($this->getArg('tag')) : null;
            Mage::app()->getCacheInstance()->clean($tags);
            echo "Cache tags cleaned\n";
        } else {
            echo $this->usageHelp();
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f cache.php -- [options]
        php -f cache.php -- clean --days 1

  clean             Clean Cache
  --tags <tag>      Clean specific tag
  help              This help

USAGE;
    }
}

$shell = new Mage_Shell_Cache();
$shell->run();
