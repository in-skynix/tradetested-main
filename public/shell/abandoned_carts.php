<?php
require_once 'abstract.php';
class Avid_Shell_AbandonedCarts extends Mage_Shell_Abstract
{
    protected $_helper;
    protected $_scheduler;
    protected $_queue;
    public function run()
    {
        $this->_helper      = Mage::helper('avid_abandoned_cart_reminders');
        $this->_scheduler   = Mage::helper('avid_abandoned_cart_reminders/scheduler');
        $this->_queue       = Mage::helper('avid_abandoned_cart_reminders/queue');
        if ($this->getArg('send_reminders')) {
            $this->_queue->start();
            register_shutdown_function(array(&$this->_queue,'end'));
            $this->_setStatus('Sending. Finding reminders to send.');
            $remindersToSend = Mage::helper('avid_abandoned_cart_reminders/scheduler')->getRemindersToSend();
            $count = $remindersToSend->count();
            $x = 0;
            foreach ($remindersToSend as $_reminder) {
                $x ++;
                $this->_setStatus("Sending {$x}/{$count}");
                $_reminder->send();
            }
            $this->_setStatus("Finished Sending {$count} Reminders.");
            $this->_queue->end();
        }

    }

    protected function _setStatus($msg)
    {
        $this->_queue->setMsg($msg);
    }

    public function usageHelp()
    {
        return <<<USAGE
USAGE;
    }
}

$shell = new Avid_Shell_AbandonedCarts();
$shell->run();
