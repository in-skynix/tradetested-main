<?php
require_once 'abstract.php';

class TestShell extends Mage_Shell_Abstract
{

    public function run()
    {
        Mage::getModel('tradetested_shipping/postcode')->getResource()->getReadConnection()->resetDdlCache();
        $this->_withCsvLines("postcodes.csv", function ($data) {
            $data = $this->_filterData($data, ['postcode', 'country_code', 'name', 'state', 'latitude', 'longitude']);
            /** @var TradeTested_Shipping_Model_Postcode $pc */
            $pc = Mage::getModel('tradetested_shipping/postcode')->loadByPostcode($data['postcode'], $data['country_code']);
            if ($pc->getId()) {
                return;
            }
            $pc->setData($data)->save();
            echo "*";
        });

        Mage::getResourceModel('tradetested_shipping/depot_rate')->truncate();
        Mage::getResourceModel('tradetested_shipping/depot')->truncate();
        $depotIds = [];
        $packageOptions = ['*' => '*'];
        $options = Mage::getSingleton('eav/config')->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'package_id')
            ->getSource()->getAllOptions();
        foreach ($options as $_option) {
            $packageOptions[$_option['label']] = $_option['value'];
        }
        return;
        $this->_withCsvLines("depot_rates.csv", function ($data) use (&$depotIds, $packageOptions) {
            $depotData = $this->_filterData($data, [
                'country_code',
                ['depot_code', 'code'],
                ['depot_name', 'name'],
                ['depot_address', 'address'],
                ['depot_postcode', 'postcode'],
                ['depot_state', 'state'],
                'latitude',
                'longitude'
            ]);
            $code = $depotData['code'];

            if (!isset($depotIds[$code])) {
                if (!$depotData['latitude']) {
                    $postcodeObject = Mage::getModel('tradetested_shipping/postcode')->loadByPostcode(
                        $depotData['postcode'], $depotData['country_code']
                    );
                    $depotData['latitude'] = $postcodeObject['latitude'];
                    $depotData['longitude'] = $postcodeObject['longitude'];
                }
                $depot = Mage::getModel('tradetested_shipping/depot')->load($code, 'code')->setData($depotData)->save();
                $depotIds[$code] = $depot->getId();
            }

            $rateData = $this->_filterData(
                $data,
                ['package_id', 'website_id', 'weight_from', 'weight_to', 'price', 'algorithm', 'shipping_time_estimate']
            );
            $rateData['depot_id'] = $depotIds[$code];
            $rateData['package_id'] = $packageOptions[$rateData['package_id']];
            Mage::getModel('tradetested_shipping/depot_rate')->setData($rateData)->save();
            echo ".";
        });
    }

    protected function _withCsvLines(string $filename, $function)
    {
        $fh      = fopen($filename, "r");
        $headers = fgetcsv($fh, 1024);
        while (!feof($fh)) {
            $line = fgetcsv($fh, 1024);
            $data = [];
            foreach ($line as $_key => $_val) {
                $data[$headers[$_key]] = $_val;
            }
            if (count($data)) {
                call_user_func($function, $data);
            }
        }
        fclose($fh);

    }

    protected function _filterData(array $data, array $keys)
    {
        $filteredData = [];
        foreach ($keys as $_keyData) {
            $key                   = is_array($_keyData) ? $_keyData[0] : $_keyData;
            $newKey                = is_array($_keyData) ? $_keyData[1] : $_keyData;
            $filteredData[$newKey] = $data[$key] ?? null;
        }

        return $filteredData;
    }

    public function usageHelp()
    {
        return <<<USAGE
USAGE;
    }
}

$shell = new TestShell();
$shell->run();
