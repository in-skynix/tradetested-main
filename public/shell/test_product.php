<?php
require_once 'abstract.php';

class TestShell extends Mage_Shell_Abstract
{
    public function run()
    {
//        $product = Mage::getModel('catalog/product')->load(746);
        $products = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect(['url_key', 'name', 'url_path']);
        foreach ($products as $_product) {
            Mage::helper('tradetested_core')->convertBinToGuid($_product);
            if (
                ($_product->getGuid())
                && ($data = Mage::helper('tradetested_messaging/extractor_product')->extractMessageData($_product))
            ){
                Mage::helper('tradetested_messaging')->sendMessage('product_update', $data);
            }
        }
    }

    public function usageHelp()
    {
        return <<<USAGE
USAGE;
    }
}

$shell = new TestShell();
$shell->run();
