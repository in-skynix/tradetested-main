<?php
require_once 'abstract.php';

class TestShell extends Mage_Shell_Abstract
{
    public function run()
    {
        Mage::getModel('tradetested_core/task_manager')->sub(function($client, $channel, $message){
            echo $message;
            echo "\n";
        });
    }

    public function usageHelp()
    {
        return <<<USAGE
USAGE;
    }
}

$shell = new TestShell();
$shell->run();
