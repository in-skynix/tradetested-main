<?php
require_once 'abstract.php';
class TestShell extends Mage_Shell_Abstract
{
    protected $_helper;
    protected $_scheduler;
    protected $_queue;

    public function run()
    {
        $job = Mage::getModel('cron/schedule')->setJobCode('trademe_sync_base');
        Mage::getSingleton('trademe/observer')->sync($job);
    }

    public function usageHelp()
    {
        return <<<USAGE
USAGE;
    }
}

$shell = new TestShell();
$shell->run();
