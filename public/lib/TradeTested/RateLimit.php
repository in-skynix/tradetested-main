<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 05/02/2016
 * Time: 11:26
 */
namespace TradeTested;
require_once dirname(__FILE__).'/../../../vendor/autoload.php';
class RateLimit
{
    protected $_client;

    public function __construct($settings = array())
    {
        $defaultSettings = array(
            'host'      => 'localhost',
            'port'      => 6379,
            'db'        => 2,
            'password'  => null
        );
        $settings = array_merge($defaultSettings, $settings);
        $this->_client = new \Credis_Client(
            $settings['host'], $settings['port'], null, $settings['db'], $settings['password']
        );
    }

    /**
     * @param array $tags
     * @param array $limits e.g. array(1 => 5, 60 => 100) = 5r/s, or 60 requests per minute.
     * @param bool $increment Whether to increment the counter or not. If false, performs a test only
     * @return bool
     *
     * https://www.binpress.com/tutorial/introduction-to-rate-limiting-with-redis/155
     */
    public function isOverLimit($tags = array(), $limits = array(1 => 5, 60 => 200))
    {
        $rLimits = array();
        foreach ($limits as $_timeframe => $_count) {
            $rLimits[] = array($_timeframe, $_count);
        }
        $script = <<<LUA
local limits = cjson.decode(ARGV[1])
local now = tonumber(ARGV[2])
for i, limit in ipairs(limits) do
    local duration = limit[1]

    local bucket = ':' .. duration .. ':' .. math.floor(now / duration)
    for j, id in ipairs(KEYS) do
        local key = id .. bucket

        local count = redis.call('INCR', key)
        redis.call('EXPIRE', key, duration)
        if tonumber(count) > limit[2] then
            return 1
        end
    end
end
return 0
LUA;
        return !!($this->_client->eval(
            $script,
            array($this->_getIdentifier($tags)),
            array(
                json_encode($rLimits),
                time(),
            )
        ));

    }

    /**
     * Add a 'lock' entry to db
     *
     * @param $tags
     * @param $duration
     * @return $this
     */
    public function addLock($tags, $duration)
    {
        $this->_client->setEx('lock.'.$this->_getIdentifier($tags), $duration, true);
        return $this;
    }

    /**
     * Remove a 'lock' entry from db
     * @param $tags
     * @return $this
     */
    public function removeLock($tags)
    {

        $script = <<<LUA
for _,k in ipairs(redis.call('keys', ARGV[1])) do
    redis.call('del', k)
end
LUA;
        $this->_client->eval($script, null, array($this->_getIdentifier($tags).':*'));
        $this->_client->del('lock.'.$this->_getIdentifier($tags));
        return $this;
    }

    /**
     * Check if 'lock' entry exists in db
     * @param $tags
     * @return int
     */
    public function lockExists($tags)
    {
        return $this->_client->exists('lock.'.$this->_getIdentifier($tags));
    }

    protected function _getIdentifier($tags)
    {
        ksort($tags);
        return md5(json_encode($tags));
    }
}