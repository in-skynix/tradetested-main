var radioSerializerController = Class.create(serializerController, {
    initialize: function(hiddenDataHolder, predefinedData, inputsToManage, grid, reloadParamName){
        //Grid inputs
        this.tabIndex = 1000;
        this.inputsToManage       = inputsToManage;
        this.multidimensionalMode = inputsToManage.length > 0;

        //Hash with grid data
        this.gridData             = this.getGridDataHash(predefinedData);

        //Hidden input data holder
        this.hiddenDataHolder     = $(hiddenDataHolder);
        this.hiddenDataHolder.value = this.serializeObject();

        this.grid = grid;

        // Set old callbacks
        this.setOldCallback('row_click', this.grid.rowClickCallback);
        this.setOldCallback('init_row', this.grid.initRowCallback);
        this.setOldCallback('checkbox_check', this.grid.checkboxCheckCallback);

        //Grid
        this.reloadParamName = reloadParamName;
        this.grid.reloadParams = {};
        this.grid.reloadParams[this.reloadParamName] = this.getDataForReloadParam();
        this.grid.rowClickCallback = this.rowClick.bind(this);
        this.grid.initRowCallback = this.rowInit.bind(this);
        this.grid.checkboxCheckCallback = this.registerData.bind(this);
        this.grid.rows.each(this.eachRow.bind(this));
    },
    registerData : function(grid, element, checked) {
        var tr = element.up('tr');
        this.gridData.set(tr.productId, {selected: element.value});
        for(var i = 0; i < tr.inputElements.length; i++) {
            tr.inputElements[i].disabled = false;
            this.gridData.get(tr.productId)[tr.inputElements[i].name] = tr.inputElements[i].value;
        }

        this.hiddenDataHolder.value = this.serializeObject();
        this.grid.reloadParams = {};
        this.grid.reloadParams[this.reloadParamName] = this.getDataForReloadParam();
        this.getOldCallback('checkbox_check')(grid, element, checked);
    },
    rowInit : function(grid, row) {
        var selectors = this.inputsToManage.map(function (name) { return ['input[name="' + name + '"]', 'select[name="' + name + '"]']; });
        var radioButtons = Element.select(row, 'input[type=radio]');
        row.productId = radioButtons[0].name.replace( /^\D+/g, '');
        var inputs = $(row).select.apply($(row), selectors.flatten());
        if (inputs.length > 0) {
            row.inputElements = inputs;
            for(var i = 0; i < inputs.length; i++) {
                inputs[i].tabIndex = this.tabIndex++;
                Event.observe(inputs[i],'keyup', this.inputChange.bind(this));
                Event.observe(inputs[i],'change', this.inputChange.bind(this));
            }
        }
        this.getOldCallback('init_row')(grid, row);
    },
    inputChange : function(event) {
        var element = Event.element(event);
        var row = element.up('tr');
        if(data = this.gridData.get(row.productId)){
            data[element.name] = element.value;
            this.hiddenDataHolder.value = this.serializeObject();
            this.grid.reloadParams = {};
            this.grid.reloadParams[this.reloadParamName] = this.getDataForReloadParam();
        }
    },
    rowClick : function(grid, event) {
        var trElement = Event.findElement(event, 'tr');
        if(trElement){
            if (event.target.tagName == 'INPUT') {
                var radioButtons = Element.select(trElement, 'input:checked[type=radio]');
                this.grid.setCheckboxChecked(radioButtons[0], true);
            } else {
                var radioButtons = Element.select(trElement, 'input[type=radio]');
                var selectIndex = 0;
                radioButtons.each(function (radioButton, index) {
                    if (radioButton.checked) {
                        selectIndex = index + 1
                    }
                });
                if (selectIndex >= radioButtons.length) {
                    selectIndex = 0
                }
                this.grid.setCheckboxChecked(radioButtons[selectIndex], true);
            }
        }
        this.getOldCallback('row_click')(grid, event);
    },
    getDataForReloadParam: function(){
        return this.serializeObject();
    }
});
