if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}
(function(window) {
        
        var Component = {
            controller: function(config) {
                this.tasks = m.prop({});
                var ctrl = this;
                var socket = glue(config.url);
                socket.send(config.jwt);
                socket.onMessage(function(data) {
                    var taskData = JSON.parse(data);
                    if (Object.keys(taskData).length) {
                        var tasks = ctrl.tasks();
                        tasks[taskData.task_id] = taskData;
                        ctrl.tasks(tasks);
                        m.redraw();
                    }
                });
                
            },
            view: function(ctrl) {
                var tasks = ctrl.tasks();
                return m('ul', Object.keys(tasks).map(function(key){
                    var task = tasks[key];
                    return m('li',[
                        m('span.task_id', task.task_id),
                        m('span.task_progress', m('span.meter', {style: 'width:'+task.percent+'%'}, task.percent+'%')),
                        m('span.task_note', task.note)
                    ]);
                }));
            }
        };
        
        window.TradeTested.TaskNotifier = function(el, config) {
            m.mount(el, m.component(Component, config))
        }
})(window);
