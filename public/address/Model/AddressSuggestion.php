<?php
namespace AddressLookup\Model;
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 14/01/2016
 * Time: 11:52
 */
class AddressSuggestion extends AbstractModel
{
    protected $_dataMap = array(
        'label' => 'FullAddress',
        'address_id' => 'AddressId',
    );

    /**
     * Set Address ID
     *
     * We hide the CourierPost ID, but still make it retrievable by encoding the ID.
     * @param $id
     * @return $this
     */
    public function setAddressId($id)
    {
        if (is_numeric($id)) {
            $id = \AddressLookup\Helper\Auth::encode(array('aid' => $id));
        }
        return $this->setData('address_id', $id);
    }

    /**
     * Check for a street direction in the suggestion
     *
     * Courier Post returns street directions (e.g. Customs St West) in suggestions, but not when
     * retrieving the full address, we extract it here, then pass to the controller when fetching full address.
     * @return $this
     */
    public function checkForDirection()
    {
        $matches = array();
        //Check only before first comma after a number, otherwise could get suburb/town with direction.
        if (preg_match('/\d[^,]*(north|south|east|west)(,|$)/', strtolower($this->getLabel()), $matches)) {
            $this->addSupplementaryData('street_direction', ucfirst($matches[1]));
        }
        return $this;
    }

    /**
     * Get data to be returned by controller
     *
     * We add extra data to the label if it is not complete according to what the customer entered.
     *
     * @return mixed
     */
    public function getDataWithExtendedLabel()
    {
        $data = $this->getData();
        $data['label'] = $this->getExtendedLabel();
        return $data;
    }

    /**
     * Get label including extra data the customer has provided
     *
     * @return string
     */
    public function getExtendedLabel()
    {
        $label = $this->getLabel();
        $floor = preg_match('/(floor|level)/', strtolower($label)) ? null : $this->getSupplementaryData('floor');
        $unit = preg_match('/(unit|flat)/', strtolower($label)) ? null : implode(" ", array_filter(array(
                $this->getSupplementaryData('unit_type'), $this->getSupplementaryData('unit_value')
            )));
        $floorUnit = implode(" ", array_filter(array($unit, $floor)));
        return implode(", ", array_filter(array(
            $this->getFilteredCustomerInput(),
            $floorUnit,
            $label
        )));
    }

    /**
     * Get Supplementary Data
     *
     * Supplementary data should be provided to the controller when fetching a full address,
     * To help it fill in address data that may not be available in database.
     *
     * @param null $key
     * @return mixed|null
     */
    public function getSupplementaryData($key = null)
    {
        $data = $this->getData('supplementary_data');
        if ($key) {
            return isset($data[$key]) ? $data[$key] : null;
        } else {
            return $data;
        }
    }

    /**
     * Set Some supplementary data extracted by QueryExtractor
     *
     * e.g. Direction is provided in suggestion, but not full address.
     * e.g. Level, Unit extracted by QueryExtractor, some suggestions may not have this data.
     *
     * @param $key
     * @param $value
     * @return $this
     */
    public function addSupplementaryData($key, $value = null)
    {
        if (is_array($key)) {
            foreach (array('floor', 'unit_value', 'unit_type') as $_allowedKey) {
                $data = $key;
                if (isset($data[$_allowedKey]) && !$this->getDataUsingMethod($_allowedKey)) {
                    $this->addSupplementaryData($_allowedKey, $data[$_allowedKey]);
                }
            }
        } elseif ($key) {
            if (!($data = $this->getSupplementaryData())) {
                $data = array();
            }
            $data[$key] = $value;
            $this->setSupplementaryData($data);
        }
        return $this;
    }
}
