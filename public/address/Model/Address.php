<?php
namespace AddressLookup\Model;
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 14/01/2016
 * Time: 17:00
 *
 * @method string getBuildingName()
 * @method $this setBuildingName(string $value)
 * @method string getCompanyName()
 * @method $this setCompanyName(string $value)
 * @method string getFloor()
 * @method $this setFloor(string $value)
 * @method int getPostalCode()
 * @method $this setPostalCode(int $value)
 * @method int getIsRuralDelivery()
 * @method string getRdNumber()
 * @method $this setRdNumber(string $value)
 * @method string getStreet()
 * @method $this setStreet(string $value)
 * @method string getStreetAlpha()
 * @method $this setStreetAlpha(string $value)
 * @method int getStreetNumber()
 * @method $this setStreetNumber(int $value)
 * @method string getStreetType()
 * @method $this setStreetType(string $value)
 * @method string getSuburb()
 * @method $this setSuburb(string $value)
 * @method string getTown()
 * @method $this setTown(string $value)
 * @method string getUnitType()
 * @method $this setUnitType(string $value)
 * @method string getUnitValue()
 * @method $this setUnitValue(string $value)
 * @method string getStreetDirection()
 * @method $this setStreetDirection(string $value)
 * @method string getFilteredCustomerInput()
 *
 * @todo: Refactor into separate formatter objects for Magento vs OpenERP.
 */
class Address extends AbstractModel
{
    /*
    array(21) {
  ["BuildingName"]=>
  NULL
  ["CompanyName"]=>
  string(0) ""
  ["DepotName"]=>
  string(22) "AKL Central East Fleet"
  ["Floor"]=>
  NULL
  ["GatedCommunityFlag"]=>
  NULL
  ["Latitude"]=>
  float(-37.0524)
  ["Longitude"]=>
  float(174.961)
  ["PostCode"]=>
  string(4) "2110"
  ["RDFlag"]=>
  string(1) "N"
  ["RDNumber"]=>
  NULL
  ["RateableSuburbID"]=>
  int(15978)
  ["RunNumber"]=>
  string(8) "ACP565  "
  ["Street"]=>
  string(6) "Cooper"
  ["StreetAlpha"]=>
  NULL
  ["StreetNumber"]=>
  int(12)
  ["StreetType"]=>
  string(5) "Place"
  ["Suburb"]=>
  string(8) "Papakura"
  ["SuburbID"]=>
  int(1196)
  ["Town"]=>
  string(8) "Papakura"
  ["UnitType"]=>
  NULL
  ["UnitValue"]=>
  NULL
}
     */

    /**
     * @var array
     * Maps data to courier post data
     */
    protected $_dataMap = array(
        'building_name' => 'BuildingName',
        'company_name'  => 'CompanyName',
        'floor' => 'Floor',
        'postal_code' => 'PostCode',
        'is_rural_delivery' => 'RDFlag',
        'rd_number' => 'RDNumber',
        'street' => 'Street',
        'street_alpha' => 'StreetAlpha',
        'street_number' => 'StreetNumber',
        'street_type' => 'StreetType',
        'suburb' => 'Suburb',
        'town' => 'Town',
        'unit_type' => 'UnitType',
        'unit_value' => 'UnitValue',
    );

    /**
     * Set first line entered directly by customer
     *
     * If customer has entered a unit/flat identical to that returned by courierpost,
     * we ignore that part of the custom input data so we can format the unit line/prefix/postfix correctly
     *
     * @todo: This is likely no longer needed, now that the QueryExtractor removes unit/flat/floor.
     * However, that is done by regex, so might not always match.
     */
    public function setFilteredCustomerInput($line)
    {
        $line = str_ireplace($this->getUnitLine(), "", $line);
        $line = str_ireplace($this->getFloor(), "", $line);
        $line = ltrim($line, " \t\n\r\0\x0B,");
        return $this->setData('filtered_customer_input', $line);
    }

    /**
     * Set Is RD
     *
     * CourierPost returns Y/N. We want boolean.
     */
    public function setIsRuralDelivery($flag)
    {
        return $this->setData('is_rural_delivery', ($flag && ($flag != 'N')));
    }

    /**
     * Set Some supplementary data only available in suggestion from courierpost
     *
     * e.g. Direction is provided in suggestion, but not full address.
     * e.g. Floor or Unit may have been extracted from input data, but not always available in database.
     * @param $data
     */
    public function setSupplementaryData($data)
    {
        foreach (array('street_direction', 'floor', 'unit_value', 'unit_type') as $_allowedKey) {
            if (isset($data[$_allowedKey]) && !$this->getDataUsingMethod($_allowedKey)) {
                $this->setDataUsingMethod($_allowedKey, $data[$_allowedKey]);
            }
        }
    }

    /**
     * Get Address Data for Magento format.
     *
     * @return array
     */
    public function getMagentoData()
    {
        return $this->sign(array(
            'street_1'  => $this->getStreet1(),
            'street_2'  => $this->getStreet2(),
            'suburb'    => $this->getSuburbLine(),
            'city'      => $this->getTown(),
            'postcode'  => $this->getPostalCode(),
        ));
    }

    /**
     * Get Street1 for Magento
     *
     * This is either the delivery line, or a combination of Unit/Floor/Company/Building/CustomerInput
     *
     * @return string
     */
    public function getStreet1()
    {
        if ($this->_isDeliveryLineFirst()) {
            return $this->getDeliveryLine();
        } else {
            return $this->_getUnitFloorCompanyInput();
        }
    }

    /**
     * Get Street2 for Magento
     *
     * @return string
     */
    public function getStreet2()
    {
        if (!$this->_isDeliveryLineFirst()) {
            return $this->getDeliveryLine();
        }
    }

    /**
     * Get Suburb Line for Magento
     *
     * RD Addresses should have RD Number in place of Suburb
     *
     * @return string
     */
    public function getSuburbLine()
    {
        return ($this->getIsRuralDelivery()) ? $this->getRdNumber() : $this->getSuburb();
    }

    /**
     * Gets The Delivery Line, as specified by NZ Post
     *
     * @return string
     */
    public function getDeliveryLine()
    {
        return implode(' ', array_filter(array(
            trim($this->getUnitPrefix()).trim($this->getStreetNumber())
            .trim($this->getStreetAlpha()).trim($this->getUnitPostFix()),
            $this->getStreet(),
            $this->getStreetType(),
            $this->getStreetDirection(),
        )));
    }

    /**
     * Get Floor and Unit formatted together
     *
     * @return string
     */
    public function getFloorAndUnit()
    {
        return implode(' ', array_filter(array(
            $this->getUnitLine(),
            $this->getFloor(),
        )));
    }

    /**
     * Get the unit line.
     *
     * Will return null if there is no unit for the address, or we should otherwise not display as a full unit line.
     *
     * @return string
     */
    public function getUnitLine()
    {
        if ($this->getUnitValue() && $this->_showFullUnitLine()) {
            return $this->getUnitType()." ".$this->getUnitValue();
        }
    }

    /**
     * Get Unit value as a prefix for use in the Delivery Line
     *
     * Returns null if we cannot prefix the unit, or if we would be showing the unit as a full line.
     *
     * @return string
     */
    public function getUnitPrefix()
    {
        if ($this->_canPrefixUnit() && !$this->_showFullUnitLine()) {
            return $this->getUnitValue()."/";
        }
    }

    /**
     * Get Unit value as a postfix for use in the Delivery Line
     *
     * Returns null if we cannot postfix the unit, or if we would be showing the unit as a full line.
     *
     * @return string
     */
    public function getUnitPostfix()
    {
        if ($this->_canPostfixUnit() && !$this->_showFullUnitLine()) {
            return $this->getUnitValue();
        }
    }

    /**
     * Add a signature element to an array of data.
     *
     * The signature can later be used to verify that the data has not changed.
     *
     * @param array $data
     * @return array
     */
    public function sign($data)
    {
        $signature = hash_hmac('sha256', json_encode($data), $this->_getSignatureKey());
        $data['signature'] = $signature;
        return $data;
    }

    /**
     * CAN The Unit be prefixed?
     *
     * - Don't prefix whole words
     * - Don't prefix a letter, if it can be postfixed
     *
     * @return bool
     */
    protected function _canPrefixUnit()
    {
        $val = $this->getUnitValue();
        return ($val && strlen($val) <= 3 && !$this->_canPostfixUnit());
    }

    /**
     * CAN The Unit be postfixed?
     *
     * - Is it all letters?
     * - Is it not a full word?
     * - Is there no existing street alpha?
     *
     * @return bool
     */
    protected function _canPostfixUnit()
    {
        $val = $this->getUnitValue();
        return ($val && strlen($val) <= 3 && !$this->getStreetAlpha() && !preg_match('/[^A-Za-z]/', $val));
    }

    /**
     * Should Street1 be Delivery Line?
     *
     * @return bool
     */
    protected function _isDeliveryLineFirst()
    {
        return (!$this->_getUnitFloorCompanyInput());
    }

    /**
     * Get Street1 details if not Delivery Line
     *
     * - Company Name: if we don't have a unit line.
     * - Building Name: if should be displayed.
     * - Customer Input.
     * - Floor and Unit
     * - Building name if required or there would be an empty space
     * @return string
     */
    protected function _getUnitFloorCompanyInput()
    {
        $companyName = $this->getUnitLine() ? "" : $this->getCompanyName();
        $buildingName = $this->_shouldDisplayBuildingName() ? $this->getBuildingName() : "";
        return implode(', ', array_filter(array(
            $this->getFilteredCustomerInput(),
            $companyName,
            $this->getFloorAndUnit(),
            $buildingName,
        )));
    }

    /**
     * Should we display Building Name?
     *
     * Two cases where we should:
     * 1) It is required because we have no street number
     * 2) We can include it because there would be an empty line otherwise
     *
     * @return bool
     */
    protected function _shouldDisplayBuildingName()
    {
        return (
            !$this->getStreetNumber()
            || !($this->getFilteredCustomerInput() || $this->getCompanyName() || $this->getFloorAndUnit())
        );
    }

    /**
     * Should we show Full Unit Details, rather than Prefix/Postfix
     *
     * cases where we should:
     * 1) The unit cannot be prefixed/postfixed.
     * 2) There is a floor in the address, in which case we always display line and floor together
     * 3) There would otherwise be an empty line.
     * @return bool
     */
    protected function _showFullUnitLine()
    {
        return (
            $this->getUnitValue()
            && (
                (!$this->getFilteredCustomerInput() && !$this->getCompanyName())
                || (!$this->_canPrefixUnit() && !$this->_canPostfixUnit())
                || $this->getFloor()
            )
        );
    }

    /**
     * @todo: fixme
     * @return string
     */
    protected function _getSignatureKey()
    {
        return 'qazpio123';
    }
}