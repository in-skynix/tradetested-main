<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 18/01/2016
 * Time: 16:07
 */
namespace AddressLookup;
require_once "bootstrap.php";
$controller = new IndexController();
if (isset($_GET['uri']) && $_GET['uri']) {
    $uri = $_GET['uri'];
} elseif (isset($_SERVER['DOCUMENT_URI'])) {
    $u = explode("/", $_SERVER['DOCUMENT_URI']);
    $uri = array_pop($u);
} else {
    $uri = '';
}
switch ($uri) {
    case 'fetch':
        return $controller->fetchAction();
    case 'suggest':
        return $controller->suggestAction();
    default:
        return header("HTTP/1.0 404 Not Found");
}
