<?php
namespace AddressLookup\Interactor;
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 18/01/2016
 * Time: 17:55
 */
class InteractorContext extends \AddressLookup\Model\AbstractModel
{

}

class AbstractInteractor
{
    public static function call($contextData)
    {
        $instance = new static($contextData);
        return $instance->process();
    }

    protected function __construct($contextData)
    {
        if (is_object($contextData)) {
            $this->_context = $contextData;
        } else {
            $this->_context = new InteractorContext();
            $this->_context->setData($contextData);
        }
    }

    public function process()
    {
        return $this->getContext();
    }

    public function getContext()
    {
        return $this->_context;
    }
}