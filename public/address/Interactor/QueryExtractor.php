<?php
namespace AddressLookup\Interactor;
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 18/01/2016
 * Time: 15:48
 */
class QueryExtractor extends AbstractInteractor
{
    protected $_inputLines;
    protected $_q = '';
    protected $_unit = '';
    protected $_deliveryLine = '';
    protected $_afterDeliveryLine = '';
    protected $_supplementaryData = null;
    protected $_filteredCustomerInput = '';

    /**
     * Call interactor instance
     *
     * Inputs:
     *
     * - input_text: The text that customer has entered into address fields.
     * - current_line_no: The street line number the customer is currently editing.
     *
     * Outputs:
     *
     * - courier_post_query: search query to send to courier post.
     *
     * @return mixed
     */
    public function process()
    {
        $this->_filteredCustomerInput = $this->getContext()->getInputText();
        $context = $this->getContext();
        $this->_extractPOBox(); //Do this first to remove PO Box number, which could interfere if text after #.
        $this->_extractRDNo();
        $this->_extractLevel();
        $this->_extractUnit();
        $this->_extractDeliveryLine();
        $this->_extractFilteredCustomerInput();
        $this->_q = trim(preg_replace("/[^A-Za-z0-9, ]/", '', $this->_q)); //CourierPost fails if there is slash in query (even escaped).
        $this->_q = $this->_q ? $this->_q : null;
        $context->setCourierPostQuery($this->_q)
            ->setSupplementaryData($this->_supplementaryData);
            ;
        return parent::process();
    }

    /**
     * Extract Level from Address.
     *
     * In case the level is not found in the database
     */
    protected function _extractLevel()
    {
        $input = $this->getContext()->getInputText();
        $matches = array();
        if (preg_match('/([^a-zA-Z0-9]|^)+(level|lv|lvl|lev|l)([^a-zA-Z0-9\n]*)(\d+)([^a-zA-Z0-9])+([^\d]*)(\d+)/', strtolower($input), $matches)) {
            $this->_addSupplementaryData('floor', "Level {$matches[4]}");
            //Remove last number from match, as that is likely the street number, which we have in match to ensure we don't match street number as level.
            $this->_filterInput(substr($matches[0], 0, strlen($matches[0])-strlen($matches[7])));
        } elseif (preg_match('/([^a-zA-Z0-9]|^)+(\d+)(st|nd|rd|th) (floor|level)([^a-zA-Z0-9]|$)+/', strtolower($input), $matches)) {
            $this->_addSupplementaryData('floor', "Level {$matches[2]}");
            $this->_filterInput($matches[0]);
        } elseif (preg_match('/([^a-zA-Z0-9]|^)+(floor|fl:f)(\s*)(\d+)([^a-zA-Z0-9])+([^\d]*)(\d+)/', strtolower($input), $matches)) {
            $this->_addSupplementaryData('floor', "Floor {$matches[4]}");
            //Remove last number from match, as that is likely the street number, which we have in match to ensure we don't match street number as level.
            $this->_filterInput(substr($matches[0], 0, strlen($matches[0])-strlen($matches[7])));
        }
    }

    /**
     * Extract Customer Input
     *
     * Which is everything before any address data, including level/unit
     *
     * When we match a level/unit, that match is usually removed from the input text,
     * as well as removing everything after that match from _filteredCustomerInput.
     */
    protected function _extractFilteredCustomerInput()
    {
        if ($this->_q) {
            $input = trim($this->_filteredCustomerInput, " \t\n\r\0\x0B,");
            $this->getContext()->setFilteredCustomerInput(str_replace("\n", ", ", $input));
        }
    }

    /**
     * Extract unit data from the address, and remove from input text.
     *
     * E.g. Unit D, 123 Test st, or 6/123 Test St.
     */
    protected function _extractUnit()
    {
        $input = $this->getContext()->getInputText();
        $matches = array();
        if (preg_match('/(unit|flat)([^a-zA-Z0-9\n]*)(\w+)([^a-zA-Z0-9\n]*)/', strtolower($input), $matches)) {
            $this->_filterInput($matches[0]);
            $unitType = ucfirst($matches[1]);
            $unitValue = strtoupper($matches[3]);
        } elseif(preg_match('/([^a-zA-Z0-9\n]*)(\w+)([^a-zA-Z0-9\n]*)\/([^a-zA-Z0-9\n]*)(\d+)(\D+)$/',$input, $matches)) {//e.g. 3/12 Co
            $this->_filterInput($matches[2].$matches[3].'/');
            if (preg_match('/^[u|f]\d/', strtolower($matches[2]))) {
                $unitValue = substr($matches[2], 1);
            } else {
                $unitValue = strtoupper($matches[2]);
            }
            $unitType = 'Unit';
        } else {
            return;
        }
        $this->_addSupplementaryData('unit_value', $unitValue);
        $this->_addSupplementaryData('unit_type', $unitType);
        $this->_unit .= "{$unitType} {$unitValue}";
    }

    /**
     * Remove any RD number from search query, as it will interfere with extracting delivery line.
     */
    protected function _extractRDNo()
    {
        $input = $this->getContext()->getInputText();
        $matches = array();
        if (preg_match('/([^a-zA-Z0-9\n]*)RD(\d)/', $input, $matches)) {
            $this->_filterInput($matches[0]);
        }
    }

    /**
     * Extract delivery Line
     *
     * - Grabs the last set of digits in currently edited line, and any text after that.
     * - If we already have a delivery line (from PO Box), we just use that data instead.
     */
    protected function _extractDeliveryLine()
    {
        $currentLine = $this->_getInputLine($this->getContext()->getCurrentLineNo());
        $matches = array();
        if ($this->_deliveryLine) {
            $this->_q = implode(' ', array_filter(array(
                $this->_deliveryLine,
                ucfirst(trim(str_replace("\n", '', $this->_afterDeliveryLine)))
            )));
        } elseif (preg_match('/^(.*?)(\d+)(\D+)$/', $currentLine, $matches)) {
            $this->_filterInput($matches[2].$matches[3]);
            $this->_q .= implode(', ', array_filter(array(
                $this->_unit,
                $matches[2].$matches[3]
            )));
        }
    }

    /**
     * Extract PO Box from Input Text
     *
     * Remove after extracting data, as it would interfere with extracting delivery line etc.
     */
    protected function _extractPOBox()
    {
        $input = $this->getContext()->getInputText();
        $matches = array();
        if (preg_match('/po box([^a-zA-Z0-9\n]*)(\d+)([^a-zA-Z0-9\n]*)(.*)/s', strtolower($input), $matches)) {
            $this->_filterInput($matches[0]);
            $this->_deliveryLine .= "PO Box {$matches[2]}";
            $this->_afterDeliveryLine .= $matches[4];
        }
    }

    protected function _filterInput($text)
    {
        $input = $this->getContext()->getInputText();
        $this->getContext()->setInputText(str_ireplace(str_replace("\n", "", $text), ' ', $input));
        $pos = strpos(strtolower($this->_filteredCustomerInput), rtrim(strtolower($text)));
        if ($pos !== false) {
            $this->_filteredCustomerInput = mb_substr($this->_filteredCustomerInput, 0, $pos, 'UTF-8');
        }
    }

    /**
     * Get a line from the text the customer has input.
     *
     * @param $lineNo
     * @return string
     */
    protected function _getInputLine($lineNo)
    {
        if (!$this->_inputLines) {
            $this->_inputLines = explode("\n", $this->getContext()->getInputText());
        }
        return isset($this->_inputLines[$lineNo-1]) ? $this->_inputLines[$lineNo-1] : '';
    }

    /**
     * Add Supplementary Data to array
     */
    protected function _addSupplementaryData($key, $value)
    {
        if (!($data = $this->_supplementaryData)) {
            $data = array();
        }
        $data[$key] = $value;
        $this->_supplementaryData = $data;
    }
}