<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 18/01/2016
 * Time: 16:21
 */
namespace AddressLookup;
class IndexController
{
    protected $_jwtToken;

    /**
     * Suggest addresses while typing
     */
    public function suggestAction()
    {
        header('Content-Type: application/json');
        Helper\Auth::apply();

        $q = $_GET['q'];
        $lineNo = $_GET['l'];
        $context = Interactor\QueryExtractor::call(array('input_text' => $q, 'current_line_no' => intval($lineNo)));

        $client = \CourierPost::getClient();
        $suggestions = array();
        if ($data = $client->suggest($context->getCourierPostQuery())) {
            foreach ($data as $_cpData) {
                $suggestions[] = Model\AddressSuggestion::fromCourierPostData($_cpData)
                    ->addSupplementaryData($context->getSupplementaryData())
                    ->checkForDirection()
                    ->setFilteredCustomerInput($context->getFilteredCustomerInput())
                    ->getDataWithExtendedLabel();
            }
        }

        echo json_encode($suggestions);
    }

    /**
     * Get Full details of selected address with existing input
     */
    public function fetchAction()
    {
        header('Content-Type: application/json');
        Helper\Auth::apply();

        $id = Helper\Auth::decode($_GET['i'])->aid;
        $client = \CourierPost::getClient();
        $data = $client->fetch($id);
        /** @var Model\Address $address */
        $address = Model\Address::fromCourierPostData($data)->setFilteredCustomerInput($_GET['fci']);

        if (isset($_GET['suppl'])) {
            $address->setSupplementaryData($_GET['suppl']);
        }

        echo json_encode($address->getMagentoData());
    }
}