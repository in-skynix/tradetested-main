<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 14/01/2016
 * Time: 16:46
 */
class CourierPost
{
    protected $_user;
    protected $_pass;
    protected $_baseUrl = "https://address.courierpost.co.nz/ECL.AddressLookup.Service/AddressLookup";

    protected function __construct($user, $pass)
    {
        $this->_user = $user;
        $this->_pass = $pass;
    }

    public static function getClient()
    {
        return new CourierPost("WS.Lookup", "P@ssw0rd");
    }

    public function suggest($searchTerm)
    {
        $url = sprintf(
            "/StreetLookup/%s/10",
            rawurlencode($searchTerm)
        );
        return $this->_request($url);
    }

    public function fetch($addressId)
    {
        return $this->_request("/AddressIdLookup/".$addressId);
    }

    protected function _request($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "{$this->_user}:{$this->_pass}");
        curl_setopt($curl, CURLOPT_URL, $this->_baseUrl.$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        return json_decode($result, true);
    }
}