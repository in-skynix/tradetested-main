<?php
namespace AddressLookup\Interactor;
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 15/01/2016
 * Time: 16:15
 */
class QueryExtractorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider queryExtractorProvider
     *
     * @param $inputData
     * @param $expectedQuery
     */
    public function testQueryExtractor($inputData, $expectedQuery, $filteredCustomerInput, $supplementaryData)
    {
        $context = QueryExtractor::call($inputData);
        $this->assertEquals($expectedQuery, $context->getCourierPostQuery());
        $this->assertEquals($filteredCustomerInput, $context->getFilteredCustomerInput());
        $this->assertEquals($supplementaryData, $context->getSupplementaryData());
    }

    public function queryExtractorProvider()
    {
        $string = file_get_contents("./test/fixtures/query_extractor_tests.json");
        $tests = json_decode($string, true);
        $data = array();
        foreach ($tests as $_test) {
            $filteredCustomerIn = isset($_test['filtered_customer_input']) ? $_test['filtered_customer_input'] : null;
            $supplementaryData = isset($_test['supplementary_data']) ? $_test['supplementary_data'] : null;
            $data[] = array($_test['in'], $_test['courier_post_query'], $filteredCustomerIn, $supplementaryData);
        }
        return $data;
    }
}