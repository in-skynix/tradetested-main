<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 15/01/2016
 * Time: 16:15
 */
namespace AddressLookup\Model;
class AddressTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider magentoDataFormatProvider
     *
     * @param $inputData
     * @param $expectedMagentoData
     */
    public function testMagentoDataFormat($inputData, $expectedMagentoData)
    {
        $address = new Address();
        $data = $address->setAllDataUsingMethod($inputData)->getMagentoData();
        unset($data['signature']);
        $this->assertEquals($expectedMagentoData, $data);
    }

    public function magentoDataFormatProvider()
    {
        $string = file_get_contents("./test/fixtures/address_reformat_tests.json");
        $tests = json_decode($string, true);
        $data = array();
        foreach ($tests as $_test) {
            $data[] = array($_test['in'], $_test['out']);
        }
        return $data;
    }
}