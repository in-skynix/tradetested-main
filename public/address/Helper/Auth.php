<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 14/01/2016
 * Time: 11:44
 */
namespace AddressLookup\Helper;
use \Firebase\JWT\JWT;
class Auth
{
    protected static function _getKey()
    {
        return 'qwerlkjsdflakj';
    }

    public static function apply()
    {
        if (!isset($_SERVER['HTTP_AUTHORIZATION'])) {
            http_response_code(401);
            die('{"error":"not_authorized"}');
        }

        try {

            list($jwt) = sscanf($_SERVER['HTTP_AUTHORIZATION'], 'JWT %s');
            $claims = self::decode($jwt);
        } catch (UnexpectedValueException $e) {
            http_response_code(401);
            die('{"error":"not_authorized"}');
        }

        if ($claims->user != "tradetested") {
            http_response_code(401);
            die('{"error":"not_authorized"}');
        }
    }

    public static function getToken()
    {
        return self::encode(array(
            "exp" => strtotime('+12 hours'),
            "user" => "tradetested"
        ));
    }

    public static function encode($data) {
        return JWT::encode($data, self::_getKey());
    }

    public static function decode($jwt)
    {
        return JWT::decode($jwt, self::_getKey(), array('HS256'));
    }
}