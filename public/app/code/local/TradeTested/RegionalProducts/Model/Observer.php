<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 19/12/14
 * Time: 14:23
 */
class TradeTested_RegionalProducts_Model_Observer
{
    public function catalogProductCollectionLoadBefore(Varien_Event_Observer $observer)
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = $observer->getEvent()->getCollection();
        if ($regionName = Mage::helper('tradetested_regions')->getAreaName()) {
            $attribute = Mage::getModel('catalog/resource_eav_attribute')->loadByCode('catalog_product', 'region');
            $optionId = $attribute->getSource()->getOptionId($regionName);
            $collection
                ->addAttributeToFilter(
                    array(
                        array('attribute'=>'region', 'null' => true),
                        array('attribute'=>'region', 'finset'=>$optionId),
                    ), null, 'left'
                );
        } else {
            $collection->addAttributeToFilter('region', array('null' => true), 'left');
        }
    }
}