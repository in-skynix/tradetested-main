<?php
$installer = $this;
$installer->startSetup();
$installer->addAttribute(
    'catalog_product',
    'region',
    array(
        'label' => 'Product Region',
        'group' => 'Region',
        'type' => 'varchar',
        'input' => 'multiselect',
        'backend' => 'eav/entity_attribute_backend_array',
        'frontend' => '',
        'source' => '',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible' => true,
        'required' => false,
        'user_defined' => false,
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'option' => array (
            'value' => array(
                'NTL' => array('Northland'),
                'AUK' => array('Auckland'),
                'WKO' => array('Waikato'),
                'BOP' => array('Bay of Plenty'),
                'GIS' => array('Gisborne'),
                'HKB' => array("Hawke's Bay"),
                'TKI' => array('Taranaki'),
                'MWT' => array('Manawatu'),
                'WGN' => array('Wellington'),
                'NSN' => array('Nelson'),
                'TAS' => array('Tasman'),
                'MBH' => array('Marlborough'),
                'WTC' => array('West Coast'),
                'CAN' => array('Canterbury'),
                'OTA' => array('Otago'),
                'STL' => array('Southland')
            )
        ),
        'visible_on_front' => true,
        'visible_in_advanced_search' => false,
        'used_in_product_listing' => true,
        'unique' => false )
);
$installer->endSetup();
