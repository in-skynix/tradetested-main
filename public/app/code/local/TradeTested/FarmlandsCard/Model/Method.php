<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 16/11/2015
 * Time: 11:40
 */
class TradeTested_FarmlandsCard_Model_Method extends Mage_Payment_Model_Method_Abstract
{
    /**
     * Payment method code
     *
     * @var string
     */
    protected $_code = "farmlands_card";

    /**
     * @var string
     */
    protected $_formBlockType = 'tradetested_farmlands_card/form';
    protected $_infoBlockType = 'tradetested_farmlands_card/info';

    /**
     * Assign data to info model instance
     *
     * @param   mixed $data
     * @return  Mage_Payment_Model_Info
     */
    public function assignData($data)
    {
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $FarmlandsCardNumber = $data->getFarmlandsCardNumber();
        $FarmlandsCardNumber = preg_replace('/[\-\s]+/', '', $FarmlandsCardNumber);
        $info = $this->getInfoInstance();
        $expiry = str_pad($data->getFarmlandsCardExpMonth(), 2, '0', STR_PAD_LEFT)
            . substr($data->getFarmlandsCardExpYear(), 2, 2);
        $info
            ->setFarmlandsCardOwner($data->getFarmlandsCardOwner())
            ->setFarmlandsCardNumber($FarmlandsCardNumber)
            ->setFarmlandsCardExpMonth($data->getFarmlandsCardExpMonth())
            ->setFarmlandsCardExpYear($data->getFarmlandsCardExpYear())
            ->setAdditionalInformation(array(
                'farmlands_card_number_enc' => Mage::helper('tradetested_farmlands_card')->encryptString(
                    $FarmlandsCardNumber,
                    str_pad($expiry, 16, '0', STR_PAD_LEFT)
                ),
                'farmlands_account_name'    => $info->getFarmlandsCardOwner(),
                'farmlands_card_expiry'     => $expiry,
            ));
        return $this;
    }

    /**
     * Prepare info instance for save
     *
     * @return $this
     */
    public function prepareSave()
    {
        $info = $this->getInfoInstance();
        $info->setFarmlandsCardNumber(null)
            ->setCcCid(null);
        return $this;
    }

    /**
     * Validate payment method information object
     *
     * @return  $this
     */
    public function validate()
    {
        parent::validate();

        $info = $this->getInfoInstance();

        $cardNumber = $info->getFarmlandsCardNumber();
        $cardNumber = preg_replace('/[\-\s]+/', '', $cardNumber);
        $info->setFarmlandsCardNumber($cardNumber);

        if (!$info->getFarmlandsCardOwner()) {
            Mage::throwException(Mage::helper('payment')->__('Cardholder Name is required'));
        }

        if (!$this->_validateFarmlandsCardNumber($cardNumber)) {
            Mage::throwException(Mage::helper('payment')->__('Invalid Card Details'));
        }

        if (!$this->_validateExpDate($info->getFarmlandsCardExpYear(), $info->getFarmlandsCardExpMonth())) {
            Mage::throwException(Mage::helper('payment')->__('Invalid Expiration Date.'));
        }

        $exp = substr($info->getFarmlandsCardExpYear(), 2).str_pad($info->getFarmlandsCardExpMonth(), 2, 0, STR_PAD_LEFT);
        if (!$this->_validateFarmlandsApi($cardNumber, $exp)) {
            Mage::throwException(Mage::helper('payment')->__('Invalid Card Details'));
        }

        return $this;
    }

    /**
     * Validate Farmlands Card details against TT Farmlands Service
     *
     * @param $cardNumber
     * @param $exp
     * @return bool
     */
    protected function _validateFarmlandsApi($cardNumber, $exp)
    {
        //Sanitise the input data a bit before using in URL
        $cardNumber = preg_replace("/[^0-9]/", "", $cardNumber);
        $exp = preg_replace("/[^0-9]/", "", $exp);

        /** @var TradeTested_Api_Model_RestClient $client */
        $client = Mage::getModel('tradetested_api/restClient')->addAuthGrant('verify_farmlands');
        return !!($client->get("farmlands/validate/{$cardNumber}/{$exp}")->getResponse());
    }

    /**
     * Validate Farmlands Card Number
     *
     * @param $cardNumber
     * @return bool
     */
    protected function _validateFarmlandsCardNumber($cardNumber)
    {
        return (
            (strlen($cardNumber) == 16)
            && ((strpos($cardNumber, '6014') === 0) || (strpos($cardNumber, '6005') === 0))
//            && ($this->_validateLuhn($cardNumber)) //Some Farmlands cards do not validate against Luhn Algorithm.
        );
    }

    /**
     * Validate Card Number against Luhn Algorithm
     *
     * @param $ccNumber
     * @return bool
     */
    protected function _validateLuhn($ccNumber)
    {
        $cardNumber = strrev($ccNumber);
        $numSum = 0;

        for ($i=0; $i<strlen($cardNumber); $i++) {
            $currentNum = substr($cardNumber, $i, 1);

            /**
             * Double every second digit
             */
            if ($i % 2 == 1) {
                $currentNum *= 2;
            }

            /**
             * Add digits of 2-digit numbers together
             */
            if ($currentNum > 9) {
                $firstNum = $currentNum % 10;
                $secondNum = ($currentNum - $firstNum) / 10;
                $currentNum = $firstNum + $secondNum;
            }

            $numSum += $currentNum;
        }

        /**
         * If the total has no remainder it's OK
         */
        return ($numSum % 10 == 0);
    }

    /**
     * @param $expYear
     * @param $expMonth
     * @return bool
     */
    protected function _validateExpDate($expYear, $expMonth)
    {
        $date = Mage::app()->getLocale()->date();
        if (!$expYear || !$expMonth || ($date->compareYear($expYear) == 1)
            || ($date->compareYear($expYear) == 0 && ($date->compareMonth($expMonth) == 1))
        ) {
            return false;
        }
        return true;
    }

    /**
     * Check whether there are CC types set in configuration
     *
     * @param Mage_Sales_Model_Quote|null $quote
     * @return bool
     */
    public function isAvailable($quote = null)
    {
        return Mage_Payment_Model_Method_Abstract::isAvailable($quote);
    }

}
