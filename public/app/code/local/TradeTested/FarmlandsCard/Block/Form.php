<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 16/11/2015
 * Time: 11:42
 */
class TradeTested_FarmlandsCard_Block_Form extends Mage_Payment_Block_Form_Cc
{
    /**
     * Block construction. Set block template.
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('tradetested/farmlands_card/form.phtml');
    }
}