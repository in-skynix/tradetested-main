<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 16/11/2015
 * Time: 11:43
 */
class TradeTested_FarmlandsCard_Block_Info extends Mage_Payment_Block_Info
{
    /**
     * Block construction. Set block template.
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('tradetested/farmlands_card/info.phtml');
    }
}