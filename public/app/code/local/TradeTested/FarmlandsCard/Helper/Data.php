<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 16/11/2015
 * Time: 11:50
 */
class TradeTested_FarmlandsCard_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Use own encryption key that can be used by OpenERP also.
     *
     * @param string $data
     * @param string $iv Initialization Vector
     * @return string
     */
    public function encryptString($data, $iv)
    {
        return openssl_encrypt($data, 'aes-256-cbc', $this->getEncryptionKey(), false, $iv);
    }

    public function getEncryptionKey()
    {
        return "OmpIWmINrqHB4JjFyunLKyZMU34gLmPa";
    }

}