<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 05/06/15
 * Time: 15:49
 */
class TradeTested_TagManager_Helper_Data extends Mage_Core_Helper_Data
{
    public function getContainerId()
    {
        return Mage::getStoreConfig('google/tag_manager/container_id');
    }

    public function getIdentifier()
    {
        return Mage::getStoreConfig('google/tag_manager/identifier');
    }
}