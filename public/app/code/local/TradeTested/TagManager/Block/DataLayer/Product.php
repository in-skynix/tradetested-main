<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 05/06/15
 * Time: 16:08
 */
class TradeTested_TagManager_Block_DataLayer_Product extends TradeTested_TagManager_Block_DataLayer_Abstract
{
    public function getDataLayer()
    {
        $product = Mage::registry('current_product');
        return parent::getDataLayer() + array(
            'sku'           => $product->getSku(),
            'pageType'     => 'product',
            'totalValue'   => floatval($product->getFinalPrice()),
            'ecommerce'     => array(
                'detail'        => array(
                    'products'      => array(array(
                        'name'          => $product->getName(),
                        'id'            => $product->getId(),
                        'price'         => floatval($product->getFinalPrice()),
                    ))
                )
            )
        );
    }
}