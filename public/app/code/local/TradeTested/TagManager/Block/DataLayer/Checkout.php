<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 05/06/15
 * Time: 16:08
 */
class TradeTested_TagManager_Block_DataLayer_Checkout extends TradeTested_TagManager_Block_DataLayer_Abstract
{
    public function getDataLayer()
    {
        $quote = Mage::getModel('checkout/cart')->getQuote();
        $skus = array();
        $items = array();
        foreach ($quote->getAllVisibleItems() as $_item) {
            $skus[] = $_item->getSku();
            $items[] = array(
                'sku'       => $this->jsQuoteEscape($_item->getSku()),
                'name'      => $this->jsQuoteEscape($_item->getName()),
                'price'     => floatval($_item->getBasePrice()),
                'quantity'  => floatval($_item->getQty())
            );
        }
        $step = Mage::app()->getRequest()->getActionName();
        if ($step == 'index') {
            $step = 'details';
        }
        return parent::getDataLayer() + array(
            'skus'          => $skus,
            'checkoutStep'  => $step,
            'pageType'      => 'checkout',
            'totalValue'    => floatval($quote->getSubtotal()),
            'transactionProducts' => $items
        );
    }
}