<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 05/06/15
 * Time: 15:47
 */
class TradeTested_TagManager_Block_DataLayer_Abstract extends Mage_Core_Block_Abstract
{

    public function getDataLayer()
    {
        return array(
            'md5email' => $this->_getMd5Email(),
            'layout'   => $this->getLayoutCode()
        );
    }

    public function getLayoutCode()
    {
        if (!$this->getData('layout_code')){
            $code = $this->getParentBlock()->getLayoutCode();
            $this->setData('layout_code', ($code ? $code : 'desktop'));
        }
        return $this->getData('layout_code');
    }

    protected function _toHtml()
    {
        if ($data = $this->_jsonData()) {
            return "dataLayer.push({$data});\n";
        }
    }

    protected function _jsonData()
    {
        if ($data = $this->getDataLayer()) {
            $data['appIdentifier'] = Mage::helper('tradetested_tag_manager')->getIdentifier();
            return json_encode($data);
        }
    }

    protected function _getMd5email()
    {
        $email = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
        if (!$email) {
            $email = base64_decode(Mage::app()->getCookie()->get('pxl_emh'));
        }
        if (!$email) {
            return '';
        }

        $val = trim(strtolower($email));
        $val = mb_convert_encoding($val, "UTF-8", "ISO-8859-1");
        return md5($val);
    }
}
