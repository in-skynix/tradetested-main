<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 05/06/15
 * Time: 16:08
 */
class TradeTested_TagManager_Block_DataLayer_SearchResult extends TradeTested_TagManager_Block_DataLayer_Abstract
{
    public function getDataLayer()
    {
        $skus = array();
        // Need to add attribute to select before it is loaded in toHtml()
        Mage::app()->getLayout()->getBlock('search_result_list')->getLoadedProductCollection()
            ->addAttributeToSelect('sku');
        // Need to run _beforeToHtml on this block in order to add sort orders etc. to toolbar.
        // Otherwise, the toolbar ends up never having the 'relevance' sort order available to it.
        Mage::app()->getLayout()->getBlock('search_result_list')->toHtml();
        $toolbar = Mage::app()->getLayout()->getBlock('product_list_toolbar');
        foreach ($toolbar->getCollection() as $_product) {
            $skus[] = $_product->getSku();
        }
        return parent::getDataLayer() + array(
            'pageType'     => 'searchresults',
            'searchQuery'  => $this->helper('catalogsearch')->getQueryText(),
            'resultCount'  => $this->getLayout()->getBlock('search.result')->getResultCount(),
            'skus'         => $skus
        );
    }
}