<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 09/07/15
 * Time: 16:39
 */
class TradeTested_TagManager_Block_DataLayer_Default extends TradeTested_TagManager_Block_DataLayer_Abstract
{
    public function getDataLayer()
    {
        return parent::getData() + array(
            'pageType'     => 'other',
        );
    }
}