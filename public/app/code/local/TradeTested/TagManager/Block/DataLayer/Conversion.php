<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 05/06/15
 * Time: 16:08
 */
class TradeTested_TagManager_Block_DataLayer_Conversion extends TradeTested_TagManager_Block_DataLayer_Abstract
{
    public function getDataLayer()
    {
        //@todo: If multishipping enabled, could have multiple order_ids, as per Ga.php
        $order = Mage::getModel('sales/order')->load(Mage::getSingleton('checkout/session')->getLastOrderId());
        $skus = array();
        $items = array();
        foreach ($order->getAllVisibleItems() as $_item) {
            $skus[] = $_item->getSku();
            $items[] = array(
                'sku'       => $this->jsQuoteEscape($_item->getSku()),
                'name'      => $this->jsQuoteEscape($_item->getName()),
                'price'     => floatval($_item->getBasePrice()),
                'quantity'  => floatval($_item->getQtyOrdered())
            );
        }
        return parent::getDataLayer() + array(
            'skus'                 => $skus,
            'pageType'            => 'purchase',
            'totalValue'          => floatval($order->getGrandTotal()),
            'storeName'           => Mage::app()->getStore()->getFrontendName(),
            // The below required for eCommerce Tracking. It requires the dataLayer variables to have specific names.
            'transactionId'       => $order->getIncrementId(),
            'transactionTotal'    => floatval($order->getGrandTotal()),
            'transactionTax'      => floatval($order->getBaseTaxAmount()),
            'transactionShipping' => floatval($order->getBaseShippingAmount()),
            'transactionProducts' => $items
        );
    }
}
