<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 09/07/15
 * Time: 16:39
 */
class TradeTested_TagManager_Block_DataLayer_Cms extends TradeTested_TagManager_Block_DataLayer_Abstract
{
    public function getDataLayer()
    {
        return parent::getDataLayer() + array(
            'pageType'     => $this->getIsHomePage() ? 'home' : 'cms_page',
            'pageIdentifier' => Mage::getSingleton('cms/page')->getIdentifier(),
        );
    }

    public function getIsHomePage()
    {
        if (!$this->getData('is_home_page')) {
            $result = !!($this->getUrl('') == $this->getUrl('*/*/*', array('_current'=>true, '_use_rewrite'=>true)));
            $this->setData('is_home_page', $result);
        }
        return $this->getData('is_home_page');
    }
}