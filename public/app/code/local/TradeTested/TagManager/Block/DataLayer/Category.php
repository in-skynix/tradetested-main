<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 05/06/15
 * Time: 16:08
 */
class TradeTested_TagManager_Block_DataLayer_Category extends TradeTested_TagManager_Block_DataLayer_Abstract
{
    public function getDataLayer()
    {
        $skus = array();
        $collection = Mage::getSingleton('catalog/layer')->getProductCollection()->addAttributeToSelect('sku');
        $toolbar = Mage::app()->getLayout()->getBlock('product_list_toolbar')->setCollection($collection);
        foreach ($toolbar->getCollection() as $_product) {
            $skus[] = $_product->getSku();
        }
        $category = Mage::registry('current_category');
        return parent::getDataLayer() + array(
            'pageType' => 'category',
            'categoryName' => $category->getName(),
            'skus'  => $skus
        );
    }
}