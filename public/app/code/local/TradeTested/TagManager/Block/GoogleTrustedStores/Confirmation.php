<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 13/11/2015
 * Time: 09:47
 */
class TradeTested_TagManager_Block_GoogleTrustedStores_Confirmation extends Mage_Core_Block_Template
{
    /**
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        if (!$this->getData('order')) {
            $order = Mage::getModel('sales/order')->load(Mage::getSingleton('checkout/session')->getLastOrderId());
            $this->setData('order', $order);
        }
        return $this->getData('order');
    }

    /**
     * @return int
     */
    public function getDispatchDate()
    {
        $backorderDate = $this->getBackorderDate();
        return $backorderDate ? strtotime(date('Y-m-d', $backorderDate).' +3 Weekdays') : strtotime('+3 Weekdays');
    }

    /**
     * @return int
     */
    public function getEstimatedDeliveryDate()
    {
        return $this->getDispatchDate() + 3600*24*7;
    }

    /**
     * Get date of last-arriving pre-order item in order, or false if none.
     *
     * @return bool|int
     */
    public function getBackorderDate()
    {
        $backorderDate = false;
        /** @var Mage_Sales_Model_Order_Item $_item */
        foreach ($this->getOrder()->getAllItems() as $_item) {
            if ($this->_itemHasBackorder($_item)) {
                $itemDate = $this->_extractBackorderDate($_item);
                $backorderDate = ($itemDate > $backorderDate) ? $itemDate : $backorderDate;
            }
        }
        return $backorderDate;
    }

    /**
     * Extract the pre-order date from a stock description string
     *
     * @param Mage_Sales_Model_Order_Item $item
     * @return int
     */
    protected function _extractBackorderDate(Mage_Sales_Model_Order_Item $item)
    {
        $matches = array();
        if (preg_match('/new stock arriving (\d+ \w+)/', $item->getProduct()->getStockDescription(), $matches)) {
            return strtotime($matches[1]);
        } else {
            return strtotime('+25 Days');
        }
    }

    /**
     * @param Mage_Sales_Model_Order_Item $item
     * @return bool
     */
    protected function _itemHasBackorder(Mage_Sales_Model_Order_Item $item)
    {
        $stockDescription = $item->getProduct()->getStockDescription();
        return ($stockDescription && ($stockDescription != 'In Stock for Delivery'));
    }

    /**
     * @return bool
     */
    public function getHasBackorder()
    {
        /** @var Mage_Sales_Model_Order_Item $_item */
        foreach ($this->getOrder()->getAllItems() as $_item) {
            if ($this->_itemHasBackorder($_item)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function getHasDigitalGoods()
    {
        /** @var Mage_Sales_Model_Order_Item $_item */
        foreach ($this->getOrder()->getAllItems() as $_item) {
            if ($_item->getProduct()->getIsVirtual()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return string
     */
    public function getGoogleShoppingId()
    {
        return Mage::getStoreConfig('google/tag_manager/google_shopping_id');
    }

    /**
     * @return float
     */
    public function getTaxAmount()
    {
        $total = $this->getOrder()->getGrandTotal();
        $percent = Mage::app()->getStore()->getCode() == 'default' ? 0.15 : 0.10;
        return $total - $total/(1+$percent);
    }

    public function getDomain()
    {
        return $_SERVER["HTTP_HOST"];
    }
}