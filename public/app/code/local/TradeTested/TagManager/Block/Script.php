<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 05/06/15
 * Time: 15:47
 */
class TradeTested_TagManager_Block_Script extends Mage_Core_Block_Template
{
    protected $_helper;

    public function getContainerId()
    {
        return $this->_getHelper()->getContainerId();
    }

    protected function _beforeToHtml()
    {
        if(empty($this->getSortedChildren())) {
            $block = $this->getLayout()
                ->createBlock('tradetested_tag_manager/dataLayer_default', 'google_tag_params_default');
            $this->setChild('tag_params', $block);
        }
        return parent::_beforeToHtml();
    }

    protected function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = Mage::helper('tradetested_tag_manager');
        }
        return $this->_helper;
    }
}