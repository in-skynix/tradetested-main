<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 10/02/2016
 * Time: 14:55
 */
class TradeTested_Newsletter_OptionsController extends Mage_Core_Controller_Front_Action
{
    /**
     * Newsletter options form
     */
    public function editAction()
    {
        if (!($subscriber = $this->_getSubscriber()) || !$subscriber->getEmailAddress()) {
            $this->_getSession()->addError('Could not find subscription');
            return $this->_redirect('/');
        }
        Mage::register('subscriber', $subscriber);
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Save newsletter options
     */
    public function editPostAction()
    {
        $subscriber = $this->_getSubscriber();

        $jwt = Mage::getModel('tradetested_api/jwt')->setEmh($subscriber->getId());
        $limiter = Mage::helper('tradetested_security/rateLimit_mailchimpUpdate');
        try {
            $limiter->checkLimit('mailchimp_update', array('ip_address' => Mage::helper('core/http')->getRemoteAddr()));
        } catch (TradeTested_Security_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            return $this->_redirect('*/*/edit', array('jwt' => $jwt->__toString()));
        }

        $data = $this->getRequest()->getPost();
        if ($data['unsubscribe'] ?? false) {
            $subscriber->setStatus('unsubscribed')->save();
        } elseif ($data['subscribe'] ?? false) {
            $subscriber->setStatus('subscribed')->save();
        } else {
            $suspendUntil = null;
            if ($data['suspend'] ?? false) {
                $time = $data['suspend_until'] == 4 ? strtotime('+4 weeks') : strtotime('+2 weeks');
                $suspendUntil = date('Y-m-d', $time);
            }
            $subscriber->setSuspendedUntil($suspendUntil)->setLimitFrequency($data['limit_frequency'] ?? null)->save();
        }

        $this->_getSession()->addSuccess('Your settings have been updated');
        $this->_redirect('*/*/edit', array('jwt' => $jwt->__toString()));
    }

    /**
     * Different layout depending on if customer is logged in / unsubscribed
     * @return $this
     */
    public function addActionLayoutHandles()
    {
        parent::addActionLayoutHandles();
        $subscriber = Mage::registry('subscriber');
        if ($subscriber->getStatus() != 'subscribed') {
            $this->getLayout()->getUpdate()->addHandle('newsletter_options_unsubscribed');
        }
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->getLayout()->getUpdate()->addHandle('newsletter_options_logged_in');
        }
        return $this;
    }

    /**
     * @return Mage_Core_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('core/session');
    }

    /**
     * @return TradeTested_Newsletter_Model_Subscriber
     */
    protected function _getSubscriber()
    {
        if ($jwtString = $this->getRequest()->getParam('jwt')) {
            $jwt = Mage::getModel('tradetested_api/jwt', $jwtString);
            return Mage::getModel('tradetested_newsletter/subscriber')->load($jwt->getEmh());
        } elseif ($mcid = $this->getRequest()->getParam('mcid')) {
            return Mage::getModel('tradetested_newsletter/subscriber')->load($mcid, 'mcid');
        } elseif ($customer = Mage::getSingleton('customer/session')->getCustomer()) {
            return Mage::getModel('tradetested_newsletter/subscriber')->loadByCustomer($customer);
        }
    }
}
