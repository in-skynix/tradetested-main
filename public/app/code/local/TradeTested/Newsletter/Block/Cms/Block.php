<?php
/**
 * Class TradeTested_Newsletter_Block_Cms_Block
 *
 * @method int getShowOnlyOnce()
 * @method $this setShowOnlyOnce(int $value)
 * @method int getHideIfEmailTagged()
 * @method $this setHideIfEmailTagged(int $value)
 */
class TradeTested_Newsletter_Block_Cms_Block extends  Mage_Cms_Block_Block
{
    /**
     * Prepare Content HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        $blockId = $this->getBlockId();
        $html = '';
        if ($blockId) {
            $block = Mage::getModel('cms/block')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($blockId);
            if ($block->getIsActive()) {
                $processor = Mage::getModel('tradetested_newsletter/template_filter');
                $html = '<div class="std">'.$processor->filter($block->getContent()).'</div>';
            }
        }
        if ($html && $this->_doNotShow()) {
            return '';
        } else {
            return $html;
        }
    }

    /**
     * Get cache key informative items
     * Provide string array key to share specific info item with FPC placeholder
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return array(
            $this->getBlockId(),
            $this->getNameInLayout(),
            Mage::app()->getStore()->getId(),
            $this->_doNotShow()
        );
    }

    protected function _doNotShow()
    {
        $cookie = Mage::getSingleton('core/cookie');
        if ($this->getShowOnlyOnce()) {
            if ($cookie->get('shown_'.$this->getNameInLayout())) {
                return true;
            } else {
                $cookie->set('shown_'.$this->getNameInLayout(), true, 3600 * 24 * 365 * 5);
            }
        }
        if ($this->getHideIfEmailTagged()) {
            return !!($cookie->get('pxl_emh'));
        }
    }
}