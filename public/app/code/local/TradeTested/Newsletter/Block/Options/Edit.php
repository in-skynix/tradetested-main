<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 10/02/2016
 * Time: 16:23
 */
class TradeTested_Newsletter_Block_Options_Edit extends Mage_Core_Block_Template
{
    /**
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('*/*/editPost');
    }

    /**
     * @return null|string
     */
    public function getSuspendedUntil()
    {
        $date = $this->getSubscriber()->getSuspendedUntil();
        return $date ? strftime('%e %b', strtotime($date)) : null;
    }

    /**
     * Get a JWT with the subscriber ID in it, to securely communicate to POST action.
     *
     * Prevent people subscribing/editing/viewing others just by creating md5 of email.
     *
     * @return mixed
     */
    public function getJwt()
    {
        return Mage::getModel('tradetested_api/jwt')->setEmh($this->getSubscriber()->getId());
    }

    /**
     * @return TradeTested_Newsletter_Model_Subscriber
     */
    public function getSubscriber()
    {
        if (!$this->getData('subscriber')) {
            $this->setData('subscriber', Mage::registry('subscriber'));
        }
        return $this->getData('subscriber');
    }
}