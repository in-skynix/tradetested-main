<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 10/02/2016
 * Time: 16:41
 */
class TradeTested_Newsletter_Helper_Mailchimp extends Mage_Core_Helper_Abstract
{
    /**
     * @return string
     */
    public function getListId()
    {
        return Mage::getStoreConfig('newsletter/mailchimp/list_id');
    }

    /**
     * Get Email From Mailchimp UNIQID
     *
     * Uses the legacy 2.0 API to get the email address corresponding to a mailchimp UNIQID
     *
     * The 3.0 API uses MD5 of the email address as ID, but there is no way to get this into a merge var.
     *
     * UNIQID is a merge var that can be used to create links without exposing the full email address.
     *
     * @param $mcid
     * @return mixed
     */
    public function getEmailFromMcid($mcid)
    {
        /** @var TradeTested_Newsletter_Model_MailchimpClient $client */
        $client = Mage::getModel('tradetested_newsletter/mailchimpClient', array('version' => '2.0'));
        $data = $client->post('lists/member-info.json', json_encode(array(
            'apikey' => Mage::helper('core')->decrypt(Mage::getStoreConfig('newsletter/mailchimp/api_key')),
            'id'    => $this->getListId(),
            'emails' => array(array('euid' => $mcid))
        )))->getResponse();
        return $data['data'][0]['email'] ?? null;
    }
}