<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 12/02/2016
 * Time: 09:50
 */
class TradeTested_Newsletter_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param Mage_Customer_Model_Customer $customer
     */
    public function subscribeCustomer(Mage_Customer_Model_Customer $customer)
    {
        Mage::getModel('tradetested_newsletter/subscriber')
            ->loadByCustomer($customer)
            ->setStatus('subscribed')
            ->save();
    }
}
