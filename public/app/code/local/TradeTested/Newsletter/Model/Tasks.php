<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 19/02/2016
 * Time: 17:21
 */
class TradeTested_Newsletter_Model_Tasks
{
    /**
     * Mailchimp cannot have complex conditions without nesting segments,
     * and cannot compare a date to today().
     *
     * So we update the date in the segment every night.
     *
     * This segment is for those subscribers who have not currently suspended their subscription.
     *
     * @throws Exception
     */
    public function updateNotSuspendedSegment()
    {
        /** @var Mage_Core_Model_Store $_store */
        foreach (Mage::app()->getStores() as $_store) {
            if ($segmentId = Mage::getStoreConfig('newsletter/mailchimp/not_suspended_segment_id', $_store)) {
                /** @var TradeTested_Newsletter_Model_Segment $segment */
                $segment = Mage::getModel('tradetested_newsletter/segment')
                    ->setStoreId($_store->getId())
                    ->load($segmentId);
                $options = $segment->getOptions();
                foreach ($options['conditions'] as $_k => $_condition) {
                    if (($_condition['field']  == 'SUSPEND_TO') && ($_condition['op'] == 'less')) {
                        $_condition['value'] = date('Y-m-d');
                        $options['conditions'][$_k] = $_condition;
                        break;
                    }
                }
                $segment->setOptions($options)->save();
            }
        }
    }

}