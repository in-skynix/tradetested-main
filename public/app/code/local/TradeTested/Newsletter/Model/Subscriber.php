<?php
require_once 'HumanName.php';
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 11/02/2016
 * Time: 18:25
 *
 * @method string getEmailAddress()
 * @method $this setEmailAddress(string $value)
 * @method string getUniqueEmailId()
 * @method $this setUniqueEmailId(string $value)
 * @method string getEmailType()
 * @method $this setEmailType(string $value)
 * @method string getStatus()
 * @method $this setStatus(string $value)
 * @method array getStats()
 * @method $this setStats(array $value)
 * @method string getIpSignup()
 * @method $this setIpSignup(string $value)
 * @method string getTimestampSignup()
 * @method $this setTimestampSignup(string $value)
 * @method string getIpOpt()
 * @method $this setIpOpt(string $value)
 * @method string getTimestampOpt()
 * @method $this setTimestampOpt(string $value)
 * @method string getMemberRating()
 * @method $this setMemberRating(string $value)
 * @method string getLastChanged()
 * @method $this setLastChanged(string $value)
 * @method string getLanguage()
 * @method $this setLanguage(string $value)
 * @method bool getVip()
 * @method $this setVip(bool $value)
 * @method string getEmailClient()
 * @method $this setEmailClient(string $value)
 * @method array getLocation()
 * @method $this setLocation(array $value)
 * @method string getListId()
 * @method $this setListId(string $value)
 * @method string getFirstname()
 * @method $this setFirstname(string $value)
 * @method string getLastname()
 * @method $this setLastname(string $value)
 * @method string setFullName()
 * @method $this getFullName(string $value)
 * @method string getSuspendedUntil()
 * @method $this setSuspendedUntil(string $value)
 * @method string getLimitFrequency()
 * @method $this setLimitFrequency(string $value)
 * @method string getSource()
 * @method $this setSource(string $value)
 * @method string getNameSource()
 * @method $this setNameSource(string $value)
 */
class TradeTested_Newsletter_Model_Subscriber extends TradeTested_Newsletter_Model_Abstract
{
    /**
     * Constructor, assign resource.
     */
    protected function _construct()
    {
        $this->_init('tradetested_newsletter/subscriber');
    }

    /**
     * Load By Customer
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return mixed
     */
    public function loadByCustomer(Mage_Customer_Model_Customer $customer)
    {
        return $this->load($customer->getEmail(), 'email')
            ->setMagentoCustomerId($customer->getId())
            ->setFullName($customer->getFirstname());
    }

    protected function _afterLoad()
    {
        if ($this->getSuspendedUntil() && (strtotime($this->getSuspendedUntil()) < time())) {
            $this->setSuspendedUntil(null);
        }
        return parent::_afterLoad();
    }

    /**
     * Ensure we don't subscribe someone without explicitly setting status and saving.
     */
    protected function _beforeSave()
    {
        if (!$this->getStatus()) {
            Mage::throwException('Subscriber requires a status');
        }
        if (!$this->getEmailAddress()) {
            Mage::throwException('Subscriber requires an email address');
        }
        if (!$this->getId()) {
            $this->setSource('Website')
                ->setId(md5(strtolower($this->getEmail())))
                ->setIpSignup(preg_replace('/[^(\d|\.)]/', '', Mage::helper('core/http')->getRemoteAddr(true)))
                ->setTimestampSignup(date('Y-m-d H:i:s'));
        }
        if (!$this->getLimitFrequency()) {
            //Filters are based on this field being blank, not falsey. It is a nullable Int field
            $this->setLimitFrequency(null);
        }
        if (!$this->getFirstname() && !$this->getLastname()) {
            $this->_extractNameParts();
        }
        return parent::_beforeSave();
    }

    /**
     * Turn full name into First and Last name
     *
     * @return $this
     */
    protected function _extractNameParts()
    {

        if (!($fullName = $this->getFullName())) {
            return $this;
        } elseif (strpos($fullName, ' ') === false) {
            return $this->setFirstname($fullName);
        }

        $name = new HumanName($this->getFullName());
        return $this->setNameSource('magento')
            ->setFirstname($name->getFirst())
            ->setLastname($name->getLast());
    }
}