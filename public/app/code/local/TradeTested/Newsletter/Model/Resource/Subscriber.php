<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 11/02/2016
 * Time: 18:57
 */
class TradeTested_Newsletter_Model_Resource_Subscriber extends TradeTested_Newsletter_Model_Resource_Abstract
{

    protected $_urlTemplate = 'lists/%s/members/%s';

    /**
     * Load an object
     *
     * @param Mage_Core_Model_Abstract $object
     * @param mixed $value
     * @param string $field field to load by (defaults to model id)
     * @return $this
     * @throws RestClientException
     */
    public function load(Mage_Core_Model_Abstract $object, $value, $field = null)
    {
        if ($field == 'email') {
            $email = $value;
            $value = md5(strtolower($email));
        } elseif ($field == 'mcid') {
            $email = Mage::helper('tradetested_newsletter/mailchimp')->getEmailFromMcid($value);
            $value = md5(strtolower($email));
        }

        parent::load($object, $value);

        if (isset($email)) {
            $object->setEmailAddress($email);
        }

        return $this;
    }

    /**
     * @param Mage_Core_Model_Abstract $object
     * @return Varien_Object
     */
    public function save(Mage_Core_Model_Abstract $object)
    {
        $response = $this->_getClient($object)->put(
            $this->_getResourceUrl($object),
            $this->_getMailchimpJson($object)
        );
        $data = $this->_extractDataFromMailchimp($response->getResponse());
        return $object->setData($data);
    }

    /**
     * @param array $mailchimpData
     * @return array
     */
    protected function _extractDataFromMailchimp($mailchimpData)
    {
        $data = parent::_extractDataFromMailchimp($mailchimpData);
        unset($data['merge_fields']);
        if (isset($mailchimpData['merge_fields'])) {
            foreach ($this->_getMergeVarMap() as $_mageKey => $_mcKey) {
                if (isset($mailchimpData['merge_fields'][$_mcKey])) {
                    $data[$_mageKey] = $mailchimpData['merge_fields'][$_mcKey];
                }
            }
        }
        return $data;
    }

    /**
     * @param Mage_Core_Model_Abstract $object
     * @return string
     */
    protected function _getMailchimpJson(Mage_Core_Model_Abstract $object)
    {
        $data = $object->getData();
        $data['merge_fields'] = array();
        foreach ($this->_getMergeVarMap() as $_mageKey => $_mcKey) {
            $val = $object->getData($_mageKey);
            $data['merge_fields'][$_mcKey] = $val ? $val : '';
            unset($data[$_mageKey]);
        }
        return json_encode($data);
    }

    /**
     * @return array
     */
    protected function _getMergeVarMap()
    {
        return array(
            'firstname'         => 'FNAME',
            'lastname'          => 'LNAME',
            'suspended_until'   => 'SUSPEND_TO',
            'source'            => 'SOURCE',
            'name_source'       => 'NSOURCE',
            'limit_frequency'   => 'LIMITFREQ',
        );
    }

}