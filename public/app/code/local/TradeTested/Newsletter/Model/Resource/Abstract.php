<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 23/02/2016
 * Time: 14:13
 */
abstract class TradeTested_Newsletter_Model_Resource_Abstract
{
    protected $_urlTemplate = 'lists/%s/abstracts/%s';
    /**
     * Load an object
     *
     * @param Mage_Core_Model_Abstract $object
     * @param mixed $value
     * @param string $field field to load by (defaults to model id)
     * @return $this
     * @throws RestClientException
     */
    public function load(Mage_Core_Model_Abstract $object, $value, $field = null)
    {
        $storeId = $object->getStoreId();
        try {
            $o = clone($object);
            $o->setId($value);
            $response = $this->_getClient($o)->get($this->_getResourceUrl($o));
            $data = $this->_extractDataFromMailchimp($response->getResponse());
        } catch (RestClientException $e) {
            if ($e->getCode() == '404') {
                $data = array();
            } else {
                throw $e;
            }
        }

        if ($data) {
            $object->setData($data)->setStoreId($storeId);
        }
        return $this;
    }

    /**
     * @param Mage_Core_Model_Abstract $object
     * @return Varien_Object
     * @todo: when object is new, create it via POST
     */
    public function save(Mage_Core_Model_Abstract $object)
    {
        $response = $this->_getClient($object)->patch(
            $this->_getResourceUrl($object),
            $this->_getMailchimpJson($object)
        );
        $data = $this->_extractDataFromMailchimp($response->getResponse());
        return $object->setData($data);
    }

    /**
     * @param Mage_Core_Model_Abstract $object
     */
    public function delete(Mage_Core_Model_Abstract $object)
    {
        $this->_getClient($object)->delete($this->_getResourceUrl($object));
    }

    /**
     * @return string
     */
    public function getIdFieldName()
    {
        return 'id';
    }

    /**
     * @param array $mailchimpData
     * @return array
     */
    protected function _extractDataFromMailchimp($mailchimpData)
    {
        $data = $mailchimpData;
        unset($data['_links']);
        return $data;
    }

    /**
     * @param Mage_Core_Model_Abstract $object
     * @return string
     */
    protected function _getMailchimpJson(Mage_Core_Model_Abstract $object)
    {
        return json_encode($object->getData());
    }

    /**
     * @param Varien_Object $object
     * @return string
     */
    protected function _getResourceUrl(Varien_Object $object)
    {
        return sprintf($this->_urlTemplate, $this->_getListId($object), $object->getId());
    }

    /**
     * @param Varien_Object $object
     * @return string
     */
    protected function _getListId(Varien_Object $object)
    {
        return $this->_getClient($object)->getListId();
    }

    /**
     * @param Varien_Object $object
     * @return TradeTested_Newsletter_Model_MailchimpClient
     */
    protected function _getClient(Varien_Object $object)
    {
        return Mage::getModel('tradetested_newsletter/mailchimpClient')->setStoreId($object->getStoreId());
    }
}