<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 23/02/2016
 * Time: 14:50
 */

/**
 * Class TradeTested_Newsletter_Model_Abstract
 * @method int getId()
 * @method $this setId(int $value)
 * @method int getStoreId()
 * @method $this setStoreId(int $value)
 */
abstract class TradeTested_Newsletter_Model_Abstract extends Mage_Core_Model_Abstract
{
    /**
     * Save without using resource commit/rollback
     *
     * @return $this|Mage_Core_Model_Abstract|TradeTested_Newsletter_Model_Subscriber
     * @throws Exception
     */
    public function save()
    {
        /**
         * Direct deleted items to delete method
         */
        if ($this->isDeleted()) {
            return $this->delete();
        }
        if (!$this->_hasModelChanged()) {
            return $this;
        }
        try {
            $this->_beforeSave();
            if ($this->_dataSaveAllowed) {
                $this->_getResource()->save($this);
                $this->_afterSave();
            }
            $this->_hasDataChanges = false;
        } catch (Exception $e) {
            $this->_hasDataChanges = true;
            throw $e;
        }
        return $this;
    }

    /**
     * Delete without using resource commit/rollback
     *
     * @return $this
     */
    public function delete()
    {
        $this->_beforeDelete();
        $this->_getResource()->delete($this);
        $this->_afterDelete();
        $this->_afterDeleteCommit();
        return $this;
    }
}