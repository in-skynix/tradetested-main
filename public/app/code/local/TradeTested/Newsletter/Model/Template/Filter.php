<?php
class TradeTested_Newsletter_Model_Template_Filter extends Mage_Cms_Model_Template_Filter
{
    protected $_templateVars = array();
    public function __construct()
    {
        $time = new DateTime();
        $time = ($time->format('n')%2==0) ? $time : $time->modify('last day of next month');
        $this->_templateVars = array(
            'draw_closes' => $time->format("t M Y"),
            'signup_form' => Mage::app()->getLayout()->createBlock('core/template', null, array(
                'template'      => 'tradetested/newsletter/newsletter_signup.phtml',
                'source'        => 'Website',
                'hide_label'    => true,
            ))->toHtml()
        );
        return parent::__construct();
    }
}