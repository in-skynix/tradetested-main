<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 19/02/2016
 * Time: 17:47
 */

/**
 * Class TradeTested_Newsletter_Model_Segment
 *
 * @method int getId()
 * @method $this setId(int $value)
 * @method string get()
 * @method $this set(string $value)
 * @method int getMemberCount()
 * @method string getType()
 * @method $this setType(string $value)
 * @method string getUpdatedAt()
 * @method $this setUpdatedAt(string $value)
 * @method string getCreatedAt()
 * @method $this setCreatedAt(string $value)
 * @method array getOptions()
 * @method $this setOptions(array $value)
 * @method string getListId()
 * @method $this setListId(string $value)
 */
class TradeTested_Newsletter_Model_Segment extends TradeTested_Newsletter_Model_Abstract
{
    /**
     * Constructor, assign resource.
     */
    protected function _construct()
    {
        $this->_init('tradetested_newsletter/segment');
    }

}
