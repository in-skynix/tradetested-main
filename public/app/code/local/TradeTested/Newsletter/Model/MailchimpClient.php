<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 11/02/2016
 * Time: 14:23
 */

/**
 * Class TradeTested_Newsletter_Model_MailchimpClient
 *
 * @method int getStoreId()
 * @method $this setStoreId(int $value)
 */
class TradeTested_Newsletter_Model_MailchimpClient extends TradeTested_Api_Model_RestClient
{
    protected $_listId;

    /**
     * TradeTested_Newsletter_Model_MailchimpClient constructor.
     * @param array $options
     */
    public function __construct($options=array())
    {
        $apiKey = Mage::helper('core')->decrypt(Mage::getStoreConfig('newsletter/mailchimp/api_key'));
        $version = isset($options['version']) ? $options['version'] : '3.0';
        list($_, $dataCenter) = explode("-", $apiKey);
        $defaultOptions = array(
            'headers' => array(),
            'parameters' => array(),
            'curl_options' => array(),
            'user_agent' => "Magento RestClient/0.1.0",
            'base_url' => "https://{$dataCenter}.api.mailchimp.com/{$version}/",
            'format'   => 'json',
            'timeout' => 10,
            'decoders' => array(
                'json' => array($this, '_jsonArrayDecode'),
            ),
            'username' => 'tradetested',
            'password' => $apiKey
        );

        $this->setOptions(array_merge($defaultOptions, $options));
    }

    /**
     * @return string
     */
    public function getListId()
    {
        if (!$this->_listId) {
            $this->_listId = Mage::getStoreConfig('newsletter/mailchimp/list_id', $this->getStoreId());
        }
        return $this->_listId;
    }
}