<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 12/02/2016
 * Time: 11:31
 */
class TradeTested_Newsletter_Model_Observer
{
    /**
     * After saving customer, update mailchimp if email changed.
     *
     * @param Varien_Event_Observer $observer
     */
    public function customerSaveCommitAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $observer->getEvent()->getCustomer();
        if($customer->dataHasChangedFor('email')) {
            $subscriber = Mage::getModel('tradetested_newsletter/subscriber')
                ->load($customer->getOrigData('email'), 'email');
            if ($subscriber->getStatus() && $subscriber->getId()) {
                $subscriber->delete()->setId(null)->setEmailAddress($customer->getEmail())->save();
            }
        }
    }
}