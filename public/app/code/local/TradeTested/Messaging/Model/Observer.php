<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 10/03/2016
 * Time: 13:37
 */
class TradeTested_Messaging_Model_Observer
{
    public function reviewSaveAfter(Varien_Event_Observer $observer)
    {
        /** @var TradeTested_Reviews_Model_Rewrite_Review $review */
        $review = $observer->getEvent()->getObject();
        if ($review->getStoreId() == 2) {
            $appEmulation = Mage::getSingleton('core/app_emulation');
            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($review->getStoreId());
            $data = Mage::helper('tradetested_messaging/extractor_review')->extractMessageData($review);
            if (isset($data['action'])) {
                $event = Mage::getModel('messenger/event')
                    ->setName('review_update')
                    ->setData($data);
                Mage::getSingleton('messenger/di')
                    ->newInstance('messenger/event_dispatcher')
                    ->dispatch($event);
            }
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        }
    }
    
    public function catalogProductSaveAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getEvent()->getProduct();
        if (
            $product->getReturnToBus()
            || $product->dataHasChangedFor('url_key')
            || $product->getIsChangedCategories()
            || $product->getIsChangedWebsites()
            || $product->dataHasChangedFor('media_gallery')
            || $product->dataHasChangedFor('image')
        ) {
            if ($data = Mage::helper('tradetested_messaging/extractor_product')->extractMessageData($product)) {
                Mage::helper('tradetested_messaging')->sendMessage('product_update', $data);
            }
        }
    }

    public function salesOrderSaveAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();
        //Don't send twice when first placing order, and only send if changing status.
        if ($order->getStatus() && $order->dataHasChangedFor('status')) {
            $data = Mage::helper('tradetested_messaging/extractor_order')
                ->extractMessageData($order);
            Mage::helper('tradetested_messaging')->sendMessage('order_created', $data);
        }
    }

    public function messengerDiInitialized(Varien_Event_Observer $observer)
    {
        /** @var Zend\Di\Di $di */
        $di = $observer->getEvent()->getDi();
        $di->instanceManager()
            ->removeTypePreference(
                'Oggetto_Messenger_Model_Message_Interface',
                'Oggetto_Messenger_Model_Message_Xml'
            )
            ->addTypePreference(
                'Oggetto_Messenger_Model_Message_Interface',
                'TradeTested_Messaging_Model_Message_Json'
            )
            ->removeTypePreference(
                'Oggetto_Messenger_Model_Transport_Interface',
                'Oggetto_Messenger_Model_Transport_Rabbitmq'
            )
            ->addTypePreference(
                'Oggetto_Messenger_Model_Transport_Interface',
                'TradeTested_Messaging_Model_Transport_Rabbitmq'
            )
        ;
    }

    public function tradetestedPxpostValidateFailed(Varien_Event_Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $orderId = $order ? get_class($order).$order->getId() : null;
        $response = $observer->getEvent()->getResponse();
        $validationResult = [
            'is_success'     => $response->getIsSuccess(),
            'dps_ref'        => !!$response->getDpsTxnRef(),
            'not_status_req' => !$response->getStatusRequired(),
            'order_exists'   => !!($order),
            'amounts'        => (
                !in_array($response->getTxnType(), ['Auth', 'Purchase'])
                || (abs($response->getAmount() - sprintf("%9.2f", $order->getBaseGrandTotal())) < 0.005)
            ),
            'currency'       => (
                !$order->getBaseCurrencyCode()
                || !in_array($response->getTxnType(), ['Auth', 'Purchase'])
                || ($response->getCurrencyName() === $order->getBaseCurrencyCode())
            ),
            'cvc'            => ($response->getIsCvcValid())
        ];
        
        $data = [
            'message'     => 'DPS Returned success, but magento failed validation of payment',
            'description' => 'Check the payment in DPS, and look at refunding the amount if it does not match an order',
            'response'    => $response->getData(),
            'order'       => $orderId,
            'validation_details' => $validationResult
        ];
        Mage::helper('tradetested_messaging')->sendMessage('admin_exception', $data);
    }
}