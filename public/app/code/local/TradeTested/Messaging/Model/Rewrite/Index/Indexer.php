<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 16/02/2016
 * Time: 14:14
 */
class TradeTested_Messaging_Model_Rewrite_Index_Indexer extends Mage_Index_Model_Indexer
{
    /**
     * Create new event log and register event in all processes
     *
     * @param   Varien_Object $entity
     * @param   string $entityType
     * @param   string $eventType
     * @param   bool $doSave
     * @return  Mage_Index_Model_Event
     */
    public function logEvent(Varien_Object $entity, $entityType, $eventType, $doSave=true)
    {
        $event = Mage::getModel('index/event')
            ->setEntity($entityType)
            ->setType($eventType)
            ->setDataObject($entity)
            ->setEntityPk($entity->getId());

        $this->registerEvent($event);
        $event->setDataObject(null);

        $messengerEvent = Mage::getModel('messenger/event')
            ->setName('index_event')
            ->setData($event->getData());
        Mage::getSingleton('messenger/di')
            ->newInstance('messenger/event_dispatcher')
            ->dispatch($messengerEvent);

        return $event;
    }

    /**
     * Create new event log and register event in all processes.
     * Initiate events indexing procedure.
     *
     * @param   Varien_Object $entity
     * @param   string $entityType
     * @param   string $eventType
     * @throws Exception
     * @return  Mage_Index_Model_Indexer
     */
    public function processEntityAction(Varien_Object $entity, $entityType, $eventType)
    {
        $this->logEvent($entity, $entityType, $eventType, false);
        return $this;
    }

    public function processEventMessage(array $eventData)
    {
        /** @var Mage_Index_Model_Event $event */
        $event = Mage::getModel('index/event')->setData($eventData);
        $name = $this->_getEventTypeName($event->getEntity(), $event->getType());

        Mage::dispatchEvent('start_process_event'.$name);

        /** @var $resourceModel Mage_Index_Model_Resource_Process */
        $resourceModel = Mage::getResourceSingleton('index/process');

        $allowTableChanges = $this->_allowTableChanges && !$resourceModel->isInTransaction();
        if ($allowTableChanges) {
            $this->_currentEvent = $event;
            $this->_changeKeyStatus(false);
        }

        $resourceModel->beginTransaction();
        $this->_allowTableChanges = false;
        try {
            $this->indexEvent($event);
            $resourceModel->commit();
        } catch (Exception $e) {
            $resourceModel->rollBack();
            if ($allowTableChanges) {
                $this->_allowTableChanges = true;
                $this->_changeKeyStatus(true);
                $this->_currentEvent = null;
            }
            throw $e;
        }
        if ($allowTableChanges) {
            $this->_allowTableChanges = true;
            $this->_changeKeyStatus(true);
            $this->_currentEvent = null;
        }
        Mage::dispatchEvent('end_process_event'.$name);

        return $this;
    }

}
