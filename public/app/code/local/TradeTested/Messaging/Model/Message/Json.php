<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 08/03/2016
 * Time: 15:35
 */
class TradeTested_Messaging_Model_Message_Json implements Oggetto_Messenger_Model_Message_Interface
{
    /**
     * Meta info
     *
     * @var array
     */
    protected $_meta;

    /**
     * Message data
     *
     * @var array
     */
    protected $_data;

    /**
     * Init message from string
     *
     * @param string $string String
     * @throws InvalidArgumentException
     * @return Oggetto_Messenger_Model_Message_Interface
     */
    public function init($string)
    {
        $data = json_decode($string, true);
        $this->_meta = $data['_meta'] ?? [];
        unset($data['_meta']);
        $this->_data = $data;
        return $this;
    }

    public function toString()
    {
        return json_encode(array_merge($this->_data, ['_meta' => $this->_meta]), JSON_PRETTY_PRINT);
    }

    /**
     * Get message meta information
     *
     * @return array
     */
    public function getMeta()
    {
        return (array) $this->_meta;
    }

    /**
     * Get message contents
     *
     * @return array
     */
    public function getData()
    {
        return (array) $this->_data;
    }

    /**
     * Set message meta information
     *
     * @param array $meta Meta info
     * @return Oggetto_Messenger_Model_Message_Xml
     */
    public function setMeta($meta)
    {
        $this->_meta = $meta;
        return $this;
    }

    /**
     * Set message contents
     *
     * @param array $data Message contents
     * @return Oggetto_Messenger_Model_Message_Xml
     */
    public function setData($data)
    {
        $this->_data = $data;
        return $this;
    }

    /**
     * Test if message matches criterion
     *
     * @param Varien_Object $criterion Matching criterion
     * @return boolean
     */
    public function matchesCriterion(Varien_Object $criterion)
    {
        $meta = $this->getMeta();
        $name = $meta['name'] ?? null;

        return $criterion->getName() && $criterion->getName() == $name;
    }
}