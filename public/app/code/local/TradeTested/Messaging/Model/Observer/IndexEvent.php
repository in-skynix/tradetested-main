<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 18/02/2016
 * Time: 10:21
 */
class TradeTested_Messaging_Model_Observer_IndexEvent implements Oggetto_Messenger_Model_Event_Observer_Interface,
    Oggetto_Messenger_Model_Log_Loggable
{
    /**
     * Logger
     *
     * @var Zend_Log
     */
    private $_logger;

    /**
     * Set logger
     *
     * @param Zend_Log $logger Logger
     * @return mixed
     */
    public function setLogger(Zend_Log $logger)
    {
        $this->_logger = $logger;
        return $this;
    }

    /**
     * Check if event could be observed by this class
     *
     * @param Oggetto_Messenger_Model_Event $event Event
     * @return boolean
     */
    public function match(Oggetto_Messenger_Model_Event $event)
    {
        return $event->getName() == 'index_event';
    }

    /**
     * Observe event
     *
     * @param Oggetto_Messenger_Model_Event $event Event
     * @return void
     */
    public function observe(Oggetto_Messenger_Model_Event $event)
    {
        try {
            Mage::getSingleton('index/indexer')->processEventMessage($event->getData());
        } catch (Throwable $e) {
            Mage::helper('tradetested_messaging')->sendMessageToErrorQueue($event, $e);
        }
    }
}
