<?php
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 11/05/16
 * Time: 5:17 PM
 */
class TradeTested_Messaging_Model_Transport_Rabbitmq extends Oggetto_Messenger_Model_Transport_Rabbitmq
{

    /**
     * Receive message
     *
     * Customised to add routing key to meta in message
     *
     * @param AMQPMessage                               $rabbitMessage Rabbit message
     * @param Oggetto_Messenger_Model_Message_Interface $messagePrototype Message prototype
     * @param array|Closure                             $callback Callback
     *
     * @throws Exception
     * @throws Oggetto_Messenger_Exception_Critical
     *
     * @return void
     */
    public function receiveMessage(
        AMQPMessage $rabbitMessage, Oggetto_Messenger_Model_Message_Interface $messagePrototype, $callback
    ) {
        try {
            $this->_logger->info("Received new message: {$rabbitMessage->body}");

            $message = clone $messagePrototype;
            $message->init($rabbitMessage->body);
            $message->setMeta(
                array_merge(
                //set to name field, as a lot of oggetto_messenger assumes the name field is the place to look
                    ['name' => $rabbitMessage->{'delivery_info'}['routing_key']], $message->getMeta()
                )
            );
            call_user_func_array($callback, [$message]);
        } catch (Oggetto_Messenger_Exception_Critical $e) {
            // Only critical exceptions are supposed to stop the receiver
            throw $e;
        } catch (Exception $e) {
            $this->_logger->err($e);
        }

        $channel = $rabbitMessage->{'delivery_info'}['channel'];
        $channel->basic_ack($rabbitMessage->{'delivery_info'}['delivery_tag']);
    }

    /**
     * Send message
     *
     * Only because parent class uses private methods
     *
     * @param Oggetto_Messenger_Model_Message_Interface $message Message
     *
     * @return void
     */
    public function send(Oggetto_Messenger_Model_Message_Interface $message)
    {
        if (!$this->_publishRouter) {
            $this->_logger->warn('Publish router is not defined: message cannot be sent');

            return;
        }
        if ($queue = $this->_publishRouter->findMessageQueue($message)) {
            $queue = (string)Mage::getConfig()->getNode('global/messaging/queue_prefix').$queue;
            $this->_declareQueue($queue);
            $this->_sendMessage($message, $queue);
        } else {
            $this->_logger->warn('Destination queue not found for message:');
            $this->_logger->warn(print_r($message->getMeta(), true));
        }
    }

    /**
     * Declare RabbitMQ queue
     *
     * Only because parent class uses private methods
     *
     * @param string $queue Queue name
     *
     * @return void
     */
    private function _declareQueue($queue)
    {
        $transport = new Varien_Object;
        Mage::dispatchEvent('rabbitmq_queue_declare_before', ['queue' => $queue, 'transport' => $transport]);
        $this->_getChannel()->queue_declare($queue, false, true, false, false);
        Mage::dispatchEvent('rabbitmq_queue_declare_after', ['queue' => $queue, 'transport' => $transport]);
    }

    /**
     * Send message to queue
     *
     * Set content_type on message;
     *
     * @param Oggetto_Messenger_Model_Message_Interface $message Message
     * @param string                                    $queue Queue
     *
     * @return void
     */
    private function _sendMessage(Oggetto_Messenger_Model_Message_Interface $message, $queue)
    {
        $rabbitMessage = new AMQPMessage(
            $message->toString(), [
            'delivery_mode' => 2, // Make message persistent
            'content_type'  => 'application/json',
        ]
        );
        $this->_logger->info("Sending message to queue '{$queue}': {$rabbitMessage->body}");

        $transport = new Varien_Object;
        Mage::dispatchEvent('rabbitmq_publish_before', ['message' => $rabbitMessage, 'transport' => $transport]);
        $this->_getChannel()->basic_publish($rabbitMessage, '', $queue);
        Mage::dispatchEvent('rabbitmq_publish_after', ['message' => $rabbitMessage, 'transport' => $transport]);
    }

}