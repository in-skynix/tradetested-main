<?php
//http://stackoverflow.com/a/7168916/829039
//http://mysqlserverteam.com/storing-uuid-values-in-mysql-tables/
$installer = $this;
$installer->startSetup();
$sql = <<<SQL
create function uuid_v4() returns VARCHAR(32)
  begin
    set @h1 = lpad(hex(floor(rand() * 4294967296)), 8, '0');
    set @h2 = lpad(hex(floor(rand() * 4294967296)), 8, '0');
    set @h3 = lpad(hex(floor(rand() * 4294967296)), 8, '0');
    set @h4 = lpad(hex(floor(rand() * 4294967296)), 8, '0');

    set @uuid = concat(
        @h1,
        substr(@h2 from 1 for 4),
        '4',
        substr(@h2 from 6),
        substr('ab89' from floor(1 + rand() * 4) for 1 ),
        substr(@h3 from 2),
        @h4
    );
    return (@uuid);
  end;
SQL;
//$installer->run($sql);

$installer->run("ALTER TABLE {$installer->getTable('sales/order')} ADD guid_bin CHAR(16) CHARACTER SET BINARY NULL;");
$installer->run("CREATE UNIQUE INDEX sales_order_guid ON {$installer->getTable('sales/order')} (guid_bin);");
$installer->run("ALTER TABLE {$installer->getTable('customer/entity')} ADD guid_bin CHAR(16) CHARACTER SET BINARY NULL;");
$installer->run("CREATE UNIQUE INDEX customer_entity_guid ON {$installer->getTable('customer/entity')} (guid_bin);");
$installer->run("UPDATE {$installer->getTable('sales/order')} SET guid_bin = guid_to_binary(uuid_v4())");
$installer->run("UPDATE {$installer->getTable('customer/entity')} SET guid_bin = guid_to_binary(uuid_v4())");
$installer->endSetup();
