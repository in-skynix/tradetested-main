<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 26/05/16
 * Time: 3:16 PM
 */
class TradeTested_Messaging_Helper_Importer_Product extends Mage_Catalog_Model_Api2_Product_Rest_Admin_V1
{
    public function importProduct(array $data)
    {
        $product = Mage::helper('tradetested_core')->loadProductByGuid($data['guid']);
        if (!$product->getId()) {
            $this->_prepareNewProduct($product);
            $data['attribute_set_id'] = 4;
            $data['type_id'] = 'simple';
        }
        if ($data['stock_data'] ?? false) {
            $data['stock_data']['qty'] = $data['stock_data']['quantity'] ?? 0;
            $data['stock_data']['is_in_stock'] = !!($data['stock_data']['is_in_stock']) ? 1 : 0;
            $data['stock_data']['use_config_manage_stock'] = true;
        }

        /* @var $validator Mage_Catalog_Model_Api2_Product_Validator_Product */
        $validator = Mage::getModel(
            'catalog/api2_product_validator_product', [
            'operation' => self::OPERATION_UPDATE,
            'product'   => $product,
        ]
        );

        if (!$validator->isValidData($data)) {
            throw new Exception("Product is not valid: ".implode(", ", $validator->getErrors()));
        }
        if (isset($data['sku'])) {
            $product->setSku($data['sku']);
        }
        $this->_prepareDataForSave($product, $data);
        $product->validate();

        //If Odoo's Magento URL is not up-to-date, return to bus
        if (isset($data['magento_url_key'])) {
            $urls = Mage::helper('tradetested_messaging/extractor_product')->getUrls($product);
            if (!in_array($data['magento_url_key'], $urls)) {
                $product->setReturnToBus(true);
            }
        }

        $product->save();
    }
    
    public function getActionType()
    {
        return self::ACTION_TYPE_ENTITY; //So import works properly, by loading stock data;
    }
    
    protected function _prepareNewProduct(Mage_Catalog_Model_Product $product)
    {
        $product->setStoreId(Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID)
            ->setAttributeSetId(4)
            ->setTypeId('simple')
            ->setVisibility(4)
            ->setTaxClassId(0);
        foreach ($product->getMediaAttributes() as $mediaAttribute) {
            $mediaAttrCode = $mediaAttribute->getAttributeCode();
            $product->setData($mediaAttrCode, 'no_selection');
        }
    }
}