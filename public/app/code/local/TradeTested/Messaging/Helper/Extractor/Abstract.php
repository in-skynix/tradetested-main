<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 10/03/2016
 * Time: 13:51
 */
class TradeTested_Messaging_Helper_Extractor_Abstract extends Mage_Core_Helper_Abstract
{
    protected function _extractData(Varien_Object $object, array $fields = [])
    {
        $data = [];
        foreach ($fields as $_key) {
            if (is_array($_key)) {
                list($_key, $_newKey) = $_key;
            } else {
                $_newKey = $_key;
            }
            $data[$_newKey] = $object->getDataUsingMethod($_key);
        }
        return $data;
    }
}