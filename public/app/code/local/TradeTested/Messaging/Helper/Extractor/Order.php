<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 08/03/2016
 * Time: 09:36
 */
class TradeTested_Messaging_Helper_Extractor_Order extends TradeTested_Messaging_Helper_Extractor_Abstract
{
    /**
     * @param Mage_Sales_Model_Order $order
     *
     * @return array
     */
    public function extractMessageData(Mage_Sales_Model_Order $order)
    {
        Mage::helper('tradetested_core')->convertBinToGuid($order);
        $addressFields = [
            'company', ['firstname', 'name'], 'email', 'country_id', 'street', 'telephone', 'city', 'postcode',
        ];

        $orderData = $this->_extractData($order, [
            'guid', 'created_at', 'updated_at', 'discount_description', 'base_discount_amount',
            'base_grand_total', 'shipping_description', 'customer_note', 'signature_opt_out', 'fraud_score',
            'base_shipping_amount', 'status', 'order_comments', 'customer_id', 'base_currency_code',
            'customer_email', ['increment_id', 'customer_reference'],
        ]);
        $orderData['signature_opt_out'] = !!($orderData['signature_opt_out'] ?? false);
        $orderData['store_code'] = ($order->getStore()->getCode() == 'default') ? 'NZ' : 'AU';
        $orderData['payments'] = [];
        /** @var Mage_Sales_Model_Order_Payment $_payment */
        foreach ($order->getAllPayments() as $_payment) {
            $info = $this->_extractData($_payment, [
                'id',
                'method',
                'additional_information',
                'cc_last4',
                'cc_exp_month',
                'cc_exp_year',
                'base_amount_ordered',
                'base_amount_paid',
                'base_amount_refunded',
            ]
            );
            $info['transaction_id'] = $_payment->getCcTransId() ?? $_payment->getLastTransId();
            foreach (['date_settlement', 'response_text', 'account'] as $_key) {
                $info['additional_information'][$_key] =
                    Mage::helper('tradetested_payment_express')->getAdditionalData($_payment, $_key);
            }
            if ($info['additional_information']['date_settlement']) {
                $info['additional_information']['date_settlement'] =
                    date("c", strtotime($info['additional_information']['date_settlement']));
            }
            $info['additional_information'] = (object)$info['additional_information'];
            $orderData['payments'][] = $info;
        }
        $orderData['addresses'] = [
            'billing'  => $this->_extractData($order->getBillingAddress(), $addressFields),
            'shipping' => $this->_extractData($order->getShippingAddress(), $addressFields),
        ];
        $orderData['addresses']['billing']['region'] = Mage::getModel('directory/region')
            ->load($order->getBillingAddress()->getRegionId())->getCode();
        $orderData['addresses']['shipping']['region'] = Mage::getModel('directory/region')
            ->load($order->getBillingAddress()->getRegionId())->getCode();
        $orderData['visitor'] = [
            'ip_address'      => $order->getRemoteIp(),
            'user_agent'      => Mage::helper('core/http')->getHttpUserAgent(),
            'accept_language' => $_SERVER['HTTP_ACCEPT_LANGUAGE'] ?? '',
        ];
        $orderData['order_items'] = [];
        /** @var Mage_Sales_Model_Order_Item $_item */
        foreach ($order->getAllVisibleItems() as $_item) {
            if ($_item->getHasChildren()) {
                $childItems = $_item->getChildrenItems();
                /** @var Mage_Sales_Model_Order_Item $_childItem */
                foreach ($childItems as $_childItem) {
                    $orderData['order_items'][] = array_merge(
                        $this->_getItemData($_childItem),
                        ['base_price' => $_item->getBasePrice() / count($childItems)]
                    );
                }
            } else {
                $orderData['order_items'][] = $this->_getItemData($_item);
            }
        }
        foreach(['updated_at', 'created_at'] as $_key) { //Always use ISO8601
            $orderData[$_key] = date("c", strtotime($orderData[$_key]));
        }
        $orderData['source'] = 'magento';
        return $orderData;
    }

    /**
     * @param Mage_Sales_Model_Order_Item $item
     *
     * @return array
     */
    protected function _getItemData(Mage_Sales_Model_Order_Item $item) 
    {
        $itemData = $this->_extractData($item, ['base_price', ['qty_ordered', 'quantity_ordered'], 'name']);
        $product = Mage::getModel('catalog/product')->load($item->getProductId());
        $itemData['product_guid'] = $product->getGuid();
        return $itemData;
    }
    
}