<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 27/05/16
 * Time: 12:42 PM
 */
class TradeTested_Messaging_Helper_Extractor_Product extends TradeTested_Messaging_Helper_Extractor_Abstract
{
    public function extractMessageData(Mage_Catalog_Model_Product $product)
    {
        if (!count($product->getStoreIds())) {
            return false;
        }

        $images = [];
        foreach ($product->getMediaGalleryImages() as $_image) {
            $images[] = $_image->getFile();
        }
        // Return empty hash rather than empty array in JSON.
        $urls = $this->getUrls($product);
        if (empty($urls)) {
            $urls = new stdClass();
        }
        return ['guid' => $product->getGuid(), 'store_urls' => $urls, 'images' => $images, 'image' => $product->getImage()];

    }

    public function getUrls(Mage_Catalog_Model_Product $product)
    {
        $urls = [];
        $origStoreId = $product->getStoreId();
        foreach ($product->getStoreIds() as $_storeId) {
            $appEmulation           = Mage::getSingleton('core/app_emulation');
            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($_storeId);
            $store                  = Mage::app()->getStore($_storeId);
            $product->setStoreId($_storeId);
            Mage::helper('tradetested_catalog')->setDeepestCategoryOnProduct($product, $store);

            if ( ($category = $product->getCategory()) && $category->getId()) {
                $path = $product->getData('url_path')
                    ? $product->getUrlPath($category)
                    : Mage::getModel('catalog/url')->getProductRequestPath($product, $category);
                $urls[$store->getCode()] = $store->getBaseUrl().$path;
            }

            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        }
        $product->setStoreId($origStoreId);
        return $urls;
    }

}