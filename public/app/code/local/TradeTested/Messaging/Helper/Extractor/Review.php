<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 08/03/2016
 * Time: 09:36
 */
class TradeTested_Messaging_Helper_Extractor_Review extends TradeTested_Messaging_Helper_Extractor_Abstract
{
    public function extractMessageData(Mage_Review_Model_Review $review)
    {
        $data = $this->_extractData($review,
            ['id', 'review_url', 'title', ['detail', 'content'], ['nickname', 'name'], 'created_at', 'email']);
        if ($review->getStatusId() == Mage_Review_Model_Review::STATUS_APPROVED) {
            $data['action'] = 'published';
        } elseif (
            ($review->getStatusId() != Mage_Review_Model_Review::STATUS_APPROVED)
            && ($review->getOrigData('status_id') == Mage_Review_Model_Review::STATUS_APPROVED)
        ) {
            $data['action'] = 'unpublished';
        }
        $data['created_at'] = date("c", strtotime($data['created_at']));  //Always use ISO8601

        if ($review->getEntityId() == $review->getEntityIdByCode(Mage_Review_Model_Review::ENTITY_PRODUCT_CODE)) {
            $product = Mage::getModel('catalog/product')->load($review->getEntityPkValue());
            $data['product'] = $this->_extractData($product,['sku', 'name', ['product_url', 'url']]);
        }
        $data['ratings'] = [];
        $ratingCollection = Mage::getModel('rating/rating_option_vote')
            ->getResourceCollection()
            ->setReviewFilter($review->getId())
            ->setStoreFilter($review->getStoreId())
            ->addRatingInfo($review->getStoreId())
            ->load();
        /** @var Mage_Rating_Model_Rating $_rating */
        foreach ($ratingCollection as $_rating) {
            $data['ratings'][] = $this->_extractData($_rating, ['rating_code', 'value']);
        }

        return $data;
    }
}