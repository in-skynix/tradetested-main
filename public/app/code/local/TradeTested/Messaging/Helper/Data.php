<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 24/02/2016
 * Time: 16:46
 */
class TradeTested_Messaging_Helper_Data extends Mage_Core_Helper_Data
{
    public function sendMessageToErrorQueue(Oggetto_Messenger_Model_Event $event, Throwable $e) {
        $event
            ->addData(array('original_name' => $event->getName(), 'error' => array(
                'code'          => $e->getCode(),
                'message'       => $e->getMessage(),
                'line'          => $e->getLine(),
                'trace_string'  => $e->getTraceAsString(),
                'file'          => $e->getFile(),
            )))
            ->setName('error');
        Mage::getSingleton('messenger/di')
            ->newInstance('messenger/event_dispatcher')
            ->dispatch($event);
    }

    public function sendMessage(string $name, array $data)
    {
        if (Mage::getStoreConfig("messenger/rabbitmq/host")) {
            /** @var Mage_Sales_Model_Order $order */
            $event = Mage::getModel('messenger/event')
                ->setName($name)
                ->setData($data);
            Mage::getSingleton('messenger/di')
                ->newInstance('messenger/event_dispatcher')
                ->dispatch($event);
        }
    }
}