<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 04/12/2015
 * Time: 09:28
 *
 * @method string getTitle()
 * @method $this setTitle($value)
 * @method string getComponent()
 * @method $this setComponent($value)
 * @method string getItemType()
 * @method $this setItemType($value)
 * @method string getItemId()
 * @method $this setItemId($value)
 * @method string getRecoType()
 * @method $this setRecoType($value)
 * @method string getRecoPath()
 * @method $this setRecoPath($value)
 * @method $this setPerPage($value)
 * @method $this setNumberOfPages($value)
 */
class TradeTested_RelatedProducts_Block_Loader extends Mage_Core_Block_Abstract
{
    protected $_requestParameters = array();
    protected $_divId;

    /**
     * @return array
     */
    public function getCartSkus()
    {
        if (!$this->getData('cart_skus')) {
            $skus = array();
            /** @var Mage_Sales_Model_Quote_Item $_item */
            foreach (Mage::getSingleton('checkout/session')->getQuote()->getAllVisibleItems() as $_item) {
                $skus[] = $_item->getProductId();
            }
            $this->setData('cart_skus', $skus);
        }
        return $this->getData('cart_skus');
    }

    /**
     * @return $this
     */
    public function addCartSkusToParameters()
    {
        return $this->setRequestParameter('skus[]', $this->getCartSkus());
    }

    /**
     * @param $param
     * @param $value
     * @return $this
     */
    public function setRequestParameter($param, $value)
    {
        $this->_requestParameters[$param] = $value;
        return $this;
    }

    /**
     * @return array
     */
    public function getRequestParameters()
    {
        return empty($this->_requestParameters) ? new StdClass() : $this->_requestParameters;
    }

    /**
     * @return string
     */
    public function getRecoUrl()
    {
        if ($this->getRecoType()) {
            $id = $this->getItemId() ? $this->getItemId() : $this->getProduct()->getId();
            return $this->helper('avid_gravitator/recommendations')
                ->getRecommendationsUrl($this->getItemType(), $id, $this->getRecoType());
        } else {
            return $this->helper('avid_gravitator')->getBaseUrl('jvt').$this->getRecoPath();
        }
    }

    /**
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        if (!$this->getData('product')) {
            $this->setData('product', Mage::registry('current_product'));
        }
        return $this->getData('product');
    }

    public function getPerPage()
    {
        return $this->getData('per_page') ? $this->getData('per_page') : 4;
    }

    public function getNumberOfPages()
    {
        return $this->getData('number_of_pages') ? $this->getData('number_of_pages') : 2;
    }
    
    public function getDivId()
    {
        return 'reco_'.str_replace('.', '-', $this->getNameInLayout());
    }
    
    public function getConfigJson()
    {
        return [
            'containerId'             => $this->getDivId(),
            'url'                     => $this->getRecoUrl(),
            'title'                   => $this->getTitle(),
            'component'               => $this->getComponent(),
            'perPage'                 => $this->getPerPage(),
            'numberOfPages'           => $this->getNumberOfPages(),
            'additionalRequestParams' => $this->getRequestParameters(),
        ];
    }

    /**
     * @return array
     */
    public function getJsComponentConfig()
    {
        return ['reljs' => [
            'name'   => 'ProductRecommendations',
            'ref'    => $this->getNameInLayout(),
            'config' => $this->getConfigJson(),
        ]];
    }
    
    protected function _toHtml()
    {
        return '<div id="'.$this->getDivId().'"></div>';
    }
}
