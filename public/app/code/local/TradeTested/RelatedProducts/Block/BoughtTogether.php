<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 19/12/14
 * Time: 21:07
 */
/**
 * Class TradeTested_RelatedProducts_Block_BoughtTogether
 *
 * @method setProductId(int $value)
 * @method int getProductId()
 */
class TradeTested_RelatedProducts_Block_BoughtTogether extends Mage_Catalog_Block_Product_List_Related
{
    public function getProductsJson()
    {
        $product = $this->getProduct()->setIsMain(true);
        $data = [$this->_getProductData($product)];

        /** @var Mage_Catalog_Model_Product $_item */
        foreach ($this->getItems() as $_item) {
            if(!$_item->isComposite() && $_item->isSaleable() && !$_item->getRequiredOptions()) {
                $data[] = $this->_getProductData($_item);
            }
        }
        return $data;
    }
    
    public function getConfigJson()
    {
        if ($this->getItems()->getSize()) {
            return ['container' => 'fbt_container', 'products' => $this->getProductsJson()];
        } else {
            return ['container' => false];
        }
    }

    protected function _getProductData(Mage_Catalog_Model_Product $product) {
        $imageHelper = Mage::helper('catalog/image')->init($product, 'thumbnail')->keepFrame(false);
        return [
            'id'          => $product->getId(),
            'name'        => $product->getName(),
            'price'       => (float)$product->getFinalPrice(),
            'product_url' => $product->getProductUrl(),
            'images'      => [
                '120px' => $imageHelper->keepFrame(true)->resize(120, 90)->__toString(),
                '240px' => $imageHelper->keepFrame(true)->resize(240, 180)->__toString()
            ],
            'is_main'     => (bool)$product->getIsMain(),
            'is_selected' => true
        ];
    }
    
    public function getJsComponentConfig()
    {
        if ($this->getItems()->getSize()) {
//            return ['fbt' => ['name' => 'FrequentlyBoughtTogether', 'config' => $this->getConfigJson()]];
        }
    }
}