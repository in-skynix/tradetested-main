<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 9/06/16
 * Time: 1:31 PM
 */
class TradeTested_Checkout_Block_Rates_Depot extends TradeTested_Checkout_Block_Rates_Default
{
    protected $_template = 'tradetested/checkout/step/shipping/rate_group/depot.phtml';
    protected $_selectedDepot = false;

    /**
     * @return bool|string
     */
    public function getSelectedDepot()
    {
        if (!$this->_selectedDepot) {
            foreach ($this->getRates() as $_rate) {
                if ($_rate->getCode() === $this->getAddressShippingMethod()) {
                    return $this->_selectedDepot = $_rate->getCode();
                }
            }
        }
        return $this->_selectedDepot;
    }

    /**
     * @return bool
     */
    public function isSelected()
    {
        return !!($this->getSelectedDepot());
    }

    /**
     * @return bool|string
     */
    public function getDefaultDepot()
    {
        if ($this->isSelected()) {
            return $this->getSelectedDepot();
        }
        $defaultDepotRate = false;
        foreach ($this->getRates() as $_rate) {
            if (!$defaultDepotRate || ($_rate->getDistance() < $defaultDepotRate->getDistance())) {
                $defaultDepotRate = $_rate;
            }
        }
        return $defaultDepotRate;
    }
    
    public function getGroupedRates()
    {
        $groupedRates = [];
        foreach ($this->getRates() as $_rate) {
            $state = $_rate->getState();
            $groupedRates[$state] = $groupedRates[$state] ?? [];
            $groupedRates[$state][] = $_rate;
        }
        return $groupedRates;
    }
    
    public function getStates()
    {
        return [
            'NI' => 'North Island',
            'SI' => 'South Island',
        ];
    }
        
}