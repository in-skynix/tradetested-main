<?php
/**
 * Class TradeTested_Checkout_Block_Ajax_Totals
 *
 * For use with TradeTested_AjaxBlocks
 */
class TradeTested_Checkout_Block_Ajax_Totals extends Mage_Checkout_Block_Cart_Totals
{
    protected $_template = 'checkout/cart/totals.phtml';
    protected function _beforeToHtml()
    {
        // FormData is USER INPUT. Treat with care;
        $formData = $this->getFormData();
        if (isset($formData['postcode']) && is_string($formData['postcode'])) {
            Mage::helper('tradetested_regions')->setPostcode($formData['postcode']);
            $this->getQuote()->getShippingAddress()
                ->setCountryId(Mage::getStoreConfig('general/country/default'))
                ->setPostcode($formData['postcode']);

        }
        $this->getQuote()->getShippingAddress()->setCollectShippingRates(true);
        if (isset($formData['items'])) {
            $cartData = array();
            foreach($formData['items'] as $_itemId => $_itemData) {
                $cartData[$_itemId] = array('qty' => (int)$_itemData['quantity']);
            }
            Mage::getSingleton('checkout/cart')->updateItems($cartData);
        }
        if (($formData['shipping_method']??false) && is_string($formData['shipping_method'])) {
            $this->getQuote()->getShippingAddress()
                ->setShippingMethod($formData['shipping_method']);
        }
        $this->setAjaxData(['estimator' => Mage::helper('tradetested_shipping/estimator')->getCartConfig()]);
        $this->getQuote()->collectTotals()->save();
    }
}