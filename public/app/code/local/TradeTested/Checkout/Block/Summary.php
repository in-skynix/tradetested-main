<?php
class TradeTested_Checkout_Block_Summary extends TradeTested_Checkout_Block_Step_Abstract
{
    protected $_template = 'tradetested/checkout/summary.phtml';

    /**
     * Setup for use as AJAX Block in CheckoutShippingEstimator
     * @return Mage_Core_Block_Abstract|void
     */
    protected function _beforeToHtml()
    {
        if (!$this->getChild('totals')) {
            $totals = $this->getLayout()->createBlock('checkout/cart_totals', 'checkout.review.totals');
            $this->setChild('totals', $totals);
        }
        if($method = $this->getShippingMethod()) {
            $this->getQuote()->getShippingAddress()
                ->setShippingMethod($method);
            $this->getQuote()->collectTotals()->save();
        }
    }
    
    public function getAddressNote() {
        $address = $this->getQuote()->getShippingAddress();
        $rate = $address->getShippingRateByCode($address->getShippingMethod());
        return $rate->getAddressNote();
    }
}