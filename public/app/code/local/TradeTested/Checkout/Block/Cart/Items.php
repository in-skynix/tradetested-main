<?php
class TradeTested_Checkout_Block_Cart_Items extends Mage_Checkout_Block_Cart
{
    /**
     * @return Mage_Core_Block_Abstract|void
     */
    public function _beforeToHtml()
    {
        $this->setTemplate('tradetested/checkout/cart/items.phtml')
            ->addItemRender('simple', 'checkout/cart_item_renderer', 'checkout/cart/item/default.phtml')
            ->addItemRender('grouped', 'checkout/cart_item_renderer_grouped', 'checkout/cart/item/default.phtml')
            ->addItemRender(
                'configurable', 'checkout/cart_item_renderer_configurable', 'checkout/cart/item/default.phtml'
            );
    }

    /**
     * Get Recently Removed Items
     *
     * @return array
     */
    public function getRemovedItems()
    {
        return $this->helper('tradetested_checkout/cart')->getRecentlyRemovedItems();
    }

    /**
     * Get Add To Cart URL for Item
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @return string
     */
    public function getAddToCartUrl(Mage_Sales_Model_Quote_Item $item)
    {
        $params = array(
            'product' => $item->getProductId(),
            Mage_Core_Model_Url::FORM_KEY => Mage::getSingleton('core/session')->getFormKey()
        );
        return $this->getUrl('checkout/cart/add', $params);
    }
}