<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 15/06/16
 * Time: 3:23 PM
 */
class TradeTested_Checkout_Block_Cart_Minicart extends Mage_Checkout_Block_Cart_Minicart
{
    public function getJsComponentConfig()
    {
        $helper = Mage::helper('tradetested_checkout/cart');
        if (Mage::helper('tradetested_design/theme')->isMobile()) {
            return ['microcart' => ['name' => 'MicroCart', 'config' => $helper->getMicroCartJson()]];
        } else {
            return ['minicart' => ['name' => 'MiniCart', 'config' => $helper->getMiniCartJson()]];
        }
    }
}