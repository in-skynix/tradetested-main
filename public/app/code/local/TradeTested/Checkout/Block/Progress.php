<?php
class TradeTested_Checkout_Block_Progress extends Mage_Core_Block_Template
{
    public function getAvailableSteps()
    {
        return array(
            'cart' => array('label' => 'My Cart', 'url' => $this->getUrl('checkout/cart/index', array('_secure' => true))),
            'addresses' => array('label' => 'My Details', 'url' => $this->getUrl('*/*/index', array('_secure' => true))),
            'shipping_method' => array('label' => 'Shipping', 'url' => $this->getUrl('*/*/shippingMethod', array('_secure' => true))),
            'payment' => array('label' => 'Payment', 'url' => $this->getUrl('*/*/payment', array('_secure' => true)))
        );
    }

    public function isCurrentStep($step)
    {
        return (boolean)($this->getStep() == $step);
    }

    public function isStepComplete($step)
    {
        $keys = array_keys($this->getAvailableSteps());
        return (boolean)(array_search($this->getStep(), $keys) > array_search($step, $keys));
    }

    public function getStepStatus($step)
    {
        if ($this->isCurrentStep($step))
            return 'current';
        return $this->isStepComplete($step) ? 'complete' : 'todo';
    }
}
