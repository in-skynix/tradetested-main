<?php
class TradeTested_Checkout_Block_Cms_Block extends  Mage_Cms_Block_Block
{
    /**
     * Prepare Content HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        $blockId = $this->getBlockId();
        $html = '';
        if ($blockId) {
            $block = Mage::getModel('cms/block')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($blockId);
            if ($block->getIsActive()) {
                $processor = Mage::getModel('tradetested_checkout/template_filter');
                $html = '<div class="std">'.$processor->filter($block->getContent()).'</div>';
            }
        }
        return $html;
    }

    /**
     * Never Cache Order Number
     *
     * @return bool
     */
    protected function _saveCache($data)
    {
        return false;
    }

    /**
     * Never Cache Order Number
     *
     * @return bool
     */
    protected function _loadCache()
    {
        return false;
    }
}