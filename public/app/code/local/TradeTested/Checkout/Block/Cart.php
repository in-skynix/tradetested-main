<?php
class TradeTested_Checkout_Block_Cart extends Mage_Checkout_Block_Cart
{
    public function getCartJson()
    {
        $address = $this->getQuote()->getShippingAddress();
        $data = array('items' => array(), 'totals' => array(), 'postcode' => $address->getPostcode());
        /** @var Mage_Sales_Model_Quote_Item $_item */
        foreach ($this->getItems() as $_item) {
            $data['items'][$_item->getId()] = array(
                'quantity' => $_item->getQty(),
                'price' =>  $this->helper('checkout')->getPriceInclTax($_item),
                'subtotal' => $_item->getRowTotal(),
            );
        }

        /** @var Mage_Sales_Model_Quote_Address_Total_Abstract $_total */
        foreach ($this->getTotals() as $_total) {
            $data['totals'][$_total->getCode()] = array(
                'value' => $_total->getValue(),
                'label' => $_total->getTitle()
            );
        }
        return ['data' => $data];
    }
    
    public function getJsComponentConfig()
    {
        return ['cart' => ['name' => 'Cart', 'config' => $this->getCartJson()]];
    }
}