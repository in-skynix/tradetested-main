<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 24/09/15
 * Time: 18:29
 */
class TradeTested_Checkout_Block_AddressManager extends Mage_Core_Block_Template
{
    protected function _toHtml()
    {
        if ($this->getCustomer()->getId()) {
            return parent::_toHtml();
        }
    }

    /**
     * @return Mage_Customer_Model_Customer|mixed
     */
    public function getCustomer()
    {
        $customer = $this->getData('customer');
        if (is_null($customer)) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $this->setData('customer', $customer);
        }
        return $customer;
    }

    /**
     * @todo: sort the addresses by primary first, then date. And add is_primary?
     * @return mixed
     */
    public function getAddresses()
    {
        if(!$this->getData('addresses')) {
            $this->setData('addresses', $this->getCustomer()->getAddresses());
        }
        return $this->getData('addresses');
    }

    public function getCacheKeyInfo()
    {
        return array_merge(parent::getCacheKeyInfo(), array(
            $this->getCustomer()->getId()
        ));
    }

}