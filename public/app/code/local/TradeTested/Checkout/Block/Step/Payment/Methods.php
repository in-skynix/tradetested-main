<?php
class TradeTested_Checkout_Block_Step_Payment_Methods extends Mage_Checkout_Block_Onepage_Payment_Methods
{

    /**
     * Retrieve code of current payment method
     *
     * @return mixed
     */
    public function getSelectedMethodCode()
    {
        if ($method = $this->getQuote()->getPayment()->getMethod()) {
            return $method;
        } elseif ($this->canUseSavedCard()) {
            if (!$this->getQuote()->getPayment()->getData('tradetested_px_token_billing_agreement')) {
                $agreementId = Mage::getModel('tradetested_payment_express/method_token')
                    ->setInfoInstance($this->getQuote()->getPayment())
                    ->getBillingAgreements()
                    ->addFieldToFilter('is_default', true)
                    ->getFirstItem()
                    ->getEncId();
                $this->getQuote()->getPayment()->setData('billing_agreement_id', $agreementId);
            }
            return 'tradetested_px_token';
        } else {
            return 'tradetested_px_post';
        }
    }

    public function canUseSavedCard()
    {
        foreach ($this->getMethods() as $_method) {
            if ($_method->getCode() == 'tradetested_px_token') {
                return true;
            }
        }
        return false;
    }

}