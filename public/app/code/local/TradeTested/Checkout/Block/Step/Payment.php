<?php
class TradeTested_Checkout_Block_Step_Payment extends TradeTested_Checkout_Block_Step_Abstract
{
    public function getFormAction()
    {
        return $this->getUrl('*/*/savePayment', array('_secure' => true));
    }
    
    public function getJsComponentConfig()
    {
        return ['step' => ['name' => 'CheckoutSteps.Payment']];
    }
}