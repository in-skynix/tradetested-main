<?php
class TradeTested_Checkout_Block_Step_Abstract extends Mage_Core_Block_Template
{
    protected $_quote;

    public function getUrl($route = '', $params = array())
    {
        if (!isset($params['_secure']))
            $params['_secure'] = true;
        return parent::getUrl($route, $params);
    }

    /**
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        if (!$this->_quote)
            $this->_quote =  Mage::getSingleton('checkout/session')->getQuote();
        return $this->_quote;
    }
    public function getBillingAddress()
    {
        return $this->getQuote()->getBillingAddress();
    }
    public function getShippingAddress()
    {
        return $this->getQuote()->getShippingAddress();
    }
}
