<?php 
class TradeTested_Checkout_Block_Step_Addresses extends TradeTested_Checkout_Block_Step_Abstract
{
    public function getFormAction()
    {
        return $this->getUrl('*/*/saveAddresses', array('_secure' => true));
    }

    public function getLoginAction()
    {
        return $this->getUrl('*/*/login', array('_secure' => true));
    }

    public function getLogoutUrl()
    {
        return Mage::getUrl('*/*/logout', array('_secure' => true,));
    }

    public function getAddressManagerConfig()
    {
        $checkoutSession = Mage::getSingleton('checkout/session');

        return [
            'oldAddressData' => [
                'shipping' => $checkoutSession->getOldShippingAddressData(),
                'billing'  => $checkoutSession->getOldBillingAddressData(),
            ],
            'addressBook'    => $this->_getAddressesPrefill(),
        ];
    }

    protected function _getAddressesPrefill()
    {
        $data = array();
        foreach ($this->getQuote()->getCustomer()->getAddresses() as $_address) {
            $data[$_address->getId()] = Mage::helper('tradetested_checkout/addressPrefill')
                ->extractAddressPrefillData($_address);
        }
        return $data;
    }

    public function getLoginJwt()
    {
        return Mage::getModel(
            'tradetested_api/jwt',
            array(
                'aud'   => 'www.tradetested.co.nz',
                'exp'   => strtotime('+30 minutes'),
                'grants'=> array('email_exists_lookup'),
            )
        )->setKey('3mail3xist')->__toString();
    }
    
    public function getConfigJson()
    {
        return [
            'shipping_region' => $this->getShippingAddress()->getRegionId(),
            'billing_region'  => $this->getBillingAddress()->getRegionId(),
            'login_jwt'       => $this->getLoginJwt(),
            'address_manager' => $this->getAddressManagerConfig(),
            'store_code' => Mage::app()->getStore()->getCode(),
            'address_lookup' => [
                'jwt'        => Mage::registry('address.jwt'),
                'baseUrl'    => Mage::getUrl('address'),
            ]
        ];
    }

    /**
     * @return array
     */
    public function getUncachedJsComponentConfig()
    {
        return ['step' => ['name' => 'CheckoutSteps.Addresses', 'config' => $this->getConfigJson()]];
    }
}
