<?php

class TradeTested_Checkout_Block_Step_Shipping extends Mage_Checkout_Block_Onepage_Shipping_Method_Available
{
    protected $_rateGroupTemplates = [
        'default'           => 'tradetested_checkout/rates_default',
        'pickup'            => 'tradetested_checkout/rates_pickup',
        'tradetested_depot' => 'tradetested_checkout/rates_depot',
    ];

    public function getFormAction()
    {
        return $this->getUrl('*/*/saveShippingMethod', ['_secure' => true]);
    }

    public function getDeliveryInstructions()
    {
        return $this->getQuote()->getCustomerNote();
    }

    public function getShippingDetailHtmls()
    {
        $depotHtml = $this->getLayout()->createBlock('cms/block')->setBlockId('shipping_details_depot')->toHtml();
        $depotHtml = str_replace('{{shipping_time_estimate}}', '<span class="sd_depot_estimate"></span>', $depotHtml);
        $htmls = ['depot' => $depotHtml];
        foreach ($this->getAddress()->getAllShippingRates() as $_rate) {
            if (
                ($_code = $_rate->getCmsBlock()) 
                && ($html = $this->getLayout()->createBlock('cms/block')->setBlockId($_code)->toHtml())
            ) {
                $htmls[$_rate->getCode()] = str_replace(
                    '{{shipping_time_estimate}}', $_rate->getShippingTimeEstimate(), $html
                );
            }
        }

        return $htmls;
    }

    /**
     * @param $code
     * @param $rates
     *
     * @return string
     */
    public function renderRateGroup($code, $rates)
    {
        $postcode = $this->getQuote()->getShippingAddress()->getPostcode();
        $postcodeObject = Mage::getModel('tradetested_shipping/postcode')->loadByPostcode(
            $postcode, Mage::getStoreConfig('general/country/default')
        );

        $blockName = $this->_rateGroupTemplates[$code] ?? $this->_rateGroupTemplates['default'];
        return $this->getLayout()->createBlock($blockName, 'rates_' . $code)
            ->setPostcode($postcode)
            ->setAreaName($postcodeObject->getName())
            ->setRates($rates)
            ->toHtml();
    }

    /**
     * @return string
     */
    public function getConfigJson()
    {
        $data = [];
        $rates = $this->getShippingRates();
        if ($rates['tradetested_depot']??false) {
            $rateInfo = [];
            foreach ($rates['tradetested_depot'] as $_rate) {
                $rateInfo[$_rate->getMethod()] = ['shipping_time_estimate' => $_rate->getShippingTimeEstimate()];
            }
            $depots = Mage::getModel('tradetested_shipping/depot')->getCollection()
                ->addFieldToFilter('code', ['in' => array_keys($rateInfo)])
                ->addFieldToFilter('country_code', Mage::getStoreConfig('general/country/default'));
            foreach ($depots as $_depot) {
                $addressState = in_array($_depot->getState(), ['NI', 'SI']) ? "" : ", ".$_depot->getState();
                $rateInfo[$_depot->getCode()]['address'] = $_depot->getAddress().", ".$_depot->getName()
                    .$addressState." ".sprintf("%04d", $_depot->getPostcode());
            }
            $data['depot_info'] = $rateInfo;
        }
        return $data;
    }
}