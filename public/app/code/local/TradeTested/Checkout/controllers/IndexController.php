<?php
require_once 'address/bootstrap.php';
class TradeTested_Checkout_IndexController extends Mage_Checkout_Controller_Action
{

    public function indexAction()
    {
        Mage::register('address.jwt', \AddressLookup\Helper\Auth::getToken());
        if ($cartData = $this->getRequest()->getPost('cart'));
        $this->_updateCart($cartData);
        $quote = $this->_getCheckout()->getQuote();
        if (!$quote->hasItems() || $quote->getHasError()) {
            $this->_redirect('checkout/cart');
            return;
        }
        Mage::helper('tradetested_checkout/addressPrefill')->prefillAddresses($this->_getCheckout()->getQuote());
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        $this->renderLayout();
    }

    /**
     * Login post action
     */
    public function loginAction()
    {
        $session = Mage::getSingleton('customer/session');

        if (!$this->_validateFormKey()) {
            $this->_redirect('*/*/');
            return;
        }

        if ($session->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }

        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');

            //Submit data to saveAddressAction so we have it available and not lost after logging in.
            $addressData = array();
            parse_str($login['address_data'], $addressData);
            $this->_saveAddressData($addressData, false);

            if (!empty($login['username']) && !empty($login['password'])) {
                try {
                    $session->login($login['username'], $login['password']);
                    Mage::getSingleton('checkout/session')->setPrefillNextAddress(true);
                } catch (Mage_Core_Exception $e) {
                    $session->addError($e->getMessage());
                    $session->setUsername($login['username']);
                    //Error message may have reset password link. Retain the email address in session for one more hop.
                    Mage::getModel('tradetested_core/flash')->addHop('last_login_attempt_email');
                } catch (Exception $e) {
                    // Mage::logException($e); // PA DSS violation: this exception log can disclose customer password
                }
            } else {
                $session->addError($this->__('Login and password are required.'));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Customer logout action
     */
    public function logoutAction()
    {
        $session = Mage::getSingleton('customer/session');
        $session->logout()->renewSession();
        $this->_redirect('*/*/index');
    }

    public function shippingMethodAction()
    {
        $quote = $this->_getCheckout()->getQuote();
        if (!$quote->hasItems() || $quote->getHasError()) {
            $this->_redirect('checkout/cart');
            return;
        }
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        $this->renderLayout();
    }

    public function paymentAction()
    {
        $quote = $this->_getCheckout()->getQuote();

        //If the customer somehow stopped submission after payment went through, then tries to re-submit.
        //Mage_Persistent_Model_Observer::checkExpirePersistentQuote() causes the original quote to be re-activated! reload here so we can check if it is actually active.
        $checkQuote = Mage::getModel('sales/quote')->load($quote->getId());
        if ((!$checkQuote->getIsActive()) && $this->_getCheckout()->getCheckoutSession()->getLastOrderId())
            return $this->_redirect('checkout/onepage/success');

        if (!$quote->hasItems() || $quote->getHasError()) {
            $this->_redirect('checkout/cart');
            return;
        }
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        $this->renderLayout();
    }

    protected function _saveAddressData($formData, $shouldValidate = true)
    {
        //Temporary until we have a UI to opt in. Always save if customer is creating an account.
        $formData['shipping']['save_in_address_book'] = 1;
        $formData['billing']['save_in_address_book'] = 1;
        $formData['billing']['firstname'] = $formData['shipping']['firstname'];

        $checkout = $this->_getCheckout();
        $email = isset($formData['billing']['email'])
            ? $formData['billing']['email'] : $checkout->getQuote()->getCustomerEmail();

        $fields = (isset($formData['different_billing']) && $formData['different_billing']) ? 'billing' : 'shipping';
        $checkout->getQuote()->getBillingAddress()->setEmail($email);
        //Setting the data before the conditional to ensure both are set/saved even when there is an error.
        $setShipping = $checkout->importShippingAddress($formData['shipping'], $shouldValidate);
        $setBilling = $checkout->importBillingAddress($formData[$fields], $shouldValidate);
        $checkout->getQuote()->getShippingAddress()->setSameAsBilling(
            !(isset($formData['different_billing']) && $formData['different_billing'])
        );
        Mage::helper('tradetested_checkout/addressPrefill')->checkExistingAddresses($checkout->getQuote());
        $quote = $checkout->getQuote();
        $quote->getShippingAddress()->setCollectShippingRates(true)->collectShippingRates();
        $saved = !!($quote->save());
        return ($saved && $setShipping && $setBilling);
    }

    public function saveAddressesAction()
    {
        $success = true;
        if ($this->getRequest()->getPost('register') && !Mage::getSingleton('customer/session')->isLoggedIn()) {
            $success = $this->_registerCustomer();
        }
        if ($this->_saveAddressData($this->getRequest()->getPost()) && $success) {
            $this->_redirect('*/*/shippingMethod');
        } else {
            $this->_redirect('*/*/index');
        }
    }

    public function saveEmailAddressAction()
    {
        $email = $this->getRequest()->getPost('email');
        if (Zend_Validate::is($email, 'EmailAddress')) {
            $billing = $this->_getCheckout()->getQuote()->getBillingAddress();
            if (!$billing->getEmail())
                $billing->setEmail($email)->save();
        }
    }

    public function savePhoneAction()
    {
        if ($phone = $this->getRequest()->getPost('phone')) {
            $billing = $this->_getCheckout()->getQuote()->getBillingAddress();
            if (!$billing->getTelephone())
                $billing->setTelephone($phone)->save();
        }
    }

    public function saveShippingMethodAction()
    {
        $method = $this->getRequest()->getPost('shipping_method');
        if ($method == 'depot') {
            $method = $this->getRequest()->getPost('depot');
        }
        $checkout = $this->_getCheckout();
        if ($checkout->setShippingMethod($method) && $checkout->setDeliveryInstructions($this->getRequest()->getPost('delivery_instructions'), $this->getRequest()->getPost('signature_opt_out')) && $checkout->getQuote()->save()) {
            $this->_redirect('*/*/payment');
        } else {
            $this->_redirect('*/*/shippingMethod');
        }
    }

    public function savePaymentAction()
    {
        $checkout = $this->_getCheckout();
        $checkoutSession = $checkout->getCheckoutSession();

        //If the customer somehow stopped submission after payment went through, then tries to re-submit.
        //Mage_Persistent_Model_Observer::checkExpirePersistentQuote() causes the original quote to be re-activated! reload here so we can check if it is actually active.
        $checkQuote = Mage::getModel('sales/quote')->load($checkout->getQuote()->getId());
        if ((!$checkQuote->getIsActive()) && $checkoutSession->getLastOrderId())
            return $this->_redirect('checkout/onepage/success');

        try {
            $data = $this->getRequest()->getPost('payment');
            //Allow having fields with same name on different methods. Just have an input array specifically for the method.
            if (isset($data['method']) && isset($data[$data['method']])) {
                $methodData = $data[$data['method']];
                //Pick correct cvv from multiple fields in rebill
                if (isset($methodData['cc_cid']) && is_array($methodData['cc_cid'])) {
                    $methodData['cc_cid'] = $methodData['cc_cid'][$data['billing_agreement_id']];
                }
                $data = array_merge($data, $methodData);
            }

            if ($checkout->savePayment($data)) { //
                if ($url = $checkout->getQuote()->getPayment()->getCheckoutRedirectUrl()) {
                    return $this->_redirectUrl($url);
                }

                if ($checkout->saveOrder()) {
                    if ($url = $checkoutSession->getRedirectUrl()) {
                        return $this->_redirectUrl($url);
                    } else {
                        return $this->_redirect('checkout/onepage/success');
                    }
                }

            }
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            $checkoutSession->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
            $checkoutSession->addError('Unable to set Payment Method.');
        }
        $this->_redirect('*/*/payment');
    }

    protected function _getCheckout()
    {
        return Mage::getSingleton('tradetested_checkout/checkout');
    }

    protected function _registerCustomer()
    {
        $billing = $this->getRequest()->getPost('billing');
        $shipping = $this->getRequest()->getPost('shipping');
        $password = $this->getRequest()->getPost('register_password');

        /** @var Mage_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/customer')
            ->setPassword($password)
            ->setPasswordConfirmation($password)
            ->setEmail($billing['email'])
            ->setFirstname($shipping['firstname'])
        ;
        if (($errors = $customer->validate()) === true) {
            $customer->cleanPasswordsValidationData();
            $customer->save();
            //Do not call customer_register_success as that is intended for reg controller only, would clear quote.
            $quote = $this->_getCheckout()->getQuote();
            $quote->setCustomer($customer);
            $quote->getShippingAddress()->setCustomerId($customer->getId())->save();
            $quote->getBillingAddress()->setCustomerId($customer->getId())->save();
            Mage::getSingleton('customer/session')->setCustomerAsLoggedIn($customer);
            $customer->sendNewAccountEmail('registered', '', Mage::app()->getStore()->getId());
            return true;
        } else {
            foreach ($errors as $_error) {
                $this->_getCheckout()->getCheckoutSession()->addError($_error);
            }
            return false;
        }
    }

    protected function _redirect($path, $arguments = array())
    {
        if (!isset($arguments['_secure']))
            $arguments['_secure'] = true;
        return parent::_redirect($path, $arguments);
    }

    public function _updateCart($cartData)
    {
        try {
            if (is_array($cartData)) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $cartData = array();
                foreach ($cartData as $itemId => $data)
                    $cartData[$itemId] = array('qty' => $filter->filter(trim($data['qty'])));
                $cart = Mage::getSingleton('checkout/cart');
                if (!$cart->getCustomerSession()->isLoggedIn() && $cart->getQuote()->getCustomerId()) {
                    $cart->getQuote()->setCustomerId(null);
                }
                $cartData = $cart->suggestItemsQty($cartData);
                $cart->updateItems($cartData)
                    ->save();
            }

            Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
        } catch (Mage_Core_Exception $e) {
            Mage::getSingleton('checkout/session')->addError($e->getMessage());
            $this->_redirect('checkout/cart');
        } catch (Exception $e) {
            Mage::getSingleton('checkout/session')->addException($e, $this->__('Cannot update shopping cart.'));
            Mage::logException($e);
            $this->_redirect('checkout/cart');
        }
    }
}
