<?php
include_once('Mage/Checkout/controllers/CartController.php');

class TradeTested_Checkout_CartController extends Mage_Checkout_CartController
{
    /**
     * Minicart delete action
     */
    public function ajaxDeleteAction()
    {
        if (!$this->_validateFormKey()) {
            Mage::throwException('Invalid form key');
        }
        $id = (int)$this->getRequest()->getParam('id');
        $result = [];
        if ($id) {
            try {
                $this->_getCart()->removeItem($id)->save();
                $result = [
                    'success' => 1,
                    'messages' => ['success' => [$this->__('Item was removed successfully.')]],
                    'data' => Mage::helper('tradetested_checkout/cart')->getMiniCartJson(),
                ];
            } catch (Exception $e) {
                $result = ['success' => 0, 'messages' => ['error' => [$this->__('Can not remove the item.')]]];
            }
        }

        //ie 8 ajax not work with setHeader()
        header('Content-Type: application/json');
        echo Mage::helper('core')->jsonEncode($result);
    }

    /**
     * Add product to shopping cart action
     *
     * @return Mage_Core_Controller_Varien_Action
     * @throws Exception
     */
    public function addAction()
    {
        if (!$this->_validateFormKey()) {
            $this->_goBack();

            return;
        }
        $cart = $this->_getCart();
        $params = $this->getRequest()->getParams();
        $result = [];
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    ['locale' => Mage::app()->getLocale()->getLocaleCode()]
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            if (!$product) {
                $this->_goBack();

                return;
            }

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            Mage::dispatchEvent('checkout_cart_add_product_complete',
                ['product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
            );

            if ($cart->getQuote()->getHasError()) {
                $errors = [];
                /** @var Mage_Core_Model_Message_Abstract $_message */
                foreach ($cart->getQuote()->getMessages() as $_message) {
                    $errors[] = $_message->getText();
                }
                if (count($errors)) {
                    $result['errors'] = $errors;
                    $result['success'] = $errors[0];
                }
            } else {
                $message = $this->__('%s was added to your shopping cart.',
                    Mage::helper('core')->escapeHtml($product->getName()));
                $result['success'] = $message;
            }
        } catch (Mage_Core_Exception $e) {
            $result['errors'] = [];
            foreach (array_unique(explode("\n", $e->getMessage())) as $message) {
                $result['errors'][] = $message;
            }
        } catch (Exception $e) {
            $result['errors'] = [$this->__('Cannot add the item to shopping cart.')];
            Mage::logException($e);
        }

        return ($this->getRequest()->isAjax()) ? $this->_returnAjaxAddToCartResponse($result)
            : $this->_returnAddToCartResponse($result);
    }

    protected function _returnAjaxAddToCartResponse(array $result)
    {
        $productId = $this->getRequest()->getParam('product');
        $itemsData = Mage::helper('tradetested_checkout/cart')->getCartData($this->_getQuote());
        $data = ($result['success'] ?? null)
            ? ['success' => true, 'last_added_product_id' => $productId, "items" => $itemsData]
            : ['success' => false];
        if (($result['errors'] ?? null) && count($result['errors'])) {
            $data["messages"] = ["error" => ($result['errors'] ?? [])];
        }
        $this->getResponse()->setBody(json_encode($data));
    }

    protected function _returnAddToCartResponse(array $result)
    {
        if ($result['success'] ?? null) {
            $this->_getSession()->addSuccess($result['success']);
        } else {
            foreach (($result['errors'] ?? []) as $_error) {
                $this->_getSession()->addError(Mage::helper('core')->escapeHtml($_error));
            }
        }
        if ($url = $this->_getSession()->getRedirectUrl(true)) {
            $this->getResponse()->setRedirect($url);
        } elseif (!($result['errors'] ?? false)) {
            $this->_goBack(); //actually goes to cart
        } else {
            $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
        }
    }
}