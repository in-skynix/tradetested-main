<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 16/09/15
 * Time: 10:52
 */
require_once MAGENTO_ROOT . '/lib/TradeTested/RateLimit.php';
require_once MAGENTO_ROOT . '/lib/JWT/JWT.php';
require_once MAGENTO_ROOT . '/lib/JWT/BeforeValidException.php';
require_once MAGENTO_ROOT . '/lib/JWT/ExpiredException.php';
require_once MAGENTO_ROOT . '/lib/JWT/SignatureInvalidException.php';
class TradeTested_Checkout_Model_Login_Checker
{
    /**
     * Process without loading Magento.
     *
     * CAREFUL!! $rawData is not sanitized in any way.
     *
     * @param $rawData
     * @return array
     */
    public function processSkipMagento($rawData)
    {
        $limiter = new TradeTested\RateLimit();
        $ipAddress = $_SERVER['REMOTE_ADDR'];

        if (!$this->_isAuthorized()) {
            http_response_code(401);
            return array('html' => '', 'data' => array('error' => 'not_authorized'));
        }

        if ($limiter->isOverLimit(
            array('ip_address' => $ipAddress, 'action' => 'email_exists'),
            array(60*60*24 => 80)
        )) {
            http_response_code(503);
            return array('html' => '', 'data' => array('error' => 'too_many_attempts'));
        }

        $result = array('0');
        //Caching or retrieving the result in APC uses more resources than deserializing the XML
        $config = simplexml_load_file(MAGENTO_ROOT.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'local.xml');
        $config = $config->global->resources->default_setup->connection;
        try {
            $dbh = new PDO("mysql:host={$config->host};dbname={$config->dbname}", $config->username, $config->password);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $dbh->prepare("SELECT EXISTS(SELECT 1 FROM customer_entity WHERE email = ?)");
            $stmt->execute(array($rawData['email']));
            $result = $stmt->fetch(PDO::FETCH_NUM);
        } catch(PDOException $e) {}
        $dbh = null;
        return array('html' => '', 'data' => array('hasAccount' => ($result[0] == '1')));
    }

    protected function _isAuthorized()
    {
        if (!isset($_SERVER['HTTP_AUTHORIZATION'])) {
            return false;
        }

        try {
            list($jwt) = sscanf($_SERVER['HTTP_AUTHORIZATION'], 'JWT %s');
            $claims = \Firebase\JWT\JWT::decode($jwt, '3mail3xist', array('HS256'));
        } catch (UnexpectedValueException $e) {
            return false;
        }

        return (($claims->aud == 'www.tradetested.co.nz') && in_array('email_exists_lookup', $claims->grants));
    }
}