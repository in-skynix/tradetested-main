<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 09/02/15
 * Time: 20:55
 */
class TradeTested_Checkout_Model_Observer
{
    public function salesQuoteRemoveItem(Varien_Event_Observer $observer)
    {
        $item = $observer->getEvent()->getQuoteItem();
        Mage::helper('tradetested_checkout/cart')->addRecentlyRemovedItem($item);
    }

    public function salesQuoteMergeBefore(Varien_Event_Observer $observer)
    {
        Mage::getSingleton('checkout/session')->addNotice(
          "We've added the items you had in your cart during your last visit. ".
          "If you wish to review or change quantities, ".
          '<a href="'.Mage::getUrl('checkout/cart/index').'">head to your cart</a>'
        );
    }
    
    public function salesQuoteSaveBefore(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $observer->getEvent()->getQuote();
        $quote->setUserAgentString(Mage::helper('core/http')->getHttpUserAgent());
    }
}