<?php
class TradeTested_Checkout_Model_Checkout
{
    protected $_helper;
    protected $_checkoutSession;
    protected $_customerSession;
    protected $_quote;

    /**
     * Class constructor
     * Set customer already exists message
     */
    public function __construct()
    {
        $this->_helper = Mage::helper('checkout');
        $this->_checkoutSession = Mage::getSingleton('checkout/session');
        $this->_customerSession = Mage::getSingleton('customer/session');
    }

    /**
     * Get frontend checkout session object
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }

    /**
     * Quote object getter
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        if ($this->_quote === null) {
            return $this->_checkoutSession->getQuote();
        }
        return $this->_quote;
    }

    /**
     * Save checkout shipping address
     *
     * @param $data
     * @param bool|true $shouldValidate
     * @return bool
     */
    public function importShippingAddress($data, $shouldValidate = true)
    {
        return $this->_importAddress($this->getQuote()->getShippingAddress(), $data, $shouldValidate);
    }

    public function importBillingAddress($data, $shouldValidate = true)
    {
        return $this->_importAddress($this->getQuote()->getBillingAddress(), $data, $shouldValidate);
    }

    public function setShippingMethod($shippingMethod)
    {
        if (empty($shippingMethod)) {
            $this->getCheckoutSession()->addError('Invalid shipping method.');
            return false;
        }
        $rate = $this->getQuote()->getShippingAddress()->getShippingRateByCode($shippingMethod);
        if (!$rate) {
            $this->getCheckoutSession()->addError('Invalid shipping method.');
            return false;
        }
        $quote = $this->getQuote();
        $quote->getShippingAddress()->setShippingMethod($shippingMethod);

        $this->_recollectTotals();

        return true;
    }

    public function setDeliveryInstructions($instructions, $signatureOptOut = null)
    {
        if (strlen($instructions) > 160) {
            $this->getCheckoutSession()->addError('Sorry, your delivery instructions must be less than 160 characters');
            return false;
        }
        //$this->getQuote()->getShippingAddress()->setCustomerNotes($instructions);
        $this->getQuote()->setCustomerNote(iconv("UTF-8", "ASCII//IGNORE", $instructions))
            ->setSignatureOptOut($signatureOptOut)
            ; //->setCustomerNoteNotify(false);

        return true;
    }

    public function savePayment($data)
    {
        $quote = $this->getQuote();

        //Paypal express checks this for some reason
        if (!$quote->getCheckoutMethod()) {
            $quote->setCheckoutMethod(Mage_Checkout_Model_Type_Onepage::METHOD_GUEST)->save();
        }

        if (empty($data)) {
            $this->getCheckoutSession()->addError('Invalid data.');
            return false;
        }

        if ($quote->isVirtual()) {
            $quote->getBillingAddress()->setPaymentMethod($data['method'] ?? null);
        } else {
            $quote->getShippingAddress()->setPaymentMethod($data['method'] ?? null);
        }

        // shipping totals may be affected by payment method
        if (!$quote->isVirtual() && $quote->getShippingAddress()) {
            $quote->getShippingAddress()->setCollectShippingRates(true);
        }
        $payment = $quote->getPayment();
        $payment->importData($data);
//        $quote->save(); //CANNOT save payment!! rarrrgh.
        return true;
    }

    public function saveOrder()
    {

        $quote = $this->getQuote();

        if ($this->_customerSession->isLoggedIn()) {
            $this->_prepareCustomerQuote();
        } else {
            $this->_prepareGuestQuote();
        }

        $service = Mage::getModel('sales/service_quote', $quote);
        $service->submitAll();


        $this->_checkoutSession->setLastQuoteId($quote->getId())
            ->setLastSuccessQuoteId($quote->getId())
            ->clearHelperData();

        $order = $service->getOrder();
        if ($order) {
            Mage::dispatchEvent('checkout_type_onepage_save_order_after',
                array('order'=>$order, 'quote'=>$quote));
            /**
             * a flag to set that there will be redirect to third party after confirmation
             * eg: paypal standard ipn
             */
            $redirectUrl = $this->getQuote()->getPayment()->getOrderPlaceRedirectUrl();
            /**
             * we only want to send to customer about new order when there is no redirect to third party
             */

            // add order information to the session
            $this->_checkoutSession->setLastOrderId($order->getId())
                ->setRedirectUrl($redirectUrl)
                ->setLastRealOrderId($order->getIncrementId());

            if (!$redirectUrl && $order->getCanSendNewEmailFlag()) {
                try {
                    //Prevent multiple submissions. @todo: deactivate quotes for redirect payment methods also? The payment failed link to re-try would not work though.
                    $this->getQuote()->setIsActive(0)->save();
                    $order->sendNewOrderEmail();
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
        }
        Mage::dispatchEvent(
            'checkout_submit_all_after',
            array('order' => $order, 'quote' => $quote, 'recurring_profiles' => $service->getRecurringPaymentProfiles())
        );
        return true;
    }

    protected function _recollectTotals()
    {
        $quote = $this->getQuote();
        if (!$quote->isVirtual() && $quote->getShippingAddress()) {
            $quote->getShippingAddress()->setCollectShippingRates(true);
        }
        $quote->collectTotals();
    }

    protected function _importAddress($address, $data, $shouldValidate = true)
    {
        if (empty($data)) {
            $this->getCheckoutSession()->addError('Invalid data.');
            return false;
        }

        $data['country_id'] = Mage::getStoreConfig('general/country/default');


        $addressErrors = $this->_extractAndValidateAddressData($address, $data);


        $address->setCustomerAddressId(null);
        $address->setSaveInAddressBook($data['save_in_address_book']);
        $address->implodeStreetAddress();
        $address->setCollectShippingRates(true);

        if ($address->getAddressType() == 'shipping') {
            $this->_recollectTotals();
        }

        //Save the address before returning errors. we don't want to lose the data.
        if ($shouldValidate) {
            if ($addressErrors !== true) {
                foreach ($addressErrors as $_error) {
                    $this->getCheckoutSession()->addError($_error);
                }
                return false;
            }
            if (($validateRes = $address->validate()) !== true) {
                foreach ($validateRes as $error) {
                    $this->getCheckoutSession()->addError($error);
                }
                return false;
            }
        }
        return true;
    }

    /**
     * Extra Validation For:
     * - normal address validation
     * - Email address validation
     * - Street lines have individual max lengths
     * @param $address
     * @param $data
     * @return array|bool
     * @throws Mage_Core_Exception
     */
    protected function _extractAndValidateAddressData($address, $data)
    {
        $addressErrors = array();

        // Remove duplicated lines, which cause problems in OpenERP.
        if (!isset($data['street_line_3'])) {
            $data['street_line_3'] = '';
        }
        if (($data['street_line_2'] == $data['street_line_1']) || ($data['street_line_2'] == $data['street_line_3'])) {
            $data['street_line_2'] = '';
        }
        if ($data['street_line_3'] == $data['city']) {
            $data['street_line_3'] = '';
        }

        #Ensure street fields have their own names to avoid any ambiguity in browser autocomplete functionality
        $lines = array(
            'street_line_1' => 'Street Address 1',
            'street_line_2' => 'Street Address 2',
            'street_line_3' => 'Suburb'
        );
        $data['street'] = array();
        foreach($lines as $_line => $_label) {
            if (isset($data[$_line])) {
                $data['street'][] = $data[$_line];
                if (strlen($data[$_line]) > 40) {
                    $addressErrors[] = "{$_label} must be 40 characters or less";
                }
            }
        }

        /* @var $addressForm Mage_Customer_Model_Form */
        $addressForm    = Mage::getModel('customer/form');
        $addressForm->setFormCode('customer_address_edit')
            ->setEntityType('customer_address');
        $addressForm->setEntity($address);
        $addressData    = $addressForm->extractData($addressForm->prepareRequest($data));
        $result  = $addressForm->validateData($addressData);
        if (is_array($result)) {
            $addressErrors = array_merge($addressErrors, $result);
        }

        if ($address->getAddressType() == 'billing') {
            $attribute = Mage::getModel('customer/attribute')->loadByCode('customer', 'email');
            $dataModel      = Mage::getModel('eav/attribute_data_text')->setAttribute($attribute)
                ->setEntity(Mage::getModel('customer/customer'));
            $result = $dataModel->validateValue($address->getEmail());
            if (is_array($result)) {
                $addressErrors = array_merge($addressErrors, $result);
            }
        }

        $addressForm->compactData($addressData);
        // unset shipping address attributes which were not shown in form
        foreach ($addressForm->getAttributes() as $attribute) {
            if (!isset($data[$attribute->getAttributeCode()])) {
                $address->setData($attribute->getAttributeCode(), NULL);
            }
        }

        return empty($addressErrors) ? true : $addressErrors;
    }

    /**
     * Prepare quote for customer order submit
     *
     * @return $this
     */
    protected function _prepareCustomerQuote()
    {
        $quote      = $this->getQuote();
        $billing    = $quote->getBillingAddress();
        $shipping   = $quote->isVirtual() ? null : $quote->getShippingAddress();

        $customer = $this->_customerSession->getCustomer();
        if ($shipping && (!$shipping->getCustomerId() || $shipping->getSaveInAddressBook())) {
            $customerShipping = $shipping->exportCustomerAddress();
            $customer->addAddress($customerShipping);
            $shipping->setCustomerAddress($customerShipping);
        }
        if (
            $billing && !$shipping->getSameAsBilling()
            && (!$billing->getCustomerId() || $billing->getSaveInAddressBook())
        ) {
            $customerBilling = $billing->exportCustomerAddress();
            $customer->addAddress($customerBilling);
            $billing->setCustomerAddress($customerBilling);
        }

        if (isset($customerShipping) && !$customer->getDefaultShipping()) {
            $customerShipping->setIsDefaultShipping(true);
        }

        if (isset($customerBilling) && !$customer->getDefaultBilling()) {
            $customerBilling->setIsDefaultBilling(true);
        } elseif(isset($customerShipping) && !$customer->getDefaultBilling()) {
            $customerShipping->setIsDefaultBilling(true);
        }
        $quote->setCustomer($customer);
    }

    protected function _prepareGuestQuote()
    {
        $quote      = $this->getQuote();
        $quote->setCustomerId(null)
            ->setCustomerEmail($quote->getBillingAddress()->getEmail())
            ->setCustomerIsGuest(true)
            ->setCustomerGroupId(Mage_Customer_Model_Group::NOT_LOGGED_IN_ID);
    }
}
