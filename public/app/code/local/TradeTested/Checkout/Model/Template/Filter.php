<?php
class TradeTested_Checkout_Model_Template_Filter extends Mage_Cms_Model_Template_Filter
{
    protected $_templateVars = array();
    public function __construct()
    {
        $this->_templateVars = array(
            'order' => Mage::getModel('sales/order')->load(Mage::getSingleton('checkout/session')->getLastOrderId())
        );
        return parent::__construct();
    }
}