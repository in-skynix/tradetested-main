<?php
class TradeTested_Checkout_Model_Rewrite_Sales_Quote_Address_Total_Grand extends  Mage_Sales_Model_Quote_Address_Total_Grand
{
    /**
     * Add grand total information to address
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Sales_Model_Quote_Address_Total_Grand
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $hasShipping = !!($address->getShippingDescription());
        $address->addTotal(array(
            'code'  => $this->getCode(),
            'title' => $hasShipping ? Mage::helper('sales')->__('Grand Total') : Mage::helper('sales')->__('Subtotal'),
            'value' => $address->getGrandTotal(),
            'area'  => 'footer',
        ));
        return $this;
    }
}
