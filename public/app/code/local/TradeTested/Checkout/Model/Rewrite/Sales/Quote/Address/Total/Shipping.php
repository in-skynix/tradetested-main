<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 10/02/15
 * Time: 15:11
 */
class TradeTested_Checkout_Model_Rewrite_Sales_Quote_Address_Total_Shipping
    extends Mage_Sales_Model_Quote_Address_Total_Shipping
{

    /**
     * Add shipping totals information to address object
     *
     * Customised so not show Shipping & Handling test if it is redundant
     *
     * @param Mage_Sales_Model_Quote_Address $address
     * @return $this
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $amount = $address->getShippingAmount();
        if ($amount != 0 || $address->getShippingDescription()) {
            $title = $address->getShippingDescription() ? $address->getShippingDescription() : 'Shipping & Handling';
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => $title,
                'value' => $address->getShippingAmount()
            ));
        }
        return $this;
    }

    /**
     * Customised to not include carrier in titles.
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Sales_Model_Quote_Address_Total_Shipping
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        parent::collect($address);
        if ($method = $address->getShippingMethod()) {
            foreach ($address->getAllShippingRates() as $rate) {
                $prefix = '';
                if ($rate->getCarrier() == 'tradetested_depot') {
                    $prefix = 'Delivery to ';
                }
                if ($rate->getCode()==$method) {
                    $address->setShippingDescription($prefix.$rate->getMethodTitle());
                    break;
                }
            }
        }
        return $this;
    }
}