<?php
/**
* @author Dane Lowe
*/
class TradeTested_Checkout_Model_Rewrite_Paypal_Api_Standard extends Mage_Paypal_Model_Api_Standard
{
    /**
     * Import address object, if set, to the request
     *
     * @param array $request
     */
    protected function _importAddress(&$request)
    {
        parent::_importAddress($request);
        $request['address_override'] = 0;
    }
}