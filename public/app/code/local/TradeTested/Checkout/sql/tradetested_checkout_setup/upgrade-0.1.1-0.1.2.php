<?php
$installer = new Mage_Customer_Model_Resource_Setup('tradetested_checkout_setup');

$installer->startSetup();
$installer->updateAttribute('customer_address', 'telephone', 'is_required', false);
$installer->updateAttribute('order_address', 'telephone', 'is_required', false);
$installer->endSetup();
