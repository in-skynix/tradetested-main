<?php
$installer = new Mage_Customer_Model_Resource_Setup('tradetested_checkout_setup');

$installer->startSetup();
$installer->updateAttribute('customer', 'firstname', 'validate_rules', 'a:2:{s:15:"max_text_length";i:40;s:15:"min_text_length";i:1;}');
$installer->updateAttribute('customer', 'email', 'validate_rules', 'a:2:{s:16:"input_validation";s:5:"email";s:15:"max_text_length";i:64;}');
$installer->updateAttribute('customer_address', 'firstname', 'validate_rules', 'a:2:{s:15:"max_text_length";i:40;s:15:"min_text_length";i:1;}');
$installer->updateAttribute('customer_address', 'city', 'validate_rules', 'a:2:{s:15:"max_text_length";i:40;s:15:"min_text_length";i:1;}');
$installer->updateAttribute('customer_address', 'postcode', 'validate_rules', 'a:1:{s:15:"max_text_length";i:4;}');
$installer->updateAttribute('customer_address', 'telephone', 'validate_rules', 'a:2:{s:15:"max_text_length";i:15;s:15:"min_text_length";i:1;}');
$installer->endSetup();
