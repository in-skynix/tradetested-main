<?php
//$installer = $this;
$installer = new Mage_Sales_Model_Mysql4_Setup('tradetested_checkout_setup');
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$installer->addAttribute('order', 'signature_opt_out', array('type'=>'boolean'));
$installer->addAttribute('quote', 'signature_opt_out', array('type'=>'boolean'));
$installer->endSetup();
