<?php
$installer = new Mage_Sales_Model_Mysql4_Setup('tradetested_checkout_setup');
$installer->startSetup();
$installer->addAttribute('order', 'user_agent_string', array('type'=>'varchar'));
$installer->addAttribute('quote', 'user_agent_string', array('type'=>'varchar'));
$installer->endSetup();
