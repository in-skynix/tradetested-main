<?php
class TradeTested_Checkout_Helper_Data extends Mage_Core_Helper_Data
{
    public function getTestimonialCategoryForQuote(Mage_Sales_Model_Quote $quote)
    {
        $category = null;
        $testimonialItem = null;
        foreach($quote->getAllItems() as $_item) {
            if (!$testimonialItem ||($_item->getPrice() > $testimonialItem->getPrice())) $testimonialItem = $_item;
        }
        if ($categoryId = $testimonialItem->getProduct()->getDeepestCategoryId()) {
            $category = Mage::getModel('catalog/category')->load($categoryId);
        }
        return $category;
    }

    /**
     * @param string $message
     * @param null $exception
     */
    public function noticeError($message = 'Checkout Exception', $exception = null)
    {
        if (extension_loaded('newrelic')) {
            newrelic_notice_error($message, $exception);
        }
    }
}