<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 24/09/15
 * Time: 18:06
 */
class TradeTested_Checkout_Helper_AddressPrefill extends Mage_Core_Helper_Abstract
{
    /**
     * Prefill Customer's Default Address Data into Quote
     *
     * @param Mage_Sales_Model_Quote $quote
     */
    public function prefillAddresses(Mage_Sales_Model_Quote $quote)
    {
        $customer = $quote->getCustomer();
        if (!$customer->getId()) {
            return;
        }

        $shipping = $quote->getShippingAddress();
        $billing = $quote->getBillingAddress();
        $defaultShipping = $customer->getDefaultShippingAddress();
        $defaultBilling = $customer->getDefaultBillingAddress();
        $checkoutSession = Mage::getSingleton('checkout/session');

        if(
            (($shippingEmpty = $this->_isAddressEmpty($shipping)) || $checkoutSession->getPrefillNextAddress())
            && $defaultShipping && $defaultShipping->getId()
        ) {
            $checkoutSession->setOldShippingAddressData($this->extractAddressPrefillData($shipping));
            $shipping
                ->importCustomerAddress($defaultShipping)
                ->setIsPrefilled(true);
        }

        if(
            (
                // Billing is empty, whether same as shipping or not.
                ($shippingEmpty || !$shipping->getSameAsBilling()) && $this->_isAddressEmpty($billing)
                // Billing is to be overwritten
                || $checkoutSession->getPrefillNextAddress()
            )
            && $defaultBilling && $defaultBilling->getId()
        ) {

            if (!$shipping->getSameAsBilling()) {
                $checkoutSession->setOldBillingAddressData($this->extractAddressPrefillData($billing));
            }

            if ($defaultShipping->getId() == $defaultBilling->getId()) {
                $shipping->setSameAsBilling(true);
            } else {
                $shipping->setSameAsBilling(false);
                $billing
                    ->importCustomerAddress($defaultBilling)
                    ->setIsPrefilled(true);
            }
        }
        $checkoutSession->setPrefillNextAddress(false);
    }

    /**
     * Check for Existing Addresses
     *
     * Don't save in address book if matches existing address.
     *
     * @param Mage_Sales_Model_Quote $quote
     */
    public function checkExistingAddresses(Mage_Sales_Model_Quote $quote)
    {
        $customer = $quote->getCustomer();
        if (!$customer->getId()) {
            return;
        }
        $shipping = $quote->getShippingAddress();
        $billing = $quote->getBillingAddress();

        if ($customerAddress = $this->_customerAddressExists($customer, $shipping)) {
            $shipping->setSaveInAddressBook(false)->setCustomerAddressId($customerAddress->getId());
        }

        if ($shipping->getSameAsBilling() || ($customerAddress = $this->_customerAddressExists($customer, $billing))) {
            $billing->setSaveInAddressBook(false);
            //$customerAddress could have been set by checking shipping address above;
            if ($customerAddress) {
                $billing->setCustomerAddressId($customerAddress->getId());
            }
        }
    }

    /**
     * Extract Data from address that would be used to prefill in JS
     * @param Varien_Object $address
     * @return array
     */
    public function extractAddressPrefillData(Varien_Object $address)
    {
        return array(
            'company'           => $address->getCompany(),
            'firstname'         => $address->getFirstname(),
            'telephone'         => $address->getTelephone(),
            'street_line_1'     => $address->getStreet(1),
            'street_line_2'     => $address->getStreet(2),
            'street_line_3'     => $address->getStreet(3),
            'city'              => $address->getCity(),
            'state'             => $address->getState(),
            'postcode'          => $address->getPostcode(),
            'same_as_billing'   => $address->getSameAsBilling(),
        );
    }

    /**
     * @param Varien_Object $address
     * @return bool
     */
    protected function _isAddressEmpty(Varien_Object $address)
    {
        $fields = array('firstname', 'phone', 'street', 'city', 'postcode', 'company');
        foreach ($fields as $_field) {
            if ($address->getData($_field)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @param Mage_Sales_Model_Quote_Address $quoteAddress
     * @return bool
     */
    protected function _customerAddressExists(
        Mage_Customer_Model_Customer $customer,
        Mage_Sales_Model_Quote_Address $quoteAddress
    ) {
        /** @var Mage_Customer_Model_Address $_address */
        foreach ($customer->getAddresses() as $_address) {
            $excluded = $this->_fieldsToExcludeFromComparison($quoteAddress, $_address);
            if ($this->_getAddressHash($_address, $excluded) == $this->_getAddressHash($quoteAddress, $excluded)) {
                //if data is the same except for excluded fields, add the new data to the customer address,
                // see _fieldsToExcludeFromComparison()
                foreach ($excluded as $_field) {
                    if ($val = $quoteAddress->getDataUsingMethod($_field)) {
                        $_address->setDataUsingMethod($_field, $val);
                    }
                }
                return $_address->save();
            }
        }
        return false;
    }

    /**
     * Find Fields to exclude from comparison
     *
     * If we are comparing a customer address that was created from the other address type,
     * we want them to match excluding the fields not present in the other address.
     *
     * We will then later merge the new data provided with the existing address, rather than create a new one.
     *
     * E.g. if customer adds a company name to an existing shipping address, we add to the existing address.
     * If customer adds a first name to an existing billing address, we add the name to the existing address.
     *
     * @param Mage_Sales_Model_Quote_Address $quoteAddress
     * @param Mage_Customer_Model_Address $customerAddress
     * @return array
     */
    protected function _fieldsToExcludeFromComparison(
        Mage_Sales_Model_Quote_Address $quoteAddress,
        Mage_Customer_Model_Address $customerAddress
    ) {
        $fieldsToExclude = array();
        if ($quoteAddress->getAddressType() == 'shipping')
        {
            //if customer address was a billing address without a name, we match without name/company
            $fieldsToExclude[] = 'company';
            if (!$customerAddress->getFirstname()) {
                $fieldsToExclude[] = 'firstname';
            }
            if (!$customerAddress->getTelephone()) {
                $fieldsToExclude[] = 'telephone';
            }
        } else {
            //New address is a billing address, if customer address is a shipping address with no company and with a name.
            $fieldsToExclude[] = 'firstname';
            $fieldsToExclude[] = 'telephone';
            if (!$customerAddress->getCompany()) {
                $fieldsToExclude[] = 'company';
            }
        }
        return $fieldsToExclude;
    }

    protected function _getAddressHash(Varien_Object $address, $excludeFields = array())
    {
        $data = $this->extractAddressPrefillData($address);
        unset($data['same_as_billing']);
        // If billing/shipping address
        foreach ($excludeFields as $_field) {
            unset($data[$_field]);
        }
        return md5(json_encode($data));
    }
}