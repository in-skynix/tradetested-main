<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 09/02/15
 * Time: 20:59
 */
class TradeTested_Checkout_Helper_Cart extends Mage_Core_Helper_Abstract
{
    /**
     * Add Item to list of Recently Removed
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @return $this
     */
    public function addRecentlyRemovedItem(Mage_Sales_Model_Quote_Item $item)
    {
        Mage::unregister('tradetested_checkout.cart.recently_removed_items');
        Mage::register('tradetested_checkout.cart.recently_removed_items',
            array_merge($this->getRecentlyRemovedItems(), array($item))
        );
        return $this;
    }

    /**
     * Get Recently Removed Items
     *
     * @return array
     */
    public function getRecentlyRemovedItems()
    {
        return ($items = Mage::registry('tradetested_checkout.cart.recently_removed_items')) ? $items : array();
    }

    /**
     * @param Mage_Sales_Model_Quote $quote
     * @return array
     */
    public function getCartData(Mage_Sales_Model_Quote $quote)
    {
        $itemsData = [];
        $imageHelper = Mage::helper('catalog/image');
        /** @var Mage_Sales_Model_Quote_Item $_item */
        foreach ($quote->getAllVisibleItems() as $_item) {
            $product = $_item->getProduct();
            $itemsData[] = [
                'id' => $_item->getId(),
                'product_sku' => $product->getSku(),
                'product_name' => $product->getName(),
                'product_url' => $product->getProductUrl(),
                'images' => [
                    '50px' => $imageHelper->init($product, 'thumbnail')->keepFrame(false)->resize(50)->__toString(),
                    '100px' => $imageHelper->init($product, 'thumbnail')->keepFrame(false)->resize(100)->__toString(),
                ],
                'quantity' => $_item->getQty()
            ];
        }
        return $itemsData;
    }
    
    public function getMiniCartJson()
    {
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $totals = [];
        /** @var Mage_Sales_Model_Quote_Address_Total_Abstract $_total */
        foreach ($quote->getTotals() as $_total) {
            $totals[$_total->getCode()] = [
                'value' => $_total->getValue(),
                'value_formatted' =>  Mage::helper('core')->currency($_total->getValue(), true, false),
                'label' => $_total->getTitle(),
            ];
        }

        return [
            'cartUrl' => Mage::helper('checkout/cart')->getCartUrl(),
            'items'   => $this->getCartData($quote),
            'totals'  => $totals,
        ];
    }

    public function getMicroCartJson()
    {
        $quote = Mage::helper('checkout/cart')->getQuote();
        return ['items' => Mage::helper('tradetested_checkout/cart')->getCartData($quote)];
    }
    
    public function getAjaxAddToCartJson()
    {
        $mainProduct = ($_product = Mage::registry('product')) 
            ? ['name' => $_product->getName(), 'id' => $_product->getId(), 'product_url' => $_product->getProductUrl()] 
            : null;
        $data = [
            'mainProduct' => $mainProduct,
            'crossSellUrl' => Mage::helper('avid_gravitator')->getBaseUrl('jvt').'recommendations/cross_sell',
        ];
        if (Mage::helper('tradetested_design/theme')->isMobile()) {
            $data['header'] = 'Added to your cart';
        }
        return $data;
    }
}