<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 16/09/15
 * Time: 14:57
 */
class TradeTested_CustomerAccount_Model_Observer
{
    /**
     * Fix issue with JSON decoding null when persistent session first created
     *
     * @param Varien_Event_Observer $observer
     */
    public function coreAbstractLoadAfter(Varien_Event_Observer $observer)
    {
        $object = $observer->getEvent()->getObject();
        if ((get_class($object) == 'Mage_Persistent_Model_Session') && !$object->getInfo()) {
            $object->setInfo('{}');
        }
    }

    /**
     * Redirect directly to home on logout.
     *
     * @param Varien_Event_Observer $observer
     */
    public function controllerActionPostdispatchCustomerAccountLogout(Varien_Event_Observer $observer)
    {
        /** @var Mage_Core_Controller_Front_Action $action */
        $action = $observer->getEvent()->getControllerAction();
        $action->setRedirectWithCookieCheck('/');
    }

    /**
     * Set Gravitator Email Tracker on Login/Create account
     *
     * @param Varien_Event_Observer $observer
     */
    public function customerLogin(Varien_Event_Observer $observer)
    {
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $observer->getEvent()->getCustomer();
        //Customer session stores in 'id' when logging in, but there is also a memoised 'customer_id',
        //which gets out of sync
        Mage::getSingleton('customer/session')->setCustomerId(null);
        Mage::getSingleton('core/cookie')->set('pxl.emh', base64_encode($customer->getEmail()) ,time()+63072000,'/');
    }

    public function customerRegisterSuccess(Varien_Event_Observer $observer)
    {
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $observer->getEvent()->getCustomer();
        /** @var TradeTested_CustomerAccount_AccountController $controller */
        $controller = $observer->getEvent()->getAccountController();

        if ($controller->getRequest()->getPost('newsletter_signup')) {
            Mage::helper('tradetested_newsletter')->subscribeCustomer($customer);
        }
    }
}