<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 16/09/15
 * Time: 15:09
 */
class TradeTested_CustomerAccount_Helper_Data extends Mage_Core_Helper_Data
{
    public function sendMissingAccountEmail(string $email)
    {
        if (Mage::getStoreConfigFlag('customer/password_reset/send_missing_account_email')) {
            Mage::getModel('core/email_template')
                ->setDesignConfig(['area' => 'frontend'])
                ->sendTransactional(
                    Mage::getStoreConfig('customer/password_reset/missing_account_email_template'),
                    Mage::getStoreConfig('contacts/email/sender_email_identity'),
                    $email,
                    'person',
                    []
                )->getSentSuccess()
            ;
        }
    }

    public function getAccountMenuConfigJson()
    {
        return [
            'urls'           => [
                'forgot_password' => Mage::helper('customer')->getForgotPasswordUrl(),
                'action'          => Mage::helper('customer')->getLoginPostUrl(),
            ],
        ];
    }
}