<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 06/10/2015
 * Time: 15:40
 */
class TradeTested_CustomerAccount_Block_Navigation extends Mage_Customer_Block_Account_Navigation
{
    public function getLinks()
    {
        return $this->getLayout()->getBlock('account_top_menu')->getLinks();
    }
}