<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/01/2016
 * Time: 18:29
 */
require_once 'Mage/Customer/controllers/AccountController.php';
class TradeTested_CustomerAccount_AccountController extends Mage_Customer_AccountController
{
    /**
     * Don't require password confirmation
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return array|string
     */
    protected function _getCustomerErrors($customer)
    {
        $this->getRequest()->setPost('confirmation',$this->getRequest()->getPost('password'));
        return parent::_getCustomerErrors($customer);
    }

    /**
     * Require current password in order to change email address!
     */
    public function editPostAction()
    {

        if ($this->getRequest()->isPost()) {
            /** @var $customer Mage_Customer_Model_Customer */
            $customer = $this->_getSession()->getCustomer();
            if (
                ($this->getRequest()->getPost('email') != $customer->getEmail())
                && !$customer->validatePassword($this->getRequest()->getPost('current_password'))
            ) {
                $this->_getSession()->addError('Incorrect password supplied');
                return $this->_redirect('customer/account');
            }
        }
        return parent::editPostAction();
    }

    /**
     * Don't require password confirmation to reset password.
     */
    public function resetPasswordPostAction()
    {
        $this->getRequest()->setPost('confirmation',$this->getRequest()->getPost('password'));
        return parent::resetPasswordPostAction();
    }

    /**
     * Primarily used for mobile navigation
     */
    public function menuAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * If customer is locked out of account, redirect them to reset password page.
     * 
     * @return Mage_Core_Controller_Varien_Action|void
     */
    protected function _loginPostRedirect()
    {
        $session = $this->_getSession();
        $login = $this->getRequest()->getPost('login');
        if (
            ($login['username'] ?? null)
            && !$session->isLoggedIn() 
            && Mage::helper('tradetested_security/rateLimit_customerLogin')
                ->lockExists('customer_login_failed', ['email' => $login['username']])
        ) {
            return $this->_redirect('*/*/forgotPassword');

        }
        return parent::_loginPostRedirect();
    }

    /**
     * Forgot customer password action
     * 
     * Customised: If there is no account for the email, send the customer an email to let them know.
     */
    public function forgotPasswordPostAction()
    {
        $email = (string) $this->getRequest()->getPost('email');
        if ($email) {
            if (!Zend_Validate::is($email, 'EmailAddress')) {
                $this->_getSession()->setForgottenEmail($email);
                $this->_getSession()->addError($this->__('Invalid email address.'));
                $this->_redirect('*/*/forgotpassword');
                return;
            }

            /** @var $customer Mage_Customer_Model_Customer */
            $customer = $this->_getModel('customer/customer')
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                ->loadByEmail($email);

            if ($customer->getId()) {
                try {
                    $newResetPasswordLinkToken =  $this->_getHelper('customer')->generateResetPasswordLinkToken();
                    $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                    $customer->sendPasswordResetConfirmationEmail();
                } catch (Exception $exception) {
                    $this->_getSession()->addError($exception->getMessage());
                    $this->_redirect('*/*/forgotpassword');
                    return;
                }
            } else {
                Mage::helper('tradetested_customer_account')->sendMissingAccountEmail($email);
            }
            $this->_getSession()
                ->addSuccess( $this->_getHelper('customer')
                    ->__('If there is an account associated with %s you will receive an email with a link to reset your password.',
                        $this->_getHelper('customer')->escapeHtml($email)));
            $this->_redirect('*/*/');
            return;
        } else {
            $this->_getSession()->addError($this->__('Please enter your email.'));
            $this->_redirect('*/*/forgotpassword');
            return;
        }
    }
}