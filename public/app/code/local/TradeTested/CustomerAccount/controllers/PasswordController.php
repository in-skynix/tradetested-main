<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 23/09/15
 * Time: 17:02
 */
class TradeTested_CustomerAccount_PasswordController extends Mage_Core_Controller_Front_Action
{
    /**
     * Retrieve customer session model object
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     */
    public function preDispatch()
    {
        parent::preDispatch();

        if (!$this->getRequest()->isDispatched()) {
            return;
        }

        if (!$this->_getSession()->authenticate($this)) {
            $this->setFlag('', 'no-dispatch', true);
        }
    }

    /**
     * Action postdispatch
     *
     * Remove No-referer flag from customer session after each action
     */
    public function postDispatch()
    {
        parent::postDispatch();
        $this->_getSession()->unsNoReferer(false);
    }

    /**
     * Change Password Form
     */
    public function editAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    /**
     * Change Password Action
     */
    public function editPostAction()
    {
        /** @var $customer Mage_Customer_Model_Customer */
        $customer = $this->_getSession()->getCustomer();

        $currPass   = $this->getRequest()->getPost('current_password');
        $newPass    = $this->getRequest()->getPost('password');
        $confPass   = $this->getRequest()->getPost('password');

        $oldPass = $this->_getSession()->getCustomer()->getPasswordHash();
        if ( Mage::helper('core/string')->strpos($oldPass, ':')) {
            list($_salt, $salt) = explode(':', $oldPass);
        } else {
            $salt = false;
        }

        if ($customer->hashPassword($currPass, $salt) == $oldPass) {
            if (strlen($newPass)) {
                $customer->setPassword($newPass);
                $customer->setPasswordConfirmation($confPass);
            } else {
                $errors[] = $this->__('New password field cannot be empty.');
            }
        } else {
            $errors[] = $this->__('Invalid current password');
        }

        if (!empty($errors)) {
            foreach ($errors as $message) {
                $this->_getSession()->addError($message);
            }
            $this->_redirect('*/*/edit');
            return $this;
        }

        try {
            $customer->cleanPasswordsValidationData();
            $customer->save();
            $this->_getSession()->setCustomer($customer)
                ->addSuccess($this->__('Your password has been changed'));

            return $this->_redirect('customer/account');
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot save the customer.'));
        }
    }
}