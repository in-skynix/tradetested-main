<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 04/02/2016
 * Time: 14:11
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = new Mage_Customer_Model_Resource_Setup('tradetested_customer_account_setup');
$installer->startSetup();

$installer->updateAttribute('customer_address', 'firstname', 'validate_rules', 'a:1:{s:15:"max_text_length";i:40;}');
$installer->updateAttribute('customer_address', 'firstname', 'is_required', false);
$installer->endSetup();
