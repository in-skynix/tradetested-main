<?php
class TradeTested_ProductAlert_AddController extends Mage_Core_Controller_Front_Action
{
    public function stockAction()
    {
        /* @var $session Mage_Catalog_Model_Session */
        $session = Mage::getSingleton('catalog/session');
        $productId  = (int) $this->getRequest()->getParam('product_id');

        $email = $this->getRequest()->getPost('email');
        if (!Zend_Validate::is($email, 'EmailAddress')) {
            $session->addError($this->__('Please enter your email address.'));
            return $this->_redirectReferer();
        }

        if (!$product = Mage::getModel('catalog/product')->load($productId)) {
            /* @var $product Mage_Catalog_Model_Product */
            $session->addError($this->__('Could not find product.'));
            return $this->_redirectReferer();
        }

        try {
            $model = Mage::getModel('productalert/stock')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->setName(trim($this->getRequest()->getPost('name')))
                ->setEmail($this->getRequest()->getPost('email'))
                ->setProductId($product->getId())
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
            $model->save();
            $session->addSuccess($this->__('We will notify you when the product is back in stock.'));
        }
        catch (Exception $e) {
            $session->addException($e, $this->__('Unable to update the alert subscription.'));
        }
        $this->_redirectReferer();
    }

}