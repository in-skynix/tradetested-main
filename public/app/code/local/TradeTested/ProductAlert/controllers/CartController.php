<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 09/09/15
 * Time: 15:46
 */
class TradeTested_ProductAlert_CartController extends Mage_Core_Controller_Front_Action
{
    public function AddProductAction()
    {
        $product = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('product'));
        if (Mage::getSingleton('checkout/session')->getQuote()->getItemByProduct($product)) {
            $this->_redirectUrl(Mage::helper('checkout/cart')->getCartUrl());
        } else {
            $this->_redirectUrl(Mage::helper('checkout/cart')->getAddUrl($product));
        }
    }
}