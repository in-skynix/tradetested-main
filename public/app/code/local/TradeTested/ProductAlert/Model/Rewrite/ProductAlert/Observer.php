<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 07/09/15
 * Time: 15:47
 */
class TradeTested_ProductAlert_Model_Rewrite_ProductAlert_Observer extends Mage_ProductAlert_Model_Observer
{
    /**
     * Process stock emails
     *
     * Customised to allow sending to guests with emails instead of customer ID.@deprecated
     *
     * @param Mage_ProductAlert_Model_Email $email
     * @return Mage_ProductAlert_Model_Observer
     */
    protected function _processStock(Mage_ProductAlert_Model_Email $email)
    {
        $email->setType('stock');
        $originalStore = Mage::app()->getStore();

        foreach ($this->_getWebsites() as $website) {
            /* @var $website Mage_Core_Model_Website */

            if (!$website->getDefaultGroup() || !$website->getDefaultGroup()->getDefaultStore()) {
                continue;
            }
            if (!Mage::getStoreConfig(
                self::XML_PATH_STOCK_ALLOW,
                $website->getDefaultGroup()->getDefaultStore()->getId()
            )) {
                continue;
            }
            try {
                $collection = Mage::getModel('productalert/stock')
                    ->getCollection()
                    ->addWebsiteFilter($website->getId())
                    ->addStatusFilter(0)
                    ->setCustomerOrder();
            }
            catch (Exception $e) {
                $this->_errors[] = $e->getMessage();
                return $this;
            }

            $previousCustomer = null;
            $email->setWebsite($website);
            Mage::app()->setCurrentStore($website->getDefaultGroup()->getDefaultStore());
            foreach ($collection as $alert) {
                try {
                    if (!$previousCustomer || ($previousCustomer->getEmail() != $alert->getEmail())) {
                        $customer = Mage::getModel('customer/customer')->load($alert->getCustomerId());
                        if ($previousCustomer) {
                            $email->send();
                        }
                        if (!$customer->getEmail()) {
                            $customer
                                ->setStoreId($alert->getStoreId())
                                ->setLastname($alert->getName()) //Cheat for spacing in template
                                ->setEmail($alert->getEmail());
                        }
                        if (!$customer->getEmail()) {
                            continue;
                        }
                        $previousCustomer = $customer;
                        $email->clean();
                        $email->setCustomer($customer);
                    }
                    else {
                        $customer = $previousCustomer;
                    }

                    $product = Mage::getModel('catalog/product')
                        ->setStoreId($website->getDefaultStore()->getId())
                        ->load($alert->getProductId());
                    /* @var $product Mage_Catalog_Model_Product */
                    if (!$product) {
                        continue;
                    }

                    $product->setCustomerGroupId($customer->getGroupId());

                    if ($product->isSalable()) {
                        $email->addStockProduct($product);

                        $alert->setSendDate(Mage::getModel('core/date')->gmtDate());
                        $alert->setSendCount($alert->getSendCount() + 1);
                        $alert->setStatus(1);
                        $alert->save();
                    }
                } catch (Exception $e) {
                    $this->_errors[] = $e->getMessage();
                }
            }

            if ($previousCustomer) {
                try {
                    $email->send();
                } catch (Exception $e) {
                    $this->_errors[] = $e->getMessage();
                }
            }
        }
        Mage::app()->setCurrentStore($originalStore);

        return $this;
    }
}