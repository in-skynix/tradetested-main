<?php
/**
 * @author Dane Lowe
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$installer->getConnection()->dropForeignKey(
    $this->getTable('productalert/stock'),
    $installer->getFkName('productalert/stock', 'customer_id', 'customer/entity', 'entity_id')
);
$installer->getConnection()
    ->addColumn(
        $this->getTable('productalert/stock'),
        'email',
        array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length'    => 255,
            'comment'   => 'Email',
            'nullable'  => true
        ));
$installer->getConnection()
    ->addColumn(
        $this->getTable('productalert/stock'),
        'name',
        array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length'    => 255,
            'comment'   => 'Name',
            'nullable'  => true
        ));
$installer->getConnection()
    ->addColumn(
        $this->getTable('productalert/stock'),
        'store_id',
        array(
            'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
            'unsigned'  => true,
            'nullable'  => false,
            'default'   => '0',
            'comment'   => 'Store ID'
        ));
$installer->getConnection()
    ->addIndex(
        $this->getTable('productalert/stock'),
        $installer->getIdxName(array('catalog/product', 'datetime'), array('store_id')),
        array('store_id'));
$installer->getConnection()
    ->addForeignKey(
        $installer->getFkName(
            'productalert/stock',
            'store_id',
            'core/store',
            'store_id'
        ),
        $this->getTable('productalert/stock'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);
$installer->endSetup();