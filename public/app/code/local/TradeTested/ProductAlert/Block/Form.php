<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 07/09/15
 * Time: 13:48
 *
 * @method Mage_Catalog_Model_Product getProduct()
 * @method $this setProduct(Mage_Catalog_Model_Product $value)
 */
class TradeTested_ProductAlert_Block_Form extends Mage_Core_Block_Template
{


    /**
     * Helper instance
     *
     * @var null|Mage_ProductAlert_Helper_Data
     */
    protected $_helper = null;

    /**
     * Check whether the stock alert data can be shown and prepare related data
     *
     * @return void
     */
    public function _prepareLayout()
    {
        $this->setProduct(Mage::registry('current_product'));
        if (!$this->_getHelper()->isStockAlertAllowed() || !$this->getProduct() || $this->getProduct()->isAvailable()) {
            $this->setTemplate('');
            return;
        }
    }

    public function getFormAction()
    {
        return $this->_getHelper()->getSaveUrl('stock');
    }

    /**
     * Retrieve helper instance
     *
     * @return Mage_ProductAlert_Helper_Data|null
     */
    protected function _getHelper()
    {
        if (is_null($this->_helper)) {
            $this->_helper = Mage::helper('productalert');
        }
        return $this->_helper;
    }
}