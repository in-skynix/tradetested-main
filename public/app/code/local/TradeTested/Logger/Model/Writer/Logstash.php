<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 28/10/2015
 * Time: 13:40
 */
class TradeTested_Logger_Model_Writer_Logstash extends FireGento_Logger_Model_Logstash
{
    /**
     * Builds a JSON Message that will be sent to a Logstash Server.
     *
     * @param  FireGento_Logger_Model_Event $event           A Magento Log Event.
     * @param  bool                         $enableBacktrace Indicates if a backtrace should be added to the log event.
     * @return string A JSON structure representing the message.
     */
    protected function buildJSONMessage($event, $enableBacktrace = false)
    {
        Mage::helper('firegento_logger')->addEventMetadata($event, '-', $enableBacktrace);

        $fields = array(
            '@timestamp' => date('c', strtotime($event->getTimestamp())),
            '@version' => "1",
            'level' => $event->getPriorityName(),
            'priority' => $event->getPriority(),
            'file' => $event->getFile(),
            'line_number' => $event->getLine(),
            'store_code' => $event->getStoreCode(),
            'time_elapsed' => $event->getTimeElapsed(),
            'source_host' => $event->getHostname(),
            'message' => $event->getMessage(),
            'backtrace' => $event->getBacktrace(),
            'request_method' => $event->getRequestMethod(),
            'request_uri' => $event->getRequestUri(),
            'request_data' => $event->getRequestData(),
            'clientip' => $event->getRemoteAddress(),
            'http_user_agent' => $event->getHttpUserAgent(),
            'hostname' => $event->getHostname(),
            'type' => 'magento',
        );

        // udp/tcp inputs require a trailing EOL character.
        $encodedMessage = trim(json_encode($fields)) . "\n";
        return $encodedMessage;
    }
    /**
     * Class constructor
     *
     * @param string $filename Filename
     */
    public function __construct($filename)
    {
        /* @var $helper FireGento_Logger_Helper_Data */
        $helper = Mage::helper('firegento_logger');
        $this->_logstashServer = $helper->getLoggerConfig('logstash/host');
        $this->_logstashPort = $helper->getLoggerConfig('logstash/port');
        $this->_logstashProtocol = 'ssl';

    }

    /**
     * Sends a JSON Message to Logstash.
     *
     * @param  string $message The JSON-Encoded Message to be sent.
     * @throws Zend_Log_Exception
     * @return bool True if message was sent correctly, False otherwise.
     */
    protected function publishMessage($message)
    {
        /* @var $helper FireGento_Logger_Helper_Data */
        $helper = Mage::helper('firegento_logger');

        $streamContext = stream_context_create();
        $cert = Mage::getBaseDir().DS.'app'.DS.'code'.DS.'local'.DS.'TradeTested'.DS.'Logger'.DS.'logstash-forwarder.crt';

        stream_context_set_option($streamContext, 'ssl', 'cafile', $cert);
        $connStr = sprintf('%s://%s:%s', $this->_logstashProtocol, $this->_logstashServer, $this->_logstashPort);
        $fp = stream_socket_client($connStr, $errorNumber, $errorMessage, $this->_timeout,
            STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $streamContext);
        try {
            $result = fwrite($fp, $message);
//            fclose($fp);

            if ($result == false) {
                throw new Zend_Log_Exception(
                    sprintf($helper->__('Error occurred posting log message to logstash via tcp. Posted Message: %s'),
                        $message)
                );
            }
        } catch (Exception $e) {
            throw new Zend_Log_Exception($e->getMessage(), $e->getCode());
        }

        return true;
    }

}