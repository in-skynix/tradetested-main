<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 01/12/14
 * Time: 14:35
 */
class TradeTested_Survey_Block_Adminhtml_Response_View_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getData('action'),
            'method'    => 'post',
            'enctype'   => 'multipart/form-data'
        ));

        $surveyResponse = Mage::registry('tradetested_survey.response');

        if ($surveyResponse->getId()) {
            $form->addField('entity_id', 'hidden', array(
                'name' => 'response_id',
            ));
            $form->setValues($surveyResponse->getData());
        }
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}