<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 01/12/14
 * Time: 14:29
 */
class TradeTested_Survey_Block_Adminhtml_Response_View_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('customer_subscription_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('tradetested_survey')->__('Survey Response'));
    }
}