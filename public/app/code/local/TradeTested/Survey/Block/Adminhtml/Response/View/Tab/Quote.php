<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 01/12/14
 * Time: 14:30
 */
class TradeTested_Survey_Block_Adminhtml_Response_View_Tab_Quote
    extends TradeTested_Survey_Block_Adminhtml_Response_View_Tab_Abstract
{
    protected $_tabTitle = 'Quote Details';

    public function getItemsHtml()
    {
        return $this->getChildHtml('quote_items');
    }
}