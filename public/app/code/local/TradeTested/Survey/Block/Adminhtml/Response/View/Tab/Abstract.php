<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 01/12/14
 * Time: 14:43
 */
abstract class TradeTested_Survey_Block_Adminhtml_Response_View_Tab_Abstract extends Mage_Adminhtml_Block_Template
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected $_tabTitle = '';
    /**
     * Can show tab
     *
     * @var boolean
     */
    protected $_canShowTab = true;

    /**
     * Is tab hidden
     *
     * @var boolean
     */

    protected $_isHidden = false;
    /**
     * Check if tab can be shown
     *
     * @return boolean
     */

    public function canShowTab()
    {
        return $this->_canShowTab;
    }

    /**
     * Alias for tab title
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->getTabTitle();
    }

    /**
     * Get the title for the tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('tradetested_survey')->__($this->_tabTitle);
    }

    /**
     * Always show subscriptions tab
     *
     * @return boolean
     */
    public function isHidden()
    {
        return $this->_isHidden;
    }
    /**
     * @return TradeTested_Survey_Model_Response
     */
    public function getSurveyResponse()
    {
        if (!$this->getData('survey_response')) {
            $this->setData('survey_response', Mage::registry('tradetested_survey.response'));
        }
        return $this->getData('survey_response');
    }

    /**
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return $this->getSurveyResponse()->getQuote();
    }
}
