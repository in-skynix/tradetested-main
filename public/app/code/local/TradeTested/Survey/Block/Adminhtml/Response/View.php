<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 01/12/14
 * Time: 12:47
 */
class TradeTested_Survey_Block_Adminhtml_Response_View extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected $_mode = 'view';
    protected $_blockGroup = 'tradetested_survey';
    protected $_controller = 'adminhtml_response';
    protected $_objectId = 'response_id';

    public function __construct()
    {
        parent::__construct();

        $this->_removeButton('save');
        $this->_removeButton('reset');
        $this->_removeButton('delete');
    }

    /**
     * @return TradeTested_Survey_Model_Response
     */
    public function getSurveyResponse()
    {
        if (!$this->getData('survey_response')) {
            $this->setData('survey_response', Mage::registry('tradetested_survey.response'));
        }
        return $this->getData('survey_response');
    }

    /**
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return $this->getSurveyResponse()->getQuote();
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper('tradetested_survey')->__('Survey Response From: ').
        $this->escapeHtml($this->getQuote()->getCustomer()->getName()).
        Mage::helper('tradetested_survey')->__('(').
        $this->escapeHtml($this->getQuote()->getCustomerEmail()).
        Mage::helper('tradetested_survey')->__(')');
    }
}
  