<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 28/11/14
 * Time: 16:03
 */
class TradeTested_Survey_Block_Adminhtml_Response_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Set Sort and AJAX
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('survey_responses_grid');
        $this->setDefaultSort('created_at', 'desc');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('tradetested_survey/response_collection')
            ->addQuoteAssociation(
                array('customer_email', 'is_active', 'store_id', 'customer_email', 'grand_total', 'customer_firstname', 'entity_id')
            )
            ->addBillingAddressAssociation(array('telephone', 'firstname', 'lastname'))
        ;
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('response_id', array(
            'header'    => Mage::helper('tradetested_survey')->__('ID'),
            'width'     => '100',
            'index'     => 'response_id',
        ));

        $this->addColumn('created_at', array(
            'header'    => Mage::helper('tradetested_survey')->__('Created At'),
            'index'     => 'created_at',
            'type'      => 'datetime',
        ));

        $this->addColumn('is_active', array(
            'header'    => Mage::helper('tradetested_survey')->__('Quote Active?'),
            'index'     => 'is_active',
            'type'      => 'options',
            'options'   => array(1 => 'Yes', 0 => 'No')
        ));

        $this->addColumn('customer_email', array(
            'header'    => Mage::helper('tradetested_survey')->__('Customer Email'),
            'index'     => 'customer_email',
        ));

        $this->addColumn('firstname', array(
            'header'    => Mage::helper('tradetested_survey')->__('Name'),
            'index'     => 'firstname',
        ));

        $this->addColumn('grand_total', array(
            'header'    => Mage::helper('tradetested_survey')->__('Grand Total'),
            'index'     => 'grand_total',
            'type'      => 'price',
        ));

        $storeCodes = array();
        foreach (Mage::app()->getStores() as $_store) {
            $storeCodes[$_store->getId()] = $_store->getName();
        }
        $this->addColumn('store_id', array(
            'header'    => Mage::helper('tradetested_survey')->__('Store'),
            'index'     => 'store_id',
            'type'      => 'options',
            'options'   => $storeCodes
            //'type'    => 'store' // works, but I'd rather just the code.
        ));

        if ($this->_isExport) {
            $this->addColumn('entity_id', array(
                'header'    => 'Quote ID',
                'index'     => 'entity_id',
            ));
            $this->addColumn('shipping_price_too_high', array(
                'header'    => 'Shipping Price Too High',
                'index'     => 'shipping_price_too_high',
                'type'      => 'options',
                'options'   => Mage::helper('tradetested_survey')->getYesNoMaybe(),
            ));
            $this->addColumn('shipping_price_details', array(
                'header'    => 'Shipping Price Details',
                'index'     => 'shipping_price_details',
                'type'      => 'longtext'
            ));
            $this->addColumn('shipping_options_not_suitable', array(
                'header'    => 'Shipping Options Not Suitable',
                'index'     => 'shipping_options_not_suitable',
                'type'      => 'options',
                'options'   => Mage::helper('tradetested_survey')->getYesNoMaybe(),
            ));
            $this->addColumn('shipping_options_details', array(
                'header'    => 'Shipping Options Details',
                'index'     => 'shipping_options_details',
                'type'      => 'longtext'
            ));
            $this->addColumn('payment_options_not_suitable', array(
                'header'    => 'Payment Options Not Suitable',
                'index'     => 'payment_options_not_suitable',
                'type'      => 'options',
                'options'   => Mage::helper('tradetested_survey')->getYesNoMaybe(),
            ));
            $this->addColumn('payment_options_details', array(
                'header'    => 'Payment Options Details',
                'index'     => 'payment_options_details',
                'type'      => 'longtext'
            ));
            $this->addColumn('other', array(
                'header'    => 'Other',
                'index'     => 'other',
                'type'      => 'longtext'
            ));
        }
        $this->addExportType('*/*/exportCsv', Mage::helper('tradetested_survey')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('tradetested_survey')->__('Excel XML'));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/view', array('response_id' => $row->getId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid');
    }
}