<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 28/11/14
 * Time: 16:01
 */
class TradeTested_Survey_Block_Adminhtml_Responses extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_response';
        $this->_blockGroup = 'tradetested_survey';
        $this->_headerText = Mage::helper('tradetested_survey')->__('View Survey Responses');
        parent::__construct();
        $this->removeButton('add');
    }

    public function getHeaderCssClass()
    {
        return parent::getHeaderCssClass().' head-edit-form';
    }
}