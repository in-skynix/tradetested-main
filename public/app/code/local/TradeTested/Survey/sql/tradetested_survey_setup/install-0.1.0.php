<?php
/**
 * @author Dane Lowe
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$table = $installer->getConnection()
    ->newTable($installer->getTable('tradetested_survey/response'))
    ->addColumn('response_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Response ID')
    ->addColumn('quote_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'Quote ID')
    ->addColumn('shipping_price_too_high', Varien_Db_Ddl_Table::TYPE_INTEGER, 1, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'Shipping Price Too High')
    ->addColumn('shipping_price_details', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
    ), 'Shipping Price Details')
    ->addColumn('shipping_options_not_suitable', Varien_Db_Ddl_Table::TYPE_INTEGER, 1, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'Shipping Price Not Suitable')
    ->addColumn('shipping_options_details', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
    ), 'Shipping Options Details')
    ->addColumn('payment_options_not_suitable', Varien_Db_Ddl_Table::TYPE_INTEGER, 1, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'Payment Options Not Suitable')
    ->addColumn('payment_options_details', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
    ), 'Payment Options Details')
    ->addColumn('other', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
    ), 'Other')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
        array(
            'nullable' => false,
            'default'  => Varien_Db_Ddl_Table::TIMESTAMP_INIT
        ), 'Created At')
    ->addForeignKey($installer->getFkName('tradetested_survey/response', 'quote_id', 'sales/quote', 'entity_id'),
        'quote_id', $installer->getTable('sales/quote'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)

;
$installer->getConnection()->createTable($table);
$installer->endSetup();