<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 28/11/14
 * Time: 15:48
 */
class TradeTested_Survey_Adminhtml_Survey_ResponseController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function viewAction()
    {
        $this->_initSurveyResponse();
        $this->loadLayout();
        $this->renderLayout();
    }

    public function exportCsvAction()
    {
        $fileName   = 'survey_responses.csv';
        $grid       = $this->getLayout()->createBlock('tradetested_survey/adminhtml_response_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export responses grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName   = 'survey_responses.xml';
        $grid       = $this->getLayout()->createBlock('tradetested_survey/adminhtml_response_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $this->_prepareDownloadResponse($fileName, $content, $contentType);
    }

    /**
     * @param string $idField
     * @return TradeTested_Survey_Model_Response
     */
    protected function _initSurveyResponse($idField = 'response_id')
    {
        $surveyResponse = Mage::getModel('tradetested_survey/response')->load($this->getRequest()->getParam($idField));
        Mage::register('tradetested_survey.response', $surveyResponse);
        return $surveyResponse;
    }
}