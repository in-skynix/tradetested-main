<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 06/11/14
 * Time: 15:42
 */
 class TradeTested_Survey_IndexController extends Mage_Core_Controller_Front_Action
 {
     public function incompleteAction()
     {
         $quote = Mage::helper('tradetested_survey')->getQuote($this->getRequest()->getParam('quote'));
         if ($quote && $quote->getId()) {
             Mage::getSingleton('tradetested_survey/session')->setQuoteId($quote->getId());
             $this->loadLayout();
             $this->_initLayoutMessages('tradetested_survey/session');
             $this->renderLayout();
         } else {
             $this->_redirect('*/*/success');
         }
     }

     public function incompletePostAction()
     {
         if ($this->getRequest()->getPost() && $this->_validateFormKey()) {
             try{
                 $response = Mage::getModel('tradetested_survey/response')
                     ->addData($this->getRequest()->getPost('survey'))
                     ->setQuoteId(Mage::getSingleton('tradetested_survey/session')->getQuoteId())
                     ->save();
                 Mage::helper('tradetested_survey/email')->newResponse($response)->send();
             } catch (Exception $e) {
                 Mage::getSingleton('tradetested_survey/session')->addError('Sorry, we couldn\'t save your response. Please try again');
             }
             $this->_redirect('*/*/success');
         } else {
             Mage::getSingleton('tradetested_survey/session')->addError('Sorry, we couldn\'t save your response. Please try again');
             $this->_redirect('*/*/incomplete');
         }
     }

     public function successAction()
     {
         $this->loadLayout();
         $this->_initLayoutMessages('tradetested_survey/session');
         $this->renderLayout();
     }
 }