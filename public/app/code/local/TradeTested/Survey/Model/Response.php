<?php
/**
 * Class TradeTested_Survey_Model_Response
 *
 * @method string getCreatedAt()
 * @method $this setCreatedAt(string $value)
 * @method int getResponseId()
 * @method $this setResponseId(int $value)
 * @method int getQuoteId()
 * @method $this setQuoteId(int $value)
 * @method int getShippingPriceTooHigh()
 * @method $this setShippingPriceTooHigh(int $value)
 * @method string getShippingPriceDetails()
 * @method $this setShippingPriceDetails(string $value)
 * @method int getShippingOptionsNotSuitable()
 * @method $this setShippingOptionsNotSuitable(int $value)
 * @method string getShippingOptionsDetails()
 * @method $this setShippingOptionsDetails(string $value)
 * @method int getPaymentOptionsNotSuitable()
 * @method $this setPaymentOptionsNotSuitable(int $value)
 * @method string getPaymentOptionsDetails()
 * @method $this setPaymentOptionsDetails(string $value)
 * @method string getOther()
 * @method $this setOtheelect (string $value)
 */
class TradeTested_Survey_Model_Response extends Mage_Core_Model_Abstract
{
    const CACHE_TAG     = 'tradetested.survey.response';
    protected $_cacheTag= 'tradetested.survey.response';
    protected function _construct()
    {
        $this->_init('tradetested_survey/response');
    }

    /**
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        if (!$this->getData('quote')) {
            $this->setData('quote', Mage::getModel('sales/quote')->loadByIdWithoutStore($this->getQuoteId()));
        }
        return $this->getData('quote');
    }
}