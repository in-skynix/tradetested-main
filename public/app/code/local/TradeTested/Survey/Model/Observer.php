<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 02/12/14
 * Time: 16:09
 */
class TradeTested_Survey_Model_Observer
{
    public function addSurveyUrl($observer)
    {
        $variables = $observer->getEvent()->getVariables();
        if ($quote = $variables->getData('quote')) {
            $variables->setData('survey_url', Mage::helper('tradetested_survey')->getSurveyUrl($quote));
        }
    }
}