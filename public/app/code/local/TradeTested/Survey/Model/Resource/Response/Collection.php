<?php
class TradeTested_Survey_Model_Resource_Response_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('tradetested_survey/response');
    }

    public function addQuoteAssociation($cols = '*')
    {
        return $this->join(
            array('quote' => 'sales/quote'),
            'main_table.quote_id = quote.entity_id',
            $cols
        );
    }

    public function addBillingAddressAssociation($cols = '*')
    {
        $this->getSelect()->joinLeft(
            array('address' => $this->getTable('sales/quote_address')),
            "address.quote_id = quote.entity_id AND address.address_type = 'billing'",
            $cols
        );
        return $this;
    }
}