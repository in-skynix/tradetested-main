<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 02/12/14
 * Time: 16:35
 */
class TradeTested_Survey_Helper_Email extends Mage_Core_Helper_Abstract
{
    const XML_PATH_NEW_RESPONSE_EMAIL_TEMPLATE = 'tradetested_survey/email/new_response_email_template';

    public function newResponse(TradeTested_Survey_Model_Response $response)
    {
        $storeId = $response->getQuote()->getStoreId();
        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo(
            Mage::getStoreConfig('tradetested_survey/email/recipient_email', $storeId),
            Mage::getStoreConfig('tradetested_survey/email/recipient_name', $storeId)
        );
        $mailer->addEmailInfo($emailInfo);
        // Set all required params and send emails
        $mailer->setSender(array(
            'email' => Mage::getStoreConfig('trans_email/ident_general/email', $storeId),
            'name'  => Mage::getStoreConfig('trans_email/ident_general/name', $storeId)
        ));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId(Mage::getStoreConfig(self::XML_PATH_NEW_RESPONSE_EMAIL_TEMPLATE));
        $mailer->setTemplateParams(
            array(
                'response'  => $response,
                'quote'     => $response->getQuote(),
            )
        );
        return $mailer;

    }
}