<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 28/11/14
 * Time: 15:36
 */
class TradeTested_Survey_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Create a semi-secure hash representing a quote
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return string
     */
    public function hash(Mage_Sales_Model_Quote $quote)
    {
        $ext = substr(preg_replace("/[^a-zA-Z0-9]+/", "", $quote->getId()), -2);
        return strrev(base64_encode($quote->getId())).$ext;
    }

    /**
     * Get Quote ID From Hash
     *
     * @param $hash
     * @return string
     */
    public function unhash($hash)
    {
        return base64_decode(strrev(substr($hash, 0 ,-2)));
    }

    /**
     * Validate Hash and get Quote Object
     *
     * @param $hash
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote($hash)
    {
        $id = $this->unhash($hash);
        $ext = substr($hash, -2);
        $quote = Mage::getModel('sales/quote')->loadByIdWithoutStore($id);
        if (substr(preg_replace("/[^a-zA-Z0-9]+/", "", $quote->getId()), -2) == $ext) {
            return $quote;
        }
    }

    /**
     * Get Survey URL for Quote
     *
     * @param $quote
     * @return string
     */
    public function getSurveyUrl(Mage_Sales_Model_Quote $quote)
    {
        return $quote->getStore()->getUrl('survey/index/incomplete', array('quote' => $this->hash($quote)));
    }

    /**
     * Quick get options, or option
     *
     * @param null|int $int
     * @return array|string
     */
    public function getYesNoMaybe($int = null)
    {
        $options = array(1 => 'Yes', 0 => 'No', 2 => 'Maybe');
        return $int === null ? $options : $options[$int];
    }
}