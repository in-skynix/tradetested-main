<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 19/04/13
 * Time: 4:59 PM
 * To change this template use File | Settings | File Templates.
 */
class TradeTested_Home_Block_Categories extends Mage_Core_Block_Template
{
    protected $_sections = array();
    public function getSections($limit = null)
    {
        $storeRootId = Mage::app()->getStore()->getRootCategoryId();
        if (!count($this->_sections)) {
        /** @var  Mage_Catalog_Model_Resource_Category_Collection $categories */
        $categories = Mage::getModel('catalog/category')->getCollection()
            ->addPathsFilter("1/{$storeRootId}/")
            ->setStore(Mage::app()->getStore())
            ->addAttributeToFilter('show_on_home_page', array('eq' => 1))
            ->addAttributeToSelect(array('name', 'name_on_home_page', 'image', 'thumbnail', 'meta_title'))
            ->addIsActiveFilter()
            ->addUrlRewriteToResult()
            ->setOrder('sort_order_on_home_page', 'asc')
            ;
            $categories->getSelect()->joinLeft(
                array('categories_revenue' => $categories->getTable('tradetested_catalog/index_category_revenue')),
                'e.entity_id = categories_revenue.category_id'
            )->order('categories_revenue.revenue DESC');
        if ($limit) {
            $categories->setPage(1, $limit);
        }
        foreach ($categories as $category)
            $this->_sections[] = Mage::getModel('tradetested_home/categories_section')->setCategory($category);
        }
        return $this->_sections;
    }
}