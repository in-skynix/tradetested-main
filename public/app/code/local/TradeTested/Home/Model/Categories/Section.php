<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 19/04/13
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */
class TradeTested_Home_Model_Categories_Section extends Mage_Core_Model_Abstract
{
    protected $_subSections = array();

    public function getName()
    {
        return $this->getCategory()->getNameOnHomePage() ? $this->getCategory()->getNameOnHomePage() : $this->getCategory()->getName();
    }
    public function getUrl()
    {
        return $this->getCategory()->getUrl();
    }
    public function getImage($width, $height = null)
    {
        //@todo: move this into Avid_Thumbor?
        $height = $height ? $height : $width;
        $url = Mage::helper('avid_thumbor')->getUrl("catalog" . DS . "category" . DS . $this->getCategory()->getThumbnail())
            ->fitIn($width, $height)
            ->addFilter('no-upscale')
//            ->addFilter('fill', 'white')
            ;
        return $url->__toString();
    }
    public function getSubSections($limit = 5)
    {
        if (!count($this->_subSections)) {
            $subCategories = $this->getCategory()->getCollection();
            /* @var $collection Mage_Catalog_Model_Resource_Category_Collection */
            $subCategories->addAttributeToSelect('url_key')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('all_children')
                ->addAttributeToSelect('is_anchor')
                ->addAttributeToFilter('is_active', 1)
                ->addIdFilter($this->getCategory()->getChildren())
                ->joinUrlRewrite()
                ->addIsActiveFilter()
                ->setProductStoreId(Mage::app()->getStore()->getId())
                ->setOrder('sort_order_on_home_page', 'asc')
                //->setLoadProductCount(true) //Seems that the collections loadProductCount is N+1 anyway, and doesn't take into account out of stock products as our overloaded method on the category model does.
            ;
            foreach ($subCategories as $category) {
                if($category->getProductCount()) $this->_subSections[] = Mage::getModel('tradetested_home/categories_section')->setCategory($category);
            }
        }
        return array_slice($this->_subSections, 0, $limit);
    }
}