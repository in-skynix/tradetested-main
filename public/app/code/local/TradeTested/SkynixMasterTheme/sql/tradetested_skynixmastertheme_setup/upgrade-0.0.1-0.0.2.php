<?php

/** @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;
$connection = $installer->getConnection();

$homePage = Mage::getModel('cms/page')->load('home', 'identifier');
$homePage->setContent("<div></div>")->save();

$installer->endSetup();