<?php

/** @var  $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$pages = [

    [
        'identifier'    => 'buying-guides',
        'content'       => '
            <div class="buying-guides-main right-section cms not-selected">
                {{block type="core/template" name="buyingHead" template="cms/buyinghead.phtml"}}
            </div>
        '
    ],
    [
        'identifier'    => 'brushcutter-buying-guide',
        'content'       => '<div class="buying-guides-main right-section cms">
                                {{block type="core/template" name="buyingHead" template="cms/buyinghead.phtml"}}
                                <div class="buying-guide-container">
                                    <div class="guides-section">
                                        <h1>Brush Cutter Buying Guides</h1>
                                        <p>Brush cutters offer excellent flexibility. They&rsquo;re made to be great at trimming the backyard, clearing bush or undergrowth, or even cutting down saplings. Strong and robust, this is the perfect machine for your backyard maintenance.&nbsp;</p>
                            
                                        <h2>What\'s Right for You?</h2>
                            
                                        <p>When choosing a reliable brush cutter, stick with the petrol models. Electric are fine for trimming a small suburban yard, but won&rsquo;t be able to tackle tougher tasks. Depending on the job, you can switch out the tool being used. All our brush cutters come with a harness, so that no matter how much work you need to do, you&rsquo;ll have as little stress on your back as possible.&nbsp;&nbsp;</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Attachments</h2>
                            
                                        <p><strong>Nylon Trimmer &ndash; </strong>Great when putting the final touches on a mown lawn. Perfect for trimming and thinning small areas of grass and working around obstacles.</p>
                            
                                        <p>&nbsp;</p>
                            
                                        <p><strong>3 Prong Blade</strong> <strong>&ndash;</strong> Great for trimming hard to reach places, mowing large areas of long grass and cutting through small batches of heavy grass. Also good for clearing out tough undergrowth and light vegetation.</p>
                                        <p>&nbsp;</p>
                                        <p><strong>4 Tooth Blade &ndash; </strong>It&rsquo;s sturdy wide teeth are perfect for mowing through a large amount of ferns and dry and matted grass in difficult conditions.</p>
                                        <p><strong>&nbsp;</strong></p>
                                        <p><strong>8 Tooth Blade &ndash; </strong>Eight cutters made for mowing heavy vegetation or for tough grass that the other blades may be having trouble with. Good for cutting down light bush and stinging nettles.</p>
                                        <p>&nbsp;</p>
                                        <p><strong>40 Tooth Blade &ndash; </strong>This is the tool we recommend for sawing through vines, saplings and clearing knotty brush. Should only be sharpened by a professional.</p>
                                        <div>&nbsp;</div>
                                        <div>
                                        </div>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Safety</h2>
                                        <p><strong>&bull; &nbsp;&nbsp;</strong>Always be sure to wear sturdy and close-fitted clothing. Protect your legs with tough trousers and your feet with strong shoes that won&rsquo;t slip.</p>
                                        <p><strong>&bull; &nbsp; </strong>Never wear a scarf, tie or jewellery that may catch. Always make sure to tie your hair back if you have long hair.</p>
                                        <p><strong>&bull; &nbsp; </strong>Protect your eyes with safety goggles or a face mask.</p>
                                        <p><strong>&bull; &nbsp;&nbsp;</strong>Keep your hearing safe and wear ample ear protection.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Maintenance</h2>
                                        <p><strong>&bull; &nbsp;&nbsp;</strong>Use the best 2-cycle oil, designed for air cooled engines.</p>
                                        <p><strong>&bull; &nbsp;&nbsp;</strong>You can buy&nbsp;<a href="/browse/outdoor-power-garden/brush-cutters/blades-nylon.html"><span style="text-decoration: underline;">back-up or replacement attachments</span></a>&nbsp;from us.</p>
                                        <p><strong>&bull; &nbsp;&nbsp;</strong>Before putting your machine into storage, clean it thoroughly and then drain the tank completely.</p>
                                        <p><strong>&bull; &nbsp;&nbsp;</strong>Remove the metal cutting blade and check it for any damage. Clean it and protect if from corrosion.</p>
                                        <p><strong>&bull; &nbsp;&nbsp;</strong>To keep the nylon line elastic, remove the spool from the mowing head and store it in a damp place. A few days before you use it, place the line in water to keep it flexible.</p>
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Still need more help?</h2>
                                        <p>Either&nbsp;<a href="/contact-us">send us an email</a>&nbsp;or give us a bell during office hours on {{config path="general/store_information/phone"}}.</p>
                                        <p>&nbsp;</p>
                                        <h2><span style="text-decoration: underline;"><strong><a href="/browse/outdoor-power-garden/brush-cutters.html">Browse Brush Cutters</a></strong></span></h2>
                                    </div>
                                </div>
                            </div>
                            <div class="button-section">
                                <a href="/chainsaw-buying-guide" class="button-cms next">Chainsaw buying guide</a>
                            </div>'
    ],
    [
        'identifier'    => 'chainsaw-buying-guide',
        'content'       => '<div class="buying-guides-main right-section cms">
                                {{block type="core/template" name="buyingHead" template="cms/buyinghead.phtml"}}
                                <div class="buying-guide-container">
                                    <div class="guides-section which-chainsaw">
                                        <h1>Chainsaw buying guide</h1>
                                        <p>There\'s no need to overcomplicate things when choosing the chainsaw that\'s right for you. A quality chainsaw is reliable and durable and does the simple things right. It should start easily every time, feel balanced in your hands, and give you years of quality performance.</p>
                                        <h2>Which chainsaw is right for me?</h2>
                                        <p>The main things to consider when choosing a chainsaw are the power, the weight, and the length of the bar. If you\'ve got a clear idea of what you\'ll be using it for, selecting the right one should be a pretty straightforward process.
                                        </p>
                                        <p>These features in a nutshell:</p>
                                        <h3>1. Bar Length</h3>
                                        <p>This is the length of the bar that the chain runs around. It generally indicates the active cutting area, the largest piece of wood the chainsaw will cut through in a single pass. Typically a longer bar length comes with more power and weight.
                                        </p>
                                        <h3>2. Power / Engine Size</h3>
                                        <p>Harder wood requires more power to cut through it, and a longer bar requires more power to run the chain around it. Typically a higher power rating results in a heavier chainsaw.</p>
                                        <h3>3. Weight</h3>
                                        <p>Weight usually increases with both bar length and power. The heavier the chainsaw the more tiring it’ll be to hold; and generally lighter chainsaws are easier to control.</p>
                                        <p>Because added power and bar length come at a cost (a higher weight) you need to make a decision as to which chainsaw suits you based on what you’ll be using it for.</p>
                                        <img src="{{media url="wysiwyg/buying_guides/guides_table.png"}}" alt="" />
                            
                                    </div>
                                    <div class="guides-section chainsaw-features">
                                        <h2>Other features to consider:</h2>
                                        <p>There are a few basic features that come standard with all Trade Tested chainsaws:</p>
                                        <ul>
                                            <li><span>Vibration dampening</span> takes the muscle work out of using a chainsaw so you can cut more wood for longer without getting tired.</li>
                                            <li><span>Automatic bar oiling</span> keeps the chain-to-bar friction to a minimum and enhances the lifespan of your engine. It keeps the chain running smoothly and makes it less likely to break. It also takes the effort out of maintenance.</li>
                                            <li>An <span>inertia activated chain brake</span> feature is essential for safe chainsaw use, and is now a legal requirement for all new chainsaws in New Zealand. Sometimes during a cut the bar will kick back when it hits something hard like a knot. The chain brake makes sure the chain stops running during a kickback, so you don\'t seriously injure yourself.</li>
                                        </ul>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Safety</h2>
                                        <p>It\'s better to be safe than sorry, especially with chainsaws. One American study found that the average chainsaw injury requires 110 stitches. If you\'re safe you can avoid looking like Frankenstein\'s monster or a spontaneous amputee, so consult our
                                            <a href="#">safety guide</a> to avoid making any mistakes</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Maintenance</h2>
                                        <p>Good maintenance will extend the chainsaw\'s life and make it safer to use. When you finish a job, make sure the air filters, sprocket cover and chain brake mechanism are free from sawdust; clean the guide bar groove; oil the holes and check everything is in place.</p>
                                        <p>When filling up your chainsaw, stick to the specified petrol to oil ratio (Trade Tested\'s chainsaws use an oil to petrol ratio of 1:25).</p>
                                        <p>Keep your chain sharp. It makes cutting easier, puts less strain on you and the chainsaw, and increases chain and engine longevity. We strongly recommend buying a chainsaw sharpener for your chainsaw and using it regularly.</p>
                                    </div>
                                    <div class="guides-section buying-question">
                                        <h2>Got a Question?</h2>
                                        <a href="#">Send us an email</a>
                                        <span> or give us a bell during office hours on</span> <span>{{config path="general/store_information/phone"}}</span>
                                        <a href="#" class="trade-button">View Chainsaws</a>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="button-section">
                                <a href="/brushcutter-buying-guide" class="button-cms prev">Brush cutter buying guide</a>
                                <a href="/chickencoop-buying-guide" class="button-cms next">Chicken coop buying guide</a>
                            </div>'
    ],
    [
        'identifier'    => 'chickencoop-buying-guide',
        'content'       => '<div class="buying-guides-main right-section cms">
                                {{block type="core/template" name="buyingHead" template="cms/buyinghead.phtml"}}
                                <div class="buying-guide-container">
                                    <div class="guides-section">
                                        <h1>Chicken Coop Buying Guide</h1>
                                        <p>The soft clucking of hens is becoming a familiar sound in the suburbs of Australian and New Zealand cities. More and more people are getting wise to the fact that owning chickens isn\'t just the cheapest way to get eggs, it also provides the freshest, best quality eggs in the most humane way.</p>
                                        <p>Keeping chickens is easy and rewarding. It\'s also a fun way to bring the family together. Chickens make excellent pets, being the curious, quirky creatures that they are. So don\'t be surprised if you find yourself getting attached to your chooks. Or if they get attached to you.</p>
                                        <p>Chickens don\'t just lay eggs. They do a great job of generating high-grade compost and fertilizer. They\'re even able to weed your lawn or your vegetable patches (just keep an eye on them to make sure they\'re only grazing on what you want them to).</p>
                                        <p>Even though keeping chickens is a relaxing, low maintenance activity, there are a few simple things to bear in mind. This guide will cover all the basics to make sure you do right by your chooks. They\'ll be sure to notice, and you\'ll be rewarded with lots of fresh delicious eggs.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>The Coop</h2>
                                        <p>The best coops provide a good environment for chickens to sleep, lay eggs, feed, and generally feel at home in.</p>
                                        <p>Trade Tested\'s chicken coops are made from high-grade, treated Russian fir, kiln dried for 72 hours. This sturdy wood ensures chickens are kept warm, dry, and protected from both the elements and from predators. The run areas are contained within corrosion-free mesh walls, giving extra protection from pests and hungry intruders. All our coops have elevated henhouses, which maximises the run space and offers chooks a shady outdoor area to escape the sun. Our coop designs focus on easy egg collection and hassle-free maintenance.</p>
                                        <p>Having enough space is important to a chicken. The SPCA recommends that you allow for a minimum of 2ft&sup2; per chicken. But if you want to make sure that you have the healthiest and happiest chickens, we strongly suggest that you leave 3ft&sup2; for each bird.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>The Run</h2>
                                        <p>Consider where the chickens are going to strut around. Smaller coops are not designed to be 24 hour living spaces for chickens. Trade Tested\'s larger coops, however, have big enough runs to keep chooks happy without feeling cooped in.</p>
                                        <p>A well-planned run can reap a lot of benefits. Roaming chooks will fertilize your lawn and garden. They\'ll scratch and turn the soil in the garden, keeping it aerated. And they\'ll do an excellent job of keeping weeds under control.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>The Chooks</h2>
                                        <p>There are two main choices in choosing chooks. You can either go for commercial hybrids (the brown shavers) or heritage breeds. If your main reason for getting chickens is eggs, then go for the brown shavers. These chooks are thoroughbred egg-laying machines (they\'ll lay an egg a day if properly cared for). They also have a gentle temperament, and make great pets for kids.</p>
                                        <p>Heritage breeds are a good choice for people whose priority is more chicken than egg. They won\'t lay eggs year round and can be less tame than brown shavers, but they\'re big on personality.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Feed</h2>
                                        <p>It\'s best to feed your chooks a mix of kitchen scraps and commercial layer pellet. You should also feed them a small amount of finely crushed oyster shells, which provides calcium for strong eggshells and helps them digest their food. Chickens don\'t overfeed, so if you make sure there\'s always a ready supply of food, they\'ll take care of the portions themselves. As a rule of thumb, we recommend feeding chickens 200 grams of feed per day.</p>
                                        <p>A Chooketeria allows the chickens to feed at any time without assistance. The feeder closes when the chicken is done, keeping the food dry from rain and safe from rodents. Remember, the more chooks you have the more competition they face when eating. Consider using the Chooketeria All Rounder or more than one feeder to make sure that they all have a reliable and healthy food source.</p>
                                        <p>Chickens also drink a lot of water, so it\'s really important to make sure they always have a fresh supply. This should be changed daily.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Maintenance</h2>
                                        <p>Trade Tested\'s coops are easy to clean. Nesting areas are fitted with a removable tray that should be taken out and cleaned several times a year. The run areas can be cleaned less frequently. Sweep the coop out, wash it with a high power hose, leave it to dry, and put in some clean shavings. You should never let your coop get smelly.</p>
                                        <p>Mite killing sprays such as Poultry Shield are excellent for good coop maintenance. A red mite infestation can be a real pain. It\'s best avoided by spraying your coop each time you clean it.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>New Hens / Pecking Order</h2>
                                        <p>Chickens are conservative creatures of habit and live by a strict pecking order. They get stressed out when the pecking order gets disturbed, which happens when new chickens are introduced to the brood. A certain amount of conflict is necessary to establish a functioning chicken society, but you want to make sure things don\'t get out of hand.</p>
                                        <p>When introducing new chickens, it\'s best to introduce more than one at a time. Introduce new chooks to the existing brood in stages. Start by putting the new hens within sight of the old ones, but keep them physically separated with a mesh fence or a different enclosure. When you first put the new hens in the coop, do it at night (they don\'t like to fight at night) and put them directly amongst the original hens. Don\'t interfere with a moderate amount of argy bargy, but if blood gets drawn then remove the injured hen and try again after the wound has healed. It\'s sometimes a good move to isolate the most aggressive original hen, then reintroduce her once all the other hens, old and new, have established a new pecking order.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Got a Question?</h2>
                                        <p>Either <a href="/contact-us">send us an email</a> or give us a bell during office hours on {{config path="general/store_information/phone"}}.</p>
                                        <h2><a href="/browse/chicken-coops.html"><span><strong>Browse Chicken Coops</strong></span></a></h2>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="button-section">
                                <a href="/chainsaw-buying-guide" class="button-cms prev">Chainsaw buying guide</a>
                                <a href="/chipper-buying-guide" class="button-cms next">Chipper / Shredder buying guide</a>
                            </div>'
    ],
    [
        'identifier'    => 'chipper-buying-guide',
        'content'       => '<div class="buying-guides-main right-section cms">
                                {{block type="core/template" name="buyingHead" template="cms/buyinghead.phtml"}}
                                <div class="buying-guide-container">
                                    <div class="guides-section">
                                        <h1>Chipper Shredder Buying Guide</h1>
                                        <h2>So You\'re Thinking of Getting a Chipper Shredder</h2>
                                        <p>If you spend a lot of time dealing with sticks, leaves, twigs, and branches, then a chipper shredder is an essential tool for your backyard.</p>
                            
                                        <p>Chipper shredders eliminate the need for hacking, stacking, burning and breaking. You just feed it all into the chute, then watch huge piles of branches and leaves quickly turn into small mounds of useful mulch. These awesome machines can turn twelve bags of garden waste into a single bag of chips!</p>
                            
                                        <p>Chipper shredders aren\'t just ace at dealing to waste. Essentially two machines in one, the wood chipper makes uniformly sized wood chips, and the leaf shredder makes nutrient rich green mulch. Both the mulch and the chips (contrary to popular belief) make excellent additions to your compost and increase the fertility of your soil.</p>
                            
                                        <p>If you regularly use a garden waste collection service, you\'ll save money in the long run by getting your own chipper shredder. You can keep your property tidy, save space in landfills, and use the mulch to  make your garden thrive. Why hire one, when for not much more, you can have one of your very own.</p>
                                    </div>
                                    <div class="guides-section">
                            
                                        <h2>Which Size is Right for You?</h2>
                                        <p>While there are electric chippers on the market, they won\'t do the trick when it comes to thicker sticks. Petrol and diesel chippers are far more effective for the kinds of debris that will inevitably come your way.</p>
                            
                                        <p>For smaller jobs around the house, a petrol model under 10HP is a good choice. They\'re easy to manoeuvre and relatively light. If your main focus is on shredding leaves and chipping branches less than 3” thick, these models will do the job nicely.</p>
                            
                                        <p>Bigger jobs need bigger chippers. If you need to chip branches that are thicker than 3” in diameter, or if you need to deal to tougher material like flax, palm fronds, and heavily pruned fruit tree branches, you should take a look at a petrol chipper over 10HP, or a diesel model. These pack a tonne of grunt, as seen on Trade Tested\'s world famous Will it Chip <a href="https://www.youtube.com/user/TradeTested"><span style="text-decoration: underline;">(watch it here)</span></a>. Larger chippers usually come with a handy ATV / trailer towing hitch, perfect for moving them around bigger properties.</p>
                            
                                        <p>All our chippers feature an oversize funnel, which makes them very speedy leaf shredders.</p>
                                    </div>
                                    <div class="guides-section">
                            
                                        <h2>Features that Make All the Difference</h2>
                                        <li>Reliable Motor: To extend engine life and ensure smooth running, make sure the chipper has  dual ball bearings, oil level centres, and an OHV design.</li>
                            
                                        <li>Self-feeding: Chippers are easier to use when you don\'t have to push your wood and leaves into the chute.</li>
                            
                                        <li>Easy starting: An electric ignition makes starting your chipper especially easy.</li>
                            
                                        <li>Multiple chipping blades: the more blades, the more effective the chipping action. All Trade Tested chippers feature either a single, double-edged blade, or multiple single-edge blades.</li>
                            
                                        <li>Accessories: A safety kit including goggles, earmuffs and gloves should come standard with all chippers. Tow hitches are also very useful for larger chippers.</li>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Chip Safe</h2>
                                        <p>There are a few simple things you can do to avoid any chipping mishaps. Always check the chutes to make sure nothing\'s fallen in between chips. Avoid wearing baggy clothing that could get caught, and use goggles and earmuffs to protect your eyes and ears. While chipping, keep your body, and especially your head, clear of openings incase the chipper kicks back some debris.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Other Things to Think About</h2>
                            
                                        <li>Deciduous trees drop more leaves. If you have a lot of these consider getting a bigger shredder.</li>
                            
                                        <li>While wood chips can make a fantastic addition to your compost, there\'s an art to getting the best out of them. Rather than putting them into your compost fresh from the chipper, you should leave them to decompose until they\'ve turned dark brown. That way, they won\'t soak up precious nitrogen. Wood chips are one of the best things you can add to compost to improve your soil\'s texture.</li>
                            
                                        <li>Chipper shredders are useful year-round. They\'re good in autumn to deal with falling leaves, in winter and spring to deal with storm debris, and in summer to chew through weeds and finished crops.</li>
                                    </div>
                                    <div class="guides-section">
                            
                                        <h2>Got a Question?</h2>
                                        <p>Either&nbsp;<a href="/contact-us">send us an email</a>&nbsp;or give us a bell during office hours on {{config path="general/store_information/phone"}}.</p>
                            
                                        <p><strong style="font-size: 1.5em;"><a href="/browse/chippers.html">Browse Chippers</a></strong></p>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="button-section">
                                <a href="/chickencoop-buying-guide" class="button-cms prev">Chicken coop buying guide</a>
                                <a href="/garden-shed-buying-guide" class="button-cms next">Garden Shed buying guide</a>
                            </div>'
    ],
    [
        'identifier'    => 'garden-shed-buying-guide',
        'content'       => '<div class="buying-guides-main right-section cms">
                                {{block type="core/template" name="buyingHead" template="cms/buyinghead.phtml"}}
                                <div class="buying-guide-container">
                                    <div class="guides-section">
                                        <h1>Garden Shed Buying Guide</h1>
                                        <p>The New Zealand lifestyle is busy, whether walking through the native bush, messing around on the water or completing the weekend DIY list. All of our activities and hobbies tend to mean that we collect a lot of stuff, from gardening gear to sports equipment, paddleboards to mountain bikes.  To store and protect everything, you’re going to need a spacious, stylish garden shed.</p>
                            
                                        <h2>Why Kitset Sheds?</h2>
                            
                                        <p>Our kitset sheds are a simple, cost-effective solution for all your storage needs.  Unlike custom-built sheds, we’ve got the kits already made and packed up, ready for your order, so we can get your new shed to you quickly and at a fraction of the price.</p>
                            
                                        <p>Some kitset sheds are made up of many small pieces, which are tedious and time-consuming to put together.  Ours are in as few pieces as possible, to make the assembly process fast and straightforward.</p>
                            
                                        <p>All Trade Tested sheds come with a 20 year warranty, so you can be sure you’re getting a high quality shed that will last you many years to come.</p>
                            
                                    </div>
                                    <div class="guides-section">
                                        <h2>Purpose</h2>
                            
                                        <p>Step one is to figure out what you will be using the shed for.  We have lots of models available for a huge variety of different purposes, from storing garden tools and supplies, to housing bikes, kayaks and other outdoor gear, to providing workshop or hobby space.  If you need it for storage, will your new shed be your only storage area, or will it be used alongside a garage space or basement?  If you’ll be working in there too, how much time will you spend in there, and what facilities will you need?  Here’s some examples of what your shed space might look like.</p>
                            
                                        <p>Our sheds come in a range of colours and styles to suit the design and character of your outdoor living area.</p>
                            
                                        <p>Once you’re clear on your shed’s purpose, you can get started on choosing specifications.</p>
                            
                                    </div>
                                    <div class="guides-section">
                                        <h2>Choosing a Shed</h2>
                            
                                        <p>Find your perfect shed in five easy steps:</p>
                            
                                        <style>
                                            .demo {
                                                border:1px solid #C0C0C0;
                                                border-collapse:collapse;
                                                padding:5px;
                                            }
                                            .demo th {
                                                border:1px solid #C0C0C0;
                                                padding:5px;
                                                background:#F0F0F0;
                                            }
                                            .demo td {
                                                border:1px solid #C0C0C0;
                                                padding:5px;
                                            }
                                        </style>
                                        <table class="demo">
                                            <tbody>
                                            <tr>
                                                <td>&nbsp;1</td>
                                                <td>&nbsp;Choose a Size</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;2</td>
                                                <td>&nbsp;Select your Material</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;3</td>
                                                <td>&nbsp;Pick a Roof Style</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;4&nbsp;</td>
                                                <td>&nbsp;Choose your Colour</td>
                                            </tr>
                                            <tbody>
                                        </table>
                            
                                        <br />
                            
                                        <h3>1. Size</h3>
                            
                                        <p>Decide how much of your backyard you are willing to devote to the shed.  If your property is small, this might be limited by space.  Even on a larger property, you might want to leave enough lawn area for kids and pets, or even a space to put some outdoor furniture.</p>
                            
                                        <p>Of course, you also need to know how much you need to store in your shed.  Besides what you’re planning to put in your shed immediately, think about what you’ll want to use it for in the future.  As your family expands, your kids grow up, or you take on new hobbies, your storage needs will probably increase.  Sheds that are properly cared for can last decades and you don’t want to have to replace your shed a year or two down the track because you’ve run out of room.  It’s best to opt for a bigger shed than you think you need right now.  Trade Tested’s sheds have floor areas ranging from 0.8m2 to 14.8m2.</p>
                            
                                        <p>As well as floor area, internal shed height is particularly important if you’re planning to use it as a workshop or if you want to add some shelving. How many shelves do you need and what height should these sit at?  If you’re planning to work inside your shed, you’ll need to make sure the roof is high enough for you to comfortably stand up in.  We have some larger sheds up to 2.1m high that might suit.</p>
                            
                                        <p>Some of our larger models have double doors, while our smaller ones tend to have single doors.  Ease of access might be an important consideration, especially if you’re storing large or bulky items.  As well as fitting into the shed itself, your items – and the people moving them – need to be able to safely fit through the door.</p>
                            
                                        <br />
                            
                                        <h3>2. Material</h3>
                            
                                        <strong>Steel</strong>
                                        <p>Most sheds in New Zealand are made from steel, mainly because it is light, easy to install, and it doesn’t warp or fade over time.  Membranes over steel frames give a shed that is robust and rigid, while easy to assemble.  For the conditions we experience in our country, metal sheds really do stack up.  The strength and corrosion resistance of galvanised steel means your shed will stand the test of time, without the need for much maintenance at all.  In fact, all our metal sheds come with a 20-year ‘no rust’ warranty.</p>
                            
                                        <strong>Wood</strong>
                                        <p>Wooden sheds have a classic look and an aesthetic appeal that can’t be underestimated.  Sitting in your garden or your back lawn, the shed will not only be a functional space, but it will look gorgeous and add to the overall style of your property.</p>
                            
                                        <p>Wooden sheds require some occasional maintenance, otherwise the finish of the shed can deteriorate over time. Put some thought into your thirst for DIY and how much time you have to spend keeping it up to scratch.  It won’t take a lot of your time but it will be another addition to your to-do list.  Wooden sheds also tend to command a little more in price, however getting the look you want and having something a little special is a worthwhile investment.</p>
                            
                                        <strong>Resin Plastic</strong>
                                        <p>Resin sheds are the ultimate in lightweight and durability – the material does not fade or crack and the ability to keep everything dry is a testament to the weather-tightness of resin construction.  A variety of colours are available, with new shades being released all the time.  The easy-to-manufacture nature of resin means there is also a huge range of configurations and sizes, allowing you to choose the right shed for your particular needs.  Remember that the need for a weather tight and durable shed in New Zealand conditions is paramount and resin might be the best material to stand up to the rigours of four seasons in one day!</p>
                            
                                        <br />
                            
                                        <h3>3. Roof Style</h3>
                            
                                        <p>Pitched roof sheds are great for storing tall items like skis or surfboards. They also work well as workshops, providing comfortable standing room in the centre. Flat roof sheds are ideal if you have limited vertical space to put them in.  A flat roof is still slightly sloped, to allow water to run off.  Neither of our roof styles include a gutter, so you won’t have to spend time cleaning algae or leaves out of a shed gutter.</p>
                            
                                        <br />
                            
                                        <h3>4. Colour</h3>
                            
                                        <p>When choosing the right colour, consider the surrounding landscape and how you want the shed to blend in. Some people like their shed to be subtle and inconspicuous, while some make it a bold focal point of their garden.  Cream or white may be attractive in a tidily manicured, landscaped garden, perhaps alongside a patio or water feature.  Earthy tones, are nicely suited to gardens with a softer, more naturalistic feel, and green is often a good colour choice if you’re putting your shed in amongst leafy flora.  Dark, cool colours have a more modern or bold feel, and could suit a more minimalist garden design, with lots of straight edges.  Check out <a href="/shedmaster-gallery">some photos</a> of our sheds in other gardens, and think about which colour and style would fit best with the current design of your garden.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>How to Choose a Floor</h2>
                            
                                        <p>Every shed needs a suitable base to help hold it down and to keep what’s in your shed off the ground.  There’s a few options as far as what to make your shed floor from – timber, a steel raised floor foundation kit or a concrete pad might suit you. This will mostly depend on what’s already on the site your shed is going on, what you need from your shed and the level of cost and effort involved.</p>
                            
                                        <p>Securing your shed to a fixed, level base will help prevent the frame bending and maintain its strength and stability in bad weather.  All sheds come with free anchor kits to secure your shed to a timber or concrete base.</p>
                            
                                        <strong>Timber</strong>
                            
                                        <p>Timber is the most popular option for shed floor construction in New Zealand, and for good reason.  Any way you look at it, a wooden foundation and floor is a strong and versatile platform from which to build your dream shed. Using timber flooring is also a good option if you may ever need to move the shed to a different location.</p>
                            
                                        <p>With timber floors it is important that you allow for windy conditions by securing the wooden floor to the ground and the use of pegs will accomplish this nicely.</p>
                            
                                        <p>If you have plenty of timber on hand, you can make the bearers and floorboards of your shed floor yourself by cutting them to the right size.  This way you can choose your favourite timber and finish it with a varnish, stain or paint if you like. If you’d prefer a ready-to-build floor, we have floor kits available for each size of shed.  These come with the bearers and floorboards cut to the right size, along with all the nails and nail plates you need to put your floor together.  Our floor kits are made locally from FSC certified New Zealand pine from Rotorua and are pre-treated to prevent moisture damage.</p>
                            
                                        <strong>Raised floor foundations</strong>
                            
                                        <p>An easy and cost effective alternative is adding floor to steel raised flooring foundations.  After constructing the steel foundation frame, you cover it with timber or plywood.  The frame ensures that your actual floor is off the ground and is designed to fit with your shed. A great method for attaining a flat surface, this option provides flexibility for your choice of based surfaces.  It can be ideal when you are setting the shed up on an existing hard surface e.g. driveway, patio, paved or concrete area.</p>
                            
                                        <strong>Concrete</strong>
                            
                                        <p>You might already have a raised concrete pad on your property that’s just the right size for the shed you plan to buy.  Building a shed on it can be a great way to repurpose an old carport, driveway or patio area.  The frame of the shed will attach to the concrete slab using 8mm bolts and a series of clamps to ensure a solid connection.</p>
                            
                                        <p>If you don’t already have a concrete pad, making one can be a worthwhile investment.  The additional cost involved in buying, mixing and pouring cement, as well as making sure the cement is straight and level, may not suit everyone.  It’ll take a bit more time than putting together a timber floor, but it’ll make for the strongest and most permanent floor.</p>
                            
                                        <p>A concrete floor won’t expand and shrink the way that timber might, and it won’t crack either, or get damaged if you drop something heavy on it.  It’s also easier to keep clean, as you can simply hose it down.  This could be a pretty big advantage if you’re keeping animals in your shed, potting your plants, or storing anything that could get messy.</p>
                            
                                        <p>Pouring a 100mm recessed concrete floor allows you to have a solid foundation for the shed, providing a robust platform that will last years.  Of course once you’ve poured the concrete slab, you will be unable to move the shed to a different location – so take your time planning this option.  It’s ideal if you own your property and want your floor to last you as long as the shed will.</p>
                            
                                        <p>We can provide a guide to help you plan your concrete floor and secure the shed to it.</p>
                            
                                        <p class="tip">We recommend making the floor higher than the ground outside the shed, so that rain water doesn’t get in.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>DIY Assembly</h2>
                            
                                        <p>For those with a good level of general DIY skills, setting up a shed is quite a straightforward task.  Our sheds come with all the components you need and a full set of clear instructions to guide you through the process.  It will probably take you around 4 hours to put a smaller shed together, or 6-8 hours if you get one of our larger ones.</p>
                                        <p>Many hands make light work, so the assembly will probably be a lot easier and faster if you have a second pair of hands.  You’ll especially need some help to put the roof onto the shed.  Arrange for your partner, a friend or a family member to help out for the day.</p>
                                        <p>You’ll need a cordless drill, tape measure and a ladder.</p>
                                        <p>Trade Tested are more than happy to help with all the advice you need.  If you get stuck, just call us toll free on 0800 800 880.</p>
                            
                                        <p class="callout">Remember to put your safety first.  Make sure you use the right tools for the job, and don’t take on more than you’re capable of.  If you’re using a ladder, correct set-up is important, and remember to keep three points of contact at all times when you’re on the ladder and avoid over-reaching.  Read more about DIY safety on <a href="http://www.acc.co.nz/preventing-injuries/at-home/diy-around-the-home/PI00010">ACC\'s website</a>.</p>
                            
                                    </div>
                                    <div class="guides-section">
                                        <h2>Professional Assembly</h2>
                            
                                        <p>Our sheds are designed to be as easy and quick to put together as possible.  However if you don’t quite feel confident enough or are short on time, we can organise a free, no-obligation assembly quote to have your shed put together by an assembly partner in your area.  See more details on our <a href="/garden-sheds/assembly-service.html">Professional Assembly Service</a>.
                            
                                        <p>Our assembly partners can put down flooring in your shed too, if you need it.  However, they cannot clear your site or level the ground to work on.  If you’d like an assembly partner to put your shed up, you’ll need to make sure the site is flat and clear before they arrive.</p>
                            
                            
                                        <h2>Legal and Compliance</h2>
                            
                                        <p>In most areas, you won’t need building consent to put up a shed less than 10m2.  For our bigger sheds though, council regulations may be something you’ll need to look into.  Check with your local council to find out what requirements apply in your area.</p>
                            
                                    </div>
                                    <div class="guides-section">
                                        <h2>Positioning</h2>
                            
                                        <p>It could be a good idea to measure out space area you want your shed to go on before you decide.  Use pegs or stakes to mark it out, to make sure the shed will fit properly and look the way you want it to.  Depending on what you already have on your property, there might be concrete, lawn, or bare ground where you want to put your shed.  Alternatively, you might like to put your shed onto a deck or patio.  What type of location you choose will determine how your shed is secured to the ground and what kind of foundations and flooring you need.</p>
                            
                                        <p>Some people like to place their shed away from the house, but balance these aesthetic considerations against practicality and convenience.  Having the shed near to where you use it is important to get the most use out of it.  You might find that the kids put their outdoor toys away if the shed is near to where they play.  Keep your shed near to facilities that you might need in the shed too, like power or water supply.</p>
                            
                                        <p>Understanding the features of your property will help you decide where your shed should go.  Think about the contour of the ground – it’s best for the shed to go on flat ground, or if there’s a slight incline, that should be towards the back of the shed.  If there’s any areas where drainage is poor, or water leaks from a feature such as a pond, that area will probably be too damp.  Wet ground could damage a timber floor or let water get into your shed.</p>
                            
                                        <p>You might like to leave a bit of space around your shed so that you can easily clean it and maintain it.  Make sure there’s space at the front for the door to open and for you to get any large items in and out without hassle.</p>
                            
                                        <p>Council regulations usually require that sheds be placed away from other buildings and fences – at least as far away as the shed is high.  If you have a swimming pool, keep in mind also that your shed probably needs to be well away from the swimming pool fence.</p>
                            
                                        <p>Be careful of putting your shed near any trees on your property, especially if the tree still has a lot of growing to do.  Leave enough room around the shed for the tree’s growth in the future.  Remember that the roots may still be spreading too, so you may not want to put the shed directly over them.  Use the drip line – the area on the ground covered by the tree’s canopy – to give you an idea of where the tree’s roots are.  You might also think twice about putting your shed under a tree that loses a lot of leaves, or drops seeds, flowers or fruit – clearing tree matter off the shed roof might be an extra garden chore you could do without!</p>
                            
                                        <p>Our sheds are tough enough for all weather conditions, but they’ll last even longer if they’re screened a little – wooden sheds especially so.  Shelter from the sun and wind will also be more comfortable if you’ll be working in the shed a lot, or a pet will be living inside.  Depending on the features of your property and the purpose of your shed, a sheltered area might be a good idea.</p>
                            
                                        <p>Remember too, to be nice to your neighbours.  If they enjoy a view from their property, try not to put the shed where it could block their view – attractive though your shed will be!</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Customisation and Accessories</h2>
                            
                                        <p>It’s possible to move the doors on pitched roof sheds, if you find that will suit your needs better.  You can’t move the door on a flat roofed shed, so consider whether you’d like to take this option when you choose a roof style.  To move the door, you’ll need to re-drill holes, so if you do it when you first set the shed up, the assembly process will take longer.</p>
                            
                                        <p>If you’ll be using tools or other electrical equipment in your shed, or if you want to put lighting in, you might want to install a permanent electrical supply.  We recommend you have this work done by a qualified electrician to make sure it’s done safely.</p>
                            
                                        <p>A ramp might also be a good idea if you’re storing anything with wheels, such as bikes or lawnmowers, or if you’ll need to carry heavy or bulky things in and out of the shed.</p>
                            
                                        <p>Ventilation and insulation might be things to think about too, especially if you’ll use your shed as a workshop, or the kids will keep a small pet in it.  Our sheds can easily be customised with windows or skylights, and you can even line with plaster board and add insulation.</p>
                            
                                        <p>Security may be an important consideration; if you have costly items like lawnmowers, bikes or power tools, you’ll want the peace of mind of knowing they’re safe.  Our sheds can easily be fitted with locks, and with a drill and a few screws you could even put up security lighting.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>How to Get One</h2>
                            
                                        <p>It’s quick and easy to order online, and if you have any trouble, you can live chat with us during business hours or call us free on 0800 800 880.  Most of our sheds are already in stock at our warehouse near Auckland Airport, so we can start getting it ready to send out as soon as you place your order.  In most areas, you’ll get it within about three days – a lot faster than some shed retailers, who can take several weeks to get a shed sent out.  We can deliver door-to-door nationwide.  You can also choose to have your shed delivered to the nearest depot. <a href="/shipping">Find your nearest depot</a>.</p>
                            
                                        <p>If it would suit you better to shop in person, you’re welcome to visit our Auckland Store.  It’s open 7 days a week and has an assembled shed on display, so you can see what they look like before you decide which one to take home.</p>
                            
                            
                                        <h2><a href="/browse/garden-sheds.html"><span style="text-decoration: underline;">Browse NZ\'s Best Garden Sheds </span></a></h2>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="button-section">
                                <a href="/chipper-buying-guide" class="button-cms prev">Chipper / Shredder buying guide</a>
                                <a href="/generator-buying-guide" class="button-cms next">Generator buying guide</a>
                            </div>'
    ],
    [
        'identifier'    => 'generator-buying-guide',
        'content'       => '<div class="buying-guides-main right-section cms">
                                {{block type="core/template" name="buyingHead" template="cms/buyinghead.phtml"}}
                                <div class="buying-guide-container">
                                    <div class="guides-section">
                                        <h1>Generator Buying Guide</h1>
                            
                                        <p>There are a number of things to consider when choosing the generator that\'s right for you. Most importantly, you\'ll need to know the power requirements of the equipment you\'ll be running. It\'s easy to be intimidated by a barrage of technical electrical information, but this simple guide will help you make an informed and confident purchase.</p>
                            
                                        <p>The main things you need to consider are:</p>
                            
                                        <h2>1. Power output (either measured in watts or kVA)</h2>
                            
                                        <p>There are two main power measurements for generators: startup watts (also known as peak power) and running watts.</p>
                            
                                        <p>When choosing a generator, you\'ll need to choose one that can handle the startup wattage of the equipment you\'re planning to power. As a rule of thumb, the startup wattage is generally around 2-3 times the running wattage of a device. This varies however, and your best bet is to check the manual of the device you\'re planning to power for an exact value.</p>
                            
                                        <p>At the bottom of this guide is a list of the approximate values for the running wattage of common household appliances.</p>
                            
                                        <h2>2. Fuel type - petrol or diesel?</h2>
                            
                                        <p>Most portable generators run on petrol, but it\'s worth looking at diesel models as well. Diesel engines are generally harder to start and are a little bit more expensive than petrol. But they last longer, especially with continuous use. For larger models diesel is going to be cheaper to run due to lower fuel costs.</p>
                            
                                        <p>We recommend using a conditioner with diesel fuel, to stop the diesel growing fungus and degrading its quality (this happens naturally).</p>
                            
                                        <h2>3. Backup (stationary) or portable?</h2>
                            
                                        <p>If you\'re looking for backup household power, portability won\'t be a priority. Stationary generators have numerous advantages. They’re generally quieter, easier to start and more powerful than portable options. Plus if they’re hardwired into your home or business (don’t do this yourself!) there will be no mucking around with extension cords when the power is out.</p>
                            
                                        <p>For camping, motorhomes, marine, tradies, or basic home use you’ll likely be wanting something portable. The biggest decision to make with portable generators is what type of start up you want it to have. Both battery start and pull start options are available.</p>
                            
                                        <h2>4. Traditional or Inverter?</h2>
                            
                                        <p>Inverter technology makes these models perfect for powering sensitive electronics like computers, TVs, microwaves, etc. They produce a clean sine wave of electricity which guarantees there will be no power surges.<p>
                            
                                        <p>Standard / traditional generators run at full speed regardless of how much power you need. But an inverter uses a self-governing smart throttle which adjusts when you turn on a light or appliance. This means you save on fuel, it\'s quiet, runs smoothly, and is better for the environment.</p>
                            
                                        <h2>5. Single phase or three phase?</h2>
                            
                                        <p>Single phase generators produce single phase power which is similar to that used by most common household appliances. Hence single phase generators are suitable for most homes and small businesses.</p>
                            
                                        <p>Larger commercial operations often require three phase power, hence have the need for a three phase generator. </p>
                            
                                        <p>Three phase generators can be run as single phase however the power output will be roughly a third that of when it runs as a three phase.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Power required for common appliances</h2>
                            
                                        <p>The list below provides approximate values for the running wattage of common household appliances:</p>
                            
                                        <p>Remember your generator needs to have enough power output to handle the startup wattage (approximate 2 to 3 times the values below)</p>
                            
                                        <p><strong>Common Appliances:</strong></p>
                            
                                        <ul class="list-float">
                                            <li>Clothes Dryer 4000W</li>
                                            <li>Dishwasher 1200 - 3600W</li>
                                            <li>Electric Oven 2000W</li>
                                            <li>Heater 150 - 2000W</li>
                                            <li>Microwave 600 - 1500W</li>
                                            <li>Toaster 800 - 1500W</li>
                                            <li>Electric Frying Pan 1200W</li>
                                            <li>Iron 1000W</li>
                                            <li>Blow Hair Dryer 1000W</li>
                                            <li>Stove Range 800W</li>
                                            <li>Coffee Maker 800W</li>
                                            <li>Vacuum 200 - 800W</li>
                                            <li>Washing Machine 500W</li>
                                            <li>Fridge/Freezer 200 - 700W</li>
                                            <li>Television 100 - 450W</li>
                                            <li>Computer + Monitor 100 - 400W</li>
                                            <li>Blender 350W</li>
                                            <li>Electric Blanket 200W</li>
                                            <li>Sewing Machine 100W</li>
                                            <li>Light Bulb 18 - 60W</li>
                                            <li>Laptop 50W</li>
                                            <li>Shaver 15W</li>
                                        </ul>
                            
                            
                                        <p><strong>Outdoor Power Tools:</strong></p>
                                        <ul>
                                            <li>Electric Mower 1500W</li>
                                            <li>10" Bench Saw 1500W</li>
                                            <li>Weed Eater 500W</li>
                                            <li>Hedge Trimmer 450W</li>
                                            <li>Belt Sander 380W</li>
                                            <li>Drill 330W</li>
                                        </ul>
                            
                            
                                        <p>When you browse the generators on Trade Tested, you\'ll see they are listed by their power capacity in kVA (kilo volt-amperes). This is standard practice outside America. You can find a generator\'s capacity in watts (both startup watts and running watts) in the description section of each generator on our site.</p>
                            
                                        <p>Choosing the wrong sized generator is an easy mistake to make. Most models can sustain only 80% of their maximum power for the long haul. If you constantly push your generator to over 80% power, it\'s not going to last as long, and risks damaging the appliances connected to it. As a general rule, bigger is always better.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Backup Household Generators vs Portable Generators</h2>
                            
                                        <p>If you\'re looking for backup household power, portability won\'t be a priority. To chose the right size, think about how many appliances you\'ll be wanting to power simultaneously and add up their wattage values. For most households we would suggest a 6.8kVA Petrol Digital Inverter as a backup. If you’re only going to be running one or two pieces of equipment or appliances, the 4kVA Digital Inverter generator is our most popular model.</p>
                            
                                        <p>For camping, motorhomes, marine, tradies, or basic home use, the 3.5kVA Digital Inverter generator offers great portability and enough grunt to comfortably handle most equipment you\'ll be plugging into it.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Safety Tips</h2>
                            
                                        <p>It\'s important to consult the operator manual for a full list of safety precautions, but here\'s a few basic tips worth following:</p>
                            
                                        <li>Don\'t run your generator indoors or in poorly ventilated areas. Avoid running it near windows, doors, and other openings where the exhaust can enter indoors.</li>
                            
                                        <li>Make sure the generator is placed on a level surface.</li>
                            
                                        <li>Avoid exposing the generator to excessive moisture, dirt, and dust.</li>
                            
                                        <li>Avoid overfilling the fuel tank and never fill up the generator when it is running or still hot from recent use.</li>
                            
                                        <li>Keep the generator a safe distance from open flames, pilot lights, and anything sparky.</li>
                            
                                        <li>Never connect a generator directly to your home\'s wiring. If you need to do this, hire an electrician and get them to install a transfer switch.</li>
                            
                                        <li>Use heavy duty outdoor extension cords to plug appliances into the machine. Or connect the generator to a power-transfer switch.</li>
                            
                                        <li>Do not overload the generator.</li>
                            
                                    </div>
                                    <div class="guides-section">
                                        <h2>Got a Question?</h2>
                                        <p>Either&nbsp;<a href="/contact-us">send us an email</a>&nbsp;or give us a bell during office hours on {{config path="general/store_information/phone"}}.</p>
                                        <p>&nbsp;</p>
                            
                                        <h2><span style="text-decoration: underline;"><strong><a href="/browse/generators.html">Save up to 35% on Generators </a></strong></span></h2>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="button-section">
                                <a href="/garden-shed-buying-guide" class="button-cms prev">Garden Shed buying guide</a>
                                <a href="/greenhouse-buying-guide" class="button-cms next">Greenhouse buying guide</a>
                            </div>'
    ],
    [
        'identifier'    => 'greenhouse-buying-guide',
        'content'       => '<div class="buying-guides-main right-section cms">
                                {{block type="core/template" name="buyingHead" template="cms/buyinghead.phtml"}}
                                <div class="buying-guide-container">
                                    <div class="guides-section">
                                        <h1>Greenhouse Buying Guide</h1>
                                        <p>Owning a greenhouse is great. They can transform even a small section and add to anyone\'s quality of life. Greenhouse owners eat more healthily, have less stress, save money on groceries, and know exactly where their food comes from. Not only will you help the environment, you\'ll also have the opportunity to get the kids interested in eating healthy. Greenhouse gardening is relaxing, financially smart, and a uniquely satisfying part of modern living.</p>
                            
                                        <p>No longer just for commercial growers, new materials and technology have made domestic greenhouse gardening easier and more affordable than ever. As more and more people experience how great it really is, it\'s not hard to see why it\'s such a growing trend.</p>
                            
                                        <p>Trade Tested has greenhouses to suit sections of any size. All you need to know is which greenhouse is the right one for you.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>The Golden Rule</h2>
                                        <p>If you buy a greenhouse, you\'re going to use it, and you\'re going to want to use it more. Greenhouse gardeners quickly develop an enthusiasm that takes on a momentum of its own. It\'s not like those gym memberships people pay for and then never use. By far the most common regret among first time greenhouse buyers is that they didn\'t think ahead to accommodate their growing interest. It\'s far easier to buy a greenhouse one size larger than it is to put an extension on a smaller one.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>So What\'s Right for You?</h2>
                                        <p>There\'s a lot of different greenhouse technology on the market. The quality and material of greenhouse frames and panels varies. So do the designs. The best frames are durable and rust-free. The best panels provide proper insulation, diffused light, and UV protection. The best designs look good and provide proper insulation.</p>
                            
                                        <p>Trade Tested\'s greenhouses are constructed from sturdy, rust-free aluminium frames. Our panels are made from twin-walled polycarbonate that diffuses the light, insulates the interior, and offers plants UV protection. Polycarbonate is a strong, lightweight material that makes for safe and easy construction. It won’t break like glass in strong wind or an earthquake, and it won\'t crack or discolour like acrylic sheets can in the winter.</p>
                            
                                        <p>Other things to consider include how the greenhouse will look on your section, whether or not you want to customize it, and any accessories you may want to add to it.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Protect your Plants</h2>
                                        <p>Overheating is the main reason plants fail in a greenhouse, so controlling ventilation is vital. Our automatic vent openers are an excellent option that\'ll do it for you, and are especially handy when you\'re away from home.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Location, Location, Location</h2>
                                        <p>When choosing a spot to put your greenhouse, there are a few simple things to bear in mind. Find an open part of the garden away from any shadows from trees and buildings. It\'s simple to shade a greenhouse if the sun becomes too bright, but a lot harder to move a tree or building whose shade stunts your plant.</p>
                            
                                        <p>Think about wind too. Wind can cool the temperature inside a greenhouse. Eliminate this to begin with by selecting a site near a hedge or fence, just remember to look out for shadows. </p>
                            
                                        <p>To get the most efficient use of space inside, multi-tiered staging shelves can maximize your growing output. Raised garden beds can also make a useful addition to a greenhouse for those who don\'t like bending over too far to tend to their plants. You can check out our greenhouse accessories page to see if any of these tickle your fancy.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Got a Question?</h2>
                                        <p>Either&nbsp;<a href="/contact-us">send us an email</a>&nbsp;or give us a bell during office hours on {{config path="general/store_information/phone"}}.</p>
                            
                                        <h2><span style="text-decoration: underline;"><strong><a href="/browse/greenhouses.html">Browse Greenhouses</a></strong></span></h2>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="button-section">
                                <a href="/generator-buying-guide" class="button-cms prev">Generator buying guide</a>
                                <a href="/logsplitter-buying-guide" class="button-cms next">Log splitter buying guide</a>
                            </div>'
    ],
    [
        'identifier'    => 'logsplitter-buying-guide',
        'content'       => '<div class="buying-guides-main right-section cms">
                                {{block type="core/template" name="buyingHead" template="cms/buyinghead.phtml"}}
                                <div class="buying-guide-container">
                                    <div class="guides-section">
                                        <h1>Log Splitter Buying Guide</h1>
                                        <p>Before buying a log splitter, it&rsquo;s good to understand that the log splitter doesn&rsquo;t actually cut wood but splits it. It uses a wedge which slowly rams up against the log, generating enough force to split it in half. The force exerted by a log splitter is measured in tons and the greater the tonnage, the wider and harder the logs it can split.</p>
                            
                                        <h2>What\'s Right for You?</h2>
                                        <p>Finding a suitable lop splitter, is simply a matter of knowing how thick your logs are. Eg. if you plan on splitting 200mm diameter branches, a small 6-ton log splitter will be adequate.</p>
                            
                                        <p>Fresh, green wood is very moist and difficult to cut. As it ages, the moisture inside slowly evaporates, making it more brittle. As a result, older firewood splits and burns much easier. If you plan on splitting green wood, you&rsquo;ll need a more powerful splitter.</p>
                            
                                        <p>The chart below is a great guide. But harder woods will require additional force.</p>
                                        <p>&nbsp;</p>
                                        <p><img src="{{media url="wysiwyg/buying_guides/LogSplitters-TonnageTable.jpg"}}" alt="" width="457" height="312" /></p>
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Petrol or Electric?</h2>
                                        <p class="SecondaryHeader-BG"><span style="font-size: medium;">Electric</span></p>
                                        <p>Most electric log splitters are powerful enough to split small quantities of mid-sized logs.</p>
                            
                                        <p>With a gross weight of 45 kg, it can be transported easily from your home to a camping ground or bach.</p>
                            
                                        <p>Remember, that you will also be restricted by the power cord. So think twice before taking an electric log splitter to a remote area.</p>
                            
                                        <p>&nbsp;</p>
                                        <p class="SecondaryHeader-BG"><span style="font-size: medium;">Petrol and Diesel</span></p>
                                        <p><a title="Gas Log Splitters" href="/browse/log-splitters/petrol.html">Petrol powered log splitters</a>&nbsp;are larger and bulkier that their electric counter-parts, but are by far the most popular choice for people who split wood regularly. <br /> <br /> Although petrol models are more popular, diesel splitters are worth considering. They&rsquo;re cheaper to run and last long but can be a bit harder to start. We recommend using a conditioner with diesel fuel, to stop the diesel growing fungus and degrading its quality (this happens naturally).</p>
                            
                            
                                    </div>
                                    <div class="guides-section">
                                        <h2>Safety</h2>
                                        <p><strong>&bull; &nbsp;</strong>Wear proper safety gear. Goggles, gloves and proper footwear are essential.</p>
                                        <p><strong>&bull; &nbsp;</strong>Make sure your log splitter is sitting squarely on level ground.</p>
                                        <p><strong>&bull; &nbsp;</strong>Do not attempt to cut anything besides wood.</p>
                                        <p><strong>&bull; &nbsp;</strong>Never operate a splitter in wet, muddy, or icy conditions.</p>
                                        <p><strong>&bull; &nbsp;</strong>For petrol and diesel units, only operate in an open ventilated area.</p>
                                        <p><strong>&bull; &nbsp;</strong>Only use you splitter with proper lighting, never at night.</p>
                                        <p><strong>&bull; &nbsp;</strong>Be careful of how you hold the log. Never place your hands between the wood and the splitting wedge.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Still need more help?</h2>
                                        <p>Either&nbsp;<a href="/contact-us">send us an email</a>&nbsp;or give us a bell during office hours on {{config path="general/store_information/phone"}}.</p>
                                        <h2><span style="text-decoration: underline;"><strong><a href="/browse/log-splitters.html">Browse Log Splitters</a></strong></span></h2>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="button-section">
                                <a href="/greenhouse-buying-guide" class="button-cms prev">Greenhouse buying guide</a>
                                <a href="/pump-buying-guide" class="button-cms next">Pump buying guide</a>
                            </div>'
    ],
    [
        'identifier'    => 'pump-buying-guide',
        'content'       => '<div class="buying-guides-main right-section cms">
                                {{block type="core/template" name="buyingHead" template="cms/buyinghead.phtml"}}
                                <div class="buying-guide-container">
                                    <div class="guides-section">
                                        <h1>Water Pump Buying Guide</h1>
                                        <h2>How to choose a pump and flow rate</h2>
                            
                                        <p>When you choose your pump you need to figure out how many devices or appliances that use water could be operating at the same time.  This is commonly known as "taps". For example,  3 taps could be your washing machine, a kitchen tap and someone using a shower or hose all at once.</p>
                            
                                        <p>To determine the flow you should also consider the distance from the water source to your pump, and from your pump to the required outlet of water - your home.  Elevation, pipe size as well as bends will also reduce the flow rate. This is known as system friction a head loss.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>What are the benefits of a pressure tank or controller?</h2>
                            
                                        <p>A pressure tank will:</p>
                            
                                        <ul>
                                            <li>Use less power</li>
                                            <li>Reduce wear and tear by eliminating unnecessary pump starts</li>
                                            <li>Prevent burn outs by preventing low flow cycling. (dripping taps/tap being turned on and off quickly)</li>
                                            <li>Eliminate pump failures because of water hammer</li>
                                        </ul>
                            
                                        <p>A controller will:</p>
                            
                                        <ul>
                                            <li>Protect the pump from damage using dry-run protection</li>
                                            <li>Have adjustable pressure using a simple switch</li>
                                            <li>Protect plumbing by regulating the pressure</li>
                                            <li>Be easy to install and is nice and compact</li>
                                            <li>Provide constant reliable pressure</li>
                                        </ul>
                            
                                        <p class="tip">We recommend the use of a water hammer arrestor with any system. Especially with filter systems and UV. Pumps should also always be protected from power surges.</p>
                            
                                        <h2>What size tank should I get?</h2>
                            
                                        <p>For most average size houses we recommend a tank of no less than 50L. And the general is always bigger is better. A pressure tank only holds about 1/3rd of the tank\'s capacity at pressure. The bladder in the tank tends to last about 40,000 cycles, so the less it cycles the better.</p>
                            
                                        <p>Average water requirements:</p>
                            
                                        <p>Cottage or Bach:  1 bathroom - 1-2 taps<br/>
                                            15-20 litres per minute</p>
                            
                                        <p>Small Home:  1 bathroom, single storey - 2+ taps<br/>
                                            30-40 litres per minute</p>
                            
                                        <p>Typical Home:  1-2 bathrooms, single storey - 3-4 taps<br/>
                                            40-50 litres per minute</p>
                            
                                        <p>Larger Home:  2 bathrooms, 2 storey - 4+ taps<br/>
                                            50-70 litres per minute</p>
                            
                                        <p>Large Home:  2+ bathrooms, 2 storey, multiple appliances - 5+ taps<br/>
                                            70-110 litres per minute</p>
                            
                                        <p>Large Building:  Multiple bathrooms, appliances and levels - 6+ taps<br/>
                                            90-250 litres per minute</p>
                            
                                    </div>
                                    <div class="guides-section">
                                        <h2>What maintenance do I need to do?</h2>
                            
                                        <p>The best thing to do is to check your tanks every month just to make sure you’re not loosing pressure. Re-Pressurising should be only necessary about every 4-6 months, at the most.  More than likely, the bladder will need re-pressurising or have broken.  With our tanks, you can buy a replacement bladder for much less than the cost of replacing the tank.</p>
                            
                                        <p>To replace a bladder, simply let the air out using the air pressure relief valve, and then undo the unit at the base. The bladder should come out easily. It’s best to give the tank a good wipe to remove any surface rust, and then let it dry thoroughly. Then simply insert the new bladder, close the unit and bring back up to pressure.</p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>I can buy a cheap tank - are these ok?</h2>
                            
                                        <p>Yes of course, but we urge you to ensure that the bladder is EPDM rubber if coming in contact with drinking water.  All Trade Tested bladders are certified as EPDM. Cheap tanks often use non EPDM rubber, and this can be unsafe as chemicals used in the manufacture can leak. But don’t worry. We don’t sell any ones like that. So you’re looking in the right place!</p>
                                        <p>If you have any more questions just give us a call 0800 8008880 or <a href="/contact-us">send us an email</a>.</p>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="button-section">
                                <a href="/logsplitter-buying-guide" class="button-cms prev">Log splitter buying guide</a>
                                <a href="/trailer-buying-guide" class="button-cms next">Trailer buying guide</a>
                            </div>'
    ],
    [
        'identifier'    => 'trailer-buying-guide',
        'content'       => '<div class="buying-guides-main right-section cms">
                            {{block type="core/template" name="buyingHead" template="cms/buyinghead.phtml"}}
                            <div class="buying-guide-container">
                                <div class="guides-section">
                                    <h1>Trailer Buying Guide</h1>
                                    <p>There are a few things to consider before buying your trailer, check out our guide to make sure that you buy the perfect trailer for the job.</p>
                                    <h2>What\'s Right for You?</h2>
                                    <h2>1. How Big?</span></h2>
                                    <p>Check the load capacity of trailer and match this with the weight of what you\'re going to be towing.</p>
                                    <p>Generally speaking for heavy loads of up to 1,000kg, choose a tandem axle. For lighter use (up to 500kg), a single axle is more appropriate. If there is a chance you could be towing heavier and bulkier loads in future look at a&nbsp;<a href="/browse/trailers/tandem/trailer-8ft-x-5ft-tandem-axle-braked-with-cage.html">tandem axle with removable cage.</a></p>
                                    <h2>2. How Much Can My Vehicle Tow? Do I Need a Braked Trailer?</h2>
                                    <p>Brakes on a trailer will increase safety. For braked models, the weight of the trailer plus the load, must not be more than the towing vehicle\'s mass weight.</p>
                                    <p>Check your motor vehicle\'s handbook, which will list the following:</p>
                                    <ul>
                                        <li>The maximum weight of laden trailer, without brakes, that can be towed by the motor vehicle.</li>
                                        <li>The maximum weight of laden trailer, with brakes, that can be towed by the motor vehicle.</li>
                                    </ul>
                                    <p>Below is a guide to let you know the approximate towing capacity of common motor vehicles. Be sure to contact your vehicle\'s manufacturer for actual towing capacity, or have a look online.</p>
                                    <p>Vehicle Towing Capacity (approx):</p>
                                    <ul class="list-float">
                                        <li>Ford Ranger 2.5 1000 Kg</li>
                                        <li>Ford Ranger 3.0 1800 Kg</li>
                                        <li>Landrover Defender 3500 Kg</li>
                                        <li>Landrover Discovery 3500 Kg</li>
                                        <li>RangeRover 2500 Kg</li>
                                        <li>Nissan NP300 1400 Kg</li>
                                        <li>Nissan Navara 2950 Kg</li>
                                        <li>Nissan Patrol P/U 2500 Kg</li>
                                        <li>Nissan Patrol 3500 Kg</li>
                                        <li>Mitsubishi Pajero 3000 Kg</li>
                                        <li>Mitsubishi Pajero 1500 Kg</li>
                                        <li>Toyota Hilux 2.7 1500 Kg</li>
                                        <li>Toyota Hilux 2.5 1650 Kg</li>
                                        <li>Toyota Hilux 3.0 1590 Kg</li>
                                        <li>Toyota Fortuna 2020 Kg</li>
                                        <li>Toyota Landcrusier 70 P/U 1500 Kg</li>
                                        <li>Toyota Landcrusier GX 3500 Kg</li>
                                    </ul>
                                    <h2>3. Cage or No Cage?</h2>
                                    <p>Caged trailers are perfect for bulky items like beds, furniture, small and ATVs, Make sure to check that their weight doesn\'t exceed trailer load capacity.</p>
                                    <p>Non-caged trailers are excellent for general purposes, especially for landscaping supplies like pebbles, compost and sand because of easy tray access.</p>
                                    <h2>4. Tandem Axle or Single Axle?</h2>
                                    <p style="margin: 5px 0 5px 0;"><strong>Tandem Axles:</strong></p>
                                    <p style="margin: 5px 0 5px 0;"><strong></strong><strong><strong>&bull; &nbsp;</strong></strong>Better stability and smoother riding.</p>
                                    <p style="margin: 5px 0 5px 0;"><strong><strong>&bull; &nbsp;</strong></strong>Carry up to 1500kg.</p>
                                    <p style="margin: 5px 0 5px 0;"><strong><strong>&bull; &nbsp;</strong></strong>A decrease in turning radius causes the turning motion to be harder on the tyres.</p>
                                    <p style="margin: 5px 0 5px 0;"><strong><strong>&bull; &nbsp;</strong></strong>A less powerful vehicle will struggle with the weight and friction of tandem axles when carrying large loads. Check there\'s even weight distribution over both axles otherwise there can be too much stress on a single axle.</p>
                                    <p style="margin: 5px 0 5px 0;"><strong><strong>&bull; &nbsp;</strong></strong>When attaching, the vehicle often needs to be moved to the trailer.</p>
                                    <p style="margin: 5px 0 5px 0;"><strong><strong>&bull; &nbsp;</strong></strong>Front and rear closing doors.</p>
                                    <p style="margin: 0 0 5px 0;"><strong><strong>&bull; &nbsp;</strong></strong>A less powerful vehicle will struggle with the increased weight and friction of tandem axles with large loads. Check there\'s even weight distribution over both axles otherwise there can be too much stress on a single axle.</p>
                                    <p style="margin: 0 0 5px 0;"><strong><br /></strong></p>
                                    <p style="margin: 0 0 5px 0;"><strong>Single Axle Trailers:</strong></p>
                                    <p style="margin: 0 0 5px 0;"><strong></strong><strong><strong>&bull; &nbsp;</strong></strong>Lighter to tow due to reduced friction from tyres.</p>
                                    <p style="margin: 0 0 5px 0;"><strong><strong>&bull; &nbsp;</strong></strong>Tend to bounce and sway more.</p>
                                    <p style="margin: 0 0 5px 0;"><strong><strong>&bull; &nbsp;</strong></strong>Better gas mileage.</p>
                                    <p style="margin: 0 0 5px 0;"><strong><strong>&bull; &nbsp;</strong></strong>Easier to manoeuvre and less stress on tyres when turning.</p>
                                    <p style="margin: 0 0 5px 0;"><strong><strong>&bull; &nbsp;</strong></strong>One person can move the trailer around to hook up to vehicle.&nbsp;</p>
                                    <p>When loading a single axle, balance weight over the axle and secure the items to avoid sliding.</p>
                        
                                    <p style="margin: 5px 0 0 0;">&nbsp;</p>
                                    <div class="category-title">
                                    </div>
                                    </div>
                                    <div class="guides-section">
                                        <h2>I\'ve got my trailer, now what?</h2>
                                    <p style="margin: 5px 0 15px 0;">Once we\'ve delivered it to you, you\'ll need to assemble the trailer and license it for road use.</p>
                                    <p style="margin: 5px 0 15px 0;"><span style="font-size: medium;">1. Trailer Assembly</span></p>
                                    <p style="margin: 5px 0 15px 0;">Here are <a href="/resources/trailers/trailer-assembly-instructions.pdf" target="_blank">instructions for assembling your trailer</a>, it will take you approximately 2 hrs.</p>
                                    <p style="margin: 0 0 5px 0;">These are the tools you\'ll need:</p>
                                    <p style="margin: 0 0 5px 0;"><strong><strong>&bull; &nbsp;</strong></strong>Ratchet set</p>
                                    <p style="margin: 0 0 5px 0;"><strong><strong>&bull; &nbsp;</strong></strong>Crescent</p>
                                    <p style="margin: 0 0 5px 0;">&nbsp;</p>
                                    <p style="margin: 0 0 5px 0;"><span style="font-size: medium;">2. Trailer Registration and Licensing:</span></p>
                                    <p style="margin: 5px 0 15px 0;">Registering and licensing your trailer for road use is approximately $135 for 12 months. You can do this at a VTNZ station. There are VTNZ stations all around New Zealand. <a href="http://www.vtnz.co.nz/NearestStationSearch" rel="nofollow" target="_blank"> Find a vehicle registration station near you.</a></p>
                                    <p style="margin: 5px 0 15px 0;"><span style="font-size: medium;">3. Safety Information</span></p>
                                    <p style="margin: 0 0 5px 0;">Once your trailer is on the road, refer to NZTA\'s <a href="http://www.nzta.govt.nz/resources/glovebox-guide-safe-loading-towing/docs/guide-safe-loading-towing.pdf" rel="nofollow" target="_blank"> Guide to Safe Towing.</a></p>
                                    </div>
                                    <div class="guides-section">
                                        <h2>Still need more help?</h2>
                                        <p>Either&nbsp;<a href="/contact-us">send us an email</a>&nbsp;or give us a bell during office hours on {{config path="general/store_information/phone"}}.</p>
                        
                                        <h2><strong><span style="text-decoration: underline;"><a href="/browse/trailers.html">Browse Trailers</a></span></strong></h2>
                                    </div>
                            </div>
                        </div>
                        
                        <div class="button-section">
                            <a href="/pump-buying-guide" class="button-cms prev">Pump buying guide</a>
                            <a href="/tv-bracket-buying-guide" class="button-cms next">TV bracket buying guide</a>
                        </div>'
    ],
    [
        'identifier'    => 'tv-bracket-buying-guide',
        'content'       => '<div class="buying-guides-main right-section cms">
                            {{block type="core/template" name="buyingHead" template="cms/buyinghead.phtml"}}
                            <div class="buying-guide-container">
                                <div class="guides-section">
                                    <h1>TV Bracket Buying Guide</h1>
                                    <h2>What\'s Right for You?</h2>
                                    <p><a href="/browse/storage-utility/tv-brackets/flat.html"><strong>Low profile</strong></a><strong> &ndash;</strong>&nbsp;Ultra slim brackets keep your screen as close to the wall as possible and are designed for sitting 90 - 120cm from the floor. These are great for the lounge and bedroom and also in waiting rooms and reception areas.</p>
                                    <p>&nbsp;</p>
                                    <p><a href="/browse/storage-utility/tv-brackets/tilting.html"><strong>Tilt</strong></a><strong> &ndash;</strong>&nbsp;Perfect for mounting higher on the wall as the screen can be angled downwards. They sit slightly further out from the wall to allow for the tilting. Some low profile brackets can tilt, but most don\'t.</p>
                                    <p>&nbsp;</p>
                                    <p><a href="/browse/storage-utility/tv-brackets/full-motion-tilt-swivel.html"><strong>Swivel</strong></a><strong>&nbsp;&ndash;</strong>&nbsp;Great for mounting your TV in a corner, or if you need to move the screen around often. They sit further from the wall than other mounts but provide greater flexibility and viewing options. The arm can move out for watching then be folded close to the wall when not in use. Many can also be tilted for optimal viewing. Great for bedrooms, kitchens and reception rooms. The&nbsp;Full Motion&nbsp;brackets feature tilt, swivel, rotate and extension functions.</p>
                                    <p>&nbsp;</p>
                                    <p><a href="/browse/storage-utility/tv-brackets/projector-mounts.html"><strong>Ceiling &amp; Projector</strong></a><strong> &ndash;</strong>&nbsp;Brackets are used to suspend a projector from the ceiling safely and securely. This keeps it out of the way, removing the need for a dedicated table or shelf.</p>
                                    <h2>Things to Consider</h2>
                                    <p><span style="font-size: medium;">Why mount my TV?</span></p>
                                    <p><strong>1. Save space </strong>- Flat screens are designed to be mounted. It saves you finding a table or stand for your TV. These will take up space and need to be strong enough to support the screen\'s weight.</p>
                                    <p><strong>2. It\'s sleek &amp; tidy</strong> - Hanging your screen on the wall looks great and allows you to hide the cables.</p>
                                    <p><strong>3. Achieve the best viewing angle</strong> - Avoid neck and eye strain by finding the best height for your TV. TV mounts can also be great for positioning your screen out of reach of children.</p>
                                    <p><span style="font-size: medium;">Can my screen be wall mounted?</span></p>
                                    <p>Televisions and TV brackets marked "VESA mounting compliant" are compatible. This is also known as FDMI or MIS. Most modern flat screen TVs and LCD monitors are VESA compliant. Universal mounting holes will fit virtually any flat screen.</p>
                                    <p><span style="font-size: medium;">Where should I mount my screen?</span></p>
                                    <p>For comfortable TV viewing, the centre of the screen should be at eye level for maximum comfort.</p>
                                    <p><br /> To figure out how high to mount the bracket:</p>
                                    <p>(1) Measure distance between bottom of wall bracket to bottom of screen panel.<br /> (2) Make a mark on the wall where you want the bottom of the TV to be.<br /> (3) Using your measurement from (1), mark the wall where the bottom of the bracket should be.</p>
                                    <p class="SecondaryHeader-BG"><span style="font-size: medium;">How do I put it on the wall?</span></p>
                                    <p>If you\'re comfortable with hanging a picture frame or mounting shelving, then you will be able to wall mount your TV. You\'ll need a powerdrill, measuring tape, pencil, a cloth, and a stud finder. Depending on the bracket, you may also require a screwdriver, sockets and a ratchet.</p>
                                    <p><strong>1. Where to put it.</strong></p>
                                    <p>(a) Determine what type of wall you have.<br /> (b) Locate studs (if applicable) and mark location with a pencil.<br /> (c) Draw where the midline of TV will be on the wall, use a level if you have one.<br /> (d) Hold up the bracket\'s wallplate to your midline mark.<br /> (e) Pencil mark where the holes in the wallplate meet the wall so you know where to drill. Make sure holes are equidistant to floor.</p>
                                    <p><strong>Wall types &amp; drilling advice:</strong></p>
                                    <p><strong></strong><strong><strong>&bull; &nbsp;</strong></strong>For&nbsp;dry-wall / plaster walls find a wall stud. Use a stud-finder if you have one, otherwise knock on the wall until the sound changes from hollow and resonant to short and sharp. This signifies a wooden stud. Be sure to drill as close as possible to the wall stud\'s centre point.</p>
                                    <p><strong><strong>&bull; &nbsp;</strong></strong>If you have a larger TV (55 inch +) consider cutting out the drywall at the stud, screw a piece of plywood to the wall stud, then mount the screen to the plywood to distribute weight more evenly. The extra weight of the large TV can split the wall stud over time. Alternatively, bridge two vertical wall studs with a piece of standard 4 x 2 wood, then drill the holes for your bracket into this horizontal backing beam.</p>
                                    <p><strong><strong>&bull; &nbsp;</strong></strong>If your bracket has an extending arm, always secure the bracket to a wooden stud.</p>
                                    <p><strong><strong>&bull; &nbsp;</strong></strong>For&nbsp;solid concrete, rammed earth&nbsp;or&nbsp;brick walls, drill directly at the desired location after lining up your TV. Remember, you&rsquo;ll need a concrete specific drill bit.</p>
                                    <p><strong><strong>&bull; &nbsp;</strong></strong>If your walls have&nbsp;metal studs&nbsp;and you wish to mount your TV to these instead, visit your local hardware or home store and purchase toggle bolts or togglers and washers, then pre-drill the holes. Metal studs aren&rsquo;t recommended for heavy TV&rsquo;s.</p>
                                    <p>Remember: Drill holes using a tool slightly smaller than the screws included with your wall bracket.</p>
                                    <p><strong>2. Attach the wallplate to wall</strong></p>
                                    <p>Use the hardware included to anchor the mounting plate to wall. Either drill the screws directly into the wooden studs, or pre-drill holes then screw hardware into wall manually. If you have concrete/brick walls, pre-drill holes, then drill screws into these holes while holding up the wall plate.</p>
                                    <p><strong>3. Attach brackets to back of the TV</strong></p>
                                    <p>Lay a towel onto the floor and place your LCD or plasma screen face down on the towel. Attach the mounting brackets to the back of your TV. If the holes have caps covering them, pop them out with a flat-head screwdriver. Attach the mounting brackets securely to the TV and make sure everything is tight. Slowly pull up on the mount to ensure it&rsquo;s secure and holding the TV\'s weight.</p>
                                    <p><strong>4. Lift the TV and secure to wall plate</strong></p>
                                    <p>Depending on how heavy your TV is you will probably need an extra set of hands to help you lift the TV and keep it steady while you screw it in. Once everything is securely attached, gently release the TV to allow the mount to hold the weight. Now put your feet up and relax.</p>
                                    <p class="Header-BG">&nbsp;</p>
                                </div>
                                <div class="guides-section">
                                    <h2>Safety</h2>
                                    <p class="Header-BG"><strong><strong>&bull; &nbsp;</strong></strong>Avoid electric shock by turning off the power in areas you\'ll be drilling.</p>
                                    <p class="Header-BG"><strong><strong>&bull; &nbsp;</strong></strong>Get a person to help you lift the TV and keep it steady while attaching to the bracket and lining it up.</p>
                                </div>
                                <div class="guides-section">
                                    <h2>Frequently Asked Questions</h2>
                                    <p><strong>&bull;&nbsp;Is there a difference between dual arm of single arm swivels?</strong></p>
                                    <p>Dual arm brackets offer increased support and are the perfect choice for larger monitors and screens.&nbsp;Single-arm Swivel&nbsp;wall brackets are a popular choice for small screens. While some will hold up to a 37" screen, they are predominantly designed for LCD monitors and small televisions.</p>
                                </div>
                                <div class="guides-section">
                                    <h2>Still need more help?</h2>
                                    <p>Either&nbsp;<a href="/contact-us">send us an email</a>&nbsp;or give us a bell during office hours on {{config path="general/store_information/phone"}}.</p>
                                    <p><strong style="font-size: 1.5em;"><span style="text-decoration: underline;"><a href="/browse/storage-utility/tv-brackets.html">Browse TV Brackets</a></span></strong></p>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="button-section"><a class="button-cms prev" href="/trailer-buying-guide">Trailer Buying Guide</a></div>'
    ],
    [
        'identifier'    => 'about-us',
        'content'       => '<div class="about-head-block right-section cms">
                                <h1>About Trade Tested</h1>
                                <p>
                                    Trade Tested has grown to be New Zealand’s top online, outdoor hardware shop. 100% NZ owned and operated, we’re a combination of new technology and good old fashioned Kiwi customer service. As we don’t have a chain of giant shops, or a million staff, our prices can’t be beaten.
                                </p>
                                <p>
                                    It all began in 2001 when we started importing to a small garage in central Auckland, selling on what was then a little site called Trade Me. As Kiwis got more used to buying things online, we got more used to selling them. After several years of being one of the most popular businesses on Trade Me, we decided to start a website. Just for us.
                                </p>
                                <p>
                                    So after a few sleepless nights and lots of coffee, TradeTested.co.nz launched on the 10th of July, 2010. Our goal –
                                    <span>deliver Kiwis the best deals on everything for their backyards straight to their front door</span>
                                    . And since then we have been doing just that.
                                </p>
                                <p>
                            
                                    From the humble water blaster in Ponsonby to the giant farm shelter for an orchard in Cromwell, no matter how big your backyard is we’ve got you covered. Last year alone we are proud to have delivered to over 90,000 satisfied customers all around the country.
                                </p>
                                <p>
                                    Over the next few years we’re going to continue to grow, adding a bunch of new product lines and categories. So if you have any questions, queries, suggestions or theories, we’d love to hear from you.
                                </p>
                                <div class="signature"></div>
                                <span>Richard Humphries</span>
                                <span>Trade Tested Founder</span>
                            </div>
                            <div class="about-team-wrap cms">
                                <div class="about-team">
                                    <h2>The Trade Tested Team</h2>
                                    <ul>
                                        <li>
                            
                                            <img src="{{media url="wysiwyg/about-us/about_richard.png"}}" alt="Richard Humphries">
                                            <span>Richard Humphries</span>
                                            <span>Trade Tested Founder</span>
                                        </li>
                                        <li>
                                            <img src="{{media url="wysiwyg/about-us/about_terry.png"}}" alt="Terry Metcalfe">
                                            <span>Terry Metcalfe</span>
                                            <span>eCommerce and Senior Sales Manager</span>
                                        </li>
                                        <li>
                                            <img src="{{media url="wysiwyg/about-us/about_fernanda.png"}}" alt="Fernanda Santos de Sousa">
                                            <span>Fernanda Santos de Sousa</span>
                                            <span>Office Administrator</span>
                                        </li>
                                        <li>
                                            <img src="{{media url="wysiwyg/about-us/about_kimberley.png"}}" alt="Kimberley Mackey">
                                            <span>Kimberley Mackey</span>
                                            <span>Director of Operations</span>
                                        </li>
                                        <li>
                                            <img src="{{media url="wysiwyg/about-us/about_james.png"}}" alt="James Parsons">
                                            <span>James Parsons</span>
                                            <span>Customer Service Specialist</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="about-why cms">
                                <div>
                                    <h2>Why Trade Tested</h2>
                                    <p>At Trade Tested, we manufacture a lot of the products you see when browsing our catalogue. Using our office in Shanghai, we are able to stay in constant contact with our suppliers. This allows us to be very hands on with the design and development process.</p>
                                    <p>When we try out a new range, it all gets tested by someone at our headquarters in Auckland. We go back and forth with changes and improvements, finally ending up with something we know our customers will be happy with. Only then are we confident enough to put our sticker on the side and call it Trade Tested.
                                    </p>
                                    <p>In saying that, as a Kiwi business we also like to partner with well established locals like Bosch, Bahco, Skellerup, Fiskars, Steel Fort and Hills. All of these companies have great, reliable products that have been around for years and have proven to withstand the toughest New Zealand conditions.</p>
                                    <p>So if you have a trusted Kiwi brand and are keen to see your products up on our site, get in touch today. We’d love to hear from you.</p>
                                </div>
                                <div>
                                    <h2>Our Service Network</h2>
                                    <p>We have a network of over 50 reliable service and repair agents stretching from Kaitaia to Invercargill. So should anything go wrong, or you just need some handy tips, there is always someone nearby who can help you out.</p>
                                    <p>To find the agent closest to you, just give us a bell or </p>
                                    <a href="">send us a message</a>
                                </div>
                            </div>
                            <div class="about-store cms">
                                <h2>Our Store & Warehouse</h2>
                                <div>
                                    <img src="{{media url="wysiwyg/about-us/about_store.png"}}" alt="Trade Tested store">
                                    <p>A typical day out at the Trade Tested store and warehouse by Auckland Airport.</p>
                                </div>
                                <div>
                                    <img src="{{media url="wysiwyg/about-us/about_warehouse.png"}}" alt="Trade Tested warehouse">
                                    <p>We started out in a garage and soon realised we may need a bit more room. So this is our new garage - the Trade Tested warehouse.</p>
                                </div>
                            </div>'
    ],
    [
        'identifier'    => 'careers',
        'content'       => '<div class="right-section careers-wrap cms">
                                <h1>Careers</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in mi arcu. Praesent eu turpis diam. Fusce sed maximus turpis. Duis eu risus interdum, tristique sapien non, mattis purus. Ut non dui sed orci dignissim commodo. Duis magna sem, iaculis a egestas at, aliquet sit amet diam. Proin ullamcorper sapien in nunc tempus, ut sollicitudin diam mollis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                                <img alt="Trade Tested careers" src="{{media url="wysiwyg/auckland-store/career_main.png"}}" />
                                <h2>Our Culture And Values</h2>
                                <p>Sed nec suscipit mi. Nam id tincidunt leo. Sed posuere, leo a tincidunt aliquet, felis turpis laoreet augue, id laoreet augue dolor a ante. Mauris convallis turpis ut risus mollis hendrerit. Vivamus placerat congue urna, sagittis scelerisque lorem molestie eu. In tempus imperdiet sapien, eget bibendum leo imperdiet convallis. Vivamus at vestibulum nulla. Integer ac pharetra ex, eget bibendum diam. Pellentesque posuere ullamcorper rutrum. Donec sapien est, sodales vel ex sit amet, consectetur eleifend lorem. Sed lobortis euismod tortor sit amet feugiat. Integer commodo accumsan facilisis. In posuere orci id nulla congue cursus vel in ipsum.</p>
                                <h2>Current Vacancies</h2>
                                <p>We always want to hear from passionate, experienced people. If you think you&rsquo;d fit in well at Trade Tested then send your CV and cover letter to <a>info@tradetested.co.nz</a>.</p>
                                <a class="trade-button">View Current Vacancies</a></div>
                            <!--------------------------->
                            
                            <!--url key: auckland-store -->
                            <div class="auckland-store-wrap cms">
                                <div class="about-store-auckland auckland-store-block">
                                    <h1>Our Auckland Store</h1>
                                    <p>Come and check out our Auckland store, near the Airport. Most products are on display or just next door in the warehouse. Our Trade Tested gurus, Devon and Te are always happy to help or give out a bit of free advice. Just don’t ask them about rugby.</p>
                                    <div>
                                        <h3>Address</h3>
                            
                                        <span>3 Timberly Road</span>
                                        <span>Mangere</span>
                                        <span>Auckland 2022</span>
                                        <span>New Zealand</span>
                            
                                        <p>{{config path="general/store_information/phone"}}</p>
                                    </div>
                                    <div>
                                        <h3>Opening hours</h3>
                                        <ul>
                                            <li>Monday<span>9:00am - 5:00pm</span></li>
                                            <li>Tuesday<span>9:00am - 5:00pm</span></li>
                                            <li>Wednesday<span>9:00am - 5:00pm</span></li>
                                            <li>Thursday<span>9:00am - 5:00pm</span></li>
                                            <li>Friday<span>9:00am - 5:00pm</span></li>
                                            <li>Saturday<span>9:00am - 4:00pm</span></li>
                                            <li>Sunday<span>10:00am - 4:00pm</span></li>
                                            <li>Public holidays<span>Closed</span></li>
                                        </ul>
                                    </div>
                                    <div>
                                        <h3>Payment options</h3>
                                        <span>EFTPOS</span>
                                        <span>Credit Card (Visa & Mastercard)</span>
                                        <span>Cash</span>
                                    </div>
                                </div>
                            
                                <div class="map-section-auckland" id="map-auckland"></div>
                                <div class="auckland-look-around about-store">
                                    <h2>Take A Look Around</h2>
                                    <p>A bit more information about what is on display in store. Can people try stuff out? Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse convallis diam sit amet sollicitudin auctor. Nam eget congue velit. Aenean dapibus consectetur turpis, a luctus magna tristique a. Cras venenatis libero dictum, venenatis nibh nec, sodales massa. Sed tempus egestas turpis sed dapibus. Sed ut odio et leo vulputate iaculis eu eu felis. Integer in cursus metus. Phasellus vulputate erat et venenatis scelerisque. Vivamus posuere leo quis felis hendrerit sollicitudin. Vestibulum ligula metus, finibus eu auctor eu, porta quis leo. Nam sit amet sapien elit.</p>
                                    <img src="{{media url="wysiwyg/auckland-store/auckland_warehouse.png"}}" alt="">
                                    <div>
                                        <img src="{{media url="wysiwyg/about-us/about_store.png"}}" alt="">
                                        <p>A typical day out at the Trade Tested store and warehouse by Auckland Airport.</p>
                                    </div>
                                    <div>
                                        <img src="{{media url="wysiwyg/about-us/about_store.png"}}" alt="">
                                        <p>Craig and Te are the on site gurus. They know all about engines, building and fishing.</p>
                                    </div>
                                </div>
                                <div class="auckland-store-block"></div>
                            </div>
                            
                            <script>
                                var map,
                                    tradeLatLang = {lat: -36.983975, lng: 174.788361};
                                function initAuckMap() {
                                    map = new google.maps.Map(document.getElementById(\'map-auckland\'), {
                                        center: tradeLatLang,
                                        zoom: 15,
                                        disableDefaultUI: true,
                                        scrollwheel: false
                                    });
                                    marker = new google.maps.Marker({
                                        map:map,
                                        position: tradeLatLang,
                                        icon: \'{{media url="wysiwyg/auckland-store/map_marker.png"}}\'
                                    });
                                }
                            </script>
                            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWRF3JNDdtq34EjoGC4HgCDnEaK6xgtiQ&callback=initAuckMap" async defer></script>'
    ],
];

$connection = $installer->getConnection();
foreach ( $pages as $page ) {
    try {
        if ( ( $cmsPage = Mage::getModel('cms/page')->load( $page['identifier'], 'identifier') ) ) {
            $cmsPage->setContent( $page['content'] )->save();
        }
    } catch (Exception $e) {}
}
$installer->endSetup();