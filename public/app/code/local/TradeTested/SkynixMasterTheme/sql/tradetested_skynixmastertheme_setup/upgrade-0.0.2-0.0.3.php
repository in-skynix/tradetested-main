<?php

$blocks = array();

$blocks['Footer_Block_Category_Links'] = array(
    'content' => '<div class="one_block">
                        <ul>
                            <li><a href="/">Garden sheds</a></li>
                            <li><a href="/">Garages and carports</a></li>
                            <li><a href="/">Outdoor living</a></li>
                            <li><a href="/">Gardening</a></li>
                            <li><a href="/">Generators</a></li>
                            <li><a href="/">Tools & Hardware</a></li>
                            <li><a href="/">Marquees & Events</a></li>
                            <li><a href="/">Home & lifestyle</a></li>
                            <li><a href="/">Automotive</a></li>
                            <li><a href="/">Farming</a></li>
                        </ul>
                    </div>',
    'is_active' => 1
);

$blocks['Footer_Block_Page_Links'] = array(
    'content'   => '<div class="two_block">
                        <ul>
                            <li><a href="/">About us</a></li>
                            <li><a href="/">Contact us</a></li>
                            <li><a href="/">Shipping</a></li>
                            <li><a href="/">Careers</a></li>
                            <li><a href="/">Privacy Policy</a></li>
                            <li><a href="/">Site map</a></li>
                            <li><a href="/">Search terms</a></li>
                            <li><a href="/">Trade tested AU</a></li>
                            <li class="facebook"><a href="/">Follow us on Facebook</a></li>
                            <li> <a href="/"><img src=" {{skin url=\'images/facebook.png\'}}" alt="Banner"/></a></li>
                        </ul>
                    </div>',
    'is_active' => 1
);

$blocks['Footer_Block_Contacts'] = array(
    'content'   => '<div class="three_block">
                        <ul>
                            <li class="padding1"><span class="boldSize">Call us</span> <span class="yellowBold">0800 800 880</span></li>
                            <li ><span class="boldSize1">Contact Center</span> <span class="greenBold">OPEN NOW</span></li>
                            <li class="padding1"><span class="boldSize2">Our Contact Centre is open 7 days.</span></li>
                            <li><span class="boldSize1">Auckland Store</span> <span class="greenBold">OPEN NOW</span></li>
                            <li class="padding1"><span class="boldSize2">Our Auckland is open 7 days.</span></li>
                            <li><span class="boldSize3">Trade Tested</span></li>
                            <li><span class="boldSize2">3 Timberly Road,</span></li>
                            <li><span class="boldSize2">Auckland Airport,</span></li>
                            <li><span class="boldSize2">Mangere</span></li>
                        </ul>
                    </div>',
    'is_active' => 1
);

$blocks['Footer_Block_Payment_Info'] = array(
    'content'   => '<li class="boldSize1">Payment</li>
                    <li>All prices are in NZD and include GST.</li>
                    <li class="cards">We accept Visa, Mastercard, Internet banking.</li>
                    <li class="cards">Paypal, Q Card and Farmlands Card. Credit</li>
                    <li class="pictures1">card payments are fully secure.</li>',
    'is_active' => 1
);

$blocks['Footer_Block_Shipping_Info'] = array(
    'content'   => '<div class="twoNext">
                         <ul>
                            <li class="boldSize1">Shipping</li>
                            <li class="shippingMain"><span class="shipping">We use PBT, Toll and Courierpost. Shipping prices are</span>
                         shown on product pages and in the checkout.</li>
                         </ul>
                    </div>',
    'is_active' => 1
);


$blocks['Footer_Block_Refunds_Info'] = array(
    'content'   => '<div class="threeNext">
                        <ul>
                            <li class="boldSize1">Refunds & Warranties</li>
                            <li>We offer a 30 day no question Return Policy</li>
                            <li>and minimum 12 month parts and labor</li>
                            <li>Warranty on all products.</li>
                        </ul>
                    </div>',
    'is_active' => 1
);
$storeId = Mage::app()->getStore()->getId();

foreach ($blocks as $identifier => $block) {

    if ( ( $cmsBlock = Mage::getModel('cms/block')->load( $identifier, 'identifier') )) {

        $cmsBlock->setContent($block['content'])
                ->save();
    } else {

        Mage::getModel('cms/block')
            ->setIdentifier($identifier)
            ->setContent($block['content'])
            ->setTitle($identifier)
            ->setIsActive($block['is_active'])
            ->setStores(array($storeId))
            ->save();

    }
}