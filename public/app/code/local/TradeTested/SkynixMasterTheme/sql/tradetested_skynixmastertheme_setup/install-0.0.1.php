<?php
/**
 * Created by Skynix Team
 * Date: 9/17/16
 * Time: 11:48
 */

/** @var  $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$DB_NAME_CONFIG = 'core_config_data';

$installer->run("

    UPDATE `" . $DB_NAME_CONFIG . "`  SET value=1 WHERE path='cms/wysiwyg/use_static_urls_in_catalog';
    UPDATE `" . $DB_NAME_CONFIG . "`  SET value='Skynix' WHERE path='design/package/name';
    UPDATE `" . $DB_NAME_CONFIG . "`  SET value='default' WHERE path='design/theme/template';
    UPDATE `" . $DB_NAME_CONFIG . "`  SET value='a:1:{s:18:\"_1474091236447_447\";a:2:{s:6:\"regexp\";s:73:\"iPhone|BlackBerry|Palm|Googlebot-Mobile|Windows Mobile|Android|Opera Mini\";s:5:\"value\";s:6:\"mobile\";}}' WHERE path='design/theme/template_ua_regexp';
    UPDATE `" . $DB_NAME_CONFIG . "`  SET value='default' WHERE path='design/theme/skin';
    UPDATE `" . $DB_NAME_CONFIG . "`  SET value='default' WHERE path='design/theme/layout';
    UPDATE `" . $DB_NAME_CONFIG . "`  SET value='a:1:{s:18:\"_1474091265220_220\";a:2:{s:6:\"regexp\";s:73:\"iPhone|BlackBerry|Palm|Googlebot-Mobile|Windows Mobile|Android|Opera Mini\";s:5:\"value\";s:6:\"mobile\";}}' WHERE path='design/theme/layout_ua_regexp';
    UPDATE `" . $DB_NAME_CONFIG . "`  SET value='default' WHERE path='design/theme/default';
    UPDATE `" . $DB_NAME_CONFIG . "`  SET value='a:1:{s:18:\"_1474091281204_204\";a:2:{s:6:\"regexp\";s:73:\"iPhone|BlackBerry|Palm|Googlebot-Mobile|Windows Mobile|Android|Opera Mini\";s:5:\"value\";s:6:\"mobile\";}}' WHERE path='design/theme/default_ua_regexp';
    UPDATE `" . $DB_NAME_CONFIG . "`  SET value='Trade Tested' WHERE path='design/head/default_title';
    UPDATE `" . $DB_NAME_CONFIG . "`  SET value='Trade Tested Logo' WHERE path='design/header/logo_alt';
    
");

$installer->endSetup();