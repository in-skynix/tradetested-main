<?php

/** @var  $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$DB_NAME_CONFIG = 'core_config_data';

$installer->run("

    UPDATE `" . $DB_NAME_CONFIG . "`  SET value='&copy; Copyright 2010-2016 Trade Tested Limited' WHERE path='design/footer/copyright';
       
");

$installer->endSetup();