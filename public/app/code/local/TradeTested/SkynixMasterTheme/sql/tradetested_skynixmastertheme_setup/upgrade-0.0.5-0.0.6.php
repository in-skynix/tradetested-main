<?php

/** @var  $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$pages = [

    [
        'identifier'    => 'shipping',
        'content'       => '
            <div class="content-wrapper cms right-section">
                <div class="cms-section">
                    <h1>Shipping Information</h1>
                    <p style="margin-bottom:10px;">We offer the best shipping rates in New Zealand for all our products. Depending on where you are, a range of options are displayed on each product page, shopping cart and the checkout before you make a payment.</p>
                    <p style="margin-bottom:10px;">All of our products are in stock at our Auckland warehouse, unless clearly stated on the product page and checkout. The shipping times below are estimated based on 80% of our deliveries.</p>
                    <p style="margin-bottom:25px;"><strong>Pickup</strong></strong><br />Pickup is always welcome from <a href="/auckland-store">our store</a> just around the corner from Auckland Airport. When you select this option, we\'ll email the address and pickup instructions after you place your order.</p>
                </div>
                <div class="cms-section">
                    <div class="category-title" style="margin-bottom:8px;"><h2>How It Works</h2></div>
                    <p style="margin-bottom:10px;"><img src="{{media url="wysiwyg/Shipping-Diagram.jpg"}}" alt="Delivery Diagram" height="304" width="740" style="margin-top:10px;margin-bottom:10px;"/></p>
                    <p style="margin-bottom:10px;"><strong>Processing time</strong><br />For most of our orders, the standard processing time is less than 1 day. For larger products, that have lots of little parts it can take 2-5 days (scaffolding, trailers, etc).</p>
                    <p style="margin-bottom:25px;"><strong>Shipping time</strong><br />All our deliveries are taken care of by third party delivery agents - Courierpost, Toll Transport and Peter Baker Transport (PBT). Once a product is released to the delivery agent, you’ll receive a shipping confirmation email with updated tracking information.</p>
                </div>
                <div class="cms-section">
                    <div class="category-title" style="margin-bottom:8px;"><h2>Options For Products Less Than 30kg</h2></div>
                    <p style="margin-bottom:10px;"><strong>Courierpost</strong><br />We use Courierpost to deliver all small items around the country. This normally takes from 2 to 3 days after processing at our Auckland warehouse.</p>
                    <p style="margin-bottom:25px;"><strong>Courierpost Rural Delivery</strong><br />If you live out of town, your item will leave our warehouse with Courierpost. It then gets handed over to your local rural delivery driver. As it has to travel a little further, this can take 3 to 5 days to arrive after processing at our Auckland warehouse.</p>
                </div>
                <div class="cms-section">
                    <div class="category-title" style="margin-bottom:8px;"><h2>Options For Products Greater Than 30kg</h2></div>
                    <p style="margin-bottom:10px;"><strong>Door to Door</strong><br />Express Door to Door service is available to non rural delivery addresses. Depending on how bulky your order is we either use Toll Transport or PBT. For North Island deliveries this normally takes 1 to 2 days after processing. South Island deliveries are around 3 to 5 days.</p>
                    <p style="margin-bottom:10px;"><strong>Rural Door to Door</strong><br />We offer Door to Door service to rural delivery addresses for most products. There is a $50 surcharge everywhere except West Coast, Otago and Southland which cost $100 per order, regardless of the size of your order. For North Island deliveries this normally takes 3 to 7 days after processing. South Island deliveries are around 5 to 10 days. Delivery is not possible to some remote locations, if your order is affected we will contact you with alternative options.</p>
                    <p style="margin-bottom:10px;"><strong>Auckland Metro Door to Door</strong><br />As our main distribution warehouse is in Auckland, we have a special rate for the Auckland Metro area. We define this as central Auckland stretching to Albany, Henderson, Papakura and Botany. Occasionally there are exceptions, depending on our third party carrier.</p>
                    <p style="margin-bottom:25px;"><strong>Door to Depot</strong><br />This is an especially popular option for rural customers. Depending on how bulky your order is we use either Toll Transport or PBT. For all depot deliveries you’ll get sent an email with the pickup address. When your order is ready for collection depot staff will give you a call to let you know. All rural depot locations are available here and are listed below.</p>
                </div>
                <div class="cms-section">
                    <div class="category-title" style="margin-bottom:8px;"><h2>Frequently Asked Questions</h2></div>
                    <p style="margin-bottom:10px;"><strong>How much does my product weigh?</strong><br />Weights are listed in each product description under specifications. If you’re buying multiple items with different weights, they all get bundled together with the largest product for a single delivery.</p>
                    <p style="margin-bottom:10px;"><strong>How do I track my order?</strong><br />Our delivery agents all have websites where it’s easy to track your order. Simply enter the tracking number we sent you in the dispatch email.</p>
                    <p style="margin-bottom:10px;"><a href="http://www.courierpost.co.nz/track/track-and-trace/" target="_blank">Courierpost tracking</a><br />
                        <a href="http://pbt.com/" target="_blank">PBT tracking</a><br />
                        <a href="http://fasttracker-nz.tollgroup.com/" target="_blank">Toll Transport tracking</a></p>
                    <p style="margin-bottom:10px;"><strong>I missed my delivery!</strong><br />Unless requested, all our deliveries require a signature to confirm you have received the product. If no one is there to accept the delivery you can organise re-delivery by following one of the links below.</p>
                    <p style="margin-bottom:10px;"><strong>What should I do if nothing arrives?</strong><br />Try tracking the delivery online, using the tracking details we have sent you. If the order is showing as ‘delivered’ but you have not received anything please get in touch with the shipping carrier directly (refer to the phone number from your dispatch confirmation email). There is likely a reason that the order has been taken back to the depot.</p>
                    <p style="margin-bottom:10px;"><strong>What happens if my order arrives damaged?</strong><br />If you find your parcel damaged upon arrival please do not sign for it. By signing for delivery you agree to accept the goods as they are. Instead, get in touch with us as soon as you have been made aware of any damage so we can send you out a replacement. You must notify us of any damage within 24 hours of delivery.</p>
                    <p style="margin-bottom:10px;"><strong>How do I change the delivery address I gave you?</strong><br />We can’t make changes to the shipping address once an order has been placed. But if you have an urgent problem <a href="/contact-us">contact us</a>.</p>
                    <p style="margin-bottom:0"><strong>Where are the depots located?</strong></p>
                    <div>
                        <div style="float:left;width:40%;">
                            <p style="margin-bottom:5px;">
                                North Island
                            <ul class="list-float">
                                <li>Whangarei</li>
                                <li>Auckland</li>
                                <li>Hamilton</li>
                                <li>Tauranga</li>
                                <li>Rotorua</li>
                                <li>Taupo (Toll only)</li>
                                <li>Napier/Hastings</li>
                                <li>Gisborne (Toll only)</li>
                                <li>New Plymouth</li>
                                <li>Wanganui</li>
                                <li>Palmerston North</li>
                                <li>Wellington</li>
                            </ul>
                            </p>
                        </div>
                        <div style="float:right;width:50%">
                            <p style="margin-bottom:5px;">
                                South Island
                            <ul class="list-float">
                                <li>Blenheim</li>
                                <li>Nelson</li>
                                <li>Greymouth</li>
                                <li>Christchurch</li>
                                <li>Ashburton</li>
                                <li>Timaru</li>
                                <li>Oamaru (Toll only)</li>
                                <li>Dunedin</li>
                                <li>Cromwell</li>
                                <li>Queenstown (Toll only)</li>
                                <li>Invercargill</li>
                            </ul>
                            </p>
                        </div>
                    </div>
                    </p>
                </div>
            
                <div style="clear:both;"/>
            </div>
            </div>
            <br />
            </div>'
    ],
    [
        'identifier'    => 'warranty-policy',
        'content'       => '<div class="content-wrapper cms right-section">
                                <div class="cms-section">
                                    <h1>Warranty Policy</h1>
                                    <p style="margin-bottom:5px;">Everything we sell comes with a minimum 12 month parts and labour warranty. For some items we even offer a warranty of up to 20 years, so be sure to check.</p>
                                    <p style="margin-bottom:5px;">We will always endeavour to repair or replace faulty goods within their warranty period. Exception may be made in the case of damage to or misuse of the item. This also includes negligent operation, inadequate maintenance, or unauthorised repairs and modifications. Limits in cover will apply for commercial applications. Warranties are not transferable. Freight damage or missing parts must be reported to us within 24 hours of delivery.</p>
                                    <p style="margin-bottom:5px;">All warranty claims must first be approved by a Trade Tested representative. In the case of repair, you will be required to return the unit to one of our nationwide service agents. We will then do our best to repair or replace the item, and get it back to you within a reasonable timeframe.</p>
                                    <p style="margin-bottom:5px; margin-top:15px;">For warranty questions or support please <a title="contact us" href="/contact-us">contact us</a>.</p>
                                </div>
                            </div>'
    ],
    [
        'identifier'    => 'refund-policy',
        'content'       => '<div class="content-wrapper cms right-section">
                                <div class="cms-section refund-cms">
                                    <h1>30 Day Returns Policy</h1>
                                    <div style="float:left;" class="refund-left">
                                        <p style="margin-bottom:5px;">Our 30 day money back guarantee gives you time to make sure your purchase is perfect. If you need to return it for any reason, we\'ll happily provide you with an exchange or full refund.</p>
                                        <p style="margin-bottom:10px;">The 30 day period starts from the date of delivery or collection and items must be received back before the end of the 30 day period. All returns must come back in unused, resaleable condition in original packaging and shipping costs are non-refundable. If you are not completely satisfied with your purchase, the best thing to do is <a title="contact us" href="/contact-us">contact us</a> and we\'ll be happy to do our best to resolve any issue.</p>
                                        <p style="margin-bottom:5px;">Return options:</p>
                                        <p style="margin-bottom:5px;">1. Return the item to our <a href="/auckland-store">Auckland Store</a>.</p>
                                        <p style="margin-bottom:10px;">2. Return the item to our warehouse via a shipment method of your choice. Please <a title="contact us" href="/contact-us">contact us</a> for return address details.</p>
                                        <p style="margin-bottom:5px;">Please make sure you <a title="contact us" href="/contact-us">contact us</a> first for an authorisation code before returning your product.</p>
                                    </div>
                                    <div style="float:right; height:60px;" class="refund-cms-img">
                                        <img src="{{media url="wysiwyg/30-Day-Returns.gif"}}" alt="30 Day Money Back Guarantee">
                                    </div>
                                </div>
                            </div>'
    ],
    [
        'identifier'    => 'careers',
        'content'       => '<div class="right-section careers-wrap cms">
                            <h1>Careers</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in mi arcu. Praesent eu turpis diam. Fusce sed maximus turpis. Duis eu risus interdum, tristique sapien non, mattis purus. Ut non dui sed orci dignissim commodo. Duis magna sem, iaculis a egestas at, aliquet sit amet diam. Proin ullamcorper sapien in nunc tempus, ut sollicitudin diam mollis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                            <img alt="Trade Tested careers" src="{{media url="wysiwyg/auckland-store/career_main.png"}}" />
                            <h2>Our Culture And Values</h2>
                            <p>Sed nec suscipit mi. Nam id tincidunt leo. Sed posuere, leo a tincidunt aliquet, felis turpis laoreet augue, id laoreet augue dolor a ante. Mauris convallis turpis ut risus mollis hendrerit. Vivamus placerat congue urna, sagittis scelerisque lorem molestie eu. In tempus imperdiet sapien, eget bibendum leo imperdiet convallis. Vivamus at vestibulum nulla. Integer ac pharetra ex, eget bibendum diam. Pellentesque posuere ullamcorper rutrum. Donec sapien est, sodales vel ex sit amet, consectetur eleifend lorem. Sed lobortis euismod tortor sit amet feugiat. Integer commodo accumsan facilisis. In posuere orci id nulla congue cursus vel in ipsum.</p>
                            <h2>Current Vacancies</h2>
                            <p>We always want to hear from passionate, experienced people. If you think you&rsquo;d fit in well at Trade Tested then send your CV and cover letter to <a>info@tradetested.co.nz</a>.</p>
                            <a class="trade-button">View Current Vacancies</a></div>'
    ]

];

$connection = $installer->getConnection();
foreach ( $pages as $page ) {

    if ( ( $cmsPage = Mage::getModel('cms/page')->load( $page['identifier'], 'identifier') ) ) {

        $cmsPage->setContent( $page['content'] )->save();

    }

}
$installer->endSetup();