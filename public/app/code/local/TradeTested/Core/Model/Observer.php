<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 17/08/15
 * Time: 15:19
 */
class TradeTested_Core_Model_Observer
{
    public function controllerActionLayoutLoadBefore(Varien_Event_Observer $observer)
    {
        /** @var Mage_Core_Controller_Varien_Action $action */
        $action = $observer->getEvent()->getAction();
        if ($action->getRequest()->isAjax()) {
            /** @var Mage_Core_Model_Layout_Update $update */
            $update = $observer->getEvent()->getLayout()->getUpdate();
            foreach ($update->getHandles() as $_handle) {
                $update->addHandle($_handle."_AJAX");
            }
        }
        if ($popup = $action->getRequest()->getParam('popup')) {
            $update = $observer->getEvent()->getLayout()->getUpdate();
            foreach ($update->getHandles() as $_handle) {
                $update->addHandle($_handle."_POPUP_".$popup);
            }
        }
    }

    /**
     * Clear data from Flash session, which is intended to retain data for only one full request.
     * @param Varien_Event_Observer $observer
     */
    public function controllerFrontSendResponseAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_Core_Controller_Varien_Front $controller */
        $controller = $observer->getEvent()->getFront();
        $hop = (
            ($controller->getResponse()->getHttpResponseCode() === 200)
            && Mage::app()->getLayout()->getBlock('header')
        ) ? 1 : 0;
        Mage::getModel('tradetested_core/flash')->registerHop($hop);
    }

    /**
     * Add handles specific to CMS pages, for use in layout XML
     *
     * @param Varien_Event_Observer $observer
     */
    public function cmsPageRender(Varien_Event_Observer $observer)
    {
        $identifier = $observer->getEvent()->getPage()->getIdentifier();
        $identifier = str_replace('-', '_', $identifier);
        /** @var Mage_Core_Controller_Front_Action $action */
        $action = $observer->getEvent()->getControllerAction();
        $action->getLayout()->getUpdate()->addHandle('cms_page_'.$identifier);
    }

    /**
     * When saving an object with a GUID, check for a new GUID and convert to guid_bin
     * 
     * Will ignore any changes to an existing GUID.
     * 
     * @param Varien_Event_Observer $observer
     * @return bool
     */
    public function saveGuid(Varien_Event_Observer $observer)
    {
        Mage::helper('tradetested_core')->convertGuidToBin($observer->getEvent()->getDataObject());
    }

    /**
     * When loading an object with a GUID, convert binary format to readable.
     * 
     * We don't add guid as an attribute, so magento won't try to save it.
     * 
     * @param Varien_Event_Observer $observer
     */
    public function loadGuid(Varien_Event_Observer $observer)
    {
        Mage::helper('tradetested_core')->convertBinToGuid($observer->getEvent()->getDataObject());
    }

    /**
     * Don't add guid_bin to flat table. 
     * 
     * Magento doesn't like using data-types that are not 'standard'
     * 
     * @param Varien_Event_Observer $observer
     */
    public function catalogProductFlatPrepareColumns(Varien_Event_Observer $observer)
    {
        /** @var Varien_Object $columns */
        $columns = $observer->getEvent()->getColumns();
        $columnsArray = $columns->getData('columns');
        unset($columnsArray['guid_bin']);
        $columns->setData('columns', $columnsArray);
    }
    
    public function coreBlockAbstractToHtmlAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_Core_Block_Abstract $block */
        $block = $observer->getEvent()->getBlock();
        
        if ($config = Mage::helper('tradetested_core/javascript')->getJsComponentConfig($block)) {
            /** @var TradeTested_Core_Block_Javascript $jsBlock */
            $jsBlock = $block->getLayout()->getBlock('javascript');
            foreach($config as $_config) {
                $jsBlock->addComponent($_config);
            }
        }
    }
}