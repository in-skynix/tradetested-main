<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 16/09/15
 * Time: 17:38
 */
class TradeTested_Core_Model_Urlcrypt extends Varien_Crypt_Abstract
{
    protected $_table = "1bcd2fgh3jklmn4pqrstAvwxyz567890";
    protected $_key = "";
    protected $_cipher = MCRYPT_RIJNDAEL_128;
    protected $_mode = MCRYPT_MODE_CBC;

    public function __construct($key)
    {
        parent::__construct();
        $this->_key = $key;
    }

    public function encode($str)
    {
        $n = strlen($str) * 8 / 5;
        $arr = str_split($str, 1);
        $m = "";
        foreach ($arr as $c) {
            $m .= str_pad(decbin(ord($c)), 8, "0", STR_PAD_LEFT);
        }
        $p = ceil(strlen($m) / 5) * 5;
        $m = str_pad($m, $p, "0", STR_PAD_RIGHT);
        $newstr = "";
        for ($i = 0; $i < $n; $i++) {
            $newstr .= $this->_table[bindec(substr($m, $i * 5, 5))];
        }
        return $newstr;
    }

    public function decode($str)
    {
        $n = strlen($str) * 5 / 8;
        $arr = str_split($str, 1);
        $m = "";
        foreach ($arr as $c) {
            $m .= str_pad(decbin(array_search($c, str_split($this->_table, 1))), 5, "0", STR_PAD_LEFT);
        }
        $oldstr = "";
        for ($i = 0; $i < floor($n); $i++) {
            $oldstr .= chr(bindec(substr($m, $i * 8, 8)));
        }
        return $oldstr;
    }

    public function encrypt($str)
    {
        if ($this->_key === "") {
            throw new \Exception('No key provided.');
        }
//        $key = pack('H*', $this->_key); //converts hex to str. but our key is already str
        $iv_size = mcrypt_get_iv_size($this->_cipher, $this->_mode);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $str = utf8_encode($str);
        $ciphertext = mcrypt_encrypt($this->_cipher, $this->_key, $str, $this->_mode, $iv);
        $ciphertext = $iv . $ciphertext;
        return self::encode($ciphertext);
    }

    public function decrypt($str)
    {
        if ($this->_key === "") {
            throw new \Exception('No key provided.');
        }
//        $key = pack('H*', $this->_key); //converts hex to str. but our key is already str
        $str = self::decode($str);
        $iv_size = mcrypt_get_iv_size($this->_cipher, $this->_mode);
        $iv_dec = substr($str, 0, $iv_size);
        $str = substr($str, $iv_size);
        $str = mcrypt_decrypt($this->_cipher, $this->_key, $str, $this->_mode, $iv_dec);
        // http://jonathonhill.net/2013-04-05/write-tests-you-might-learn-somethin/
        return rtrim($str, "\0");
    }
}