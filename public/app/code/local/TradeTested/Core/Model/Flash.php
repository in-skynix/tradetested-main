<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 03/03/2016
 * Time: 11:11
 * 
 * Flash Session. A session singleton that retains data only until the next time a page is fully loaded.
 */
class TradeTested_Core_Model_Flash extends Mage_Core_Model_Session_Abstract
{
    public function __construct()
    {
        $this->init('flash_'.Mage::app()->getStore()->getWebsite()->getCode());
    }

    /**
     * Register that Magento has sent a response.
     * 
     * All data added in this request have a flag for set_in_this_request. Unset these.
     * 
     * Clear any values with a hop value of less than zero after decrementing hop.
     * 
     * @param int $hop usually 1 for a 200 response, 0 for a redirect
     */
    public function registerHop(int $hop)
    {
        foreach(parent::getData() as $_key => $_rawData) {
            if (is_array($_rawData) && isset($_rawData['value'])) {
                if ($_rawData['set_in_this_request'] ?? false) {
                    $_rawData['set_in_this_request'] = false;
                } else {
                    $_rawData['hop'] = intval($_rawData['hop']??0) - $hop;
                }
                if ($_rawData['hop'] < 0) {
                    $this->unsetData($_key);
                } else {
                    parent::setData($_key, $_rawData);
                }
                
            }
        }
    }

    /**
     * Add another hop onto some data in the flash.
     * 
     * @param string $key
     * @param int $hop
     */
    public function addHop(string $key, int $hop = 1)
    {
        $rawData = parent::getData($key);
        if ($rawData !== null) {
            parent::setData($key, array_merge($rawData, ['hop' => (intval($rawData['hop']) + $hop)]));
        }
    }

    /**
     * Get actual data value without hop
     *
     * @param string $key
     * @param bool $clear
     * @return mixed
     */
    public function getData($key='', $clear = false)
    {
        $data = parent::getData($key, $clear);
        return (is_array($data) && isset($data['value']))? $data['value'] : $data;
    }

    /**
     * Set data with hop and just set flag
     * 
     * @param array|string $key
     * @param null $value
     * @param int $hop
     * @return Varien_Object
     */
    public function setData($key, $value=null, $hop = 1)
    {
        if (is_array($key)) {
            foreach($key as $_key => $_value) {
                $this->clear();
                $this->setData($_key, $_value, $hop);
            }
        } else {
            return parent::setData($key, ['value' => $value, 'hop' => $hop, 'set_in_this_request' => true]);
        }
    }
}