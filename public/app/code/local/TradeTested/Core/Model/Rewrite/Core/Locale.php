<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 19/02/2016
 * Time: 10:53
 */
class TradeTested_Core_Model_Rewrite_Core_Locale extends Mage_Core_Model_Locale
{
    /**
     * Create Zend_Currency object for current locale
     *
     * Zend_Currency has huge performance issues with loading currency data from locale.
     *
     * Zend_Locale_Data::getContent calls self::_getFile ~200 times when getting currency short name from locale etc.
     *
     * Here we hard-code the name and codes for the most common currencies we use.
     *
     * @param   string $currency
     * @return  Zend_Currency
     */
    public function currency($currency)
    {
        if (!isset(self::$_currencyCache[$this->getLocaleCode()][$currency])) {

            if (($currency == 'NZD') && ($this->getLocale()->__toString() == 'en_NZ')) {
                $currencyObject = new Zend_Currency(array(
                    'currency'  => 'NZD',
                    'name'      => 'New Zealand Dollar',
                    'symbol'    => '$',
                ), $this->getLocale());
            } elseif (($currency == 'AUD') && ($this->getLocale()->__toString() == 'en_AU')) {
                $currencyObject = new Zend_Currency(array(
                    'currency'  => 'AUD',
                    'name'      => 'Australian Dollar',
                    'symbol'    => '$',
                ), $this->getLocale());
            }

            if (isset($currencyObject) && $currencyObject) {
                $options = array();
                $options = new Varien_Object($options);
                Mage::dispatchEvent('currency_display_options_forming', array(
                    'currency_options' => $options,
                    'base_code' => $currency
                ));

                $currencyObject->setFormat($options->toArray());
                self::$_currencyCache[$this->getLocaleCode()][$currency] = $currencyObject;
            } else {
                return parent::currency($currency);
            }
        }
        return self::$_currencyCache[$this->getLocaleCode()][$currency];
    }
}
