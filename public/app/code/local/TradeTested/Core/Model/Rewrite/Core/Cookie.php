<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 06/02/15
 * Time: 10:38
 */
class TradeTested_Core_Model_Rewrite_Core_Cookie extends Mage_Core_Model_Cookie
{
    /**
     * Cookies are always secure in production
     *
     * @return bool
     */
    public function isSecure()
    {
        return (
            !Mage::getIsDeveloperMode()
            && (strpos(Mage::getStoreConfig('web/unsecure/base_url'),'https://') === 0)
        );
    }
}