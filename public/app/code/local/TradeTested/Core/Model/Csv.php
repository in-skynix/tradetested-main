<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 21/06/16
 * Time: 2:15 PM
 */
class TradeTested_Core_Model_Csv
{
    protected $_fh;
    protected $_filename;
    protected $_headers = [];
    protected $_totalRows;
    
    public function open(string $filename, array $headers, $mode = 'w')
    {

        $this->_filename = $filename;
        if (substr($filename, -3) == '.gz') {
            $this->_fh = gzopen($this->_filename,$mode);
        } else {
            $this->_fh = fopen($this->_filename,$mode);
        }
        $this->_headers = $headers;
        return $this;
    }

    /**
     * @param      $collection
     * @param bool $addHeaders
     *
     * @return $this
     */
    public function writeCollection($collection, $addHeaders = true, $chunkSize = 0)
    {
        if ($addHeaders) {
            $headers = [];
            foreach ($this->_headers as $_header) {
                $headers[] = is_array($_header) ? $_header[0] : $_header;
            }
            fputcsv($this->_fh, $headers);
        }
        
        if ($chunkSize) {
            $page = 1;
            /** @var Varien_Data_Collection_Db $collection */
            $collection->setPageSize($chunkSize);
            $lastPage = $collection->getLastPageNumber();
            while ($page <= $lastPage) {
                $collection->setCurPage($page++)->clear()->load();
                $this->writeCollection($collection, false);
            }
        } else {
            foreach ($collection as $_item) {
                fputcsv($this->_fh, array_values($this->_convertData($_item, 'csv')));
            }
        }
        return $this;
    }
    
    public function withRows($callback, $addlData = [])
    {
        $rows = 0;
        $importHeaders = fgetcsv($this->_fh, 1024);
        while (!feof($this->_fh)) {
            $line = fgetcsv($this->_fh, 1024);
            $data = [];
            foreach ($line as $_key => $_val) {
                $data[$importHeaders[$_key]] = $_val;
            }
            if (count($data)) {
                $data = array_merge($addlData, $this->_convertData($data, 'object'));
                call_user_func($callback, $data, $rows+1);
                $rows++;
            }
        }
        return $rows;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->_filename;
    }

    /**
     * @return string
     */
    public function getCSVStream()
    {
        return stream_get_contents($this->_fh, -1, 0);
    }
    
    public function getTotalRows()
    {
        if (!$this->_totalRows) {
            rewind($this->_fh);
            fgetcsv($this->_fh, 1024);
            $x = 0;
            while (!feof($this->_fh)) {
                $line = fgetcsv($this->_fh, 1024);
                if(count($line)) {
                    $x++;
                }
            }
            rewind($this->_fh);
            $this->_totalRows = $x;
        }
        return $this->_totalRows;
    }

    /**
     * @param array|Varien_Object $input
     * @param string              $toFormat
     * 
     * @return array
     */
    protected function _convertData($input, string $toFormat)
    {
        $data = [];
        $srcIndex = ($toFormat == 'csv') ? 1 : 0;
        $destIndex = ($toFormat == 'csv') ? 0 : 1;
        $isObj = $input instanceof Varien_Object;
        foreach ($this->_headers as $_header) {
            $srcAttr = is_array($_header) ? $_header[$srcIndex] : $_header;
            $destAttr = is_array($_header) ? $_header[$destIndex] : $_header;
            $data[$destAttr] = $isObj ? $input->getDataUsingMethod($srcAttr) : $input[$srcAttr];
        }
        return $data;
    }

    public function __destruct() {
        if ($this->_fh) {
            fclose($this->_fh);
        }
    }
}