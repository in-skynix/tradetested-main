<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 22/06/16
 * Time: 3:33 PM
 */
abstract class TradeTested_Core_Model_Task_Abstract extends Mage_Core_Model_Abstract
{
    abstract public function run($args);
}