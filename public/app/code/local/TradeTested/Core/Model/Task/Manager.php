<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 22/06/16
 * Time: 11:30 AM
 */
class TradeTested_Core_Model_Task_Manager extends Mage_Core_Model_Abstract
{
    const DEFAULT_TIMEOUT = 2.5;
    /** @var  Credis_Client */
    protected $_redis;
    protected $_lastPercent;
    
    public function getTaskId()
    {
        if (!$this->getData('task_id')) {
            $this->setData('task_id', uniqid());
        }
        return $this->getData('task_id');
    }
    
    public function schedule()
    {
        Mage::helper('tradetested_messaging')->sendMessage('tasks', $this->getData());
    }
    
    public function run() {
        $name = $this->getTaskName();
        /** @var TradeTested_Core_Model_Task_Abstract $task */
        $task = Mage::getModel($name);
        if (!($task instanceof TradeTested_Core_Model_Task_Abstract)) {
            throw new Mage_Adminhtml_Exception($name.' is not a valid task');
        }
        return $task->run($this->getArguments());
    }

    public function updateStatus($note, $percent = null, $force = false)
    {
        $note = str_replace('{totalRecords}', $this->getTotalRecords(), $note);
        $this->_connect();
        if ($this->getTotalRecords() && ($percent < 0)) {
            $percent = -$percent / $this->getTotalRecords();
        }
        $percent = round($percent*100, $this->getPrecision());
        if ($force || ($percent != $this->_lastPercent)) {
            $this->_lastPercent = $percent;
            $this->_redis->publish(
                'task_manager', json_encode(['task_id' => $this->getTaskId(), 'percent' => $percent, 'note' => $note])
            );
        }
        return $this;
    }

    public function sub($callback)
    {
        $this->_connect();
        $this->_redis->setReadTimeout(0)->subscribe(['task_manager'], $callback);
    }
    
    protected function _connect()
    {
        if ($this->_redis) {
            return;
        }
        $config        = Mage::getConfig()->getNode('global/cache/backend_options');
        $host          = ("{$config->descend('server')}" ?: '127.0.0.1');
        $port          = ((int)$config->descend('port') ?: 6379);
        $pass          = (string)($config->descend('password') ?: '');
        $timeout       = (float)($config->descend('read_timeout') ?: self::DEFAULT_TIMEOUT);
        $persistent    = '1';
        $this->_redis  = new Credis_Client($host, $port, $timeout, $persistent);
        if (!empty($pass)) {
            $this->_redis->auth($pass) or Zend_Cache::throwException('Unable to authenticate with the redis server.');
        }
    }
}
