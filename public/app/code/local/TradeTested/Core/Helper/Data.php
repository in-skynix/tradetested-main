<?php
class TradeTested_Core_Helper_Data extends Mage_Core_Helper_Data
{
    public function getEnv()
    {
        if(!($env = (string)Mage::getConfig()->getNode('global/environment'))) {
            $env = 'production';
        }
        return $env;
    }

    /**
     * Generate a UUID V4
     * 
     * http://stackoverflow.com/questions/2040240/php-function-to-generate-v4-uuid
     * 
     * @return string
     */
    public function generateGuid()
    {
        $data = openssl_random_pseudo_bytes(16);
        assert(strlen($data) == 16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    /**
     * When saving an object with a GUID, check for a new GUID and convert to guid_bin
     *
     * Will ignore any changes to an existing GUID.
     * 
     * @param Varien_Object $object
     * @return bool
     */
    public function convertGuidToBin(Varien_Object $object)
    {
        if ($object->getOrigData('guid') || $object->getId()) {
            return false;
        }
        $guid = $object->getData('guid') ?? Mage::helper('tradetested_core')->generateGuid();
        $object->setData('guid_bin', $this->guidToBin($guid));
    }

    /**
     * @param string $guid
     * @return string
     */
    public function guidToBin(string $guid)
    {
        $guid = str_replace('-', '', $guid);
        return hex2bin(substr($guid, 6, 2)).hex2bin(substr($guid, 4, 2)).hex2bin(substr($guid, 2, 2))
            .hex2bin(substr($guid, 0, 2)).hex2bin(substr($guid, 10, 2)).hex2bin(substr($guid, 8, 2))
            .hex2bin(substr($guid, 14, 2)).hex2bin(substr($guid, 12, 2)).hex2bin(substr($guid, 16, 16));
    }

    /**
     * When loading an object with a GUID, convert binary format to readable.
     *
     * We don't add guid as an attribute, so magento won't try to save it.
     * 
     * @param Varien_Object $object
     */
    public function convertBinToGuid(Varien_Object $object)
    {
        if ($object->getData('guid_bin')) {
            $bin = $object->getData('guid_bin');
            $guid =  bin2hex(substr($bin, 3, 1)).bin2hex(substr($bin, 2, 1)).bin2hex(substr($bin, 1, 1))
                .bin2hex(substr($bin, 0, 1)).'-'.bin2hex(substr($bin, 5, 1)).bin2hex(substr($bin, 4, 1)).'-'
                .bin2hex(substr($bin, 7, 1)).bin2hex(substr($bin, 6, 1)).'-'.bin2hex(substr($bin, 8, 2)).'-'
                .bin2hex(substr($bin, 10, 6));
            $object->setData('guid', $guid);
        }
    }

    /**
     * Returns string with newline formatting converted into HTML paragraphs.
     *
     * @param string $string String to be formatted.
     * @param boolean $lineBreaks When true, single-line line-breaks will be converted to HTML break tags.
     * @param boolean $xml When true, an XML self-closing tag will be applied to break tags (<br />).
     * @return string
     */
    function nl2p($string, $lineBreaks = true, $xml = true)
    {
        if (!$string) {
            return '';
        }
        // Remove existing HTML formatting to avoid double-wrapping things
        $string = preg_replace(array('{</?p>}i', '{<br\s*/?>}i'), '', $string);
        // It is conceivable that people might still want single line-breaks
        // without breaking into a new paragraph.
        if ($lineBreaks == true) {
            return '<p>'.preg_replace(array("/(\n\s*\n+)/i", "/([^>])\n([^<])/i"), array("</p>\n<p>", "$1<br".($xml == true ? ' /' : '').">$2"), trim($string)).'</p>';
        } else {
            return '<p>'.preg_replace("/(\n+[\n\s]*)/i", "</p>\n<p>", trim($string)).'</p>';
        }
    }
    
    /**
     * Get object identifier by guid
     *
     * @param Mage_Core_Model_Abstract $object
     * @param string $guid
     * @return int|false
     */
    public function getIdByGuid(Mage_Core_Model_Abstract $object, $guid)
    {
        /** @var Mage_Core_Model_Resource_Abstract $resource */
        $resource = $object->getResource();
        $adapter = $resource->getReadConnection();

        $select = $adapter->select()
            ->from($resource->getEntityTable(), $object->getIdFieldName())
            ->where('guid_bin = :guid');

        $bind = array(':guid' => $this->guidToBin($guid));
        return $adapter->fetchOne($select, $bind);
    }

    /**
     * @param string $guid
     *
     * @return Mage_Catalog_Model_Product
     */
    public function loadProductByGuid(string $guid)
    {
        $product = Mage::getModel('catalog/product');
        return ($id = $this->getIdByGuid($product, $guid)) ? $product->load($id) : $product->setGuid($guid);
    }
}
