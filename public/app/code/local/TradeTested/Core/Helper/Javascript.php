<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 15/06/16
 * Time: 2:02 PM
 */
class TradeTested_Core_Helper_Javascript extends Mage_Core_Helper_Abstract
{
    /**
     * Cache group Tag
     */
    const CACHE_GROUP = 'block_html';

    /**
     * @param Mage_Core_Block_Abstract $block
     *
     * @return array|bool|mixed
     */
    public function getJsComponentConfig(Mage_Core_Block_Abstract $block)
    {
        if (!($config = $this->_loadJsCache($block))) {
            $config = $block->getJsComponentConfig();
            $this->_saveJsCache($block, $config);
        }
        if ($uncachedConfig = $block->getUncachedJsComponentConfig()) {
            $config = $config ? $config : [];
            $config = array_merge_recursive($config, $uncachedConfig);
        }
        return $config;
    }

    /**
     * @param Mage_Core_Block_Abstract $block
     *
     * @return bool|mixed
     */
    protected function _loadJsCache(Mage_Core_Block_Abstract $block)
    {
        if (is_null($block->getCacheLifetime()) || !Mage::app()->useCache(self::CACHE_GROUP)) {
            return false;
        }
        $cacheKey = $block->getCacheKey().'_js';
        return  json_decode(Mage::app()->loadCache($cacheKey), true);
    }

    /**
     * @param Mage_Core_Block_Abstract $block
     * @param                          $data
     *
     * @return $this|bool
     */
    protected function _saveJsCache(Mage_Core_Block_Abstract $block, $data)
    {
        if (is_null($block->getCacheLifetime()) || !Mage::app()->useCache(self::CACHE_GROUP)) {
            return false;
        }
        $cacheKey = $block->getCacheKey()."_js";
        $tags = $block->getCacheTags();
        $data = json_encode($data);
        Mage::app()->saveCache($data, $cacheKey, $tags, $block->getCacheLifetime());
        return $this;
    }
}