<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 30/03/2016
 * Time: 13:34
 */
class TradeTested_Core_Block_Rewrite_Core_Messages extends Mage_Core_Block_Messages
{
    public function getGroupedHtml()
    {
        if (Mage::registry('messages_block_out')) {
            return '';
        }
        Mage::register('messages_block_out', true);
        $types = [
            Mage_Core_Model_Message::ERROR,
            Mage_Core_Model_Message::WARNING,
            Mage_Core_Model_Message::NOTICE,
            Mage_Core_Model_Message::SUCCESS
        ];
        $messagesJson = [];
        $html = '';
        foreach ($types as $type) {
            if ($messages = $this->getMessages($type)) {
                $messagesJson[$type] = [];
                if (!$html) {
                    $html .= '<' . $this->_messagesFirstLevelTagName . ' class="messages">';
                }
                $html .= '<' . $this->_messagesSecondLevelTagName . ' class="' . $type . '-msg">';
                $html .= '<' . $this->_messagesFirstLevelTagName . '>';

                foreach ($messages as $message) {
                    $messagesJson[$type][] = str_replace('&quot;', '"', $message->getText()); //allow unescaped quote, but leave dangerous things quoted.
                    $html .= '<' . $this->_messagesSecondLevelTagName . '>';
                    $html .= '<' . $this->_messagesContentWrapperTagName . '>';
                    $html .= ($this->_escapeMessageFlag) ? $this->escapeHtml($message->getText()) : $message->getText();
                    $html .= '</' . $this->_messagesContentWrapperTagName . '>';
                    $html .= '</' . $this->_messagesSecondLevelTagName . '>';
                }
                $html .= '</' . $this->_messagesFirstLevelTagName . '>';
                $html .= '</' . $this->_messagesSecondLevelTagName . '>';
            }
        }
        if ($html) {
            $html .= '</' . $this->_messagesFirstLevelTagName . '>';
        }
        $messagesJson = json_encode($messagesJson);

        if (Mage::app()->getStore()->isAdmin()) {
            return $html;
        }

        return <<<HTML
<div id="messages_block">
 {$html}
</div>
<script type="text/javascript">
    _components([{ref: null, name: 'MessagesBlock', args: [{container: document.getElementById('messages_block'), messages: {$messagesJson}}]}])
</script>
HTML;

    }
}