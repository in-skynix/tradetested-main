<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 13/07/15
 * Time: 08:51
 */
class TradeTested_Core_Block_Rewrite_Cms_Block extends Mage_Cms_Block_Block
{
    /**
     * Add store code to cache key info, so CMS Blocks can be cached in layout
     *
     * Magento 1.9.2.0 Started caching CMS blocks, but the cache was shared between stores.
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return array(
            Mage::app()->getStore()->getCode(),
            $this->getNameInLayout(),
            $this->getBlockId()
        );
    }
}