<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 27/04/15
 * Time: 10:48
 */

/**
 * Class TradeTested_Core_Block_Rewrite_Page_Html_Footer
 *
 * @method boolean hasCustomCacheKey()
 * @method string getCustomCacheKey()
 * @method $this setCustomCacheKey(string $value)
 */
class TradeTested_Core_Block_Rewrite_Page_Html_Footer extends Mage_Page_Block_Html_Footer
{
    /**
     * Get cache key informative items
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        $key = $this->hasCustomCacheKey() ? $this->getCustomCacheKey() : 'PAGE_FOOTER';
        return array(
            $key,
            Mage::app()->getStore()->getId(),
            (int)Mage::app()->getStore()->isCurrentlySecure(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            Mage::getSingleton('customer/session')->isLoggedIn()
        );
    }
}