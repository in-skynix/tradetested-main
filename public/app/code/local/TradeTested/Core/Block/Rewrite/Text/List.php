<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 11/07/15
 * Time: 12:01
 */
class TradeTested_Core_Block_Rewrite_Text_List extends Mage_Core_Block_Text_List
{
    /**
     * Add store code to cache key info, so lists of widgets can be cached in layout
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return array(
            Mage::app()->getStore()->getCode(),
            $this->getNameInLayout()
        );
    }
}