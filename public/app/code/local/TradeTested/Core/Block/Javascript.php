<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 10/06/16
 * Time: 1:47 PM
 */
class TradeTested_Core_Block_Javascript extends Mage_Core_Block_Abstract
{
    protected $_components = [];
    protected $_blackList = [];

    /**
     * @param             $name
     * @param string|null $ref
     * @param string|null $configMethod
     *
     * @return $this
     */
    public function addComponent($name, string $ref = null, string $configMethod = null)
    {
        if (is_string($name)) {
            $this->_components[$ref?$ref:$name] = ['name' => $name, 'config_method' => $configMethod, 'ref' => $ref];
        } else {
            $this->_components[$name['ref']??$name['name']] = $name;
        }
        return $this;
    }

    /**
     * @param string $ref
     */
    public function removeComponent(string $ref)
    {
        if (isset($this->_components[$ref])) {
            unset($this->_components[$ref]);
        } else {
            $this->_blackList[] = $ref;
        }
    }

    /**
     * @return array
     */
    public function getComponents()
    {
        return $this->_components;
    }

    /**
     * @return string
     */
    protected function _toHtml()
    {
        $components = [];
        foreach ($this->getComponents() as $_ref => $_component) {
            if (in_array($_ref, $this->_blackList)) {
                continue;
            }
            if ($configMethod = ($_component['config_method']??false)) {
                list($type, $configMethod) = explode(':', $configMethod);
                $parts = explode('.', $configMethod);
                $method = array_pop($parts);
                $objName = implode('.', $parts);
                try {
                    if ($type == 'block') {
                        $_component['config'] = call_user_func([$this->getLayout()->getBlock($objName), $method]);
                    } elseif ($type == 'helper') {
                        $_component['config'] = call_user_func([Mage::helper($objName), $method]);
                    }
                } catch (Throwable $t) {
//                    throw $t;
//                    var_dump($_component);
                    continue;
                }
            }
            $args = ($_component['config']??false) ? [$_component['config']] : null;
            $components[] = ['ref' => ($_component['ref']??''), 'name' => $_component['name'], 'args' => $args];
        }
        $componentJson = json_encode($components);
        return <<<HTML
<script type="text/javascript" async="true">
_components({$componentJson});
</script>
HTML;
    }
}