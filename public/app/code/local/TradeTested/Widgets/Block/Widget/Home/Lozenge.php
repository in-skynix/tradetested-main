<?php
class TradeTested_Widgets_Block_Widget_Home_Lozenge
    extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface
{
    public function getImageUrl($density = 1)
    {
        return Mage::helper('avid_thumbor')->getUrl($this->getImage())
            ->fitIn($this->getWidth()*$density, $this->getHeight()*$density)
            ->addFilter(
                'rgb',
                -$this->getDarkenPercentage(),
                -$this->getDarkenPercentage(),
                -$this->getDarkenPercentage()
            )
            ->addFilter('no-upscale');
    }

    public function getWidth()
    {
        return 374;
    }

    public function getHeight()
    {
        return 150;
    }
}
