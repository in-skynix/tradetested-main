<?php
/**
 * Class TradeTested_Widgets_Block_Widget_BrandLink
 *
 * @method string getBrandName()
 * @method $this setBrandName(string $value)
 * @method string getImage()
 * @method $this setImage(string $value)
 * @method string getLinkUrl()
 * @method $this setLinkUrl(string $value)
 */
class TradeTested_Widgets_Block_Widget_BrandLink
    extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface
{
    public function getImageUrl($density = 1)
    {
        return Mage::helper('avid_thumbor')->getUrl($this->getImage())
            ->fitIn($this->getWidth()*$density, $this->getHeight()*$density)
            ->addFilter('no-upscale');
    }

    public function getWidth()
    {
        return 220;
    }

    public function getHeight()
    {
        return 50;
    }
}
