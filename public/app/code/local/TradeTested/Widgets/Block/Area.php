<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 03/08/15
 * Time: 14:32
 */
class TradeTested_Widgets_Block_Area extends Mage_Core_Block_Template
{
    /**
     * Get child blocks split into chunks as evenly as possible.
     *
     * @param int $minRows
     * @param int $minPerRow
     * @param int $maxPerRow
     * @return array
     */
    public function getChunkedChildren($maxPerRow = 4)
    {
        $children = $this->getSortedChildBlocks();
//        $children = array_splice($children, 0, 3);
        $count = count($children);
        $numberOfRows = ceil($count/$maxPerRow);

        //at each step, pick the number to display based on the abliity to divide the reimainder
        //array_chunk doesn't work well for e.g. 10 children. (4/4/2 vs 4/3/3)
        $results = array();
        while ($c = count($children)) {
            $size = $numberOfRows ? ceil($c/$numberOfRows) : 1;
            $results[] = array_splice($children, 0, $size);
            $numberOfRows--;
        }
        return $results;
    }

    /**
     * Get child blocks split into chunks as evenly as possible.
     *
     * $minPerRow and $maxPerRow override $minRows
     *
     * @param int $minRows
     * @param int $minPerRow
     * @param int $maxPerRow
     * @return array
     */
    public function getChunkedChildrenMinRows($minRows = 2, $minPerRow = 2, $maxPerRow = 4)
    {
        $children = $this->getSortedChildBlocks();
        $count = count($children);
        $numberOfRows = ceil($count/$maxPerRow);
        $numberOfRows = ($minRows > $numberOfRows) ? $minRows : $numberOfRows;
        $maxSize = ceil($count/$numberOfRows);
        $remainder = $count/$maxSize;
        if ($remainder < $minPerRow) {
            $numberOfRows--;
        }
        if ($count <= $minPerRow) {
            $numberOfRows = 1;
        }

        //at each step, pick the number to display based on the abliity to divide the reimainder
        //array_chunk doesn't work well for e.g. 10 children. (4/4/2 vs 4/3/3)
        $results = array();
        while ($c = count($children)) {
            $size = $numberOfRows ? ceil($c/$numberOfRows) : 1;
            $size = ($size > $maxPerRow) ? $maxPerRow : $size;
            $results[] = array_splice($children, 0, $size);
            $numberOfRows--;
        }
        return $results;
    }
}