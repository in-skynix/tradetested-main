<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 05/02/2016
 * Time: 16:54
 */
class TradeTested_Security_Block_Adminhtml_Unblock_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(
            array('id' => 'unblock_form', 'action' => $this->getUrl('*/*/unblockPost'), 'method' => 'post')
        );
        $form->setUseContainer(true);
        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('cms')->__('Unblock Customer Login')));
        $fieldset->addField('bucket', 'select', array(
            'name'      => 'bucket',
            'label'     => Mage::helper('tradetested_security')->__('Block Type'),
            'title'     => Mage::helper('tradetested_security')->__('Block Type'),
            'required'  => true,
            'options'   => array(
                'customer_login_failed' => 'Login Failed: Email',
                'ip_address_login_failed' => 'Login Failed: IP Address',
            )
        ));
        $fieldset->addField('tag_value', 'text', array(
            'name'      => 'tag_value',
            'label'     => Mage::helper('tradetested_security')->__('Value'),
            'title'     => Mage::helper('tradetested_security')->__('Value'),
            'required'  => true,
            'after_element_html' => '<br/>Enter either the email address or the IP address you wish to unblock',
        ));
        $fieldset->addField('submit', 'submit', array(
            'required'  => true,
            'value'  => 'Remove Lockout',
            'class'  => 'form-button'
        ));
        $this->setForm($form);
        return parent::_prepareForm();
    }

}