<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 05/02/2016
 * Time: 14:02
 */
class TradeTested_Security_Model_Observer
{
    /**
     * Before trying to authenticate customer, check if too many failed login attempts for user or from IP
     *
     * @param Varien_Event_Observer $observer
     * @throws TradeTested_Security_Exception
     */
    public function customerCustomerAuthenticationBefore(Varien_Event_Observer $observer)
    {
        $email = $observer->getEvent()->getEmail();
        $ipAddress = Mage::helper('core/http')->getRemoteAddr();
        $limiter = Mage::helper('tradetested_security/rateLimit_customerLogin');
        $limiter->checkLimit('customer_login', array('ip_address' => $ipAddress));
        $limiter->checkLock('customer_login_failed', array('email' => $email));
        $limiter->checkLock('ip_address_login_failed', array('ip_address' => $ipAddress));
    }

    /**
     * After login failure, increment counts against IP address and user.
     *
     * @param Varien_Event_Observer $observer
     * @throws TradeTested_Security_Exception
     */
    public function customerCustomerAuthenticationFailed(Varien_Event_Observer $observer)
    {
        $email = $observer->getEvent()->getEmail();
        $ipAddress = Mage::helper('core/http')->getRemoteAddr();
        $limiter = Mage::helper('tradetested_security/rateLimit_customerLogin');
        $limiter->checkLimit('customer_login_failed', array('email' => $email));
        $limiter->checkLimit('ip_address_login_failed', array('ip_address' => $ipAddress));
    }

    /**
     * After resetting a customer's password, clear their rate limit blocking.
     * 
     * This only works in front-end as admin uses changePassword(), which updates attribute directly.
     * 
     * @param Varien_Event_Observer $observer
     */
    public function customerSaveAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $observer->getEvent()->getCustomer();
        if ($customer->hasDataChanges() && $customer->dataHasChangedFor('password_hash')) {
            Mage::helper('tradetested_security/rateLimit_customerLogin')->removeLock(
                'customer_login_failed', 
                ['email' => $customer->getEmail()]
            );
        }
    }

    /**
     * Clear block on customer after admin saves customer in any way.
     * 
     * @param Varien_Event_Observer $observer
     */
    public function adminhtmlCustomerSaveAfter(Varien_Event_Observer $observer)
    {
        Mage::helper('tradetested_security/rateLimit_customerLogin')->removeLock(
            'customer_login_failed',
            ['email' => $observer->getEvent()->getCustomer()->getEmail()]
        );
    }
}
