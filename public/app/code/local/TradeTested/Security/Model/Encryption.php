<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 09/02/2016
 * Time: 15:49
 */
class TradeTested_Security_Model_Encryption extends Mage_Core_Model_Encryption
{
    /**
     * Use PHP native password_hash, which includes stretching and secure algos,
     * and auto-generates secure salts without the bias of mt_rand.
     *
     * @param string $password
     * @param mixed $salt
     * @return string
     */
    public function getHash($password, $salt = false)
    {
        if ($salt === false) {
            return parent::getHash($password, false);
        } else {
            return password_hash($password, PASSWORD_BCRYPT);
        }
    }

    /**
     * Validate hash against hashing method (with or without salt)
     *
     * @param string $password
     * @param string $hash
     * @return bool
     * @throws Exception
     */
    public function validateHash($password, $hash)
    {
        return password_verify($password, $hash);
    }
}
