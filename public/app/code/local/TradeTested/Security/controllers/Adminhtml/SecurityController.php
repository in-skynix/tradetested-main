<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 05/02/2016
 * Time: 16:48
 */
class TradeTested_Security_Adminhtml_SecurityController extends Mage_Adminhtml_Controller_Action
{
    public function unblockAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function unblockPostAction()
    {
        if ($this->_validateFormKey()) {
            $bucket = $this->getRequest()->getPost('bucket');
            $value = $this->getRequest()->getPost('tag_value');
            $tags = ($bucket == 'customer_login_failed') ? array('email' => $value) : array('ip_address' => $value);
            Mage::helper('tradetested_security/rateLimit_customerLogin')->removeLock($bucket, $tags);
            $this->_getSession()->addSuccess('Removed Rate Limit Lockout');
        }
        $this->_redirect('*/*/unblock');
    }
}