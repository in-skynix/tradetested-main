<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 04/02/2016
 * Time: 16:41
 */
require_once 'TradeTested/RateLimit.php';

/**
 * Class TradeTested_Security_Helper_RateLimit
 *
 * Magento helper wrapper for lib that is also used outside Magento
 *
 * Checks against rate limits in Redis, and throws Exception if locked or over.
 */
class TradeTested_Security_Helper_RateLimit extends Mage_Core_Helper_Abstract
{
    protected $_limitClient;
    protected $_config = array();

    /**
     * TradeTested_Security_Helper_RateLimit constructor.
     * @param array $config The rate limit config, in format e.g.
     *
     * array(
     *   'customer_login' => array(
     *   'limits'    => array(5 => 5),
     *   'message'   => 'Too many login attempts. Wait 20 seconds and try again.'
     *   ),
     *   'customer_login_failed' => array(
     *   'limits'            => array(60*30 => 5),
     *   'lockout_duration'  => 60*60*24,
     *   'message'           => 'Too many failed login attempts for this user. Contact Support.'
     *   ),
     *   'ip_address_login_failed' => array(
     *   'limits'            => array(60*30 => 20),
     *   'lockout_duration'  => 60*60*2,
     *   'lockout_callback'  => array($this, 'sendCustomerLockoutNotification'),
     *   'message'           => 'Too many failed login attempts from this IP Address. Contact Support.'
     *   ),
     * )
     */
    public function __construct($config)
    {
        $this->_config = array_merge($this->_config, $config);
    }

    /**
     * Get config for rate limit bucket
     *
     * @param $bucket
     * @return array|null
     */
    protected function _getConfig($bucket)
    {
        $config = $this->_config;
        if ($bucket) {
            return isset($config[$bucket]) ? $config[$bucket] : null;
        } else {
            return $config;
        }
    }

    /**
     * Checks if action is already locked,
     * if not will increment the times the action has fired, and checks if over limit
     *
     * @param $bucket
     * @param $tags
     * @throws TradeTested_Security_Exception
     */
    public function checkLimit($bucket, $tags)
    {
        $this->checkLock($bucket, $tags);
        $config = $this->_getConfig($bucket);
        $tags = array_merge($tags, array('action' => $bucket));
        if ($this->_getLimitClient()->isOverLimit($tags, $config['limits'])) {
            if (isset($config['lockout_duration'])) {
                $this->_getLimitClient()->addLock($tags, $config['lockout_duration']);
            }
            if (isset($config['lockout_callback'])) {
                call_user_func($config['lockout_callback'], $tags);
            }
            $message = isset($config['message']) ? $config['message'] : 'Too many requests';
            throw new TradeTested_Security_Exception($message);
        }
    }

    /**
     * Check if an action is locked without checking limit or incrementing count of times action fired.
     *
     * @param $bucket
     * @param $tags
     * @throws TradeTested_Security_Exception
     */
    public function checkLock($bucket, $tags)
    {
        if($this->lockExists($bucket, $tags)) {
            $config = $this->_getConfig($bucket);
            $message = isset($config['message']) ? $config['message'] : 'Too many requests';
            throw new TradeTested_Security_Exception($message);
        }
    }

    /**
     * Check if an action is locked without checking limit or incrementing count of times action fired.
     * 
     * @param $bucket
     * @param $tags
     * @return bool
     */
    public function lockExists($bucket, $tags)
    {
        $config = $this->_getConfig($bucket);
        $tags = array_merge($tags, array('action' => $bucket));
        if ($this->_getLimitClient()->lockExists($tags)) {
            if (isset($config['lock_exists_callback'])) {
                call_user_func($config['lock_exists_callback'], $tags);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Remove a 'lock' entry
     *
     * @param $bucket
     * @param $tags
     */
    public function removeLock($bucket, $tags)
    {
        $tags = array_merge($tags, array('action' => $bucket));
        $this->_getLimitClient()->removeLock($tags);
    }

    /**
     * Get client that interacts with Redis
     *
     * @return \TradeTested\RateLimit
     */
    protected function _getLimitClient()
    {
        if (!$this->_limitClient) {
            $password = ($pw = $this->_getStoreConfig('redis_password')) ? Mage::helper('core')->decrypt($pw) : null;
            $this->_limitClient = new TradeTested\RateLimit(array(
                'host'      => $this->_getStoreConfig('redis_host'),
                'port'      => $this->_getStoreConfig('redis_port'),
                'db'        => $this->_getStoreConfig('redis_db'),
                'password'  => $password
            ));
        }
        return $this->_limitClient;
    }

    /**
     * Get Store Config
     *
     * Convenience Method.
     */
    protected function _getStoreConfig($key)
    {
        return Mage::getStoreConfig('tradetested_security/rate_limit/'.$key);
    }
}
