<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 05/02/2016
 * Time: 14:11
 */
class TradeTested_Security_Helper_RateLimit_MailchimpUpdate extends TradeTested_Security_Helper_RateLimit
{
    public function __construct()
    {
         return parent::__construct(array(
            'mailchimp_update' => array(
                'limits'    => array(60*5 => 5, 60*60*24 => 10),
                'lockout_duration'  => 60*60*24,
                'message'   => 'Too many attempts. Please contact Support.'
            ),
        ));
    }
}