<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 05/02/2016
 * Time: 14:11
 */
class TradeTested_Security_Helper_RateLimit_CustomerLogin extends TradeTested_Security_Helper_RateLimit
{
    public function __construct()
    {
        return parent::__construct([
            'customer_login'          => [
                'limits'  => [5 => 5],
                'message' => 'Too many login attempts. Wait 20 seconds and try again.'
            ],
            'customer_login_failed'   => [
                'limits'           => [60 * 30 => 7],
                'lockout_duration' => 60 * 60 * 24,
                'lock_exists_callback' => [$this, 'registerFailedLoginEmail'],
                'message'          => 'Too many failed sign in attempts for this email address. '
                    .'If there is an account associated with this email address, '
                    .'you can <a href="' . $this->_getUrl('customer/account/forgotPassword') . '" target="_blank">'
                    .'reset your password</a> to unlock it.'
            ],
            'ip_address_login_failed' => [
                'limits'           => [60 * 30 => 20],
                'lockout_duration' => 60 * 60 * 2,
                'message'          => 'Too many failed login attempts from this IP Address. Please contact Support.'
            ],
        ]);
    }

    public function registerFailedLoginEmail($tags)
    {
        Mage::getModel('tradetested_core/flash')->setLastLoginAttemptEmail($tags['email']);
    }
}