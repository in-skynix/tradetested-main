<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 19/12/14
 * Time: 20:37
 */
class TradeTested_Regions_Block_Locate_Ajax extends Mage_Core_Block_Abstract
{
    public function __construct()
    {
        Mage::helper('tradetested_regions')->setAreaNameFromGeoIP();
    }
}