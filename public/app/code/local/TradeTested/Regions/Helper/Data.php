<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 19/12/14
 * Time: 14:58
 */
require_once 'geoip2.phar';
class TradeTested_Regions_Helper_Data extends Mage_Core_Helper_Data
{
    public function getAreaNameOptions()
    {
        if (Mage::app()->getStore()->getCode() == 'default') {
            return array(
                'NTL' => 'Northland',
                'AUK' => 'Auckland',
                'WKO' => 'Waikato',
                'BOP' => 'Bay of Plenty',
                'GIS' => 'Gisborne',
                'HKB' => "Hawke's Bay",
                'TKI' => 'Taranaki',
                'MWT' => 'Manawatu',
                'WGN' => 'Wellington',
                'NSN' => 'Nelson',
                'MBH' => 'Marlborough',
                'WTC' => 'West Coast',
                'CAN' => 'Canterbury',
                'OTA' => 'Otago',
                'STL' => 'Southland'
            );
        }
    }

    public function isLookupPerformed()
    {
        return $this->_getCookieData('area_name') || $this->_getCookieData('postcode');
    }

    public function setPostcode($postcode)
    {
        $this->_setCookieData('postcode', $postcode);
        return $this;
    }
    
    public function getPostcodeFromCurrentRegion()
    {
        return $this->estimatePostcode($this->getAreaName());
    }

    public function estimatePostcode($areaName)
    {
        if (!$areaName) {
            return false;
        }
        $postcodes = [
            'Northland'     => 474,
            'Auckland'      => 1010,
            'Waikato'       => 3200,
            'Bay of Plenty' => 3127,
            'Gisborne'      => 4010,
            "Hawke's Bay"   => 4110,
            'Taranaki'      => 4310,
            'Manawatu'      => 4410,
            'Wellington'    => 6011,
            'Nelson'        => 7010,
            'Marlborough'   => 7201,
            'West Coast'    => 7805,
            'Canterbury'    => 8011,
            'Otago'         => 9076,
            'Southland'     => 9810,
        ];
        
        return $postcodes[$areaName] ?? false;
    }

    public function setAreaCode($areaCode)
    {
        $name = null;
        if ($options = $this->getAreaNameOptions()) {
            $name = $options[$areaCode] ?? null;
        }
        $this->_setCookieData('area_name', $name);
    }

    public function getAreaCode()
    {
        $code = null;
        if ($options = $this->getAreaNameOptions()) {
            $d = array_flip($this->getAreaNameOptions());
            $code = $d[$this->getAreaName()] ?? null;
        }
        return $code;
    }

    public function setAreaName($areaName)
    {
        $this->_setCookieData('area_name', $areaName);
        return $this;
    }

    public function getAreaName()
    {
        return $this->_getCookieData('area_name');
    }

    public function getPostcode()
    {
        return $this->_getCookieData('postcode');
    }

    public function setAreaNameFromGeoIP()
    {
        if ($this->isLookupPerformed()) {
            return $this;
        }
        $hostname = $_SERVER["REMOTE_ADDR"];
//        $hostname = '139.80.174.9'; //NZ (otago)
//        $hostname = '116.90.134.28'; //christchurch.wonderproxy.com
        try {
            $client = new GeoIp2\WebService\Client(84048, 'aAzaoHvBLEKg');
            $record = $client->city($hostname);
        } catch (Exception $e) {
            return $this->_setCookieData('area_name', 'Auckland');
        }
        $options = $this->getAreaNameOptions();
        if (strtoupper($record->country->isoCode) == 'NZ') {
            foreach($record->subdivisions as $_subdivision) {
                if(isset($options[$_subdivision->isoCode])) {
                    return $this->_setCookieData('area_name', $options[$_subdivision->isoCode]);
                }
            }
        }
        return $this->_setCookieData('area_name', 'Auckland');
    }


    protected function _getCookieData($name)
    {
        if (!Mage::registry('tradetested_regions.'.$name)) {
            Mage::unregister('tradetested_regions.'.$name);
            Mage::register('tradetested_regions.'.$name, Mage::getModel('core/cookie')->get('pse_'.$name));
        }
        return Mage::registry('tradetested_regions.'.$name);
    }

    protected function _setCookieData($name, $value)
    {
        Mage::unregister('tradetested_regions.'.$name);
        Mage::register('tradetested_regions.'.$name, $value);
        Mage::getModel('core/cookie')->set('pse_'.$name, $value, 60*60*24*365);
        return $this;
    }

}