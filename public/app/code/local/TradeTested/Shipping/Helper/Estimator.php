<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 19/12/14
 * Time: 19:32
 */

/**
 * Class TradeTested_ShippingEstimator_Block_Product
 *
 * @method $this setProductId(int $value)
 * @method int getProductId()
 */
class TradeTested_Shipping_Helper_Estimator extends Mage_Core_Helper_Abstract
{
    /**
     * @return string
     */
    public function getProductConfigJson()
    {
        $product   = Mage::registry('product');
        if(!$product->getIsInStock()) {
            return ['disabled' => true];
        }
        $estimator = Mage::getSingleton('tradetested_shipping/estimator_product');

        $data = [
            'rates'           => $estimator->setProduct($product)->getRatesData(),
            'area_options'    => Mage::helper('tradetested_regions')->getAreaNameOptions(),
            'product_id'      => $product->getId(),
            'selected_method' => $estimator->getSelectedMethod(),
        ];
        if ($postcode = $estimator->getQuote()->getShippingAddress()->getPostcode()) {
            $data = array_merge($data, $this->getDataFromPostcode($postcode));
        } else {
            $data['area_code'] = Mage::helper('tradetested_regions')->getAreaCode();
        }
        return $this->transformData($data);
    }

    public function getCartConfigJson()
    {
        return $this->getCartConfig();
    }

    public function getCartConfig()
    {
        /** @var TradeTested_Shipping_Model_Estimator_Cart $estimator */
        $estimator      = Mage::getSingleton('tradetested_shipping/estimator_cart');
        $postcode       = $estimator->getQuote()->getShippingAddress()->getPostcode();
        

        $data = array_merge([
            'rates'           => $estimator->getRatesData(),
            'selected_method' => $estimator->getSelectedMethod(),
            'postcode'        => $postcode,
        ], $this->getDataFromPostcode($postcode));
        return $this->transformData($data);
    }
    
    public function getDataFromPostcode($postcode)
    {
        $data = [];
        $postcodeObject = Mage::getModel('tradetested_shipping/postcode')->loadByPostcode(
            $postcode, Mage::getStoreConfig('general/country/default')
        );
        $data['state'] = $postcodeObject->getState();
        $data['area_name'] = 'postcode '.$postcode;
        return $data;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function transformData(array $data)
    {
        if ((Mage::getStoreConfig('general/country/default') == 'AU') && ($data['state'] != 'NSW')) {
            unset($data['rates']['rates']['pickup']);
        } 
        return $data;
    }

}