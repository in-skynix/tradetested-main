<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 27/04/2016
 * Time: 17:48
 */
class TradeTested_Shipping_Helper_Algorithm extends Mage_Core_Helper_Abstract
{
    /**
     * Calculate Algos
     * 
     * copied from Webshopapps_Productmatrix_Model_Mysql4_Carrier_Productmatrix
     * 
     * Some algorithms simply removed, rather than working out how they are meant to work
     * 
     * @param Mage_Shipping_Model_Rate_Request $request
     * @param TradeTested_Shipping_Model_Depot_Rate $rate
     * @return array
     */
    public function calculate(Mage_Shipping_Model_Rate_Request $request, TradeTested_Shipping_Model_Depot_Rate $rate)
    {
//        var_dump($request->getData()); die();
        $conditionName = 'some_string'; //or 'per_package; ??
        $data = $rate->getData();
        $decrease = -1;
        if ($rate->getAlgorithm()) {
            $algorithm_array=explode("&",$rate->getAlgorithm());  // Multi-formula extension
            reset($algorithm_array);
            foreach ($algorithm_array as $algorithm_single) {
                $algorithm=explode("=",$algorithm_single,2);
                if (!empty($algorithm) && count($algorithm)==2) {
                    $algKey = strtolower($algorithm[0]);
                    $algValue = $algorithm[1];

                    switch ($algKey) {
                        case "w":
                            // weight based
                            $weightIncrease=explode("@",$algValue);
                            if (!empty($weightIncrease) && count($weightIncrease)==2 ) {
                                if($data['weight_from'] == -1 ){$data['weight_from'] = 0;}
                                $weightDifference=	$request->getPackageWeight()-$data['weight_from'];
                                $quotient=$weightDifference / $weightIncrease[0];
                                $data['price']=$data['price']+$weightIncrease[1]*$quotient;
                            }
                            break;
                        case "wa":
                            $weightIncrease=explode("@",$algValue);
                            if (!empty($weightIncrease) && count($weightIncrease)==2 ) {
                                $weight= $request->getPackageWeight();
                                $quotient=$weight / $weightIncrease[0];
                                $data['price']=$data['price']+$weightIncrease[1]*$quotient;
                            }
                            break;
                        case "wc":
                            // weight based
                            $weightIncrease=explode("@",$algValue);
                            if (!empty($weightIncrease) && count($weightIncrease)==2 ) {
                                if($data['weight_from'] == -1 ){$data['weight_from'] = 0;}
                                $weightDifference=	$request->getPackageWeight()-$data['weight_from'];
                                $quotient=ceil($weightDifference / $weightIncrease[0]);
                                $data['price']=$data['price']+$weightIncrease[1]*$quotient;
                            }
                            break;
                        case "wca":
                            // weight based
                            $weightIncrease=explode("@",$algValue);
                            if (!empty($weightIncrease) && count($weightIncrease)==2 ) {
                                $weight= $request->getPackageWeight();
                                $quotient=ceil($weight / $weightIncrease[0]);
                                $data['price']=$data['price']+$weightIncrease[1]*$quotient;
                            }
                            break;
                        case "p":
//                            $this->_prioritySet=true;
                            $data['priority']=$algValue;
                            break;
                        case "m":
//                            $this->_maxPriceSet = true;
                            $data['max_price']=$algValue;
                            break;
                        case "i":
                            if (strtolower($algValue)=='ignore') {
//                                $this->_ignoreAdditionalItemPrice=true;
                            } else {
                                if ($conditionName=='per_package') {
                                    $data['price']+=$algValue*($request->getPackageQty()-$data['item_from_value']);
                                } else {
                                    $data['multiprice']=$algValue;
                                    $data['qty']=$request->getPackageQty();
                                }
                            }
                            break;
                        case "io":
                            if ($conditionName=='per_package') {
                                $data['price']+=$algValue*($request->getPackageQty()-$data['item_from_value']);
                            } else {
                                $data['multiprice']=$algValue;
                                $data['qty']=1;
                            }
                            break;
                        case "im":
                            $itemIncrease=explode("@",$algValue);
                            if (!empty($itemIncrease) && count($itemIncrease)==2 ) {
                                $qty=$request->getPackageQty();
                                $quotient=ceil($qty / $itemIncrease[0]);
                                if ($conditionName=='per_package') {
                                    $data['price']=$data['price']+$itemIncrease[1]*$quotient;
                                } else {
                                    $data['multiprice']=$data['price']+$itemIncrease[1]*$quotient;
                                    $data['qty']=$request->getPackageQty();
                                }
                            }
                            break;
                        case "ai":
                            if ($conditionName=='per_package') {
                                $data['price']+=$algValue*($request->getPackageQty());
                            } else {
                                $data['multiprice']=$algValue;
                                $data['qty']=$request->getPackageQty();
                            }
                            break;
                        case "o":
                            $data['order']=$algValue;
                            break;
                        case "%":
                            $perSplit=explode("+",$algValue);
                            if (!empty($perSplit) && count($perSplit)==2) {
                                $percentage = $perSplit[0];
                                $flatAdd = $perSplit[1];
                            } else {
                                $percentage = $algValue;
                                $flatAdd = 0;
                            }
                            $percPrice=($request->getPackageValueWithDiscount()*$percentage/100)+$flatAdd;
                            if ($percPrice>$data['price']) {
                                $data['price']=$percPrice;
                            }
                            break;
                        case "r":
                            $decrease=$algValue;
                            break;
                        case "c":
                            $data['method_name']=$algValue;
                            break;
                        case "setcart":
                            $data['set_cart']=$algValue;
                            break;
                    }
                }
                switch ($algorithm_single) {
                    case "OVERRIDE":
                        $data['override']=true;
                        break;
                    case "SHOWALL":
                        $data['showall']=true;
                        break;
                }

            }
            if ($decrease>-1) {
                $data['price'] = $data['price']-$decrease;
                if ($data['price']<0) { $data['price']=0; }
            }
        }
        return $data;
    }
}