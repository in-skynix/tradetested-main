<?php
class TradeTested_Shipping_Helper_Freeshipping extends Mage_Core_Helper_Abstract
{
    protected $_rules;
    protected $_websiteId;

    protected function _getRules()
    {
        if (!$this->_rules) {
            if (! $this->_rules = Mage::registry('ttcatalog.freeshipping_rules')) {
                $this->_rules = Mage::getResourceModel('salesrule/rule_collection')
                    ->setValidationFilter($this->_websiteId, 0, null)
                    ->addFieldToFilter('simple_free_shipping', array('gt' => 0))
                    ->addFieldToFilter('coupon_type', 1)
                    ->load();
                Mage::register('ttcatalog.freeshipping_rules', $this->_rules);
            }
        }
        return $this->_rules;
    }

    protected function _setCachedResult($productId, $result)
    {
        $cache = Mage::app()->getCache();
        $cache->save(
            $result, 'fs_cp_' . $productId . "_s_" . $this->_websiteId,
            [Mage_Core_Block_Abstract::CACHE_GROUP, 'freeshipping_catalog'], 60 * 60
        );

        return $this;
    }

    protected function _getCachedResult($productId)
    {
        return Mage::app()->getCache()->load('fs_cp_'.$productId."_s_".$this->_websiteId);
    }

    public function canHasFreeShipping(Mage_Catalog_Model_Product $product)
    {
        $this->_websiteId = Mage::app()->getWebsite()->getId();
        if ($cachedResult = $this->_getCachedResult($product->getId())) {
            return $cachedResult == 't' ? true : false;
        }
        $item = Mage::getModel('sales/quote_item')->setProduct($product);
        foreach ($this->_getRules() as $rule) {
            Varien_Profiler::start('validate_actions');
            $result = $rule->getActions()->validate($item);
            Varien_Profiler::stop('validate_actions');
            if ($result) {
                $this->_setCachedResult($product->getId(), 't');
                return true;
            }
        }
        $this->_setCachedResult($product->getId(), 'f');
        return false;
    }
}