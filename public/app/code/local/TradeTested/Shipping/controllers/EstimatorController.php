<?php

class TradeTested_Shipping_EstimatorController extends Mage_Core_Controller_Front_Action
{
    public function productAction()
    {
        $data = [];
        if ($areaCode = $this->getRequest()->getParam('area_code')) {
            Mage::helper('tradetested_regions')->setAreaCode($areaCode);
            $data['area_code'] = Mage::helper('tradetested_regions')->getAreaCode();
        } elseif ($postcode = $this->getRequest()->getParam('postcode')) {
            $data = array_merge($data, Mage::helper('tradetested_shipping/estimator')->getDataFromPostcode($postcode));
        }

        $postcode = isset($postcode) ? $postcode : null;
        Mage::helper('tradetested_regions')->setPostcode($postcode);
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $quote->getShippingAddress()->setPostcode($postcode);
        $quote->save();
        Mage::getSingleton('checkout/session')->setQuoteId($quote->getId());
        
        $product = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('product_id'));
        $estimator = Mage::getSingleton('tradetested_shipping/estimator_product')->setProduct($product);
        $data['rates'] = $estimator->getRatesData();
        $data['selected_method'] = $estimator->getSelectedMethod();
        $data = Mage::helper('tradetested_shipping/estimator')->transformData($data);
        $this->getResponse()->setBody(json_encode($data))->setHeader('Content-Type', 'application/json', true);
    }
    
    public function resetAction()
    {
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $quote->getShippingAddress()->setPostcode(null);
        $quote->save();
        Mage::getSingleton('checkout/session')->setQuoteId($quote->getId());
        $this->getResponse()->setBody(json_encode(null))->setHeader('Content-Type', 'application/json', true);
    }
}