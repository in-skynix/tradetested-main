<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 4/10/16
 * Time: 12:28 PM
 */
class TradeTested_Shipping_Adminhtml_ShippingController extends Mage_Adminhtml_Controller_Action
{
    public function exportAction()
    {
        $carrierCode = $this->getRequest()->getParam('carrier');
        return $this->_prepareDownloadResponse(
            $carrierCode.'.csv',
            Mage::getModel('shipping/shipping')->getCarrierByCode($carrierCode)->exportCSV()
        );
    }
}