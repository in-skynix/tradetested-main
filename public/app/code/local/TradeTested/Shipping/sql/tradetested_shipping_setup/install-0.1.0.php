<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('tradetested_shipping/postcode'))
    ->addColumn(
        'postcode_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, 16,
        ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary'  => true,],
        'Postcode ID'
    )
    ->addColumn(
        'country_code',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        3,
        ['nullable' => false],
        'Country Code'
    )
    ->addColumn(
        'state',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        3,
        ['nullable' => false],
        'State'
    )
    ->addColumn(
        'postcode',
        Varien_Db_Ddl_Table::TYPE_INTEGER, 5,
        ['unsigned' => true, 'nullable' => false],
        'Postcode'
    )
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, ['nullable' => false], 'Name')
    ->addColumn('latitude', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', ['default' => '0.0000'], 'Latitude')
    ->addColumn('longitude', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', ['default' => '0.0000'], 'Longitude')
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable('tradetested_shipping/postcode'),
            ['postcode', 'country_code'],
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        ['postcode', 'country_code'],
        ['type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE]
    );
;
$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('tradetested_shipping/depot'))
    ->addColumn(
        'depot_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, 16,
        ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary'  => true,],
        'Depot ID'
    )
    ->addColumn('code', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, ['nullable' => false], 'Code')
    ->addColumn('country_code', Varien_Db_Ddl_Table::TYPE_VARCHAR, 3, ['nullable' => false], 'Country Code')
    ->addColumn('state', Varien_Db_Ddl_Table::TYPE_VARCHAR, 3, ['nullable' => false], 'State')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, ['nullable' => false], 'Name')
    ->addColumn('address', Varien_Db_Ddl_Table::TYPE_TEXT, 255, ['nullable' => false], 'Address')
    ->addColumn('postcode', Varien_Db_Ddl_Table::TYPE_INTEGER, 5, ['nullable' => false, 'unsigned' => true], 'Postcode')
    ->addColumn('latitude', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', ['default' => '0.0000'], 'Latitude')
    ->addColumn('longitude', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', ['default' => '0.0000'], 'Longitude')
    ->addIndex(
        $installer->getIdxName('tradetested_shipping/depot', ['code'], Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE), 
        'code',
        ['type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE]
    );
$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('tradetested_shipping/depot_rate'))
    ->addColumn(
        'depot_rate_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, 16,
        ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary'  => true,],
        'Depot Rate ID'
    )
    ->addColumn('depot_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 8, ['nullable' => false], 'Depot ID')
    ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 8, ['nullable' => false], 'Website ID')
    ->addColumn('package_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 30, ['nullable' => false,], 'Package ID')
    ->addColumn('weight_from', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', ['default' => '0.0000'], 'Weight From')
    ->addColumn('weight_to', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', ['default' => '0.0000'], 'Weight To')
    ->addColumn('price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', ['default' => '0.0000'], 'Price')
    ->addColumn('algorithm', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, ['nullable' => false,], 'Package ID')
    ->addColumn('shipping_time_estimate', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, [], 'Shipping Time Estimate')
    ->addForeignKey(
        $installer->getFkName('tradetested_shipping/depot_rate', 'depot_id', 'tradetested_shipping/depot', 'depot_id'),
        'depot_id', $installer->getTable('tradetested_shipping/depot'), 'depot_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('tradetested_shipping/depot_rate', 'website_id', 'core/website', 'website_id'),
        'website_id', $installer->getTable('core/website'), 'website_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    );
$installer->getConnection()->createTable($table);

$installer->getConnection()->addColumn($this->getTable('sales/quote_address_shipping_rate'), 'state', 'varchar(3) not null');
$installer->getConnection()->addColumn($this->getTable('sales/quote_address_shipping_rate'), 'distance', 'integer(6) not null');

$installer->addAttribute('catalog_product', 'sidebar_content',  array(
    'type'                      => 'text',
    'group'                     => 'General',
    'label'                     => 'Sidebar Content',
    'input'                     => 'textarea',
    'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'                   => true,
    'required'                  => false,
    'user_defined'              => false,
    'is_searchable'             => false,
    'is_wysiwyg_enabled'        => true,
    'is_html_allowed_on_front'  => true
));
$installer->updateAttribute('catalog_product', 'sidebar_content', 'is_wysiwyg_enabled', 1);

$entityTypeId = $installer->getEntityTypeId('catalog_product');
$attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $installer->getAttributeGroupId($entityTypeId, $attributeSetId, 'General');

// Add existing attribute to group
$attributeId = $installer->getAttributeId($entityTypeId, 'sidebar_content');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);

if (method_exists($installer->getConnection(), 'resetDdlCache')) {
    $installer->getConnection()->resetDdlCache();
}

$installer->endSetup();
