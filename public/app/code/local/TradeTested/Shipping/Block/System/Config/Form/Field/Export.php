<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 4/10/16
 * Time: 9:52 AM
 */
class TradeTested_Shipping_Block_System_Config_Form_Field_Export extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        $buttonBlock = $this->getLayout()->createBlock('adminhtml/widget_button');
        $params = [
            'website' => $buttonBlock->getRequest()->getParam('website'),
            'carrier'  => preg_replace('/groups\[(.*?)\].*/', '$1', $element->getName()),
        ];
        $data = [
            'label'   => Mage::helper('adminhtml')->__('Export CSV'),
            'onclick' => 'setLocation(\'' . Mage::helper('adminhtml')->getUrl("*/shipping/export", $params) . '\')',
            'class'   => '',
        ];

        $html = $buttonBlock->setData($data)->toHtml();

        return $html;
    }


}

