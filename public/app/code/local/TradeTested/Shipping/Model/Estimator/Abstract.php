<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 8/06/16
 * Time: 10:38 AM
 */
class TradeTested_Shipping_Model_Estimator_Abstract extends Mage_Core_Model_Abstract
{

    /** @var  array */
    protected $_rates;

    /** @var  TradeTested_Regions_Helper_Data */
    protected $_regionHelper;
    
    /** @var  Mage_Sales_Model_Quote */
    protected $_quote;

    public function __construct()
    {
        $this->_regionHelper = Mage::helper('tradetested_regions');
        return parent::__construct();
    }

    /**
     * @return bool
     */
    public function isComplete()
    {
        return (boolean)($this->getQuote()->getShippingAddress()->getPostcode());
    }

    /**
     * @return bool
     */
    protected function _canDisplayRates()
    {
        return true;
    }

    /**
     * Get Rates
     *
     * @return array
     */
    public function getRates()
    {
        if (!($this->isComplete() && $this->_canDisplayRates())) {
            return false;
        }

        if (!$this->_rates) {
            $quote = $this->getQuote();
            $address = $quote->getShippingAddress();
            $address->setCountryId(Mage::getStoreConfig('general/country/default'));
            if (
                ($postcode = $this->getQuote()->getShippingAddress()->getPostcode())
                || ($postcode = $this->_regionHelper->getPostcode())
            ) {
                $address->setPostcode($postcode);
            }
            $address->collectTotals();
            $shipping = $this->_collectRatesByAddress($quote->getShippingAddress());

            $this->_rates = $shipping->getResult()->getAllRates();
            usort($this->_rates, array($this, '_sortRate'));
        }
        return $this->_rates;
    }

    /**
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        if (!$this->_quote) {
            $this->_quote = Mage::getSingleton('checkout/session')->getQuote();
        }
        return $this->_quote;
    }

    /**
     * Should the rates include a time estimate?
     *
     * If the product is in stock and doesn't have a stock description, then allow.
     *
     * @return bool
     */
    public function getHasTimeEstimate()
    {
        foreach ($this->getQuote()->getAllItems() as $_item) {
            $product = $_item->getProduct();
            if ($product->getData('stock_description') || !$product->isAvailable()) {
                //If the product has a stock_description, it is likely on pre-order
                return false;
            }
        }
        foreach ($this->_rates as $_rate) {
            if ($_rate->getShippingTimeEstimate()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get rates data for use in estimator JS etc.
     *
     * @return array|bool
     */
    public function getRatesData()
    {
        if (!($rates = $this->getRates())) {
            return false;
        }
        //Always have at least an empty productmatrix, js prompt user to change location when productmatrix is empty.
        $data = ['rates' => ['productmatrix' => []], 'note'  => $this->_getNote(), 'in_area' => ''];

        /** @var Mage_Shipping_Model_Rate_Result_Method $_rate */
        foreach ($rates as $_rate) {
            $code = $_rate->getCarrier()."_".$_rate->getMethod();
            $formattedPrice = number_format($_rate->getPrice(), (count(explode('.', $_rate->getPrice())) > 1) ? 2 : 0);
            $formattedPrice = ($formattedPrice == 0) ? 'FREE' : '$'.$formattedPrice;
            $timeEstimate = ($this->getHasTimeEstimate()) ? $_rate->getShippingTimeEstimate() : null;
            if ($_rate->getAreaName() && !$data['in_area']) {
                $data['in_area'] = $_rate->getAreaName();
            }
            $data['rates'][$_rate->getCarrier()][] = [
                'code'            => $code,
                'price_formatted' => $formattedPrice,
                'price'           => $_rate->getPrice(),
                'title'           => $_rate->getMethodTitle(),
                'distance'        => $_rate->getDistance(),
                'address'         => $_rate->getAddress(),
                'state'           => $_rate->getState(),
                'time_estimate'   => $timeEstimate,
            ];
        }
        return $data;
    }

    /**
     * @return string
     */
    public function getSelectedMethod()
    {
        $address = $this->getQuote()->getShippingAddress();
        if ($method = $address->getShippingMethod()) {
            return $method;
        }
        
        if (!$this->getRates()) {
            return;
        }

        /** @var Mage_Shipping_Model_Rate_Result_Method $_rate */
        foreach ($this->getRates() as $_rate) {
            if (($_rate->getCarrier() == 'productmatrix') || ($_rate->getCarrier() == 'pickup')) {
                $code = $_rate->getCarrier()."_".$_rate->getMethod();
                $address->setShippingMethod($code);
                return $code;
            }
        }
    }

    /**
     * @return string
     */
    protected function _getNote()
    {
        $note = '';

        $hasTimeEstimate = $this->getHasTimeEstimate();

        if (Mage::app()->getStore()->getCode() == 'default') {
            $rdNote = $depotNote = '';
            /** @var Mage_Sales_Model_Quote_Item $_item */
            foreach ($this->getQuote()->getAllItems() as $_item) {
                $product = $_item->getProduct();
                if($product->getWeight() > 30) {
                    if($product->getPackageId() == 15 ) {
                        //TOLL - without having to look up join tables
                        $rdNote = 'Rural delivery surcharge applies. ';
                    } elseif($product->getPackageId() == 17) {
                        // TOLL_DEPOT
                        $hasTimeEstimate = false;
                        $depotNote = 'All orders go to the nearest depot. ';
                    } else {
                        $rdNote = 'Rural delivery surcharge applies. ';
                    }
                }
            }
            $note = $note.$rdNote.$depotNote;
        }

        if ($hasTimeEstimate) {
            // prevent displaying shipping time estimate if product is on pre-order 
            $note = 'Shipping times are estimated based on 90% of deliveries. '.$note;
        }

        if (Mage::app()->getStore()->getCode() == 'au') {
            $rdNote = '';
            /** @var Mage_Sales_Model_Quote_Item $_item */
            foreach ($this->getQuote()->getAllItems() as $_item) {
                $product = $_item->getProduct();
                if (($product->getPackageId() == 15) || ($product->getPackageId() == 17 )) {
                    $rdNote = 'Additional charges may apply to remote locations. ';
                }
            }
            $note = $rdNote.$note;
        }

        return $note;
    }

    /**
     * Collect Rates By Address
     *
     * Based on method from shipping/shipping, but adding in areaName to request
     *
     * @param Varien_Object $address
     * @return Mage_Shipping_Model_Shipping
     */
    protected function _collectRatesByAddress(Varien_Object $address)
    {
        $request = Mage::getModel('shipping/rate_request');
        $request->setAllItems($address->getAllItems());
        $request->setDestCountryId($address->getCountryId());
        $request->setDestRegionId($address->getRegionId());
        $request->setDestPostcode($address->getPostcode());
        $request->setPackageValue($address->getBaseSubtotal());
        $request->setPackageValueWithDiscount($address->getBaseSubtotalWithDiscount());
        $request->setPackageWeight($address->getWeight());
        $request->setFreeMethodWeight($address->getFreeMethodWeight());
        $request->setPackageQty($address->getItemQty());
        $request->setStoreId(Mage::app()->getStore()->getId());
        $request->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
        $request->setBaseCurrency(Mage::app()->getStore()->getBaseCurrency());
        $request->setPackageCurrency(Mage::app()->getStore()->getCurrentCurrency());

        if ($areaName = $this->_regionHelper->getAreaName()) {
            $request->setEstimatorAreaName($areaName);
        }

        $request->setBaseSubtotalInclTax($address->getBaseSubtotalInclTax()
            + $address->getBaseExtraTaxAmount());
        return Mage::getModel('shipping/shipping')->collectRates($request);
    }

    protected function _sortRate($a, $b)
    {
        $carrierSort = ['productmatrix' => 1, 'tradetested_depot' => 2, 'pickup' => 3];
        if (($a->getCarrier() == 'tradetested_depot') && ($b->getCarrier() == 'tradetested_depot')) {
            return floatval($b->getDepotLatitude()) <=> floatval($a->getDepotLatitude());
        }
        return ($carrierSort[$a->getCarrier()] ?? 0) - ($carrierSort[$b->getCarrier()] ?? 0);
    }
}