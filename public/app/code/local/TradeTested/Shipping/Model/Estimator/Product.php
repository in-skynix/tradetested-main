<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 23/01/14
 * Time: 2:35 PM
 */
class TradeTested_Shipping_Model_Estimator_Product extends TradeTested_Shipping_Model_Estimator_Abstract
{
    /** @var  null|Varien_Object */
    protected $_request;
    
    /** @var  Mage_Catalog_Model_Product */
    protected $_product;


    //We don't store product in session data.
    public function setProduct(Mage_Catalog_Model_Product $product)
    {
        if ($product->getTypeId() == 'configurable') {
            //just use the first available
            $request = array('super_attribute' => array(), 'qty' => 1, 'product' => $product->getId());
            foreach($product->getTypeInstance()->getConfigurableAttributes($product) as $_attribute) {
                foreach($_attribute->getPrices() as $_price) {
                    $request['super_attribute'][$_attribute->getAttributeId()] = $_price['value_index'];
                    break;
                }
            }
            $this->_request = new Varien_Object($request);
        }
        $this->_product = $product;
        return $this;
    }

    /**
     * Get Currently Select Area Name/
     * @return mixed|string
     */
    public function getAreaName()
    {
        if (Mage::app()->getStore()->getCode() == 'default') {
            return $this->_regionHelper->getAreaName();
        }

        if ($postcode = $this->_regionHelper->getPostcode()) {
            $countryCode = Mage::getStoreConfig('general/country/default');
            $country = Mage::getModel('directory/country')->loadByCode($countryCode);
            return $postcode.', '.$country->getName();
        }

        //else select first from matrix rates
        foreach ($this->getRates() as $_rate) {
            if ($label = $_rate->getAreaName()) {
                return $label;
            }
        }
        return 'Unidentified Area';
    }

    /**
     * @return bool
     */
    public function isComplete()
    {
        return (boolean)($this->_regionHelper->getPostcode() || $this->_regionHelper->getAreaName());
    }
    
    protected function _canDisplayRates()
    {
        return ($this->_product && $this->_product->isAvailable());
    }

    /**
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        if (!$this->_quote) {
            $this->_quote = Mage::getModel('sales/quote');
            if ($q = Mage::getSingleton('checkout/session')->getQuote()) {
                $this->_quote->getShippingAddress()->setPostcode($q->getShippingAddress()->getPostcode());
            }
            try {
                $this->_quote->addProduct($this->_product, $this->_request);
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('core/session')->addError($e->getMessage());
            }
        }
        return $this->_quote;
    }
}