<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 17/05/16
 * Time: 3:56 PM
 */
class TradeTested_Shipping_Model_Depot_Rate_Result extends Mage_Shipping_Model_Rate_Result
{
    public function sortRatesByPrice()
    {
        usort($this->_rates, function($a, $b){return intval($b->getDepotLatitude() - $a->getDepotLatitude());});
        return $this;
    }
}