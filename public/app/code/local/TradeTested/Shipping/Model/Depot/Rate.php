<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 27/04/2016
 * Time: 17:33
 *
 * @method int getDepotRateId()
 * @method $this setDepotRateId(int $value)
 * @method int getDepotId()
 * @method $this setDepotId(int $value)
 * @method int getWebsiteId()
 * @method $this setWebsiteId(int $value)
 * @method string getPackageId()
 * @method $this setPackageId(string $value)
 * @method string getPackageCode()
 * @method $this setPackageCode(string $value)
 * @method float getWeightFrom()
 * @method $this setWeightFrom(float $value)
 * @method float getWeightTo()
 * @method $this setWeightTo(float $value)
 * @method float getPrice()
 * @method $this setPrice(float $value)
 * @method string getAlgorithm()
 * @method $this setAlgorithm(string $value)
 * @method string getShippingTimeEstimate()
 * @method $this setShippingTimeEstimate(string $value)
 *
 * @method string getDepotName()
 * @method string getDepotCode()
 * @method float getDepotLatitude()
 * @method float getDepotLongitude()
 * @method string getDepotAddress()
 * @method string getDepotState()
 * @method string getDepotPostcode()
 */
class TradeTested_Shipping_Model_Depot_Rate extends Mage_Core_Model_Abstract
{
    const CACHE_TAG = 'tradetested_shipping.depot_rate';
    protected $_cacheTag = 'tradetested_shipping.depot_rate';

    protected function _construct()
    {
        $this->_init('tradetested_shipping/depot_rate');
    }

    public function getRateResult(Mage_Shipping_Model_Rate_Request $request)
    {
        $addressState = in_array($this->getDepotState(), ['NI', 'SI']) ? "" : ", ".$this->getDepotState();
        $address = $this->getDepotAddress().", ".$this->getDepotName().$addressState." "
            .sprintf("%04d",$this->getDepotPostcode());
        $data = array_merge(
            $this->getData(), 
            [
                'method'       => $this->getDepotCode(),
                'state'        => $this->getDepotState(),
                'method_title' => $this->getDepotName() . " Depot",
                'address'      => $address,
            ], 
            Mage::helper('tradetested_shipping/algorithm')->calculate($request, $this)
        );
        
        if($this->_isEligibleForFreeShipping($request)) {
            $data['price'] = 0;
        }

        return Mage::getModel('shipping/rate_result_method')->setData($data);
    }

    /**
     * Either cart is eligible for free shipping, or all individual items in cart are
     * 
     * @param Mage_Shipping_Model_Rate_Request $request
     *
     * @return bool
     */
    protected function _isEligibleForFreeShipping(Mage_Shipping_Model_Rate_Request $request)
    {
        if ($request->getFreeShipping() === true) {
            return true;
        }
        
        $freeQty = 0;
        if ($request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {
                if ($item->getFreeShipping()) {
                    $freeShipping = is_numeric($item->getFreeShipping()) ? $item->getFreeShipping() : 0;
                    $freeQty += $item->getQty() - $freeShipping;
                }
            }
        }
        return ($request->getPackageQty() == $freeQty);
    }

    public function getWebsiteCode() {
        return Mage::app()->getWebsite($this->getWebsiteId())->getCode();
    }
}