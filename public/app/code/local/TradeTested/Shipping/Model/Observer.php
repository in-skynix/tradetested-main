<?php

/**
 * Class TradeTested_Shipping_Model_Observer
 */
class TradeTested_Shipping_Model_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     *
     * the fix in Avid_TTCatalog_Model_Rewrite_SalesRule_Validator requires that weight is loaded,
     * but Mage_Catalog_Model_Product_Type_Configurable::getProductByAttributes doesn't load weight
     * when we add a configurable product to a quote without saving using shipping estimator.
     */
    public function catalogProductCollectionLoadBefore(Varien_Event_Observer $observer)
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = $observer->getEvent()->getCollection();
        if ($collection->getFlag('product_children')) {
            $collection->addAttributeToSelect('weight');
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     * 
     * When adding product to cart, we check to see if there was a shipping method chosen, and set it on the quote
     * if we can...
     */
    public function checkoutCartAddProductComplete(Varien_Event_Observer $observer)
    {
        /** @var Mage_Core_Controller_Request_Http $request */
        $request = $observer->getEvent()->getRequest();
        if ($methodCode = $request->getPost('shipping_method')) {
            //Set the checkout state to begin so that it doesn't reset the cart and clear addresses.
            Mage::getSingleton('checkout/session')->setCheckoutState(Mage_Checkout_Model_Session::CHECKOUT_STATE_BEGIN);
            $address = Mage::getSingleton('checkout/cart')->getQuote()->getShippingAddress();
            $address->setCountryId(Mage::getStoreConfig('general/country/default'))
                ->setShippingMethod($methodCode)
                ->save();
        }
    }
}