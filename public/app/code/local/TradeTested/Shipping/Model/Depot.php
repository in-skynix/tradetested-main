<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 27/04/2016
 * Time: 13:44
 */

/**
 * Class TradeTested_Shipping_Model_Depot
 *
 * @method int getDepotId()
 * @method $this setDepotId(int $value)
 * @method string getCode()
 * @method $this setCode(string $value)
 * @method string getCountryCode()
 * @method $this setCountryCode(string $value)
 * @method string getName()
 * @method $this setName(string $value)
 * @method string getAddress()
 * @method $this setAddress(string $value)
 * @method string getState()
 * @method $this setState(string $value)
 * @method int getPostcode()
 * @method $this setPostcode(int $value)
 * @method float getLatitude()
 * @method $this setLatitude(float $value)
 * @method float getLongitude()
 * @method $this setLongitude(float $value)
 */
class TradeTested_Shipping_Model_Depot extends Mage_Core_Model_Abstract
{
    const CACHE_TAG     = 'tradetested_shipping.depot';
    protected $_cacheTag= 'tradetested_shipping.depot';

    protected function _construct()
    {
        $this->_init('tradetested_shipping/depot');
    }
}