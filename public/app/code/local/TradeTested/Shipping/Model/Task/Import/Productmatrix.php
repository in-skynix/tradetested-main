<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 22/06/16
 * Time: 3:49 PM
 */
class TradeTested_Shipping_Model_Task_Import_Productmatrix extends TradeTested_Core_Model_Task_Abstract
{
    public function run($args)
    {
		Mage::getResourceModel('productmatrix_shipping/carrier_productmatrix')
            ->import($args['csv_file'], $args['website_id']);
    }
}