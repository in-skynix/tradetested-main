<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 27/04/2016
 * Time: 13:46
 */
class TradeTested_Shipping_Model_Resource_Depot extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource model
     */
    protected function _construct()
    {
        $this->_init('tradetested_shipping/depot', 'depot_id');
    }

    /**
     * @return $this
     */
    public function truncate() {
        $this->_getWriteAdapter()->query('DELETE FROM '.$this->getMainTable());
        return $this;
    }
    
    public function load(Mage_Core_Model_Abstract $object, $value, $field = null)
    {
        if (is_null($field)) {
            $field = $this->getIdFieldName();
        }

        $read = $this->_getReadAdapter();
        if ($read && !is_null($value)) {
            $select = $this->_getLoadSelect($field, $value, $object);
            $data = $read->fetchRow($select);

            if ($data) {
                $object->setData($data);
            }
        }

        $this->unserializeFields($object);
        $this->_afterLoad($object);

        return $this;
    }
}