<?php
class TradeTested_Shipping_Model_Resource_Depot_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('tradetested_shipping/depot');
    }
}