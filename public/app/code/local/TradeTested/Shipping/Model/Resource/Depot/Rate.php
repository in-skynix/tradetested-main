<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 27/04/2016
 * Time: 18:27
 */
class TradeTested_Shipping_Model_Resource_Depot_Rate extends Mage_Core_Model_Resource_Db_Abstract
{
    protected $_importedDepotIds = [];
    protected $_csvHeaders = [
        'website_code', 'depot_code', 'package_code', 'weight_from', 'weight_to', 'price', 'algorithm',
        'shipping_time_estimate', 'depot_name', 'depot_latitude', 'depot_longitude', 'depot_address', 'depot_state',
        'depot_postcode', 'depot_country_code',
    ];

    /**
     * Initialize resource model
     */
    protected function _construct()
    {
        $this->_init('tradetested_shipping/depot_rate', 'depot_rate_id');
    }

    /**
     * Retrieve select object for load object data
     *
     * @param string                   $field
     * @param mixed                    $value
     * @param Mage_Core_Model_Abstract $object
     *
     * @return Zend_Db_Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $field  = $this->_getReadAdapter()->quoteIdentifier(sprintf('%s.%s', $this->getMainTable(), $field));
        $select = $this->_getReadAdapter()->select()->from($this->getMainTable())->join(
            ['depots' => $this->getTable('tradetested_shipping/depot')], 'main_table.depot_id = depots.depot_id', [
                'depot_name'      => 'name',
                'depot_code'      => 'code',
                'depot_latitude'  => 'latitude',
                'depot_longitude' => 'longitude',
                'depot_address'   => 'address',
                'depot_state'     => 'state',
                'depot_postcode'  => 'postcode',
                'depot_country_code'  => 'country_code',
            ]

        )->where(sprintf('%s.%s = ?', $this->getMainTable(), $field), $value);

        return $select;
    }

    /**
     * @return $this
     */
    public function truncate() {
        $this->_getWriteAdapter()->query('DELETE FROM '.$this->getMainTable());
        return $this;
    }

    /**
     * CSV Import Routine
     *
     * [dane][avid] Rewrote entire method
     *
     * @param Varien_Object $object
     * @return $this
     * @throws Exception
     */
    public function uploadAndImport(Varien_Object $object)
    {
        if (empty($_FILES['groups']['tmp_name']['tradetested_depot']['fields']['import']['value'])) {
            return $this;
        }
        $csvFile = $_FILES['groups']['tmp_name']['tradetested_depot']['fields']['import']['value'];
        $packageOptions = Mage::getResourceModel('tradetested_shipping/depot_rate_collection')->getAllPackageOptions();
        $connection = $this->_getWriteAdapter();
        $connection->beginTransaction();
        Mage::getResourceModel('tradetested_shipping/depot_rate')->truncate();
        Mage::getResourceModel('tradetested_shipping/depot')->truncate();
        $csv = Mage::getModel('tradetested_core/csv')->open($csvFile, $this->_csvHeaders, 'r');
        $csv->withRows([$this, 'importRow'], ['package_options' => $packageOptions]);
        $connection->commit();
        Mage::getSingleton('adminhtml/session')
            ->addSuccess(Mage::helper('adminhtml')->__($csv->getTotalRows().' rows have been imported.'));
        return $this;
    }

    public function importRow($data, $lineNo)
    {
        $depotCode = $data['depot_code'];
        if (!isset($this->_importedDepotIds[$depotCode])) {
            $depotData = [];
            foreach ($data as $_k => $_v) {
                if (strpos($_k, 'depot_') === 0) {
                    $depotData[str_replace('depot_', '', $_k)] = $_v;
                }
            }
            if (!$depotData['latitude']) {
                $postcodeObject = Mage::getModel('tradetested_shipping/postcode')->loadByPostcode(
                    $depotData['postcode'], $depotData['country_code']
                );
                $depotData['latitude'] = $postcodeObject['latitude'];
                $depotData['longitude'] = $postcodeObject['longitude'];
            }
            $depot = Mage::getModel('tradetested_shipping/depot')->load($depotCode, 'code')->setData($depotData)->save();
            $this->_importedDepotIds[$depotCode] = $depot->getId();
        }
        $data['depot_id'] = $this->_importedDepotIds[$depotCode];
        if (!($data['package_id']??false)) {
            $data['package_id'] = $data['package_options'][$data['package_code']];
        }
        if (!($data['website_id']??false)) {
            $data['website_id'] = Mage::app()->getWebsite($data['website_code'])->getId();
        }
        Mage::getModel('tradetested_shipping/depot_rate')->setData($data)->save();

    }

    public function exportCSV()
    {
        $collection = Mage::getResourceModel('tradetested_shipping/depot_rate_collection')->addPackageCodes();
        return Mage::getModel('tradetested_core/csv')->open('php://temp', $this->_csvHeaders)
            ->writeCollection($collection, true, 3000)->getCSVStream();
    }
}
