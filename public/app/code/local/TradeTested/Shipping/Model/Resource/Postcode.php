<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 27/04/2016
 * Time: 13:46
 */
class TradeTested_Shipping_Model_Resource_Postcode extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource model
     */
    protected function _construct()
    {
        $this->_init('tradetested_shipping/postcode', 'postcode_id');
    }

    /**
     * @param Varien_Object $object
     * @param               $postcode
     * @param               $countryCode
     *
     * @return $this
     */
    public function loadByPostcode(Varien_Object $object, $postcode, $countryCode)
    {
        $read = $this->_getReadAdapter();
        if ($read && $postcode && $countryCode) {
            $select = $read->select()
                ->from($this->getMainTable())
                ->where('postcode =:postcode')
                ->where('country_code = :country_code');
            $data = $read->fetchRow($select, [':postcode' => $postcode, ':country_code' => $countryCode]);
            if ($data) {
                $object->setData($data);
            }
        }
        if ($object instanceof Mage_Core_Model_Abstract) {
            $this->_afterLoad($object);
        }
        return $this;
    }
}