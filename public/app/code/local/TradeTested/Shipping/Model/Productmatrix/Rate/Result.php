<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 23/06/16
 * Time: 1:49 PM
 */
class TradeTested_Shipping_Model_Productmatrix_Rate_Result extends Mage_Shipping_Model_Rate_Result
{
    public function sortRatesByPrice()
    {
        usort($this->_rates, function($a, $b){
            $diff = $b->getPrice() - $a->getPrice();
            if ($diff == 0) {
                $diff = strlen($b->getMethod()) - strlen($a->getMethod());
            }
            return $diff;
        });
        return $this;
    }
}