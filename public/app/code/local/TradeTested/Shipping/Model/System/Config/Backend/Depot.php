<?php
class TradeTested_Shipping_Model_System_Config_Backend_Depot extends Mage_Core_Model_Config_Data
{
    public function _afterSave()
    {
        Mage::getResourceModel('tradetested_shipping/depot_rate')->uploadAndImport($this);
    }
}
