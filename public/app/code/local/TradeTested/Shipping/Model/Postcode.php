<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 16/05/16
 * Time: 5:14 PM
 */

/**
 * Class TradeTested_Shipping_Model_Postcode
 *
 * @method int getPostcode()
 * @method $this setPostcode(int $value)
 * @method string getCountryCode()
 * @method $this setCountryCode(string $value)
 * @method string getState()
 * @method $this setState(string $value)
 * @method string getName()
 * @method $this setName(string $value)
 * @method float getLatitude()
 * @method $this setLatitude(float $value)
 * @method float getLongitude()
 * @method $this setLongitude(float $value)
 */
class TradeTested_Shipping_Model_Postcode extends Mage_Core_Model_Abstract
{
    const CACHE_TAG     = 'tradetested_shipping.postcode';
    protected $_cacheTag= 'tradetested_shipping.postcode';

    protected function _construct()
    {
        $this->_init('tradetested_shipping/postcode');
    }

    /**
     * @param $lat
     * @param $long
     *
     * @return float Distance in KM 
     */
    public function getDistanceFrom($lat, $long)
    {
        $theta = $this->getLongitude() - $long;
        $dist = sin(deg2rad($this->getLatitude())) * sin(deg2rad($lat)) 
            + cos(deg2rad($this->getLatitude())) * cos(deg2rad($lat)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        return $dist * 60 * 1.85316;
    }

    /**
     * @param $postcode
     * @param $countryCode
     *
     * @return $this
     */
    public function loadByPostcode($postcode, $countryCode)
    {
        $this->getResource()->loadByPostcode($this, $postcode, $countryCode);
        return $this;
    }
}
