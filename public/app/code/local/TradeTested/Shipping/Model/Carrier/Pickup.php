<?php
class TradeTested_Shipping_Model_Carrier_Pickup
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{

    protected $_code = 'pickup';
    protected $_isFixed = true;

    /**
     * Enter description here...
     *
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return Mage_Shipping_Model_Rate_Result
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigFlag('active'))
            return false;
        $method = Mage::getModel('shipping/rate_result_method')
            ->setCarrier('pickup')
            ->setCarrierTitle($this->getConfigData('title'))
            ->setMethodTitle($this->getConfigData('name'))
            ->setMethod('store')
            ->setPrice(0.00)
            ->setCost(0)
            ->setAddress($this->getAddress());
        return Mage::getModel('shipping/rate_result')->append($method);
    }

    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        return array('pickup'=>Mage::helper('shipping')->__('Store Pickup'));
    }

    /**
     * @return null|string
     */
    public function getAddressNote()
    {
        if (Mage::app()->getStore()->getCode() == 'default') {
            return "38 Airpark Drive, Mangere, Auckland 2022";
        } else {
            return "";
        }
    }
}
