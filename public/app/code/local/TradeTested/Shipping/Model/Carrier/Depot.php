<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 27/04/2016
 * Time: 14:21
 */
class TradeTested_Shipping_Model_Carrier_Depot
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
    protected $_code = 'tradetested_depot';

    /**
     * @todo: return one per depot.
     * @return array
     */
    public function getAllowedMethods()
    {
        return [
            'standard' => 'Standard delivery',
            'express'  => 'Express delivery',
        ];
    }

    public function exportCSV()
    {
        return Mage::getResourceModel('tradetested_shipping/depot_rate')->exportCSV();
    }

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        /** @var Mage_Shipping_Model_Rate_Result $result */
        $result = Mage::getModel('tradetested_shipping/depot_rate_result');

        $this->_addAdditionalRequestData($request);

        $depotRates = Mage::getModel('tradetested_shipping/depot_rate')->getCollection()
            ->addFieldToFilter('country_code', $request->getDestCountryId())
            ->addFieldToFilter('package_id', ['in' => $request->getPackageIds()])
            ->addFieldToFilter('weight_from', ['lteq' => $request->getPackageWeight()])
            ->addFieldToFilter('weight_to', ['gt' => $request->getPackageWeight()])
            ->setOrder('latitude', 'DESC')
        ;
        $depotRates->getSelect()->order('latitude DESC');

        $postcode = $request->getDestPostcode()
            ?? Mage::helper('tradetested_regions')->estimatePostcode($request->getEstimatorAreaName());
        $pc = Mage::getModel('tradetested_shipping/postcode')
            ->loadByPostcode($postcode, Mage::getStoreConfig('general/country/default'));

        foreach ($this->_compactDepotRates($depotRates) as $_depotRate) {
            $result->append(
                $_depotRate->getRateResult($request)
                    ->setCarrier($this->_code)
                    ->setCarrierTitle($this->getConfigData('title'))->setDistance(
                        intval($pc->getDistanceFrom($_depotRate->getDepotLatitude(), $_depotRate->getDepotLongitude()))
                    )
            );
        }
        
        return $result;
    }

    /**
     * Compact Depot Rates so there is only one per method code. E.g. pick the highest price for a given method.
     *
     * @param TradeTested_Shipping_Model_Resource_Depot_Rate_Collection $depotRates
     *
     * @return array
     */
    protected function _compactDepotRates(TradeTested_Shipping_Model_Resource_Depot_Rate_Collection $depotRates)
    {
        $rates = [];
        foreach ($depotRates as $_depotRate) {
            $code = $_depotRate->getDepotCode();
            if (!isset($rates[$code]) || ($_depotRate->getPrice() > $rates[$code]->getPrice())) {
                $rates[$code] = $_depotRate;
            }
        }

        return $rates;
    }

    /**
     * @todo:
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return array
     */
    protected function _addAdditionalRequestData(Mage_Shipping_Model_Rate_Request $request)
    {
        $packageIds = [];
        /** @var Mage_Sales_Model_Quote_Item $_item */
        foreach ($request->getAllItems() as $_item) {
            $packageId = $_item->getProduct()->getPackageId()??'*';
            $packageIds[$packageId] = $packageId;
        }
        $request->addData(['package_ids' => array_keys($packageIds)]);
    }

    public function getAddressNote($rate)
    {
        $depot = Mage::getModel('tradetested_shipping/depot')->load($rate->getMethod(), 'code');
        $addressState = in_array($depot->getState(), ['NI', 'SI']) ? "" : ", ".$depot->getState();
        return $depot->getAddress().", ".$depot->getName().$addressState." "
            .sprintf("%04d",$depot->getPostcode());
    }
}