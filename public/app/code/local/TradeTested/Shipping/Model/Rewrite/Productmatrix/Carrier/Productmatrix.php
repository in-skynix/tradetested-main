<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 24/01/14
 * Time: 10:14 AM
 */

/**
 * Class Avid_TTCatalog_Model_Rewrite_Productmatrix_Carrier_Productmatrix
 *
 * Overload to simply add extra fields into the rates that are returned.
 * 
 * Allow retrieving rates and having them saved against quote before a postcode is set
 */
class TradeTested_Shipping_Model_Rewrite_Productmatrix_Carrier_Productmatrix
    extends Webshopapps_Productmatrix_Model_Carrier_Productmatrix
{
    protected function _getQuotes()
    {
        $result = Mage::getModel('tradetested_shipping/productmatrix_rate_result');
        $request = $this->_rawRequest;
        $version = Mage::helper('wsacommon')->getVersion();
        $freeTextSet = false;

        $customErrorText = null;
        
        //[dane][tradetested] Allow retrieving rates and having them saved against quote before a postcode is set
        //(if we know the region)
        if (!$request->getDestPostcode()) {
            $request->setEstimatorAreaName(Mage::helper('tradetested_regions')->getAreaName());
        }
        $ratearray = $this->getRate($request,$customErrorText);

        //This is fixing M1.4.0-1.4.1 when a cart is purely free shipping.
        $brokenFree = false;

        if ($version == 1.6 || $version == 1.7 || $version == 1.8){
            if ($request->getFreeShipping() === true || $request->getPackageQty() == $this->getFreeBoxes()){
                $brokenFree = true;
            }
        }

        if (empty($ratearray)) {
            if(!$brokenFree){
                if ($this->getConfigData('specificerrmsg')!='') {
                    $error = Mage::getModel('shipping/rate_result_error');
                    $error->setCarrier('productmatrix');
                    $error->setCarrierTitle($this->getConfigData('title'));
                    $error->setErrorMessage($customErrorText == null ? $this->getConfigData('specificerrmsg') :
                        $customErrorText);
                    $result->append($error);
                }
                return $result;
            } else {
                $method = Mage::getModel('shipping/rate_result_method');
                $method->setCarrier('productmatrix');
                $method->setCarrierTitle($this->getConfigData('title'));

                $shippingPrice = 0.00;

                if ($this->getConfigData('free_shipping_text') != "") {
                    $modifiedName=preg_replace('/&|;| /',"_",$this->getConfigData('free_shipping_text'));
                    $method->setMethodTitle($this->getConfigData('free_shipping_text'));
                    $freeTextSet = true;
                } else {
                    $modifiedName="productmatrix_free_promotion";
                    $method->setMethodTitle(Mage::helper('shipping')->__('Free Shipping'));
                }
                $method->setMethod($modifiedName);
                $method->setMethodDescription('Free Shipping');
                $method->setPrice($shippingPrice);
                $method->setCost($shippingPrice);
                $method->setDeliveryType('productmatrix_free_promotion');
                $result->append($method);
                return $result;
            }
        }

        $max_shipping_cost=$this->getConfigData('max_shipping_cost');
        $min_shipping_cost=$this->getConfigData('min_shipping_cost');

        foreach ($ratearray as $rate)
        {
            if (!empty($rate) && $rate['price'] >= 0) {
                $method = Mage::getModel('shipping/rate_result_method');

                $method->setCarrier('productmatrix');
                $method->setCarrierTitle($this->getConfigData('title'));

                $price=$rate['price'];
                if (!empty($max_shipping_cost) && $max_shipping_cost>0 && $price>$max_shipping_cost ) {
                    $price=$max_shipping_cost;
                }
                if (!empty($min_shipping_cost) && $min_shipping_cost>0 && $price<$min_shipping_cost) {
                    $price=$min_shipping_cost;
                }
                if (!in_array('apply_handling',$this->_options) && $price==0) {
                    $shippingPrice = $price;
                } else {
                    $shippingPrice = $this->getFinalPriceWithHandlingFee($price);
                }
                //this is a fix for 1.4.1.1 and earlier versions where the free ship logic used for UPS doesnt work
                if (($version == 1.6 || $version == 1.7 || $version == 1.8) &&
                    ($request->getFreeShipping() === true || $request->getPackageQty() == $this->getFreeBoxes() )) {
                    $shippingPrice = 0.00;
                    if ($this->getConfigData('free_shipping_text') != "") {
                        $modifiedName=preg_replace('/&|;| /',"_",$this->getConfigData('free_shipping_text'));
                        $method->setMethodTitle($this->getConfigData('free_shipping_text'));
                        $freeTextSet = true;
                    } else {
                        $modifiedName=preg_replace('/&|;| /',"_",$rate['delivery_type']);
                        $method->setMethodTitle(Mage::helper('shipping')->__($rate['delivery_type']));
                    }
                }
                else if ($price==0  && $this->getConfigData('zero_shipping_text')!='') {
                    $modifiedName=preg_replace('/&|;| /',"_",$this->getConfigData('zero_shipping_text'));
                    $method->setMethodTitle($this->getConfigData('zero_shipping_text'));
                } else {
                    $modifiedName=preg_replace('/&|;| /',"_",$rate['method_name']);
                    $method->setMethodTitle(Mage::helper('shipping')->__($rate['delivery_type']));
                }

                $method->setMethod($modifiedName);
                $method->setMethodDescription($rate['notes']);

                $method->setPrice($shippingPrice);
                $method->setCost($rate['cost']);
                $method->setDeliveryType($rate['delivery_type']);
//[dane][avid] Add extra fields.
                $method->setShippingTimeEstimate($rate['shipping_time_estimate'])
                    ->setAreaName($rate['area_name'])
                    ->setCmsBlock($rate['cms_block'])
                    ;

                $result->append($method);

                if ($freeTextSet) break;
            }
        }
        return $result;
    }

    public function exportCSV()
    {
        return Mage::getResourceModel('productmatrix_shipping/carrier_productmatrix')->exportCSV();
    }
}