<?php
class TradeTested_Shipping_Model_Rewrite_Sales_Quote_Address_Rate extends Mage_Sales_Model_Quote_Address_Rate
{
    /*
     * Also save cms_block value of rate if exists. For use in the shipping methods step.
     */
    public function importShippingRate(Mage_Shipping_Model_Rate_Result_Abstract $rate)
    {
        if ($rate instanceof Mage_Shipping_Model_Rate_Result_Error) {
            $this
                ->setCode($rate->getCarrier().'_error')
                ->setCarrier($rate->getCarrier())
                ->setCarrierTitle($rate->getCarrierTitle())
                ->setErrorMessage($rate->getErrorMessage())
            ;
        } elseif ($rate instanceof Mage_Shipping_Model_Rate_Result_Method) {
            $cmsBlock = $rate->getCmsBlock() ? $rate->getCmsBlock() : 'shipping_details_'.$rate->getCarrier();
            $this
                ->setCode($rate->getCarrier().'_'.$rate->getMethod())
                ->setCarrier($rate->getCarrier())
                ->setCarrierTitle($rate->getCarrierTitle())
                ->setMethod($rate->getMethod())
                ->setMethodTitle($rate->getMethodTitle())
                ->setMethodDescription($rate->getMethodDescription())
                ->setPrice($rate->getPrice())
                ->setCmsBlock($cmsBlock)
                ->setShippingTimeEstimate($rate->getShippingTimeEstimate())
                ->setDistance($rate->getDistance())
                ->setState($rate->getState())
            ;
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressNote()
    {
        $note = Mage::getModel('shipping/shipping')->getCarrierByCode($this->getCarrier())->getAddressNote($this);
        return $note === null ? $this->getAddress()->format('oneline') : $note;
    }
}
