<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 23/01/14
 * Time: 4:45 PM
 */
/**
 * Class Avid_TTCatalog_Model_Rewrite_Productmatrix_Mysql4_Carrier_Productmatrix
 *
 * Overloading ProductMatrix Carrier Resource
 *
 * Because the ProductMatrix code has so many private methods and private variables, and MASSIVE methods, we have
 * a very large chunk of code in order to edit just a couple of lines. Marked //[dane][avid]
 *
 * WARNING: The following variables need to REMAIN AT ALL TIMES filtered for SQL.
 * $this->_zipSearchString
 * $this->_longMatchPostcode
 * $this->_shortMatchPostcode
 * 
 */
class TradeTested_Shipping_Model_Rewrite_Productmatrix_Mysql4_Carrier_Productmatrix
    extends Webshopapps_Productmatrix_Model_Mysql4_Carrier_Productmatrix
{
    /** @var  TradeTested_Core_Model_Task_Manager */
    protected $_taskManager;

    /**
     * Parent constructs with incorrect resource name!
     */
    protected function _construct()
    {
        $this->_init('productmatrix_shipping/productmatrix', 'pk');
    }
    
    private $_request;
    private $_zipSearchString;
    private $_table;
    private $_customerGroupCode;
    private $_starIncludeAll;
    private $_minusOne;
    private $_exclusionList;
    private $_structuredItems;
    private $_hasEmptyPackages;
    private $_prioritySet;
    private $_maxPriceSet;
    private $_roundPrice;
    private $_freeShipping;
    private $_useParent;
    private $_useBundleParent=false;
    private $_useConfigurableParent=true;
    private $_appendStarRates;
    private $_debug;
    private $_options;
    private $_ignoreAdditionalItemPrice;
    private $_shortMatchPostcode = '';
    private $_longMatchPostcode = '';
    protected $_postcodeSearchString = '';
    protected $_postcodeFilterCanada = false;
    private $_uDrop;


    /**
     * [dane][avid] Even though this method is protected, and identical, it needs to be copied here because
     * IT OPERATES ON A PRIVATE VARIABLE
     * @param $finalResults
     * @return array
     */
    protected function organisePriorities(&$finalResults) {
        if ($this->_prioritySet) {
            foreach ($finalResults as $i => $rate) {
                $priceArr[$i] = $rate['price'];
                $priority[$i] = $rate['priority'];
            }

            array_multisort($priceArr, SORT_ASC, $priority, SORT_ASC, $finalResults);
            $previousPrice=-100;
            $previousPriority="";
            foreach ($finalResults as $data) {
                if ($previousPrice==$data['price'] && $data['priority']!=$previousPriority  && is_numeric($data['priority']) && is_numeric($previousPriority)) {
                    continue;
                } else {
                    $previousPrice=$data['price'];
                    $previousPriority=$data['priority'];
                    $absoluteResults[]=$data;
                }
            }

        } else {
            $absoluteResults=$finalResults;
        }

        return $absoluteResults;
    }

//[dane][avid] Have to copy this entire method, as we've edited a private method, and will only call that private method
//if the method calling it is in the same class.
    public function getNewRate(Mage_Shipping_Model_Rate_Request $request, &$errorText)
    {
        // make global as used all around
        $this->_request=$request;
        $this->_debug= Mage::helper('wsalogger')->isDebug('Webshopapps_Productmatrix');
        $this->_hasEmptyPackages=false;
        $this->_prioritySet = false;
        $this->_ignoreAdditionalItemPrice=false;
        $this->_maxPriceSet = false;
        $this->_freeShipping = $request->getFreeShipping();
        $this->_starIncludeAll = Mage::getStoreConfig("carriers/productmatrix/star_include_all");
        $this->_options = explode(',',Mage::getStoreConfig("carriers/productmatrix/ship_options"));
        $this->_uDrop = Mage::getConfig()->getNode('productmatrix/specialvars/udrop') == 1;
        $this->_table = Mage::getSingleton('core/resource')->getTableName('productmatrix_shipping/productmatrix');
        $this->_appendStarRates =  in_array('append_star_rates',$this->_options);
        $this->_roundPrice = in_array('round_price',$this->_options);

        $items = $request->getAllItems();
        if (empty($items) || ($items=='')) {
            return;
        }

        $this->_customerGroupCode = $this->getCustomerGroupCode();
        // get the package_id's for the items in the cart

        $this->_processPostcode();
        $this->_populateStructuredItems();

        $first=true;
        $finalResults = array();
        foreach ($this->_structuredItems as $structuredItem) {
            if ($structuredItem['package_id']=='none' && $this->_starIncludeAll) { continue; }
            $this->_minusOne=false;
            $data=$this->_getItemData($structuredItem, $errorText);
            if (!empty($data)) {
                if(!$first) {
                    switch ($this->_request->getPMConditionName()) {
                        case 'highest':
                            $this->mergeHighest($data,$finalResults);
                            break;
                        case 'lowest':
                            $this->mergeLowest($data,$finalResults);
                            break;
                        case 'order':
                            $this->mergeOrdered($data,$finalResults);
                            break;
                        default:
                            $this->mergeResults($data,$finalResults);
                    }
                } else {
                    $first=false;
                    $finalResults=$data;
                }
            } elseif (!$this->_starIncludeAll  || (!$this->_starIncludeAll && $this->_minusOne)) {
                return;
            }
        }

        $this->_removeExcludedDeliveryMethods($finalResults);

        if (empty($finalResults)) { return; }
        $absoluteResults = $this->organisePriorities($finalResults);

        if (in_array('custom_sorting',$this->_options)) {
            usort($absoluteResults, array($this,'cmp_notes'));
        }

        $this->manipulatePricing($absoluteResults,$this->_request->getPMConditionName());
        return $absoluteResults;

    }
    
    protected $_headers = [['country_id', 'dest_country_id'], ['region_id', 'dest_region_id'], ['city', 'dest_city'],
['postcode', 'dest_zip'], ['postcode_to', 'dest_zip_to'], 'package_id',
['weight_from', 'weight_from_value'], ['weight_to', 'weight_to_value'], ['price_from', 'price_from_value'],
['price_to', 'price_to_value'], ['item_from', 'item_from_value'], ['item_to', 'item_to_value'],
'price', 'algorithm', 'customer_group', 'cost', 'delivery_type', 'notes', 'shipping_time_estimate',
'area_name', 'cms_block'
];
    
    public function exportCSV()
    {
        $rates = Mage::getResourceModel('productmatrix_shipping/carrier_productmatrix_collection')
            ->addFieldToFilter('website_id', 2);
        return Mage::getModel('tradetested_core/csv')->open('php://temp', $this->_headers)
            ->writeCollection($rates, true, 3000)->getCSVStream();
    }
    
    public function importRow($data, $lineNo)
    {
        foreach (['dest_country_id', 'customer_group', 'dest_region_id', 'dest_zip', 'dest_city'] as $_splittable) {
            $values = explode(",", $data[$_splittable]);
            if (count($values) > 1) {
                foreach ($values as $_value) {
                    $data[$_splittable] = $_value;
                    $this->importRow($data, $lineNo);
                }
            } else {
                $data[$_splittable] = $values[0];
            }
        }
        
        $data['dest_zip_to'] = trim(strtoupper($data['dest_zip_to']));
        $data['dest_zip'] = trim(strtoupper($data['dest_zip']));
        $data['dest_city'] = strval(trim($data['dest_city']??''));
        $data['shipping_time_estimate'] = strval(trim($data['shipping_time_estimate']));

        foreach (
            [
                'weight_from_value',
                'weight_to_value',
                'price_from_value',
                'price_to_value',
                'item_from_value',
                'item_to_value',
            ] as $_attribute
        ) {
            if(
                !preg_match ("/^[0-9]+(\.[0-9]*)?$/", $data[$_attribute]) 
                && !((in_array($_attribute, ['price_from_value', 'weight_from_value'])) && ($data[$_attribute] == -1))
            ) {
                throw new Mage_Adminhtml_Exception("Line: {$lineNo}: {$_attribute} must be a positive decimal. Was {$data[$_attribute]}");
            }
        }

        $this->_getWriteAdapter()->insert($this->getMainTable(), $data);
        $this->_taskManager->updateStatus("Importing record $lineNo/{totalRecords}", -$lineNo);
        //@todo:
        //validate all lines (just validates that there are enough columns)
        //get array of region ids for region name
        //zip_to: change * to '', trim and uppercase.
        //package_id: change * to '';
        //split countries, regions, postcodes, cities, customer groups;
        //weight_from, price_from: change * or '' to -1;
        //weight_to, price_to, item_to: change * or '' to 10000000;
        //item_from: change * or '' to 0;
        //customer_group: change * to ''; trim;
        //country_code: convert to ID ?? Using the array of country codes to IDs
        //region_id convert code to ID.
        //zip: convert * or '' to ''; others trim, and convert - to a zip_to;
        //city: convert * or '' to ''; trim;
    }

    /**
     * CSV Import Routine
     * 
     * [dane][avid] Rewrote entire method
     * 
     * @param Varien_Object $object
     * @return unknown_type|void
     * @throws Exception
     */
    public function uploadAndImport(Varien_Object $object)
    {
        $file = [
            'tmp_name' =>$_FILES["groups"]["tmp_name"]["productmatrix"]["fields"]["import"]["value"],
            'name'  => $_FILES["groups"]["name"]["productmatrix"]["fields"]["import"]["value"],
        ];
        if (empty($file['name'])) {
            return;
        }
        $uploader = new Mage_Core_Model_File_Uploader($file);
        $uploader->setAllowedExtensions(['csv', 'gz']);
        $uploader->setAllowRenameFiles(true);
        $uploadDir = Mage::getBaseDir('var') . DS . 'import';
        $uploader->skipDbProcessing(true)->save($uploadDir);

        Mage::getModel('tradetested_core/task_manager')
            ->setTaskName('tradetested_shipping/task_import_productmatrix')
            ->setArguments([
                    'website_id' => $object->getScopeId(),
                    'csv_file'   => $uploader->getUploadedFileName(),
            ])
            ->schedule();
    }
    
    public function import($fileName, $websiteId)
    {
        $fileName = Mage::getBaseDir('var') . DS . 'import' . DS . $fileName;
        $connection = $this->_getWriteAdapter();
        $connection->beginTransaction();
        $condition = [$connection->quoteInto('website_id = ?', $websiteId)];
        $connection->delete($this->getMainTable(), $condition);
        $csv = Mage::getModel('tradetested_core/csv')->open($fileName, $this->_headers, 'r');
        $taskManager =Mage::getModel('tradetested_core/task_manager')
            ->setPrecision(0)
            ->setTotalRecords($csv->getTotalRows())
            ->updateStatus('Starting', 0);
        $this->_taskManager = $taskManager;
        $csv->withRows([$this, 'importRow'], ['website_id' => $websiteId]);
        $this->_taskManager->updateStatus('Committing updates', 1, true);
        $connection->commit();
        $this->_taskManager->updateStatus('Completed', 1, true);

        Mage::getSingleton('adminhtml/session')
            ->addSuccess(Mage::helper('adminhtml')->__($csv->getTotalRows().' rows have been imported.'));
    }

    private function getCustomerGroupCode() {

        if ($ruleData = Mage::registry('rule_data')) {
            $gId = $ruleData->getCustomerGroupId();
            return Mage::getModel('customer/group')->load($gId)->getCode();
        } else {
            return Mage::getModel('customer/group')->load(
                Mage::getSingleton('customer/session')->getCustomerGroupId())->getCode();
        }

    }

    private function mergeHighest($indResults,&$baseResults)
    {
        foreach ($baseResults as $key=>$result)
        {
            $found=false;
            foreach ($indResults as $indKey=>$data) {
                if ($result['delivery_type']==$data['delivery_type']) {
                    if (!$baseResults[$key]['override'] && ($data['price']>$baseResults[$key]['price'] || $data['override'])) { // if higher get higher
                        $baseResults[$key]['price']=$data['price'];
                        $baseResults[$key]['additional_price']+=$baseResults[$key]['multiprice']*$baseResults[$key]['qty'];
                        $baseResults[$key]['multiprice']=$data['multiprice'];
                        $baseResults[$key]['qty']=$data['qty'];
                    } else {
                        $baseResults[$key]['additional_price']+=$data['multiprice']*$data['qty'];
                    }
                    if ($baseResults[$key]['max_price']<$data['max_price']) {
                        $baseResults[$key]['max_price']=$data['max_price'];
                    }
                    $indResults[$indKey]['found']=true;
                    $found=true;
                    break;
                }
            }
            if (!$found && !$this->_starIncludeAll && !$baseResults[$key]['showall']) {
                // no match so remove
                if ($this->_debug) {
                    Mage::helper('wsalogger/log')->postInfo('productmatrix','Delivery Type - No Match Found - Removing',$baseResults[$key]['delivery_type']);
                }
                $baseResults[$key]="";
            }
        }
        // get show all items
        foreach ($indResults as $key=>$result)
        {
            if ( !$found && $result['showall']) {
                $baseResults[]=$result;
                $indResults[$key]['found']=true;
            }
        }

        if ($this->_starIncludeAll) {
            // check for missing
            foreach ($indResults as $data) {
                if (empty($data['found'])) {
                    $baseResults[]=$data;
                }
            }
        } else {
            // unset here so we dont upset the apple cart
            foreach ($baseResults as $key=>$result) {
                if (empty($baseResults[$key])) {
                    unset($baseResults[$key]);
                }
            }
        }
    }




    /**
     * Merge results together, ignore any not in base result set
     * @param $indResults
     * @param $baseResults - passed by reference
     * @return unknown_type
     */
    private function mergeResults($indResults,&$baseResults)
    {
        if ($this->_debug) {
            Mage::helper('wsalogger/log')->postDebug('productmatrix','merge result to add',$indResults);
            Mage::helper('wsalogger/log')->postDebug('productmatrix','merge base results',$baseResults);
        }
        foreach ($baseResults as $key=>$result)
        {
            $found=false;
            foreach ($indResults as $indKey=>$data) {
                if ($result['delivery_type']==$data['delivery_type']) {
                    $baseResults[$key]['found']=true;
                    $indResults[$indKey]['found']=true;
                    if ($baseResults[$key]['max_price']<$data['max_price']) {
                        $baseResults[$key]['max_price']=$data['max_price'];
                    }
                    if (preg_match('/c=/',$result['algorithm'])){
                        if(preg_match('/alt/',$result['algorithm'])){
                            $baseResults[$key]['method_name'] = $data['method_name'];
                        }
                    }
                    if ($baseResults[$key]['override']) {
                        $found=true;
                        break;
                    } else if ($data['override']) {
                        $baseResults[$key]['price']=$data['price'];
                        $baseResults[$key]['override']=true;
                        $baseResults[$key]['package_id']=$baseResults[$key]['package_id'].",".$data['package_id'];
                        $found=true;
                        break;
                    } else {
                        // add to existing
                        $baseResults[$key]['price']+=$data['price'];
                        $baseResults[$key]['package_id']=$baseResults[$key]['package_id'].",".$data['package_id'];
                        $found=true;
                        break;
                    }
                }
            }
            if (!$found && !$baseResults[$key]['showall']) {  // no match
                if ( !$this->_starIncludeAll) {
                    if ($this->_debug) {
                        Mage::helper('wsalogger/log')->postInfo('productmatrix','Delivery Type - No Match Found - Removing',$baseResults[$key]['delivery_type']);
                    }
                    $baseResults[$key]="";
                } else {
                    if ($result['package_id']!="" && count($this->_structuredItems)>1 && $indResults[0]['package_id']!="" && !$baseResults[$key]['showall'] ) {
                        if ($this->_debug) {
                            Mage::helper('wsalogger/log')->postInfo('productmatrix','Delivery Type - No Match Found - Removing',$baseResults[$key]['delivery_type']);
                        }
                        $baseResults[$key]="";
                    }
                }
            }
        }
        if ($this->_starIncludeAll) {
            // check for missing
            foreach ($indResults as $key=>$data) {
                if (empty($data['found']) && $data['package_id']=="") {
                    $indResults[$key]['found']=true;
                    $baseResults[]=$data;
                }
            }

            // this was changed to be ==package_id from != - reason unclear
            if ($this->_appendStarRates) {
                foreach ($baseResults as $key=>$result) {
                    if ($result!="" && !$baseResults[$key]['showall'] && $this->_hasEmptyPackages && $result['package_id']=="") {
                        if ($this->_debug) {
                            Mage::helper('wsalogger/log')->postInfo('productmatrix','Delivery Type - No Match Found - Removing',$baseResults[$key]['delivery_type']);
                        }
                        $baseResults[$key]="";
                    }
                }
            } else {
                foreach ($baseResults as $key=>$result) {
                    if ($result!=""  && !$baseResults[$key]['showall'] && (!isset($result['found']) || !$result['found'])) {
                        if ($this->_debug) {
                            Mage::helper('wsalogger/log')->postInfo('productmatrix','Delivery Type - No Match Found - Removing',$baseResults[$key]['delivery_type']);
                        }
                        $baseResults[$key]="";
                    }
                }
            }
        }
        foreach ($indResults as $key=>$result)
        {
            if ($result['showall'] && (!array_key_exists('found',$result) || !$result['found'])) {
                $baseResults[]=$result;
            }
        }
        // unset here so we dont upset the apple cart
        foreach ($baseResults as $key=>$result) {
            if (empty($baseResults[$key])) {
                unset($baseResults[$key]);
            }
        }

    }

    private function mergeOrdered($indResults,&$baseResults) {

        //this also supports sorting

        if (!array_key_exists('order',$indResults[0]) || !array_key_exists('order',$baseResults[0])) {
            return;
        }
        // just take the first
        $order=$indResults[0]['order'];
        if ($order<$baseResults[0]['order']) {
            reset($baseResults);
            $baseResults=$indResults;
        }


    }

    private function mergeLowest($indResults,&$baseResults)
    {

        foreach ($baseResults as $key=>$result)
        {
            $found=false;
            foreach ($indResults as $indKey=>$data) {
                if ($result['delivery_type']==$data['delivery_type']) {
                    // if lower get lower
                    if ($data['price']<$baseResults[$key]['price']) {
                        $baseResults[$key]['price']=$data['price'];
                    }
                    $indResults[$indKey]['found']=true;
                    $found=true;
                    break;
                }
            }
            if (!$found && !$this->_starIncludeAll) {
                // no match so remove
                $baseResults[$key]="";
            }
        }

        if ($this->_starIncludeAll) {
            // check for missing
            foreach ($indResults as $data) {
                if (empty($data['found'])) {
                    $baseResults[]=$data;
                }
            }
        } else {
            // unset here so we dont upset the apple cart
            foreach ($baseResults as $key=>$result) {
                if (empty($baseResults[$key])) {
                    unset($baseResults[$key]);
                }
            }
        }
    }

    protected function _getItemSelect($structuredItem)
    {
        $read = $this->_getReadAdapter();
        $request = $this->_request;
        $destCity =  // if POBOX search on CITY field
            (preg_match('/(^|(?:post(al)? *(?:office *)?|p[. ]*o\.? *))box *#? *\w+/ui', $request->getDestStreet())) ?
                'POBOX' :
                $request->getDestCity();
        $select = $read->select()->from(array('productmatrix'=>$this->_table),
            array(
                'pk'=>'pk',
                'price'=>'price',
                'delivery_type'=>'delivery_type',
                'package_id'=>'package_id',
                'weight_from_value'=>'weight_from_value',
                'dest_zip'=>'dest_zip',
                'dest_zip_to'=>'dest_zip_to',
                'item_from_value'=>'item_from_value',
                'algorithm'=>'algorithm',
                'notes'=>'notes',
                'cost'=>'cost',
                'shipping_time_estimate' => 'shipping_time_estimate',
                'area_name' => 'area_name',
                'cms_block' => 'cms_block',
            )
        );


        $select->where("dest_country_id = ? OR dest_country_id = '0'", $request->getDestCountryId())
            ->where("dest_region_id = ? OR dest_region_id = '0'", $request->getDestRegionId())
            ->where("(STRCMP(LOWER(dest_city),LOWER(?)) = 0 ) OR dest_city =''", $destCity)
            ->where("(STRCMP(LOWER(customer_group),LOWER(?) = 0)) or customer_group = ''")
            ->where('website_id=?', $request->getWebsiteId())
            ->where('weight_from_value<?', $structuredItem['weight'])
            ->where('weight_to_value>=?', $structuredItem['weight'])
            ->where('price_from_value<?', $structuredItem['price'])
            ->where('price_to_value>=?', $structuredItem['price'])
            ->where('item_from_value<?', $structuredItem['qty'])
            ->where('item_to_value>=?', $structuredItem['qty']);

        if (!$this->_request->getDestPostcode() && ($areaName = $this->_request->getEstimatorAreaName())) {
            //For shipping estimator where we are not using postcode
            $select->where('area_name = ?', $areaName)
                ->group('delivery_type');
        } elseif (isset($this->_longMatchPostcode) && $this->_longMatchPostcode) {
            $select->where("(STRCMP(LOWER(dest_zip),LOWER(?)) = 0) OR {$this->_shortMatchPostcode} OR dest_zip = ''", $this->_longMatchPostcode);
        } else {
            $select->where($this->_zipSearchString." OR dest_zip=''");
        }

        if(Mage::getConfig()->getNode('productmatrix/specialvars/udrop') == 1){
            $select->where("udropship_vendor IS NULL OR udropship_vendor in (0,?)", $this->_request->getVendorId());
        }

        if ($structuredItem['package_id']=='include_all' || $structuredItem['package_id']=='none') {
            $select->where('package_id=?','');
        } else {
            $select->where('package_id=?', $structuredItem['package_id']);
        }

        $select->order('notes ASC');
        $select->order('price ASC');
        $select->order('algorithm ASC');

        return $select;
    }

    protected function _getItemRatePrice($data, $structuredItem)
    {
        switch ($this->_request->getPMConditionName()) {
            case 'per_product':
            case 'per_product_bare':
                return $data['price']*$structuredItem['unique'];
            case 'per_item':
            case 'per_item_bare':
                if(!$data['set_cart']){
                    return $data['price']*$structuredItem['qty'];
                }
                break;
            case 'per_item_surcharge':
                if ($structuredItem['package_id']!='include_all') {
                    return $data['price']*$structuredItem['qty'];
                }
        }
        return $data['price'];
    }

    protected function _getItemData($structuredItem, &$errorText)
    {
        try {
            $row = $this->_getReadAdapter()->fetchAll($this->_getItemSelect($structuredItem));
        } catch (Exception $e) {
            Mage::helper('wsalogger/log')->postWarning('productmatrix','SQL Exception',$e->getMessage());
        }
        //@todo: work out wth is up with this 'filter canada' stuff and add it here with some style.

        if (!empty($row)) {
            $row = $this->_filterCanadianPostcode($row);
            if ($this->_debug) {
                Mage::helper('wsalogger/log')->postDebug('productmatrix','SQL Result',$row);
            }
            $newdata=array();
            $priorityData=array();
            foreach ($row as $data) {
                if ($data['price']==-1) {
                    $this->_exclusionList[] = $this->_addExclusion($data,$structuredItem,$errorText);
                    continue;
                }

                $this->_initData($data);
                $priorityTypes = array();
                if (!$this->_populateAlgorithm($data, $structuredItem, $this->_request->getPMConditionName(), $priorityTypes)) {
                    continue;
                }

                $data['price'] = $this->_getItemRatePrice($data, $structuredItem);


                if (count($priorityTypes)>0) {
                    $data['priority']=0;
                    $priorityNum=1;
                    $this->_prioritySet=true;
                    foreach ($priorityTypes as $priorityType) {
                        $copyData = $data;
                        $copyData['priority']=$priorityNum;
                        $priorityNum++;
                        $copyData['delivery_type']=$priorityType;
                        $copyData['method_name']=$priorityType;
                        $priorityData[]=$copyData;
                    }

                }

                $newdata[]=$data;
            }
            if (count($priorityData)>0) {
                $newdata = array_merge($newdata, $priorityData);
            }
            if (!empty($newdata)) {
                return $newdata;
            } else {
                return;
            }
        }
    }

    protected function _filterCanadianPostcode($outerRow)
    {
        if ($this->_postcodeFilterCanada && 'CA' == $this->_request->getDestCountryId())  {
            $row = array();
            $zipBreakdown = strtoupper(substr(trim($this->_request->getDestPostcode()),0,3));
            foreach ($outerRow as $data) {
                $zipFromArry = $data['dest_zip'];
                $zipToArry = $data['dest_zip_to'];
                if (strlen($zipFromArry)!=3 || strlen($zipToArry)!=3) {
                    $row[] = $data;
                    continue;
                }
                if ($zipFromArry[1]==$zipBreakdown[1] && strcmp($zipBreakdown[2],$zipFromArry[2])<0) {
                    // matches on first
                    continue;
                }
                if ($zipToArry[1]==$zipBreakdown[1]) {
                    if (strcmp($zipToArry[2],$zipBreakdown[2])<0) {
                        continue;
                    }
                }
                $row[] = $data;
            }
        } else {
            return $outerRow;
        }
    }

    protected function _processTwoPhasePostcode($customerPostcode)
    {
        $readAdaptor = $this->_getReadAdapter();
        $longPostcode=substr_replace($customerPostcode,"",-3);
        $this->_longMatchPostcode = $readAdaptor->quoteInto('?', trim($longPostcode));
        $this->_shortMatchPostcode = $readAdaptor->quoteInto(
            "(STRCMP(LOWER(dest_zip),LOWER(?)) = 0)",
            preg_replace('/\d/','', $this->_longMatchPostcode)
        );
    }

    protected function _processCanadaPostcode($customerPostcode)
    {
        $readAdaptor = $this->_getReadAdapter();
        $shortPart = substr($customerPostcode,0,3);
        if (strlen($shortPart) < 3 || !is_numeric($shortPart[1]) || !ctype_alpha($shortPart[2])) {
            return $this->_postcodeSearchString = $readAdaptor->quoteInto("? LIKE dest_zip )", $customerPostcode);
        } else {
            $zipFromRegExp='^'.$shortPart[0].'[0-'.$shortPart[1].'].';
            $zipToRegExp='^'.$shortPart[0].'['.$shortPart[1].'-9].';
            $this->_shortMatchPostcode = $readAdaptor->quoteInto("(dest_zip REGEXP ?", $zipFromRegExp).$readAdaptor->quoteInto(" AND dest_zip_to REGEXP ? )",$zipToRegExp );
            $this->_longMatchPostcode = $readAdaptor->quoteInto('?', $customerPostcode);
            $this->_postcodeFilterCanada = true;
        }
    }

    //Dane modified to remove all the ANDs, which are craaaazy
	protected function _processPostcode()
    {
        $request = $this->_request;
        $zipcodeMaxLength = Mage::getStoreConfig('carriers/productmatrix/zipcode_max_length');
        if(empty($zipcodeMaxLength)) {
            if ( Mage_Usa_Model_Shipping_Carrier_Abstract::USA_COUNTRY_ID == $request->getDestCountryId() ||  'BR' == $request->getDestCountryId()) {
                $splitPostcode = explode('-',$request->getDestPostcode());
                $postcode = $splitPostcode[0];
            } else {
                $postcode = $request->getDestPostcode();
            }
        } else {
            $postcode = substr($request->getDestPostcode(), 0, $zipcodeMaxLength);
        }

        $readAdaptor = $this->_getReadAdapter();
		$customerPostcode = trim($postcode);
        $this->_postcodeFilterCanada = false;
        $this->_postcodeSearchString = $this->_shortMatchPostcode = $this->_longMatchPostcode = '';

        switch (Mage::getStoreConfig("carriers/productmatrix/postcode_filter")) {
            case 'numeric':
                return is_numeric($customerPostcode) ?
                    $this->_zipSearchString = $readAdaptor->quoteInto("(? BETWEEN dest_zip AND dest_zip_to)", $customerPostcode, 'INTEGER') :
                    $this->_processTwoPhasePostcode($customerPostcode);
            case 'uk':
                if (strlen($customerPostcode) > 4) {
                    return $this->_processTwoPhasePostcode($customerPostcode);
                }
                break;
            case 'both':
                return is_numeric($customerPostcode) ?
                    $this->_zipSearchString = $readAdaptor->quoteInto("(? BETWEEN dest_zip AND dest_zip_to)", $customerPostcode, 'INTEGER') :
                    $this->_processTwoPhasePostcode($customerPostcode);
            case 'canada':
                return $this->_processCanadaPostcode($customerPostcode);
            case 'can_numeric':
                return is_numeric($customerPostcode) ?
                    $this->_zipSearchString = $readAdaptor->quoteInto("(? BETWEEN dest_zip AND dest_zip_to)", $customerPostcode, 'INTEGER') :
                    $this->_processCanadaPostcode($customerPostcode);
        }
        $this->_zipSearchString = $readAdaptor->quoteInto("? LIKE dest_zip", $customerPostcode);
	}

    protected function _populateStructuredItems()
    {
        $items = $this->_request->getAllItems();
        $conditionName = $this->_request->getPMConditionName();
        $specialPrice = 0;
        $specialWeight = 0;
        $this->_structuredItems=array();
        $this->_useParent = Mage::getStoreConfig("carriers/productmatrix/parent_group");
        if ($this->_debug) {
            Mage::helper('wsalogger/log')->postDebug('productmatrix','Settings','Use Parent:'.$this->_useParent);
        }
        switch ($this->_useParent) {
            case "none":
                $this->_useBundleParent=false;
                $this->_useConfigurableParent=false;
                break;
            case "both":
                $this->_useBundleParent=true;
                $this->_useConfigurableParent=true;
                break;
            case "bundle":
                $this->_useBundleParent=true;
                $this->_useConfigurableParent=false;
                break;
            case "configurable":
                $this->_useBundleParent=false;
                $this->_useConfigurableParent=true;
                break;
            default:
                $this->_useBundleParent=false;
                $this->_useConfigurableParent=false;
                break;

        }

        $filterPrice = in_array('filter_subtotal',$this->_options);
        $useDiscountValue = in_array('use_discounted',$this->_options);
        $splitCustom = in_array('split_custom',$this->_options);
        $useBase = in_array('use_base',$this->_options);
        $useTax = in_array('use_tax',$this->_options);
        $runningCartPrice = 0;
        $includeVirtual = !in_array('remove_virtual',$this->_options);

        $version = Mage::helper('wsacommon')->getVersion();
        $freeship=$this->_request->getIgnoreFreeItems();
        if  ($version == 1.6 || $version == 1.7 || $version == 1.8) {
            // freemethodweight not supported. Do what we did before
            $freeship=true;
        }
        foreach($items as $item) {

            $weight=0;
            $qty=0;
            $price=0;
            $temp='';

            if($item->getProduct()->isVirtual()){
                if (!Mage::helper('wsacommon/shipping')->getVirtualItemTotals($item, $weight,$qty,$price,$this->_useBundleParent,
                    $freeship,$temp,$useDiscountValue, false, $useBase, $useTax, $includeVirtual)) {
                    continue;
                }
            }
            else {
                if (!Mage::helper('wsacommon/shipping')->getItemTotals($item, $weight,$qty,$price,$this->_useBundleParent,
                    $freeship,$temp,$useDiscountValue, false, $useBase, $useTax)) {
                    continue;
                }
            }

            $runningCartPrice += $price;

            if ($item->getParentItem()!=null &&
                $this->_useBundleParent &&
                $item->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE ) {
                // must be a bundle/configurable
                $product = $item->getParentItem()->getProduct();
            } else if ( $item->getParentItem()!=null &&
                $this->_useConfigurableParent &&
                $item->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE ) {
                $product = $item->getParentItem()->getProduct();
            } else if ( $item->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE &&
                !$this->_useConfigurableParent ) {
                if ($item->getHasChildren()) {
                    foreach ($item->getChildren() as $child) {
                        $product=$child->getProduct();
                        break;
                    }
                }
            } else {
                $product = $item->getProduct();
            }



            // if find a surcharge check to see if it is on the item or the order
            // if on the order then add to the surcharge_order_price if > than previous
            // if on the item then multiple by qty and add to the surcharge_price

            if (!is_object($product)) {
                $this->_structuredItems[] = "";
                Mage::helper('wsalogger/log')->postCritical('productmatrix','Fatal Error','Item/Product is Malformed');
                return;
            }

            $shipQty=$product->getData('shipping_qty');
            $freightQtyThreshold=$product->getData('freight_qty_threshold');

            if ($shipQty=="" || !is_numeric($shipQty)) {
                $shipQty=1;
            }

            if (in_array('group_text',$this->_options)) {
                $packageId = $product->getData('package_id');
            } else {
                $packageId = $product->getAttributeText('package_id');
            }

            if (empty($packageId) && is_numeric($freightQtyThreshold) && $qty>=$freightQtyThreshold) {
                $packageId = 'SHIP_FREIGHT_AUTO';
            }

            if (empty($packageId)) { $packageId='none'; $this->_hasEmptyPackages=true; }

            if ($splitCustom) {
                $options = Mage::helper('catalog/product_configuration')->getCustomOptions($item);
                if (!empty($options)  && is_array($options[0])) {
                    $packageId .= '-OPTIONS';
                }
            }

            $found=false;


            if ($conditionName=='per_item_bare' || $conditionName=='per_item_surcharge' ||  $conditionName=='per_product_bare') {
                $prodArray=array( 'package_id'  => $packageId,
                    'qty' 				=> $qty,
                    'weight'				=> $weight/$qty,
                    'price'				=> $price/$qty,
                    'unique'		 		=> 1);
                $this->_structuredItems[]=$prodArray;

            } else {

                foreach($this->_structuredItems as $key=>$structuredItem) {
                    if ($structuredItem['package_id']==$packageId) {
                        // have already got this package id
                        $this->_structuredItems[$key]['qty']=$this->_structuredItems[$key]['qty']+$qty*$shipQty;
                        $this->_structuredItems[$key]['weight']=$this->_structuredItems[$key]['weight']+ $weight;
                        $this->_structuredItems[$key]['price']=$this->_structuredItems[$key]['price']+ $price;
                        $this->_structuredItems[$key]['unique']+=1;
                        $found=true;
                        break;
                    }
                }

                if (!$found){
                    $prodArray=array( 'package_id'  => $packageId,
                        'qty' 				=> $qty*$shipQty,
                        'weight'				=> $weight,
                        'price'				=> $price,
                        'unique'				=> 1);
                    $this->_structuredItems[]=$prodArray;

                }
            }
            // also add to include_all package Id
            if ($this->_starIncludeAll ) {
                if ($packageId=="SPECIAL_FREE") {
                    $specialPrice += $price;
                    $specialWeight += $weight;
                } else {
                    $found=false;
                    if ($useDiscountValue) {
                        $groupPrice = $runningCartPrice;
                    } else {
                        $groupPrice = $price;
                    }
                    foreach($this->_structuredItems as $key=>$structuredItem) {
                        if ($structuredItem['package_id']=='include_all') {
                            $this->_structuredItems[$key]['qty']=$this->_structuredItems[$key]['qty']+$qty*$shipQty;
                            $this->_structuredItems[$key]['weight']=$this->_structuredItems[$key]['weight']+ $weight;
                            if (!$useDiscountValue) {
                                $this->_structuredItems[$key]['price']=$this->_structuredItems[$key]['price']+ $price;
                            }
                            $this->_structuredItems[$key]['unique']+=1;
                            $found=true;
                            break;
                        }
                    }
                    if (!$found) {
                        $prodArray=array( 'package_id'  => 'include_all',
                            'qty' 				=> $qty*$shipQty,
                            'weight'				=> $weight,
                            'price'				=> $groupPrice,
                            'unique'				=> 1);
                        $this->_structuredItems[]=$prodArray;
                    }
                }
            }
        }

        if ($filterPrice) {
            if(Mage::helper('wsacommon')->isModuleEnabled('Webshopapps_Dropship','carriers/dropship/active')) {
                if(Mage::getStoreConfig('carriers/dropship/use_cart_price')) {
                    $runningCartPrice = $this->_request->getPackageValue();
                }
            }
            foreach($this->_structuredItems as $key=>$structuredItem) {
                $this->_structuredItems[$key]['price'] = $runningCartPrice - $specialPrice;
                $this->_structuredItems[$key]['weight'] = $this->_request->getPackageWeight() - $specialWeight;
            }
        }
        if ($this->_debug) {
            Mage::helper('wsalogger/log')->postDebug('productmatrix','Structured Items',$this->_structuredItems);
        }
    }
}