<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/03/2016
 * Time: 14:05
 */
class TradeTested_PaymentExpress_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Currency codes supported by DPS
     *
     * @var array
     * @see http://www.paymentexpress.com/Knowledge_Base/Merchant_Info/Multi_Currency
     */
    protected $_supportedCurrencies = [
        'AUD' => 'Australian Dollar',
        'BRL' => 'Brazil Real',
        'BND' => 'Brunei Dollar',
        'CAD' => 'Canadian Dollar',
        'CNY' => 'Chinese Yuan Renminbi',
        'CZK' => 'Czech Koruna',
        'DKK' => 'Danish Kroner',
        'EGP' => 'Egyptian Pound',
        'EUR' => 'Euro',
        'FJD' => 'Fiji Dollar',
        'HKD' => 'Hong Kong Dollar',
        'HUF' => 'Hungarian Forint',
        'INR' => 'Indian Rupee',
        'IDR' => 'Indonesia Rupiah',
        'JPY' => 'Japanese Yen',
        'KRW' => 'Korean Won',
        'MOP' => 'Macau Pataca',
        'MYR' => 'Malaysian Ringgit',
        'MUR' => 'Mauritius Rupee',
        'ANG' => 'Netherlands Guilder',
        'TWD' => 'New Taiwan Dollar',
        'NOK' => 'Norwegian Kronor',
        'NZD' => 'New Zealand Dollar',
        'PGK' => 'Papua New Guinea Kina',
        'PHP' => 'Philippine Peso',
        'PLN' => 'Polish Zloty',
        'GBP' => 'Pound Sterling',
        'PKR' => 'Pakistan Rupee',
        'WST' => 'Samoan Tala',
        'SAR' => 'Saudi Riyal',
        'SBD' => 'Solomon Islands Dollar',
        'LKR' => 'Sri Lankan Rupee',
        'SGD' => 'Singapore Dollar',
        'ZAR' => 'South African Rand',
        'SEK' => 'Swedish Kronor',
        'CHF' => 'Swiss Franc',
        'THB' => 'Thai Baht',
        'TOP' => 'Tongan Pa\'anga',
        'AED' => 'UAE Dirham',
        'USD' => 'United States Dollar',
        'VUV' => 'Vanuatu Vatu'
    ];

    /**
     * Does Payment Express support Currency?
     *
     * @param $currencyCode
     * @return bool
     */
    public function canUseCurrency($currencyCode)
    {
        return in_array($currencyCode, array_keys($this->_supportedCurrencies));
    }

    /**
     * Should we offer to save the billing token when paying with new card?
     *
     * @return bool
     */
    public function shouldOfferSaveBillingToken()
    {
        $pxPostMethod = Mage::getModel('tradetested_payment_express/method_pxpost');
        $tokenMethod = Mage::getModel('tradetested_payment_express/method_token');
        $storeId = Mage::app()->getStore()->getId();

        return (
            $pxPostMethod->getConfigData('active', $storeId)
            && $pxPostMethod->getConfigData('frontend_checkout', $storeId)
            && $pxPostMethod->getConfigData('postusername', $storeId)
            && $pxPostMethod->getConfigData('postpassword', $storeId)
            && $tokenMethod->getConfigData('active', $storeId)
        );
    }

    /**
     * Get CSS class for a given CC Type
     *
     * e.g. visa, mastercard, other
     *
     * @param TradeTested_PaymentExpress_Model_Rewrite_Sales_Billing_Agreement $agreement
     * @return string
     */
    public function getCcTypeClass(TradeTested_PaymentExpress_Model_Rewrite_Sales_Billing_Agreement $agreement)
    {
        return strtolower(preg_replace('/\s+/', '_', $agreement->getAdditionalData('cc_type')));
    }

    /**
     * Extract Additional Data from Payment Instance
     *
     * @param Varien_Object $info
     * @param null $key
     * @return mixed|string
     */
    public function getAdditionalData(Varien_Object $info, $key = null)
    {
        if ($info->getAdditionalData() && strpos($info->getAdditionalData(), 'Array') !== 0) {
            $data = unserialize($info->getAdditionalData());
        }
        $data = is_null($key)
            ? ($data ?? [])
            : array_merge(($data ?? []), ($info->getData('additional_information') ?? []));
        $obj = new Varien_Object($data);

        return $obj->getData($key);
    }

    public function getFraudAttributes()
    {
        return [
            'risk_score'                                        => 'Risk Score',
            'credit_card/brand'                                 => 'Card Brand',
            'credit_card/country'                               => 'Card Country',
            'credit_card/issuer/name'                           => 'Card Issuer',
            'credit_card/issuer/phone_number'                   => 'Card Issuer Ph.',
            'credit_card/is_issued_in_billing_address_countrey' => 'Card issued in Billing Country?',
            'credit_card/is_prepaid'                            => 'Card is Prepaid?',
            'billing_address/distance_to_ip_location'           => 'Billing Address Distance to IP',
            'billing_address/is_in_ip_country'                  => 'Billing is in IP Country?',
            'billing_address/is_postal_in_city'                 => 'Billing postcode in city?',
            'billing_address/latitude'                          => 'Billing latitude',
            'billing_address/longitude'                         => 'Billing longitude',
            'shipping_address/distance_to_billing_address'      => 'Shipping Distance to Billing',
            'shipping_address/distance_to_ip_location'          => 'Shipping Distance to IP',
            'shipping_address/is_high_risk'                     => 'Shipping is high risk?',
            'shipping_address/is_in_ip_country'                 => 'Shipping in IP Country?',
            'shipping_address/is_postal_in_city'                => 'Shipping Postal in City?',
            'shipping_address/latitude'                         => 'Shipping Latitude',
            'shipping_address/longitude'                        => 'Shipping Longitude',
            'ip_address/city/name'                              => 'IP City',
            'ip_address/city/confidence'                        => 'IP City Confidence',
            'ip_address/country/name'                           => 'IP Country',
            'ip_address/country/confidence'                     => 'IP Country Confidence',
            'ip_address/country/is_high_risk'                   => 'IP Country high risk?',
            'ip_address/traits/domain'                          => 'IP Domain',
            'ip_address/traits/ip_address'                      => 'IP Address',
            'ip_address/traits/is_anonymous_proxy'              => 'Using anon proxy?',
            'ip_address/traits/is_satellite_provider'           => 'Using satellite?',
            'ip_address/traits/isp'                             => 'ISP',
            'ip_address/traits/organization'                    => 'IP Organization',
            'ip_address/traits/user_type'                       => 'IP User Type',
            'email/is_free'                                     => 'Email is free?',
            'email/is_high_risk'                                => 'Email is high risk?',
        ];
    }
}