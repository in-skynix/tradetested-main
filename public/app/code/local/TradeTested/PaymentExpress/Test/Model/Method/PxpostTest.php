<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 25/02/2016
 * Time: 14:28
 */
class TradeTested_PaymentExpress_Test_Model_Method_Pxpost extends TradeTested_PaymentExpress_Test_Case
{
    public function testPaymentMethodCode()
    {
        $methodInstance = Mage::getModel('tradetested_payment_express/method_pxpost');
        $this->assertEquals('tradetested_px_post', $methodInstance->getCode());
    }

    public function testPaymentActionIsCapture()
    {
        $methodInstance = Mage::getModel('tradetested_payment_express/method_pxpost');
        $this->assertEquals('authorize_capture', $methodInstance->getConfigPaymentAction());
    }

    /**
     * @loadFixture default
     */
    public function testValidatesCardAndExpiryBeforeSending()
    {
        $this->setConfig();
        $this->setCurrentStore(1);
        $this->_stubSessions();

        $clientMock = $this->getModelMockBuilder('tradetested_payment_express/client')
            ->setMethods(['sendRequest'])
            ->getMock();
        $clientMock->expects($this->never())->method('sendRequest');
        $this->replaceByMock('model', 'tradetested_payment_express/client', $clientMock);

        $quote = Mage::getModel('sales/quote')->load(1);
        $quote->collectTotals();
        $this->setExpectedException('Mage_Core_Exception');
        $this->_submitQuote($quote, ['cc_exp_year' => '2012']);
        $this->_submitQuote($quote, ['cc_number' => '4111123412341234']);
    }

    /**
     * @loadFixture default
     */
    public function testCaptureActionIsCalled()
    {
        $this->setConfig();
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $methodMock = $this->getModelMock('tradetested_payment_express/method_pxpost', ['capture']);
        $methodMock->expects($this->once())
            ->method('capture')
            ->will($this->returnValue($methodMock));
        $this->replaceByMock('model', 'tradetested_payment_express/method_pxpost', $methodMock);

        $quote = Mage::getModel('sales/quote')->load(1)->collectTotals();
        $this->_submitQuote($quote);
    }

    /**
     * @loadFixture default
     */
    public function testCaptureSavesResponse()
    {
        $this->setConfig();
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $quote = Mage::getModel('sales/quote')->load(1)->collectTotals();
        $order = $this->_submitQuote($quote);
        $order = Mage::getModel('sales/order')->load($order->getId());
        $payment = $order->getPayment();
        $this->assertEquals('P000000031986f8b2', $payment->getLastTransId());
        $this->assertEquals('P000000031986f8b2', $payment->getCcTransId());
    }

    /**
     * @loadFixture default
     */
    public function testCaptureSavesExpiryDateToPayment()
    {
        $this->setConfig();
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $quote = Mage::getModel('sales/quote')->load(1)->collectTotals();
        $order = $this->_submitQuote($quote);
        $order = Mage::getModel('sales/order')->load($order->getId());
        $payment = $order->getPayment();
        $this->assertEquals(date('Y', strtotime('+1 year')), $payment->getCcExpYear());
        $this->assertEquals(date('m', strtotime('+1 year')), $payment->getCcExpMonth());
    }

    /**
     * @loadFixture default
     */
    public function testCaptureSavesTxnIdToCaptureTransaction()
    {
        $this->setConfig();
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $quote = Mage::getModel('sales/quote')->load(1)->collectTotals();
        $order = $this->_submitQuote($quote);
        $order = Mage::getModel('sales/order')->load($order->getId());
        $transaction = $order->getPayment()
            ->lookupTransaction(false, Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE);
        $this->assertEquals('P000000031986f8b2', $transaction->getTxnId());
        $this->assertEquals(1, $transaction->getIsClosed());
    }

    /**
     * @loadFixture default
     */
    public function testCaptureSetsAmountOrdered()
    {
        $this->setConfig();
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $quote = Mage::getModel('sales/quote')->load(1);
        $quote->collectTotals();
        $order = $this->_submitQuote($quote);
        $order = Mage::getModel('sales/order')->load($order->getId());
        $payment = $order->getPayment();
        $this->assertEquals('20.00', $payment->getAmountOrdered());
    }

    /**
     * @loadFixture default
     */
    public function testCaptureSetsOrderStatusToProcessing()
    {
        $this->setConfig();
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $quote = Mage::getModel('sales/quote')->load(1);
        $quote->collectTotals();
        $order = $this->_submitQuote($quote);
        $order = Mage::getModel('sales/order')->load($order->getId());
        $this->assertEquals('processing', $order->getStatus());
    }

    /**
     * @loadFixture default
     */
    public function testCaptureSavesBillingAgreement()
    {
        $this->setConfig();
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $this->_createCustomerSession(13, 1);
        $agreementCount = Mage::getResourceModel('sales/billing_agreement_collection')
            ->addFieldToFilter('customer_id', 13)->count();
        $quote = Mage::getModel('sales/quote')->load(1);
        $quote->collectTotals();
        $order = $this->_submitQuote($quote, ['cc_save' => '1']);
        $order = Mage::getModel('sales/order')->load($order->getId());
        $this->assertEquals('processing', $order->getStatus());
        $this->assertEquals(
            $agreementCount+1, 
            Mage::getResourceModel('sales/billing_agreement_collection')->addFieldToFilter('customer_id', 13)->count()
        );
        /** @var TradeTested_Payment_Model_Rewrite_Sales_Billing_Agreement $agreement */
        $agreement = Mage::getResourceModel('sales/billing_agreement_collection')
            ->addFieldToFilter('customer_id', 13)
            ->setOrder('created_at', 'desc')
            ->getFirstItem();
        $expectedData = [
            'customer_id'     => '13',
            'method_code'     => 'tradetested_px_token',
            'reference_id'    => 'billing_id_123',
            'status'          => 'active',
            'agreement_label' => '•••• •••• •••• 1111',
            'is_default'      => '1',
            'expires_at'      => date('Y-m-d', strtotime('first day of next month +1 year')),
            'store_id'        => '1'
        ];
        $expectedAddl = [
            'expiry_label' => date('m', strtotime('+1 year')) . '/' . date('y', strtotime('+1 year')),
            'cc_type'      => 'Visa'
        ];
        $this->assertEquals($expectedData, array_intersect_key($agreement->getData(), $expectedData));
        $this->assertEquals($expectedAddl, array_intersect_key($agreement->getAdditionalData(), $expectedAddl));
    }

    /**
     * @loadFixture default
     */
    public function testCaptureNotSaveBillingAgreement()
    {
        $this->setConfig();
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $this->_createCustomerSession(13, 1);
        $agreementCount = Mage::getResourceModel('sales/billing_agreement_collection')
            ->addFieldToFilter('customer_id', 13)->count();
        $quote = Mage::getModel('sales/quote')->load(1);
        $quote->collectTotals();
        $order = $this->_submitQuote($quote, ['cc_save' => '0']);
        $order = Mage::getModel('sales/order')->load($order->getId());
        $this->assertEquals('processing', $order->getStatus());
        $this->assertEquals(
            $agreementCount,
            Mage::getResourceModel('sales/billing_agreement_collection')->addFieldToFilter('customer_id', 13)->count()
        );
    }


    /**
     * @loadFixture default
     */
    public function testAuthorizeActionIsCalled()
    {
        $this->setConfig(['payment/tradetested_px_post/payment_action' => 'authorize']);
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $methodMock = $this->getModelMock('tradetested_payment_express/method_pxpost', ['authorize']);
        $methodMock->expects($this->once())
            ->method('authorize')
            ->will($this->returnValue($methodMock));
        $this->replaceByMock('model', 'tradetested_payment_express/method_pxpost', $methodMock);

        $quote = Mage::getModel('sales/quote')->load(1)->collectTotals();
        $this->_submitQuote($quote);
    }

    /**
     * @loadFixture default
     */
    public function testAuthorizeSavesResponse()
    {
        $this->setConfig(['payment/tradetested_px_post/payment_action' => 'authorize']);
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $quote = Mage::getModel('sales/quote')->load(1)->collectTotals();
        $order = $this->_submitQuote($quote);
        $order = Mage::getModel('sales/order')->load($order->getId());
        $payment = $order->getPayment();
        $this->assertEquals('A000000031986f8b2', $payment->getLastTransId());
        $this->assertEquals('A000000031986f8b2', $payment->getCcTransId());
    }

    /**
     * @loadFixture default
     */
    public function testAuthorizeSavesExpiryDateToPayment()
    {
        $this->setConfig(['payment/tradetested_px_post/payment_action' => 'authorize']);
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $quote = Mage::getModel('sales/quote')->load(1)->collectTotals();
        $order = $this->_submitQuote($quote);
        $order = Mage::getModel('sales/order')->load($order->getId());
        $payment = $order->getPayment();
        $this->assertEquals(date('Y', strtotime('+1 year')), $payment->getCcExpYear());
        $this->assertEquals(date('m', strtotime('+1 year')), $payment->getCcExpMonth());
    }

    /**
     * @loadFixture default
     */
    public function testAuthorizeSavesTxnIdToAuthorizeTransaction()
    {
        $this->setConfig(['payment/tradetested_px_post/payment_action' => 'authorize']);
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $quote = Mage::getModel('sales/quote')->load(1)->collectTotals();
        $order = $this->_submitQuote($quote);
        $order = Mage::getModel('sales/order')->load($order->getId());
        $transaction = $order->getPayment()
            ->lookupTransaction(false, Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH);
        $this->assertEquals('A000000031986f8b2', $transaction->getTxnId());
        $this->assertEquals(0, $transaction->getIsClosed());
    }

    /**
     * @loadFixture default
     */
    public function testAuthorizeSetsAmountOrdered()
    {
        $this->setConfig(['payment/tradetested_px_post/payment_action' => 'authorize']);
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $quote = Mage::getModel('sales/quote')->load(1);
        $quote->collectTotals();
        $order = $this->_submitQuote($quote);
        $order = Mage::getModel('sales/order')->load($order->getId());
        $payment = $order->getPayment();
        $this->assertEquals('20.00', $payment->getAmountOrdered());
    }

    /**
     * @loadFixture default
     */
    public function testAuthorizeSavesBillingAgreement()
    {
        $this->setConfig(['payment/tradetested_px_post/payment_action' => 'authorize']);
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $this->_createCustomerSession(13, 1);
        $agreementCount = Mage::getResourceModel('sales/billing_agreement_collection')
            ->addFieldToFilter('customer_id', 13)->count();
        $quote = Mage::getModel('sales/quote')->load(1);
        $quote->collectTotals();
        $order = $this->_submitQuote($quote, ['cc_save' => '1']);
        $order = Mage::getModel('sales/order')->load($order->getId());
        $this->assertEquals(
            $agreementCount+1,
            Mage::getResourceModel('sales/billing_agreement_collection')->addFieldToFilter('customer_id', 13)->count()
        );
        /** @var TradeTested_Payment_Model_Rewrite_Sales_Billing_Agreement $agreement */
        $agreement = Mage::getResourceModel('sales/billing_agreement_collection')
            ->addFieldToFilter('customer_id', 13)
            ->setOrder('created_at', 'desc')
            ->getFirstItem();
        $expectedData = [
            'customer_id'     => '13',
            'method_code'     => 'tradetested_px_token',
            'reference_id'    => 'billing_id_123',
            'status'          => 'active',
            'agreement_label' => '•••• •••• •••• 1111',
            'is_default'      => '1',
            'expires_at'      => date('Y-m-d', strtotime('first day of next month +1 year')),
            'store_id'        => '1'
        ];
        $expectedAddl = [
            'expiry_label' => date('m', strtotime('+1 year')) . '/' . date('y', strtotime('+1 year')),
            'cc_type'      => 'Visa'
        ];
        $this->assertEquals($expectedData, array_intersect_key($agreement->getData(), $expectedData));
        $this->assertEquals($expectedAddl, array_intersect_key($agreement->getAdditionalData(), $expectedAddl));
    }

    /**
     * @loadFixture default
     */
    public function testAuthorizeNotSaveBillingAgreement()
    {
        $this->setConfig(['payment/tradetested_px_post/payment_action' => 'authorize']);
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $this->_createCustomerSession(13, 1);
        $agreementCount = Mage::getResourceModel('sales/billing_agreement_collection')
            ->addFieldToFilter('customer_id', 13)->count();
        $quote = Mage::getModel('sales/quote')->load(1);
        $quote->collectTotals();
        $order = $this->_submitQuote($quote, ['cc_save' => '0']);
        $order = Mage::getModel('sales/order')->load($order->getId());
        $this->assertEquals('processing', $order->getStatus());
        $this->assertEquals(
            $agreementCount,
            Mage::getResourceModel('sales/billing_agreement_collection')->addFieldToFilter('customer_id', 13)->count()
        );
    }

    /**
     * @loadFixture default
     */
    public function testAuthorizeAndCaptureSavesAllDataAndTransactions()
    {
        $this->setConfig(['payment/tradetested_px_post/payment_action' => 'authorize']);
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $quote = Mage::getModel('sales/quote')->load(1);
        $quote->collectTotals();
        $order = $this->_submitQuote($quote);
        $order = Mage::getModel('sales/order')->load($order->getId());
        $payment = $order->getPayment();
        $this->assertEquals('A000000031986f8b2', $payment->getLastTransId());
        $this->assertEquals('A000000031986f8b2', $payment->getCcTransId());
        $this->assertEquals(date('Y', strtotime('+1 year')), $payment->getCcExpYear());
        $this->assertEquals(date('m', strtotime('+1 year')), $payment->getCcExpMonth());
        $transaction = $order->getPayment()
            ->lookupTransaction(false, Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH);
        $this->assertEquals(0, $transaction->getIsClosed());
        $this->assertEquals('A000000031986f8b2', $transaction->getTxnId());
        $this->assertEquals('20.00', $payment->getAmountOrdered());

        //Reload order so payment has parent transaction ID.
        $order = Mage::getModel('sales/order')->load($order->getId());
        $invoice = $order->getInvoiceCollection()->getFirstItem();
        $this->assertEquals('Unknown State', $invoice->getStateName());
        //Capture payment for the same instance of order that we just (re)loaded.
        $payment = $order->getPayment();
        $payment->capture(null);
        $order->setDataChanges(true)->save();
        $payment->save();
        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->load($order->getId());
        /** @var Mage_Sales_Model_Order_Payment $payment */

        $payment = $order->getPayment();
        $this->assertEquals('C000000031986f8b2', $payment->getLastTransId());
        $this->assertEquals('C000000031986f8b2', $payment->getCcTransId());
        $this->assertEquals(date('Y', strtotime('+1 year')), $payment->getCcExpYear());
        $this->assertEquals(date('m', strtotime('+1 year')), $payment->getCcExpMonth());
        $transaction = $payment->lookupTransaction(false, Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE);
        //The transaction actually isn't saved.
        $this->assertEquals('C000000031986f8b2', $transaction->getTxnId());
        $authTransaction = $payment->getAuthorizationTransaction();
        $this->assertNotEmpty($authTransaction->getId());
        $this->assertEquals($authTransaction->getTxnId(), $transaction->getParentTxnId());
        $this->assertEquals(1, $transaction->getIsClosed());
        $this->assertEquals(1, $authTransaction->getIsClosed());
        $this->assertEquals('20.00', $payment->getAmountOrdered());
        $invoice = $order->getInvoiceCollection()->getFirstItem();
        $this->assertEquals('Paid', $invoice->getStateName());
    }

    /**
     * @loadFixture default
     * This process is used in the add a new card controller.
     */
    public function testCreatesBillingAgreementForOneDollarQuote()
    {
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $this->_createCustomerSession(13, 1);
        $agreementCount = Mage::getResourceModel('sales/billing_agreement_collection')
            ->addFieldToFilter('customer_id', 13)->count();
        $customer = Mage::getModel('customer/customer')->load(13);
        $data = [
            'method'       => 'tradetested_px_post',
            'cc_number'    => '4111 1111 1111 1111',
            'cc_exp_year'  => date('Y', strtotime('+1 year')),
            'cc_exp_month' => date('m', strtotime('+1 year')),
            'cc_cid'       => '123',
        ];
        $quote = Mage::getModel('sales/quote')->setCustomer($customer);
        $order = Mage::getModel('sales/order')
            ->setBaseGrandTotal(1.00)
            ->setCustomerId($customer->getId())
            ->setStoreId(1);
        $payment = Mage::getModel('sales/order_payment')
            ->setData($data)
            ->setCcSave(true)
            //Yeah the payment method depends on this stuff.
            ->setQuote($quote)
            ->setOrder($order);
        $payment->getMethodInstance()->authorize($payment, 1.00);
        Mage::getModel('sales/billing_agreement')->importOrderPayment($payment)->save();
        
        $this->assertEquals(
            $agreementCount+1,
            Mage::getResourceModel('sales/billing_agreement_collection')->addFieldToFilter('customer_id', 13)->count()
        );
        /** @var TradeTested_Payment_Model_Rewrite_Sales_Billing_Agreement $agreement */
        $agreement = Mage::getResourceModel('sales/billing_agreement_collection')
            ->addFieldToFilter('customer_id', 13)
            ->setOrder('created_at', 'desc')
            ->getFirstItem();
        $expectedData = [
            'customer_id'     => '13',
            'method_code'     => 'tradetested_px_token',
            'reference_id'    => 'billing_id_123',
            'status'          => 'active',
            'agreement_label' => '•••• •••• •••• 1111',
            'is_default'      => '1',
            'expires_at'      => date('Y-m-d', strtotime('first day of next month +1 year')),
            'store_id'        => '1'
        ];
        $expectedAddl = [
            'expiry_label' => date('m', strtotime('+1 year')) . '/' . date('y', strtotime('+1 year')),
            'cc_type'      => 'Visa'
        ];
        $this->assertEquals($expectedData, array_intersect_key($agreement->getData(), $expectedData));
        $this->assertEquals($expectedAddl, array_intersect_key($agreement->getAdditionalData(), $expectedAddl));
        
    }

    /**
     * @loadFixture default
     */
    public function testSendsCorrectDataToClient()
    {
        $this->setConfig();
        $this->setCurrentStore(1);
        $this->_stubSessions();

        $clientMock = $this->getModelMockBuilder('tradetested_payment_express/client')
            ->setMethods(['sendRequest'])
            ->getMock();
        $clientMock->expects($this->once())->method('sendRequest')->with($this->callback(function ($input) {
            $input = (array)$input->getRequestData();
            $this->assertNotEmpty($input['txn_id']);
            unset($input['txn_id']);
            $this->assertNotEmpty($input['merchant_reference']);
            unset($input['merchant_reference']);
            $this->assertEquals($input, [
                'txn_type'             => 'Purchase',
                'input_currency'       => 'NZD',
                'enable_add_bill_card' => false,
                'card_number'          => '4111111111111111',
                'date_expiry'          => date('my', strtotime('+1 year')),
                'amount'               => '20.00',
                'cvc2'                 => '123',
                'cvc2_presence'        => true,
            ]);
            return true;
        }));
        $this->replaceByMock('model', 'tradetested_payment_express/client', $clientMock);

        $quote = Mage::getModel('sales/quote')->load(1);
        $quote->collectTotals();
        $this->setExpectedException('Mage_Payment_Exception');
        $this->_submitQuote($quote);
    }

    /**
     * @loadFixture default
     */
    public function testRefundActionIsCalled()
    {
        $this->setConfig();
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $methodMock = $this->getModelMock('tradetested_payment_express/method_pxpost', ['refund']);
        $methodMock->expects($this->once())
            ->method('refund')
            ->will($this->returnValue($methodMock));
        $this->replaceByMock('model', 'tradetested_payment_express/method_pxpost', $methodMock);

        $quote = Mage::getModel('sales/quote')->load(1)->collectTotals();
        $order = $this->_submitQuote($quote);
        $invoice = $order->getInvoiceCollection()->getFirstItem();
        $this->assertTrue($invoice->canRefund());
        $creditMemo = Mage::getModel('sales/service_order', $order)->prepareInvoiceCreditmemo($invoice)
            ->setDoTransaction(true);
        $this->assertTrue($creditMemo->canRefund());
        $creditMemo->refund();
    }

    /**
     * @loadFixture default
     */
    public function testRefundSavesTxnIdToRefundTransaction()
    {
        $this->setConfig();
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $quote = Mage::getModel('sales/quote')->load(1);
        $quote->collectTotals();
        $order = $this->_submitQuote($quote)->save();
        $order = Mage::getModel('sales/order')->load($order->getId());
        $invoice = $order->getInvoiceCollection()->getFirstItem();
        $this->assertTrue($invoice->canRefund());
        /** @var Mage_Sales_Model_Order_Creditmemo $creditMemo */
        $creditMemo = Mage::getModel('sales/service_order', $order)->prepareInvoiceCreditmemo($invoice)
            ->setDoTransaction(true);
        $this->assertTrue($creditMemo->canRefund());
        $creditMemo->refund();
        $creditMemo->getOrder()->save();
        $order = Mage::getModel('sales/order')->load($order->getId());
        $transaction = $order->getPayment()
            ->lookupTransaction(false, Mage_Sales_Model_Order_Payment_Transaction::TYPE_REFUND);
        $this->assertEquals('R000000031986f8b2', $transaction->getTxnId());
        $this->assertEquals(1, $transaction->getIsClosed());
    }

    /**
     * @loadFixture default
     */
    public function testRefundSetsAmountRefunded()
    {
        $this->setConfig();
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $quote = Mage::getModel('sales/quote')->load(1);
        $quote->collectTotals();
        $order = $this->_submitQuote($quote);
        $invoice = $order->getInvoiceCollection()->getFirstItem();
        $this->assertTrue($invoice->canRefund());
        $creditMemo = Mage::getModel('sales/service_order', $order)->prepareInvoiceCreditmemo($invoice)
            ->setDoTransaction(true);
        $this->assertTrue($creditMemo->canRefund());
        $creditMemo->refund();
        $payment = $order->getPayment();
        $this->assertEquals('20.00', $payment->getAmountRefunded());
    }

    /**
     * @loadFixture default
     */
    public function testRefundCallsApi()
    {
        $this->setConfig();
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $quote = Mage::getModel('sales/quote')->load(1);
        $quote->collectTotals();
        $order = $this->_submitQuote($quote);
        $invoice = $order->getInvoiceCollection()->getFirstItem();
        $this->assertTrue($invoice->canRefund());
        $creditMemo = Mage::getModel('sales/service_order', $order)->prepareInvoiceCreditmemo($invoice)
            ->setDoTransaction(true);

        $clientMock = $this->getModelMockBuilder('tradetested_payment_express/client')
            ->setMethods(['sendRequest'])
            ->getMock();
        $clientMock->expects($this->once())->method('sendRequest')->with($this->callback(function ($input) {
            $input = (array)$input->getRequestData();
            $this->assertNotEmpty($input['merchant_reference']);
            unset($input['merchant_reference']);
            $this->assertEquals($input, [
                'txn_type'    => 'Refund',
                'amount'      => '20.00',
                'dps_txn_ref' => 'P000000031986f8b2'
            ]);
            return true;
        }));
        $this->replaceByMock('model', 'tradetested_payment_express/client', $clientMock);
        $this->assertTrue($creditMemo->canRefund());
        $this->setExpectedException('Mage_Payment_Exception');
        $creditMemo->refund();
    }

    /**
     * @loadFixture default
     */
    public function testRefundPartial()
    {
        $this->setConfig();
        $this->setCurrentStore(1);
        $this->_stubSessions();
        $this->_stubClient();
        $quote = Mage::getModel('sales/quote')->load(1);
        $quote->collectTotals();
        $order = $this->_submitQuote($quote);
        $invoice = $order->getInvoiceCollection()->getFirstItem();
        $this->assertTrue($invoice->canRefund());
        $creditMemo = Mage::getModel('sales/order_creditmemo');
        $creditMemo->setOrder($order)
            ->setStoreId($order->getStoreId())
            ->setCustomerId($order->getCustomerId())
            ->setBillingAddressId($order->getBillingAddressId())
            ->setShippingAddressId($order->getShippingAddressId())
            ->setDoTransaction(true)
            ->setGrandTotal(5.00)
            ->setBaseGrandTotal(5.00);
        $creditMemo->collectTotals();
        $this->assertTrue($creditMemo->canRefund());
        $creditMemo->refund();
        $payment = $order->getPayment();
        $this->assertEquals('5.00', $payment->getAmountRefunded());
    }

    protected function _submitQuote($quote, $paymentData = [])
    {
        $paymentData = array_merge([
            'method'       => 'tradetested_px_post',
            'cc_number'    => '4111 1111 1111 1111',
            'cc_exp_year'  => date('Y', strtotime('+1 year')),
            'cc_exp_month' => date('m', strtotime('+1 year')),
            'cc_cid'       => '123',
        ], $paymentData);
        $quote->getPayment()->setData($paymentData);

        $addressMock = $this->getModelMock('sales/service_quote', ['_validate'], false, [$quote]);
        $addressMock->expects($this->any())
            ->method('_validate')
            ->will($this->returnValue(true));
        $this->replaceByMock('model', 'sales/service_quote', $addressMock);
        $service = Mage::getModel('sales/service_quote', $quote);
        return $service->submitOrder();
    }
}