<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/02/2016
 * Time: 10:48
 */
class TradeTested_PaymentExpress_Test_Model_Client_Request_CaptureTest extends TradeTested_PaymentExpress_Test_Case
{
    public function testGetRequestDataPurchase()
    {
        $data = array(
            'dps_txn_ref'           => null,
            'input_currency'        => 'NZD',
            'card_holder_name'      => 'Test Card',
            'card_number'           => '4111 1111 1111 1111',
            'date_expiry'           => '1220',
            'amount'                => '20.00',
            'merchant_reference'    => 'ref123',
            'txn_id'                => 'tx123',
            'cvc2'                  => '123'
        );
        $request = Mage::getModel('tradetested_payment_express/client_request_capture');
        $expectedData = array(
            'txn_type'              => 'Purchase',
            'input_currency'        => 'NZD',
            'enable_add_bill_card'  => false,
            'card_holder_name'      => 'Test Card',
            'card_number'           => '4111111111111111',
            'date_expiry'           => '1220',
            'amount'                => '20.00',
            'merchant_reference'    => 'ref123',
            'txn_id'                => 'tx123',
            'cvc2'                  => '123',
            'cvc2_presence'         => true
        );
        $this->assertEquals($expectedData, $request->setData($data)->getRequestData());
    }
    
    public function testGetRequestDataPurchaseNoCvc()
    {
        $data = array(
            'dps_txn_ref'           => null,
            'input_currency'        => 'NZD',
            'card_holder_name'      => 'Test Card',
            'card_number'           => '4111 1111 1111 1111',
            'date_expiry'           => '1220',
            'amount'                => '20.00',
            'merchant_reference'    => 'ref123',
            'txn_id'                => 'tx123',
            'cvc2'                  => '123'
        );
        $request = Mage::getModel('tradetested_payment_express/client_request_capture')
            ->setData($data)
            ->setRequireCvc(false);
        $expectedData = array(
            'txn_type'              => 'Purchase',
            'input_currency'        => 'NZD',
            'enable_add_bill_card'  => false,
            'card_holder_name'      => 'Test Card',
            'card_number'           => '4111111111111111',
            'date_expiry'           => '1220',
            'amount'                => '20.00',
            'merchant_reference'    => 'ref123',
            'txn_id'                => 'tx123',
            'cvc2'                  => '123',
            'cvc2_presence'         => false
        );
        $this->assertEquals($expectedData, $request->getRequestData());
    }

    public function testGetRequestDataPurchaseRequestBillingToken()
    {
        $data = array(
            'dps_txn_ref'           => null,
            'input_currency'        => 'NZD',
            'card_holder_name'      => 'Test Card',
            'card_number'           => '4111 1111 1111 1111',
            'date_expiry'           => '1220',
            'amount'                => '20.00',
            'merchant_reference'    => 'ref123',
            'txn_id'                => 'tx123',
            'cvc2'                  => '123'
        );
        $request = Mage::getModel('tradetested_payment_express/client_request_capture')
            ->setData($data)
            ->setRequestBillingToken(true);
        $expectedData = array(
            'txn_type'              => 'Purchase',
            'input_currency'        => 'NZD',
            'enable_add_bill_card'  => true,
            'card_holder_name'      => 'Test Card',
            'card_number'           => '4111111111111111',
            'date_expiry'           => '1220',
            'amount'                => '20.00',
            'merchant_reference'    => 'ref123',
            'txn_id'                => 'tx123',
            'cvc2'                  => '123',
            'cvc2_presence'         => true
        );
        $this->assertEquals($expectedData, $request->getRequestData());
    }
    
    public function testGetRequestDataCapture()
    {
        $data = array(
            'dps_txn_ref'           => 'dpsref123',
            'input_currency'        => 'NZD',
            'amount'                => '20.00',
            'merchant_reference'    => 'ref123',
            'txn_id'                => 'tx123',
            'cvc2'                  => '123'
        );
        $request = Mage::getModel('tradetested_payment_express/client_request_capture')
            ->setData($data)
            ->setRequestBillingToken(true);
        $expectedData = array(
            'txn_type'              => 'Complete',
            'amount'                => '20.00',
            'merchant_reference'    => 'ref123',
            'txn_id'                => 'tx123',
            'cvc2'                  => '123',
            'cvc2_presence'         => false,
            'dps_txn_ref'           => 'dpsref123'
        );
        $this->assertEquals($expectedData, $request->getRequestData());
    }
}
