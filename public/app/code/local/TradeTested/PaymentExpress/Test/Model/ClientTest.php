<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 25/02/2016
 * Time: 17:28
 */
class TradeTested_PaymentExpress_Test_Model_Client extends TradeTested_PaymentExpress_Test_Case
{
    public function testGetRequestXml()
    {
        $client = Mage::getModel('tradetested_payment_express/client', array(
            'pxpost_username' => 'post_username',
            'pxpost_password' => 'post_password'
        ));
        $data = array(
            'txn_type'              => 'Purchase',
            'input_currency'        => 'NZD',
            'enable_add_bill_card'  => false,
            'card_holder_name'      => '',
            'card_number'           => '4111111111111111',
            'date_expiry'           => '0119',
            'amount'                => '20.00',
            'cvc2'                  => '123',
            'cvc2_presence'         => true,
            'merchant_reference'    => 'ref123',
            'txn_id'                => 'txn123'
        );
        $expectedXml = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Txn>
  <Amount>20.00</Amount>
  <CardHolderName />
  <CardNumber>4111111111111111</CardNumber>
  <Cvc2>123</Cvc2>
  <Cvc2Presence>1</Cvc2Presence>
  <DateExpiry>0119</DateExpiry>
  <EnableAddBillCard/>
  <InputCurrency>NZD</InputCurrency>
  <MerchantReference>ref123</MerchantReference>
  <PostPassword>post_password</PostPassword>
  <PostUsername>post_username</PostUsername>
  <TxnId>txn123</TxnId>
  <TxnType>Purchase</TxnType>
</Txn>
XML;
        $this->assertXmlStringEqualsXmlString($expectedXml, $client->getXml($data)->asXML());

    }
    
    public function testParseResponse()
    {
        $client = Mage::getModel('tradetested_payment_express/client', array(
            'pxpost_username' => 'post_username',
            'pxpost_password' => 'post_password'
        ));
        //DPS doesn't return XML header
        $xml = <<<XML
<Txn>
   <Transaction success="1" reco="00" responseText="APPROVED" pxTxn="true">
      <Authorized>1</Authorized>
      <ReCo>00</ReCo>
      <RxDate>20160225174352</RxDate>
      <RxDateLocal>20160226064352</RxDateLocal>
      <LocalTimeZone>NZT</LocalTimeZone>
      <MerchantReference>100038881</MerchantReference>
      <CardName>Visa</CardName>
      <Retry>0</Retry>
      <StatusRequired>0</StatusRequired>
      <AuthCode>064352</AuthCode>
      <AmountBalance>0.00</AmountBalance>
      <Amount>169.00</Amount>
      <CurrencyId>554</CurrencyId>
      <InputCurrencyId>554</InputCurrencyId>
      <InputCurrencyName>NZD</InputCurrencyName>
      <CurrencyRate>1.00</CurrencyRate>
      <CurrencyName>NZD</CurrencyName>
      <CardHolderName />
      <DateSettlement>20160226</DateSettlement>
      <TxnType>Purchase</TxnType>
      <CardNumber>411111........11</CardNumber>
      <TxnMac>2BC20210</TxnMac>
      <DateExpiry>0119</DateExpiry>
      <ProductId />
      <AcquirerDate>20160226</AcquirerDate>
      <AcquirerTime>064352</AcquirerTime>
      <AcquirerId>9001</AcquirerId>
      <Acquirer>Undefined</Acquirer>
      <AcquirerReCo>00</AcquirerReCo>
      <AcquirerResponseText>APPROVED</AcquirerResponseText>
      <TestMode>0</TestMode>
      <CardId>2</CardId>
      <CardHolderResponseText>APPROVED</CardHolderResponseText>
      <CardHolderHelpText>The Transaction was approved</CardHolderHelpText>
      <CardHolderResponseDescription>The Transaction was approved</CardHolderResponseDescription>
      <MerchantResponseText>APPROVED</MerchantResponseText>
      <MerchantHelpText>The Transaction was approved</MerchantHelpText>
      <MerchantResponseDescription>The Transaction was approved</MerchantResponseDescription>
      <UrlFail />
      <UrlSuccess />
      <EnablePostResponse>0</EnablePostResponse>
      <PxPayName />
      <PxPayLogoSrc />
      <PxPayUserId />
      <PxPayXsl />
      <PxPayBgColor />
      <PxPayOptions />
      <Cvc2ResultCode>NotUsed</Cvc2ResultCode>
      <AcquirerPort>10000000-10005172</AcquirerPort>
      <AcquirerTxnRef>368981</AcquirerTxnRef>
      <GroupAccount>9997</GroupAccount>
      <DpsTxnRef>000000031986f8b2</DpsTxnRef>
      <AllowRetry>1</AllowRetry>
      <DpsBillingId />
      <BillingId />
      <TransactionId>1986f8b2</TransactionId>
      <PxHostId>00000003</PxHostId>
      <RmReason />
      <RmReasonId>0000000000000000</RmReasonId>
      <RiskScore>-1</RiskScore>
      <RiskScoreText />
   </Transaction>
   <ReCo>00</ReCo>
   <ResponseText>APPROVED</ResponseText>
   <HelpText>Transaction Approved</HelpText>
   <Success>1</Success>
   <DpsTxnRef>000000031986f8b2</DpsTxnRef>
   <ICCResult />
   <TxnRef>160312706556cf3d</TxnRef>
   <RmReason />
   <RmReasonId>0000000000000000</RmReasonId>
   <RiskScore>-1</RiskScore>
   <RiskScoreText />
</Txn>
XML;
        $expectedResponse = [
            'transaction'     => [
                'attributes'                       => [
                    'success'       => "1",
                    'reco'          => "00",
                    'response_text' => "APPROVED",
                    'px_txn'        => "true",
                ],
                'authorized'                       => "1",
                'response_code'                    => "00",
                'rx_date'                          => "20160225174352",
                'rx_date_local'                    => "20160226064352",
                'local_time_zone'                  => "NZT",
                'merchant_reference'               => "100038881",
                'card_name'                        => "Visa",
                'retry'                            => "0",
                'status_required'                  => "0",
                'auth_code'                        => "064352",
                'amount_balance'                   => "0.00",
                'amount'                           => "169.00",
                'currency_id'                      => "554",
                'input_currency_id'                => "554",
                'input_currency_name'              => "NZD",
                'currency_rate'                    => "1.00",
                'currency_name'                    => "NZD",
                'card_holder_name'                 => null,
                'date_settlement'                  => "20160226",
                'txn_type'                         => "Purchase",
                'card_number'                      => "411111........11",
                'txn_mac'                          => "2BC20210",
                'date_expiry'                      => "0119",
                'product_id'                       => null,
                'acquirer_date'                    => "20160226",
                'acquirer_time'                    => "064352",
                'acquirer_id'                      => "9001",
                'acquirer'                         => "Undefined",
                'acquirer_response_code'           => "00",
                'acquirer_response_text'           => "APPROVED",
                'test_mode'                        => "0",
                'card_id'                          => "2",
                'card_holder_response_text'        => "APPROVED",
                'card_holder_help_text'            => "The Transaction was approved",
                'card_holder_response_description' => "The Transaction was approved",
                'merchant_response_text'           => "APPROVED",
                'merchant_help_text'               => "The Transaction was approved",
                'merchant_response_description'    => "The Transaction was approved",
                'url_fail'                         => null,
                'url_success'                      => null,
                'enable_post_response'             => "0",
                'px_pay_name'                      => null,
                'px_pay_logo_src'                  => null,
                'px_pay_user_id'                   => null,
                'px_pay_xsl'                       => null,
                'px_pay_bg_color'                  => null,
                'px_pay_options'                   => null,
                'cvc2_result_code'                 => "NotUsed",
                'acquirer_port'                    => "10000000-10005172",
                'acquirer_txn_ref'                 => "368981",
                'group_account'                    => "9997",
                'dps_txn_ref'                      => "000000031986f8b2",
                'allow_retry'                      => "1",
                'dps_billing_id'                   => null,
                'billing_id'                       => null,
                'transaction_id'                   => "1986f8b2",
                'px_host_id'                       => "00000003",
                'rm_reason'                        => null,
                'rm_reason_id'                     => "0000000000000000",
                'risk_score'                       => "-1",
                'risk_score_text'                  => null,
            ],
            'response_code'   => "00",
            'response_text'   => "APPROVED",
            'help_text'       => "Transaction Approved",
            'success'         => "1",
            'dps_txn_ref'     => "000000031986f8b2",
            'i_cc_result'     => null,
            'txn_ref'         => "160312706556cf3d",
            'rm_reason'       => null,
            'rm_reason_id'    => "0000000000000000",
            'risk_score'      => "-1",
            'risk_score_text' => null,
        ];
        $this->assertEquals($expectedResponse, $client->parseXml($xml));
    }

    /**
     * @test
     */
    public function testPostWithSuccessResponse()
    {
        $this->_stubClient();
        $client = Mage::getModel('tradetested_payment_express/client');
        $client->mergeConfig([
            'pxpost_username' => 'username',
            'pxpost_password' => 'password'
        ]); //Ecomdev_PHPUnit mocks don't have consructors
        $request = Mage::getModel('tradetested_payment_express/client_request_capture', [
            'txn_type'             => 'Purchase',
            'input_currency'       => 'NZD',
            'enable_add_bill_card' => false,
            'card_holder_name'     => '',
            'card_number'          => '4111111111111111',
            'date_expiry'          => '1220',
            'amount'               => '20.00',
            'cvc2'                 => '123',
            'cvc2_presence'        => true,
            'merchant_reference'   => 'ref123',
            'txn_id'               => 'txn123'
        ]);
        $expectedResponse = [
            'authorized'                    => true,
            'success'                       => true,
            'response_text'                 => 'APPROVED',
            'response_code'                 => '00',
            'merchant_reference'            => 'ref123',
            'card_name'                     => 'Visa',
            'retry'                         => false,
            'amount_balance'                => 0.0,
            'amount'                        => 20.0,
            'currency_name'                 => 'NZD',
            'card_holder_name'              => null,
            'txn_type'                      => 'Purchase',
            'card_number'                   => '411111........11',
            'date_expiry'                   => '1220',
            'merchant_help_text'            => 'The Transaction was approved',
            'merchant_response_description' => 'The Transaction was approved',
            'acquirer_port'                 => '10000000-10005172',
            'allow_retry'                   => true,
            'dps_billing_id'                => null,
            'billing_id'                    => null,
            'transaction_id'                => 'txn123',
        ];
        $response = $client->sendRequest($request);
        //Gives more meaningful failure messages than assertArraySubset.
        $this->assertEquals($expectedResponse, array_intersect_key($response->getData(), $expectedResponse));
        $this->assertTrue($response->getIsSuccess());
    }
    
    public function testPostWithFailureResponse()
    {
        $this->_stubClient();
        $client = Mage::getModel('tradetested_payment_express/client');
        $client->mergeConfig([
            'pxpost_username' => 'username',
            'pxpost_password' => 'password'
        ]); //Ecomdev_PHPUnit mocks don't have consructors
        $request = Mage::getModel('tradetested_payment_express/client_request_capture', []);
        $expectedResponse = [
            'authorized'    => false,
            'success'       => false,
            'response_text' => 'Error in getting response.',
            'response_code' => 'Zz',
        ];
        $response = $client->sendRequest($request);
        $this->assertEquals($expectedResponse, array_intersect_key($response->getData(), $expectedResponse));
        $this->assertFalse($response->getIsSuccess());
    }
    
    public function testPostWithFailureCard()
    {
        $this->_stubClient();
        $client = Mage::getModel('tradetested_payment_express/client');
        $client->mergeConfig([
            'pxpost_username' => 'username',
            'pxpost_password' => 'password'
        ]); //Ecomdev_PHPUnit mocks don't have constructors
        $request = Mage::getModel('tradetested_payment_express/client_request_capture', [
            'txn_type'             => 'Purchase',
            'input_currency'       => 'NZD',
            'enable_add_bill_card' => false,
            'card_holder_name'     => '',
            'card_number'          => '5431111111111301',
            'date_expiry'          => '1220',
            'amount'               => '20.00',
            'cvc2'                 => '123',
            'cvc2_presence'        => true,
            'merchant_reference'   => 'ref123',
            'txn_id'               => 'txn123'
        ]);
        $expectedResponse = [
            'authorized'                    => false,
            'success'                       => false,
            'response_text'                 => 'DECLINED',
            'response_code'                 => '30',
            'merchant_reference'            => 'ref123',
            'card_name'                     => 'MasterCard',
            'auth_code'                     => null,
            'amount_balance'                => 0.0,
            'amount'                        => 20.0,
            'currency_name'                 => 'NZD',
            'card_holder_name'              => null,
            'txn_type'                      => 'Purchase',
            'card_number'                   => '543111........01',
            'date_expiry'                   => '1220',
            'merchant_help_text'            => 'The transaction was Declined (30)',
            'merchant_response_description' => 'The transaction was Declined (30)',
            'allow_retry'                   => true,
            'dps_billing_id'                => null,
            'billing_id'                    => null,
            'transaction_id'                => 'txn123',
        ];
        $response = $client->sendRequest($request);
        $this->assertEquals($expectedResponse, array_intersect_key($response->getData(), $expectedResponse));
        $this->assertFalse($response->getIsSuccess());
    }
}
