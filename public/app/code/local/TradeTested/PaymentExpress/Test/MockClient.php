<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/02/2016
 * Time: 14:12
 */
class TradeTested_PaymentExpress_Test_MockClient extends TradeTested_PaymentExpress_Model_Client
{
    public function request($httpMethod)
    {
        if (! $this->uri instanceof Zend_Uri_Http) {
            throw new Zend_Http_Client_Exception('No valid URI has been passed to the client');
        }
        $data = $this->parseXml($this->_prepareBody());
        if (
            (($data['card_number'] ?? null) == '4111111111111111') 
            && in_array($data['txn_type'] ?? null, ['Purchase', 'Auth'])
        ) {
            return $this->_buildHttpResponse($this->_getSuccessResponse($data));
        } elseif (($data['txn_type'] === 'Complete') && ($data['dps_txn_ref'] === 'A000000031986f8b2')) {
            return $this->_buildHttpResponse($this->_getSuccessResponse($data));
        } elseif (($data['card_number'] ?? null) && in_array(($data['txn_type'] ?? null), ['Purchase', 'Auth'])) {
            return $this->_buildHttpResponse($this->_getDeclinedResponse($data));
        } elseif (
            (($data['dps_billing_id'] ?? null) == 'bill_id_123')
            && in_array(($data['txn_type'] ?? null), ['Purchase', 'Auth'])
            && (!($data['cvc2_presence']) || ($data['cvc2'] ?? null))
        ) {
            return $this->_buildHttpResponse($this->_getSuccessResponse($data));
        } elseif((($data['txn_type'] ?? null) === 'Refund') && $data['dps_txn_ref'] === 'P000000031986f8b2') {
            return $this->_buildHttpResponse($this->_getSuccessResponse($data));
        } else {
            return $this->_buildHttpResponse(
                "<Txn><ReCo>Zz</ReCo><ResponseText>Error in getting response.</ResponseText><Success>0</Success></Txn>"
                //This is response when did not send PXPost user and pass
            );
        }
    }
    
    protected function _buildHttpResponse($body)
    {
        $headers = [
            'Cache-control'    => "private",
            'Content-length'   => "101",
            'Content-type'     => "application/xhtml+xml; charset=utf-8",
            'Expires'          => "Mon, 29 Feb 2016 16:38:54 GMT",
            'Server'           => "",
            'X-aspnet-version' => "",
            'X-powered-by'     => "",
            'Date'             => "Mon, 29 Feb 2016 16:39:53 GMT",
            'Connection'       => "close"
        ];
        return new Zend_Http_Response(200, $headers, $body, '1.1', 'OK');
    }
    
    protected function _getSuccessResponse($data)
    {
        $date = date('YmdHis');
        $shortDate = date('Ymd');
        $time = date('His');
        $cardNumber = isset($data['card_number']) 
            ? substr($data['card_number'], 0, 6).'........'.substr($data['card_number'], -2)
            : null;
        $prefix = substr($data['txn_type'], 0, 1);
        $inputCurrency = $data['input_currency'] ?? null;
        $expiry = $data['date_expiry'] ?? null;
        $txnId = $data['txn_id'] ?? null;
        $billingId = ($data['enable_add_bill_card'] ?? null) ? 'billing_id_123' : null;
        $merchantReference = $data['merchant_reference'] ?? null;
        return <<<XML
<Txn>
   <Transaction success="1" reco="00" responseText="APPROVED" pxTxn="true">
      <Authorized>1</Authorized>
      <ReCo>00</ReCo>
      <RxDate>{$date}</RxDate>
      <RxDateLocal>{$date}</RxDateLocal>
      <LocalTimeZone>NZT</LocalTimeZone>
      <MerchantReference>{$merchantReference}</MerchantReference>
      <CardName>Visa</CardName>
      <Retry>0</Retry>
      <StatusRequired>0</StatusRequired>
      <AuthCode>064352</AuthCode>
      <AmountBalance>0.00</AmountBalance>
      <Amount>{$data['amount']}</Amount>
      <CurrencyId>554</CurrencyId>
      <InputCurrencyId>554</InputCurrencyId>
      <InputCurrencyName>{$inputCurrency}</InputCurrencyName>
      <CurrencyRate>1.00</CurrencyRate>
      <CurrencyName>{$inputCurrency}</CurrencyName>
      <CardHolderName />
      <DateSettlement>{$shortDate}</DateSettlement>
      <TxnType>{$data['txn_type']}</TxnType>
      <CardNumber>{$cardNumber}</CardNumber>
      <TxnMac>2BC20210</TxnMac>
      <DateExpiry>{$expiry}</DateExpiry>
      <ProductId />
      <AcquirerDate>{$shortDate}</AcquirerDate>
      <AcquirerTime>{$time}</AcquirerTime>
      <AcquirerId>9001</AcquirerId>
      <Acquirer>Undefined</Acquirer>
      <AcquirerReCo>00</AcquirerReCo>
      <AcquirerResponseText>APPROVED</AcquirerResponseText>
      <TestMode>0</TestMode>
      <CardId>2</CardId>
      <CardHolderResponseText>APPROVED</CardHolderResponseText>
      <CardHolderHelpText>The Transaction was approved</CardHolderHelpText>
      <CardHolderResponseDescription>The Transaction was approved</CardHolderResponseDescription>
      <MerchantResponseText>APPROVED</MerchantResponseText>
      <MerchantHelpText>The Transaction was approved</MerchantHelpText>
      <MerchantResponseDescription>The Transaction was approved</MerchantResponseDescription>
      <UrlFail />
      <UrlSuccess />
      <EnablePostResponse>0</EnablePostResponse>
      <PxPayName />
      <PxPayLogoSrc />
      <PxPayUserId />
      <PxPayXsl />
      <PxPayBgColor />
      <PxPayOptions />
      <Cvc2ResultCode>NotUsed</Cvc2ResultCode>
      <AcquirerPort>10000000-10005172</AcquirerPort>
      <AcquirerTxnRef>368981</AcquirerTxnRef>
      <GroupAccount>9997</GroupAccount>
      <DpsTxnRef>{$prefix}000000031986f8b2</DpsTxnRef>
      <AllowRetry>1</AllowRetry>
      <DpsBillingId>{$billingId}</DpsBillingId>
      <BillingId />
      <TransactionId>{$txnId}</TransactionId>
      <PxHostId>00000003</PxHostId>
      <RmReason />
      <RmReasonId>0000000000000000</RmReasonId>
      <RiskScore>-1</RiskScore>
      <RiskScoreText />
   </Transaction>
   <ReCo>00</ReCo>
   <ResponseText>APPROVED</ResponseText>
   <HelpText>Transaction Approved</HelpText>
   <Success>1</Success>
   <DpsTxnRef>000000031986f8b2</DpsTxnRef>
   <ICCResult />
   <TxnRef>160312706556cf3d</TxnRef>
   <RmReason />
   <RmReasonId>0000000000000000</RmReasonId>
   <RiskScore>-1</RiskScore>
   <RiskScoreText />
</Txn>
XML;
    }

    protected function _getDeclinedResponse($data)
    {
        $date = date('YmdHis');
        $shortDate = date('Ymd');
        $time = date('His');
        $cardNumber = substr($data['card_number'], 0, 6).'........'.substr($data['card_number'], -2);
        return <<<XML
<Txn>
   <Transaction success="0" reco="30" responseText="DECLINED" pxTxn="true">
      <Authorized>0</Authorized>
      <ReCo>30</ReCo>
      <RxDate>{$date}</RxDate>
      <RxDateLocal>{$date}</RxDateLocal>
      <LocalTimeZone>{$data['input_currency']}</LocalTimeZone>
      <MerchantReference>{$data['merchant_reference']}</MerchantReference>
      <CardName>MasterCard</CardName>
      <Retry>0</Retry>
      <StatusRequired>0</StatusRequired>
      <AuthCode />
      <AmountBalance>0.00</AmountBalance>
      <Amount>{$data['amount']}</Amount>
      <CurrencyId>554</CurrencyId>
      <InputCurrencyId>554</InputCurrencyId>
      <InputCurrencyName>{$data['input_currency']}</InputCurrencyName>
      <CurrencyRate>1.00</CurrencyRate>
      <CurrencyName>{$data['input_currency']}</CurrencyName>
      <CardHolderName />
      <DateSettlement>{$shortDate}</DateSettlement>
      <TxnType>{$data['txn_type']}</TxnType>
      <CardNumber>{$cardNumber}</CardNumber>
      <TxnMac>2BC20210</TxnMac>
      <DateExpiry>{$data['date_expiry']}</DateExpiry>
      <ProductId />
      <AcquirerDate>{$shortDate}</AcquirerDate>
      <AcquirerTime>{$time}</AcquirerTime>
      <AcquirerId>9001</AcquirerId>
      <Acquirer>Undefined</Acquirer>
      <AcquirerReCo>30</AcquirerReCo>
      <AcquirerResponseText>DECLINED</AcquirerResponseText>
      <TestMode>0</TestMode>
      <CardId>1</CardId>
      <CardHolderResponseText>DECLINED</CardHolderResponseText>
      <CardHolderHelpText>The transaction was Declined (30)</CardHolderHelpText>
      <CardHolderResponseDescription>The transaction was Declined (30)</CardHolderResponseDescription>
      <MerchantResponseText>DECLINED</MerchantResponseText>
      <MerchantHelpText>The transaction was Declined (30)</MerchantHelpText>
      <MerchantResponseDescription>The transaction was Declined (30)</MerchantResponseDescription>
      <UrlFail />
      <UrlSuccess />
      <EnablePostResponse>0</EnablePostResponse>
      <PxPayName />
      <PxPayLogoSrc />
      <PxPayUserId />
      <PxPayXsl />
      <PxPayBgColor />
      <PxPayOptions />
      <Cvc2ResultCode>NotUsed</Cvc2ResultCode>
      <AcquirerPort>10000000-10005172</AcquirerPort>
      <AcquirerTxnRef>443878</AcquirerTxnRef>
      <GroupAccount>9997</GroupAccount>
      <DpsTxnRef>0000000319b9a835</DpsTxnRef>
      <AllowRetry>1</AllowRetry>
      <DpsBillingId />
      <BillingId />
      <TransactionId>{$data['txn_id']}</TransactionId>
      <PxHostId>00000003</PxHostId>
      <RmReason />
      <RmReasonId>0000000000000000</RmReasonId>
      <RiskScore>-1</RiskScore>
      <RiskScoreText />
   </Transaction>
   <ReCo>30</ReCo>
   <ResponseText>DECLINED</ResponseText>
   <HelpText>The transaction was Declined (30)</HelpText>
   <Success>0</Success>
   <DpsTxnRef>0000000319b9a835</DpsTxnRef>
   <ICCResult />
   <TxnRef>txn123</TxnRef>
   <RmReason />
   <RmReasonId>0000000000000000</RmReasonId>
   <RiskScore>-1</RiskScore>
   <RiskScoreText />
</Txn>
XML;

    }
}