<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 25/02/2016
 * Time: 09:16
 */
class TradeTested_PaymentExpress_Test_Case extends EcomDev_PHPUnit_Test_Case
{
    public function setUp()
    {
        $this->setCurrentStore(1);
        $this->clearConfig();
    }

    public static function clearConfig()
    {
        Mage::getConfig()->reinit();
        Mage::app()->reinitStores();
    }

    public function setConfig($override = array())
    {
        $settings = [
            'payment/tradetested_px_post/payment_action' => 'authorize_capture',
            'messenger/rabbitmq/host' => null
        ];
        $settings = array_merge($settings, $override);
        foreach ($settings as $_node => $_value) {
            Mage::getConfig()->setNode('stores/admin/'.$_node, $_value);
            Mage::getConfig()->setNode('stores/default/'.$_node, $_value);
        }
    }

    /**
     * Set customer as logged in using session
     *
     * @param $customerId
     * @param null $storeId
     * @return EcomDev_PHPUnit_Mock_Proxy
     */
    protected function _createCustomerSession($customerId, $storeId = null)
    {
        // Create customer session mock, for making our session singleton isolated
        $customerSessionMock = $this->mockSession('customer/session', array('renewSession'));
        $this->replaceByMock('singleton', 'customer/session', $customerSessionMock);

        if ($storeId === null) {
            $storeId = $this->app()->getAnyStoreView()->getCode();
        }

        $this->setCurrentStore($storeId);
        $customerSessionMock->loginById($customerId);

        return $customerSessionMock;
    }

    /**
     * Clear customer from session.
     */
    protected function _logout()
    {
        Mage::getSingleton('customer/session')
            ->setId(null)
            ->setCustomerGroupId(Mage_Customer_Model_Group::NOT_LOGGED_IN_ID);
    }

    public static function stubSessions($case)
    {
        $sessionMock = $case->getModelMockBuilder('customer/session')
            ->disableOriginalConstructor() // This one removes session_start and other methods usage
            ->setMethods(null) // Enables original methods usage, because by default it overrides all methods
            ->getMock();
        $case->replaceByMock('singleton', 'customer/session', $sessionMock);

        $sessionMock = $case->getModelMockBuilder('core/session')
            ->disableOriginalConstructor()
            ->setMethods(array('init'))
            ->getMock();
        $sessionMock->expects($case->any())
            ->method('init')
            ->will($case->returnValue($sessionMock));
        $case->replaceByMock('model', 'core/session', $sessionMock);
        $case->replaceByMock('singleton', 'core/session', $sessionMock);

        $cookieMock = $case->getModelMockBuilder('core/cookie')
            ->setMethods(array('set'))
            ->getMock();
        $cookieMock->expects($case->any())
            ->method('set')
            ->will($case->returnValue($cookieMock));
        $case->replaceByMock('model', 'core/cookie', $cookieMock);
        $case->replaceByMock('singleton', 'core/cookie', $cookieMock);
    }

    protected function _stubSessions()
    {
        $this->stubSessions($this);
    }

    public static function stubClient(EcomDev_PHPUnit_Test_Case $case)
    {
        $proxy = new EcomDev_PHPUnit_Mock_Proxy(
            $case, 
            'TradeTested_PaymentExpress_Test_MockClient', 
            'tradetested_payment_express/client'
        );
        $clientMock = $proxy->setMethods(array('__none__'))->getMock(); //Stubs all methods if setMethods not used.
        $case->replaceByMock('model', 'tradetested_payment_express/client', $clientMock);
    }

    protected function _stubClient()
    {
        $this->stubClient($this);
    }

    /**
     * This method is called when a test method did not execute successfully.
     *
     * Overridden to cater for PHP7 Errors as well as Exceptions
     *
     * @param Throwable $e
     * @since Method available since Release 3.4.0
     * @throws Throwable
     */
    protected function onNotSuccessfulTest(Throwable $e)
    {
        throw $e;
    }
}