<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 25/02/2016
 * Time: 09:48
 */
class TradeTested_PaymentExpress_Test_Config_All extends EcomDev_PHPUnit_Test_Case_Config
{
    public function setUp()
    {
        TradeTested_PaymentExpress_Test_Case::clearConfig();
    }

    /**
     * @test
     */
    public function testModule()
    {
        $this->assertModuleCodePool('local');
        $this->assertModuleVersionGreaterThanOrEquals('0.1.0');
    }

    /**
     * @test
     */
    public function testConfig()
    {
        $this->assertConfigNodeHasChild('global/helpers', 'tradetested_payment_express');
        $this->assertConfigNodeValue(
            'global/helpers/tradetested_payment_express/class',
            'TradeTested_PaymentExpress_Helper'
        );
        $this->assertConfigNodeHasChild('global/models', 'tradetested_payment_express');
        $this->assertConfigNodeValue(
            'global/models/tradetested_payment_express/class',
            'TradeTested_PaymentExpress_Model'
        );
        $this->assertConfigNodeHasChild('global/blocks', 'tradetested_payment_express');
        $this->assertConfigNodeValue(
            'global/blocks/tradetested_payment_express/class',
            'TradeTested_PaymentExpress_Block'
        );
    }

    /**
     * @test
     */
    public function testModels()
    {
        $this->assertModelAlias(
            'tradetested_payment_express/method_pxpost',
            'TradeTested_PaymentExpress_Model_Method_Pxpost'
        );
        $this->assertModelAlias(
            'tradetested_payment_express/method_token',
            'TradeTested_PaymentExpress_Model_Method_Token'
        );
    }

    /**
     * @test
     */
    public function testDefaults()
    {
        $this->assertConfigNodeValue(
            'stores/default/payment/tradetested_px_post/allow_billing_agreement_wizard', 1
        );
        $this->assertConfigNodeValue('stores/default/payment/tradetested_px_post/active', 0);
        $this->assertConfigNodeValue('stores/default/payment/tradetested_px_post/frontend_checkout', 1);
        $this->assertConfigNodeValue('stores/default/payment/tradetested_px_post/cctypes', 'OT');
        $this->assertConfigNodeValue(
            'stores/default/payment/tradetested_px_post/payment_action', 'authorize_capture'
        );
        $this->assertConfigNodeValue(
            'stores/default/payment/tradetested_px_post/order_status', 'processing'
        );
        $this->assertConfigNodeValue('stores/default/payment/tradetested_px_post/title', 'Credit Card');
        $this->assertConfigNodeValue('stores/default/payment/tradetested_px_post/min_order_total', 0);
        $this->assertConfigNodeValue('stores/default/payment/tradetested_px_post/max_order_total', 0);
        $this->assertConfigNodeValue('stores/default/payment/tradetested_px_post/debug', 0);


        $this->assertConfigNodeValue('stores/default/payment/tradetested_px_token/active', 0);
        $this->assertConfigNodeValue('stores/default/payment/tradetested_px_token/frontend_checkout', 1);
        $this->assertConfigNodeValue(
            'stores/default/payment/tradetested_px_token/payment_action', 'authorize_capture'
        );
        $this->assertConfigNodeValue(
            'stores/default/payment/tradetested_px_token/order_status', 'processing'
        );
        $this->assertConfigNodeValue('stores/default/payment/tradetested_px_token/title', 'Credit Card');
        $this->assertConfigNodeValue('stores/default/payment/tradetested_px_token/min_order_total', 0);
        $this->assertConfigNodeValue('stores/default/payment/tradetested_px_token/max_order_total', 0);
        $this->assertConfigNodeValue('stores/default/payment/tradetested_px_token/debug', 0);
        $this->assertConfigNodeValue('stores/default/payment/tradetested_px_token/cvv_order_value', 100);

    }

    /**
     * @test
     */
    public function testPaymentMethodIsSpecified()
    {
        $this->assertConfigNodeValue(
            'stores/default/payment/tradetested_px_post/model',
            'tradetested_payment_express/method_pxpost'
        );
        $this->assertConfigNodeValue(
            'stores/default/payment/tradetested_px_token/model',
            'tradetested_payment_express/method_token'
        );
    }
}
