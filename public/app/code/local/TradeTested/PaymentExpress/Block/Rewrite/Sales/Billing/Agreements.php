<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 09/10/2015
 * Time: 10:29
 */
class TradeTested_PaymentExpress_Block_Rewrite_Sales_Billing_Agreements extends Mage_Sales_Block_Billing_Agreements
{
    /**
     * Retrieve billing agreements collection
     *
     * @return Mage_Sales_Model_Mysql4_Billing_Agreement_Collection
     */
    public function getBillingAgreements()
    {
        if (is_null($this->_billingAgreements)) {
            $this->_billingAgreements = Mage::getResourceModel('sales/billing_agreement_collection')
                ->addFieldToFilter('status', 'active')
                ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomerId())
                ->setOrder('agreement_id', 'desc');
        }
        return $this->_billingAgreements;
    }
}