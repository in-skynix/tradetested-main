<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/03/2016
 * Time: 13:55
 */
class TradeTested_PaymentExpress_Block_Pxpost_Info extends Mage_Payment_Block_Info_Cc
{
    /**
     * Set template on construct
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('tradetested/payment_express/pxpost/info.phtml');
    }

    /**
     * Get Additional Data from Payment
     *
     * @param $key
     * @return mixed|string
     */
    public function getAdditionalData($key)
    {
        return Mage::helper('tradetested_payment_express')->getAdditionalData($this->getInfo(), $key);
    }
}