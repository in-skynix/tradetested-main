<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/03/2016
 * Time: 13:55
 */
class TradeTested_PaymentExpress_Block_Pxpost_Form extends Mage_Payment_Block_Form_Cc
{
    /**
     * Set template on construct
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('tradetested/payment_express/pxpost/form.phtml');
    }

    /**
     * Should we ask to save CC?
     *
     * @return bool
     */
    public function displayAddCcSave()
    {
        return (
            Mage::getSingleton('customer/session')->isLoggedIn()
            && $this->helper('tradetested_payment_express')->shouldOfferSaveBillingToken()
        );
    }
}