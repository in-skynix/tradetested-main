<?php

class TradeTested_PaymentExpress_Block_Token_Info extends Mage_Payment_Block_Info_Cc
{
    /**
     * Set template on construct
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('tradetested/payment_express/token/info.phtml');
    }

    /**
     * Get Additional Data from Payment Instance
     *
     * @param $key
     * @return mixed|string
     */
    public function getAdditionalData($key)
    {
        return Mage::helper('tradetested_payment_express')->getAdditionalData($this->getInfo(), $key);
    }
}
