<?php

class TradeTested_PaymentExpress_Block_Token_Form extends Mage_Payment_Block_Form_Cc
{
    /**
     * Set template and method code on construct.
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('tradetested/payment_express/token/form.phtml');
        $this->setMethodCode(Mage::getModel('tradetested_payment_express/method_token')->getCode());
    }

    /**
     * Get Billing Agreements for current quote/order
     *
     * @return array
     */
    public function getBillingAgreements()
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn() || Mage::app()->getStore()->isAdmin()) {
            return $this->getMethod()->getBillingAgreements()->getItems();
        }
        return array();
    }
}
