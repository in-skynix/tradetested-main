<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 09/10/2015
 * Time: 16:49
 */
class TradeTested_PaymentExpress_Credit_CardController extends Mage_Core_Controller_Front_Action
{
    /**
     * Add new card form
     */
    public function newAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Save add new card.
     *
     * @return Mage_Core_Controller_Varien_Action
     */
    public function saveAction()
    {
        try {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $data = $this->getRequest()->getPost('payment') + array('method' => 'tradetested_px_post');
            $quote = Mage::getModel('sales/quote')->setCustomer($customer);
            $order = Mage::getModel('sales/order')
                ->setBaseGrandTotal(1.00)
                ->setCustomerId($customer->getId())
                ->setStoreId(1);
            $payment = Mage::getModel('sales/order_payment')
                ->setData($data)
                ->setCcSave(true)
                //Yeah the payment method depends on this stuff.
                ->setQuote($quote)
                ->setOrder($order);
            $payment->getMethodInstance()->authorize($payment, 1.00);
            Mage::getModel('sales/billing_agreement')->importOrderPayment($payment)->save();
        } catch (Exception $e) {
            Mage::getSingleton('customer/session')->addError('We could not verify your card details. Please try again');
            return $this->_redirect('sales/billing_agreement/index');
        }
        Mage::getSingleton('customer/session')->addSuccess('Your new card details have been verified');
        $this->_redirect('sales/billing_agreement/index');
    }

    /**
     * Set card as default
     */
    public function setDefaultAction()
    {
        $agreement = Mage::getModel('sales/billing_agreement')->load($this->getRequest()->getParam('agreement'));
        $agreement->setIsDefault(true)->save();
        $this->_redirect('sales/billing_agreement/index');
    }

    /**
     * Cancel action
     * Set billing agreement status to 'Canceled'
     *
     * Lifted from Mage_Sales_Billing_AgreementController, but with ability to redirect back to checkout
     */
    public function deleteAction()
    {
        $agreement = $this->_initAgreement();
        if(!$agreement) {
            $this->_redirect('ttcheckout/index/payment', array('_current' => true));
            return;
        }

        $customerIdSession = Mage::getSingleton('customer/session')->getCustomer()->getId();
        $checkoutSession = Mage::getSingleton('checkout/session');
        if (!$agreement->canPerformAction($customerIdSession)) {
            $this->_redirect('ttcheckout/index/payment', array('_current' => true));
            return;
        }

        if ($agreement->canCancel()) {
            try {
                $agreement->cancel();
                $checkoutSession->addNotice($this->__(
                    'The billing agreement "%s" has been canceled.', $agreement->getReferenceId())
                );
            } catch (Mage_Core_Exception $e) {
                $checkoutSession->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::logException($e);
                $checkoutSession->addError($this->__('Failed to cancel the billing agreement.'));
            }
        }
        $this->_redirect('ttcheckout/index/payment', array('_current' => true));
    }

    /**
     * Init billing agreement model from request
     *
     * @return Mage_Sales_Model_Billing_Agreement
     */
    protected function _initAgreement()
    {
        $agreementId = $this->getRequest()->getParam('agreement');
        if ($agreementId) {
            $billingAgreement = Mage::getModel('sales/billing_agreement')->load($agreementId);
            if (!$billingAgreement->getAgreementId()) {
                $this->_getSession()->addError($this->__('Wrong billing agreement ID specified.'));
                $this->_redirect('*/*/');
                return false;
            }
        }
        Mage::register('current_billing_agreement', $billingAgreement);
        return $billingAgreement;
    }
}