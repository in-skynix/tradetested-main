<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 09/10/2015
 * Time: 10:26
 */
class TradeTested_PaymentExpress_Sales_Billing_AgreementController extends Mage_Core_Controller_Front_Action
{
    /**
     * View Billing Agreements
     */
    public function viewAction()
    {
        $this->_redirect('*/*/index');
    }
}