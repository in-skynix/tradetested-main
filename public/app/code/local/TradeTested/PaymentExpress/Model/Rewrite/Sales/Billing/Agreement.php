<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 08/10/2015
 * Time: 17:38
 */
class TradeTested_PaymentExpress_Model_Rewrite_Sales_Billing_Agreement extends Mage_Sales_Model_Billing_Agreement
{

    /**
     * Set as default if new
     *
     * @return Mage_Core_Model_Abstract
     * @throws Exception
     */
    public function save()
    {
        if (!$this->getId()) {
            $this->setIsDefault(true);
        }
        return parent::save();
    }

    /**
     * @param int $id
     * @param null $field
     * @return TradeTested_PaymentExpress_Model_Rewrite_Sales_Billing_Agreement
     */
    public function load($id, $field = null)
    {
        if (is_null($field) && (strpos($id, 'e:') === 0)) {
            return $this->loadByEncId($id);
        } else {
            return parent::load($id, $field);
        }
    }

    /**
     * Import payment data to billing agreement
     *
     * $payment->getBillingAgreementData() contains array with following structure :
     *  [billing_agreement_id]  => string
     *  [method_code]           => string
     *
     * @param Mage_Sales_Model_Order_Payment $payment
     * @return Mage_Sales_Model_Billing_Agreement
     */
    public function importOrderPayment(Mage_Sales_Model_Order_Payment $payment)
    {
        $baData = $payment->getBillingAgreementData();

        $this->_paymentMethodInstance = (isset($baData['method_code']))
            ? Mage::helper('payment')->getMethodInstance($baData['method_code'])
            : $payment->getMethodInstance();
        if ($this->_paymentMethodInstance) {
            $this->_paymentMethodInstance->setStore($payment->getMethodInstance()->getStore());
            $this->setCustomerId($payment->getOrder()->getCustomerId())
                ->setMethodCode($this->_paymentMethodInstance->getCode())
                ->setReferenceId($baData['billing_agreement_id'])
                ->setStatus(self::STATUS_ACTIVE)
                ->setAdditionalData($baData['additional_data'] ?? null)
                ->setAgreementLabel($baData['agreement_label'] ?? null)
                ->setStoreId($payment->getOrder()->getStoreId())
                ->setExpiresAt($baData['expires_at'] ?? null);
        }
        return $this;
    }

    /**
     * @param $key
     * @param null $value
     * @return $this
     */
    public function setAdditionalData($key, $value = null)
    {
        if (is_null($key)) {
            return $this->setData('additional_data', serialize([]));
        } elseif (is_array($key)) {
            return $this->setData('additional_data', serialize($key));
        } else {
            return $this->setData('additional_data', array_merge($this->getAdditionalData(), [$key => $value]));
        }
    }

    /**
     * @param null $key
     * @return array|mixed 
     */
    public function getAdditionalData($key = null)
    {
        if ($data = $this->getData('additional_data')) {
            $data = unserialize($data);
        } else {
            $data = [];
        }
        return is_null($key) ? $data : ($data[$key] ?? null);
    }

    /**
     * @return string
     */
    public function getEncId()
    {
        $val = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->_getKey(), $this->getId(), MCRYPT_MODE_ECB);
        return "e:".strtr(base64_encode($val), '+/=', '-_,');
    }

    /**
     * @param $encId
     * @return $this
     */
    public function loadByEncId($encId)
    {
        $val = base64_decode(strtr(substr($encId, 2), '-_,', '+/='));
        $id = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->_getKey(), $val, MCRYPT_MODE_ECB);
        return $this->load($id);
    }

    /**
     * @return string
     */
    protected function _getKey()
    {
        return 'adfcww1231lkjwee';
    }

    /**
     * Maintain single default if setting to default or cancelling default
     */
    protected function _afterSave()
    {
        parent::_afterSave();
        if($this->getOrigData('is_default') && $this->getStatus() == 'canceled') {
            $this->setIsDefault(false);
            $newDefault = Mage::getModel('sales/billing_agreement')->getCollection()
                ->addFieldToFilter('agreement_id', array('neq' => $this->getId()))
                ->addFieldToFilter('customer_id', $this->getCustomerId())
                ->addFieldToFilter('method_code', $this->getMethodCode())
                ->addFieldToFilter('status', 'active')
                ->setOrder('created_at')
                ->getFirstItem();
            if ($newDefault->getId()) {
                $newDefault->setIsDefault(true)->save();
            }
        }
        if($this->getIsDefault() && !$this->getOrigData('is_default')) {
            $oldDefaults = Mage::getModel('sales/billing_agreement')->getCollection()
                ->addFieldToFilter('agreement_id', array('neq' => $this->getId()))
                ->addFieldToFilter('customer_id', $this->getCustomerId())
                ->addFieldToFilter('method_code', $this->getMethodCode())
                ->addFieldToFilter('is_default', true);
            foreach ($oldDefaults as $_existing) {
                $_existing->setIsDefault(false)->save();
            }
        }
    }
}