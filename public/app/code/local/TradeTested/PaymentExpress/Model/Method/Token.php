<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 25/02/2016
 * Time: 14:30
 */
class TradeTested_PaymentExpress_Model_Method_Token extends TradeTested_PaymentExpress_Model_Method_Pxpost
    implements Mage_Payment_Model_Billing_Agreement_MethodInterface
{

    protected $_code = 'tradetested_px_token';
    protected $_formBlockType = 'tradetested_payment_express/token_form';
    protected $_infoBlockType = 'tradetested_payment_express/token_info';

    /**
     * Assign data to info model instance
     *
     * @param   mixed $data
     * @return  Mage_Payment_Model_Info
     */
    public function assignData($data)
    {
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
        $info->setBillingAgreementId($data->getBillingAgreementId())
            ->setCcCid($data->getCcCid());
        return $this;
    }

    /**
     * Retrieve information from payment configuration
     *
     * @param string $field
     * @param int|string|null|Mage_Core_Model_Store $storeId
     *
     * @return mixed
     */
    public function getConfigData($field, $storeId = null)
    {
        if ($field == 'useccv') {
            return $this->_getRequireCvv($storeId);
        } else {
            return parent::getConfigData($field, $storeId);
        }
    }

    /**
     * Get billing agreements that can be used by the order or quote.
     *
     * @param null $customerId
     * @return mixed
     */
    public function getBillingAgreements($customerId = null, $storeId = null)
    {
        $storeId = $storeId ? $storeId : Mage::app()->getStore()->getId();
        $customerId = $customerId ?? $this->_getOrderOrQuote()->getCustomerId();
        return Mage::getModel('sales/billing_agreement')
            ->getAvailableCustomerBillingAgreements($customerId)
            ->addFieldToFilter('store_id', $storeId)
            ->addFieldToFilter('method_code', $this->_code);
    }

    /**
     * Validate the CVV if required
     *
     * Validating the billing agreement is done when it is loaded, because completions don't use agreements.
     *
     * @throws Mage_Core_Exception
     */
    public function validate()
    {
        $info = $this->getInfoInstance();
        if ($this->_getRequireCvv()) {
            $verifcationRegEx = $this->getVerificationRegEx();
            $regExp = $verifcationRegEx['OT'];
            if (!$info->getCcCid() || !$regExp || !preg_match($regExp, $info->getCcCid())) {
                Mage::throwException(
                    Mage::helper('payment')->__('Please enter a valid credit card verification number.')
                );
            }
        }
    }

    /**
     * Can use in checkout?
     *
     * @return bool
     */
    public function canUseCheckout()
    {
        $customerSession = Mage::getSingleton('customer/session');
        return (
            parent::canUseCheckout()
            && $customerSession->isLoggedIn()
            && $this->getBillingAgreements($customerSession->getCustomerId())->count()
        );
    }

    /**
     * @return $this
     * @throws Mage_Payment_Exception
     */
    protected function _setTransactionInformation()
    {
        parent::_setTransactionInformation();
        if ($billingAgreement = $this->_getBillingAgreement()) {
            $order = $this->getPayment()->getOrder();
            $this->getPayment()->setAdditionalData(serialize($this->getTransactionAdditionalInfo()));
            $order->addRelatedObject($billingAgreement);
            $billingAgreement->setIsObjectChanged(true);
            $billingAgreement->addOrderRelation($order);
        }
        return $this;
    }

    /**
     * Get data to be sent to DPS for any transaction type
     *
     * @return array
     * @throws Mage_Payment_Exception
     */
    protected function _getRequestData()
    {
        $orderOrQuote = $this->_getOrderOrQuote();
        $payment = $this->getPayment();
        $txId = $payment->getParentTransactionId() ? $payment->getParentTransactionId() : $payment->getLastTransId();
        $agreement = $this->_getBillingAgreement();

        //@todo: fix this to use either order number or secure random
        if (!$this->getTransactionId()) {
            $this->setTransactionId(substr(uniqid(rand()), 0, 16));
        }

        return [
            'dps_txn_ref'        => $txId,
            'input_currency'     => $orderOrQuote->getBaseCurrencyCode(),
            'amount'             => trim(sprintf("%9.2f", $this->getAmount())),
            'merchant_reference' => $orderOrQuote->getIncrementId()
                ? $orderOrQuote->getIncrementId()
                : $orderOrQuote->getReservedOrderId(),
            'txn_id'             => $this->getTransactionId(),
            'cvc2'               => htmlentities($payment->getCcCid()),
            'dps_billing_id'     => $agreement ? $this->_getBillingAgreement()->getReferenceId() : null,
        ];
    }

    /**
     * @return Mage_Sales_Model_Billing_Agreement
     * @throws Mage_Payment_Exception
     */
    protected function _getBillingAgreement()
    {
        if (!$this->getData('billing_agreement')) {
            if (!($id = $this->getPayment()->getBillingAgreementId())) {
                return null;
            }
            $agreement = Mage::getModel('sales/billing_agreement')->loadByEncId($id);
            $this->setData('billing_agreement', $agreement);
        }
        if (!$this->_validateBillingAgreement($this->getData('billing_agreement'))) {
            $this->unsetData('billing_agreement');
            throw new Mage_Payment_Exception('Invalid Credit Card Token');
        }
        return $this->getData('billing_agreement');
    }

    /**
     * Validate billing agreement after loading
     *
     * @param Mage_Sales_Model_Billing_Agreement $agreement
     * @return bool
     */
    protected function _validateBillingAgreement(Mage_Sales_Model_Billing_Agreement $agreement)
    {
        $time = strtotime("midnight", Mage::app()->getLocale()->storeTimeStamp($agreement->getStoreId()));
        $order = $this->_getOrderOrQuote();
        return (
            $agreement->getId()
            && $agreement->getCustomerId()
            && ($agreement->getCustomerId() == $order->getCustomer()->getId())
            && (strtotime($agreement->getExpiresAt()) >= $time)
            && ($agreement->getStatus() == Mage_Sales_Model_Billing_Agreement::STATUS_ACTIVE)
            && ($agreement->getStoreId() == $order->getStoreId())
        );

    }

    /**
     * Should require CVV?
     *
     * @param $storeId
     * @return bool
     */
    protected function _getRequireCvv($storeId = null)
    {
        if (null === $storeId) {
            $storeId = $this->getStore();
        }
        $min = Mage::getStoreConfig('payment/' . $this->getCode() . '/cvv_order_value', $storeId);
        return ($this->_getOrderOrQuote()->getBaseGrandTotal() > $min);
    }

    protected function _getRequestBillingToken()
    {
        return false;
    }

    /**
     * Init billing agreement
     *
     * called from startWizardAction()
     *
     * @param Mage_Payment_Model_Billing_AgreementAbstract $agreement
     */
    public function initBillingAgreementToken(Mage_Payment_Model_Billing_AgreementAbstract $agreement)
    {
        //We're not using a wizard to create the token just yet.
    }

    /**
     * Retrieve billing agreement details
     *
     * Gets other data about the agreement. e.g. from an API.
     *
     * Adds the data to the agreement, and returns the raw data.
     *
     * @param Mage_Payment_Model_Billing_AgreementAbstract $agreement
     */
    public function getBillingAgreementTokenInfo(Mage_Payment_Model_Billing_AgreementAbstract $agreement)
    {
        //We can create another table for credit card details, and look them up here.
        //But we also want the data loaded in a collection, so may be easier to just add a serialized field to ba.
    }

    /**
     * Create billing agreement
     *
     * Called from Mage_Sales_Model_Billing_Agreement::place(), which is called from returnWizardAction
     *
     * @param Mage_Payment_Model_Billing_AgreementAbstract $agreement
     */
    public function placeBillingAgreement(Mage_Payment_Model_Billing_AgreementAbstract $agreement)
    {
        //We're not using a wizard to create the token just yet.
    }

    /**
     * Update billing agreement status
     *
     * When billing agreement is cancelled, we may want to inform the gateway.
     *
     * @param Mage_Payment_Model_Billing_AgreementAbstract $agreement
     */
    public function updateBillingAgreementStatus(Mage_Payment_Model_Billing_AgreementAbstract $agreement)
    {
        //DPS doesn't support cancelling a token. Could just set the reference_id to null so it is lost forever.
    }

}