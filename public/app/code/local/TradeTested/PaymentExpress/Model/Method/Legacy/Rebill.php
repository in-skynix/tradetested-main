<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/03/2016
 * Time: 13:57
 */
class TradeTested_PaymentExpress_Model_Method_Legacy_Rebill extends TradeTested_PaymentExpress_Model_Method_Pxpost
{
    protected $_code = 'foomandpspropxpostrebill';

    /**
     * Check whether there are CC types set in configuration
     *
     * @param Mage_Sales_Model_Quote|null $quote
     * @return bool
     */
    public function isAvailable($quote = null)
    {
        return false;
    }
}