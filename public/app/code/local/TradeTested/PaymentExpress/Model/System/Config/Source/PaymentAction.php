<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/03/2016
 * Time: 14:12
 */
class TradeTested_PaymentExpress_Model_System_Config_Source_PaymentAction
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'value' => Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE,
                'label' => Mage::helper('tradetested_payment_express')->__('Authorize Only')
            ),
            array(
                'value' => Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE,
                'label' => Mage::helper('tradetested_payment_express')->__('Purchase')
            ),
        );
    }
}