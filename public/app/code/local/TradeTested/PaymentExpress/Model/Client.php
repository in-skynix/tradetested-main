<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 25/02/2016
 * Time: 14:52
 */
class TradeTested_PaymentExpress_Model_Client extends Zend_Http_Client
{
    const URL_PXPOST = 'https://sec.paymentexpress.com/pxpost.aspx';
    
    protected static $_underscoreCache = array();

    /**
     * TradeTested_PaymentExpress_Model_Client constructor.
     *
     * @param array $config
     */
    public function __construct($config = array())
    {
        $defaultConfig = array_merge($this->config, array(
            'maxredirects' => 0,
            'timeout'      => 30,
        ));
        $config = array_merge($defaultConfig, $config);
        parent::__construct(self::URL_PXPOST, $config);
    }

    /**
     * @param array|Zend_Config|string $key
     * @param null $value
     * @return mixed
     */
    public function setConfigKey($key, $value = null)
    {
        if (!is_string($key) && ($value === null)) {
            $this->config = $key;
        } else {
            $this->config = array_merge($this->config, array($key => $value));
        }
        return $this;
    }

    /**
     * @param array $config
     * @return $this
     */
    public function mergeConfig($config = array())
    {
        $this->config = array_merge($this->config, $config);
        return $this;
    }

    /**
     * @param null $key
     * @return array|null
     */
    public function getConfig($key = null)
    {
        $config = $this->config;
        if ($key === null) {
            return $config;
        } else {
            return isset($config[$key]) ? $config[$key] : null;
        }
    }

    /**
     * @param TradeTested_PaymentExpress_Model_Client_Request_Abstract $request
     * @return false|TradeTested_PaymentExpress_Model_Client_Response
     * @throws Mage_Payment_Exception
     * @throws Zend_Http_Client_Exception
     */
    public function sendRequest(TradeTested_PaymentExpress_Model_Client_Request_Abstract $request)
    {
        $this->setRawData($this->getXml($request->getRequestData())->asXML());
        try {
            $response = $this->request('POST');
            if ($response->isSuccessful()) {
                return Mage::getModel('tradetested_payment_express/client_response', $this->parseXml($response->getBody()));
            } else {
                throw new Mage_Payment_Exception(
                    'There has been an error processing your payment. Please try later or contact us for help.'
                );
            }
        } catch (Zend_Http_Client_Exception $e) {
            Mage::logException($e);
            throw new Mage_Payment_Exception(
                'There has been an error processing your payment. Please try later or contact us for help.'
            );
        }
    }

    /**
     * @param array $data
     * @param null $existingXml
     * @return null|SimpleXMLElement
     */
    public function getXml($data = array(), &$existingXml = null)
    {
        if ($existingXml == null) {
            $data['post_username'] = $this->getConfig('pxpost_username');
            $data['post_password'] = $this->getConfig('pxpost_password');
            $existingXml = new SimpleXMLElement('<?xml version="1.0"?><Txn></Txn>');
        }
        ksort($data);
        foreach($data as $_key => $_value) {
            if(is_array($_value)) {
                $subnode = $existingXml->addChild($_key);
                $this->getXml($_value, $subnode);
            } else {
                $xmlKey = $this->_camelize($_key);
                $existingXml->addChild("$xmlKey",htmlspecialchars("$_value"));
            }
        }
        return $existingXml;
    }

    /**
     * @param $xmlString
     * @return array|null
     */
    public function parseXml($xmlString)
    {
        $data = json_decode(json_encode((array(simplexml_load_string($xmlString)))), true);
        return $this->_normalizeStruct($data[0]);
    }

    /**
     * @param $data
     * @return array|null
     */
    protected function _normalizeStruct($data)
    {
        if (empty($data)) {
            return null;
        }
        $newData = array();
        foreach ($data as $_key => $_value) {
            $newKey = str_replace('re_co', 'response_code', str_replace('@', '', $this->_underscore($_key)));
            $newData[$newKey] = is_array($_value) ? $this->_normalizeStruct($_value) : $_value;
        }
        return $newData;
    }

    /**
     * @param $name
     * @return mixed|string
     */
    protected function _underscore($name)
    {
        if (isset(self::$_underscoreCache[$name])) {
            return self::$_underscoreCache[$name];
        }
        #Varien_Profiler::start('underscore');
        $result = strtolower(preg_replace('/(.)([A-Z])/', "$1_$2", $name));
        #Varien_Profiler::stop('underscore');
        self::$_underscoreCache[$name] = $result;
        return $result;
    }

    /**
     * @param $name
     * @return mixed
     */
    protected function _camelize($name)
    {
        return str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
    }

}