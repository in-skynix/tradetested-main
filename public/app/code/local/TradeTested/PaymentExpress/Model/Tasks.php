<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/02/2016
 * Time: 15:35
 */
class TradeTested_PaymentExpress_Model_Tasks extends Mage_Core_Model_Abstract
{
    /**
     * Cron job to cancel billing agreements that should have just expired.
     */
    public function cancelExpiredBillingAgreements()
    {
        $timeInTimezone = Mage::getModel('core/date')->timestamp(time());
        $agreements = Mage::getResourceModel('sales/billing_agreement_collection')
            ->addFieldToFilter('status', Mage_Sales_Model_Billing_Agreement::STATUS_ACTIVE)
            ->addFieldToFilter('expires_at', array('to' => date('Y-m-d', $timeInTimezone)));
        /** @var Mage_Sales_Model_Billing_Agreement $_billingAgreement */
        foreach ($agreements as $_billingAgreement) {
            if ($_billingAgreement->canCancel()) {
                $_billingAgreement->cancel();
            }
        }
    }
}