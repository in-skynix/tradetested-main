<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/02/2016
 * Time: 10:45
 */
class TradeTested_PaymentExpress_Model_Client_Request_Authorize 
    extends TradeTested_PaymentExpress_Model_Client_Request_Abstract
{
    const ACTION_AUTHORIZE = 'Auth';

    /**
     * @return array
     */
    protected function _getRequestData()
    {
        $data = $this->_getFilteredData([
            'input_currency', 'enable_add_bill_card', 'card_holder_name', 'card_number', 'date_expiry', 'amount',
            'merchant_reference', 'txn_id', 'cvc2', 'cvc2_presence', 'dps_billing_id'
        ]);
        $data['txn_type'] = self::ACTION_AUTHORIZE;
        return $data;
    }
}
