<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/02/2016
 * Time: 10:45
 */
class TradeTested_PaymentExpress_Model_Client_Request_Capture
    extends TradeTested_PaymentExpress_Model_Client_Request_Abstract
{
    const ACTION_COMPLETE = 'Complete';
    const ACTION_PURCHASE = 'Purchase';

    /**
     * @return array
     */
    protected function _getRequestData()
    {
        if ($this->getDpsTxnRef()) {
            if ($this->getRequireCvc() === null) {
                $this->setRequireCvc(false);
            }
            $data = $this->_getFilteredData(['dps_txn_ref']);
            $data['txn_type'] = self::ACTION_COMPLETE;
        } else {
            $data = $this->_getFilteredData(
                [
                    'input_currency', 'enable_add_bill_card', 'card_holder_name', 'card_number', 'date_expiry',
                    'dps_billing_id'
                ]
            );
            $data['txn_type'] = self::ACTION_PURCHASE;
        }
        return array_merge($data, $this->_getFilteredData(
            ['amount', 'merchant_reference', 'txn_id', 'cvc2', 'cvc2_presence']
        ));
    }
}
