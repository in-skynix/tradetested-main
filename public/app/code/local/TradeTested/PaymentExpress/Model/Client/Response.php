<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/02/2016
 * Time: 13:53
 */
class TradeTested_PaymentExpress_Model_Client_Response extends Varien_Object
{
    /**
     * Error codes returned by DPS that we wish to communicate to customer
     *
     * We don't currently use these, and instead use cardholder response text.
     * @var array
     */
    protected $_errorCodes = [
        '51' => 'Card with Insufficient Funds',
        '54' => 'Expired Card',
        'IC' => 'Invalid Key or Username. Also check that if a TxnId is being supplied that it is unique.',
        'ID' => 'Invalid transaction type. Esure that the transaction type is either Auth or Purchase.',
        'IK' => 'Invalid UrlSuccess. Ensure that the URL being supplied does not contain a query string.',
        'IL' => 'Invalid UrlFail. Ensure that the URL being supplied does not contain a query string.',
        'IM' => 'Invalid PxPayUserId.',
        'IN' => 'Blank PxPayUserId.',
        'IP' => 'Invalid parameter. Ensure that only documented properties are being supplied.',
        'IQ' => 'Invalid TxnType. Ensure that the transaction type being submitted is either "Auth" or "Purchase".',
        'IT' => 'Invalid currency. Ensure that the CurrencyInput is correct and in the correct format e.g. "USD".',
        'IU' => 'Invalid AmountInput. Ensure that the amount is in the correct format e.g. "20.00".',
        'NF' => 'Invalid Username.',
        'NK' => 'Request not found. Check the key and the mcrypt library if in use.',
        'NL' => 'User not enabled. Contact DPS.',
        'NM' => 'User not enabled. Contact DPS.',
        'NN' => 'Invalid MAC.',
        'NO' => 'Request contains non ASCII characters.',
        'NP' => 'PXPay Closing Request tag not found.',
        'NQ' => 'User not enabled for PxPay. Contact DPS.',
        'NT' => 'Key is not 64 characters.',
        'U5' => 'Invalid User / Password',
        'U9' => 'Timeout for Transaction',
        'QD' => 'The transaction was Declined.', //Invalid TxnRef
        'Q4' => 'Invalid Amount Entered. Transaction has not been Approved',
        'Q8' => 'Invalid Currency',
        'QG' => 'Invalid TxnType',
        'QI' => 'Invalid Expiry Date (month not between 1-12)',
        'QJ' => 'Invalid Expiry Date (non numeric value submitted)',
        'QK' => 'Invalid Card Number Length',
        'QL' => 'Invalid Card Number',
        'JC' => 'Invalid BillingId',
        'JD' => 'Invalid DPSBillingId',
        'JE' => 'DPSBillingId not matched',
        'D2' => 'Invalid username',
        'D3' => 'Invalid / missing Password',
        'D4' => 'Maximum number of logon attempts exceeded'
    ];

    /**
     * TradeTested_PaymentExpress_Model_Client_Response constructor.
     * @param array $data
     */
    public function __construct($data = array())
    {
        return parent::__construct($this->_normalizeData($data));
    }

    /**
     * @return bool
     */
    public function getIsSuccess()
    {
        return ($this->getAuthorized() && $this->getSuccess() && ($this->getResponseCode() === '00'));
    }

    /**
     * @return bool
     */
    public function getIsCvcValid()
    {
        return ($this->getCvc2ResultCode() && ($this->getCvc2ResultCode() != 'N'));
    }

    /**
     * @return mixed
     */
    public function getCustomerErrorMessage()
    {
        $msg = ($m = $this->getCardHolderResponseDescription())
            ? preg_replace('/\s\(\d+\)/', '', $m)
            : 'There was an error with the payment gateway. Please try again.';
        return preg_replace('/([^\.])$/', '$1.', $msg);
    }

    /**
     * Extract the data we want from the XML-based schema, and flatten data into one array.
     * @param array $data
     * @return array
     * @throws Mage_Payment_Exception
     */
    protected function _normalizeData($data = array())
    {
        $tx = $data['transaction'] ?? array();
        $responseText = $this->_matchingVals($data['response_text']??null, $tx['attributes']['response_text']??null);
        return [
            'authorized'                       => (($tx['authorized'] ?? null) === '1'),
            'success' => ($this->_matchingVals($data['success']??null, $tx['attributes']['success']??null) === '1'),
            'response_text'                    => $responseText,
            'response_code' => $this->_matchingVals(($data['response_code'] ?? null), ($tx['response_code'] ?? null)),
            'rx_date'                          => $tx['rx_date'] ?? null,
            'rx_date_local'                    => $tx['rx_date_local'] ?? null,
            'local_time_zone'                  => $tx['local_time_zone'] ?? null,
            'merchant_reference'               => $tx['merchant_reference'] ?? null,
            'card_name'                        => $tx['card_name'] ?? null,
            'retry'                            => (($tx['retry'] ?? null) === "1"),
            'status_required'                  => (($tx['status_required'] ?? null) === "1"),
            'auth_code'                        => $tx['auth_code'] ?? null,
            'amount_balance'                   => doubleval($tx['amount_balance'] ?? null),
            'amount'                           => doubleval($tx['amount'] ?? null),
            'input_currency_name'              => $tx['input_currency_name'] ?? null,
            'currency_name'                    => $tx['currency_name'] ?? null,
            'currency_rate'                    => doubleval($tx['currency_rate'] ?? null),
            'card_holder_name'                 => $tx['card_holder_name'] ?? null,
            'date_settlement'                  => $tx['date_settlement'] ?? null,
            'txn_type'                         => $tx['txn_type'] ?? null,
            'card_number'                      => $tx['card_number'] ?? null,
            'txn_mac'                          => $tx['txn_mac'] ?? null,
            'date_expiry'                      => $tx['date_expiry'] ?? null,
            'dps_txn_ref'                      => $tx['dps_txn_ref'] ?? null,
            'txn_ref'                          => $data['txn_ref'] ?? null,
            'rm_reason'                        => $tx['rm_reason'] ?? null,
            'risk_score_text'                  => $tx['risk_score_text'] ?? null,
            'acquirer_id'                      => $tx['acquirer_id'] ?? null,
            'acquirer'                         => $tx['acquirer'] ?? null,
            'acquirer_response_code'           => $tx['acquirer_response_code'] ?? null,
            'card_holder_response_text'        => $tx['card_holder_response_text'] ?? null,
            'card_holder_response_description' => $tx['card_holder_response_description'] ?? null,
            'merchant_response_text'           => $tx['merchant_response_text'] ?? null,
            'merchant_help_text'               => $tx['merchant_help_text'] ?? null,
            'merchant_response_description'    => $tx['merchant_response_description'] ?? null,
            'cvc2_result_code'                 => $tx['cvc2_result_code'] ?? null,
            'acquirer_port'                    => $tx['acquirer_port'] ?? null,
            'allow_retry'                      => (($tx['allow_retry'] ?? null) === '1'),
            'dps_billing_id'                   => $tx['dps_billing_id'] ?? null,
            'billing_id'                       => $tx['billing_id'] ?? null,
            'transaction_id'                   => $tx['transaction_id'] ?? null,
        ];
    }

    /**
     * Get one value from matching values
     *
     * PX returns the same value in many potential places.
     * Get the first, and throw exception if some don't match.
     *
     * @return null
     * @throws Mage_Payment_Exception
     */
    protected function _matchingVals()
    {
        $vals = func_get_args();
        $firstVal = null;
        foreach ($vals as $_val) {
            $firstVal = ($firstVal === null) ? $_val : $firstVal;
            if (($_val !== null) && ($_val !== $firstVal)) {
                throw new Mage_Payment_Exception('Mismatched response values');
            }
        }
        return $firstVal;
    }
}
