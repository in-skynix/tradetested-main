<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/02/2016
 * Time: 10:45
 */
class TradeTested_PaymentExpress_Model_Client_Request_Refund
    extends TradeTested_PaymentExpress_Model_Client_Request_Abstract
{
    const ACTION_REFUND = 'Refund';

    /**
     * @return array
     */
    protected function _getRequestData()
    {
        $data = $this->_getFilteredData(['amount', 'txn_type', 'dps_txn_ref', 'merchant_reference']);
        $data['txn_type'] = self::ACTION_REFUND;
        return $data;
    }
}
