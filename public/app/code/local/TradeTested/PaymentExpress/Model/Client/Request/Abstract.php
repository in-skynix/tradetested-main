<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/02/2016
 * Time: 10:44
 */
abstract class TradeTested_PaymentExpress_Model_Client_Request_Abstract extends Varien_Object
{
    abstract protected function _getRequestData();

    /**
     * Get Data to be sent to DPS
     *
     * @return mixed
     */
    public function getRequestData()
    {
        return $this->_getRequestData();
    }

    /**
     * Get filtered set of data including specified keys only
     *
     * @param array $keys
     * @return array
     */
    protected function _getFilteredData($keys = array())
    {
        $data = array();
        foreach ($keys as $_key) {
            $value = $this->getDataUsingMethod($_key);
            if (($value !== null) && ($value !== '')) {
                $data[$_key] = $value;
            }
        }
        return $data;
    }

    /**
     * @return bool
     */
    public function getEnableAddBillCard()
    {
        return !!($this->getRequestBillingToken());
    }

    /**
     * @return bool
     */
    public function getCvc2Presence()
    {
        return ($this->getRequireCvc() !== false);
    }

    /**
     * @return mixed
     */
    public function getCardNumber()
    {
        return preg_replace("/[^0-9]/", "", $this->getData('card_number'));
    }
}
