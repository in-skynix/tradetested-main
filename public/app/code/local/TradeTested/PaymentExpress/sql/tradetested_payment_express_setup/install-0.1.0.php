<?php
/**
 * @author Dane Lowe
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

$installer->getConnection()->update(
    $this->getTable('sales/billing_agreement'),
    ['method_code' => 'tradetested_px_token'],
    "method_code = 'magebasedpspxpost'"
);

//disable temporarily, as copied from TradeTested_Payment
//$installer->getConnection()
//    ->addColumn(
//        $this->getTable('sales/billing_agreement'),
//        'additional_data',
//        array(
//            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
//            'length'    => '64k',
//            'comment'   => 'Additional Data',
//            'nullable'  => true
//        ));
//$installer->getConnection()
//    ->addColumn(
//        $this->getTable('sales/billing_agreement'),
//        'is_default',
//        array(
//            'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
//            'comment'   => 'Is Default'
//        )
//    );
//$installer->getConnection()
//    ->addColumn(
//        $this->getTable('sales/billing_agreement'),
//        'expires_at',
//        array(
//            'type'      => Varien_Db_Ddl_Table::TYPE_DATE,
//            'comment'   => 'Expires At',
//            'nullable'  => true
//        ));
$installer->endSetup();
