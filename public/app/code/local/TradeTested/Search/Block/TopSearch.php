<?php
class TradeTested_Search_Block_TopSearch extends Mage_Core_Block_Template
{
    /** @var  TradeTested_Search_Helper_Algolia */
    protected $_algoliaSearchHelper;
    /** @var  Integer */
    protected $_storeId;
    
    /** @var  array */
    protected $_jsonConfig;

    /**
     * @return $this
     */
    protected function _construct()
    {
        /** @var TradeTested_Search_Helper_Algolia _algoliaSearchHelper */
        $this->_algoliaSearchHelper = $this->helper('tradetested_search/algolia');
        $this->_storeId = Mage::app()->getStore()->getStoreId();
        return $this;
    }

    /**
     * Get JSON Config for autocomplete JS
     *
     * @return string
     */
    public function getIndexConfigJson()
    {
        //hideIfPage: For all except suggestion, don't show if more than 1 page of results.
        //Looks like _cms is used for autocomplete pages. _pages has all pages.
        
        $helper = $this->_algoliaSearchHelper;
        return [
            $helper->getIndexName('product') => [
                'sortOrder' => 10,
                'header' => 'Products',
                'params' => ['hitsPerPage' => 3],
                'hideIfPage' => true,
                'component' => 'ProductListComponent',
            ],
            $helper->getIndexName('category') => [
                'sortOrder' => 15,
                'header' => 'Categories',
                'params' => ['hitsPerPage' => 3],
                'hideIfPage' => true,
                'component' => 'ResultListComponent',
            ],
            $helper->getIndexName('suggestion') => [
                'header' => 'Suggestions',
                'params' => ['hitsPerPage' => 5],
                'component' => 'SuggestionListComponent',
            ],
            $helper->getIndexName('cms') => [
                'header' => 'Pages',
                'params' => ['hitsPerPage' => 3],
                'hideIfPage' => true,
                'component' => 'ResultListComponent',
            ],
        ];
    }
    
    public function getConfigJson()
    {
        if (!$this->_jsonConfig) {
            $data = [
                'appId'       => $this->_algoliaSearchHelper->getApplicationID(),
                'apiKey'      => $this->_algoliaSearchHelper->getSearchOnlyAPIKey(),
                'indexConfig' => $this->getIndexConfigJson(),
            ];
            $this->_jsonConfig = array_merge( $data, Mage::helper('tradetested_search/data')->getSearchFieldParams()['data'] );
        }
        return $this->_jsonConfig;
    }

    /**
     * @return bool
     */
    public function isSearchEnabled()
    {
        return (
            $this->_algoliaSearchHelper->isEnabled($this->_storeId)
            && count($this->_algoliaSearchHelper->getApplicationID($this->_storeId)) >= 1
        );
    }

    /**
     * @return array
     */
    public function getJsComponentConfig()
    {
        return ['search_autocomplete' => ['name' => 'SearchAutocomplete', 'config' => $this->getConfigJson()]];
    }

}