<?php
/**
 * add all required configration into database. This will delete exsiting configration.
*/
$installer = $this;
$installer->startSetup();
$configTable = $installer->getTable('core_config_data');
$websiteCode = 'default';
$websiteId = '0';

$installer->run("
DELETE FROM core_config_data where path like '%algoliasearch%';
INSERT INTO {$configTable} (`scope`,`scope_id`,`path`,`value`) VALUES ($websiteCode,$websiteId,'algoliasearch/credentials/application_id','RRDW3654WG');
INSERT INTO {$configTable} (`scope`,`scope_id`,`path`,`value`) VALUES ($websiteCode,$websiteId,'algoliasearch/credentials/search_only_api_key','03400c30217f8cbbfd50dc8f062b1171');
INSERT INTO {$configTable} (`scope`,`scope_id`,`path`,`value`) VALUES ($websiteCode,$websiteId,'algoliasearch/credentials/api_key','57cb67603df7651c664357dabd8ed532');
INSERT INTO {$configTable} (`scope`,`scope_id`,`path`,`value`) VALUES ($websiteCode,$websiteId,'algoliasearch/credentials/index_prefix','magento_');
INSERT INTO {$configTable} (`scope`,`scope_id`,`path`,`value`) VALUES ($websiteCode,$websiteId,'algoliasearch/credentials/is_enabled','1');
INSERT INTO {$configTable} (`scope`,`scope_id`,`path`,`value`) VALUES ($websiteCode,$websiteId,'algoliasearch/products/results_limit','1000');
INSERT INTO {$configTable} (`scope`,`scope_id`,`path`,`value`) VALUES ($websiteCode,$websiteId,'algoliasearch/suggestions/suggestion_identification_code','suggestion');
DELETE FROM {$configTable} where path = 'algoliasearch/credentials/mobile_search_form_classname';
UPDATE {$configTable} SET `value`='Search...' where path = 'algoliasearch/credentials/placeholder_content';
UPDATE {$configTable} SET `value`='.search_form .search-input' where path = 'algoliasearch/credentials/search_form_classname';

UPDATE {$configTable} SET `value`='W5D55KF0JM' where path = 'algoliasearch/credentials/application_id';
UPDATE {$configTable} SET `value`='812629b726bd0c94a0ac0ab0037c48bc' where path = 'algoliasearch/credentials/search_only_api_key';
UPDATE {$configTable} SET `value`='a0eba35da7e8fa7ce47afffa71b813dc' where path = 'algoliasearch/credentials/api_key';
");
$installer->endSetup();
