<?php
class TradeTested_Search_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_CATEGORY_ATTRIBUTES      = 'algoliasearch/categories/indexable_attributes';
    const XML_PATH_CMS_ATTRIBUTES           = 'algoliasearch/cms/indexable_attributes';
    const XML_PATH_SEARCHTERM_ATTRIBUTES    = 'algoliasearch/suggestions/indexable_attributes';
    protected $_productAttributes;

    /**
     * Get Category Attributes to Index
     *
     * @param null $storeId
     * @return array|mixed
     */
    public function getCategoryAttributes($storeId = null)
    {
        return explode(',',Mage::getStoreConfig(self::XML_PATH_CATEGORY_ATTRIBUTES, $storeId));
    }

    /**
     * Get CMS (Page) Attributes to Index
     * @param null $storeId
     * @return array
     */
    public function getCmsAttributes($storeId = null)
    {
        return explode(',',Mage::getStoreConfig(self::XML_PATH_CMS_ATTRIBUTES, $storeId));
    }

    /**
     * Get Suggestion Attributes to Index
     *
     * @param null $storeId
     * @return array
     */
    public function getSuggestionAttributes($storeId = null)
    {
        return explode(',',Mage::getStoreConfig(self::XML_PATH_SEARCHTERM_ATTRIBUTES, $storeId));
    }

    /**
     * Return array of all product attributes that can be indexed
     *
     * Based on which attributes are set to searchable in their config.
     *
     * @return array
     */
    public function getAvailableProductAttributes()
    {
        if (is_null($this->_productAttributes)) {
            /** @var Mage_Catalog_Model_Resource_Eav_Attribute $attribute */
            foreach (Mage::getResourceSingleton('catalogsearch/fulltext')->getSearchableAttributes() as $attribute) {
                $this->_productAttributes[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
            }
        }
        return $this->_productAttributes;
    }

    public function getSearchFieldParams()
    {
        /** @var $catalogSearchHelper Mage_CatalogSearch_Helper_Data */
        $catalogSearchHelper = Mage::helper('catalogsearch');
        if (!Mage::helper('tradetested_design/theme')->isMobile()) {
            return [
                'data' => [
                    'placeholder' => 'Search the site',
                    'label'       => 'I\'m looking for:',
                    'inputValue'  => $catalogSearchHelper->getEscapedQueryText(),
                ]
            ];
        } else{
            return [
                'data' => [
                    'placeholder' => 'Search the site',
                    'inputName'   => $catalogSearchHelper->getQueryParamName(),
                    'inputValue'  => $catalogSearchHelper->getEscapedQueryText(),
                    'buttonText'  => ' ',
                    'action'      => Mage::helper('catalogSearch')->getResultUrl(),
                    'maxlength'   => Mage::helper('catalogSearch')->getMaxQueryLength(),
                    'label'       => ' '
                ]
            ];
        }

    }
}