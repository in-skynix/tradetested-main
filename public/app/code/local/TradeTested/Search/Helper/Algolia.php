<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/09/15
 * Time: 09:20
 */
require_once 'AlgoliaSearch/Version.php';
require_once 'AlgoliaSearch/AlgoliaException.php';
require_once 'AlgoliaSearch/ClientContext.php';
require_once 'AlgoliaSearch/Client.php';
require_once 'AlgoliaSearch/Index.php';
class TradeTested_Search_Helper_Algolia extends Mage_Core_Helper_Abstract
{
    const XML_PATH_APPLICATION_ID                   = 'algoliasearch/credentials/application_id';
    const XML_PATH_API_KEY                          = 'algoliasearch/credentials/api_key';
    const XML_PATH_SEARCH_ONLY_API_KEY              = 'algoliasearch/credentials/search_only_api_key';
    const XML_PATH_INDEX_PREFIX                     = 'algoliasearch/credentials/index_prefix';
    const XML_PATH_INDEX_PRODUCT_COUNT              = 'algoliasearch/categories/index_product_count';

    const XML_PATH_RESULTS_LIMIT                    = 'algoliasearch/products/results_limit';
    const XML_PATH_REMOVE_IF_NO_RESULT              = 'algoliasearch/relevance/remove_words_if_no_result';

    /** @var AlgoliaSearch\Client */
    protected $_client;

    /**
     * Is Algolia Search Setup?
     *
     * @param null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null)
    {
        return (Mage::getStoreConfig('catalog/search/engine', $storeId) == 'tradetested_search/engine_algolia');
    }

    /**
     * @param null $storeId
     * @return string
     */
    public function getApplicationID($storeId = NULL)
    {
        return Mage::getStoreConfig(self::XML_PATH_APPLICATION_ID, $storeId);
    }

    /**
     * @param null $storeId
     * @return string
     */
    public function getAPIKey($storeId = NULL)
    {
        return Mage::getStoreConfig(self::XML_PATH_API_KEY, $storeId);
    }

    /**
     * @param null $storeId
     * @return string
     */
    public function getSearchOnlyAPIKey($storeId = NULL)
    {
        return Mage::getStoreConfig(self::XML_PATH_SEARCH_ONLY_API_KEY, $storeId);
    }

    /**
     * @param $storeId
     * @param $entityCode
     * @return \AlgoliaSearch\Index
     * @throws \AlgoliaSearch\AlgoliaException
     */
    public function getIndex($storeId, $entityCode)
    {
        if (!Mage::app()->getStore($storeId)->getIsActive()) {
            return false;
        }
        return $this->getClient()->initIndex($this->getIndexName($entityCode, $storeId));
    }

    /**
     * Get Index Name
     *
     * @param $entityCode
     * @param null $storeId
     * @return string
     */
    public function getIndexName($entityCode, $storeId = null)
    {
        return (string)Mage::getStoreConfig(self::XML_PATH_INDEX_PREFIX, $storeId)
            . Mage::app()->getStore($storeId)->getCode() . '_' . $entityCode;
    }

    /**
     * @return \AlgoliaSearch\Client
     */
    public function getClient()
    {
        if (!$this->_client) {
            $this->_client = new \AlgoliaSearch\Client($this->getApplicationID(), $this->getAPIKey());
        }
        return $this->_client;
    }

    /**
     * Get Search Result Data
     *
     * Originally overloaded to add Category Suggestions Data.
     * I've left the cache function in for now.
     *
     * @param $queryText
     * @param $storeId
     * @return array|mixed
     * @throws Exception
     */
    public function getSearchResult($queryText, $storeId)
    {
        $cache = Mage::app()->getCache();
        $cacheData = $cache->load($this->_getResultCacheKey());
        if ($cacheData != null) {
            return unserialize($cacheData);
        } else {
            $data = $this->_getSearchResult($queryText, $storeId);
            $cache->save(serialize($data), $this->_getResultCacheKey(), array('ALGOLIA_CACHE'), 60 * 60 * 12);
            return $data;
        }
    }

    /**
     * Get array of [product_id => [relevance]
     *
     * @todo Load entire collection from search data. or do so in Javascript.
     *
     * @param $queryText
     * @param $storeId
     * @return array
     * @throws Exception
     */
    protected function _getSearchResult($queryText, $storeId)
    {
        $resultsLimit = Mage::getStoreConfig(self::XML_PATH_RESULTS_LIMIT, $storeId);
        $removeWords =  Mage::getStoreConfig(self::XML_PATH_REMOVE_IF_NO_RESULT, $storeId);

        $answer = $this->getIndex($storeId, 'product')->search($queryText, array(
            'hitsPerPage' => max(5, min($resultsLimit, 1000)), // retrieve all the hits (hard limit is 1000)
            'attributesToRetrieve' => 'objectID',
            'attributesToHighlight' => '',
            'attributesToSnippet' => '',
            'removeWordsIfNoResult'=> $removeWords,
        ));
        $data = array();
        $total = count($answer['hits']);
        foreach ($answer['hits'] as $i => $hit) {
            if ($productId = $hit['objectID']) {
                $data[$productId] = $total - $i;
            }
        }

        return $data;
    }

    /**
     * Get Cache Key
     *
     * @return string
     */
    protected function _getResultCacheKey()
    {
        $query = Mage::app()->getRequest()->getParam('q');
        $storeId = Mage::app()->getStore()->getStoreId();
        if ($query != null && $storeId != null) {
            return "algolia_search_results_".md5($query.$storeId);
        }
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function deleteStoreIndex($storeId = NULL)
    {
        $indexName = (string)Mage::getStoreConfig(self::XML_PATH_INDEX_PREFIX, $storeId)
            . Mage::app()->getStore($storeId)->getCode();
        return $this->getClient()->deleteIndex($indexName);
    }

}