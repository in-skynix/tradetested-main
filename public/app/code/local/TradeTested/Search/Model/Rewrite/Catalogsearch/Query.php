<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 27/07/15
 * Time: 15:40
 */
class TradeTested_Search_Model_Rewrite_Catalogsearch_Query extends Mage_CatalogSearch_Model_Query
{

    /**
     * Set Popularity
     *
     * We only want to increase the popularity of a search term at most once per hour.
     *
     * Can't be done in an observer as Magento has no way of preventing save in a _save_before observer.
     * And we want to prevent changing updated_at
     *
     * @param int $value
     * @return $this
     */
    public function setPopularity($value)
    {
        if ((Mage::getDesign()->getArea() == 'adminhtml') || ((time() - strtotime($this->getUpdatedAt())) > 3600)) {
            parent::setPopularity($value);
        } else {
            $this->setDataChanges(false);
            foreach($this->getData() as $k => $v) {
                if($this->getOrigData($k) != $v) {
                    $this->setDataChanges(true);
                }
            }
        }
        return $this;
    }

    /**
     * Has Changed?
     *
     * Re-check for changes. Something is setting the dirty flag, when we want to avoid changing updated_at
     */
    protected function _hasModelChanged()
    {
        $this->setDataChanges(false);
        foreach($this->getData() as $k => $v) {
            if($this->getOrigData($k) != $v) {
                $this->setDataChanges(true);
            }
        }
        return parent::_hasModelChanged();
    }

}
