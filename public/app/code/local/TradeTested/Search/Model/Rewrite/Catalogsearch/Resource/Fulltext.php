<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/09/15
 * Time: 11:22
 */
class TradeTested_Search_Model_Rewrite_Catalogsearch_Resource_Fulltext
    extends Mage_CatalogSearch_Model_Resource_Fulltext
{
    /**
     * @return array
     */
    public function getSearchableAttributes()
    {
        return $this->_getSearchableAttributes();
    }

    protected function _getProductChildrenIds($productId, $typeId, $websiteId = null)
    {
        return null;
    }

    /**
     * Retrieve searchable products per store
     *
     * @param int $storeId
     * @param array $staticFields
     * @param array|int $productIds
     * @param int $lastProductId
     * @param int $limit
     * @return array
     */
    protected function _getSearchableProducts(
        $storeId, array $staticFields, $productIds = null, $lastProductId = 0, $limit = 100
    ) {
        if (!Mage::helper('tradetested_search/algolia')->isEnabled()) {
            return parent::_getSearchableProducts($storeId, $staticFields, $productIds, $lastProductId, $limit);
        }
        if (!Mage::app()->getStore($storeId)->getIsActive()) {
            return array();
        }
        $websiteId      = Mage::app()->getStore($storeId)->getWebsiteId();
        $writeAdapter   = $this->_getWriteAdapter();

        $collection = Mage::getResourceModel('catalog/product_collection')
            ->setStoreId($storeId)
            ->addWebsiteFilter(array($websiteId))
            ->addAttributeToFilter(
                'status',
                array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            )
            ->setVisibility(array(
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
            ))
        ;
        $catNameAttr = Mage::getSingleton('eav/entity_attribute')
            ->loadByCode(Mage_Catalog_Model_Category::ENTITY, 'name');
        $linkTypeId = Mage_Catalog_Model_Product_Link::LINK_TYPE_CROSSSELL;

        $collection->getSelect()
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns(array_merge(array('entity_id', 'type_id'), $staticFields))
            ->joinLeft(
                array('stock_status' => $this->getTable('cataloginventory/stock_status')),
                $writeAdapter->quoteInto(
                    'stock_status.product_id=e.entity_id AND stock_status.website_id=?',
                    $websiteId
                ),
                array('in_stock' => 'stock_status')
            )
            ->joinLeft(
                array('ci' => $this->getTable('catalog/category_product_index')),
                'ci.product_id = e.entity_id',
                array('category_ids' => new Zend_Db_Expr('GROUP_CONCAT(ci.category_id)'))
            )
            ->joinLeft(
                array('cat_name' => $catNameAttr->getBackendTable()),
                "cat_name.entity_id = ci.category_id AND cat_name.attribute_id = {$catNameAttr->getId()}",
                array('category_names' => new Zend_Db_Expr('GROUP_CONCAT(cat_name.value)'))
            )
            ->joinLeft(
                array('revenue' => $this->getTable('tradetested_catalog/index_product_revenue')),
                'revenue.product_id = e.entity_id',
                array('revenue')
            )
            ->joinLeft(
                array('link' => $this->getTable('catalog/product_link')),
                "link.linked_product_id = e.entity_id AND link.link_type_id = {$linkTypeId}",
                array('is_crosssell' => new Zend_Db_Expr('(link.link_id IS NOT NULL)'))
            )
        ;

        Mage::helper('tradetested_catalog/url')->addRequestPathsToCollection($collection, $storeId);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInSearchFilterToCollection($collection);

        $attrs = array('thumbnail', 'image');
        $collection->addAttributeToSelect($attrs, 'left');

        $select = $collection->getSelect();
        if (!is_null($productIds)) {
            $select->where('e.entity_id IN(?)', $productIds);
        }

        $select->where('e.entity_id>?', $lastProductId)
            ->group('e.entity_id')
            ->limit($limit)
            ->order('e.entity_id');
        $result = $writeAdapter->fetchAll($select);

        return $result;
    }

    /**
     * Prepare Fulltext index value for product
     *
     * @param array $indexData
     * @param array $productData
     * @param int $storeId
     * @return string
     */
    protected function _prepareProductIndex($indexData, $productData, $storeId)
    {
        $data = parent::_prepareProductIndex($indexData, $productData, $storeId);
        if (Mage::helper('tradetested_search/algolia')->isEnabled()) {
            $appEmulation = Mage::getSingleton('core/app_emulation');
            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
            $data['category_ids'] = array_values(array_unique(explode(',', $productData['category_ids'])));
            $data['category_names'] = array_values(array_unique(explode(',', $productData['category_names'])));
            /** @var Mage_Catalog_Model_Product $product */
            $product = Mage::getModel('catalog/product')->setData($productData);
            $data['url'] = $product->getProductUrl();
            $data['thumbnail_url'] = Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(75, 75)->__toString();
            $data['image_url'] = Mage::helper('catalog/image')->init($product, 'image')->resize(800, 600)
                ->__toString();
            $data['type_id'] = $product->getTypeId();
            $data['objectID'] = (int)$product->getId();
            $data['price'] = isset($data['price']) ? (int)$data['price'] : null;
            $data['in_stock'] = isset($data['in_stock']) ? (int)$data['in_stock'] : null;
            //Zend fetchAll seems to just return string, despite columns being decimal
            $data['sale_count'] = isset($data['sale_count']) ? (float)$data['sale_count'] : null;
            $data['revenue'] = (float)$product->getRevenue();
            $data['is_crosssell'] = (boolean)$product->getIsCrosssell();
            $data['revenue_digits'] = strlen(intval($product->getRevenue()));
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        }
        return $data;
    }

    /**
     * Prepare results for query
     *
     * @param Mage_CatalogSearch_Model_Fulltext $object
     * @param string $queryText
     * @param Mage_CatalogSearch_Model_Query $query
     * @return Mage_CatalogSearch_Model_Resource_Fulltext
     */
    public function prepareResult($object, $queryText, $query)
    {
        if (!Mage::helper('tradetested_search/algolia')->isEnabled()) {
            return parent::prepareResult($object, $queryText, $query);
        }
        $this->_foundData = Mage::helper('tradetested_search/algolia')
            ->getSearchResult($queryText, $query->getStoreId());
        return $this;
    }
}