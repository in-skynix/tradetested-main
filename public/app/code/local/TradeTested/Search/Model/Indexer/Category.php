<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/07/15
 * Time: 14:13
 */
class TradeTested_Search_Model_Indexer_Category extends TradeTested_Search_Model_Indexer_Abstract
{
    protected $_matchedEntities = array(
        Mage_Catalog_Model_Category::ENTITY => array(
            Mage_Index_Model_Event::TYPE_SAVE,
            Mage_Index_Model_Event::TYPE_DELETE,
        )
    );

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('tradetested_search/indexer_category');
    }

    /**
     * Get Indexer name
     *
     * @return string
     */
    public function getName()
    {
        return Mage::helper('tradetested_search')->__('Algolia Category');
    }

    /**
     * Get Indexer description
     *
     * @return string
     */
    public function getDescription()
    {
        return Mage::helper('tradetested_search')->__('Reindex Category Suggestions');
    }

    /**
     * Register indexer required data inside event object
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        /** @var $category Mage_Catalog_Model_Category */
        $category = $event->getDataObject();
        $event->addNewData('tradetested_search_category_id', $category->getId());
    }

    /**
     * Process event
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        $data = $event->getNewData();
        $ids = empty($data['tradetested_search_category_id']) ?
            null : array($data['tradetested_search_category_id']);
        $this->reindex($ids);
    }

    /**
     * @param null $ids
     * @return $this
     */
    public function reindex($ids = null)
    {
        return $this->_getResource()->reindex($ids);
    }
}