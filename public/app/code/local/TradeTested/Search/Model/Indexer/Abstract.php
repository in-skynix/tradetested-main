<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/07/15
 * Time: 14:13
 */
abstract class TradeTested_Search_Model_Indexer_Abstract extends Mage_Index_Model_Indexer_Abstract
{

    /**
     * Related Configuration Settings for match (reindex)
     *
     * @var array
     */
    protected $_relatedConfigSettingsReindex = array(
        Mage_CatalogSearch_Model_Fulltext::XML_PATH_CATALOG_SEARCH_TYPE,
        TradeTested_Search_Helper_Data::XML_PATH_CATEGORY_ATTRIBUTES,
        TradeTested_Search_Helper_Algolia::XML_PATH_INDEX_PREFIX,
        TradeTested_Search_Helper_Algolia::XML_PATH_INDEX_PRODUCT_COUNT
    );

    /**
     * Related Configuration Settings for match (update settings)
     *
     * @var array
     */
    protected $_relatedConfigSettingsUpdate = array(
    );

}