<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/07/15
 * Time: 14:13
 */
class TradeTested_Search_Model_Indexer_Cms extends TradeTested_Search_Model_Indexer_Abstract
{
    /**
     * Data key for matching result to be saved in
     */
    const EVENT_MATCH_RESULT_KEY = 'tradetested_search_index_cms';

    protected $_matchedEntities = array(
        'cms_page' => array(
            Mage_Index_Model_Event::TYPE_SAVE,
            Mage_Index_Model_Event::TYPE_DELETE,
            Mage_Index_Model_Event::TYPE_MASS_ACTION,
            Mage_Index_Model_Event::TYPE_REINDEX
        )
    );

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('tradetested_search/indexer_cms');
    }

    /**
     * Get Indexer name
     *
     * @return string
     */
    public function getName()
    {
        return Mage::helper('tradetested_search')->__('Algolia CMS');
    }

    /**
     * Get Indexer description
     *
     * @return string
     */
    public function getDescription()
    {
        return Mage::helper('tradetested_search')->__('Reindex CMS Suggestions');
    }

    /**
     * Register indexer required data inside event object
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        $event->addNewData(self::EVENT_MATCH_RESULT_KEY, true);
        if ($event->getEntity() == 'cms_page') {
            $page = $event->getDataObject();
            $event->addNewData('tradetested_search_index_cms_page_id', $page->getId());
        }
    }

    /**
     * Process event
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        $data = $event->getNewData();
        $pageIds = empty($data['tradetested_search_index_cms_page_id']) ?
            null : array($data['tradetested_search_index_cms_page_id']);
        $this->reindex($pageIds);
    }

    /**
     * @param null $pageIds
     * @return $this
     */
    public function reindex($pageIds = null)
    {
        return $this->_getResource()->reindex($pageIds);
    }
}