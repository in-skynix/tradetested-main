<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/07/15
 * Time: 14:13
 */
class TradeTested_Search_Model_Indexer_Suggestion extends TradeTested_Search_Model_Indexer_Abstract
{
    /**
     * Data key for matching result to be saved in
     */
    const EVENT_MATCH_RESULT_KEY = 'tradetested_search_index_suggestion';

    protected $_matchedEntities = array(
        //We don't reindex on any query save, otherwise it would be indexing whenever a search is made.
    );

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('tradetested_search/indexer_suggestion');
    }

    /**
     * Get Indexer name
     *
     * @return string
     */
    public function getName()
    {
        return Mage::helper('tradetested_search')->__('Algolia Search Suggestion');
    }

    /**
     * Get Indexer description
     *
     * @return string
     */
    public function getDescription()
    {
        return Mage::helper('tradetested_search')->__('Reindex Search Suggestions');
    }

    /**
     * Register indexer required data inside event object
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        $event->addNewData(self::EVENT_MATCH_RESULT_KEY, true);
    }

    /**
     * Process event
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        $this->reindex();
    }

    /**
     * @return $this
     */
    public function reindex()
    {
        return $this->_getResource()->reindex();
    }
}