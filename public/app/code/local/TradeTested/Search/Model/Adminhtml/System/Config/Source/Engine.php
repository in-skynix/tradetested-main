<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/09/15
 * Time: 09:04
 */
class TradeTested_Search_Model_Adminhtml_System_Config_Source_Engine
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $engines = array(
            'catalogsearch/fulltext_engine' => Mage::helper('adminhtml')->__('MySQL'),
            'tradetested_search/engine_algolia' => Mage::helper('adminhtml')->__('Algolia'),
        );
        $options = array();
        foreach ($engines as $k => $v) {
            $options[] = array(
                'value' => $k,
                'label' => $v
            );
        }
        return $options;
    }
}