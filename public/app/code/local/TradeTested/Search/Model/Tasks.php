<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 31/07/15
 * Time: 11:19
 */
class TradeTested_Search_Model_Tasks
{
    /**
     * Index suggestions daily via cron, rather than every time someone searches.
     */
    public function dailyIndex()
    {
        Mage::getResourceModel('tradetested_search/indexer_suggestion')->reindex(strtotime('-2 days'));
    }
}
