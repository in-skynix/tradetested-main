<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 07/10/2015
 * Time: 10:37
 */
class TradeTested_Search_Model_System_Config_Source_SuggestionAttributes
{
    public function toOptionArray()
    {
        $arr = array();
        foreach ($this->toArray() as $_key => $_label) {
            $arr[] = array('value' => $_key, 'label' => $_label);
        }
        return $arr;
    }

    public function toArray()
    {
        $helper = Mage::helper('tradetested_search');
        return array (
            'query_id'      => $helper->__('Query Id'),
            'query_text'    => $helper->__('Query Text'),
            'num_results'   => $helper->__('Number of Results'),
            'popularity'    => $helper->__('Popularity'),
            'updated_at'    => $helper->__('Updated Date')
        );
    }
}