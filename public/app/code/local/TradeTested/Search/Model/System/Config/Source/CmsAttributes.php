<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 07/10/2015
 * Time: 10:37
 */
class TradeTested_Search_Model_System_Config_Source_CmsAttributes
{
    public function toOptionArray()
    {
        $arr = array();
        foreach ($this->toArray() as $_key => $_label) {
            $arr[] = array('value' => $_key, 'label' => $_label);
        }
        return $arr;
    }

    public function toArray()
    {
        $helper = Mage::helper('tradetested_search');
        return array (
            'page_id'           => $helper->__('Page ID'),
            'title'             => $helper->__('Page Title'),
            'meta_keywords'     => $helper->__('Page Meta Keywords'),
            'meta_description'  => $helper->__('Page Meta Description'),
            'identifier'        => $helper->__('Page Identifier'),
            'content_heading'   => $helper->__('Page Content Heading'),
            'content'           => $helper->__('Page Content'),
            'creation_time'     => $helper->__('Creation Time'),
            'update_time'       => $helper->__('Updated Date'),
            'url'               => $helper->__('Page Url')
        );
    }
}