<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 07/10/2015
 * Time: 10:37
 */
class TradeTested_Search_Model_System_Config_Source_CategoryAttributes
{
    public function toOptionArray()
    {
        $arr = array();
        foreach ($this->toArray() as $_key => $_label) {
            $arr[] = array('value' => $_key, 'label' => $_label);
        }
        return $arr;
    }

    public function toArray()
    {
        $arr = array();
        /** @var $config Mage_Eav_Model_Config */
        $config = Mage::getSingleton('eav/config');
        $allAttributes = $config->getEntityAttributeCodes('catalog_category');
        $categoryAttributes = array_merge($allAttributes, array('product_count'));
        $excludedAttributes = array(
            'all_children', 'available_sort_by', 'children', 'children_count', 'custom_apply_to_products',
            'custom_design', 'custom_design_from', 'custom_design_to', 'custom_layout_update',
            'custom_use_parent_settings', 'default_sort_by', 'display_mode', 'filter_price_range',
            'global_position', 'image', 'include_in_menu', 'is_active', 'is_always_include_in_menu', 'is_anchor',
            'landing_page', 'level', 'lower_cms_block', 'page_layout', 'path_in_store', 'position', 'small_image',
            'thumbnail', 'url_key', 'url_path', 'visible_in_menu'
        );
        $categoryAttributes = array_diff($categoryAttributes, $excludedAttributes);

        foreach ($categoryAttributes as $attributeCode) {
            $label = $config->getAttribute('catalog_category', $attributeCode)->getFrontendLabel();
            $arr[$attributeCode] = $label ? $label : $attributeCode;
        }
        return $arr;
    }
}