<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/09/15
 * Time: 08:59
 */
class TradeTested_Search_Model_Resource_Engine_Algolia
{

    /**
     * @var TradeTested_Search_Helper_Algolia
     */
    private $_helper;

    public function __construct()
    {
        $this->_helper = Mage::helper('tradetested_search/algolia');
    }

    /**
     * Define if engine is available
     *
     * @todo: test connection to algolia
     *
     * @return bool
     */
    public function test()
    {
        return
            $this->_helper->getApplicationID()
            && $this->_helper->getAPIKey()
            && $this->_helper->getSearchOnlyAPIKey();
    }

    /**
     * Retrieve allowed visibility values for current engine
     *
     * @return array
     */
    public function getAllowedVisibility()
    {
        return Mage::getSingleton('catalog/product_visibility')->getVisibleInSearchIds();
    }

    /**
     * Define if current search engine supports advanced index
     *
     * @return bool
     */
    public function allowAdvancedIndex()
    {
        return false;
    }

    /**
     * Multi add entities data to fulltext search table
     *
     * @param int $storeId
     * @param array $entityIndexes
     * @param string $entity 'product'|'cms'
     * @return Mage_CatalogSearch_Model_Resource_Fulltext_Engine
     */
    public function saveEntityIndexes($storeId, $entityIndexes, $entity = 'product')
    {
        if ($entityIndexes && ($index = $this->_helper->getIndex((int)$storeId, $entity))) {
            $index->addObjects($entityIndexes);
        }
        return $this;
    }

    /**
     * Prepare index array. Array values will be converted to a string glued by separator.
     *
     * @param array $index
     * @param string $separator
     * @return string
     */
    public function prepareEntityIndex($index, $separator = ' ')
    {
        foreach ($index as $key => $value) {
            if (is_array($value) && ! empty($value)) {
                $index[$key] = join($separator, array_unique(array_filter($value)));
            } else if (empty($index[$key])) {
                unset($index[$key]);
            }
        }
        return $index;
    }

    /**
     * @todo Atomic reindex by creating temporary index and moving
     *
     * @param null $storeId
     * @param null $entityId
     * @param string $entity
     * @return $this
     */
    public function cleanIndex($storeId = null, $entityId = null, $entity = 'product')
    {
        if ($storeId == NULL) {
            $storeIds = array();
            foreach (Mage::app()->getStores() as $store) {
                if ($store->getIsActive()) {
                    $storeIds[] = $store->getId();
                }
            }
        } else {
            $storeIds = array($storeId);
        }

        $data = (is_array($entityId) || is_null($entityId)) ? $entityId : array($entityId);
        foreach ($storeIds as $storeId) {
            if ($index = $this->_helper->getIndex($storeId, $entity)) {
                if (is_null($entityId)) {
                    $index->clearIndex();
                } else {
                    $index->deleteObjects($data);
                }
            }
        }
        return $this;
    }
}