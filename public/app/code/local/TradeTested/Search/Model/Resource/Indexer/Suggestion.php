<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/07/15
 * Time: 16:23
 */
class TradeTested_Search_Model_Resource_Indexer_Suggestion extends TradeTested_Search_Model_Resource_Indexer_Abstract
{
    protected $_indexEntityCode = 'suggestion';

    /**
     * Reindex all suggestions
     *
     * If timestamp is given will update all queries updated since timestamp.
     * If not given, will clear and rebuild the index from queries updated in the last year.
     *
     * @param int|null $timestamp Timestamp from which time to pick updated records
     * @return $this
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    public function reindex($timestamp = null)
    {
        foreach (Mage::app()->getStores() as $store) {
            /** @var $store Mage_Core_Model_Store */
            if ($store->getIsActive()) {
                $this->setStoreId($store->getId());
                $collection = $this->_getSuggestionCollection($timestamp);
                if ($collection->getSize() > 0) {
                    $replace = $timestamp ? array() : null;
                    $this->updateIndex($collection, $replace);
                }
            } else {
                $this->_getAlgoliaHelper()->deleteStoreIndex($store->getId());
            }
        }
        Mage::getSingleton('catalogsearch/fulltext')->resetSearchResults();
        return $this;
    }

    /**
     * Get collection of Queries
     *
     * @param int|null $timestamp Timestamp from which time to pick updated records
     * @return Mage_CatalogSearch_Model_Resource_Query_Collection
     */
    protected function _getSuggestionCollection($timestamp = null)
    {
        $storeId = $this->_storeId;
        $popularity = Mage::getStoreConfig('algoliasearch/suggestions/suggestion_identification_popularity', $storeId);
        if ($popularity == null) {
            $popularity = 0;
        }
        $timestamp = $timestamp ? $timestamp : strtotime('-1 year');
        $date = Mage::getModel('core/date')->date(null, $timestamp);

        $collection = Mage::getModel('catalogsearch/query')
            ->getCollection()
            ->addStoreFilter($storeId)
            ->addFieldToFilter('is_active', array('eq' => '1'))
            ->addFieldToFilter('num_results', array('gt' => '0'))
            ->addFieldToFilter('popularity', array('gteq' => $popularity))
            ->addFieldToFilter('updated_at', array('from' => $date));
        $collection->getSelect()
            ->group('query_text')
            ->where('CHARACTER_LENGTH(main_table.query_text) > ?', 2);
        return $collection;
    }

    /**
     * Extract Collection Data
     *
     * @param $collection
     * @return array
     */
    protected function _extractCollectionData($collection)
    {
        $data = array();
        foreach ($collection as $_query) {
            $data[] = $this->_extractQueryData($_query);
        }
        return $data;
    }

    /**
     * Extract Data From Query
     *
     * @param Mage_CatalogSearch_Model_Query $query
     * @return array
     */
    protected function _extractQueryData(Mage_CatalogSearch_Model_Query $query)
    {
        $data = array('objectID' => 'suggestion_' . $query->getId());
        foreach (Mage::helper('tradetested_search')->getSuggestionAttributes($this->_storeId) as $attr) {
            if (!isset($data[$attr]) && !is_null($query->getData($attr))) {
                $value = $query->getData($attr);
                if (is_numeric($value)) {
                    $value = (int) $value;
                }
                $data[$attr] = $value;
            }
        }
        return $data;
    }
}