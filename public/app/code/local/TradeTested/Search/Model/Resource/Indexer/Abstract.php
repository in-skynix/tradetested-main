<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/07/15
 * Time: 16:23
 */
abstract class TradeTested_Search_Model_Resource_Indexer_Abstract extends Mage_Core_Model_Abstract
{
    /** @var  TradeTested_Search_Helper_Algolia */
    protected $_algoliaHelper;
    protected $_indexes = array();
    protected $_indexEntityCode;
    protected $_storeId;

    /**
     * Update Index From Collection
     *
     * @todo: process collection by page
     * @param $collection
     * @param null $idsToReplace
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    public function updateIndex($collection, $idsToReplace = null)
    {
        if (!$this->_storeId) {
            throw new Mage_Core_Exception('Please set a store ID first');
        }
        $index = $this->_getIndex();
        $data = $this->_extractCollectionData($collection);
        try {
            if (count($data) > 0) {
                if ($idsToReplace) {
                    $index->deleteObjects($idsToReplace);
                } else {
                    $index->clearIndex();
                }
                $index->addObjects($data);
            }
        } catch (Exception $e)
        {
            throw $e;
        }
    }

    public function reindexAll()
    {
        $this->reindex();
    }

    /**
     * Set Store ID to operate on
     *
     * @param int $storeId
     */
    public function setStoreId($storeId)
    {
        $this->_storeId = $storeId;
    }

    /**
     * Extract array of data from collection
     *
     * @param $collection
     * @return array
     */
    abstract protected function _extractCollectionData($collection);

    /**
     * Get Algolia Index
     *
     * @return AlgoliaSearch\Index
     */
    protected function _getIndex()
    {
        $storeId = $this->_storeId;
        if (!isset($this->_indexes[$storeId])) {
            $this->_indexes[$storeId] = $this->_getAlgoliaHelper()->getIndex($storeId, $this->_indexEntityCode);
        }
        return $this->_indexes[$storeId];
    }

    /**
     * Get AlgoliaSearch Helper
     *
     * @return TradeTested_Search_Helper_Algolia
     */
    protected function _getAlgoliaHelper()
    {
        if (!$this->_algoliaHelper) {
            $this->_algoliaHelper = Mage::helper('tradetested_search/algolia');
        }
        return $this->_algoliaHelper;
    }

}