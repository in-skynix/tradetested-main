<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/07/15
 * Time: 16:23
 */
class TradeTested_Search_Model_Resource_Indexer_Cms extends TradeTested_Search_Model_Resource_Indexer_Abstract
{
    protected $_indexEntityCode = 'cms';

    /**
     * Reindex all or given pages
     *
     * @param null $pageIds
     * @return $this
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    public function reindex($pageIds = null)
    {
        foreach (Mage::app()->getStores() as $store) {
            /** @var $store Mage_Core_Model_Store */
            if ($store->getIsActive()) {
                $this->setStoreId($store->getId());
                $collection = $this->_getPageCollection($pageIds);
                if ($collection->getSize() > 0) {
                    $this->updateIndex($collection, $pageIds);
                }
            } else {
               $this->_getAlgoliaHelper()->deleteStoreIndex($store->getId());
            }
        }
        Mage::getSingleton('catalogsearch/fulltext')->resetSearchResults();
        return $this;
    }

    /**
     * @param $pageIds
     * @return Mage_Cms_Model_Resource_Page_Collection
     */
    protected function _getPageCollection($pageIds)
    {
        $collection = Mage::getModel('cms/page')->getCollection()
            ->addStoreFilter($this->_storeId, false)
            ->addFieldToFilter('is_active', array('eq' => '1'));
        if ($customPages = Mage::getStoreConfig('algoliasearch/cms/cms_index_page_ids', $this->_storeId)) {
            $collection->addFieldToFilter('page_id', array('in' => explode(",", $customPages)));
        }
        if ($pageIds) {
            $collection->addFieldToFilter('page_id', array('in' => $pageIds));
        }
        return $collection;
    }

    /**
     * Extract Collection Data
     *
     * @param $collection
     * @return array
     */
    protected function _extractCollectionData($collection)
    {
        $data = array();
        /** @var Mage_Cms_Model_Page $_page */
        foreach ($collection as $_page) {
            if (!is_null(Mage::helper('cms/page')->getPageUrl($_page->getPageId()))) {
                $data[] = $this->_extractPageData($_page);
            }
        }
        return $data;
    }

    /**
     * Extract Data From CMS Page
     *
     * @param Mage_CMS_Model_Page $page
     * @return array
     * @throws Exception
     */
    protected function _extractPageData(Mage_CMS_Model_Page $page)
    {
        $helper = Mage::helper('cms');
        $processor = $helper->getPageTemplateProcessor();
        $url = Mage::getModel('core/url')->setStore($this->_storeId)
            ->getUrl(null, array('_direct' => $page->getIdentifier()));
        $data = array(
            'objectID'      => 'page_' . $page->getId(),
            'url'           => $url,
            '_tags'         => array('cms'),
            'popularity'    => 1,
        );
        foreach (Mage::helper('tradetested_search')->getCmsAttributes($this->_storeId) as $attr) {
            if (!isset($data[$attr]) && !is_null($page->getData($attr))) {
                if ($attr == 'content') {
                    //Render {{block}} tag in cms page.
                    $html = $processor->filter($page->getContent());
                    $data['content'] = $html;
                } else {
                    $value = $page->getData($attr);
                    if (is_numeric($value)) {
                        $value = (int) $value;
                    }
                    $data[$attr] = $value;
                }
            }
        }
        return $data;
    }
}