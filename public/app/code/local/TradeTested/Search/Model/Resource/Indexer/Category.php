<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 29/07/15
 * Time: 16:23
 */
class TradeTested_Search_Model_Resource_Indexer_Category extends TradeTested_Search_Model_Resource_Indexer_Abstract
{
    protected $_indexEntityCode = 'category';

    protected $_categoryNames = array();
    protected $_rootCategoryId = -1;

    /**
     * Reindex all or given categories
     *
     * @param null|array $ids
     * @return $this
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    public function reindex($ids = null)
    {
        foreach (Mage::app()->getStores() as $store) {
            /** @var $store Mage_Core_Model_Store */
            if ($store->getIsActive()) {
                $this->setStoreId($store->getId());
                $appEmulation = Mage::getSingleton('core/app_emulation');
                $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($store->getId());
                $collection = $this->_getCategoryCollection($ids);
                if ($collection->getSize() > 0) {
                    $this->updateIndex($collection, $ids);
                }
                $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            } else {
               $this->_getAlgoliaHelper()->deleteStoreIndex($store->getId());
            }
        }
        Mage::getSingleton('catalogsearch/fulltext')->resetSearchResults();
        return $this;
    }

    /**
     * @param $ids
     * @return Mage_Catalog_Model_Resource_Category_Collection
     */
    protected function _getCategoryCollection($ids = null)
    {
        $storeRootCategoryPath = sprintf(
            '%d/%d',
            $this->getRootCategoryId(),
            Mage::app()->getStore($this->_storeId)->getRootCategoryId()
        );

        /** @var $categories Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection */
        $collection = Mage::getResourceModel('catalog/category_collection');

        $attrs = array('name') + Mage::helper('tradetested_search')->getCategoryAttributes($this->_storeId);
        $collection
            ->setStoreId($this->_storeId)
            ->setLoadProductCount(true)
            ->addPathFilter($storeRootCategoryPath)
            ->addNameToResult()
            ->addUrlRewriteToResult()
            ->addIsActiveFilter()
            ->addAttributeToSelect($attrs)
            ->addFieldToFilter('level', array('gt' => 1));
        if ($ids) {
            $collection->addFieldToFilter('entity_id', array('in' => $ids));
        }
        return $collection;
    }

    /**
     * Extract Collection Data
     *
     * @param $collection
     * @return array
     */
    protected function _extractCollectionData($collection)
    {
        $data = array();
        /** @var Mage_Cms_Model_Page $_page */
        foreach ($collection as $_category) {
            if($_category->getProductCount()) {
                $data[] = $this->_extractCategoryData($_category);
            }
        }
        return $data;
    }

    /**
     * Extract Data From Category
     *
     * @param Mage_Catalog_Model_Category $category
     * @return array
     */
    protected function _extractCategoryData(Mage_Catalog_Model_Category $category)
    {
        $path = '';

        foreach ($category->getPathIds() as $categoryId) {
            if ($path != '') {
                $path .= ' > ';
            }
            $path .= $this->_getCategoryName($categoryId);
        }
        $data = array(
            'objectID'      => $category->getId(),
            'name'          => $category->getName(),
            'path'          => $path,
            'level'         => $category->getLevel(),
            'url'           => $category->getUrl(),
            '_tags'         => array('category'),
            'popularity'    => 1,
            'product_count' => $category->getProductCount()
        );
        foreach (Mage::helper('tradetested_search')->getCategoryAttributes($this->_storeId) as $attr) {
            if (!isset($data[$attr]) && !is_null($category->getData($attr))) {
                $value = $category->getData($attr);
                if (is_numeric($value)) {
                    $value = (int) $value;
                }
                $data[$attr] = $value;
            }
        }
        return $data;
    }

    /**
     * Proxy for category names
     *
     * @param Mage_Catalog_Model_Category|int $categoryId
     * @return null|string
     */
    protected function _getCategoryName($categoryId)
    {
        if ($categoryId instanceof Mage_Catalog_Model_Category) {
            $categoryId = $categoryId->getId();
        }
        $categoryId = intval($categoryId);
        $storeId = intval($this->_storeId);

        if (empty($this->_categoryNames)) {
            $this->_categoryNames = array();
            /** @var $resource Mage_Catalog_Model_Resource_Category */
            $resource = Mage::getResourceModel('catalog/category');
            if ($attribute = $resource->getAttribute('name')) {
                /** @var $connection Varien_Db_Adapter_Pdo_Mysql */
                $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
                $select = $connection->select()
                    ->from(array('backend' => $attribute->getBackendTable()), array(new Zend_Db_Expr("CONCAT(backend.store_id, '-', backend.entity_id)"), 'backend.value'))
                    ->join(array('category' => $resource->getTable('catalog/category')), 'backend.entity_id = category.entity_id', array())
                    ->where('backend.entity_type_id = ?', $attribute->getEntityTypeId())
                    ->where('backend.attribute_id = ?', $attribute->getAttributeId())
                    ->where('category.level > ?', 1);
                $this->_categoryNames = $connection->fetchPairs($select);
            }
        }

        $categoryName = null;
        $key = $storeId.'-'.$categoryId;
        if (isset($this->_categoryNames[$key])) { // Check whether the category name is present for the specified store
            $categoryName = strval($this->_categoryNames[$key]);
        } elseif ($storeId != 0) { // Check whether the category name is present for the default store
            $key = '0-'.$categoryId;
            if (isset($this->_categoryNames[$key])) {
                $categoryName = strval($this->_categoryNames[$key]);
            }
        }

        return $categoryName;
    }

    /**
     * Retrieve root category id
     *
     * @return int
     */
    public function getRootCategoryId()
    {
        if (-1 === $this->_rootCategoryId) {
            $collection = Mage::getResourceModel('catalog/category_collection');
            $collection->addFieldToFilter('parent_id', 0);
            $collection->getSelect()->limit(1);
            $rootCategory = $collection->getFirstItem();
            $this->_rootCategoryId = $rootCategory->getId();
        }
        return $this->_rootCategoryId;
    }
}