<?php
class TradeTested_Search_Model_Observer extends Mage_Core_Model_Session_Abstract
{
    /**
     * Reindex on CMS Page Save
     *
     * Hook on order place to allow reindexing of page data after save
     * @param $observer
     */
    public function reindexCmsPageSave($observer)
    {
        $page = $observer->getEvent()->getObject();
        Mage::getSingleton('index/indexer')->processEntityAction(
            $page, 'cms_page', Mage_Index_Model_Event::TYPE_SAVE
        );
    }

    /**
     * Reindex on CMS Page Save
     *
     * Hook on order place to allow reindexing of page data after save
     * @param $observer
     */
    public function reindexCmsPageDelete($observer)
    {
        $page = $observer->getEvent()->getObject();
        Mage::getSingleton('index/indexer')->processEntityAction(
            $page, 'cms_page', Mage_Index_Model_Event::TYPE_DELETE
        );
    }
}