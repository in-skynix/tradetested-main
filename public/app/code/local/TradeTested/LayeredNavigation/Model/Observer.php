<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 08/12/14
 * Time: 14:49
 */
class TradeTested_LayeredNavigation_Model_Observer
{
    public function adminhtmlBlockHtmlBefore(Varien_Event_Observer $observer)
    {
        /** @var Mage_Adminhtml_Block_Template $block */
        $block = $observer->getEvent()->getBlock();
        // Has no name in layout.
        if(get_class($block) == 'Mage_Adminhtml_Block_Catalog_Product_Attribute_Edit_Tab_Main') {
            /** @var Varien_Data_Form_Element_Fieldset $fieldset */
            $fieldset = $block->getForm()->getElement('front_fieldset');
            $fieldset->addType(
                'icon',
                Mage::getConfig()->getBlockClassName('tradetested_layered_navigation/adminhtml_field_icon')
            );
            $fieldset->addField('icon_code', 'icon', array(
                'name'  => 'icon_code',
                'label' => Mage::helper('catalog')->__('Icon Code'),
                'title' => Mage::helper('catalog')->__('Icon Code in Layered Navigation'),
                'value' => $block->getAttributeObject()->getIconCode(),
                'values'=> Mage::helper('tradetested_layered_navigation')->getIconOptionsArray()
            ));
        }
    }
}