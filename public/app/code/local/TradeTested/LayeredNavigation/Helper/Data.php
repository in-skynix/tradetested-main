<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 08/12/14
 * Time: 15:55
 */
class TradeTested_LayeredNavigation_Helper_Data extends Mage_Core_Helper_Data
{
    const ICON_PREFIX = 'layered-nav-';

    public function getIconOptionsArray()
    {
        $options = array();
        $prefixLength = strlen(self::ICON_PREFIX);
        foreach(Mage::helper('tradetested_design/icon')->getAllOptions() as $_option) {
            if ((strlen($_option) > $prefixLength) && (strrpos($_option, self::ICON_PREFIX, -$prefixLength) !== FALSE)) {
                $options[] = array('value' => $_option, 'label' => $_option);
            }
        }
        return $options;
    }
}
  