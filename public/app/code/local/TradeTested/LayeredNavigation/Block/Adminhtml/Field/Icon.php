<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 08/12/14
 * Time: 15:21
 */
class TradeTested_LayeredNavigation_Block_Adminhtml_Field_Icon extends Varien_Data_Form_Element_Radios
{
    public function __construct($attributes=array())
    {
        parent::__construct($attributes);
        $this->setData('class', 'icon');
    }
    protected function _optionToHtml($option, $selected)
    {

        $html = '<span class="icon_radio"><input type="radio"'.$this->serialize(array('name', 'class', 'style'));
        if (is_array($option)) {
            $option = new Varien_Object($option);
        }
        $html.= 'id="'.$this->getHtmlId().$option->getValue().'"'.$option->serialize(array('label', 'title', 'value', 'class', 'style'));
        if ($option->getValue() == $selected) {
            $html.= ' checked="checked"';
        }
        $html.= ' />';
        $html.= '<label class="icon" for="'.$this->getHtmlId().$option->getValue().'">'.Mage::helper('tradetested_design/icon')->getEntityCode($option->getValue()).'</label></span>';
        $html.= $this->getSeparator() . "\n";
        return $html;
    }
}