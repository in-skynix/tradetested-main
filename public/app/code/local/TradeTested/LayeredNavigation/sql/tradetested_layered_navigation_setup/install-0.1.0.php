<?php
/**
 * @author Dane Lowe
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$installer->getConnection()
    ->addColumn(
        $this->getTable('eav/attribute'),
        'icon_code',
        array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length'    => 255,
            'comment'   => 'Icon Code',
            'nullable'  => false,
            'default'   => 'layered-nav-default'
        ));
$installer->endSetup();