<?php
class TradeTested_StoreOpen_Block_Time extends Mage_Core_Block_Template
{
    /**
     * get store open status
     *
     * @return bool
     */
    public function getIsStoreOpen()
    {
        $helper = Mage::helper('tradetested_store_open');
        $time = $helper->getTime();
        $data = $this->_getConfigData();
        $data = $data[$time->get(Zend_Date::WEEKDAY_8601)];
        return (
            isset($data['on'])
            && (int)$data['on']
            && ($time > $helper->getTime($data['from']))
            && ($time < $helper->getTime($data['to']))
        );
    }

    /**
     * Get Data for display in table
     *
     * @param string $format
     * @return array
     */
    public function getStoreOpeningData($format = 'h:mm a')
    {
        $helper = Mage::helper('tradetested_store_open');
        $data = $this->_getConfigData();
        $output = array();
        foreach ($helper->getDaysOfWeek() as $_i => $_dayName) {
            $output[$_dayName] = array(
                'status'        => isset($data[$_i]['on']) ? $data[$_i]['on'] : '0',
                'open_time'     => $helper->getTime($data[$_i]['from'])->toString($format),
                'close_time'    => $helper->getTime($data[$_i]['to'])->toString($format)
            );
        }
        return $output;
    }

    /**
     * @return mixed
     */
    protected function _getConfigData()
    {
        return unserialize(Mage::getStoreConfig($this->getConfigPath()));
    }
}
