<?php
class TradeTested_StoreOpen_Block_Adminhtml_Field_OpeningConfig extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /**
     * Get Element HTML
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        $html = <<< HTML
<style type="text/css">
.opening_config_field select.select{width: 50px !important;}
.entry-edit .opening_config_field .field-row label{width: 50px !important;}
</style>
HTML;
        $html .= '<div class="opening_config_field">';
        $value = $element->getValue();
        foreach (Mage::helper('tradetested_store_open')->getDaysOfWeek() as $_i => $_name) {
            $html .= "<h3 style=\"margin-top:16px\">{$_name}</h3>";
            $onField = new Varien_Data_Form_Element_Checkbox();
            $onField
                ->setForm($this->getForm())
                ->setName($element->getName()."[{$_i}][on]")
                ->setValue(true)
                ->setIsChecked(isset($value[$_i]['on']) ? $value[$_i]['on'] : false)
                ->setHtmlId($element->getHtmlId()."_{$_i}_on")
                ->setLabel('Open');
            ;
            $fromField = new Varien_Data_Form_Element_Time(array());
            $fromField
                ->setForm($this->getForm())
                ->setName($element->getName()."[{$_i}][from]")
                ->setValue(isset($value[$_i]['from']) ? implode(',', $value[$_i]['from']) : null)
                ->setHtmlId($element->getHtmlId()."_{$_i}_from")
                ->setLabel('From');
            ;
            $toField = new Varien_Data_Form_Element_Time(array());
            $toField
                ->setForm($this->getForm())
                ->setName($element->getName()."[{$_i}][to]")
                ->setValue(isset($value[$_i]['to']) ? implode(',', $value[$_i]['to']) : null)
                ->setHtmlId($element->getHtmlId()."_{$_i}_to")
                ->setLabel('To');
            ;
            $html .= $onField->toHtml();
            $html .= $fromField->toHtml();
            $html .= $toField->toHtml();
        }
        $html .= '</div>';

        return $html;
    }
}