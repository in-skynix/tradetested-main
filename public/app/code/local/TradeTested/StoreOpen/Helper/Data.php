<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 23/09/15
 * Time: 13:08
 */
class TradeTested_StoreOpen_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Get Days of Week in ISO8601
     *
     * @return array
     */
    public function getDaysOfWeek()
    {
        return array(1=>'Monday',2=>'Tuesday',3=>'Wednesday',4=>'Thursday',5=>'Friday',6=>'Saturday',7=>'Sunday',);
    }

    /**
     * Get Current or Specified Time
     *
     * Always returned in the store's timezone. If time array is passed, it is assumed this is also in store's timezone.
     *
     * @param null $timeArr
     * @return Zend_Date
     * @throws Zend_Date_Exception
     */
    public function getTime($timeArr = null)
    {
        $time = Zend_Date::now()->setTimezone(Mage::getStoreConfig('general/locale/timezone'));
        if (is_array($timeArr)) {
            $time->setHour($timeArr[0])
                ->setMinute($timeArr[1])
                ->setSecond($timeArr[2]);
        }
        return $time;
    }

}