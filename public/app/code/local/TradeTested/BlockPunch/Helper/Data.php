<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/07/15
 * Time: 13:17
 */
class TradeTested_BlockPunch_Helper_Data extends Mage_Core_Helper_Data
{
    public function getPlaceholderName(Mage_Core_Block_Abstract $block)
    {
        return "{{|blockpunch_{$block->getCacheKey()}({$block->getNameInLayout()})|}}";
    }
}