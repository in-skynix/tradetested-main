<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/07/15
 * Time: 16:30
 */

/**
 * Class TradeTested_BlockPunch_Block_Placeholder
 *
 * Block used as a placeholder to include another bloc that is already named in the layout.
 *
 * When using BlockPunch, it will fail to render a placeholder if the block is not part of the layout already.
 * E.g. if the block is added to the layout during rendering of a block whose HTML could be cached
 * such as is the case when we add a block within a CMS block.
 *
 * This block helps us by allowing the CMS block to include it instead of the block we want.
 * We then add the block we want rendered to the layout XML or page layout with the same name.
 * This block will always render a placeholder that is then replaced with the actual block from the layout.
 */
class TradeTested_BlockPunch_Block_Placeholder extends Mage_Core_Block_Abstract
{
    /**
     * Always render as a placeholder.
     */
    public function getUseBlockPunchPlaceholder()
    {
        return true;
    }

    /**
     * Return the name of the actual block we want rendered by the placeholder
     *
     * @return string
     */
    public function getNameInLayout()
    {
        return $this->getName();
    }
}