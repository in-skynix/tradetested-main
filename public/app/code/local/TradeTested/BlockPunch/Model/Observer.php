<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/07/15
 * Time: 13:06
 */
class TradeTested_BlockPunch_Model_Observer
{
    /**
     * To HTML After
     *
     * After a block is rendered and saved to cache, or retrieved from cache:
     *  - If this block is configured to use a placeholder, return that placeholder in place of the HTML
     *    so the parent block is cached with the placeholder.
     * - If this block has placeholders in it after rendering or cache retrieval, render each child in place.
     *
     * @param Varien_Event_Observer $observer
     */
    public function coreBlockAbstractToHtmlAfter(Varien_Event_Observer $observer)
    {
        /** @var TradeTested_BlockPunch_Helper_Data $helper */
        $helper = Mage::helper('tradetested_blockpunch');
        /** @var Mage_Core_Block_Abstract $block */
        $block = $observer->getEvent()->getBlock();
        $transport = $observer->getEvent()->getTransport();
        $html = $transport->getHtml();

        //If the block is configured as a placeholder, change the html to placeholder.
        //When the parent block is first rendered, the configured child block returns a placeholder instead of
        //The actual HTML, and so the parent block is cached with placeholders
        //but the actual HTML is already cached (or wil be stored in registry) and can be added back in.
        //We render placeholders even if cache is not enabled so that TradeTested_BlockPunch_Block_Placeholder works.
        //We also might be caching a far ancestor block.
        if ($block->getUseBlockPunchPlaceholder()) {
            //Set the original HTML to the registry, in case the parent block is not cached.
            $placeholderName = $helper->getPlaceholderName($block);
            Mage::register($placeholderName, $html, true);
            $transport->setHtml($placeholderName);
        }

        //Replace placeholders (from a rendered or cached parent block) with the actual content of the child block.
        //If any of this block's parent blocks are cached, it will end up caching too much.
        if(($block->getNameInLayout() == 'root') && (strpos($html, '{{|blockpunch_') !== false)) {
            $html = preg_replace_callback('/\{\{\|blockpunch_(.*?)\((.*?)\)\|\}\}/', function($match) use($block) {
                if ($h = Mage::registry($match[0])) {
                    return $h;
                } elseif($b = $block->getLayout()->getBlock($match[2])) {
                    // This will load itself from cache if available
                    return $b->setUseBlockPunchPlaceholder(false)->toHtml();
                }
            }, $html);
            $transport->setHtml($html);
        }
    }
}