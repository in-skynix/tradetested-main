<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
/*
 * Make sure the upgrade is not performed on installations without the tables
 * (i.e. unpatched shops).
 */
$adminVersion = Mage::getConfig()->getModuleConfig('Mage_Admin')->version;
if (version_compare($adminVersion, '1.6.1.2', '>=')) {

    $blockNames = array(
        'tradetested_blockpunch/placeholder',
    );
    foreach ($blockNames as $blockName) {
        $whitelistBlock = Mage::getModel('admin/block')->load($blockName, 'block_name');
        $whitelistBlock->setData('block_name', $blockName);
        $whitelistBlock->setData('is_allowed', 1);
        $whitelistBlock->save();
    }

    $variableNames = array(
    );

    foreach ($variableNames as $variableName) {
        $whitelistVar = Mage::getModel('admin/variable')->load($variableName, 'variable_name');
        $whitelistVar->setData('variable_name', $variableName);
        $whitelistVar->setData('is_allowed', 1);
        $whitelistVar->save();
    }
}
$installer->endSetup();
