<?php

class TradeTested_Livechat_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @return string
     */
    public function getConfigJson()
    {
        if (Mage::helper('tradetested_core')->getEnv() == 'production') {
            return [
                'license' => Mage::getStoreConfig('livechat/general/license'),
                'group'   => Mage::getStoreConfig('livechat/advanced/group'),
            ];
        }
    }
}