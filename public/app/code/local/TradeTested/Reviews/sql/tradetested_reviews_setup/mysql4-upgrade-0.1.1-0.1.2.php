<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 12/10/2015
 * Time: 12:19
 */
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$installer->run("insert  into {$this->getTable('review_status')}(`status_id`,`status_code`) values (9,'Expired');");
$installer->endSetup();
