<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 12/10/2015
 * Time: 12:19
 */
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn(
        $installer->getTable('review_detail'),
        'reply_comment',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => '64k',
            'nullable' => true,
            'comment' => 'Reply Comment'
        )
    );
$installer->endSetup();
