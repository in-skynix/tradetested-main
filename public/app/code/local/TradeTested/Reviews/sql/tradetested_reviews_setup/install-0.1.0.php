<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn(
        $installer->getTable('review_detail'),
        'email',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 128,
            'nullable' => true,
            'comment' => 'Review Guest Email'
        )
    );

$installer->getConnection()
    ->addColumn(
        $installer->getTable('review_detail'),
        'location',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 128,
            'nullable' => true,
            'comment' => 'Review Guest Location'
        )
    );

$installer->getConnection()
    ->addColumn(
        $installer->getTable('review_detail'),
        'recommend',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
            'nullable' => true,
            'comment' => 'Review Guest Recommandation'
        )
    );
$installer->getConnection()
    ->addColumn(
        $installer->getTable('review_detail'),
        'images',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable' => true,
            'comment' => 'Review Images'
        )
    );

$installer->setConfigData('advanced/modules_disable_output/Mage_Review', 0);
$installer->setConfigData('advanced/modules_disable_output/Mage_Rating', 0);
$installer->setConfigData('catalog/review/allow_guest', 1);

$installer->getConnection()->insertOnDuplicate($installer->getTable('rating'), array(
    'rating_id'     => 1,
    'entity_id'     => 1,
    'rating_code'   => 'Overall Rating',
    'position'      => 0
));

$installer->getConnection()->insertOnDuplicate($installer->getTable('rating_store'), array(
    array('rating_id' => 1, 'store_id' => 0),
    array('rating_id' => 1, 'store_id' => 1),
    array('rating_id' => 1, 'store_id' => 3),
    array('rating_id' => 1, 'store_id' => 4),
    array('rating_id' => 1, 'store_id' => 5)
));

$installer->endSetup();