<?php

class TradeTested_Reviews_Helper_Data extends Mage_Review_Helper_Data
{

    /**
     * Get review statuses with their codes
     *
     * @return array
     */
    public function getReviewStatuses()
    {
        return array(
            Mage_Review_Model_Review::STATUS_APPROVED     => $this->__('Approved'),
            Mage_Review_Model_Review::STATUS_PENDING      => $this->__('Pending'),
            Mage_Review_Model_Review::STATUS_NOT_APPROVED => $this->__('Not Approved'),
            TradeTested_Reviews_Model_Rewrite_Review::STATUS_EXPIRED => $this->__('Expired'),
        );
    }

    /**
     * Get Cache tag with product ID and store ID to allow selective expiry
     *
     * @param null $productId
     * @param null $storeId
     * @return string
     */
    public function getCacheTag($productId = null, $storeId = null)
    {
        if ($storeId == null) {
            $storeId = Mage::app()->getStore()->getId();
        }
        return "tradetested_reviews_product_{$productId}_store_{$storeId}";
    }

    /**
     * Get URL to a list of reviews for product
     *
     * @param int $page
     * @return string
     */
    public function getProductReviewListUrl($page = 1)
    {
        return Mage::getUrl('review/product/list', array('id' => Mage::registry('current_product')->getId()))
            ."?review_page={$page}";
    }

    /**
     * Eager-load a default rating's values into review collection.
     *
     * @param Mage_Review_Model_Resource_Review_Collection $collection
     */
    public function addDefaultRatingToCollection($collection, $mainTableAlias = 'main_table')
    {
        $collection->getSelect()->join(
            array('rating' => $collection->getTable('rating/rating_option_vote')),
            "rating.review_id = {$mainTableAlias}.review_id AND rating.rating_id = 1",
            array('default_rating' => 'rating.value')
        );
    }

    public function getReviewsData()
    {
        /** @var  $reviewsCollection Mage_Review_Model_Resource_Review_Collection */
        $reviewsCollection = Mage::getModel('review/review')->getCollection()
            ->addStoreFilter(Mage::app()->getStore()->getId())
            ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED)
            ->setDefaultRatingOrder()
            ->setDateOrder()
            ->setPageSize(10);
        foreach($reviewsCollection as $_review){
            $product = Mage::getModel('catalog/product')->load($_review->getEntityPkValue());
            $someReviewData = $_review->getData();
            $reviewData[] = [
                'product_name'=>$product->getName(),
                'product_url'=>$product->getProductUrl(),
                'customer_name'=> $someReviewData['nickname'],
                'review_title'=> $someReviewData['title'],
                'review_text'=> $someReviewData['detail'],
                'review_rate'=> $someReviewData['default_rating'],
                'review_date'=> $someReviewData['created_at']
            ];
        }
        return [
            'data' => [
                'total' => $reviewData
            ]
        ];
    }

}
