<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 17/08/15
 * Time: 10:51
 */
class TradeTested_Reviews_Helper_Image extends Mage_Core_Helper_Abstract
{

    /**
     * Get URL for use on frontend
     *
     * @param $filename
     * @param int $width
     * @param int $height
     * @return Thumbor\Url\Builder
     */
    public function imageUrl($filename, $width = 1280, $height = 720, $crop = true)
    {
        $builder = Mage::helper('avid_thumbor')->getUrl('reviews/'.$filename)->addFilter('no-upscale');
        return $crop ? $builder->resize($width, $height) :  $builder->fitIn($width, $height);
    }

    /**
     * Get Filepath where original image stored
     *
     * @param $filename
     * @return string
     * @throws Mage_Core_Exception
     */
    public function getFilePath($filename)
    {
        $base = realpath($this->_getReviewImagePath());
        $path = realpath($base.DS.$filename);
        if ($path === false || strpos($path, $base) !== 0) {
            throw new Mage_Core_Exception('Cannot get file outside review root');
        } else {
            return $path;
        }
    }

    /**
     * Upload Image and Resize original
     *
     * @param $fileArr
     * @return string
     * @throws Exception
     */
    public function uploadImage($fileArr)
    {
        $this->_resizeReviewImageInPlace($fileArr['tmp_name']);
        $path = $this->_getReviewImagePath();

        $uploader = new Varien_File_Uploader($fileArr);
        $uploader->setAllowRenameFiles(true);
        $uploader->setFilesDispersion(false);
        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        /** @var $storageHelper Mage_Core_Helper_File_Storage_Database */
        $storageHelper = Mage::helper('core/file_storage_database');
        $storageHelper->saveUploadedFile($uploader->save($path));
        return $uploader->getUploadedFileName();
    }

    /**
     * Path where review images are stored
     *
     * @return string
     */
    protected function _getReviewImagePath()
    {
        return Mage::getBaseDir('media') . DS . 'reviews' . DS;
    }

    /**
     * Resize newly upoaded image in place
     *
     * @param $imageFile
     */
    protected function _resizeReviewImageInPlace($imageFile)
    {
        $image = new Varien_Image($imageFile);
        $image->constrainOnly(true);
        $image->keepFrame(true);
        $image->keepAspectRatio(true);
        if (($image->getOriginalWidth() > 1280 || $image->getOriginalHeight() > 720)) {
            $image->resize(1280, 720);
        }
        $image->save($imageFile);
    }
}