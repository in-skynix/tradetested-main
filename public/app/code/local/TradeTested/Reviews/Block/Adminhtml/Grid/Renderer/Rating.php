<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 12/10/2015
 * Time: 12:11
 */
class TradeTested_Reviews_Block_Adminhtml_Grid_Renderer_Rating
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $percent = $row->getDefaultRating()*20;
        return <<<HTML
    <div class="rating-box">
        <div class="rating" style="width:{$percent}%;"></div>
    </div>
HTML;

    }
}