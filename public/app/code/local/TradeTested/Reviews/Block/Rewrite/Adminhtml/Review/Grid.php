<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 12/10/2015
 * Time: 10:53
 */
class TradeTested_Reviews_Block_Rewrite_Adminhtml_Review_Grid extends Mage_Adminhtml_Block_Review_Grid
{
    protected function _prepareCollection()
    {
        $model = Mage::getModel('review/review');
        $collection = $model->getProductCollection();

        if ($this->getProductId() || $this->getRequest()->getParam('productId', false)) {
            $productId = $this->getProductId();
            if (!$productId) {
                $productId = $this->getRequest()->getParam('productId');
            }
            $this->setProductId($productId);
            $collection->addEntityFilter($this->getProductId());
        }

        if ($this->getCustomerId() || $this->getRequest()->getParam('customerId', false)) {
            $customerId = $this->getCustomerId();
            if (!$customerId){
                $customerId = $this->getRequest()->getParam('customerId');
            }
            $this->setCustomerId($customerId);
            $collection->addCustomerFilter($this->getCustomerId());
        }

        if (Mage::registry('usePendingFilter') === true) {
            $collection->addStatusFilter($model->getPendingStatus());
        }

        $collection->addStoreData();
        Mage::helper('tradetested_reviews')->addDefaultRatingToCollection($collection, 'rt');

        $this->setCollection($collection);
        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('review_id', array(
            'header'        => Mage::helper('review')->__('ID'),
            'align'         => 'right',
            'width'         => '50px',
            'filter_index'  => 'rt.review_id',
            'index'         => 'review_id',
        ));

        $this->addColumn('created_at', array(
            'header'        => Mage::helper('review')->__('Created On'),
            'align'         => 'left',
            'type'          => 'datetime',
            'width'         => '100px',
            'filter_index'  => 'rt.created_at',
            'index'         => 'review_created_at',
        ));

        if( !Mage::registry('usePendingFilter') ) {
            $this->addColumn('status', array(
                'header'        => Mage::helper('review')->__('Status'),
                'align'         => 'left',
                'type'          => 'options',
                'options'       => Mage::helper('review')->getReviewStatuses(),
                'width'         => '100px',
                'filter_index'  => 'rt.status_id',
                'index'         => 'status_id',
            ));
        }

        $this->addColumn('title', array(
            'header'        => Mage::helper('review')->__('Title'),
            'align'         => 'left',
            'width'         => '100px',
            'filter_index'  => 'rdt.title',
            'index'         => 'title',
            'type'          => 'text',
            'truncate'      => $this->_isExport ? 999999 : 50,
            'escape'        => $this->_isExport ? false : true,
        ));

        $this->addColumn('nickname', array(
            'header'        => Mage::helper('review')->__('Nickname'),
            'align'         => 'left',
            'width'         => '100px',
            'filter_index'  => 'rdt.nickname',
            'index'         => 'nickname',
            'type'          => 'text',
            'truncate'      => $this->_isExport ? 999999 : 50,
            'escape'        => $this->_isExport ? false : true,
        ));

        $this->addColumn('detail', array(
            'header'        => Mage::helper('review')->__('Review'),
            'align'         => 'left',
            'index'         => 'detail',
            'filter_index'  => 'rdt.detail',
            'type'          => 'text',
            'truncate'      => $this->_isExport ? 999999 : 50,
            'nl2br'         => $this->_isExport ? false : true,
            'escape'        => $this->_isExport ? false : true,
        ));

        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('visible_in', array(
                'header'    => Mage::helper('review')->__('Visible In'),
                'index'     => 'stores',
                'type'      => 'store',
                'store_view' => true,
            ));
        }

        $this->addColumn('name', array(
            'header'    => Mage::helper('review')->__('Product Name'),
            'align'     =>'left',
            'type'      => 'text',
            'index'     => 'name',
            'escape'    => true
        ));

        $this->addColumn('sku', array(
            'header'    => Mage::helper('review')->__('Product SKU'),
            'align'     => 'right',
            'type'      => 'text',
            'width'     => '50px',
            'index'     => 'sku',
            'escape'    => true
        ));

        $this->addColumn('default_rating', array(
            'header'    => Mage::helper('review')->__('Rating'),
            'align'     => 'right',
            'type'      => 'number',
            'width'     => '50px',
            'index'     => 'default_rating',
            'filter_condition_callback' => array($this, 'callbackFilterDefaultRating'),
            'renderer'  => $this->_isExport ? null : 'tradetested_reviews/adminhtml_grid_renderer_rating',
            'escape'    => true,
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('tradetested_survey')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('tradetested_survey')->__('Excel XML'));

        $this->sortColumnsByOrder();
        return $this;
    }

    /**
     * Allow filtering by default rating
     *
     * @param Mage_Review_Model_Resource_Review_Product_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     */
    public function callbackFilterDefaultRating(
        Mage_Review_Model_Resource_Review_Product_Collection $collection,
        Mage_Adminhtml_Block_Widget_Grid_Column $column
    ) {
        $value = $column->getFilter()->getValue();
        $collection->getSelect()
            ->where('rating.value >= ?', $value['from'])
            ->where('rating.value <= ?', $value['to'])
        ;
    }

}