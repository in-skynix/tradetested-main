<?php
class TradeTested_Reviews_Block_Rewrite_Adminhtml_Review_Edit_Form extends Mage_Adminhtml_Block_Review_Edit_Form
{
    /**
     * Add custom fields to form
     *
     * @return $this
     */
    public function _prepareForm()
    {
        parent::_prepareForm();
        //Varien_Data_Form
        $review = Mage::registry('review_data');

        /** @var Varien_Data_Form $form */
        $form = $this->getForm();
        $form->setEnctype('multipart/form-data');
        $fieldset = $form->getElement('review_details');

        $fieldset->addType('review_images', 'TradeTested_Reviews_Block_Adminhtml_Image');

        $fieldset->addField('reply_comment', 'textarea', array(
            'label'     => Mage::helper('review')->__('Reply with Comment'),
            'required'  => false,
            'name'      => 'reply_comment',
        ));

        $fieldset->addField('location', 'text', array(
            'label'     => Mage::helper('tradetested_reviews')->__('Location'),
            'required'  => true,
            'name'      => 'location'
        ));

        $fieldset->addField('email', 'text', array(
            'label'     => Mage::helper('tradetested_reviews')->__('Email'),
            'required'  => true,
            'name'      => 'email'
        ));

        $fieldset->addField('recommend', 'select', array(
            'label'     => Mage::helper('tradetested_reviews')->__('Recommend'),
            'required'  => true,
            'name'      => 'recommend',
            'values'    => $this->getRecommendOptions()
        ));
        $fieldset->addField('image_upload', 'image', array(
            'label'     => Mage::helper('tradetested_reviews')->__('Upload Image'),
            'required'  => false,
            'name'      => 'image_upload'
        ));

        foreach ($review->getImages() as $key => $value) {
            $fieldset->addField('images'.$key, 'review_images', array(
                'label'     => Mage::helper('tradetested_reviews')->__('Images' .  $key),
                'required'  => true,
                'name'      => 'images[]',
                'filename'  => $value
            ));
        }



        $form->setValues($review->getData());
        $this->setForm($form);
        return $this;
    }

    /**
     * Get Recommended? Options
     * @return array
     */
    public function getRecommendOptions()
    {
        $options[] = array(
            'value' => '1',
            'label' => 'Yes'
        );

        $options[] = array(
            'value' => '0',
            'label' => 'No'
        );

        return $options;
    }

}