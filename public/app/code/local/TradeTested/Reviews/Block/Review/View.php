<?php
require_once Mage::getBaseDir('lib') . '/PHPAgo/timeago.inc.php';
class TradeTested_Reviews_Block_Review_View extends Mage_Review_Block_Product_View_List
{
    /**
     * Get Time on Review in current terms (timeago)
     *
     * @param $review
     * @return string
     * @throws Zend_Date_Exception
     */
    public function getCurrentTime($review)
    {
        $timeZone = Mage::getStoreConfig('general/locale/timezone');
        $datetime = Zend_Date::now();
        $timeAgo = new TimeAgo();

        $datetime->setLocale(Mage::getStoreConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_LOCALE))->setTimezone($timeZone);

        return $timeAgo->inWords($review->getCreatedAt(), date('Y-m-d h:i:s', $datetime->get()));
    }

    /**
     * Prepare link to review list for current product
     *
     * @return string
     */
    public function getBackUrl()
    {
        $url = $this->getProductData()->getData('url');
        if (!is_null($url)) {
            return $url;
        }
    }

    /**
     * Get URL ot this review
     *
     * @return string
     */
    public function getReviewUrl()
    {
        return Mage::getUrl('review/product/view', array('id' => $this->getReviewData()->getId()));
    }

    /**
     * Magento Bug fix
     * @return resources collection arary()
     */
    public function getRating()
    {
        if (!$this->getRatingCollection()) {
            $ratingCollection = Mage::getModel('rating/rating_option_vote')
                ->getResourceCollection()
                ->setReviewFilter($this->getReviewId())
                ->setStoreFilter(Mage::app()->getStore()->getId())
                ->addRatingInfo(Mage::app()->getStore()->getId())
                ->load();
            $this->setRatingCollection(($ratingCollection->getSize())?$ratingCollection : false);
        }
        return $this->getRatingCollection();
    }

    /**
     * Retrieve current review model from registry
     *
     * Overloaded to allow setting review
     *
     * @return Mage_Review_Model_Review
     */
    public function getReviewData()
    {
        if (!$this->getData('review')) {
            $this->setData('review', Mage::registry('current_review'));
        }
        return $this->getData('review');
    }

    /**
     * Get Review ID
     *
     * @return int
     * @throws Exception
     */
    public function getReviewId()
    {
        $reviewId = $this->getRequest()->getParam('id');
        if (!is_null($reviewId)) {
            return $reviewId;
        }
    }

    /**
     * Can show microdata for review?
     *
     * We only want to show microdata for reviews specifically for the product being viewed
     *
     * @return bool
     */
    public function canShowReviewMicrodata()
    {
        return !!($this->getReviewData()->getEntityPkValue() == $this->getProductData()->getId());
    }

    /**
     * Get Images Media HTML
     *
     * @return string
     */
    public function getMediaHtml()
    {
        return $this->getLayout()->createBlock('tradetested_reviews/review_images')
            ->setTemplate('tradetested/review/product/view/images.phtml')
            ->setReview($this->getReviewData())
            ->toHtml();
    }

    /**
     * Get cache key informative items
     * Provide string array key to share specific info item with FPC placeholder
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return array(
            'BLOCK_TPL',
            Mage::app()->getStore()->getCode(),
            $this->getTemplateFile(),
            'template' => $this->getTemplate(),
            $this->getNameInLayout(),
            $this->getTemplate(),
            $this->getReviewData()->getId(),
            Mage::app()->getStore()->getId()
        );
    }

    /**
     * @return int
     */
    public function getCacheLifetime()
    {
        return 3600 * 24 * 7;
    }

    /**
     * Add a tag to allow blocks to be paritally expired
     *
     * @return array
     */
    public function getCacheTags()
    {
        return array_merge(
            parent::getCacheTags(),
            array(
                'tradetested_reviews',
                Mage::helper('tradetested_reviews')
                    ->getCacheTag($this->getProductData()->getId(), Mage::app()->getStore()->getId())
            )
        );
    }

}
