<?php
class TradeTested_Reviews_Block_Review_List extends Mage_Review_Block_Product_View_List
{

    /**
     * @var Mage_Review_Model_Review $_currentReview
     */
    protected $_currentReview;

    /**
     * Allow limit to be set after this block has been added to layout.
     *
     * @return $this
     */
    protected function _beforeToHtml()
    {
        /** @var Mage_Page_Block_Html_Pager $toolbar */
        if ($toolbar = $this->getChild('toolbar')) {
            $toolbar->getCollection()->setPageSize($toolbar->getLimit());
        }

        return parent::_beforeToHtml();
    }

    /**
     * Get Reviews Collection to Display
     *
     * @return TradeTested_Reviews_Model_Rewrite_Resource_Review_Collection
     */
    public function getReviewsCollection()
    {
        if (null === $this->_reviewsCollection) {
            $this->_reviewsCollection = Mage::getModel('review/review')->getCollection()
                 ->addStoreFilter(Mage::app()->getStore()->getId())
                 ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED)
                 ->setDefaultRatingOrder()
                 ->addEntityFilter('product', $this->getProduct())
                 ->setDateOrder();
        }
        return $this->_reviewsCollection;
    }

    /**
     * conver default Rating Value
     * example 93 => 4.65
     * @return float
     */
    public function getRatingValue()
    {
        return $this->getReviewsCollection()->getAverageDefaultRating();
    }

    /**
     * Get cache key informative items
     * Provide string array key to share specific info item with FPC placeholder
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return array(
            'BLOCK_TPL',
            Mage::app()->getStore()->getCode(),
            $this->getTemplateFile(),
            'template' => $this->getTemplate(),
            $this->getNameInLayout(),
            $this->getProductId(),
            Mage::app()->getStore()->getId(),
            $this->getChild('toolbar')->getCurrentPage()
        );
    }

    /**
     * @return int
     */
    public function getCacheLifetime()
    {
        return 3600 * 24 * 7;
    }

    /**
     * Add cache tag to allow blocks to be partially expired
     *
     * @return array
     */
    public function getCacheTags()
    {
        return array_merge(
            parent::getCacheTags(),
            array(
                'tradetested_reviews',
                Mage::helper('tradetested_reviews')
                    ->getCacheTag($this->getProductId(), Mage::app()->getStore()->getId())
            )
        );
    }

    /**
     * Render a review item to HTML
     *
     * @param Mage_Review_Model_Review $review
     * @return string
     */
    public function renderItem(Mage_Review_Model_Review $review)
    {
        return $this->getLayout()->createBlock('tradetested_reviews/review_view')
            ->setReview($review)
            ->setTemplate('tradetested/review/product/view/list_item.phtml')
            ->toHtml();
    }
}