<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 24/08/15
 * Time: 20:37
 */
class TradeTested_Reviews_Block_Review_Images extends TradeTested_Reviews_Block_Review_View
{
    /**
     * Get JSON for Image Gallery
     *
     * @return array
     */
    public function getGalleryImageJSON()
    {
        $images = [];
        /** @var TradeTested_Reviews_Helper_Image $imageHelper */
        $imageHelper = Mage::helper('tradetested_reviews/image');
        foreach ($this->getReviewData()->getImages() as $_image) {
            $images[] = [
                'id'         => md5($_image),
                'src_retina' => [
                    'thumbnail' => $imageHelper->imageUrl($_image, 136, 136)->__toString(),
                    'zoom'      => $imageHelper->imageUrl($_image)->__toString(),
                ],
                'src'        => [
                    'thumbnail' => $imageHelper->imageUrl($_image, 68, 68)->__toString(),
                    'zoom'      => $imageHelper->imageUrl($_image)->__toString(),
                ],
                'label'      => 'Review Image'
            ];
        }

        return ['container_id' => $this->getGalleryId(), 'images' => $images];
    }
}