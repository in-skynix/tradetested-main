<?php
class TradeTested_Reviews_Model_Spam extends Mage_Core_Model_Abstract
{
    const XML_PATH_MGT_AKISMET_API_KEY = 'a7dcd2982c76';
    const LOGFILE = 'tradetest_review.log';

    /**
     * Log message
     *
     * @param $message
     */
    public function log($message)
    {
        Mage::log($message, null, self::LOGFILE, true);
    }

    /** @var  Zend_Service_Akismet $_service */
    protected $_service;

    /**
     * Is Spam?
     *
     * @param array $data
     * @param string $commentType
     * @return bool
     * @throws Zend_Service_Exception
     */
    public function isSpam(array $data, $commentType = 'contact')
    {
        $isSpam = false;
        $service = $this->getService();
        $apiKey = $this->getApiKey();
        if ($service->verifyKey($apiKey)) {
            $data = new Varien_Object($data);
            $httpHelper = $this->getHttpHelper();
            $data = array(
                'user_ip' => $httpHelper->getRemoteAddr(),
                'user_agent' => $httpHelper->getHttpUserAgent(),
                'comment_type' => $commentType,
                'comment_author' => $data->getName(),
                'comment_author_email' => $data->getEmail(),
                'comment_content' => $data->getComment(),
            );
            if ($service->isSpam($data)) {
                $logMessage = sprintf('Akismet Spam Detected "%s" from IP: "%s"', $data['comment_content'], $data['user_ip']);
                $this->log($logMessage);
                $isSpam = true;
            }
        } else {
            $logMessage = sprintf('Akismet ApiKey "%s" is not valid', $apiKey);
            $this->log($logMessage);
        }
        return $isSpam;
    }

    /**
     * @return Zend_Service_Akismet
     * @throws Mage_Core_Exception
     */
    public function getService()
    {
        if (!$this->_service) {
            $this->_service = new Zend_Service_Akismet(self::getApiKey(), Mage::app()->getStore()->getBaseUrl());
        }
        return $this->_service;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        $apiKey = (string) self::XML_PATH_MGT_AKISMET_API_KEY;
        return trim($apiKey);
    }

    /**
     * @return Mage_Core_Helper_Http
     */
    public function getHttpHelper()
    {
        return Mage::helper('core/http');
    }

}
