<?php
class TradeTested_Reviews_Model_Observer
{
    /**
     * Check if it's spam
     * flush review cache before review approved
     *
     * @param Varien_Event_Observer $observer
     * @throws Exception
     */
    public function reviewSaveBefore(Varien_Event_Observer $observer)
    {
        /** @var TradeTested_Reviews_Model_Rewrite_Review $review */
        $review = $observer->getEvent()->getObject();
        $akismet = Mage::getSingleton('tradetested_reviews/spam');
        if (isset($review) && !$review->getId()) {
            $reviewData = array(
                'name' => $review->getNickname(),
                'comment' => $review->getTitle() . ' ' . $review->getComment(),
            );
            if ($akismet->isSpam($reviewData)) {
                throw new Exception('Akismet Spam Detected');
            }
        }
        if (Mage::app()->getStore()->isAdmin()) {
            $productId = $review->getEntityPkValue();
            Mage::app()->cleanCache(array(
                Mage::helper('tradetested_reviews')->getCacheTag($productId, $review->getStoreId())
            ));
        }
    }
}