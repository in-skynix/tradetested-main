<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 11/11/2015
 * Time: 11:28
 */
class TradeTested_Reviews_Model_Tasks
{
    public function expireReviews()
    {
        $reviews = Mage::getModel('review/review')->getCollection()
            ->addDefaultRating()
            ->addFieldToFilter('status_id', Mage_Review_Model_Review::STATUS_APPROVED)
            ->addFieldToFilter(
                'created_at',
                array('to' => Mage::getModel('core/date')->date(null, strtotime('6 weeks ago')))
            );
        $reviews->getSelect()->where('rating.value <= 2');
        /** @var TradeTested_Reviews_Model_Rewrite_Review $_review */
        foreach ($reviews as $_review) {
            $_review->setStatusId(TradeTested_Reviews_Model_Rewrite_Review::STATUS_EXPIRED)->save();
        }
    }
}
