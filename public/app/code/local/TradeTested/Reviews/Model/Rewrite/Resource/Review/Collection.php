<?php
/**
 * Setup Resurce Model
 *
 */
class TradeTested_Reviews_Model_Rewrite_Resource_Review_Collection extends Mage_Review_Model_Resource_Review_Collection
{
    /** @var int $_productId */
    protected $_productId;

    /** @var boolean $_joinedDefaultRating */
    protected $_joinedDefaultRating = false;

    protected $_averageDefaultRating;

    /**
     * Init collection select
     *
     * @return Mage_Core_Model_Resource_Db_Collection_Abstract
     */
    protected function _initSelect()
    {
        $this->getSelect()->from(array('main_table' => $this->getMainTable()));
        $this->getSelect()
            ->join(array('detail' => $this->_reviewDetailTable),
                'main_table.review_id = detail.review_id',
                array('detail_id', 'title', 'detail', 'nickname', 'customer_id', 'email', 'location', 'recommend',
                    'images', 'reply_comment')
            );
        return $this;
    }

    /**
     * Deserialize image data in each item to array of filenames
     *
     * @return Mage_Core_Model_Resource_Db_Collection_Abstract
     */
    protected function _afterLoad()
    {
        /** @var Mage_Review_Model_Review $item */
        foreach ($this->_items as $item) {
            $item->getResource()->extractImageData($item);
        }
        parent::_afterLoad();
        return $this;
    }

    /**
     * Add default rating to select
     *
     * @return $this
     */
    public function addDefaultRating()
    {
        if (!$this->_joinedDefaultRating) {
            $this->getSelect()->join(
                array('rating' => $this->getTable('rating/rating_option_vote')),
                "rating.review_id = main_table.review_id AND rating.rating_id = 1",
                array('default_rating' => 'rating.value')
            );
            $this->_joinedDefaultRating = true;
        }
        return $this;
    }

    /**
     * Get Average Rating for collection
     *
     * @return string
     */
    public function getAverageDefaultRating()
    {
        if (!$this->_averageDefaultRating) {
            $collection = clone($this);
            $collection->addDefaultRating();
            $collection->_renderFilters();
            $countSelect = clone $collection->getSelect();
            $countSelect->reset(Zend_Db_Select::ORDER);
            $countSelect->reset(Zend_Db_Select::LIMIT_COUNT);
            $countSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
            $countSelect->reset(Zend_Db_Select::COLUMNS);
            $countSelect->columns('AVG(rating.value)');
            $this->_averageDefaultRating = $this->getConnection()->fetchOne($countSelect, $this->_bindParams);
        }
        return $this->_averageDefaultRating;
    }

    /**
     * Set order based on whether rating is 4 or above
     *
     * @return $this
     */
    public function setDefaultRatingOrder()
    {
        $this->addDefaultRating();
        return $this->setOrder(new Zend_Db_Expr('(rating.value >= 4)'));
    }

    /**
     * Add entity filter
     *
     * Customised to add similar products filter instead of specific product ID only
     *
     * @param int|string $entity
     * @param int $pkValue
     * @return Mage_Review_Model_Resource_Review_Collection
     */
    public function addEntityFilter($entity, $pkValue)
    {
        $this->_productId = $pkValue;

        if (is_numeric($entity)) {
            $this->addFilter('entity',
                $this->getConnection()->quoteInto('main_table.entity_id=?', $entity),
                'string');
        } elseif (is_string($entity)) {
            $this->_select->join($this->_reviewEntityTable,
                'main_table.entity_id='.$this->_reviewEntityTable.'.entity_id',
                array('entity_code'));

            $this->addFilter('entity',
                $this->getConnection()->quoteInto($this->_reviewEntityTable.'.entity_code=?', $entity),
                'string');
        }

        $this->addSimilarProductFilter();

        return $this;
    }

    /**
     * Get Similar product IDs by SKU
     *
     * Similar products are variations of one another with same price, e.g. colours.
     *
     * @return array
     */
    protected function getSimilarProductIdsBySKU()
    {
        $product = $this->getReviewProduct();
        $productIds[] = $product->getId();
        $fourDigitSKU = substr($product->getSku(), 0, 4);

        $products = Mage::getModel('catalog/product')
        ->getCollection()
        ->addAttributeToSelect(array('name', 'id', 'price','sku'))
        ->addAttributeToFilter('entity_id', array('neq' => $product->getId()))
        ->addAttributeToFilter('sku', array('like' => $fourDigitSKU.'%'))
        ->addAttributeToFilter('price', array("eq" => $product->getPrice()));
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);

        if (!is_null($product->getCategoryId())) {
            $currentCategory = Mage::getModel('catalog/category')->load($product->getCategoryId());
            $products->addCategoryFilter($currentCategory);
        }
        return array_merge(array($product->getId()), $products->getAllIds());
    }

    /**
     * Add filter for products that are variations of one another
     * @return $this
     */
    protected function addSimilarProductFilter()
    {
        if (!is_null($this->getReviewProduct())) {
            $condition  = $this->_getConditionSql('main_table.entity_pk_value', array("in" => array($this->getSimilarProductIdsBySKU())));
            $this->addFilter('', $condition, 'string');
        }
        //Always show the original product review first.
        $this->setOrder('main_table.entity_pk_value=' . $this->getReviewProduct()->getId(), 'DESC');
        return $this;
    }

    /**
     * Get Review Product
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function getReviewProduct()
    {
        $product = Mage::registry('current_product');
        if (is_null($product)) {
            $product = Mage::registry('product');
            if(is_null($product) && !is_null($this->_productId)) {
                $product = Mage::getModel('catalog/product')->load($this->_productId);
            }
        }
        return $product;
    }
}
