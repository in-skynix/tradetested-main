<?php
class TradeTested_Reviews_Model_Rewrite_Resource_Review extends Mage_Review_Model_Resource_Review
{

    /**
     * Clear cache of review blocks for this product and store
     *
     * @param Mage_Core_Model_Abstract $object
     * @return $this
     */
    protected function _beforeDelete(Mage_Core_Model_Abstract $object)
    {
        parent::_beforeDelete($object);
        Mage::app()->cleanCache(array(
            Mage::helper('tradetested_reviews')->getCacheTag($object->getEntityPkValue(), $object->getStoreId())
        ));
        return $this;
    }

    /**
     * Extract serialized data to array of filenames
     *
     * @param Mage_Core_Model_Abstract $object
     * @return null
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object) {
        parent::_afterLoad($object);
        $this->extractImageData($object);
    }

    /**
     * Extract Image Data
     *
     * used in _afterLoad and after loading collection
     *
     * @param Mage_Core_Model_Abstract $object
     */
    public function extractImageData(Mage_Core_Model_Abstract $object) {
        $images = json_decode($object->getImages());
        $images = $images ? $images : array();
        $object->setImages($images);
    }

    /**
     * Perform actions after object save
     *
     * Overloaded to handle additional data and images
     *
     * @param Mage_Core_Model_Abstract $object
     * @return $this
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        $adapter = $this->_getWriteAdapter();

        //Begin customisation
        //Passed from backend;
        if (!empty($object->getImages())) {
            $this->processDeleteFiles($object);
        }
        if (!empty($object->getUploadedImages())) {
            $existing = is_array($object->getImages()) ? $object->getImages() : array();
            $object->setImages(array_merge($existing, $object->getUploadedImages()));
        }

        $detail = array(
            'title'             => $object->getTitle(),
            'detail'            => $object->getDetail(),
            'nickname'          => $object->getNickname(),
            'email'             => $object->getEmail(),
            'location'          => $object->getLocation(),
            'reply_comment'     => $object->getReplyComment(),
            'recommend'         => $object->getRecommend(),
            'images'            => json_encode($object->getImages())
        );
        //End customisation

        $select = $adapter->select()
            ->from($this->_reviewDetailTable, 'detail_id')
            ->where('review_id = :review_id');
        $detailId = $adapter->fetchOne($select, array(':review_id' => $object->getId()));

        if ($detailId) {
            $condition = array("detail_id = ?" => $detailId);
            $adapter->update($this->_reviewDetailTable, $detail, $condition);
        } else {
            $detail['store_id']   = $object->getStoreId();
            $detail['customer_id']= $object->getCustomerId();
            $detail['review_id']  = $object->getId();
            $adapter->insert($this->_reviewDetailTable, $detail);
        }


        /**
         * save stores
         */
        $stores = $object->getStores();
        if (!empty($stores)) {
            $condition = array('review_id = ?' => $object->getId());
            $adapter->delete($this->_reviewStoreTable, $condition);

            $insertedStoreIds = array();
            foreach ($stores as $storeId) {
                if (in_array($storeId, $insertedStoreIds)) {
                    continue;
                }

                $insertedStoreIds[] = $storeId;
                $storeInsert = array(
                    'store_id' => $storeId,
                    'review_id'=> $object->getId()
                );
                $adapter->insert($this->_reviewStoreTable, $storeInsert);
            }
        }

        // reaggregate ratings, that depend on this review
        $this->_aggregateRatings(
            $this->_loadVotedRatingIds($object->getId()),
            $object->getEntityPkValue()
        );

        return $this;
    }

    /**
     * Process filed to delete for deleted images.
     *
     * @param $object
     * @throws Mage_Core_Exception
     */
    public function processDeleteFiles($object)
    {
        $keep = $object->getImages();
        foreach ((array)$object->getDeleteImages() as $value) {
            foreach ($keep as $k => $origValue) {
                if ($value == $origValue) {
                    unset($keep[$k]);
                    unlink(Mage::helper('tradetested_reviews/image')->getFilePath($value));
                }
            }
        }
        $object->setImages($keep);
    }
}