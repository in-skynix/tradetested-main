<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 17/08/15
 * Time: 17:56
 */
class TradeTested_Reviews_Model_Rewrite_Review extends Mage_Review_Model_Review
{
    const XML_PATH_EMAIL_RECIPIENT          = 'contacts/email/recipient_email';
    const XML_PATH_EMAIL_SENDER             = 'contacts/email/sender_email_identity';
    const XML_PATH_EMAIL_TEMPLATE_CONFIRM   = 'catalog/review/confirmation_email_template';
    const XML_PATH_EMAIL_TEMPLATE_SUPPORT   = 'catalog/review/support_email_template';
    const XML_PATH_EMAIL_ENABLED            = 'catalog/review/send_confirmation';
    const STATUS_EXPIRED   = 9;

    /**
     * Validate extra data
     *
     * @return array|bool
     * @throws Exception
     * @throws Zend_Validate_Exception
     */
    public function validate()
    {
        $errors = parent::validate();
        $errors = ($errors === true) ? array() : $errors;
        $ratings = $this->getRatings();
        if (!Zend_Validate::is($ratings[1], 'NotEmpty')) {
            $errors[] = Mage::helper('tradetested_reviews')->__('Please choose a rating');
        }
        if (!Zend_Validate::is($this->getEmail(), 'EmailAddress')) {
            $errors[] = Mage::helper('tradetested_reviews')->__('Email address is required');
        }
        if (!Zend_Validate::is($this->getLocation(), 'NotEmpty')) {
            $errors[] = Mage::helper('tradetested_reviews')->__('Location is required');
        }

        if (empty($errors)) {
            return true;
        }
        return $errors;
    }

    /**
     * Trim whitespace some browsers seem to add.
     */
    protected function _beforeSave()
    {
        $this->setDetail(trim($this->getDetail()));
        return parent::_beforeSave();
    }

    /**
     * Check for any emails that need to be sent
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterSave()
    {
        if ($this->isObjectNew()) {
            $ratings = $this->getRatings();
            $product = Mage::getModel('catalog/product')->load($this->getEntityPkValue());
            $this->setDefaultRating((int)$ratings[1])
                ->setProductSku($product->getSku())
                ->setProductName($product->getName())
            ;


            if (Mage::getStoreConfigFlag(self::XML_PATH_EMAIL_ENABLED)) {
                $this->_sendConfirmationEmail();
            }

            if ($this->getDefaultRating() <= (int)Mage::getStoreConfig('catalog/review/support_email_threshold')) {
                $this->_sendSupportEmail();
            }
        }
        return parent::_afterSave(); // TODO: Change the autogenerated stub
    }

    /**
     * Send Warning email to support for low rating
     */
    protected function _sendSupportEmail()
    {
        Mage::getModel('core/email_template')
            ->setDesignConfig(array('area' => 'frontend'))
            ->sendTransactional(
                Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE_SUPPORT),
                Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
                null,
                array('review' => $this)
            );
    }

    /**
     * Send confirmation email to customer
     */
    protected function _sendConfirmationEmail()
    {
            Mage::getModel('core/email_template')
                ->setDesignConfig(array('area' => 'frontend'))
                ->sendTransactional(
                    Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE_CONFIRM),
                    Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                    $this->getEmail(),
                    $this->getNickname(),
                    array('review' => $this)
                );

    }

}