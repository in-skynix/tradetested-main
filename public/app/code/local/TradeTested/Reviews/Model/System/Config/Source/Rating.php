<?php
class TradeTested_Reviews_Model_System_Config_Source_Rating
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 0, 'label'=>Mage::helper('tradetested_reviews')->__('0')),
            array('value' => 1, 'label'=>Mage::helper('tradetested_reviews')->__('1')),
            array('value' => 2, 'label'=>Mage::helper('tradetested_reviews')->__('2')),
            array('value' => 3, 'label'=>Mage::helper('tradetested_reviews')->__('3')),
            array('value' => 4, 'label'=>Mage::helper('tradetested_reviews')->__('4')),
            array('value' => 5, 'label'=>Mage::helper('tradetested_reviews')->__('5')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            0 => Mage::helper('tradetested_reviews')->__('0'),
            1 => Mage::helper('tradetested_reviews')->__('1'),
            2 => Mage::helper('tradetested_reviews')->__('2'),
            3 => Mage::helper('tradetested_reviews')->__('3'),
            4 => Mage::helper('tradetested_reviews')->__('4'),
            5 => Mage::helper('tradetested_reviews')->__('5'),
        );
    }

}