<?php
require_once 'Mage/Adminhtml/controllers/Catalog/Product/ReviewController.php';
class TradeTested_Reviews_Adminhtml_Catalog_Product_ReviewController
    extends Mage_Adminhtml_Catalog_Product_ReviewController
{
    public function saveAction()
    {
        if (($data = $this->getRequest()->getPost()) && ($this->getRequest()->getParam('id'))) {
            $this->getRequest()->setPost($this->_sanitizeAndUploadImages($data));
        }
        return parent::saveAction();
    }

    /**
     * Export reviews grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName   = 'survey_responses.csv';
        $grid       = $this->getLayout()->createBlock('adminhtml/review_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     * Export reviews grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName   = 'survey_responses.xml';
        $grid       = $this->getLayout()->createBlock('adminhtml/review_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * Upload Images and add data to array/
     * @param array $data
     * @return array
     */
    protected function _sanitizeAndUploadImages($data)
    {
        /** @var TradeTested_Reviews_Helper_Data $helper */
        $helper = Mage::helper('tradetested_reviews');
        $file = $_FILES['image_upload'];
        if ($file['size'] > 5242880) {
            return Mage::getSingleton('core/session')
                ->addError($helper->__('File upload failed: "' . $file['name'] . '" is larger than 5MB'));
        }
        if (isset($file['name']) and (file_exists($file['tmp_name']))) {
            $newImage = Mage::helper('tradetested_reviews/image')->uploadImage($file);
            $data['uploaded_images'] = array($newImage);
        }
        return $data;
    }

}