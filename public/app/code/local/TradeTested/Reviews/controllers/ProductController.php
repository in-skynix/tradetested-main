<?php

require_once "Mage/Review/controllers/ProductController.php";

class TradeTested_Reviews_ProductController extends Mage_Review_ProductController
{

    /**
     * Create Review. Allow Image Upload
     *
     * @return Mage_Core_Controller_Varien_Action|void
     */
    public function postAction()
    {
        if (!$this->_validateFormKey()) {
            Mage::getSingleton('core/session')->addError(Mage::helper('tradetested_reviews')->__(
                'Could not validate request. Check file upload size.'
            ));
            return $this->_redirectReferer();
        }

        $data = $this->getRequest()->getPost();
        try {
            $session = Mage::getSingleton('review/session');
            if ($data = $session->getFormData(true)) {
                $session->setFormData($this->_sanitizeAndUploadImages($data));
            } else {
                $data = $this->getRequest()->getPost();
                $this->getRequest()->setPost($this->_sanitizeAndUploadImages($data));
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/session')
                ->addError(Mage::helper('tradetested_reviews')->__('System cannot upload files'))
                ->setFormData($data);
            return $this->_redirectReferer();
        }
        return parent::postAction();
    }

    /**
     * Action for displaying the new review form
     *
     */
    public function newAction()
    {
        $product = $this->_initProduct();
        if (!$product) {
            $this->_forward('noroute');
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('review/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }

    /**
     * Upload Images and add data to array/
     * @param array $data
     * @return array
     */
    protected function _sanitizeAndUploadImages($data)
    {
        /** @var TradeTested_Reviews_Helper_Data $helper */
        $helper = Mage::helper('tradetested_reviews');
        $uploadedFiles = $_FILES['images'];
        $newImages = array();
        for ($i = 0; $i < count($uploadedFiles['name']); $i++) {
            if ($i >= 10) {
                Mage::getSingleton('core/session')->addError($helper->__('only first 10 files were uploaded'));
                break;
            }
            $file = array(
                'name'      => $uploadedFiles['name'][$i],
                'type'      => $uploadedFiles['type'][$i],
                'tmp_name'  => $uploadedFiles['tmp_name'][$i],
                'error'     => $uploadedFiles['error'][$i],
                'size'      => $uploadedFiles['size'][$i],
            );
            if ($file['size'] > 5242880) {
                Mage::getSingleton('core/session')
                    ->addError($helper->__('File upload failed: "' . $file['name'] . '" is larger than 5MB'));
                continue;
            }

            if (isset($file['name']) and (file_exists($file['tmp_name']))) {
                $newImages[] = Mage::helper('tradetested_reviews/image')->uploadImage($file);
            }
        }

        $data['uploaded_images'] = $newImages;
        return $data;
    }

    /**
     * Prevent registering category twice
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _initProduct()
    {
        if ($categoryId = (int) $this->getRequest()->getParam('category', false)) {
            $category = Mage::getModel('catalog/category')->load($categoryId);
            Mage::register('current_category', $category);
            $this->getRequest()->setParam('category', null);
        }
        return parent::_initProduct();
    }

    /**
     * Allow loading product in multiple methods
     *
     * @param int $productId
     * @return bool|Mage_Catalog_Model_Product
     */
    protected function _loadProduct($productId)
    {
        if ($product = Mage::registry('current_product')) {
            return $product;
        } else {
            $product = parent::_loadProduct($productId);
            if (!Mage::registry('current_category')) {
                $category = Mage::getModel('catalog/category')->load($product->getDeepestCategoryId());
                if ($category->getId()) {
                    Mage::register('current_category', $category);
                }
            }
            return $product;
        }
    }

    /**
     * Redirect back to product if successful
     *
     * @param null $defaultUrl
     * @return null
     */
    protected function _redirectReferer($defaultUrl = null)
    {
        $session    = Mage::getSingleton('core/session');
        if(count($session->getMessages()->getItems('success'))) {
            $productId  = (int) $this->getRequest()->getParam('id');
            $product =  $this->_loadProduct($productId);
            return $this->_redirectUrl($product->getProductUrl());
        } else {
            $refererUrl = $this->_getRefererUrl();
            if (empty($refererUrl)) {
                $refererUrl = empty($defaultUrl) ? Mage::getBaseUrl() : $defaultUrl;
            } else {
                $params = array();
                $parts = parse_url($refererUrl);
                if (isset($parts['query'])) {
                    parse_str($parts['query'], $params);
                }
                $params['popup'] = 'review_form';
                $parts['query'] = http_build_query($params);
                $port = (isset($parts['port']) && $parts['port']) ? ':'.$parts['port'] : '';
                $refererUrl = $parts['scheme'] . '://' . $parts['host'] .$port. $parts['path'] . '?' . $parts['query'];
            }
            $this->getResponse()->setRedirect($refererUrl);
            return $this;
        }

    }

    /**
     * Crops POST values
     * @param array $reviewData
     * @return array
     */
    protected function _cropReviewData(array $reviewData)
    {
        $croppedValues = array();
        $allowedKeys = array_fill_keys(array('detail', 'title', 'nickname', 'location', 'email', 'ratings', 'uploaded_images'), true);

        foreach ($reviewData as $key => $value) {
            if (isset($allowedKeys[$key])) {
                $croppedValues[$key] = $value;
            }
        }

        return $croppedValues;
    }
}

