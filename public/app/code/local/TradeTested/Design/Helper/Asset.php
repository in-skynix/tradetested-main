<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 16/05/14
 * Time: 4:38 PM
 */
class TradeTested_Design_Helper_Asset extends Mage_Core_Helper_Abstract
{
    /**
     * Get URL of Asset
     *
     * @param $asset
     * @return string
     */
    public function getUrl($asset, $fingerprint = true)
    {
        $filename = $this->_getFilename($asset, $fingerprint);
        return Mage::getBaseUrl('skin').$filename;
    }

    /**
     * Get a URL builder instance for an asset image
     *
     * @param $imagePath
     * @return \Thumbor\Url\Builder
     */
    public function getImageUrl($imagePath)
    {
        return Mage::helper('avid_thumbor')->getUrl('assets/'.$imagePath);
    }

    /**
     * Get URI Path of Asset
     *
     * @param $asset
     * @return string
     */
    public function getPath($asset, $fingerprint = true)
    {
        $filename = $this->_getFilename($asset, $fingerprint);
        return '/assets/'.$filename;
    }

    /**
     * Get File Path of Asset
     *
     * @param $asset
     * @return string
     */
    public function getFilePath($asset)
    {
        return $this->_getAssetDir().DS.$asset;
    }

    /**
     * Get Directory where assets stored
     *
     * @return string
     */
    protected function _getAssetDir()
    {
        return realpath(Mage::getBaseDir().DS.'..'.DS.'assets'.DS.'public');
    }

    /**
     * @param $asset
     * @return false|mixed|string
     */
    protected function _getFilename($asset, $fingerprint = true)
    {
        //opening and decoding file is faster than retrieving cache from redis. much faster than file cache
        $manifestFilePath = Mage::getBaseDir().DS.'..'.DS.'assets'.DS.'public'.DS.'manifest.json';
        if (!$fingerprint) {
            $filename = $asset;
        } elseif (file_exists($manifestFilePath)) {
            $manifest = json_decode(file_get_contents($manifestFilePath));
            $filename = isset($manifest->$asset) ? $manifest->$asset : '';
        } else {
            $filename = $asset."?cb=".uniqid();
        }
        return $filename;
    }
}