<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 03/02/2016
 * Time: 11:55
 */
class TradeTested_Design_Helper_Theme extends Mage_Core_Helper_Abstract
{
    public function isMobile()
    {
        return (Mage::getSingleton('core/design_package')->getTheme('frontend') == 'tradetested_mobile');
    }
}