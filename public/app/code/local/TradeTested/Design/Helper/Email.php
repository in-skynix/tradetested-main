<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 09/09/15
 * Time: 15:27
 */
class TradeTested_Design_Helper_Email extends Mage_Core_Helper_Abstract
{
    public function getButtonHtml($text = 'Go', $link = '#', $width = '160', $height = '36')
    {
        $bgUrl = Mage::helper('tradetested_design/asset')->getUrl('images/email/bg_button.png', false);
        return <<<HTML
<div>
<!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{$link}" style="height:{$height}px;v-text-anchor:middle;width:{$width}px;" arcsize="14%" strokecolor="#eeaa00" fill="t">
    <v:fill type="tile" src="{$bgUrl}" color="#fcc000" />
    <w:anchorlock/>
    <center style="color:#333333;font-family:sans-serif;font-size:13px;font-weight:bold;">{$text}</center>
  </v:roundrect>
<![endif]-->
<a href="{$link}" style="background-color:#fcc000;background-image:url({$bgUrl});border:1px solid #eeaa00;border-radius:5px;color:#333333;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:{$height}px;text-align:center;text-decoration:none;width:{$width}px;-webkit-text-size-adjust:none;mso-hide:all;">{$text}</a>
</div>
HTML;

    }
}