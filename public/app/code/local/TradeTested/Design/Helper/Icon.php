<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 24/01/14
 * Time: 4:13 PM
 */
class TradeTested_Design_Helper_Icon extends Mage_Core_Helper_Abstract
{

    public function getEntityCode($name)
    {
        $data = $this->getManifest();
        return isset($data[$name]) ? "&#x{$data[$name]};" : '';
    }

    public function getAllOptions()
    {
        return array_keys($this->getManifest());
    }

    public function getManifest()
    {
        return json_decode(
            file_get_contents(Mage::helper('tradetested_design/asset')->getFilePath('icons.json')),
            true
        );
    }

    /**
     * Get SVG
     * @param string $code
     * @param array $options (width,height,style,class,id)
     * @return false|mixed|string
     */
    public function getSvg(string $code, $options=[])
    {
        $width = ($options['width']??'100%');
        $cache = Mage::app()->getCache();
        $hash = md5(serialize($options));
        $cacheId = "svg.icon.{$code}.{$hash}";
        if(true) {
//        if (!($icon = $cache->load($cacheId))) {
            $doc = new DOMDocument();
            $doc->loadXML(file_get_contents("/assets/images/icons.svg"));
            $xml = new SimpleXMLElement(file_get_contents("/assets/images/icons.svg"));
            $xml->registerXPathNamespace('svg', 'http://www.w3.org/2000/svg');
            $symbol = $xml->xpath("//svg:symbol[@id='{$code}']")[0];
            $attrs = $symbol->attributes();
            $doc = new DOMDocument();
            $svg = $doc->createElementNS('http://www.w3.org/2000/svg', 'svg');
            $svg->setAttribute('viewBox', $attrs['viewBox']);
            $style = $options['style']??'';
            $style .= "width:{$width};";
            if ($options['height']??false) {
                $style .= "height:{$options['height']};";
            }
            $svg->setAttribute('style', $style);
            foreach (['id', 'class'] as $_attr) {
                if ($val = $options[$_attr]) {
                    $svg->setAttribute($_attr, $val);
                }
            }
            foreach ($symbol->children() as $_child) {
                $node  = $doc->importNode(dom_import_simplexml($_child), TRUE);
                $svg->appendChild($node);
            }
            $icon = $doc->saveXML($svg);
            $cache->save($icon, $cacheId, ['TRADETESTED_DESIGN']);
        }
        return $icon;
    }
}