<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 09/03/15
 * Time: 16:11
 */
class TradeTested_Design_ExcludeController extends Mage_Core_Controller_Front_Action
{
    public function setAction()
    {
        Mage::getSingleton('core/cookie')->set('view_full_site', 1);
        $this->_redirectReferer();
    }

    public function unsetAction()
    {
        Mage::getSingleton('core/cookie')->delete('view_full_site');
        $this->_redirectReferer();
    }
}