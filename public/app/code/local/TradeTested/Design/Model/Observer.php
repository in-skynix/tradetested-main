<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 14/08/15
 * Time: 21:38
 */
class TradeTested_Design_Model_Observer
{
    /**
     * Add mobile.xml as last layout update file (except for mobile.xml)
     */
    public function coreLayoutUpdateUpdatesGetAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_Core_Model_Config_Element $updatesRoot */
        $updatesRoot = $observer->getEvent()->getUpdates();
        $updatesRoot->appendChild(new Varien_Simplexml_Element('
                <tradetested_design_mobile>
                    <file>mobile.xml</file>
                </tradetested_design_mobile>
            '));
    }

    /**
     * This allows us to remove all javascript tags from a core block as rendered
     * by simply using setRemoveJavascript in the layout.
     *
     * @param Varien_Event_Observer $observer
     */
    public function coreBlockAbstractToHtmlAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_Core_Block_Abstract $block */
        $block = $observer->getEvent()->getBlock();
        if ($block->getRemoveJavascript()) {
            $transport = $observer->getEvent()->getTransport();
            $doc = new DOMDocument();
            $doc->loadHTML($transport->getHtml());
            $scriptTags = $doc->getElementsByTagName('script');
            $length = $scriptTags->length;
            for ($i = 0; $i < $length; $i++) {
                $scriptTags->item($i)->parentNode->removeChild($scriptTags->item($i));
            }
            $transport->setHtml($doc->saveHTML());
        }
    }
}