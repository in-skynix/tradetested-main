<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 09/03/15
 * Time: 16:07
 */
class TradeTested_Design_Model_Rewrite_Core_Design_Package extends Mage_Core_Model_Design_Package
{
    protected function _checkUserAgentAgainstRegexps($regexpsConfigPath)
    {
        if (Mage::getSingleton('core/cookie')->get('view_full_site')) {
            return false;
        }
        return parent::_checkUserAgentAgainstRegexps($regexpsConfigPath);
    }

    /**
     * $params['_type'] is required
     *
     * @param string $file
     * @param array $params
     * @return string
     */
    public function getFilename($file, array $params)
    {
        if (
            isset($params['_area']) && isset($params['_type'])
            && ($params['_area'] == 'frontend') && ($params['_type'] == 'skin')
        ) {
            return Mage::helper('tradetested_design/asset')->getFilePath($file);
        }
        return parent::getFilename($file, $params);
    }
}