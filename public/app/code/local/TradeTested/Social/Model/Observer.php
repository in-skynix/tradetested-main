<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/11/2015
 * Time: 16:31
 */
class TradeTested_Social_Model_Observer
{
    public function adminhtmlCmsPageEditTabMetaPrepareForm(Varien_Event_Observer $observer)
    {
        /** @var Varien_Data_Form $form */
        $form = $observer->getEvent()->getForm();
        /** @var Varien_Data_Form_Element_Fieldset $fieldset */
        $fieldset = $form->getElement('meta_fieldset');
        $fieldset->addType('cms_page_og_image', 'TradeTested_Social_Block_Adminhtml_Image');
        $fieldset->addField('opengraph_image', 'cms_page_og_image', array(
            'label' => Mage::helper('tradetested_social')->__('OpenGraph Image'),
            'required' => false,
            'name' => 'opengraph_image'
        ));
    }

    public function cmsPagePrepareSave(Varien_Event_Observer $observer)
    {
        /** @var Mage_Cms_Model_Page $page */
        $page = $observer->getEvent()->getPage();
        if(isset($_FILES['opengraph_image']['name']) && $_FILES['opengraph_image']['name']) {
            try {
                $uploader = new Varien_File_Uploader('opengraph_image');
                $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png')); // or pdf or anything
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $path = Mage::getBaseDir('media') . DS . 'cms' . DS . 'page' . DS . 'opengraph' ;
                $result = $uploader->save($path, $_FILES['opengraph_image']['name']);
                /** @var $storageHelper Mage_Core_Helper_File_Storage_Database */
                $storageHelper = Mage::helper('core/file_storage_database');
                $file = $storageHelper->saveUploadedFile($result);
                $page->setData('opengraph_image', $file);
            }catch(Exception $e) {
                Mage::getSingleton('adminhtml/session')->addException($e,
                    Mage::helper('cms')->__('Could not upload image'));
            }
        }
    }
}