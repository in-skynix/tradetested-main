<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/11/2015
 * Time: 17:26
 */
class TradeTested_Social_Block_Adminhtml_Image extends Varien_Data_Form_Element_Image
{
    /**
     * Get image preview url
     *
     * @return string
     */
    protected function _getUrl()
    {
        return Mage::helper('avid_thumbor')->getUrl('cms/page/opengraph/'.$this->getValue())->fitIn(600, 600);
    }
}