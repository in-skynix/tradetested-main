<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/11/2015
 * Time: 14:14
 */
class TradeTested_Social_Block_OpenGraph_Category extends TradeTested_Social_Block_OpenGraph_Default
{
    /**
     * @return Mage_Catalog_Model_Category
     */
    protected function _getCategory()
    {
        return Mage::registry('current_category');
    }

    public function getCanonicalUrl()
    {
        if (!$this->getData('canonical_url')) {
            $this->setData('canonical_url', $this->_getCategory()->getUrl());
        }
        return $this->getData('canonical_url');
    }

    public function getTitle()
    {
        if (!$this->getData('title')) {
            $this->setData('title', $this->_getCategory()->getName() . ' at ' . $this->getSiteName());
        }
        return $this->getData('title');
    }

    public function getImageUrl()
    {
        if (!$this->getData('image_url')) {
            if ($this->_getCategory()->getOpengraphImage()) {
                $imageUrl = $this->helper('avid_thumbor')
                    ->getUrl('catalog/category/'.$this->_getCategory()->getOpengraphImage())
                    ->fitIn(1200, 1200);
                $this->setData('image_url', $imageUrl);
            }
        }
        return $this->getData('image_url');
    }
}