<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/11/2015
 * Time: 14:14
 */
class TradeTested_Social_Block_OpenGraph_Page extends TradeTested_Social_Block_OpenGraph_Default
{
    /**
     * @return Mage_Cms_Model_Page
     */
    protected function _getPage()
    {
        return Mage::getSingleton('cms/page');
    }

    public function getTitle()
    {
        if (!$this->getData('title')) {
            $this->setData('title', $this->_getPage()->getTitle());
        }
        return $this->getData('title');
    }

    public function getImageUrl()
    {
        if (!$this->getData('image_url')) {
            if ($this->_getPage()->getOpengraphImage()) {
                $imageUrl = $this->helper('avid_thumbor')
                    ->getUrl('cms/page/opengraph/'.$this->_getPage()->getOpengraphImage())
                    ->fitIn(1200, 1200);
                $this->setData('image_url', $imageUrl);
            }
        }
        return $this->getData('image_url');
    }
}