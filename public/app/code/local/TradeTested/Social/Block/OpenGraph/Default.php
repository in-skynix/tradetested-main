<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/11/2015
 * Time: 12:21
 */
class TradeTested_Social_Block_OpenGraph_Default extends Mage_Core_Block_Template
{
    protected function  _getOpenGraphDataHelper()
    {
        return Mage::helper('tradetested_social/data_default');
    }

    public function getProperties()
    {
        return array(
            'og:image' => $this->getImageUrl(),
            'og:title' => $this->getTitle(),
            'og:description' => $this->getDescription(),
            'og:url' => $this->getCanonicalUrl(),
            'og:type' => 'website',
            'og:site_name' => $this->getSiteName(),
        );
    }

    public function getSiteName()
    {
        if (!$this->getData('site_name')) {
            $this->setData('site_name', Mage::getStoreConfig('general/store_information/name'));
        }
        return $this->getData('site_name');
    }

    public function getCanonicalUrl()
    {
        if (!$this->getData('canonical_url')) {
            $this->setData('canonical_url', $this->helper('core/url')->getCurrentUrl());
        }
        return $this->getData('canonical_url');
    }

    public function getDescription()
    {
        if (!$this->getData('description')) {
            $this->setData('description', $this->getLayout()->getBlock('head')->getDescription());
        }
        return $this->getData('description');
    }

    public function getTitle()
    {
        if (!$this->getData('title')) {
            $this->setData('title', $this->getLayout()->getBlock('head')->getTitle());
        }
        return $this->getData('title');
    }

    public function getImageUrl()
    {
        if (!$this->getData('image_url')) {
            $this->setData('image_url', '');
        }
        return $this->getData('image_url');
    }
}