<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/11/2015
 * Time: 14:14
 */
class TradeTested_Social_Block_OpenGraph_Product extends TradeTested_Social_Block_OpenGraph_Default
{
    /**
     * @return Mage_Catalog_Model_Product
     */
    protected function _getProduct()
    {
        return Mage::registry('current_product');
    }

    public function getCanonicalUrl()
    {
        if (!$this->getData('canonical_url')) {
            $this->setData('canonical_url', $this->_getProduct()->getProductUrl());
        }
        return $this->getData('canonical_url');
    }

    public function getTitle()
    {
        if (!$this->getData('title')) {
            $this->setData('title', $this->_getProduct()->getName() . ' at ' . $this->getSiteName());
        }
        return $this->getData('title');
    }

    public function getImageUrl()
    {
        if (!$this->getData('image_url')) {
            $imageUrl = $this->helper('catalog/image')->init($this->_getProduct(), 'image')
                ->resize(1200, 1200)
                ->__toString();
            $this->setData('image_url', $imageUrl);
        }
        return $this->getData('image_url');
    }
}