<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 02/11/2015
 * Time: 16:44
 */
class TradeTested_Social_Block_Rewrite_Adminhtml_Cms_Page_Edit_Form extends Mage_Adminhtml_Block_Cms_Page_Edit_Form
{
    protected function _prepareForm()
    {
        $value = parent::_prepareForm();
        $this->getForm()->setEnctype('multipart/form-data');
        return $value;
    }
}