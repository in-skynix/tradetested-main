<?php
$installer = $this;
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer->startSetup();
$attr = array(
    'type'                       => 'varchar',
    'label'                      => 'OpenGraph Image',
    'input'                      => 'image',
    'backend'                    => 'catalog/category_attribute_backend_image',
    'required'                   => false,
    'sort_order'                 => 130,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'group'                      => 'General Information',
);
$installer->addAttribute('catalog_category','opengraph_image',$attr);

$installer->getConnection()->addColumn($installer->getTable('cms/page'), 'opengraph_image', array(
        'length' => 255,
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'OpenGraph Image'
    )
);

if (method_exists($installer->getConnection(), 'resetDdlCache')) {
    $installer->getConnection()->resetDdlCache();
}

$installer->endSetup();
