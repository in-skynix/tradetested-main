<?php
require_once 'JWT/JWT.php';
require_once 'JWT/BeforeValidException.php';
require_once 'JWT/ExpiredException.php';
require_once 'JWT/SignatureInvalidException.php';
use \Firebase\JWT\JWT;

/**
 * Class TradeTested_Api_Model_Jwt
 *
 * JWT Token that can be used to authenticate with other services in the TradeTested ecosystem.
 * @method array getGrants()
 * @method $this setGrants(array $value)
 */
class TradeTested_Api_Model_Jwt extends Mage_Core_Model_Abstract
{
    protected $_privateKey;
    protected $_publicKey;
    protected $_alg = 'RS256';

    /**
     * TradeTested_Api_Model_Jwt constructor.
     *
     * @param array $data
     */
    public function __construct($data = array())
    {
        if (is_string($data)) {
            return $this->fromString($data);
        }

        parent::__construct();
        $defaults = array(
            'iss'       => 'magento@tradetested.co.nz',
            'aud'       => 'api.tradetested.co.nz',
            'iat'       => time(),
            'exp'       => strtotime('+1 day'),
            'nbf'       => strtotime('-5 days'),
            'jti'       => Mage::helper('tradetested_core')->generateGuid(),
            'sub'       => 'api.tradetested.co.nz',
            'identity'  => 'magento@tradetested.co.nz',
            'grants'    => array(),
        );
        $this->setData(array_merge($defaults, $data));
        return $this;
    }

    public function fromString($string)
    {
        $data = JWT::decode($string, $this->_getPublicKey(), array($this->_alg));
        return $this->setData((array)$data);
    }

    /**
     * Add Grant to array of available grants
     *
     * @param $grant
     * @return $this
     */
    public function addGrant($grant)
    {
        return $this->setGrants(array_merge($this->getGrants(), array($grant)));
    }

    /**
     * Get As Encoded String
     *
     * @return string
     */
    public function __toString()
    {
        try {
            return JWT::encode($this->getData(), $this->_getPrivateKey(), $this->_alg);
        } catch (DomainException $e) {
            return '';
        }
    }

    /**
     * @return string
     */
    protected function _getPrivateKey()
    {
        if (!$this->_privateKey) {
            $password = Mage::helper('core')->decrypt(Mage::getStoreConfig('tradetested_api/esb/jwt_private_key_password'));
            $encryptedKey = file_get_contents(Mage::getStoreConfig('tradetested_api/esb/jwt_private_key_location'));
            if ($password) {
                $keyResource = openssl_pkey_get_private(
                    $encryptedKey,
                    Mage::helper('core')->decrypt(Mage::getStoreConfig('tradetested_api/esb/jwt_private_key_password'))
                );
                $key = '';
                openssl_pkey_export($keyResource, $key);
                $this->_privateKey = $key;
            } else {
                $this->_privateKey = $encryptedKey;
            }
        }
        return $this->_privateKey;
    }

    protected function _getPublicKey()
    {
        if (!$this->_publicKey) {
            $keyStr = file_get_contents(Mage::getStoreConfig('tradetested_api/esb/jwt_public_key_location'));
//            $keyResource = openssl_pkey_get_public($keyStr);
//            $key = '';
//            openssl_pkey_export($keyResource, $key);
            $this->_publicKey = $keyStr;
        }
        return $this->_publicKey;
    }

    public function setKey($privateKey, $alg='HS256', $publicKey = null)
    {
        $this->_privateKey = $privateKey;
        $this->_publicKey = $publicKey ? $publicKey : $privateKey;
        $this->_alg = $alg;
        return $this;
    }

}