<?php

/**
 * Based On PHP REST Client
 * https://github.com/tcdent/php-restclient
 */
class RestClientException extends Exception {}

/**
 * Class TradeTested_Api_Model_RestClient
 *
 * @method array getOptions()
 * @method $this setOptions(array $value)
 * @method string getUrl()
 * @method $this setUrl(string $value)
 * @method $this setResponse(string $value)
 * @method object getHeaders()
 * @method $this setHeaders(object $value)
 * @method object getInfo()
 * @method $this setInfo(object $value)
 * @method string getError()
 * @method $this setError(string $value)
 */
class TradeTested_Api_Model_RestClient extends Varien_Object implements Iterator, ArrayAccess {

//    public $options;
    public $handle; // cURL resource handle.

    // Populated after execution:
//    public $response; // Response body.
//    public $headers; // Parsed reponse header object.
//    public $info; // Response info object.
//    public $error; // Response error string.

    // Populated as-needed.
    public $decodedResponse; // Decoded response body.

    public function __construct($options=array())
    {
        parent::__construct();
        $defaultOptions = array(
            'headers' => array(),
            'parameters' => array(),
            'curl_options' => array(),
            'user_agent' => "Magento RestClient/0.1.0",
            'base_url' => Mage::getStoreConfig('tradetested_api/esb/esb_url'),
            'client_cert_location' => Mage::getStoreConfig('tradetested_api/esb/client_cert_location'),
            'ca_cert' => Mage::getStoreConfig('tradetested_api/esb/ca_cert_location'),
            'client_cert_password' => Mage::helper('core')->decrypt(
                Mage::getStoreConfig('tradetested_api/esb/client_cert_password')
            ),
            'content_type' => 'application/json',
            'format' => NULL,
            'format_regex' => "/(\w+)\/(\w+)(;[.+])?/",
            'timeout' => 10,
            'decoders' => array(
                'json' => array($this, '_jsonArrayDecode'),
            ),
            'username' => NULL,
            'password' => NULL
        );

        $this->setOptions(array_merge($defaultOptions, $options));
    }

    protected function _jsonArrayDecode($input)
    {
        return json_decode($input, true);
    }

    /**
     * Add JWT Token to request with a grant
     * @param string $grant
     */
    public function addAuthGrant($grant)
    {
        /** @var TradeTested_Api_Model_Jwt $token */
        $token = Mage::getModel('tradetested_api/jwt');
        $token->addGrant($grant);
        return $this->addRequestHeader('Authorization', "JWT {$token->__toString()}");
    }

    protected function  addRequestHeader($key, $value)
    {
        $headers = $this->getOption('headers');
        $headers[$key] = $value;
        return $this->setOption('headers', $headers);
    }

    public function setOption($key, $value)
    {
        return $this->setOptions(array_merge($this->getOptions(), array($key => $value)));
    }

    public function getOption($key)
    {
        $options = $this->getOptions();
        return isset($options[$key]) ? $options[$key] : null;
    }

    public function registerDecoder($format, $method)
    {
        // Decoder callbacks must adhere to the following pattern:
        //   array my_decoder(string $data)
        return $this->setOption('decoders', array_merge($this->getOption('decoders'), array($format => $method)));
    }

    // Iterable methods:
    public function rewind()
    {
        $this->decodeResponse();
        return reset($this->decodedResponse);
    }

    public function current()
    {
        return current($this->decodedResponse);
    }

    public function key()
    {
        return key($this->decodedResponse);
    }

    public function next()
    {
        return next($this->decodedResponse);
    }

    public function valid()
    {
        return is_array($this->decodedResponse)
        && (key($this->decodedResponse) !== NULL);
    }

    // ArrayAccess methods:
    public function offsetExists($key)
    {
        $this->decodeResponse();
        return is_array($this->decodedResponse)?
            isset($this->decodedResponse[$key]) : isset($this->decodedResponse->{$key});
    }

    public function offsetGet($key)
    {
        $this->decodeResponse();
        if(!$this->offsetExists($key)) {
            return null;
        }

        return is_array($this->decodedResponse)?
            $this->decodedResponse[$key] : $this->decodedResponse->{$key};
    }

    public function offsetSet($key, $value)
    {
        throw new RestClientException("Decoded response data is immutable.");
    }

    public function offsetUnset($key)
    {
        throw new RestClientException("Decoded response data is immutable.");
    }

    // Request methods:
    /**
     * @param $url
     * @param array $parameters
     * @param array $headers
     * @return TradeTested_Api_Model_RestClient
     * @throws RestClientException
     */
    public function get($url, $parameters=array(), $headers=array())
    {
        return $this->execute($url, 'GET', $parameters, $headers);
    }

    /**
     * @param $url
     * @param array $parametersOrBody
     * @param array $headers
     * @return TradeTested_Api_Model_RestClient
     * @throws RestClientException
     */
    public function post($url, $parametersOrBody=array(), $headers=array())
    {
        return $this->execute($url, 'POST', $parametersOrBody, $headers);
    }

    /**
     * @param $url
     * @param array $parametersOrBody
     * @param array $headers
     * @return TradeTested_Api_Model_RestClient
     * @throws RestClientException
     */
    public function put($url, $parametersOrBody=array(), $headers=array())
    {
        return $this->execute($url, 'PUT', $parametersOrBody, $headers);
    }

    /**
     * @param $url
     * @param array $parametersOrBody
     * @param array $headers
     * @return TradeTested_Api_Model_RestClient
     * @throws RestClientException
     */
    public function patch($url, $parametersOrBody=array(), $headers=array())
    {
        return $this->execute($url, 'PATCH', $parametersOrBody, $headers);
    }

    /**
     * @param $url
     * @param array $parameters
     * @param array $headers
     * @return TradeTested_Api_Model_RestClient
     * @throws RestClientException
     */
    public function delete($url, $parameters=array(), $headers=array())
    {
        return $this->execute($url, 'DELETE', $parameters, $headers);
    }

    /**
     * @param $url
     * @param string $method
     * @param array $parameters
     * @param array $headers
     * @return TradeTested_Api_Model_RestClient
     * @throws RestClientException
     */
    public function execute($url, $method='GET', $parameters=array(), $headers=array())
    {
        /** @var $this $client */
        $client = clone $this;
        $client->setUrl($url);
        $client->handle = curl_init();
        $curlopt = array(
            CURLOPT_HEADER => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_USERAGENT => $client->getOption('user_agent'),
            CURLOPT_TIMEOUT => $this->getOption('timeout')
        );

        if ($certLocation = $client->getOption('client_cert_location')) {
            $curlopt[CURLOPT_SSLCERT] = $certLocation;
            $curlopt[CURLOPT_SSLCERTPASSWD] = $client->getOption('client_cert_password');
            if ($caCert = $client->getOption('ca_cert')) {
                $curlopt[CURLOPT_CAINFO] = $caCert;
            }
        }

        if($client->getOption('username') && $client->getOption('password')) {
            $curlopt[CURLOPT_USERPWD] = sprintf(
                "%s:%s", $client->getOption('username'), $client->getOption('password')
            );
        }

        if($contentType = $client->getOption('content_type')) {
            $client->addRequestHeader('Content-Type', $contentType);
        }

        if(count($client->getOption('headers')) || count($headers)) {
            $curlopt[CURLOPT_HTTPHEADER] = array();
            $headers = array_merge($client->getOption('headers'), $headers);
            foreach($headers as $key => $value) {
                $curlopt[CURLOPT_HTTPHEADER][] = sprintf("%s:%s", $key, $value);
            }
        }

        if($client->getOption('format') && $client->getOption('append_format')) {
            $url .= '.'.$client->getOption('format');
        }

        // Allow passing parameters as a pre-encoded string (or something that
        // allows casting to a string). Parameters passed as strings will not be
        // merged with parameters specified in the default options.
        if(is_array($parameters)) {
            $parameters = array_merge($client->getOption('parameters'), $parameters);
            $parameters_string = $client->formatQuery($parameters);
        } else {
            $parameters_string = (string) $parameters;
            $client->addRequestHeader('Content-Length', strlen($parameters));
        }

        if(strtoupper($method) == 'POST') {
            $curlopt[CURLOPT_POST] = TRUE;
            $curlopt[CURLOPT_POSTFIELDS] = $parameters_string;
        } elseif(strtoupper($method) != 'GET') {
            $curlopt[CURLOPT_CUSTOMREQUEST] = strtoupper($method);
            $curlopt[CURLOPT_POSTFIELDS] = $parameters_string;
        } elseif($parameters_string) {
            $url .= strpos($url, '?')? '&' : '?';
            $url .= $parameters_string;
        }

        if($client->getOption('base_url')) {
            if($url[0] != '/' && substr($client->getOption('base_url'), -1) != '/') {
                $url = '/' . $url;
            }
            $url = $client->getOption('base_url') . $url;
        }
        $curlopt[CURLOPT_URL] = $url;

        if($client->getOption('curl_options')) {
            // array_merge would reset our numeric keys.
            foreach($client->getOption('curl_options') as $key => $value) {
                $curlopt[$key] = $value;
            }
        }
        curl_setopt_array($client->handle, $curlopt);

        $client->parseResponse(curl_exec($client->handle));
        $client->setInfo((object)curl_getinfo($client->handle));
        $client->setError(curl_error($client->handle));

        curl_close($client->handle);
        $client->setUrl($url);
        if ($client->getInfo()->http_code > 400) {
            throw new RestClientException($client->getData('response'), $client->getInfo()->http_code);
        }
        return $client;
    }

    public function formatQuery($parameters, $primary='=', $secondary='&')
    {
        $query = "";
        foreach($parameters as $key => $value) {
            $pair = array(urlencode($key), urlencode($value));
            $query .= implode($primary, $pair) . $secondary;
        }
        return rtrim($query, $secondary);
    }

    public function parseResponse($response)
    {
        if (!$response) {
            return;
        }
        list($headers, $response) = explode("\r\n\r\n", $response, 2);
        $headerLines = explode("\n", $headers);
        $headers = array();
        foreach($headerLines as $_line) {
            if (strpos($_line, ':') === false) {
                continue;
            }
            list($key, $value) = explode(':', $_line, 2);
            $key = trim(strtolower(str_replace('-', '_', $key)));
            $value = trim($value);
            if(empty($headers[$key])) {
                $headers[$key] = $value;
            } elseif(is_array($headers[$key])) {
                $headers[$key][] = $value;
            } else {
                $headers[$key] = array($headers[$key], $value);
            }
        }
        $this->setHeaders((object)$headers);
        $this->setResponse($response);
    }

    public function getResponseFormat()
    {
        if($this->getData('response') == null) {
            throw new RestClientException("A response must exist before it can be decoded.");
        }

        // User-defined format.
        if(!empty($this->getOption('format'))) {
            return $this->getOption('format');
        }

        // Extract format from response content-type header.
        if(
            !empty($this->getHeaders()->content_type)
            && preg_match($this->getOption('format_regex'), $this->getHeaders()->content_type, $matches)
        ) {
            return $matches[2];
        }

        throw new RestClientException("Response format could not be determined.");
    }

    public function getResponse()
    {
        return $this->decodeResponse();
    }

    public function decodeResponse()
    {
        if(empty($this->decodedResponse)){
            $format = $this->getResponseFormat();
            if(!array_key_exists($format, $this->getOption('decoders')))
                throw new RestClientException("'${format}' is not a supported ".
                    "format, register a decoder to handle this response.");

            $decoders = $this->getOption('decoders');
            $this->decodedResponse = call_user_func($decoders[$format], $this->getData('response'));
        }

        return $this->decodedResponse;
    }
}

