<?php
class TradeTested_Testimonials_Block_Testimonial extends Mage_Core_Block_Template
{
    protected $_currentTestimonial;
    public function getLink()
    {
        
    }
    
    public function getReadAllLink()
    {
        return "/testimonials";
    }
    public function getCurrentTestimonial()
    {
        //@todo: choose testimonial by category, page, specified id in layout, etc.
        if (!$this->_currentTestimonial) {
            $collection = Mage::getModel('tradetested_testimonials/testimonial')->getCollection()->setFromDateOrder();
            $this->_currentTestimonial = $collection->getRandomSoftFilteredItem();
        }
        return $this->_currentTestimonial;
    }

    public function getRandomTestimonials($limit = null)
    {
        $storeId = Mage::app()->getStore()->getId();
        $collection = Mage::getModel('tradetested_testimonials/testimonial')->getCollection()
            ->setFromDateOrder();
        if (!Mage::app()->isSingleStoreMode())  $collection->addStoreFilter($storeId);
        if($category = $this->getCategory()) $collection->addCategoryFilter($category);
        if ($limit) $collection->getSelect()->limit($limit);
        $collection->setRandomOrder();
        return $collection;
    }

    public function clearCurrentTestimonial()
    {
        $this->_currentTestimonial = null;
        return $this;
    }
    public function getAllTestimonials()
    {
        return $this->getCollection();
    }


	public function __construct()
    {
        parent::__construct();
        $collection = Mage::getModel('tradetested_testimonials/testimonial')->getCollection();
        if (!Mage::app()->isSingleStoreMode()) {
            $collection->addStoreFilter(Mage::app()->getStore()->getId());
        }
        $collection->setOrder('sort_order', 'ASC');
        $this->setCollection($collection);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager->setAvailableLimit(array(20=>20,'all'=>'all'));
        $pager->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getCategory()
    {
        if (!$this->getData('category')) {
            $category = Mage::registry('current_category')
                ? Mage::registry('current_category')
                : Mage::getModel('catalog/category')->load($this->getCategoryId());
            $this->setData('category', $category);
        }
        return $this->getData('category');
    }

    public function getCategoryId()
    {
        if (!$this->getData('category_id')) {
            if (!$this->getProduct()) {
                $this->setProduct(Mage::getModel('catalog/product')->load($this->getProductId()));
            }
            $this->setCategoryId($this->getProduct()->getDeepestCategoryId());
        }
        return $this->getData('category_id');
    }
}