<?php
class TradeTested_Testimonials_Block_Adminhtml_List extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'tradetested_testimonials';
        $this->_headerText = Mage::helper('cms')->__('Testimonials');
        $this->_addButtonLabel = Mage::helper('cms')->__('Add New Testimonial');
        parent::__construct();
    }

    protected function _prepareLayout()
    {
        $this->setChild( 'grid',
            $this->getLayout()->createBlock('tradetested_testimonials/adminhtml_grid',
                $this->_controller . '.grid')->setSaveParametersInSession(true) );
        foreach ($this->_buttons as $level => $buttons) {
            foreach ($buttons as $id => $data) {
                $childId = $this->_prepareButtonBlockId($id);
                $this->_addButtonChildBlock($childId);
            }
        }
        return $this;
    }
}
