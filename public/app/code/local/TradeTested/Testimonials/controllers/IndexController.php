<?php
/**
 * Avid Online Enterprises Limited.
 * User: Dane
 * Date: 29/03/12
 * Time: 4:20 PM
 * Copyright (c) 2011 Avid Online Enterprises Limited. All Rights Reserved.
 */
class TradeTested_Testimonials_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * The search action /search?q=search_term
     *
     * Sets the search query into the session, and then renders the results blocks.
     */
    public function indexAction()
    {
        $this->loadLayout();
        if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbs->addCrumb('home', array('label'=>Mage::helper('cms')->__('Home'), 'title'=>Mage::helper('cms')->__('Home Page'), 'link'=>Mage::getBaseUrl()));
            $breadcrumbs->addCrumb('search', array('label'=>'Testimonials'));
        }
        $this->renderLayout();
    }
}
