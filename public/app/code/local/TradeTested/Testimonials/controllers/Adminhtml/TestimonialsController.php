<?php
class TradeTested_Testimonials_Adminhtml_TestimonialsController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Init actions
     *
     * @return Mage_Adminhtml_Cms_BlockController
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
            ->_setActiveMenu('faqs/index')
            ->_addBreadcrumb(Mage::helper('cms')->__('CMS'), Mage::helper('cms')->__('CMS'))
            ->_addBreadcrumb(Mage::helper('cms')->__('Testimonials'), Mage::helper('cms')->__('Testimonials'))
        ;
        return $this;
    }

    public function indexAction() {
        if ($this->getRequest()->isPost()) {

        }
        $this->loadLayout();
        $this->renderLayout();
    }
    /**
     * Grid for AJAX request
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    /**
     * Create new CMS block
     */
    public function newAction()
    {
        // the same form is used to create and edit
        $this->editAction();
    }

    /**
     * Edit CMS block
     */
    public function editAction()
    {
        $this->_title($this->__('CMS'))->_title($this->__('Testimonials'));

        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('testimonial_id');
        $model = Mage::getModel('tradetested_testimonials/testimonial');

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (! $model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cms')->__('This item no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }

        $this->_title($model->getId() ? $model->getTitle() : $this->__('New Testimonial'));

        // 3. Set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) {
            $model->setData($data)->setCatalogCategoryIds($data['category_ids']);
        }

        // 4. Register model to use later in blocks
        Mage::register('tradetested_testimonials.testimonial', $model);

        // 5. Build edit form
        $this->_initAction()
            ->_addBreadcrumb($id ? Mage::helper('cms')->__('Edit Testimonial') : Mage::helper('cms')->__('New Testimonieal'), $id ? Mage::helper('cms')->__('Edit Testimonial') : Mage::helper('cms')->__('New Testimonial'))
            ->renderLayout();
    }

    /**
     * Save action
     */
    public function saveAction()
    {
        // check if data sent
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('testimonial_id');
            $model = Mage::getModel('tradetested_testimonials/testimonial')->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cms')->__('This item no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
            $model->setData($data)->setCatalogCategoryIds($data['category_ids']);
            // try to save it
            try {
                // save the data
                $model->save();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('cms')->__('The testimonial has been saved.'));
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('testimonial_id' => $model->getId()));
                    return;
                }
                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // save data in session
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                // redirect to edit form
                $this->_redirect('*/*/edit', array('testimonial_id' => $this->getRequest()->getParam('testimonial_id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        // check if we know what should be deleted
        if ($id = $this->getRequest()->getParam('testimonial_id')) {
            $title = "";
            try {
                // init model and delete
                $model = Mage::getModel('tradetested_testimonials/testimonial');
                $model->load($id);
                $title = $model->getTitle();
                $model->delete();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('cms')->__('The item has been deleted.'));
                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // go back to edit form
                $this->_redirect('*/*/edit', array('testimonial_id' => $id));
                return;
            }
        }
        // display error message
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cms')->__('Unable to find a testimonial to delete.'));
        // go to grid
        $this->_redirect('*/*/');
    }

    public function savepositionsAction()
    {
        $positions = $this->getRequest()->getParam('positions');
        $ids = array_keys($positions);
        $collection = Mage::getModel('tradetested_testimonials/testimonial')->getCollection();
        $collection->addFieldToFilter('testimonial_id',
            array(
                'in'        => $ids,
            )
        );
        foreach ($collection as $item) {
            $item->setPosition($positions[$item->getId()])->save();
        }
    }

    public function categoriesJsonAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('tradetested_testimonials/adminhtml_edit_tab_categories')
                ->getCategoryChildrenJson($this->getRequest()->getParam('category'))
        );
    }
}