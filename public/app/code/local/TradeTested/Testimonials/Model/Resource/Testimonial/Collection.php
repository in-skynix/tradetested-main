<?php
class TradeTested_Testimonials_Model_Resource_Testimonial_Collection  extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Define resource model
     *
     */
    protected function _construct()
    {
        $this->_init('tradetested_testimonials/testimonial');
        $this->_map['fields']['store'] = 'store_table.store_id';
    }

    /**
     * Get SQL for get record count.
     * Extra GROUP BY strip added.
     *
     * @return Varien_Db_Select
     */
    public function getSelectCountSql()
    {
        $countSelect = parent::getSelectCountSql();
        $countSelect->reset(Zend_Db_Select::GROUP);
        return $countSelect;
    }

    public function setFromDateOrder($fromDate = null)
    {
        if (!$fromDate) {
            $fromDate = strtotime('-4 months');
        }
        $formattedDate = date('Y-m-d', $fromDate);
        $this->getSelect()->order(
            new Zend_Db_Expr("IF (created_at <= '{$formattedDate}', TIMESTAMPDIFF(DAY,created_at,NOW()), 0)")
        );
        return $this;
    }

    public function setRandomOrder()
    {
        $this->getSelect()->order(new Zend_Db_Expr('RAND()'));
        return $this;
    }
    public function getRandomSoftFilteredItem()
    {
        $this->getSelect()->limit(1);
        $storeId = Mage::app()->getStore()->getId();
        if (!Mage::app()->isSingleStoreMode())  $this->addStoreFilter($storeId);
        if($category = Mage::registry('current_category')) $this->addCategoryFilter($category);
        $item = $this->setRandomOrder()->getFirstItem();
        if (!$item->getId()) {
            $collection = Mage::getModel('tradetested_testimonials/testimonial')->getCollection()
                ->addStoreFilter($storeId)
                ->setRandomOrder();
            $collection->getSelect()->limit(1);
            $item = $collection->getFirstItem();
        }
        return $item;
    }

    /**
     * Add filter by store
     *
     * @param int|Mage_Core_Model_Store $store
     * @param bool $withAdmin
     * @return Mage_Cms_Model_Resource_Block_Collection
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        if ($store instanceof Mage_Core_Model_Store) {
            $store = array($store->getId());
        }
        if (!is_array($store)) {
            $store = array($store);
        }
        if ($withAdmin) {
            $store[] = Mage_Core_Model_App::ADMIN_STORE_ID;
        }
        $this->getSelect()->join(
            array('store_table' => $this->getTable('tradetested_testimonials/testimonial_store')),
            'main_table.testimonial_id = store_table.testimonial_id',
            array()
        );
        $this->addFieldToFilter('store_id', array('in' => $store));
        return $this;
    }

    /**
     * Add filter by category
     */
    public function addCategoryFilter($cat)
    {
        if (!($cat instanceof Mage_Catalog_Model_Category))
            $cat = Mage::getModel('catalog/category')->load($cat);
        $ids = array($cat->getId());
        $parentIds = array_reverse(explode('/', $cat->getPath()));
        // Don't use the first two levels of categories for Trade Tested as they are generic.
        array_splice($parentIds, 3, 3);
        foreach ($parentIds as $id) $ids[] = $id;

        // Where category ID in the set, or IS NULL. Trade Tested Requires that if cannot find one in category, then
        // choose one with NO category. Hence we require categoryId != 0 in the join, then sort so NULL catalog_category_id is
        // last, then order by find in set, (then by random within catalog_category_id if setRandomOrder is set later)
        $this->addFieldToFilter(array('catalog_category_id', 'catalog_category_id'), array(array('in' => $ids), array('null' => true)));
        $this->getSelect()->joinLeft(
            array('category_table' => $this->getTable('tradetested_testimonials/testimonial_catalog_category')),
            'main_table.testimonial_id = category_table.testimonial_id AND category_table.catalog_category_id != 0',
            array()
        );
        $this->getSelect()
            ->order(new Zend_Db_Expr('category_table.catalog_category_id IS NULL'))
            ->order(new Zend_Db_Expr("FIND_IN_SET(category_table.catalog_category_id, '".implode(',', $ids)."')"))
        // CANNOT use group_by as this picks random values for the catalog_category_id.
//            ->group('main_table.testimonial_id')
        ;
        return $this;
    }

    /**
     * Join store relation table if there is store filter
     */
    protected function _renderFiltersBefore()
    {
         if ($this->getFilter('store')) {
            $this->getSelect()->join(
                array('store_table' => $this->getTable('tradetested_testimonials/testimonial_store')),
                'main_table.testimonial_id = store_table.testimonial_id',
                array()
            );
            /*
             * Allow analytic functions usage because of one field grouping
             */
            $this->_useAnalyticFunction = true;
        }
        if ($this->getFilter('catalog_category_id')) {

            /*
             * Allow analytic functions usage because of one field grouping
             */
            $this->_useAnalyticFunction = true;
        }
        return parent::_renderFiltersBefore();
    }

    /**
     * Add Item
     *
     * Overloaded to not throw exception on multiple items with same ID, because our collection often cannot use
     * GROUP BY etc without removing good sorting or causing performance issues.
     *
     * @param Varien_Object $item
     * @return $this|Varien_Data_Collection
     * @throws Exception
     */
    public function addItem(Varien_Object $item)
    {
        $itemId = $this->_getItemId($item);

        if (!is_null($itemId)) {
            if (!isset($this->_items[$itemId])) {
                $this->_items[$itemId] = $item;
            }
        } else {
            $this->_addItem($item);
        }
        return $this;
    }
}
