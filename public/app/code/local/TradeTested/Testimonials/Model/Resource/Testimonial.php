<?php
class TradeTested_Testimonials_Model_Resource_Testimonial extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('tradetested_testimonials/testimonial', 'testimonial_id');
    }
    /**
     * Perform operations before object save
     *
     * @param Mage_Cms_Model_Block $object
     * @return Mage_Cms_Model_Resource_Block
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        if (! $object->getId()) {
            $object->setCreationTime(Mage::getSingleton('core/date')->gmtDate());
        }
        $object->setUpdateTime(Mage::getSingleton('core/date')->gmtDate());
        return $this;
    }
    /**
     * Process block data before deleting
     *
     * @param Mage_Core_Model_Abstract $object
\     */
    protected function _beforeDelete(Mage_Core_Model_Abstract $object)
    {
        $this->_getWriteAdapter()->delete($this->getTable('tradetested_testimonials/testimonial_store'), array('testimonial_id = ?' => (int) $object->getId()));
        return parent::_beforeDelete($object);
    }
    /**
     * Perform operations after object save
     *
     * @param Mage_Core_Model_Abstract $object
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        $oldStores = $this->lookupStoreIds($object->getId());
        $newStores = (array)$object->getStores();
        $table  = $this->getTable('tradetested_testimonials/testimonial_store');
        $insert = array_diff($newStores, $oldStores);
        $delete = array_diff($oldStores, $newStores);
        if ($delete && count($delete)) {
            $where = array(
                'testimonial_id = ?'     => (int) $object->getId(),
                'store_id IN (?)' => $delete
            );
            $this->_getWriteAdapter()->delete($table, $where);
        }
        if ($insert && count($insert)) {
            $data = array();
            foreach ($insert as $storeId) {
                $data[] = array(
                    'testimonial_id'  => (int) $object->getId(),
                    'store_id' => (int) $storeId
                );
            }
            $this->_getWriteAdapter()->insertMultiple($table, $data);
        }

        $oldCats = $this->lookupCatalogCategoryIds($object->getId());
        $newCats = (array)$object->getCatalogCategoryIds();
        $table  = $this->getTable('tradetested_testimonials/testimonial_catalog_category');
        $insert = array_diff($newCats, $oldCats);
        $delete = array_diff($oldCats, $newCats);
        if ($delete && count($delete)) {
            $where = array(
                'testimonial_id = ?'     => (int) $object->getId(),
                'catalog_category_id IN (?)' => $delete
            );
            $this->_getWriteAdapter()->delete($table, $where);
        }
        if ($insert && count($insert)) {
            $data = array();
            foreach ($insert as $catId) {
                if ($catId > 0) {
                    $data[] = array(
                        'testimonial_id' => (int)$object->getId(),
                        'catalog_category_id' => (int)$catId
                    );
                }
            }
            $this->_getWriteAdapter()->insertMultiple($table, $data);
        }

        return parent::_afterSave($object);
    }
    /**
     * Perform operations after object load
     *
     * @param Mage_Core_Model_Abstract $object
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        if ($object->getId()) {
            $stores = $this->lookupStoreIds($object->getId());
            $categories = $this->lookupCatalogCategoryIds($object->getId());
            $object->setData('store_id', $stores);
            $object->setData('catalog_category_ids', $categories);
        }
        return parent::_afterLoad($object);
    }

    /**
     * Retrieve select object for load object data
     *
     * @param string $field
     * @param mixed $value
=     * @return Zend_Db_Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);
        if ($object->getStoreId()) {
            $stores = array(
                (int) $object->getStoreId(),
                Mage_Core_Model_App::ADMIN_STORE_ID,
            );
            $select->join(
                array('tts' => $this->getTable('tradetested_testimonials/testimonial_store')),
                $this->getMainTable().'.testimonial_id = tts.block_id',
                array('store_id')
            )->where('status = ?', 'enabled')
                ->where('tts.store_id in (?) ', $stores)
                ->order('store_id DESC')
                ->limit(1);
        }
        return $select;
    }
    /**
     * Get store ids to which specified item is assigned
     *
     * @param int $id
     * @return array
     */
    public function lookupStoreIds($id)
    {
        $adapter = $this->_getReadAdapter();
        $select  = $adapter->select()
            ->from($this->getTable('tradetested_testimonials/testimonial_store'), 'store_id')
            ->where('testimonial_id = :testimonial_id');
        $binds = array(
            ':testimonial_id' => (int) $id
        );
        return $adapter->fetchCol($select, $binds);
    }

    public function lookupCatalogCategoryIds($id)
    {
        $adapter = $this->_getReadAdapter();
        $select  = $adapter->select()
            ->from($this->getTable('tradetested_testimonials/testimonial_catalog_category'), 'catalog_category_id')
            ->where('testimonial_id = :testimonial_id');
        $binds = array(
            ':testimonial_id' => (int) $id
        );
        return $adapter->fetchCol($select, $binds);
    }
}
