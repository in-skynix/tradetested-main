<?php
class TradeTested_Testimonials_Model_Testimonial extends Mage_Core_Model_Abstract
{
    const CACHE_TAG     = 'tradetested_testimonials.testimonial';
    protected $_cacheTag= 'tradetested_testimonials.testimonial';

    protected function _construct()
    {
        $this->_init('tradetested_testimonials/testimonial');
    }

    public function getAvailableStatuses()
    {
        return array(
           '1' => 'Enabled',
           '0' => 'Disabled',
        );

    }
    public function getCatalogCategoryIds()
    {
        if (is_string($this->getData('catalog_category_ids'))) {
            $ids = explode(',', $this->getData('catalog_category_ids'));
            $ids = array_filter(array_unique($ids));
            $this->setData('catalog_category_ids', $ids);
        }
        return $this->getData('catalog_category_ids');
    }

    /**
     * Set Extra Data before Save.
     *
     * @return Mage_Core_Model_Abstract|void
     */
    protected function _beforeSave()
    {
        $this->setUpdatedAt(date('Y-m-d H:i:s'));
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(date('Y-m-d H:i:s'));
        }
        return parent::_beforeSave();
    }
}