<?php
$installer = $this;
$installer->startSetup();
$table = $installer->getConnection()
    ->newTable($installer->getTable('tradetested_testimonials/testimonial'))
    ->addColumn('testimonial_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Testimonial ID')
    ->addColumn('cite', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
    ), 'Cite')
    ->addColumn('quote', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ), 'Quote')
    ->addColumn('enabled', Varien_Db_Ddl_Table::TYPE_INTEGER, 1, array(
        'unsigned' => true
    ), 'Enabled')
    ->addColumn('is_featured', Varien_Db_Ddl_Table::TYPE_INTEGER, 1, array(
        'unsigned' => true
    ), 'Is Featured')
    ->addColumn('is_sidebar', Varien_Db_Ddl_Table::TYPE_INTEGER, 1, array(
        'unsigned' => true
    ), 'Is Sidebar')
    ->addColumn('sort_order', Varien_Db_Ddl_Table::TYPE_INTEGER, 8, array(
        'nullable' => true
    ), 'Sort Order')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => true,
        'default' => null,
    ), 'Created At')
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => true,
        'default' => null,
    ), 'Updated At')
    ;
$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('tradetested_testimonials/testimonial_catalog_category'))
    ->addColumn('testimonial_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'Testimonial ID')
    ->addColumn('catalog_category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'Catalog Category ID')
    ->addForeignKey(
        $installer->getFkName(
            'tradetested_testimonials/testimonial_catalog_category',
            'testimonial_id',
            'tradetested_testimonials/testimonial',
            'testimonial_id'
        ),
        'testimonial_id',
        $this->getTable('tradetested_testimonials/testimonial'),
        'testimonial_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName(
            'tradetested_testimonials/testimonial_catalog_category',
            'catalog_category_id',
            'catalog/category',
            'entity_id'
        ),
        'catalog_category_id',
        $this->getTable('catalog/category'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ;
$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('tradetested_testimonials/testimonial_store'))
    ->addColumn('testimonial_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Testimonial ID')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Store ID')
    ->addForeignKey(
        $installer->getFkName(
            'tradetested_testimonials/testimonial_store',
            'testimonial_id',
            'tradetested_testimonials/testimonial',
            'testimonial_id'
        ),
        'testimonial_id',
        $this->getTable('tradetested_testimonials/testimonial'),
        'testimonial_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName(
            'tradetested_testimonials/testimonial_store',
            'store_id',
            'core/store',
            'store_id'
        ),
        'store_id',
        $this->getTable('core/store'),
        'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
;
$installer->getConnection()->createTable($table);

$installer->endSetup();
