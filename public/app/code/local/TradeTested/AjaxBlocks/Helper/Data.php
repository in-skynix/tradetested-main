<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 19/12/14
 * Time: 16:41
 */
require_once dirname(__FILE__).'/../../Checkout/Model/Login/Checker.php';
class TradeTested_AjaxBlocks_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getJson($data)
    {
        $outputData = array();
        //blocks are rendered in the order of the request, hence in the order of the document.
        foreach ($data as $_areaName => $_data) {
            if ($block = $this->_getBlock($_areaName)) {
                $filteredData = $this->_filterData($_areaName, $_data);
                $block = $block->setData($filteredData);
                $outputData[$_areaName] = array(
                    'html' => $block->toHtml(),
                    'data' => $block->getAjaxData()
                );
            }
        }
        return json_encode($outputData);
    }

    /**
     * Process some AJAX 'blocks' without even loading Magento framework at all.
     */
    public function processSkipMagento()
    {
        $blocks = array(
            'checkout_login' => 'TradeTested_Checkout_Model_Login_Checker'
        );
        $data = array();
        foreach ($blocks as $_code => $_klass) {
            if($rawData = $_GET[$_code]) {
                $o = new $_klass;
                $data[$_code] = $o->processSkipMagento($rawData);
            }
        }
        return json_encode($data);
    }

    protected function _getBlock($areaName)
    {
        if (
            !preg_match('/[^a-zA-Z0-9_]+/', $areaName)
            && ($blockName = (string)Mage::getConfig()->getNode("global/ajax_blocks/{$areaName}/class"))
        ) {
            /** @var Mage_Core_Block_Abstract $block */
            $block = Mage::app()->getLayout()->createBlock($blockName, $areaName);
            if ($template = (string)Mage::getConfig()->getNode("global/ajax_blocks/{$areaName}/template")) {
                $block->setTemplate($template);
            }
            return $block;
        }
    }

    protected function _filterData($areaName, $data)
    {
        $filteredData = array();
        $config = Mage::getConfig()->getNode("global/ajax_blocks/{$areaName}/user_data");
        if (!$config) {
            return false;
        }
        /** @var Mage_Core_Model_Config_Element $_node */
        foreach ($config->children() as $_node) {
            $nodeName = $_node->getName();
            if (isset($data[$nodeName]) && $this->_checkType($data[$nodeName], (string)$_node)) {
                $filteredData[$nodeName] = $data[$nodeName];
            }
        }
        return $filteredData;
    }

    protected function _checkType($value, $type)
    {
        switch ($type) {
            case 'integer':
                return is_numeric($value);
            case 'string':
                return is_string($value);
            case 'array':
                return is_array($value);
            default:
                return false;
        }
    }
}