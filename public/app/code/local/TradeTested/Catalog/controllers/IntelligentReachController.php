<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 14/04/2016
 * Time: 18:47
 */
class TradeTested_Catalog_IntelligentReachController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->_checkAuth();
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation(0);
        $helper = Mage::helper('tradetested_catalog/intelligentReach')
            ->setStoreId( $this->getRequest()->getParam('store_id'));
        $this->getResponse()->setHeader('Content-type', 'text/xml')
            ->setBody($helper->getFullXml()->asXML());
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
    }
    
    public function quantitiesAction()
    {
        $this->_checkAuth();
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation(0);
        $helper = Mage::helper('tradetested_catalog/intelligentReach')
            ->setStoreId( $this->getRequest()->getParam('store_id'));
        $this->getResponse()->setHeader('Content-type', 'text/xml')
            ->setBody($helper->getQuantitiesXml()->asXML());
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
    }
    
    protected function _checkAuth()
    {
        $user = $_SERVER['PHP_AUTH_USER'] ?? null;
        $pass = $_SERVER['PHP_AUTH_PW'] ?? null;
        if (
            ($user != Mage::getStoreConfig('tradetested_feeds/intelligent_reach/username')) 
            || ($pass != Mage::getStoreConfig('tradetested_feeds/intelligent_reach/password'))
        ) {
            header('WWW-Authenticate: Basic realm="IR Realm"');
            header('HTTP/1.0 401 Unauthorized');
            echo 'Unauthorized';
            exit;
        }
    }
    
}