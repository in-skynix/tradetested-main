<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 07/04/15
 * Time: 14:15
 */
require_once 'Mage/Catalog/controllers/ProductController.php';
class TradeTested_Catalog_ProductController extends Mage_Catalog_ProductController
{
    /**
     * Customised to show a message and redirect to category if product is disabled.
     */
    public function viewAction()
    {
        // Get initial data from request
        $categoryId = (int) $this->getRequest()->getParam('category', false);
        $productId  = (int) $this->getRequest()->getParam('id');
        $specifyOptions = $this->getRequest()->getParam('options');

        // Prepare helper and params
        $viewHelper = Mage::helper('catalog/product_view');

        $params = new Varien_Object();
        $params->setCategoryId($categoryId);
        $params->setSpecifyOptions($specifyOptions);

        // Render page
        try {
            $viewHelper->prepareAndRender($productId, $this, $params);
        } catch (Exception $e) {
            if ($e->getCode() == $viewHelper->ERR_NO_PRODUCT_LOADED) {
                if (isset($_GET['store'])  && !$this->getResponse()->isRedirect()) {
                    $this->_redirect('');
                } elseif (!$this->getResponse()->isRedirect()) {
                    $product = Mage::getModel('catalog/product')->load($productId);
                    $productName = $product->getName() ? $product->getName() : 'this product';
                    $category = $this->_getCategoryWithProducts($categoryId, $productId);
                    if ($category->getId()) {
                        Mage::getSingleton('catalog/session')
                            ->addNotice(
                                "Sorry, {$productName} is no longer available. ".
                                "Here are some similar products we recommend."
                            );
                        $this->getResponse()
                            ->setRedirect($category->getUrl(), 301)
                            ->sendResponse();
                    } else {
                        Mage::getSingleton('catalog/session')
                            ->addNotice("Sorry, {$productName} is no longer available.");
                        $this->_forward('noRoute');
                    }
                }
            } else {
                Mage::logException($e);
                $this->_forward('noRoute');
            }
        }
    }

    /**
     * @return $this
     */
    public function addActionLayoutHandles()
    {
        parent::addActionLayoutHandles();
        $product = Mage::registry('current_product');
        if (!$product->isAvailable()) {
            $this->getLayout()->getUpdate()->addHandle('product_not_available');
        }
        return $this;
    }

    protected function _getCategoryWithProducts($categoryId, $productId)
    {
        $category = Mage::getModel('catalog/category')->load($categoryId);
        if (!$category->getId()) {
            $categoryId = Mage::getResourceModel('tradetested_catalog/indexer_deepestCategory')
                ->getDeepestCategoryIdForProductId($productId);
            $category = Mage::getModel('catalog/category')->load($categoryId);
        }
        while (($category->getLevel() > 2) && !$category->getProductCount()) {
            $category = $category->getParentCategory();
        }
        return $category;
    }
}