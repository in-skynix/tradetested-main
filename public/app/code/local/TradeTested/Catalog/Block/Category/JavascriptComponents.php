<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 15/06/16
 * Time: 3:13 PM
 */
class TradeTested_Catalog_Block_Category_JavascriptComponents extends Mage_Core_Block_Abstract
{
    /**
     * @return array
     */
    public function getJsComponentConfig()
    {
        return [
            'layered_nav' => ['name' => 'LayeredNavigation'],
            'ajax_cart'   => 
                ['name' => 'AjaxCart', 'config' => Mage::helper('tradetested_checkout/cart')->getAjaxAddToCartJson()],
        ];
    }
    
    public function getCacheKeyInfo()
    {
        return array_merge(parent::getCacheKeyInfo(), [Mage::helper('tradetested_design/theme')->isMobile()]);
    }
    
    public function getCacheLifetime()
    {
        return 86400;
    }
}