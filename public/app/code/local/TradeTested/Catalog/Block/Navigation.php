<?php
class TradeTested_Catalog_Block_Navigation extends Mage_Core_Block_Template
{
    /**
     * Get Categories to show in navigation
     *
     * @param Mage_Catalog_Model_Category $parentCategory
     * @return Mage_Catalog_Model_Resource_Category_Collection
     * @throws Mage_Core_Exception
     */
    public function getChildCategories(Mage_Catalog_Model_Category $parentCategory = null)
    {
        if (!$parentCategory) {
            $parentCategory = $this->getParentCategory();
        }
        $ids = ($i = $parentCategory->getChildCategoryIdCache()) ? $i : $parentCategory->getChildren();
        $collection = Mage::getModel('catalog/category')->getCollection();
        /* @var $collection Mage_Catalog_Model_Resource_Category_Collection */
        $collection
            //->setLoadProductCount(true) //not available if using flat catalog categories, can remove and counts will be loaded via n+1 queries. However, this returns the count including non-visible product anyway! We'll be okay with N+1 as long as we're only loading the count for subcatergories.
            ->addAttributeToSelect('url_key')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('all_children')
            ->addAttributeToSelect('is_anchor')
            ->addAttributeToFilter('is_active', 1)
            ->addAttributeToFilter('include_in_menu', 1)
            ->addIdFilter($ids)
            ->setOrder('position', Varien_Db_Select::SQL_ASC)
            ->joinUrlRewrite()
            ->load();
        return $collection;
    }

    /**
     * @param Mage_Catalog_Model_Category|null $parentCategory
     * @return array
     */
    public function getChildCategoriesWithProducts(Mage_Catalog_Model_Category $parentCategory = null)
    {
        $categories = [];
        foreach ($this->getChildCategories($parentCategory) as $_category) {
            if($_category->getIsActive() && $_category->getProductCount()) {
                $categories[] = $_category;
            }
        }
        return $categories;
    }

    /**
     * @param Mage_Catalog_Model_Category|null $category
     * @return array
     */
    public function getSiblingCategoriesWithProducts(Mage_Catalog_Model_Category $category = null)
    {
        $category = $category ? $category->getParentCategory() : $this->getParentCategory();
        return $this->getChildCategoriesWithProducts($category);
    }

    /**
     * Get Parent Category of all displayed in navigation
     *
     * @return Mage_Catalog_Model_Category
     */
    public function getParentCategory()
    {
        if (!$this->getData('parent_category')) {
            if ($childIds = $this->getCurrentCategory()->getChildren()) {
                $parentCategory = $this->getCurrentCategory();
            } else {
                $parentCategory = $this->getCurrentCategory()->getParentCategory();
            }
            $parentCategory->setChildCategoryIdCache($parentCategory->getChildren());
            $this->setData('parent_category', $parentCategory);
        }
        return $this->getData('parent_category');
    }

    /**
     * Get Current Category we are viewing
     *
     * @return Mage_Catalog_Model_Category
     */
    public function getCurrentCategory()
    {
        if ($category = Mage::registry('current_category_filter')) {
            $this->setData('current_category', $category);
        } elseif (!$this->getData('current_category') && Mage::getSingleton('catalog/layer')) {
            $this->setData('current_category', Mage::getSingleton('catalog/layer')->getCurrentCategory());
        }
        return $this->getData('current_category');
    }

    /**
     * get Store Root Category
     *
     * @return Mage_Catalog_Model_Category
     */
    public function getRootCategory()
    {
        if (!$this->getData('root_category')) {
            $root = Mage::getModel('catalog/category')->load(Mage::app()->getStore()->getRootCategoryId());
            $this->setData('root_category', $root);
        }
        return $this->getData('root_category');
    }

    /**
     * Are we viewing the root category, (or page without category)
     * @return bool
     */
    public function isRootCategory()
    {
        return !!($this->getParentCategory()->getId() == Mage::app()->getStore()->getRootCategoryId());
    }

    /**
     * Get Navigation Title
     *
     * @return string
     */
    public function getTitle()
    {
        if ($this->isRootCategory()) {
            return 'Browse';
        } else {
            return $this->getCurrentCategory()->getName();
        }
    }

    /**
     * Check whether specified category is active
     *
     * @param Varien_Object $category
     * @return bool
     */
    public function isCategoryActive($category)
    {
        return $this->getCurrentCategory()
            ? in_array($category->getId(), $this->getCurrentCategory()->getPathIds()) : false;
    }

    /**
     * Is dropdown menu meant to stay open and not able to be closed?
     * @return bool
     */
    public function isStuckOpen()
    {
        return (boolean)$this->getIsStuckOpen();
    }

    public function getJsonConfig()
    {
        return ['container' => '#catalog_navigation_top', 'ignoreSubmenus' => true];
    }

    public function getCacheLifeTime()
    {
        return 60*60*24;
    }

    public function getCacheKeyInfo()
    {
        $info = parent::getCacheKeyInfo();
        $info[] = $this->isStuckOpen();
        $info[] = $this->getCurrentCategory()->getId();
        return $info;
    }
    
    public function getJsComponentConfig()
    {
        return ['navjs' => 
                    ['name' => 'HeaderMenu', 'ref' => $this->getJsComponentRef(), 'config' => $this->getJsonConfig()]
        ];
    }
}
