<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 04/02/15
 * Time: 17:43
 */
class TradeTested_Catalog_Block_Adminhtml_Form_Element_LineList extends Varien_Data_Form_Element_Label
{


    /**
     * Retrieve Element HTML
     *
     * @return string
     */
    public function getElementHtml()
    {
        $lines  = explode("\n", $this->getEscapedValue());
        $html = '<ul>';
        foreach ($lines as $_line) {
            $html.= '<li>'.$_line.'</li>';
        }
        $html.= '</ul>';
        $html .= '<input id="'.$this->getHtmlId().'" name="'.$this->getName()
            .'" value="'.$this->getEscapedValue().'" disabled="disabled" />'."\n";
        $html.= $this->getAfterElementHtml();
        return $html;
    }
}