<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 14/06/16
 * Time: 2:21 PM
 */
class TradeTested_Catalog_Block_Product_View_RelatedCategories extends Mage_Catalog_Block_Product_Abstract
{
    /** @var  array */
    protected $_pathways;

    /**
     * @return array
     * @throws Mage_Core_Exception
     */
    public function getPathways()
    {
        if (!$this->_pathways) {
            $this->_pathways = [];
            /** @var Mage_Catalog_Model_Category $_category */
            foreach ($this->getProduct()->getCategoryCollection()->setOrder('level') as $_category)  {
                $pathIds = explode(',', $_category->getPathInStore());
                $categories = Mage::getResourceModel('catalog/category_collection')
                    ->setStore(Mage::app()->getStore())
                    ->addAttributeToSelect('name')
                    ->addAttributeToSelect('url_key')
                    ->addFieldToFilter('entity_id', array('in' => $pathIds))
                    ->addFieldToFilter('is_active', 1)
                    ->setOrder('level');
                if (count($categories)) {
                    $this->_pathways[$_category->getId()] = $categories;
                }
            }
        }
        return $this->_pathways;
    }
}