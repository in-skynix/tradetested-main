<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 15/06/16
 * Time: 2:34 PM
 */
class TradeTested_Catalog_Block_Product_View_JavascriptComponents extends Mage_Core_Block_Abstract
{
    public function getJsComponentConfig()
    {
        return [
            'shipping_estimator' => [
                'name'   => 'ProductShippingEstimator',
                'config' => Mage::helper('tradetested_shipping/estimator')->getProductConfigJson(),
            ],
//            'ajax_cart'          => [
//                'name'   => 'AjaxCart',
//                'config' => Mage::helper('tradetested_checkout/cart')->getAjaxAddToCartJson(),
//            ],
        ];
    }

    public function getCacheKeyInfo()
    {
        return [
            Mage::registry('product')->getId(),
            $this->_getApp()->getStore()->getCode(),
            $this->getNameInLayout(),
            Mage::helper('tradetested_regions')->getAreaCode(),
            Mage::helper('tradetested_design/theme')->isMobile(),
        ];
    }
}