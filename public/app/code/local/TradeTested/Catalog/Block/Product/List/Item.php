<?php
class TradeTested_Catalog_Block_Product_List_Item extends Mage_Catalog_Block_Product_Abstract
{
    protected function _construct()
    {
        $this->addData([
            'cache_lifetime' => 60 * 60 * 24,
            'cache_tags' => ['block_html', 'block_html_catalog'],
        ]);
    }

    public function setProduct(Mage_Catalog_Model_Product $product)
    {
        parent::setProduct($product);
        $isOnSpecial = (
            $product->getSpecialPrice() 
            && Mage::app()->getLocale()->isStoreDateInInterval(
                $product->getStore(), $product->getSpecialFromDate(), $product->getSpecialToDate()
            )
        );
        $isOnSpecial = $isOnSpecial ? 'on_special' : 'standard';
        $theme = Mage::getSingleton('core/design_package')->getTheme('frontend');
        //If using parent::getCacheTags, it would include data from previous use of same block

        $this->addData([
            'cache_tags' => array_merge(
                ['block_html', 'block_html_catalog'],
                $product->getCacheIdTags()
            ),
            'cache_key'  => 'product_list_item_' 
                . Mage::app()->getStore()->getId() . '_' 
                . $this->getProduct()->getId() . '_' . $isOnSpecial . '_' . $theme
        ]);
        return $this;
    }

    /**
     * Don't add form key to URL
     *
     * It is added via JS, because the HTML is cached.
     */
    public function getAddToCartUrl($product, $additional = [])
    {
        $url = parent::getAddToCartUrl($product, $additional);
        return preg_replace('/\/form_key\/\w+?\//', '/', $url);
    }
}
