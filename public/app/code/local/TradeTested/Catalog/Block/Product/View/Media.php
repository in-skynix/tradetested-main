<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 27/04/15
 * Time: 14:46
 */
class TradeTested_Catalog_Block_Product_View_Media extends Mage_Catalog_Block_Product_View_Media
{
    public function getGalleryImageJSON()
    {
        $images = array();
        $helper = $this->helper('catalog/image');
        foreach ($this->getGalleryImages() as $_image) {
            /** @var Mage_Catalog_Helper_Image $img */
            $img = $helper->init(Mage::getModel('catalog/product'), 'image', $_image->getFile());
            $images[] = [
                'id'         => $_image->getId(),
                'src_retina' => [
                    'thumbnail' => $img->keepFrame(true)->resize(136, 136)->__toString(),
                    'large'     => $img->keepFrame(false)->resize(1160)->__toString(),
                    'zoom'      => $img->keepFrame(false)->resize(1600, 1200)->__toString(),
                ],
                'src'        => [
                    'thumbnail' => $img->keepFrame(true)->resize(68, 68)->__toString(),
                    'large'     => $img->keepFrame(true)->resize(580)->__toString(),
                    'zoom'      => $img->keepFrame(true)->resize(800, 600)->__toString(),
                ],
                'label'      => $_image->getLabel(),
            ];
        }

//        return ['config' => ['zoomOnly' => !!($this->getZoomOnly())],  'image_data' => $images];
        return [  'image_data' => $images];
    }

    /**
     * @return array
     */
    public function getThumbnailImages()
    {
        $images = [];
        $first = true;
        foreach ($this->getGalleryImages() as $_image) {
            if (!$first) {
                $images[] = $_image;
            }
            $first = false;
        }
        return $images;
    }

    /**
     * @return Varien_Object
     */
    public function getMainImage()
    {
        foreach ($this->getGalleryImages() as $_image) {
            return $_image;
        }
    }
    
    public function getCacheKeyInfo()
    {
        return array_merge(
            parent::getCacheKeyInfo(),
            [
                Mage::helper('tradetested_design/theme')->isMobile(),
                $this->getProduct()->getId(),
            ]
        );
    }

    public function getJsComponentConfig()
    {
        return ['gallery' => 
                    ['name' => 'ProductGallery', 'ref' => 'productGallery', 'config' => $this->getGalleryImageJSON()]
        ];
    }
}