<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 02/02/15
 * Time: 16:48
 */
class TradeTested_Catalog_Block_Product_List_Toolbar extends Mage_Catalog_Block_Product_List_Toolbar
{

    /**
     * @param Varien_Data_Collection $collection
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function setCollection($collection)
    {
        if (($this->getCurrentOrder() == 'sale_count') || ($this->getCurrentOrder() == 'view_count'))
        {
            if (Mage::registry('current_category')){
                $collection->setOrder('position', 'asc'); //take position into account, the other order is later applied
            }
            $this->setData('_current_grid_direction', 'desc'); //Populzr/most viewed always first
        }
        return parent::setCollection($collection);
    }
}