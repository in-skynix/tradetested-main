<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 06/01/15
 * Time: 17:04
 */
class TradeTested_Catalog_Model_Product_Attribute_Backend_Type_IndexData
    extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{
    public function getTable()
    {
        return 'tradetested_catalog_index_product_data';
    }

}