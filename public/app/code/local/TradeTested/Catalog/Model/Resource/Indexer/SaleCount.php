<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 30/09/14
 * Time: 8:56 PM
 */

class TradeTested_Catalog_Model_Resource_Indexer_SaleCount
    extends TradeTested_Catalog_Model_Resource_Indexer_Eav_Abstract
{
    protected $_attributeCode = 'sale_count';

    /**
     * Initialize connection and define main table
     *
     */
    protected function _construct()
    {
        $this->_init('tradetested_catalog/index_product_data', 'product_id');
    }

    public function incrementProductSaleCount($quantities, $storeId)
    {
        $attributeModel = $this->_getAttributeModel();
        $attributeData = array(
            'entity_type_id' => $attributeModel->getEntityTypeId(),
            'attribute_id' => $attributeModel->getAttributeId()
        );
        $tableData = array();
        foreach ($quantities as $_id => $_val) {
            $tableData[] =  array_merge(
                array('entity_id' => $_id, 'value' => $_val, 'store_id' => $storeId),
                $attributeData
            );
        }
        $adapter = $this->_getWriteAdapter();
        $tableName = $attributeModel->getBackendTable();
        $adapter->insertOnDuplicate($tableName, $tableData, array('value' => new Zend_Db_Expr('value+VALUES(value)')));
    }

    /**
     * Get a Select object to update the data from
     *
     * @param array $productIds
     * @return Varien_Db_Select|Zend_Db_Select
     */
    protected function _getSelect($productIds)
    {
        $select = Mage::getModel('sales/order')->getCollection()->getSelect()
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns('store_id')
            ->joinLeft(
                array('items' => $this->getTable('sales/order_item')),
                'main_table.entity_id = items.order_id',
                array(new Zend_Db_Expr('product_id as entity_id'), new Zend_Db_Expr('sum(qty_ordered) as value'))
            )
            ->where("main_table.created_at > DATE_FORMAT(NOW() - INTERVAL 3 MONTH, '%Y-%m-01')")
            ->where("main_table.status IN ('complete', 'processing', 'closed')")
            ->group(array('items.product_id', 'main_table.store_id'))
        ;
        if ($productIds) {
            $select->where('items.product_id IN (?)', $productIds);
        }
        return $select;
    }
}