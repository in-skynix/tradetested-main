<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 30/09/14
 * Time: 8:56 PM
 */

class TradeTested_Catalog_Model_Resource_Indexer_DeepestCategory
    extends TradeTested_Catalog_Model_Resource_Indexer_Eav_Abstract
{
    protected $_attributeCode = 'deepest_category_id';
    protected $_rootCategoryId = -1;

    /**
     * Initialize connection and define main table
     *
     */
    protected function _construct()
    {
        $this->_init('tradetested_catalog/index_product_data', 'product_id');
    }

    /**
     * Get a Select object to update the data from
     *
     * Check commit log for queries and strategies tested.
     *
     * @param array $productIds
     * @return Varien_Db_Select|Zend_Db_Select
     */
    protected function _getSelect($productIds = null)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $maxLevel = $connection->select()
            ->from(
                array('i' => $this->getTable('catalog/category_product_index')),
                array('store_id', 'product_id')
            )
            ->joinLeft(
                array('e' => $this->getTable('catalog/category')),
                'e.entity_id = i.category_id',
                array('level' => new Zend_Db_Expr('max(e.level)'))
            )
            ->where('i.visibility > 1')
            ->group('i.product_id')
            ->group('i.store_id')
        ;
        if ($productIds) {
            $maxLevel->where('i.product_id IN (?)', $productIds);
        }
        $catData = $connection->select()
            ->from(
                array('i' => $this->getTable('catalog/category_product_index')),
                array('store_id', 'product_id', 'category_id')
            )
            ->joinLeft(
                array('e' => $this->getTable('catalog/category')),
                'e.entity_id = i.category_id',
                array('level')
            )
            ->where('i.visibility > 1')
        ;
        if ($productIds) {
            $catData->where('i.product_id IN (?)', $productIds);
        }
        $select = $connection->select()
            ->from(
                array('max_level' => $maxLevel),
                array('store_id' => 'store_id', 'entity_id' => 'product_id')
            )
            ->joinLeft(
                array('cat_data' => $catData),
                'cat_data.level = max_level.level AND cat_data.product_id = max_level.product_id AND cat_data.store_id = max_level.store_id',
                array('value' => 'category_id')
            )
            ->group('max_level.product_id')
            ->group('max_level.store_id')
        ;
        if ($productIds) {
            $select->where('max_level.product_id IN (?)', $productIds);
        }
        return $select;
    }
    
    public function getDeepestCategoryIdForProduct($product, $store = null) 
    {
        if ( ($ids = $product->getCategoryIds()) && count($ids)) {
            $store = $store ? $store : Mage::app()->getStore();
            return $this->getDeepestCategoryId($ids, $store);
        } else {
            return $this->getDeepestCategoryIdForProductId($product->getId(), $store);
        }
        
    }
    
    public function getDeepestCategoryId(array $ids, $store) 
    {
        $storeRootCategoryPath = sprintf('%d/%d', $this->getRootCategoryId(), $store->getRootCategoryId());
        $read = $this->_getReadAdapter();
        $collection = Mage::getResourceModel('catalog/category_collection');
        $collection
            ->addIdFilter($ids)
            ->setStoreId($store->getId())
            ->addPathFilter($storeRootCategoryPath);
        $collection->getSelect()
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns('entity_id')
            ->order('level DESC')
            ->limit(1);
        return $read->fetchOne($collection->getSelect());
    }

    public function getDeepestCategoryIdForProductId($productId, $store = null)
    {
        $store = $store ? $store : Mage::app()->getStore();
        $storeRootCategoryPath = sprintf('%d/%d', $this->getRootCategoryId(), $store->getRootCategoryId());
        $read = $this->_getReadAdapter();
        $collection = Mage::getResourceModel('catalog/category_collection');
        $collection
            ->setStoreId($store->getId())
            ->addPathFilter($storeRootCategoryPath);
        $collection->getSelect()
            ->join(
                array('cp' => $this->getTable('catalog/category_product')),
                $read->quoteInto('cp.category_id = entity_id AND cp.product_id = ?', $productId),
                array()
            )
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns('entity_id')
            ->order('level DESC')
            ->limit(1);
        ;
        return $read->fetchOne($collection->getSelect());
    }

    /**
     * Retrieve root category id
     *
     * @return int
     */
    public function getRootCategoryId()
    {
        if (-1 === $this->_rootCategoryId) {
            $collection = Mage::getResourceModel('catalog/category_collection');
            $collection->addFieldToFilter('parent_id', 0);
            $collection->getSelect()->limit(1);
            $rootCategory = $collection->getFirstItem();
            $this->_rootCategoryId = $rootCategory->getId();
        }
        return $this->_rootCategoryId;
    }
}