<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 30/09/14
 * Time: 8:56 PM
 */

class TradeTested_Catalog_Model_Resource_Indexer_ProductRevenue extends Mage_Catalog_Model_Resource_Product_Indexer_Abstract
{
    /**
     * Initialize connection and define main table
     *
     */
    protected function _construct()
    {
        $this->_init('tradetested_catalog/index_product_revenue', 'product_id');
    }

    public function reindexAll()
    {
        $this->useIdxTable(true);
        $this->beginTransaction();
        try {
            $this->clearTemporaryIndexTable();
            $this->insertFromSelect($this->_getRevenueSelect(), $this->getIdxTable(), array('product_id', 'revenue'));
            $this->commit();
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
        }
        return $this;
    }

    public function reindexProductIds($ids)
    {
        $this->useIdxTable(true);
        $this->beginTransaction();
        try {
            $this->_getWriteAdapter()->delete(
                $this->getMainTable(),
                $this->_getWriteAdapter()->quoteInto($this->getIdFieldName() . ' IN (?)', $ids)
            );
            $this->insertFromSelect(
                $this->_getRevenueSelect()->where('items.product_id IN (?)', $ids),
                $this->getIdxTable(), array('product_id', 'revenue')
            );
            $this->commit();
        } catch (Exception $e) {
            Mage::logException($e);
            $this->rollBack();
            throw $e;
        }
        return $this;
    }

    protected function _getRevenueSelect()
    {
        return Mage::getModel('sales/order')->getCollection()->getSelect()
            ->reset(Zend_Db_Select::COLUMNS)
            ->joinLeft(array('items' => $this->getTable('sales/order_item')), 'main_table.entity_id = items.order_id', array('product_id', new Zend_Db_Expr('sum(base_row_total) as revenue')))
            ->where("main_table.created_at > DATE_FORMAT(NOW() - INTERVAL 3 MONTH, '%Y-%m-01')")
            ->group('items.product_id')
        ;
    }

    /**
     * Retrieve temporary index table name
     *
     * @param string $table
     * @return string
     */
    public function getIdxTable($table = null)
    {
        if ($this->useIdxTable()) {
            return $this->getTable('tradetested_catalog/index_product_revenue');
        }
    }
}