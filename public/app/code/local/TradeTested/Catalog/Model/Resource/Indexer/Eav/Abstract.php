<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 21/01/15
 * Time: 15:49
 */
abstract class TradeTested_Catalog_Model_Resource_Indexer_Eav_Abstract
    extends Mage_Core_Model_Resource_Db_Abstract
{
    protected $_attributeCode;
    protected $_entityTypeCode = 'catalog_product';

    /**
     * Reindex All
     *
     * @return $this
     * @throws Exception
     */
    public function reindexAll()
    {
        return $this->reindexProductIds();
    }

    /**
     * Reindex data for a set of product IDs
     *
     * @param null|array $productIds
     * @return $this
     * @throws Exception
     */
    public function reindexProductIds($productIds = null)
    {
        $this->beginTransaction();
        try {
            $this->_updateFromSelect($this->_getSelect($productIds));
            $this->commit();
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
        }
        Mage::getSingleton('index/indexer')->processEntityAction(
            $this->_getAttributeModel(),
            Mage_Catalog_Model_Resource_Eav_Attribute::ENTITY,
            Mage_Index_Model_Event::TYPE_SAVE
        );
        return $this;
    }

    /**
     * Get The Select that has data for the product IDs
     * Should have entity_id as one column, and value as the next
     *
     * @param array $productIds
     * @return Zend_Db_Select
     */
    abstract protected function _getSelect($productIds);

    /**
     * Update EAV Table with data from select;
     *
     * @param Zend_Db_Select $select that returns rows with one column named 'entity_id', and the other named 'value'
     * @return $this
     */
    protected function _updateFromSelect(Zend_Db_Select $select)
    {
        $query   = $this->_getReadAdapter()->query($select);
        $i      = 0;
        $data   = array();
        while ($_row = $query->fetch(PDO::FETCH_ASSOC)) {
            $i ++;
            // Quick fix
            //We need default values in order for search indexing to work.
            $data[] = array(
                'store_id'  => 0,
                'entity_id' => $_row['entity_id'],
                'value'     => null
            );
            $data[] = $_row;
            if (($i % 1000) == 0) {
                $this->_updateEavTable($data);
                $data = array();
            }
        }
        return $this->_updateEavTable($data);
    }

    /**
     * Update EAV table with data from an array
     *
     * @param $data
     * @return $this
     */
    protected function _updateEavTable($data)
    {
        if (!empty($data)) {
            $attributeModel = $this->_getAttributeModel();
            $attributeData = array(
                'entity_type_id' => $attributeModel->getEntityTypeId(),
                'attribute_id' => $attributeModel->getAttributeId()
            );
            $tableData = array();
            foreach ($data as $_row) {
                $tableData[] =  array_merge($_row, $attributeData);
            }
            $adapter = $this->_getWriteAdapter();
            $tableName = $attributeModel->getBackendTable();
            $adapter->insertOnDuplicate($tableName, $tableData, array('value'));
        }
        return $this;
    }

    /**
     * Get Attribute Model
     *
     * @return Mage_Eav_Model_Entity_Attribute_Abstract
     * @throws Mage_Core_Exception
     */
    protected function _getAttributeModel()
    {
        return Mage::getResourceModel('catalog/eav_attribute')->loadByCode($this->_entityTypeCode, $this->_attributeCode);
    }

}