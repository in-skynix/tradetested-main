<?php
class TradeTested_Catalog_Model_Resource_Indexer_CategoryRevenue extends Mage_Catalog_Model_Resource_Product_Indexer_Abstract
{
    /**
     * Initialize connection and define main table
     *
     */
    protected function _construct()
    {
        $this->_init('tradetested_catalog/index_category_revenue', 'category_id');
    }

    public function reindexAll()
    {
        $this->useIdxTable(true);
        $this->beginTransaction();
        try {
            $this->clearTemporaryIndexTable();
            $this->insertFromSelect($this->_getRevenueSelect(), $this->getIdxTable(), array('revenue', 'category_id'));
            $this->commit();
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
        }
        return $this;
    }

    public function addAmountsToCategoryIds($data = array())
    {
        $this->useIdxTable(true);
        $this->beginTransaction();
        try {
            $values = array();
            foreach($data as $_id => $_revenue) {
                if (is_int($_id) && is_numeric($_revenue)) {
                    $values[] = "({$_id}, {$_revenue})";
                }
            }
            $insertSql = sprintf(
                'INSERT INTO %s (%s) VALUES %s ON DUPLICATE KEY UPDATE revenue = revenue + VALUES(revenue)',
                $this->getMainTable(),
                'category_id, revenue',
                implode(',', $values)
            );
            $this->_getWriteAdapter()->query($insertSql);
            $this->commit();
        } catch (Exception $e) {
            Mage::logException($e);
            $this->rollBack();
            throw $e;
        }
        return $this;
    }

    protected function _getRevenueSelect()
    {
        return Mage::getModel('sales/order')->getCollection()->getSelect()
            ->reset(Zend_Db_Select::COLUMNS)
            ->joinLeft(
                array('items' => $this->getTable('sales/order_item')),
                'main_table.entity_id = items.order_id',
                array(new Zend_Db_Expr('sum(base_row_total) as revenue'))
            )
            ->joinLeft(
                array('categories_products' => $this->getTable('catalog/category_product_index')),
                'items.product_id = categories_products.product_id',
                array('category_id as category_id')
            )
            ->where("main_table.created_at > DATE_FORMAT(NOW() - INTERVAL 3 MONTH, '%Y-%m-01')")
            ->where('categories_products.category_id IS NOT NULL')
            ->group('categories_products.category_id')
        ;
    }

    /**
     * Retrieve temporary index table name
     *
     * @param string $table
     * @return string
     */
    public function getIdxTable($table = null)
    {
        if ($this->useIdxTable()) {
            return $this->getTable('tradetested_catalog/index_category_revenue');
        }
    }
}