<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 05/04/2016
 * Time: 13:25
 */
class TradeTested_Catalog_Model_Rewrite_CatalogInventory_Stock_Item extends Mage_CatalogInventory_Model_Stock_Item
{
    /**
     * Checking quote item quantity
     *
     * Second parameter of this method specifies quantity of this product in whole shopping cart
     * which should be checked for stock availability
     *
     * @param mixed $qty quantity of this item (item qty x parent item qty)
     * @param mixed $summaryQty quantity of this product
     * @param mixed $origQty original qty of item (not multiplied on parent item qty)
     * @return Varien_Object
     */
    public function checkQuoteItemQty($qty, $summaryQty, $origQty = 0)
    {
        $result = parent::checkQuoteItemQty($qty, $summaryQty, $origQty);
        if ($result->getHasError() && (!$this->checkQty($summaryQty) || !$this->checkQty($qty))) {
            $qtyAvailable = ($this->getQty() - $this->getMinQty());
            $message = Mage::helper('cataloginventory')
                ->__('This quantity is not available. Please reduce to %s or less.', $qtyAvailable);
            $result->setMessage($message);
        }
        return $result;
    }
    
}