<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 10/05/13
 * Time: 2:35 PM
 * To change this template use File | Settings | File Templates.
 */
class TradeTested_Catalog_Model_Rewrite_Catalog_Category extends Mage_Catalog_Model_Category
{
    /**
     * Get Product Count with Anchor and not OOS
     * 
     * Cater for anchor categories by using index table rather than category_product
     * 
     * Cater for Out of Stock when counting.
     * 
     * @return int|mixed
     */
    public function getProductCount()
    {
        if (!$this->hasData('product_count')) {
            //get From DB. with correction for out of stock
            $productTable = Mage::getSingleton('core/resource')->getTableName('catalog/category_product_index');
            $select = $this->getResource()->getReadConnection()->select()
                ->from(
                    array('main_table' => $productTable),
                    array(new Zend_Db_Expr('COUNT(main_table.product_id)'))
                )->joinInner('cataloginventory_stock_status', 'main_table.product_id = cataloginventory_stock_status.product_id')
                ->where('main_table.category_id = :category_id')
                ->where('cataloginventory_stock_status.stock_status = 1')
                ->where('website_id = :website_id');

            $bind = array('category_id' => (int)$this->getId(), 'website_id' => (int)Mage::app()->getStore()->getWebsiteId());
            $counts = $this->getResource()->getReadConnection()->fetchOne($select, $bind);
            $this->setData('product_count', intval($counts));
            return $this->getData('product_count');
        }
        if (!$this->getData('product_count')) return 0;
        return parent::getProductCount();
    }

    /**
     * Retrieve Layout Update Handle name
     * 
     * All our categories have same layout, except ones with display_filters set
     * 
     * Anchor categories can be set to show child category products, but not display filters.
     *
     * @return string
     */
    public function getLayoutUpdateHandle()
    {
        return  $this->getDisplayFilters() ? 'catalog_category_filtered' : 'catalog_category_default';
    }
}