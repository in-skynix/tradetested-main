<?php

/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 06/01/15
 * Time: 13:46
 */
class TradeTested_Catalog_Model_Observer
{
    /**
     * Reindex on Order Place
     *
     * Defered until after the order is saved to prevent deadlock issues.
     *
     * Hook on order place to allow reindexing of top-sellers and product_revenue
     *
     * @param $observer
     */
    public function reindexOrderSaveAfter($observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();
        if ($order->getIsNewOrder()) {
            Mage::getSingleton('index/indexer')->processEntityAction(
                $order, Mage_Sales_Model_Order::ENTITY, 'create'
            );
        }
    }

    public function salesOrderSaveBefore(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();
        if (!$order->getId()) {
            $order->setIsNewOrder(true);
        }
    }

    /**
     * When a product is viewed, we increment the view count
     *
     * We do not create log files with a new row for every view.
     *
     * @param $observer
     */
    public function logProductView($observer)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getEvent()->getProduct();

        $viewCount = $product->getViewCount() + 1;
        foreach ([Mage::app()->getStore()->getId(), 0] as $storeId) {
            Mage::getResourceModel("catalog/product_action")->updateAttributes(
                [$product->getId()], ['view_count' => $viewCount],
                $storeId
            );
        }
    }

    /**
     * When a category is saved, set the default position of products to 0 instead of 1
     * because adding a category to a product and saving the product sets its default position to 1
     * and we want the ordering to be consistent no matter how the product was added to the category
     *
     * @param $observer
     */
    public function catalogCategoryPrepareSave($observer)
    {
        $category = $observer->getEvent()->getCategory();
        $data = $category->getPostedProducts();
        if (is_array($data)) {
            foreach ($data as $_productId => $_position) {
                if ($_position == '') {
                    $data[$_productId] = '1';
                }
            }
            $category->setPostedProducts($data);
        }
    }

    /**
     * When product is loaded on frontend, set current category to deepest
     *
     * @param Varien_Event_Observer $observer
     */
    public function catalogControllerProductInitAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getEvent()->getProduct();
        Mage::helper('tradetested_catalog')->setDeepestCategoryOnProduct($product);
        Mage::unregister('current_category');
        Mage::register('current_category', $product->getCategory());
    }

    /**
     * When adding stock status filter to collections loaded for adding to TradeMe,
     * ensure that products with a stock description
     *
     * @param Varien_Event_Observer $observer
     */
    public function mventoryTrademeProductCollectionAddStockStatusFilterAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $productCollection */
        $productCollection = $observer->getEvent()->getProductCollection();
        $productCollection->addAttributeToFilter('stock_description', [['null' => true], ''], 'left');
    }

    /**
     * When we are loading the SKUs for TradeMe listed products that have gone OOS,
     * we want to include products that have a stock description
     *
     * @param Varien_Event_Observer $observer
     */
    public function mventoryTrademeOosSkusCollectionBuild(Varien_Event_Observer $observer)
    {
        /** @var Varien_Object $transport */
        $transport = $observer->getEvent()->getTransport();

        $collection = Mage::getResourceModel('trademe/product_collection')
            ->addAttributeToFilter('type_id', 'simple')
            //Load all allowed to list products (incl. for $1 dollar auctions)
            ->addAttributeToFilter('tm_relist', ['gt' => 0])
            ->addAttributeToSelect('stock_description', 'left')
            ->addWebsiteFilter($observer->getEvent()->getWebsite())
            ->joinField(
                'inventory_in_stock',
                'cataloginventory/stock_item',
                'is_in_stock',
                'product_id=entity_id'
            );

        $collection->getSelect()->where(
            "is_in_stock = 0 OR (at_stock_description.value IS NOT NULL AND at_stock_description.value != '')"
        );
        $transport->setCollection($collection);
    }

    /**
     * Add SVG to Uploader filters in product images tab
     *
     * @param Varien_Event_Observer $observer
     */
    public function catalogProductGalleryPrepareLayout(Varien_Event_Observer $observer)
    {
        /** @var Mage_Adminhtml_Block_Catalog_Product_Helper_Form_Gallery_Content $block */
        $block = $observer->getEvent()->getBlock();

        $block->getUploader()->getUploaderConfig()
            ->setFilters([
                'images' => [
                    'label' => Mage::helper('adminhtml')->__('Images (.gif, .jpg, .png, .svg)'),
                    'files' => ['*.gif', '*.jpg', '*.jpeg', '*.png', '*.svg']
                ]
            ]);
    }

    public function catalogControllerCategoryInitAfter(Varien_Event_Observer $observer)
    {
        /** @var TradeTested_Catalog_Model_Rewrite_Catalog_Category $category */
        $category = $observer->getEvent()->getCategory();
        if (!$category->getMetaDescription()) {
            $parent = $category->getParentCategory();
            $department = ($parent->getLevel() > 1) ? " in the {$parent->getName()} department" : "";
            $country = Mage::getModel('directory/country')->loadByCode(Mage::getStoreConfig('general/country/default'))
                ->getName();
            $description = "Shop for {$category->getName()}{$department} at Trade Tested. "
                ."Massive range available at {$country}’s best prices all in stock and delivered nationwide. "
                ."Order yours today.";
            $category->setMetaDescription($description);
        }

        if (!$category->getMetaTitle()) {
            $category->setMetaTitle("Shop for {$category->getName()} at");
        }
    }

    /**
     * When viewing search results or categories, we want to show out of stock products, under certain conditions.
     *
     * @param Varien_Event_Observer $observer
     */
    public function catalogProductCollectionApplyLimitationsAfter(Varien_Event_Observer $observer)
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection = $observer->getEvent()->getCollection();
        $categoryId = $observer->getEvent()->getCategoryId() ?? Mage::registry('current_category');
        $select = $collection->getSelect();
        $fromPart = $select->getPart(Zend_Db_Select::FROM);
        if (!isset($fromPart['cissx'])) {
            $select->join(
                array('cissx' => Mage::getResourceModel('cataloginventory/stock_status')->getMainTable()),
                    $select->getAdapter()->quoteInto(
                        "cissx.product_id = e.entity_id AND cissx.website_id = ?",
                        $collection->getStoreId() ?? Mage::app()->getStore()->getId()
                    ),
                array()
            );
            if ($categoryId == null) {
                //Search. Display all out of stock products, but put them last
                $select->order('cissx.stock_status DESC');
            } else {
                //Category. Display OOS products that have a position < 0
                $select->where('cissx.stock_status = ? OR cat_index.position < 0', Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK);
            }
        }
    }
}