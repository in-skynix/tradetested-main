<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 06/01/15
 * Time: 14:36
 */
class TradeTested_Catalog_Model_Indexer_SaleCount extends Mage_Index_Model_Indexer_Abstract
{
    /**
     * Data key for matching result to be saved in
     */
    const EVENT_MATCH_RESULT_KEY = 'tradetested_catalog_index_sale_count_match_result';


    /**
     * @var array
     */
    protected $_matchedEntities = array(
        Mage_Sales_Model_Order::ENTITY => array(
            'create'
        )
    );

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('tradetested_catalog/indexer_saleCount');
    }

    /**
     * Retrieve Indexer name
     *
     * @return string
     */
    public function getName()
    {
        return Mage::helper('tradetested_catalog')->__('Product Sale Count (EAV)');
    }

    /**
     * Retrieve Indexer description
     *
     * @return string
     */
    public function getDescription()
    {
        return Mage::helper('tradetested_catalog')->__('Stores the number of sales for each product in an EAV attribute');
    }

    /**
     * Register data required by process in event object
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $event->getDataObject();
        $quantities = array();
        foreach ($order->getAllItems() as $_item) {
            $quantities[$_item->getProductId()] = $_item->getQtyOrdered();
        }
        $event->addNewData('tradetested_catalog_index_sale_count_quantities', $quantities);
        $event->addNewData('tradetested_catalog_index_sale_count_store_id', $order->getStoreId());
    }

    /**
     * Process event
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        $data = $event->getNewData();
        if(!empty($data['tradetested_catalog_index_sale_count_quantities'])){
            $this->getResource()->incrementProductSaleCount(
                $data['tradetested_catalog_index_sale_count_quantities'],
                $data['tradetested_catalog_index_sale_count_store_id']
            );
        }
    }
}