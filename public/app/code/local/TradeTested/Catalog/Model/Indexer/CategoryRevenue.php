<?php
class TradeTested_Catalog_Model_Indexer_CategoryRevenue extends Mage_Index_Model_Indexer_Abstract
{
    /**
     * Data key for matching result to be saved in
     */
    const EVENT_MATCH_RESULT_KEY = 'tradetested_catalog_index_category_revenue_match_result';


    /**
     * @var array
     */
    protected $_matchedEntities = array(
        Mage_Sales_Model_Order::ENTITY => array(
            'create'
        )
    );

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('tradetested_catalog/indexer_categoryRevenue');
    }

    /**
     * Retrieve Indexer name
     *
     * @return string
     */
    public function getName()
    {
        return Mage::helper('tradetested_catalog')->__('Category Revenue');
    }

    /**
     * Retrieve Indexer description
     *
     * @return string
     */
    public function getDescription()
    {
        return Mage::helper('tradetested_catalog')->__('Category Revenue. Revenue per category in the last 3 months');
    }

    /**
     * Register data required by process in event object
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        $order = $event->getDataObject();
        $data = array();
        /** @var Mage_Sales_Model_Order_Item $_item */
        foreach ($order->getAllItems() as $_item) {
            foreach ($_item->getProduct()->getCategoryIds() as $_id) {
                if (isset($data[$_id])) {
                    $data[$_id] += $_item->getBaseRowTotal();
                } else {
                    $data[$_id] = $_item->getBaseRowTotal();
                }
            }
        }
        $event->addNewData('tradetested_catalog_index_category_revenue_amounts', $data);
    }

    /**
     * Process event
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        $data = $event->getNewData();
        if(!empty($data['tradetested_catalog_index_category_revenue_amounts'])){
            $this->getResource()->addAmountsToCategoryIds($data['tradetested_catalog_index_category_revenue_amounts']);
        }
    }
}
