<?php
class TradeTested_Catalog_Model_Indexer_Topsellers extends Mage_Index_Model_Indexer_Abstract
{
    /**
     * Data key for matching result to be saved in
     */
    const EVENT_MATCH_RESULT_KEY = 'tradetested_catalog_index_topsellers_match_result';


    /**
     * @var array
     */
    protected $_matchedEntities = array(
        Mage_Sales_Model_Order::ENTITY => array(
            'create'
        )
    );

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
//        $this->_init('tradetested_catalog/indexer_topsellers');
    }
    /**
     * Retrieve Indexer name
     *
     * @return string
     */
    public function getName()
    {
        return Mage::helper('tradetested_catalog')->__('Top Seller');
    }

    /**
     * Retrieve Indexer description
     *
     * @return string
     */
    public function getDescription()
    {
        return Mage::helper('tradetested_catalog')->__('Top Sellers. The top x selling products in categories');
    }

    /**
     * Register data required by process in event object
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        $order = $event->getDataObject();
        $ids = array();
        foreach ($order->getAllItems() as $_item) {
            $ids[] = $_item->getProductId();
        }
        $event->addNewData('tradetested_catalog_index_topsellers_product_ids', $ids);
        $event->addNewData('tradetested_catalog_index_topsellers_store_id', $order->getStoreId());
    }

    /**
     * Process event
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        $data = $event->getNewData();
        if(
            !empty($data['tradetested_catalog_index_topsellers_product_ids']) &&
            !empty($data['tradetested_catalog_index_topsellers_store_id'])
        ){
            $this->reindexProductIds(
                $data['tradetested_catalog_index_topsellers_product_ids'],
                $data['tradetested_catalog_index_topsellers_store_id']
            );
        }
    }

    public function reindexProductIds($productIds, $storeId)
    {
        //@todo:
        //update the bestsellers table with number purchased
        //one row per product/store
        //update is_bestseller in index table by grabbing product's category ids and seeing if this product ID is topseller for any.
    }

    public function reindexAll()
    {
        foreach (Mage::app()->getStores() as $_store) {
            $ids = array();
            $storeId = $_store->getId();
            $rootCategoryId = $_store->getRootCategoryId();
            $categories = Mage::getModel('catalog/category')->getCollection()
                ->setStoreId($storeId)
                ->setProductStoreId($storeId)
                ->addFieldToFilter('level', array('in' => array(3, 4, 5)))
                ->addFieldToFilter('path', array('like' => "1/{$rootCategoryId}/%"));
            foreach($categories as $_category) {
                /** @var Mage_Catalog_Model_Resource_Product_Collection $productCollection */
                $productCollection = $_category->getProductCollection()
                    ->setStoreId($storeId)
                    ->addAttributeToSelect('sale_count', 'left')
                    ->addAttributeToSort('sale_count', 'desc')
                    ;
                Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($productCollection);
                $limit = ($_category->getLevel() == 3) ? 3 : 1;
                $select = $productCollection->getSelect()
                    ->reset(Zend_Db_Select::COLUMNS)
                    ->columns('entity_id')
                    ->limit($limit);
                $ids = array_unique(array_merge($ids, $productCollection->getConnection()->fetchCol($select)));
            }
            $existing = Mage::getModel('catalog/product')->getCollection()
                ->setStoreId($storeId)
                ->addAttributeToFilter('top_seller', true, 'left')
                ->getAllIds()
            ;
            Mage::getSingleton('catalog/product_action')
                ->updateAttributes($existing, array('top_seller' => false), $storeId);
            Mage::getSingleton('catalog/product_action')
                ->updateAttributes($ids, array('top_seller' => true), $storeId);
        }
        // we need it for cron job
        $indexer = Mage::getSingleton('index/indexer');
        $process = $indexer->getProcessByCode('tradetested_catalog_topsellers');
        if ($process) {
            $process->setStatus(Mage_Index_Model_Process::STATUS_PENDING);
            $process->setEndedAt(date('Y-m-d H:i:s'));
            $process->save();
        }
    }
}
