<?php
class TradeTested_Catalog_Model_Indexer_ProductRevenue extends Mage_Index_Model_Indexer_Abstract
{
    /**
     * Data key for matching result to be saved in
     */
    const EVENT_MATCH_RESULT_KEY = 'tradetested_catalog_index_product_revenue_match_result';


    /**
     * @var array
     */
    protected $_matchedEntities = array(
        Mage_Sales_Model_Order::ENTITY => array(
            'create'
        )
    );

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('tradetested_catalog/indexer_productRevenue');
    }

    /**
     * Retrieve Indexer name
     *
     * @return string
     */
    public function getName()
    {
        return Mage::helper('tradetested_catalog')->__('Product Revenue');
    }

    /**
     * Retrieve Indexer description
     *
     * @return string
     */
    public function getDescription()
    {
        return Mage::helper('tradetested_catalog')->__('Product Revenue. Revenue per product in the last 3 months');
    }

    /**
     * Register data required by process in event object
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        $order = $event->getDataObject();
        $ids = array();
        foreach ($order->getAllItems() as $_item) {
            $ids[] = $_item->getProductId();
        }
        $event->addNewData('tradetested_catalog_index_product_revenue_product_ids', $ids);
    }

    /**
     * Process event
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        $data = $event->getNewData();
        if(!empty($data['tradetested_catalog_index_product_revenue_product_ids'])){
            $this->getResource()->reindexProductIds($data['tradetested_catalog_index_product_revenue_product_ids']);
        }
    }
}
