<?php
/**
 * Created by PhpStorm.
 * User: skynix
 * Date: 17.11.16
 * Time: 16:58
 */
class TradeTested_Catalog_Helper_Bestseller extends TradeTested_Catalog_Block_Product_List_Bestseller{

    public function getBestsellerData(){
        $_products = $this->getProductCollection();

        foreach($_products->getItems() as $_product){
            $bestsellerData[] = [
                'name' => $_product->getName(),
                'url' => $_product->getProductUrl(),
                'top_seller' => $_product->getData('top_seller'),
                'image_url_small'   => $_product->getSmallImageUrl(200,200),
                'add_to_cart'=> $this->getAddToCartUrl($_product),
                'is_salable' => $_product->isSalable(),
                'special_price' => number_format((float)$_product->getData("special_price"), 2, '.', ''),
                'product_price' => number_format((float)$_product->getPrice(), 2, '.', ''),
                'free_ship' => $this->helper('tradetested_shipping/freeshipping')->canHasFreeShipping($_product)
            ];
        }

        return ['data' => [
            'product'   => $bestsellerData
        ]];
    }
}