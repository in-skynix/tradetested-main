<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 14/04/2016
 * Time: 20:21
 */
Class SimpleXMLElementExtended extends SimpleXMLElement {

    /**
     * Adds a child with $value inside CDATA
     * 
     * @param $name
     * @param null $value
     * @return SimpleXMLElement
     */
    public function addChildWithCDATA($name, $value = NULL) {
        $new_child = $this->addChild($name);

        if ($new_child !== NULL) {
            $node = dom_import_simplexml($new_child);
            $no   = $node->ownerDocument;
            $node->appendChild($no->createCDATASection($value));
        }

        return $new_child;
    }
}
class TradeTested_Catalog_Helper_IntelligentReach extends Mage_Core_Helper_Abstract
{
    protected $_storeId = 2;
    protected $_categoryData;
    protected $_entityTypeId;
    protected $_mediaBackend;
    protected $_attributes;
    protected $_baseAttributes = ['attribute_set_id', 'type_id', 'sku', 'has_options', 'required_options',
                                  'created_at', 'updated_at', 'status', 'visibility', 'package_id', 'weight', 'price',
                                  'special_price', 'deepest_category_id', 'name', 'meta_title', 'meta_description', 
                                  'stock_description', 'price_message', 'description', 'short_description', 
                                  'meta_keyword', 'special_from_date', 'special_to_date', 'brand'];

    /**
     * TradeTested_Catalog_Helper_IntelligentReach constructor.
     */
    public function __construct()
    {
        $this->_entityTypeId = Mage::getModel('eav/config')->getEntityType(Mage_Catalog_Model_Product::ENTITY)->getId();
        $this->_mediaBackend = Mage::getResourceModel('catalog/product')->getAttribute('media_gallery')->getBackend();
    }

    /**
     * @param int $storeId
     *
     * @return $this
     */
    public function setStoreId(int $storeId)
    {
        $this->_storeId = $storeId; 
        return $this;
    }

    /**
     * @return int
     */
    protected function _getStoreId()
    {
        return $this->_storeId;
    }

    /**
     * @return SimpleXMLElementExtended
     */
    public function getFullXml()
    {
        $xml = new SimpleXMLElementExtended('<products/>');
        $attributeSets = Mage::getResourceModel('eav/entity_attribute_set_collection')
            ->setEntityTypeFilter($this->_entityTypeId);
        /** @var Mage_Eav_Model_Entity_Attribute_Set $_attributeSet */
        foreach ($attributeSets as $_attributeSet) {
            $products = $this->_getProductCollection($_attributeSet)->load(); //Load before emulating, so not flat.
            $appEmulation = Mage::getSingleton('core/app_emulation');
            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($this->_getStoreId());
            foreach ($products as $_product) {
                Mage::helper('tradetested_core')->convertBinToGuid($_product);
                $productData = array_merge(
                    ['guid' => $_product->getGuid()],
                    array_intersect_key($_product->getData(), array_flip($this->_baseAttributes)),
                    [
                        'name'                     => $this->_truncate(implode(" ", 
                            array_filter([$_product->getName(), $_product->getEbayTitleAppendix()])
                        )),
                        'description'              => Mage::helper('tradetested_core')
                            ->nl2p($_product->getDescription()),
                        'short_description'        => Mage::helper('tradetested_core')
                            ->nl2p($_product->getShortDescription()),
                        'is_in_stock'              => $this->_getIsInStock($_product),
                        'qty'                      => $this->_getQuantity($_product),
                        'top_seller'               => $_product->getData('top_seller')
                            ? $_product->getData('top_seller') : $_product->getData('top_seller_manual'),
                        'product_url'              => $_product->getProductUrl(),
                        'is_salable'               => $_product->isSalable(),
                        'image_url'                => $this->_getImageUrl($_product),
                        'image_url_thumbnail'      => $this->_getImageUrl($_product, 'thumbnail'),
                        'image_url_small'          => $this->_getImageUrl($_product, 'small_image'),
                        'parent_ids'               => $_product->getTypeInstance()
                            ->getParentIdsByChild($_product->getId()),
                        'ir_longest_category_path' => $this->_getCategoryPath($_product),
                        'custom_attributes'        => $this->_getCustomAttributeValues($_product, $_attributeSet),
                    ],
                    $this->_getGalleryData($_product)
                );
                $productNode = $xml->addChild('product');
                $this->_addProductNodeData($productData, $productNode);
            }
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        }
        return $xml;
    }



    /**
     * @return SimpleXMLElementExtended
     */
    public function getQuantitiesXml()
    {
        $xml = new SimpleXMLElementExtended('<products/>');
        $products = Mage::getResourceModel("catalog/product_collection")
            ->addAttributeToSelect('stock_description')
            ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->setStoreId($this->_getStoreId())
            ->addStoreFilter($this->_getStoreId())
            ->joinField('qty','cataloginventory/stock_item', 'qty', 'product_id=entity_id', '{{table}}.stock_id=1',
                'left')
            ->joinTable('cataloginventory/stock_item', 'product_id=entity_id', ["is_in_stock"]);
        foreach($products as $_product) {
            Mage::helper('tradetested_core')->convertBinToGuid($_product);
            $productNode = $xml->addChild('product');
            $data = [
                'guid'        => $_product->getGuid(),
                'qty'         => $this->_getQuantity($_product),
                'is_in_stock' => $this->_getIsInStock($_product),
            ];
            $this->_addProductNodeData($data, $productNode);
        }
        
        return $xml;
    }
    
    /** 
     * Truncate a string only at whitespace.
     */
    protected function _truncate($text, $length = 80) {
        $length = abs((int)$length);
        if(strlen($text) > $length) {
            $text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1', $text);
        }
        return($text);
    }

    /**
     * Ebay doesn't support colon characters in image URLs
     * 
     * @param Mage_Catalog_Model_Product $product
     * @param string                     $type
     *
     * @return mixed
     */
    protected function _getImageUrl(Mage_Catalog_Model_Product $product, string $type = 'image', $file = null)
    {
        $url = Mage::helper('catalog/image')->init($product, $type, $file)->resize(1600, 1200)->keepFrame(false)
            ->__toString();
        $url = str_replace(':8890/', ':8891/', $url);
        return preg_replace('|^https://|', 'http://', $url);
    }

    protected function _getImageUrlCarousel(Mage_Catalog_Model_Product $product, string $type = 'image', $file = null)
    {
        $url = Mage::helper('catalog/image')->init($product, $type, $file)->resize(200);
        $url = str_replace(':8890/', ':8891/', $url);
        return $url;
    }
    
    protected function _getQuantity(Mage_Catalog_Model_Product $product)
    {
        return $product->getData('stock_description') ? '0.0000' : $product->getData('qty');
    }
    
    protected function _getIsInStock(Mage_Catalog_Model_Product $product)
    {
        return ($product->getData('is_in_stock') && !$product->getData('stock_description')) ? '1' : '0 ';
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    protected function _getGalleryData(Mage_Catalog_Model_Product $product)
    {
        $this->_mediaBackend->afterLoad($product);
        $galleryData = [];
        if ($mediaGallery = $product->getData('media_gallery')) {
            $images = $mediaGallery['images'];
            usort($images, function($a, $b){
                return ($a['position']??0) <=> ($b['position']??0);
            });
            $x = 1;
            foreach($images as $_imgData) {
                if (!$_imgData['disabled']) {
                    $galleryData['image'.$x] = $this->_getImageUrl($product, 'image', $_imgData['file']);
                    $x++;
                }
            }
        }
        return $galleryData;
    }

    /**
     * Get the option text for custom attributes
     * 
     * Optimised so that all options for an attribute are loaded only once, and memoised in _getCustomAttributes();
     * 
     * @param Mage_Catalog_Model_Product $product
     * @param Mage_Eav_Model_Entity_Attribute_Set $attributeSet
     * @return array
     */
    protected function _getCustomAttributeValues(
        Mage_Catalog_Model_Product $product, 
        Mage_Eav_Model_Entity_Attribute_Set $attributeSet
    ) {
        $customAttributes = [];
        /**
         * @var string $_attributeCode
         * @val Mage_Catalog_Model_Resource_Eav_Attribute $_attribute
         */
        foreach ($this->_getCustomAttributes($attributeSet) as $_attributeCode => $_attribute) {
            $text = $_attribute->getSource()->getOptionText($product->getData($_attributeCode));
            $customAttributes[$_attributeCode] = $text;
        }
        return $customAttributes;
    }

    /**
     * Get the custom attribute codes to load for this attribute set.
     * 
     * @param Mage_Eav_Model_Entity_Attribute_Set $attributeSet
     * @return array
     */
    protected function _getCustomAttributeCodes(Mage_Eav_Model_Entity_Attribute_Set $attributeSet)
    {
        $attributeCodes = array_keys($this->_getCustomAttributes($attributeSet));
        return array_splice($attributeCodes, 0, 3); //Any more attributes and we reach MySQL join limit!
    }

    /**
     * Load and memoise attributes for an attribute set
     * 
     * The option values for each attribute are loaded before memoisation so they can be re-used for each product.
     * 
     * @param Mage_Eav_Model_Entity_Attribute_Set $attributeSet
     * @return array
     */
    protected function _getCustomAttributes(Mage_Eav_Model_Entity_Attribute_Set $attributeSet)
    {
        if (!isset($this->_attributes[$attributeSet->getId()])) {
            $attributesCollection = Mage::getResourceModel('catalog/product_attribute_collection')
                ->setAttributeSetFilter($attributeSet->getId())
                ->setEntityTypeFilter($this->_entityTypeId)
                ->addFieldToFilter('is_filterable', ['in' => ['1', '2']])
                ->addFieldToFilter('is_user_defined', '1')
                ->addFieldToFilter('is_visible', '1');
            $attributesCollection->getSelect()
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns('attribute_id')
                ->columns('attribute_code');
            $attributes = [];
            /** @var Mage_Catalog_Model_Resource_Eav_Attribute $_attribute */
            foreach ($attributesCollection as $_attribute) {
                if ($_attribute->isInSet($attributeSet->getId())) {
                    $_attribute->setStoreId($this->_getStoreId())->getSource()->getAllOptions();
                    $attributes[$_attribute->getAttributeCode()] = $_attribute;
                }
            }
            return $attributes;
        }
        return $this->_attributes[$attributeSet->getId()];
    }

    /**
     * @param $attributeSet
     * @return mixed
     */
    protected function _getProductCollection($attributeSet)
    {
        $products = Mage::getResourceModel("catalog/product_collection")
            ->setStoreId($this->_getStoreId())
            ->addFieldToFilter('attribute_set_id', $attributeSet->getId())
            ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->addStoreFilter($this->_getStoreId())
            ->addAttributeToSelect($this->_getCustomAttributeCodes($attributeSet), 'left')
            ->addAttributeToSelect($this->_baseAttributes, 'left')
            ->addAttributeToSelect([
                'guid_bin',
                'top_seller',
                'top_seller_manual',
                'media_gallery',
                'image',
                'small_image',
                'thumbnail',
                'ebay_title_appendix',
            ], 'left')
            ->joinField('qty','cataloginventory/stock_item', 'qty', 'product_id=entity_id', '{{table}}.stock_id=1',
                'left')
            ->joinTable('cataloginventory/stock_item', 'product_id=entity_id', ["is_in_stock"]);
        Mage::helper('tradetested_catalog/url')->addRequestPathsToCollection($products, $this->_getStoreId());
        return $products;
    }

    /**
      * @param array $data
     * @param SimpleXMLElementExtended $xml
     */
    protected function _addProductNodeData(array $data, SimpleXMLElementExtended &$xml)
    {
        foreach( $data as $key => $value ) {
            if (is_array($value) && (!$this->_isAssoc($value))) {
                //Not associative array;
                $subnode = $xml->addChild($key);
                $newKey = (substr_compare($key, 's', strlen($key)-1, 1) === 0) ? substr($key, 0, -1) : 'item';
                $newKey = ($key == 'media_gallery') ? 'image' : $newKey;
                foreach ($value as $_val) {
                    if (is_array($_val)) {
                        $itemNode = $subnode->addChild($newKey);
                        $this->_addProductNodeData($_val, $itemNode);
                    } else {
                        $_val = $this->_stripInvalidXml($_val);
                        (htmlspecialchars($_val, ENT_XML1, 'UTF-8') == $_val) ? $subnode->addChild($newKey, $_val)
                            : $subnode->addChildWithCDATA($newKey, $_val);
                    }
                }
            } elseif (is_array($value)) {
                $subnode = $xml->addChild($key);
                $this->_addProductNodeData($value, $subnode);
            } else {
                $value = $this->_stripInvalidXml($value);
                (htmlspecialchars($value, ENT_XML1, 'UTF-8') == $value) ? $xml->addChild($key, $value)
                    : $xml->addChildWithCDATA($key, $value);
            }
        }
    }

    /**
     * Removes invalid XML
     * 
     * Some characters will cause issues, even in CDATA.
     *
     * @param string $value
     * @return string
     */
    protected function _stripInvalidXml($value)
    {
        $ret = "";
        if (empty($value)) {
            return $ret;
        }

        $length = strlen($value);
        for ($i=0; $i < $length; $i++) {
            $current = ord($value{$i});
            if (($current == 0x9) ||
                ($current == 0xA) ||
                ($current == 0xD) ||
                (($current >= 0x20) && ($current <= 0xD7FF)) ||
                (($current >= 0xE000) && ($current <= 0xFFFD)) ||
                (($current >= 0x10000) && ($current <= 0x10FFFF))
            ) {
                $ret .= chr($current);
            } else {
                $ret .= " ";
            }
        }
        return $ret;
    }

    /**
     * @param $array
     * @return bool
     */
    protected function _isAssoc($array)
    {
        $array = array_keys($array); return ($array !== array_keys($array));
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return string
     */
    protected function _getCategoryPath(Mage_Catalog_Model_Product $product)
    {
        $categoryData = $this->_getCategoryData($product->getDeepestCategoryId());
        $names = [];
        foreach ($categoryData['path'] as $_id) {
            $d = $this->_getCategoryData($_id);
            $names[] = $d['name'];
        }
        return implode(' > ', array_filter($names));
    }

    /**
     * @param $categoryId
     * @return array
     */
    protected function _getCategoryData($categoryId)
    {
        if (!$this->_categoryData) {
            $this->_loadCategoryData();
        }
        return $this->_categoryData[$categoryId] ?? ['path' => [], 'name' => ''];
    }

    /**
     * Load and memoise category data for all categories in store
     * 
     * @throws Mage_Core_Exception
     */
    protected function _loadCategoryData()
    {
        $rootId     = Mage::app()->getStore($this->_getStoreId())->getRootCategoryId();
        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addFieldToFilter('path', array('like'=> "1/{$rootId}/%"));
        $categories->getSelect()
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns('entity_id')
            ->columns('path');
        $categories->addAttributeToSelect('name', 'left');
        $this->_categoryData = [];
        foreach ($categories as $_category) {
            $path = array_slice(explode('/', $_category->getPath()), 2);
            $this->_categoryData[$_category->getId()] = [
                'path' => $path,
                'name' => $_category->getName()
            ];
        }
    }
}