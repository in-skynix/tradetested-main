<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 12/09/16
 * Time: 5:30 PM
 */
class TradeTested_Catalog_Helper_Rewrite_File_Storage_Database extends Arkade_S3_Helper_Core_File_Storage_Database
{
    /**
     * Get unique name for passed file in case this file already exists
     * 
     * Fixed the directory separator when joining
     *
     * @param string $directory - can be both full path or partial (like in DB)
     * @param string $filename - not just a filename. Can have directory chunks. return will have this form
     * @return string
     */
    public function getUniqueFilename($directory, $filename)
    {
        if ($this->checkDbUsage()) {
            $directory = $this->_removeAbsPathFromFileName($directory);
            if($this->fileExists($directory . $filename)) {
                $index = 1;
                $extension = strrchr($filename, '.');
                $filenameWoExtension = substr($filename, 0, -1 * strlen($extension));
                $directory = rtrim($directory, '/');
                $filenameWoExtension = '/'.ltrim($filenameWoExtension, '/');
                while ($this->fileExists($directory . $filenameWoExtension . '_' . $index . $extension)) {
                    $index ++;
                }
                $filename = $filenameWoExtension . '_' . $index . $extension;
            }
        }
        return $filename;
    }
}