<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 27/04/15
 * Time: 14:46
 */
class TradeTested_Catalog_Helper_Media extends TradeTested_Catalog_Block_Product_View_Media
{
    public function getJsComponentConfig()
    {
        return ['gallery' =>
            ['name' => 'ProductGallery', 'ref' => 'productGallery', 'config' => $this->getGalleryImageJSON()]
        ];
    }
}