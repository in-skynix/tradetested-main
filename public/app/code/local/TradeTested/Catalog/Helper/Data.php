<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 06/01/15
 * Time: 14:24
 */
class TradeTested_Catalog_Helper_Data extends TradeTested_Catalog_Helper_IntelligentReach
{
    public function setDeepestCategoryOnProduct(Mage_Catalog_Model_Product $product, $store = null)
    {
        if ($categoryId = $product->getDeepestCategoryId()) {
            $category = Mage::getModel('catalog/category')->load($categoryId);
            $product->setCategoryId($categoryId)->setCategory($category);
        } elseif (
        $categoryId = Mage::getResourceModel('tradetested_catalog/indexer_deepestCategory')
            ->getDeepestCategoryIdForProduct($product, $store)
        ) {
            $product->setDeepestCategoryId($categoryId)->getResource()->saveAttribute($product, 'deepest_category_id');
            $category = Mage::getModel('catalog/category')->load($categoryId);
            $product->setCategoryId($categoryId)->setCategory($category);
        }
    }

    public function extractShortDescription(string $description, $length = 250)
    {
        $length = abs((int)$length);
        $description = str_replace("<h2>Details</h2>\n", '', $description);
        $blocks = preg_split("/(?:\r?\n){2,}/", $description);
        $description = $blocks[0];
        if (strlen($description) > $length) {
            $description = preg_replace("/^(.{1,$length})(\..*|$)/s", '\\1.', $description);
        }
        return ($description);
    }

    public function getProductsData()
    {
        $products = Mage::getResourceModel("catalog/product_collection")
            ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->addStoreFilter($this->_getStoreId())
            ->addAttributeToSelect($this->_baseAttributes, 'left')
            ->addAttributeToSelect([
                'guid_bin',
                'top_seller',
                'top_seller_manual',
                'media_gallery',
                'image',
                'small_image',
                'thumbnail',
                'ebay_title_appendix',
            ], 'left')
            ->joinField('qty', 'cataloginventory/stock_item', 'qty', 'product_id=entity_id', '{{table}}.stock_id=1',
                'left')
            ->joinTable('cataloginventory/stock_item', 'product_id=entity_id', ["is_in_stock"])
            ->setPageSize(10);
        foreach($products as $_product){
            $productData[] = [
                'name' => $_product->getName(),
                'is_in_stock' => $this->_getIsInStock($_product),
//                        'qty' => $this->_getQuantity($_product),
                'top_seller' => $_product->getData('top_seller')
                    ? $_product->getData('top_seller') : $_product->getData('top_seller_manual'),
                'product_url' => $_product->getProductUrl(),
                'product_price' => number_format((float)$_product->getPrice(), 2, '.', ''),
                'is_salable' => $_product->isSalable(),
                'image_url' => $this->_getImageUrlCarousel($_product),
                'image_url_thumbnail' => $this->_getImageUrlCarousel($_product, 'thumbnail'),
                'image_url_small' => $this->_getImageUrlCarousel($_product, 'small_image'),
                'add_to_cart' => $this->_getUrl('checkout/cart/add', [
                    'product' => $_product->getId(),
                    Mage_Core_Model_Url::FORM_KEY => Mage::getSingleton('core/session')->getFormKey()
                ]),
                'special_price' => number_format((float)$_product->getData("special_price"), 2, '.', ''),
//                'free_ship' => $this->helper('tradetested_shipping/freeshipping')->canHasFreeShipping($_product)
            ];
        }
        return [
            'data' => [
                'total' => $productData
            ],
        ];
    }

    public function getProductImageData()
    {
        return [
            'data' => [
                'product' => $this->getProductData()
            ],
        ];
    }

}