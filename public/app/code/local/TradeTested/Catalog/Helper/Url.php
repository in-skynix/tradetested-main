<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 12/08/15
 * Time: 13:50
 */
class TradeTested_Catalog_Helper_Url extends Mage_Core_Helper_Abstract
{
    /**
     * Add request_path from deepest category to product collection
     *
     * @todo: Support Flat Catalog collections as well.
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     * @param $storeId
     * @return Mage_Catalog_Model_Resource_Product_Collection
     * @throws Mage_Core_Exception
     */
    public function addRequestPathsToCollection(
        Mage_Catalog_Model_Resource_Product_Collection $collection, $storeId
    ){
        $attributeCode = 'deepest_category_id';
        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $attributeCode);
        $attributeId = $attribute->getId();

        $connection = $collection->getConnection();
        $collection->getSelect()->joinLeft(
            array('dci' => $attribute->getBackend()->getTable()),
            $connection->quoteInto(
                "dci.entity_id = e.entity_id AND dci.attribute_id = {$attributeId} AND dci.store_id = ?",
                $storeId
            ),
            array()
        )->joinLeft(
            array('url_rewrite' => Mage::getSingleton('core/resource')->getTableName('core/url_rewrite')),
            'url_rewrite.id_path = CONCAT("product/", e.entity_id, IF(dci.value IS NULL, "", CONCAT("/", dci.value)))',
            array('request_path')
        )
        ;

        return $collection;
    }
}