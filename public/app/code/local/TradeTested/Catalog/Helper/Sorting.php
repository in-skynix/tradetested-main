<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 21/01/15
 * Time: 17:15
 */
class TradeTested_Catalog_Helper_Sorting extends Mage_Core_Helper_Abstract
{
    protected $_ordersOnlyDesc = array('view_count', 'sale_count');
    /**
     * Get Link Direction for Toolbar
     *
     * Some sort orders only have one direction, for others the link should be the opposite of current, but only if
     * it is the current sort order
     * @param $code
     * @param $currentDirection
     * @param $isActive
     * @return string
     */
    public function getLinkDirection($code, $currentDirection, $isActive)
    {
        if (in_array($code, $this->_ordersOnlyDesc)) {
            return 'desc';
        } elseif ($isActive) {
            return ($currentDirection == 'asc') ? 'desc' : 'asc';
        } else {
            return 'asc';
        }
    }
}