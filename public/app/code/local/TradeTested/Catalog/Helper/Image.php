<?php

/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 10/03/2016
 * Time: 11:46
 */
class TradeTested_Catalog_Helper_Image extends Mage_Catalog_Helper_Image
{
    /**
     * Check - is this file an image
     *
     * @param string $filePath
     * @return bool
     * @throws Mage_Core_Exception
     */
    public function validateUploadFile($filePath)
    {
        if (!$this->_isRaster($filePath) && !$this->_isSvg($filePath)) {
            Mage::throwException($this->__('Disallowed file type.'));
        }
    }

    protected function _isRaster($filePath)
    {
        if (getimagesize($filePath)) {
            $_processor = new Avid_Thumbor_Processor($filePath);
            return $_processor->getMimeType() !== null;
        }

        return false;
    }

    protected function _isSvg(string $filePath)
    {
        $finfo = new finfo(FILEINFO_MIME);

        return (strpos($finfo->file($filePath), 'image/svg+xml') === 0);
    }
}