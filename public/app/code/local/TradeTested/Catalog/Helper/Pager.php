<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Product list
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class TradeTested_Catalog_Helper_Pager extends Mage_Catalog_Block_Product_List
{
    public function getPagerData()
    {
        $_productCollection= $this->getLoadedProductCollection();


        foreach ($_productCollection as $_product){
            $pagerProductData [] = [
              'name' => $_product->getName(),
                'url' => $_product->getProductUrl(),
                'top_seller' => $_product->getData('top_seller'),
                'image_url_small'   => $_product->getSmallImageUrl(200,200),
                'add_to_cart'=> $this->getAddToCartUrl($_product),
                'is_salable' => $_product->isSalable(),
                'special_price' => number_format((float)$_product->getData("special_price"), 2, '.', ''),
                'product_price' => number_format((float)$_product->getPrice(), 2, '.', ''),
                'free_ship' => $this->helper('tradetested_shipping/freeshipping')->canHasFreeShipping($_product)
            ];
        }

        return [
            'data' => [
                'product' =>  $pagerProductData,
                'total'   => $_productCollection->count()
            ],
        ];
    }
}

