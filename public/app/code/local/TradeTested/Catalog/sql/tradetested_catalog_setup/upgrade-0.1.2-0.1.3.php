<?php
//$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer = $setup;

$installer->startSetup();

$entityTypeId     = $installer->getEntityTypeId('catalog_category');
$attributeSetId   = $installer->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $installer->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);
$group = 'General Information';
$installer->addAttribute('catalog_category', 'description_bottom',  array(
    'type'                      => 'text',
    'group'                     => $group,
    'label'                     => 'Bottom Description',
    'input'                     => 'textarea',
    'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'                   => true,
    'required'                  => false,
    'user_defined'              => false,
    'is_searchable'             => true,
    'is_wysiwyg_enabled'        => true,
    'is_html_allowed_on_front'  => true

));
$installer->updateAttribute('catalog_category', 'description_bottom', 'is_wysiwyg_enabled', 1);
$installer->updateAttribute('catalog_category', 'description_bottom', 'is_html_allowed_on_front', 1);
$installer->endSetup();
