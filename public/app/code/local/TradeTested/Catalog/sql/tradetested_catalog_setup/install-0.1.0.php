<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

$attr = array (
    'type' => 'int',
    'backend' => '',
    'frontend' => '',
    'label' => 'Top Seller',
    'input' => 'boolean',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'is_visible' => false,
    'is_configurable' => true,
    'used_in_product_listing' => true,
    'required' => false,
    'default' => 0,
);
$installer->addAttribute('catalog_product','top_seller',$attr);
$attr['is_visible'] = true;
$installer->addAttribute('catalog_product','top_seller_manual',$attr);
$installer->updateAttribute('catalog_product', 'top_seller', 'used_in_product_listing', 1);
$installer->updateAttribute('catalog_product', 'top_seller', 'is_visible', 0);
$installer->updateAttribute('catalog_product', 'top_seller_manual', 'used_in_product_listing', 1);

$attr = array (
    'type' => 'decimal',// So Bubble_ElasticSearch indexes as numeric
    'backend' => '',
    'frontend' => '',
    'frontend_class' => 'validate-digits', // So Bubble_ElasticSearch indexes as integer
    'label' => 'Popular',
    'input' => 'text',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'is_visible' => false,
    'is_configurable' => true,
    'used_in_product_listing' => true,
    'required' => false,
    'default' => 0,
);
$installer->addAttribute('catalog_product','sale_count',$attr);
$installer->updateAttribute('catalog_product', 'sale_count', 'used_in_product_listing', 1);
$installer->updateAttribute('catalog_product', 'sale_count', 'is_visible', 0);
$installer->updateAttribute('catalog_product', 'sale_count', 'used_for_sort_by', 1);

$attr = array (
    'type' => 'decimal',// So Bubble_ElasticSearch indexes as numeric
    'backend' => '',
    'frontend' => '',
    'frontend_class' => 'validate-digits', // So Bubble_ElasticSearch indexes as integer
    'label' => 'Most Viewed',
    'input' => 'text',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'is_visible' => false,
    'is_configurable' => true,
    'used_in_product_listing' => true,
    'required' => false,
    'default' => 0,
);
$installer->addAttribute('catalog_product','view_count',$attr);
$installer->updateAttribute('catalog_product', 'view_count', 'used_in_product_listing', 1);
$installer->updateAttribute('catalog_product', 'view_count', 'is_visible', 0);
$installer->updateAttribute('catalog_product', 'view_count', 'used_for_sort_by', 1);


$table = $installer->getConnection()
    ->newTable($installer->getTable('tradetested_catalog/index_product_revenue'))
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Product ID')
    ->addColumn('revenue', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(), 'Revenue')
    ->addIndex($installer->getIdxName('tradetested_catalog/index_product_revenue', array('product_id')),
        array('product_id'))
    ->addForeignKey($installer->getFkName('tradetested_catalog/index_product_revenue', 'product_id', 'catalog/product', 'eid'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
;
$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('tradetested_catalog/index_category_revenue'))
    ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Category ID')
    ->addColumn('revenue', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(), 'Revenue')
    ->addIndex($installer->getIdxName('tradetested_catalog/index_product_revenue', array('category_id')),
        array('category_id'))
    ->addForeignKey(
        $installer->getFkName('tradetested_catalog/index_category_revenue', 'category_id', 'catalog/category', 'eid'),
        'category_id', $installer->getTable('catalog/category'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    );
$installer->getConnection()->createTable($table);

$installer->endSetup();
