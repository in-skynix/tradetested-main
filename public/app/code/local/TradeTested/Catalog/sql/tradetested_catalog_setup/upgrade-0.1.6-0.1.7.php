<?php
$installer = $this;
$installer->startSetup();

$group = 'Display Settings';
$installer->addAttribute('catalog_category', 'display_filters',  array(
    'type'                      => 'int',
    'group'                     => $group,
    'label'                     => 'Display Filters',
    'input'                     => 'select',
    'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'                   => true,
    'required'                  => false,
    'user_defined'              => false,
    'is_searchable'             => false,
    'is_wysiwyg_enabled'        => false,
    'is_html_allowed_on_front'  => false,
    'source_model'              => 'eav/entity_attribute_source_boolean'

));
$installer->updateAttribute(
    'catalog_category', 'display_filters', 'source_model', 'eav/entity_attribute_source_boolean'
);
$installer->updateAttribute('catalog_category', 'is_anchor', 'default_value', 1);

$installer->endSetup();
