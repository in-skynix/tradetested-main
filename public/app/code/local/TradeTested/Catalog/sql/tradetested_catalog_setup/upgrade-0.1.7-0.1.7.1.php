<?php
/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->getConnection()->modifyColumn(
    $installer->getTable('catalog/category_anchor_products_indexer_idx'),
    'position',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'length'    => 11,
        'unsigned'  => false,
    )
);

$installer->getConnection()->modifyColumn(
    $installer->getTable('catalog/category_anchor_products_indexer_tmp'),
    'position',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'length'    => 11,
        'unsigned'  => false,
    )
);

$installer->endSetup();
