<?php
//http://stackoverflow.com/a/7168916/829039
//http://mysqlserverteam.com/storing-uuid-values-in-mysql-tables/
$installer = $this;
$installer->startSetup();
$installer->addAttribute('catalog_product', 'guid_bin', [
    'type'     => 'static',
    'required' => true,
    'visible'  => false,
    'is_visible'  => false,
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
]);
$installer->run("ALTER TABLE {$installer->getTable('catalog/product')} ADD guid_bin CHAR(16) CHARACTER SET BINARY NULL;");
$installer->run("CREATE UNIQUE INDEX catalog_product_entity_guid ON {$installer->getTable('catalog/product')} (guid_bin);");
$sql = <<<'SQL'
CREATE FUNCTION `guid_to_binary`($Data VARCHAR(36)) RETURNS binary(16)
DETERMINISTIC
NO SQL
BEGIN
DECLARE $Result BINARY(16) DEFAULT NULL;
    IF $Data IS NOT NULL THEN
        SET $Data = REPLACE($Data,'-','');
        SET $Result =
            CONCAT( UNHEX(SUBSTRING($Data,7,2)), UNHEX(SUBSTRING($Data,5,2)),
                    UNHEX(SUBSTRING($Data,3,2)), UNHEX(SUBSTRING($Data,1,2)),
                    UNHEX(SUBSTRING($Data,11,2)),UNHEX(SUBSTRING($Data,9,2)),
                    UNHEX(SUBSTRING($Data,15,2)),UNHEX(SUBSTRING($Data,13,2)),
                    UNHEX(SUBSTRING($Data,17,16)));
    END IF;
    RETURN $Result;
END
SQL;
$installer->run($sql);
$sql = <<<'SQL'
CREATE FUNCTION `to_guid`(
    $Data BINARY(16)
) RETURNS char(36) CHARSET utf8
DETERMINISTIC
NO SQL
BEGIN
    DECLARE $Result CHAR(36) DEFAULT NULL;
    IF $Data IS NOT NULL THEN
        SET $Result =
            CONCAT(
                HEX(SUBSTRING($Data,4,1)), HEX(SUBSTRING($Data,3,1)),
                HEX(SUBSTRING($Data,2,1)), HEX(SUBSTRING($Data,1,1)), '-', 
                HEX(SUBSTRING($Data,6,1)), HEX(SUBSTRING($Data,5,1)), '-',
                HEX(SUBSTRING($Data,8,1)), HEX(SUBSTRING($Data,7,1)), '-',
                HEX(SUBSTRING($Data,9,2)), '-', HEX(SUBSTRING($Data,11,6)));
    END IF;
    RETURN $Result;
END
SQL;

$installer->run($sql);
$installer->endSetup();
