<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 04/02/15
 * Time: 17:27
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

$attr = array (
    'group' => 'TM',
    'type' => 'text',
    'backend' => '',
    'frontend_input_renderer' => 'tradetested_catalog/adminhtml_form_element_lineList',
    'frontend_input' => 'textarea',
    'label' => 'TradeMe Name Variants',
    'input' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'is_visible' => true,
    'is_configurable' => false,
    'required' => false
);
$installer->addAttribute('catalog_product','tm_name_variants',$attr);
$installer->endSetup();
