<?php
$installer = $this;
$installer->startSetup();

$attr = array (
    'type' => 'int',
    'backend' => '',
    'frontend' => '',
    'label' => 'Deepest Category ID',
    'input' => 'text',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'is_visible' => false,
    'is_configurable' => false,
    'used_in_product_listing' => true,
    'required' => false,
    'default' => 0,
);
$installer->addAttribute('catalog_product','deepest_category_id',$attr);
$installer->updateAttribute('catalog_product', 'deepest_category_id', 'used_in_product_listing', 1);
$installer->updateAttribute('catalog_product', 'deepest_category_id', 'is_visible', 0);

$installer->endSetup();
