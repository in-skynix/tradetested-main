<?php
$installer = $this;
$installer->startSetup();

$installer->updateAttribute('catalog_product', 'sale_count', 'is_searchable', 1);

$installer->endSetup();
