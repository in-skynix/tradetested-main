<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 27/06/13
 * Time: 2:48 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_TTCatalog_Block_Rewrite_Reports_Product_Viewed extends Mage_Reports_Block_Product_Viewed
{
    protected $_productIds;
    public function getItemsCollection()
    {
        if (!$this->_collection) {
            $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
            $collection = Mage::getModel('catalog/product')->getCollection()
                ->addIdFilter($this->getProductIds())
                ->addAttributeToSelect($attributes)
                ->addUrlRewrite()
                ->setPageSize($this->getPageSize())
                ->setCurPage(1);
            $idimpl = implode(',', $this->getProductIds());
            $collection->getSelect()->order(new Zend_Db_Expr("FIELD(entity_id, {$idimpl})"));
            $this->_collection = $collection;
        }
        return $this->_collection;
    }

    public function getProductIds()
    {
        return array();
        if (!$this->_productIds)
            $this->_productIds = Mage::helper('avid_related_products')->getSessionViewedProductIds($this->getPageSize());
        return $this->_productIds;
    }
}