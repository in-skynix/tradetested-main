<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 9/10/14
 * Time: 5:21 PM
 */
class Avid_TTCatalog_Block_Adminhtml_Catalog_Product_Edit_Action_Attribute_Tab_Related_Renderer_State
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $this->setTemplate('avid/ttcatalog/massaction/edit/renderer/state.phtml');
        $this->setRow($row);
        return $this->toHtml();
    }

    public function getValue()
    {
        $values = $this->getColumn()->getValues();
        return isset($values[$this->getRow()->getId()]) ? $values[$this->getRow()->getId()] : null;
    }

    public function getColor()
    {
        $increment = round(200/$this->getColumn()->getTotal());
        $hex = "#";
        $hex .= str_pad(dechex(255-$increment*$this->getValue()), 2, "0", STR_PAD_LEFT);
        $hex .= str_pad(dechex($increment*$this->getValue()), 2, "0", STR_PAD_LEFT);
        return $hex."00";
    }
}