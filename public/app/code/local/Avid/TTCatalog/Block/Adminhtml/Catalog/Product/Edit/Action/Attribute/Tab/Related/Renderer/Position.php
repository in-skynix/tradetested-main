<?php
class Avid_TTCatalog_Block_Adminhtml_Catalog_Product_Edit_Action_Attribute_Tab_Related_Renderer_Position
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Number
{

    /**
     * Returns value of the row
     *
     * @param Varien_Object $row
     * @return mixed|string
     */
    protected function _getInputValue(Varien_Object $row)
    {
        $values = $this->getColumn()->getValues() ? $this->getColumn()->getValues() : array();
        $id = $row->getData('entity_id');
        return isset($values[$id]) ? $values[$id] : $this->getColumn()->getDefault();
    }
}