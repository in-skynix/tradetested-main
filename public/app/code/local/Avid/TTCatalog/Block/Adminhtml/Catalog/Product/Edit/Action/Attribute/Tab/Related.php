<?php
class Avid_TTCatalog_Block_Adminhtml_Catalog_Product_Edit_Action_Attribute_Tab_Related
    extends Mage_Adminhtml_Block_Widget_Grid
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected $_linkInstance;
    protected $_selectedRelatedProducts;
    protected $_tabName = 'related_products';

    /**
     * Set grid params
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId($this->_tabName.'_grid');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
        $this->setRowClickCallback(null);
        $this->setDefaultFilter(array('in_products' => 1));
    }

    public function getTabLabel()
    {
        return Mage::helper('ttcatalog')->__('Related Products');
    }

    public function getTabTitle()
    {
        return Mage::helper('ttcatalog')->__('Related Products');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    public function getTabName()
    {
        return $this->_tabName;
    }

    /**
     * Prepare collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = $this->getLinkInstance()
            ->getProductCollection()
            ->addAttributeToSelect('*');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Add columns to grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('included_count', array(
            'header'            => Mage::helper('ttcatalog')->__('# Included'),
            'header_css_class'  => 'a-center',
            'values'            => $this->_getIncludedCounts(),
            'align'             => 'center',
            'index'             => 'entity_id',
            'renderer'          => 'ttcatalog/adminhtml_catalog_product_edit_action_attribute_tab_related_renderer_state',
            'type'              => 'range',
            'total'             => count($this->_getHelper()->getProductIds()),
        ));
        $this->addColumn('same_products', array(
            'header'            => Mage::helper('ttcatalog')->__('~'),
            'header_css_class'  => 'a-center',
            'column_css_class'  => 'mass_action_keep_same',
            'type'              => 'radio',
            'value'             => 0,
            'align'             => 'center',
            'index'             => 'entity_id',
            'name'              => 'in_products',
            'renderer'          => 'ttcatalog/adminhtml_catalog_product_edit_action_attribute_tab_related_renderer_radio',
            'filter'            => false,
            'sortable'          => false,
        ));
        $this->addColumn('add_products', array(
            'header'            => Mage::helper('ttcatalog')->__('+'),
            'header_css_class'  => 'a-center',
            'column_css_class'  => 'mass_action_add',
            'type'              => 'radio',
            'value'             => 1,
            'values'            => $this->_getSelectionsWithValue(1),
            'align'             => 'center',
            'index'             => 'entity_id',
            'name'              => 'in_products',
            'renderer'          => 'ttcatalog/adminhtml_catalog_product_edit_action_attribute_tab_related_renderer_radio',
            'filter'            => false,
            'sortable'          => false,
        ));
        $this->addColumn('remove_products', array(
            'header'            => Mage::helper('ttcatalog')->__('-'),
            'header_css_class'  => 'a-center',
            'column_css_class'  => 'mass_action_remove',
            'type'              => 'radio',
            'value'             => -1,
            'values'            => $this->_getSelectionsWithValue(-1),
            'align'             => 'center',
            'index'             => 'entity_id',
            'name'              => 'in_products',
            'renderer'          => 'ttcatalog/adminhtml_catalog_product_edit_action_attribute_tab_related_renderer_radio',
            'filter'            => false,
            'sortable'          => false,
        ));


        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('catalog')->__('ID'),
            'sortable'  => true,
            'width'     => 60,
            'index'     => 'entity_id'
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('catalog')->__('Name'),
            'index'     => 'name'
        ));

        $this->addColumn('status', array(
            'header'    => Mage::helper('catalog')->__('Status'),
            'width'     => 90,
            'index'     => 'status',
            'type'      => 'options',
            'options'   => Mage::getSingleton('catalog/product_status')->getOptionArray(),
        ));

        $this->addColumn('visibility', array(
            'header'    => Mage::helper('catalog')->__('Visibility'),
            'width'     => 90,
            'index'     => 'visibility',
            'type'      => 'options',
            'options'   => Mage::getSingleton('catalog/product_visibility')->getOptionArray(),
        ));

        $this->addColumn('sku', array(
            'header'    => Mage::helper('catalog')->__('SKU'),
            'width'     => 80,
            'index'     => 'sku'
        ));

        $this->addColumn('price', array(
            'header'        => Mage::helper('catalog')->__('Price'),
            'type'          => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index'         => 'price'
        ));

        $this->addColumn('position', array(
            'header'            => Mage::helper('catalog')->__('Position'),
            'name'              => 'position',
            'type'              => 'number',
            'validate_class'    => 'validate-number',
            'index'             => 'position',
            'values'            => $this->_getPositions(),
            'width'             => 60,
            'editable'          => true,
            'edit_only'         => false,
            'renderer'          => 'ttcatalog/adminhtml_catalog_product_edit_action_attribute_tab_related_renderer_position',
        ));

        return parent::_prepareColumns();
    }

    /**
     * Rerieve grid URL
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getData('grid_url')
            ? $this->getData('grid_url')
            : $this->getUrl('adminhtml/ttcatalog_massAction/grid', array('_current' => true, 'tab_name' => $this->_tabName));
    }

    /**
     * Retrieve selected related products
     *
     * @return array
     */
    protected function _getSelectedProducts()
    {
        $products = $this->getProductsRelated();
        if (!is_array($products)) {
            $products = array_keys($this->getSelectedRelatedProducts());
        }
        return $products;
    }

    /**
     * Retrieve related products
     *
     * @return array
     */
    public function getSelectedRelatedProducts()
    {
        if (!$this->_selectedRelatedProducts) {
            $collection = $this->getLinkInstance()->getProductCollection()
                ->addProductFilter($this->_getHelper()->getProductIds())
                ->addExpressionAttributeToSelect('included_count', new Zend_Db_Expr('count(links.product_id)'), 'entity_id')
            ;

            $collection->getSelect()
                ->group('entity_id');

            $products = array();
            foreach ($collection as $product) {
                $products[$product->getId()] = array('position' => $product->getPosition(), 'included_count' => $product->getIncludedCount());
            }
            $this->_selectedRelatedProducts = $products;
        }
        return $this->_selectedRelatedProducts;
    }

    public function getInitialChangeSelections()
    {
        return array();
    }

    protected function _getSelectionsWithValue($value)
    {
        $selections = $this->getSelections() ? $this->getSelections() : array();
        $withValue = array();
        foreach ($selections as $_id => $_value) {
            if ($_value == $value) {
                $withValue[] = $_id;
            }
        }
        return $withValue;
    }

    protected function _getPositions()
    {
        if (!$this->getPositions()) { //Could have been set in the AJAX controller;
            $positions = array();
            foreach ($this->getSelectedRelatedProducts() as $_id => $_item) {
                $positions[$_id] = $_item['position'];
            }
            $this->setPositions($positions);
        }
        return $this->getPositions();
    }

    protected function _getIncludedCounts()
    {
        $counts = array();
        foreach ($this->getSelectedRelatedProducts() as $_id => $_item) {
            $counts[$_id] = $_item['included_count'];
        }
        return $counts;
    }

    /**
     * Retrieve block attributes update helper
     *
     * @return Mage_Adminhtml_Helper_Catalog_Product_Edit_Action_Attribute
     */
    protected function _getHelper()
    {
        return $this->helper('adminhtml/catalog_product_edit_action_attribute');
    }

    /**
     * Retrieve link instance
     *
     * @return  Mage_Catalog_Model_Product_Link
     */
    public function getLinkInstance()
    {
        if (!$this->_linkInstance) {
            $this->_linkInstance = Mage::getSingleton('catalog/product_link')->useRelatedLinks();
        }
        return $this->_linkInstance;
    }
}
