<?php
class Avid_TTCatalog_Block_Adminhtml_Catalog_Product_Edit_Action_Attribute_Tab_Crosssell
    extends Avid_TTCatalog_Block_Adminhtml_Catalog_Product_Edit_Action_Attribute_Tab_Related
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected $_tabName = 'cross_sell';

    public function getTabLabel()
    {
        return Mage::helper('ttcatalog')->__('Cross Sell Products');
    }

    public function getTabTitle()
    {
        return Mage::helper('ttcatalog')->__('Cross Sell Products');
    }

    /**
     * Retrieve link instance
     *
     * @return  Mage_Catalog_Model_Product_Link
     */
    public function getLinkInstance()
    {
        if (!$this->_linkInstance) {
            $this->_linkInstance = Mage::getSingleton('catalog/product_link')->useCrossSellLinks();
        }
        return $this->_linkInstance;
    }
}
