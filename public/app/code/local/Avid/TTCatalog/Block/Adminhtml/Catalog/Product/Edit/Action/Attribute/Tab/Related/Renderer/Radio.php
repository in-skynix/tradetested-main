<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 9/10/14
 * Time: 5:21 PM
 */
class Avid_TTCatalog_Block_Adminhtml_Catalog_Product_Edit_Action_Attribute_Tab_Related_Renderer_Radio
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $this->setTemplate('avid/ttcatalog/massaction/edit/renderer/radio.phtml');
        $this->setRow($row);
        return $this->toHtml();
    }

    public function isSelected()
    {
        $values = $this->getColumn()->getValues() ? $this->getColumn()->getValues() : array();
        return ($this->getColumn()->getValue() == 0) ||
            (in_array($this->getRow()->getData($this->getColumn()->getIndex()), $values));
    }

    public function getTabName()
    {
        return $this->getColumn()->getGrid()->getTabName();
    }
}