<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 19/04/13
 * Time: 2:23 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_TTCatalog_Block_Feature extends Mage_Catalog_Block_Product_Abstract
{
    protected $_productCollection;

    public function getFeaturedSpecialsCollection($limit = null)
    {
        $limit = $limit ? $limit : $this->getProductCount();
        if (!$this->_productCollection) {
            $collection = Mage::getModel('catalog/product')->getCollection();
            $collection->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->addUrlRewrite()
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents();
            Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
            Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
            $collection->getSelect()
                ->joinLeft(
                    array('revenue' => Mage::getSingleton('core/resource')->getTableName('tradetested_catalog/index_product_revenue')),
                    'revenue.product_id = e.entity_id')
                ->where('price_index.final_price < price_index.price')
                ->limit($limit)
                ->order(new Zend_Db_Expr('RAND() * revenue.revenue DESC'))
            ;
            $this->_productCollection = $collection;
        }
        return $this->_productCollection;
    }
}