<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 2/04/13
 * Time: 3:17 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_TTCatalog_Model_Rewrite_SalesRule_Validator extends Mage_SalesRule_Model_Validator
{
    /**
     * First time quote is prcessed, the weight is unknow nso free shipping rules were not being applied.
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return Mage_SalesRule_Model_Validator|void
     */
    public function processFreeShipping(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        $address = $this->_getAddress($item);
        $item->setFreeShipping(false);

        if (!$address->getWeight()) {
            $weight = 0;
            foreach ($address->getAllItems() as $_item) $weight += $_item->getWeight();
            $address->setWeight($weight);
        }


        foreach ($this->_getRules() as $rule) {
            /* @var $rule Mage_SalesRule_Model_Rule */
            if (!$this->_canProcessRule($rule, $address)) {
                continue;
            }

            if (!$rule->getActions()->validate($item)) {
                continue;
            }

            switch ($rule->getSimpleFreeShipping()) {
                case Mage_SalesRule_Model_Rule::FREE_SHIPPING_ITEM:
                    $item->setFreeShipping($rule->getDiscountQty() ? $rule->getDiscountQty() : true);
                    break;

                case Mage_SalesRule_Model_Rule::FREE_SHIPPING_ADDRESS:
                    $address->setFreeShipping(true);
                    break;
            }
            if ($rule->getStopRulesProcessing()) {
                break;
            }
        }
        return $this;
    }
}