<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 27/10/14
 * Time: 13:05
 */
class Avid_TTCatalog_Model_Resource_Catalog_Product_Link extends Mage_Catalog_Model_Resource_Product_Link
{
    public function saveMassProductLinks($productIds, $data, $typeId)
    {
        $adapter    = $this->_getWriteAdapter();
        $deleteIds = array();
        $addData = array();
        $attributeData = array();

        foreach ($data as $_id => $_item) {
            if (isset($_item['selected']) && ($_item['selected'] == '1')) {
                $addData[$_id] = $_item;
                $attributeData[$_id] = $_item;
            } elseif(isset($_item['selected']) && ($_item['selected'] == '-1')) {
                $deleteIds[] = $_id;
            } else {
                $attributeData[$_id] = $_item;
            }
        }

        if (!empty($deleteIds)) {
            $adapter->delete($this->getMainTable(), array(
                'product_id IN (?)' => $productIds,
                'linked_product_id IN (?)' => $deleteIds,
            ));
        }

        foreach($productIds as $_productId) {
            foreach ($addData as $linkedProductId => $linkInfo) {
                try {
                        $bind = array(
                            'product_id' => $_productId,
                            'linked_product_id' => $linkedProductId,
                            'link_type_id' => $typeId
                        );
                        $adapter->insert($this->getMainTable(), $bind);
                } catch (Exception $e) { }
            }
            foreach($attributeData as $_linkedProductId => $_linkInfo) {
                $this->_updateAttributeValues($typeId, $_productId, $_linkedProductId, $_linkInfo);
            }
        }

        return $this;
    }

    protected function _updateAttributeValues($typeId, $productId, $linkedProductId, $linkInfo)
    {
        $linkIdsSelect = $this->_getReadAdapter()->select()
            ->from($this->getMainTable(), array('link_id'))
            ->where('product_id IN (?)', $productId)
            ->where('linked_product_id IN (?)', $linkedProductId);
        $linkIds = $this->_getReadAdapter()->fetchCol($linkIdsSelect);
        foreach($linkIds as $_linkId) {
            $attributes = $this->getAttributesByType($typeId);
            foreach ($attributes as $attributeInfo) {
                $attributeTable = $this->getAttributeTypeTable($attributeInfo['type']);
                if ($attributeTable) {
                    if (isset($linkInfo[$attributeInfo['code']])) {
                        $value = $this->_prepareAttributeValue($attributeInfo['type'],
                            $linkInfo[$attributeInfo['code']]);
                        $bind = array(
                            'product_link_attribute_id' => $attributeInfo['id'],
                            'link_id' => $_linkId,
                            'value' => $value
                        );
                        $this->_getWriteAdapter()->insertOnDuplicate($attributeTable, $bind, array('value'));
                    } else {
                        $this->_getWriteAdapter()->delete($attributeTable, array(
                            'link_id = ?' => $_linkId,
                            'product_link_attribute_id = ?' => $attributeInfo['id']
                        ));
                    }
                }
            }
        }
    }
}