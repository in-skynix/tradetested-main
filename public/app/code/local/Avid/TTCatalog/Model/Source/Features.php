<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 19/04/13
 * Time: 1:48 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_TTCatalog_Model_Source_Features extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
    /**
     * Retrieve Full Option values array
     *
     * @param bool $withEmpty       Add empty option to array
     * @param bool $defaultValues
     * @return array
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {
        $options = array();
        $featureSets = Mage::getModel('ttcatalog/feature_set')->getCollection();
        foreach ($featureSets as $featureSet)
            $options[] = array('label' => $featureSet->getName(), 'value' => $featureSet->getId());
        if ($withEmpty) {
            array_unshift($options, array('label' => '', 'value' => ''));
        }

        return $options;
    }
}