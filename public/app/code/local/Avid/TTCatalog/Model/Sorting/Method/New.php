<?php
class Avid_TTCatalog_Model_Sorting_Method_New
{
    public function getCode()
    {
        return 'created_at';
    }    
    
    public function getName()
    {
        return 'New';
    }
    
    public function apply($collection, $currDir)  
    {
            $orders = $collection->getSelect()->getPart(Zend_Db_Select::ORDER);
            foreach ($orders as $k => $v){
                if (false !== strpos($v[0], 'created_at')){
                    $orders[$k] = null;
                    unset($orders[$k]);
                }
            $collection->getSelect()->setPart(Zend_Db_Select::ORDER, $orders);
            $collection->addAttributeToSort('created_at', $currDir);
        }
        
        return $this;
    }
   
}