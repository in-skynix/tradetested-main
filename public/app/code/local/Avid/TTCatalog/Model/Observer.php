<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dane
 * Date: 24/04/12
 * Time: 4:54 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_TTCatalog_Model_Observer
{
    function controllerActionPredispatch($observer)
    {
        $event = $observer->getEvent();
        $action = $event->getControllerAction();
        $request = $action->getRequest();
        if (substr($request->getRequestUri(), 0, 11) == '/index.php/') {
            Header( "HTTP/1.1 301 Moved Permanently" );
            Header( "Location: ".$request->getOriginalPathInfo());
            exit();
        }
    }

   public function tempProductSaveBefore($observer)
   {
       $product = $observer->getEvent()->getProduct();
       if ($product->getStoreId() && $product->getData('name') && extension_loaded('newrelic'))
           newrelic_notice_error(null, new Exception('TRYING TO OVERRIDE PRODUCT NAME'));
   }

    public function addMassactionToProductGrid($observer)
    {
        $block = $observer->getBlock();
        if (($block instanceof Mage_Adminhtml_Block_Catalog_Product_Grid) || ($block instanceof TBT_Enhancedgrid_Block_Catalog_Product_Grid)) {
            $block->getMassactionBlock()->addItem('ttcatalog_massaction', array(
                'label' => Mage::helper('catalog')->__('Change Relations'),
                'url' => $block->getUrl('adminhtml/ttcatalog_massAction/edit', array('_current' => true)),
            ));
            
            if (Mage::getSingleton('admin/session')->isAllowed('catalog/products/flagbit_changeattributeset')) {
                $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
                    ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
                    ->load()
                    ->toOptionHash();

                $block->getMassactionBlock()->addItem(
                    'flagbit_changeattributeset',
                    array(
                        'label'      => Mage::helper('catalog')->__('Change Attribute Set'),
                        'url'        => $block->getUrl('*/*/changeattributeset', array('_current' => true)),
                        'additional' => array(
                            'visibility' => array(
                                'name'   => 'attribute_set',
                                'type'   => 'select',
                                'class'  => 'required-entry',
                                'label'  => Mage::helper('catalog')->__('Attribute Set'),
                                'values' => $sets,
                            ),
                        ),
                    )
                );
            }
        }
    }
}