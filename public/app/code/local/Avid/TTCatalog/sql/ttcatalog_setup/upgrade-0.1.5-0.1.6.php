<?php
/**
 * @author Dane Lowe
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$installer->getConnection()->addColumn($this->getTable('shipping_matrixrate'), 'cms_block', 'varchar(256) not null');
$installer->getConnection()->addColumn($this->getTable('shipping_productmatrix'), 'cms_block', 'varchar(256) not null');
$installer->getConnection()->addColumn($this->getTable('sales/quote_address_shipping_rate'), 'cms_block', 'varchar(256) not null');
$installer->getConnection()->addColumn($this->getTable('sales/quote_address_shipping_rate'), 'shipping_time_estimate', 'varchar(256) not null');
$installer->endSetup();