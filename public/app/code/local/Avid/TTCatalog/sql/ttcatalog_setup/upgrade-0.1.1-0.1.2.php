<?php
/**
 * @author Dane Lowe
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$installer->getConnection()->addIndex(
    $this->getTable('shipping_matrixrate'),
    $installer->getIdxName($this->getTable('shipping_matrixrate'), 'area_name'),
    'area_name'
);
$installer->getConnection()->addIndex(
    $this->getTable('shipping_productmatrix'),
    $installer->getIdxName($this->getTable('shipping_productmatrix'), 'area_name'),
    'area_name'
);
$installer->endSetup();