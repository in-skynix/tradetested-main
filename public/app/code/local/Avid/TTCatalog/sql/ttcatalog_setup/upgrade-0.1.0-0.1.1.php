<?php
/**
 * @author Dane Lowe
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$installer->getConnection()->addColumn($this->getTable('shipping_matrixrate'), 'shipping_time_estimate', 'varchar(256) not null');
$installer->getConnection()->addColumn($this->getTable('shipping_matrixrate'), 'area_name', 'varchar(256) not null');
$installer->getConnection()->addColumn($this->getTable('shipping_productmatrix'), 'shipping_time_estimate', 'varchar(256) not null');
$installer->getConnection()->addColumn($this->getTable('shipping_productmatrix'), 'area_name', 'varchar(256) not null');
$installer->endSetup();