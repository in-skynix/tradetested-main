<?php
//$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer = $setup;

$installer->startSetup();

$entityTypeId     = $installer->getEntityTypeId('catalog_category');
$attributeSetId   = $installer->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $installer->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);
$group = 'Home Page';
$installer->addAttribute('catalog_category', 'show_on_home_page',  array(
    'type'     => 'int',
    'group'    => $group,
    'label'    => 'Show on Home Page?',
    'note'     => 'Should this category (and subcategories) be featured on the home page?',
    'input'    => 'select',
    'source'   => 'eav/entity_attribute_source_boolean',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
    'default'           => 0
));
$installer->addAttribute('catalog_category', 'name_on_home_page',  array(
    'type'     => 'varchar',
    'group'    => $group,
    'label'    => 'Name on Home Page',
    'note'     => 'If blank will use the full category name',
    'input'    => 'text',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
));
$installer->addAttribute('catalog_category', 'sort_order_on_home_page',  array(
    'type'     => 'int',
    'group'    => $group,
    'label'    => 'Sort on Home Page',
    'note'     => 'e.g. 10 20 30 etc. Determines order in which they are displayed on home page.',
    'input'    => 'text',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
));

$installer->endSetup();
