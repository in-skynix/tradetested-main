<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 13/05/14
 * Time: 10:46 AM
 */
class Avid_TTCatalog_CouponController extends Mage_Core_Controller_Front_Action
{
    protected $_rule;

    protected function _checkActions($object)
    {
        foreach ($object->getActions() as $_action) {
            if (
                ($_action->getAttribute() == 'sku') &&
                ($_action->getOperator() == '==') &&
                ($productId = Mage::getModel('catalog/product')->getIdBySku($_action->getValue()))
            ) {
                $product = Mage::getModel('catalog/product')->load($productId);
                $this->_getCart()->addProduct($product, array('qty' => $this->_rule->getDiscountQty()))->save();
            }
            $this->_checkActions($_action);
        }

    }
    public function addProductAction()
    {
        $couponCode = (string) $this->getRequest()->getParam('coupon_code');
        $couponModel = Mage::getModel('salesrule/coupon');
        $couponModel->loadByCode($couponCode);
        $ruleId = $couponModel->getRuleId();
        /**
         * @var Mage_SalesRule_Model_Rule $ruleModel
         */
        $ruleModel = Mage::getModel('salesrule/rule');
        $ruleModel->load($ruleId);
        $this->_rule = $ruleModel;
        try {
            $this->_checkActions($ruleModel->getActions());
            $codeLength = strlen($couponCode);
            $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
            $this->_getQuote()->setCouponCode($couponCode)
                ->collectTotals()
                ->save();

            if ($codeLength) {
                if ($couponCode == $this->_getQuote()->getCouponCode()) {
                    $this->_getSession()->addSuccess(
                        $this->__('Coupon code "%s" was applied.', Mage::helper('core')->escapeHtml($couponCode))
                    );
                } else {
                    $this->_getSession()->addError(
                        $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->escapeHtml($couponCode))
                    );
                }
            } else {
                $this->_getSession()->addSuccess($this->__('Coupon code was canceled.'));
            }

        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__('Cannot apply the coupon code.'));
            Mage::logException($e);
        }
        $this->_redirect('checkout/cart/index');
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Get current active quote instance
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote()
    {
        return $this->_getCart()->getQuote();
    }

    /**
     * Retrieve shopping cart model object
     *
     * @return Mage_Checkout_Model_Cart
     */
    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }
}