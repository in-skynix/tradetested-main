<?php
/**
 * Avid Online Enterprises Limited.
 * User: Dane
 * Date: 13/03/12
 * Time: 5:29 PM
 * Copyright (c) 2011 Avid Online Enterprises Limited. All Rights Reserved.
 */
require_once('Mage/Sendfriend/controllers/ProductController.php');
class Avid_TTCatalog_SendfriendController extends Mage_Sendfriend_ProductController
{
    /**
     * Send Email Post Action
     *
     */
    public function sendmailAction()
    {
        $product    = $this->_initProduct();
        $model      = $this->_initSendToFriendModel();
        $data       = $this->getRequest()->getPost();

        if (!$product || !$data) {
            $this->_forward('noRoute');
            return;
        }

        $categoryId = $this->getRequest()->getParam('cat_id', null);
        if ($categoryId) {
            $category = Mage::getModel('catalog/category')
                ->load($categoryId);
            $product->setCategory($category);
            Mage::register('current_category', $category);
        }
        $sp = $this->getRequest()->getPost('sender');
        $sender = array(
            'name' => $sp['name'],
            'email' => 'support@tradetested.co.nz',
            'message' => $sp['message']
        );
        $model->setSender($sender);
        $model->setRecipients($this->getRequest()->getPost('recipients'));
        $model->setProduct($product);

        try {
            $validate = $model->validate();
            if ($validate === true) {
                $model->send();
                Mage::getSingleton('catalog/session')->addSuccess($this->__('The link to a friend was sent.'));
                $this->_redirectSuccess($product->getProductUrl());
                return;
            }
            else {
                if (is_array($validate)) {
                    foreach ($validate as $errorMessage) {
                        Mage::getSingleton('catalog/session')->addError($errorMessage);
                    }
                }
                else {
                    Mage::getSingleton('catalog/session')->addError($this->__('There were some problems with the data.'));
                }
            }
        }
        catch (Mage_Core_Exception $e) {
            Mage::getSingleton('catalog/session')->addError($e->getMessage());
        }
        catch (Exception $e) {
            Mage::getSingleton('catalog/session')
                ->addException($e, $this->__('Some emails were not sent.'));
        }

        // save form data
        Mage::getSingleton('catalog/session')->setSendfriendFormData($data);

        $this->_redirectError(Mage::getURL('*/*/send', array('_current' => true)));
    }
}