<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 7/07/14
 * Time: 3:03 PM
 */
require_once('Mage/Contacts/controllers/IndexController.php');
class Avid_TTCatalog_PricematchController extends Mage_Contacts_IndexController
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('price_match_form')
            ->setFormAction( Mage::getUrl('*/*/post') )
            ->setProduct(Mage::getModel('catalog/product')->load($this->getRequest()->getParam('product')));

        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }

    public function postAction()
    {
        $post = $this->getRequest()->getPost();
        $product = Mage::getModel('catalog/product')->load($this->getRequest()->getPost('product_id'));
        if ( $post ) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);
                $postObject->setData('product_sku', $product->getSku());
                $postObject->setData('product_name', $product->getName());
                $postObject->setData('product_url', $product->getProductUrl());

                $error = false;

                if (!Zend_Validate::is(trim($post['name']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                    $error = true;
                }

                if ($error) {
                    throw new Exception();
                }
                $mailTemplate = Mage::getModel('core/email_template');
                /* @var $mailTemplate Mage_Core_Model_Email_Template */
                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->setReplyTo($post['email'])
                    ->sendTransactional(
                        'price_match_email_template',
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
                        null,
                        array('data' => $postObject)
                    );

                if (!$mailTemplate->getSentSuccess()) {
                    throw new Exception();
                }

                $translate->setTranslateInline(true);

                Mage::getSingleton('catalog/session')->addSuccess(Mage::helper('contacts')->__('Your enquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
                $this->_redirectUrl($product->getProductUrl());

                return;
            } catch (Exception $e) {
                $translate->setTranslateInline(true);

                Mage::getSingleton('catalog/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. Please, try again later'));
                $this->_redirectUrl($product->getProductUrl());
                return;
            }

        } else {
            $this->_redirectUrl($product->getProductUrl());
        }
    }
}