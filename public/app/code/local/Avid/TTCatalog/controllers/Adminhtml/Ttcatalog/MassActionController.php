<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 9/10/14
 * Time: 2:26 PM
 */
class Avid_TTCatalog_Adminhtml_Ttcatalog_MassActionController extends Mage_Adminhtml_Controller_Action
{
    protected $_availableGridBlocks = array(
        'cross_sell' => 'ttcatalog/adminhtml_catalog_product_edit_action_attribute_tab_crosssell',
        'related_products' => 'ttcatalog/adminhtml_catalog_product_edit_action_attribute_tab_related',
    );
    public function editAction()
    {
        if (!$this->_validateProducts()) {
            return;
        }

        $this->loadLayout();
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $selectionData = Mage::helper('adminhtml/js')->decodeGridSerializedInput($this->getRequest()->getPost('related_selections', ''));
        $selections = array();
        $positions = array();
        foreach ($selectionData as $_id => $_data) {
            $selections[$_id] = $_data['selected'];
            $positions[$_id] = $_data['position'];
        }
        $tabName = $this->getRequest()->getParam('tab_name', '');
        $block = $this->getLayout()->createBlock($this->_availableGridBlocks[$tabName], 'tab_'.$tabName)
            ->setSelections($selections)
            ->setPositions($positions)
        ;
        $this->getLayout()->getBlock('root')->append($block);
        $this->renderLayout();
    }

    public function saveAction()
    {

        if (!$this->_validateProducts()) {
            return;
        }
        $productIds = Mage::helper('adminhtml/catalog_product_edit_action_attribute')->getProductIds();
        $links = $this->getRequest()->getParam('links');

        $related = Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['related']);
        Mage::getResourceModel('ttcatalog/catalog_product_link')->saveMassProductLinks($productIds, $related, Mage_Catalog_Model_Product_Link::LINK_TYPE_RELATED);

        $crossSell = Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['cross_sell']);
        Mage::getResourceModel('ttcatalog/catalog_product_link')->saveMassProductLinks($productIds, $crossSell, Mage_Catalog_Model_Product_Link::LINK_TYPE_CROSSSELL);
        $this->_redirect('*/*/edit');
    }

    /**
     * Attributes validation action
     *
     */
    public function validateAction()
    {
        $response = new Varien_Object();
        $response->setError(false);
        $this->getResponse()->setBody($response->toJson());
    }


    /**
     * Rertive data manipulation helper
     *
     * @return Mage_Adminhtml_Helper_Catalog_Product_Edit_Action_Attribute
     */
    protected function _getHelper()
    {
        return Mage::helper('adminhtml/catalog_product_edit_action_attribute');
    }

    /**
     * Validate selection of products for massupdate
     *
     * @return boolean
     */
    protected function _validateProducts()
    {
        $error = false;
        $productIds = $this->_getHelper()->getProductIds();
        if (!is_array($productIds)) {
            $error = $this->__('Please select products for attributes update');
        } else if (!Mage::getModel('catalog/product')->isProductsHasSku($productIds)) {
            $error = $this->__('Some of the processed products have no SKU value defined. Please fill it prior to performing operations on these products.');
        }

        if ($error) {
            $this->_getSession()->addError($error);
            $this->_redirect('*/catalog_product/', array('_current'=>true));
        }

        return !$error;
    }
}
