<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 22/09/14
 * Time: 4:44 PM
 */
class Avid_HealthCheck_Block_Checker extends Mage_Core_Block_Template
{
    protected $_items = array();

    protected function _construct()
    {
        $this->setTemplate('avid/health_check/checker.phtml');
    }

    public function addItem($item)
    {
        $this->_items[] = $item;
    }

    public function getItemRenderers()
    {
        $renderers = array();
        foreach($this->_items as $_item) {
            $renderers[] = $_item->getRenderer();
        }
        return $renderers;
    }
}