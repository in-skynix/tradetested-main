<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 22/09/14
 * Time: 4:44 PM
 */
class Avid_HealthCheck_Block_Item extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        $this->setTemplate('avid/health_check/item.phtml');
    }
}