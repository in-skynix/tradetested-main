<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 22/09/14
 * Time: 4:02 PM
 */
class Avid_HealthCheck_Model_Item_Abstract extends Mage_Core_Model_Abstract
{
    protected $_title = 'Health Check';

    public function getTitle()
    {
        return $this->_title;
    }

    public function run()
    {
        $this->setTextOutput('results');
    }

    public function getRenderer()
    {
        return Mage::app()->getLayout()->createBlock('avid_health_check/item')->setItem($this);
    }
}