<?php
/**
 * Class Avid_HealthCheck_Model_Item_TruncateTables
 *
 * Truncate a bunch of tables that grow out of hand and aren't very useful
 * e.g.
 * TRUNCATE dataflow_batch_export;
 * TRUNCATE dataflow_batch_import;
 * TRUNCATE log_customer;
 * TRUNCATE log_quote;
 * TRUNCATE log_summary;
 * TRUNCATE log_summary_type;
 * TRUNCATE log_url;
 * TRUNCATE log_url_info;
 * TRUNCATE log_visitor;
 * TRUNCATE log_visitor_info;
 * TRUNCATE log_visitor_online;
 * TRUNCATE report_viewed_product_index;
 * TRUNCATE report_compared_product_index;
 * TRUNCATE report_event;
 * TRUNCATE index_event;
 */
class Avid_HealthCheck_Model_Item_TruncateTables extends Avid_HealthCheck_Model_Item_Abstract
{
    protected $_title = "Truncate Tables";

    public function run()
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $x = 0;
        foreach ($this->_getResourcesNamesToTruncate() as $_resourceName) {
            $table = Mage::getSingleton('core/resource')->getTableName($_resourceName);
            $connection->truncate($table);
            $x++;
        }
        $this->setNumberTruncated($x);
    }

    public function getTextOutput()
    {
        return "Truncated {$this->getNumberTruncated()} tables";
    }

    protected function _getResourcesNamesToTruncate()
    {
        return array(
            'dataflow/batch_export',
            'dataflow/batch_import',
            'log/customer',
            'log/quote_table',
            'log/visitor',
            'log/visitor_info',
            'log/visitor_online',
            'reports/viewed_product_index',
            'reports/compared_product_index',
            'reports/event',
            'index/process_event'
        );
    }
}