<?php
/**
 * Class Avid_HealthCheck_Model_Item_SmtpProLogs
 *
 * Clean out old SMTP Pro logs and report on the number cleaned
 */
class Avid_HealthCheck_Model_Item_SmtpProLogs extends Avid_HealthCheck_Model_Item_Abstract
{
    protected $_title = 'Old SMTP Pro Logs';

    public function run()
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $condition = $connection->quoteInto('log_at < DATE_SUB(Now(), INTERVAL 60 DAY)');
        $table = Mage::getSingleton('core/resource')->getTableName('smtppro/email_log');
        $result = $connection->delete($table,$condition);
        $this->setNumberDeleted($result);
    }

    public function getTextOutput()
    {
        return "Deleted {$this->getNumberDeleted()} SMTP Pro logs that were more than 60 days old";
    }
}
