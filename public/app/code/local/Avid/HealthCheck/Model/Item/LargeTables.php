<?php
/**
 * Class Avid_HealthCheck_Model_Item_LargeTables
 *
 * Find the largest tables in the database and report on their size
 * e.g.
 *
 *
 */
class Avid_HealthCheck_Model_Item_LargeTables extends Avid_HealthCheck_Model_Item_Abstract
{
    protected $_title =  'Largest Tables in Database';

    public function run()
    {
        $sql = <<<SQL
SELECT CONCAT(table_schema, '.', table_name)                                          'table',
       CONCAT(ROUND(table_rows / 1000000, 2), 'M')                                    rows,
       CONCAT(ROUND(data_length / ( 1024 * 1024 * 1024 ), 2), 'G')                    DATA,
       CONCAT(ROUND(index_length / ( 1024 * 1024 * 1024 ), 2), 'G')                   idx,
       CONCAT(ROUND(( data_length + index_length ) / ( 1024 * 1024 * 1024 ), 2), 'G') total_size,
       ROUND(index_length / data_length, 2)                                           idxfrac
FROM   information_schema.TABLES
ORDER  BY data_length + index_length DESC
LIMIT  10;
SQL;
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $results = $readConnection->fetchAll($sql);
        $this->setResults($results);
    }

    public function getTextOutput()
    {
        $renderer = new Avid_AsciiTable($this->getResults());
        $renderer->showHeaders(true);
        return $renderer->render();
    }

}