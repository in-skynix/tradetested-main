<?php
/**
 * Class Avid_HealthCheck_Model_Item_UrlRewrite
 *
 * Reports on the growth of core_url_rewrite_table since last run.
 */
class Avid_HealthCheck_Model_Item_UrlRewrite extends Avid_HealthCheck_Model_Item_Abstract
{
    protected $_title = "URL Rewrite Growth Informations";

    public function run()
    {
        if (!($oldRowCount = Mage::getStoreConfig('avid_health_check/url_rewrite_rowcount'))) {
            $oldRowCount = 0;
        }
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $results = $readConnection->fetchAll(
            "SELECT count(url_rewrite_id) as count FROM ".Mage::getSingleton('core/resource')->getTableName('core/url_rewrite')
        );
        $newRowCount = $results[0]['count'];
        Mage::getModel('core/config')->saveConfig('avid_health_check/url_rewrite_rowcount', $newRowCount);
        $this->setOldRowCount($oldRowCount);
        $this->setNewRowCount($newRowCount);
    }

    public function getTextOutput()
    {
        return "core_url_rewrite has grown {$this->_getPercentGrowth()}% from {$this->getOldRowCount()} to {$this->getNewRowCount()}";
    }

    protected function _getPercentGrowth()
    {
        if (!$this->getOldRowCount()) {
            return 100;
        }
        return round(($this->getNewRowCount() - $this->getOldRowCount())/$this->getOldRowCount()*100);
    }

}