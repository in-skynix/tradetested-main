<?php
/**
 * Class Avid_HealthCheck_Model_Item_OldQuotes
 *
 * Deletes old quotes from the database, and reports on the number cleared e.g.
 * DELETE FROM sales_flat_quote WHERE updated_at < DATE_SUB(Now(),INTERVAL 60 DAY)
 */
class Avid_HealthCheck_Model_Item_OldQuotes extends Avid_HealthCheck_Model_Item_Abstract
{
    protected $_title = 'Old Quotes';

    public function run()
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $condition = $connection->quoteInto('updated_at < DATE_SUB(Now(), INTERVAL 60 DAY)');
        $table = Mage::getSingleton('core/resource')->getTableName('sales/quote');
        $result = $connection->delete($table,$condition);
        $this->setNumberDeleted($result);
    }

    public function getTextOutput()
    {
        return "Deleted {$this->getNumberDeleted()} quotes that are more than 60 days old";
    }

}