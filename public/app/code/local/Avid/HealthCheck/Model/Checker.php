<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 22/09/14
 * Time: 4:01 PM
 */
class Avid_HealthCheck_Model_Checker extends Mage_Core_Model_Abstract
{
    protected $_block;

    protected function _getRecipients()
    {
        return array(
            'dane@avidonline.co.nz',
            'richard@tradetested.co.nz'
        );
    }

    /**
     * @todo: refactor entirely, run should set the results, which can later be used in other methods.
     */
    public function email()
    {
        $body = $this->run();
        foreach ($this->_getRecipients() as $email) {
            $mail = Mage::getModel('core/email');
            $mail->setToEmail($email);
            $mail->setBody($body);
            $mail->setSubject('Magento Health Check Results');
            $mail->setFromEmail('magento@tradetested.co.nz');
            $mail->setFromName("Magento Store");
            $mail->setType('text');
            $mail->send();
        }
    }

    public function run()
    {
        $items = $this->getItems();
        $block = $this->getBlock();
        foreach ($items as $_item) {
            $_item->run();
            $block->addItem($_item);
        }
        return $block->setFormat('html')->toHtml();
    }

    public function getBlock()
    {
        if (!$this->_block) {
            $this->_block = Mage::app()->getLayout()->getBlockSingleton('avid_health_check/checker');
        }
        return $this->_block;
    }

    /**
     * Get Item from Code
     *
     * @param $code
     * @return false|Avid_HealthCheck_Model_Item_Abstract
     */
    public function getItem($code)
    {
        return Mage::getModel(Mage::getConfig()->getNode("global/avid/health_check/items/{$code}/model"));
    }

    /**
     * Get All Registered Item Models.
     *
     * @return array
     */
    public function getItems()
    {
        $statistics = array();
        foreach ($this->getAllItemCodes() as $_code)
            $statistics[] = $this->getItem($_code);
        return $statistics;
    }

    /**
     * Get All Registered Item Codes
     *
     * @return array
     */
    public function getAllItemCodes()
    {
        return array_keys(Mage::getConfig()->getNode("global/avid/health_check/items")->asArray());
    }
}