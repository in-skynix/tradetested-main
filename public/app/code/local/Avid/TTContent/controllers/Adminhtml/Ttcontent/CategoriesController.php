<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 20/03/14
 * Time: 2:25 PM
 */
class Avid_TTContent_Adminhtml_Ttcontent_CategoriesController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Init actions
     *
     * @return Mage_Adminhtml_Cms_PageController
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
            ->_setActiveMenu('cms/ttcontent')
            ->_addBreadcrumb(Mage::helper('cms')->__('CMS'), Mage::helper('cms')->__('CMS'))
            ->_addBreadcrumb(Mage::helper('ttcontent')->__('Manage Content Categories'), Mage::helper('ttcontent')->__('Manage Content Categories'))
        ;
        return $this;
    }


    /**
     * @todo: refactor
     */
    public function categoriesJsonAction()
    {
        $field = new Avid_Data_Form_Element_Categories(
            array('value' => explode(',', $this->getRequest()->getParam('v')))
        );
        $this->getResponse()->setBody(
            $field->getCategoryChildrenJson($this->getRequest()->getParam('category'))
        );
    }

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->_title($this->__('CMS'))
            ->_title($this->__('Content Categories'));

        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Create new CMS page
     */
    public function newAction()
    {
        // the same form is used to create and edit
        $this->_forward('edit');
    }

    /**
     * Edit CMS page
     */
    public function editAction()
    {
        $this->_title($this->__('CMS'))
            ->_title($this->__('Content Categories'));

        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('content_category_id');
        $model = Mage::getModel('ttcontent/content_category');

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (! $model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ttcontent')->__('This content category no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }

        $this->_title($model->getId() ? $model->getTitle() : $this->__('New Content Category'));

        // 3. Set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) {
            $model->setData($data);
        }

        // 4. Register model to use later in blocks
        Mage::register('ttcontent_content_category', $model);

        // 5. Build edit form
        $this->_initAction()
            ->_addBreadcrumb(
                $id ? Mage::helper('ttcontent')->__('Edit Content Category')
                    : Mage::helper('ttcontent')->__('New Content Category'),
                $id ? Mage::helper('ttcontent')->__('Edit Content Category')
                    : Mage::helper('ttcontent')->__('New Content Category'));

        $this->renderLayout();
    }

    /**
     * Save action
     */
    public function saveAction()
    {
        // check if data sent
        if ($data = $this->getRequest()->getPost()) {
            //init model and set data
            $model = Mage::getModel('ttcontent/content_category');

            if ($id = $this->getRequest()->getParam('content_category_id')) {
                $model->load($id);
            }

            $model->setData($data);

            // try to save it
            try {
                // save the data
                $model->save();

                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ttcontent')->__('The content category has been saved.'));
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('content_category_id' => $model->getId(), '_current'=>true));
                    return;
                }
                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
            catch (Exception $e) {
                $this->_getSession()->addException($e,
                    Mage::helper('ttcontent')->__('An error occurred while saving the content category.'));
            }

            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/edit', array('content_category_id' => $this->getRequest()->getParam('content_category_id')));
            return;
        }
        $this->_redirect('*/*/');
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        // check if we know what should be deleted
        if ($id = $this->getRequest()->getParam('content_category_id')) {
            $title = "";
            try {
                // init model and delete
                $model = Mage::getModel('ttcontent/content_category');
                $model->load($id);
                $title = $model->getTitle();
                $model->delete();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ttcontent')->__('The content category has been deleted.'));
                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // go back to edit form
                $this->_redirect('*/*/edit', array('content_category_id' => $id));
                return;
            }
        }
        // display error message
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ttcontent')->__('Unable to find a content category to delete.'));
        // go to grid
        $this->_redirect('*/*/');
    }
}