<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 24/03/14
 * Time: 2:45 PM
 */
class Avid_TTContent_CategoriesController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function viewAction()
    {
        $contentCategory = Mage::getModel('ttcontent/content_category')->load($this->getRequest()->getParam('id'));
        if (!$contentCategory->getId()) {
            return $this->_forward('noRoute');
        }
        Mage::register('ttcontent.content_category', $contentCategory);
        $this->loadLayout();
        $this->renderLayout();
    }
}