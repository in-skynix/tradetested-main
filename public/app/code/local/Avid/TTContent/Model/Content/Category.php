<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 20/03/14
 * Time: 1:43 PM
 */
class Avid_TTContent_Model_Content_Category extends Mage_Core_Model_Abstract
{
    const CACHE_TAG     = 'ttcontent.content_category';
    protected $_cacheTag= 'ttcontent.content_category';

    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('ttcontent/content_category');
    }

    public function getPages()
    {
        return Mage::getModel('cms/page')->getCollection()
            ->addStoreFilter(Mage::app()->getStore())
            ->addFieldToFilter('content_category_id', $this->getId())
            ->addFieldToFilter('is_active', 1)
            ->setOrder('title ', 'ASC');
    }

    /**
     * Check if page identifier exist for specific store
     * return page id if page exists
     *
     * @param string $identifier
     * @param int $storeId
     * @return int
     */
    public function checkIdentifier($identifier)
    {
        return $this->_getResource()->checkIdentifier($identifier);
    }
}