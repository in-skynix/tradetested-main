<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 20/03/14
 * Time: 5:35 PM
 */
class Avid_TTContent_Model_Observer
{
    public function cmsPagePrepareSave($observer)
    {
        $event = $observer->getEvent();
        $page = $event->getPage();
        //Need to null out blank values due to foreign key constraint failing
        foreach (array('content_category_id', 'catalog_category_id') as $_attribute) {
            if (!$page->getData($_attribute)) {
                $page->setData($_attribute, null);
            }
        }
    }

    public function cmsPageLoadAfter($observer)
    {
        $event = $observer->getEvent();
        $page = $event->getObject();
        if ($page->getId()) {
            $categories = Mage::getResourceModel('ttcontent/content_category')->categoryIdsForPageId($page->getId());
            $page->setData('catalog_category_ids', $categories);
        }
    }

    public function cmsPageSaveAfter($observer)
    {
        $event = $observer->getEvent();
        $page = $event->getObject();
        if (!is_array($page->getCatalogCategoryIds())) {
            $page->setCatalogCategoryIds(array_filter(explode(',', $page->getCatalogCategoryIds())));
        }
        if ($page->getId()) {
            Mage::getResourceModel('ttcontent/content_category')->afterSavePage($page);
        }
    }
}