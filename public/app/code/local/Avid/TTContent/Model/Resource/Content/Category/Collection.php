<?php
class Avid_TTContent_Model_Resource_Content_Category_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('ttcontent/content_category');
    }

    public function asOptionArray()
    {
        $options = array('' => '');
        foreach ($this as $_item) {
            $options[$_item->getId()] = $_item->getTitle();
        }
        return $options;
    }
}