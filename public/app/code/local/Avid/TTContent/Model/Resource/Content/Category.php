<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 20/03/14
 * Time: 1:46 PM
 */
class Avid_TTContent_Model_Resource_Content_Category extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('ttcontent/content_category', 'content_category_id');
    }

    public function categoryIdsForPageId($pageId)
    {
        $adapter = $this->_getReadAdapter();
        $select  = $adapter->select()
            ->from($this->getTable('ttcontent/cms_page_catalog_category'), 'catalog_category_id')
            ->where('page_id = ?',(int)$pageId);
        return $adapter->fetchCol($select);
    }

    /**
     * Assign page to store views
     *
     * @param Mage_Cms_Model_Page $object
     * @return $this
     */
    public function afterSavePage(Mage_Cms_Model_Page $object)
    {
        $oldCats = $this->categoryIdsForPageId($object->getId());
        $newCats = (array)$object->getCatalogCategoryIds();
        $table  = $this->getTable('ttcontent/cms_page_catalog_category');
        $insert = array_diff($newCats, $oldCats);
        $delete = array_diff($oldCats, $newCats);

        if ($delete) {
            $where = array(
                'page_id = ?'     => (int) $object->getId(),
                'catalog_category_id IN (?)' => $delete
            );

            $this->_getWriteAdapter()->delete($table, $where);
        }

        if ($insert) {
            $data = array();

            foreach ($insert as $catId) {
                $data[] = array(
                    'page_id'  => (int) $object->getId(),
                    'catalog_category_id' => (int) $catId
                );
            }
            $this->_getWriteAdapter()->insertMultiple($table, $data);
        }
    }


    /**
     * Load an object using 'url_key' field if there's no field specified and value is not numeric
     *
     * @param Mage_Core_Model_Abstract $object
     * @param mixed $value
     * @param string $field
     * @return $this
     */
    public function load(Mage_Core_Model_Abstract $object, $value, $field = null)
    {
        if (!is_numeric($value) && is_null($field)) {
            $field = 'url_key';
        }

        return parent::load($object, $value, $field);
    }

    /**
     * Retrieve load select with filter by identifier, store and activity
     *
     * @param $identifier
     * @return Varien_Db_Select
     */
    protected function _getLoadByIdentifierSelect($identifier)
    {
        $select = $this->_getReadAdapter()->select()
            ->from(array('main' => $this->getMainTable()))
            ->where('url_key = ?', $identifier);
        return $select;
    }

    /**
     * Check if category identifier exist for specific store
     * return page id if page exists
     *
     * @param string $identifier
     * @return int
     */
    public function checkIdentifier($identifier)
    {
        $select = $this->_getLoadByIdentifierSelect($identifier, 1);
        $select->reset(Zend_Db_Select::COLUMNS)
            ->columns('content_category_id')
            ->limit(1);

        return $this->_getReadAdapter()->fetchOne($select);
    }
}