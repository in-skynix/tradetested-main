<?php
class Avid_TTContent_Controller_Router extends Mage_Core_Controller_Varien_Router_Standard
{
    /**
     * Initialize Controller Router
     *
     * @param Varien_Event_Observer $observer
     */
    public function initControllerRouters($observer)
    {
        /* @var $front Mage_Core_Controller_Varien_Front */
        $front = $observer->getEvent()->getFront();
        $front->addRouter('ttcontent', $this);
    }

    /**
     * Validate and Match Content Category and modify request
     *
     * @param Zend_Controller_Request_Http $request
     * @return bool
     */
    public function match(Zend_Controller_Request_Http $request)
    {
        if (!Mage::isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                ->setRedirect(Mage::getUrl('install'))
                ->sendResponse();
            exit;
        }

        $identifier = trim($request->getPathInfo(), '/');
        $contentCategory   = Mage::getModel('ttcontent/content_category');
        $id = $contentCategory->checkIdentifier($identifier);
        if (!$id) {
            return false;
        }

        $request->setModuleName('ttcontent')
            ->setRouteName('avid_ttcontent')
            ->setControllerName('categories')
            ->setActionName('view')
            ->setParam('id', $id);

        return true;
    }
}
