<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('ttcontent/content_category'))
    ->addColumn('content_category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Content Category ID')
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_CHAR, 255, array(), 'Title')
    ->addColumn('url_key', Varien_Db_Ddl_Table::TYPE_CHAR, 255, array(), 'URL Key')
    ->addColumn('short_description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ), 'Short Description')
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable('ttcontent/content_category'),
            array('url_key'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        'url_key',
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    );
$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('ttcontent/cms_page_catalog_category'))
    ->addColumn('page_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Page ID')
    ->addColumn('catalog_category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Catalog Category ID')
    ->addForeignKey(
        $installer->getFkName(
            'ttcontent/cms_page_catalog_category',
            'catalog_category_id',
            'catalog/category',
            'entity_id'
        ),
        'catalog_category_id',
        $this->getTable('catalog/category'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName(
            'ttcontent/cms_page_catalog_category',
            'page_id',
            'cms/page',
            'page_id'
        ),
        'page_id',
        $this->getTable('cms/page'),
        'page_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );
$installer->getConnection()->createTable($table);

$tableName = $installer->getTable('cms/page');

$installer->getConnection()->addColumn($tableName, 'content_category_id', array(
            'nullable' => true,
            'unsigned'  => true,
            'length' => 9,
            'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'comment' => 'Content Category ID'
        )
    );
$installer->getConnection()->addColumn($tableName, 'short_description', array(
            'nullable' => false,
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'comment' => 'Short Description'
        )
    );
$installer->getConnection()->addForeignKey(
        $installer->getFkName('cms/page', 'content_category_id', 'ttcontent/content_category', 'content_category_id'),
        $tableName,
        'content_category_id',
        $this->getTable('ttcontent/content_category'),
        'content_category_id',
        Varien_Db_Ddl_Table::ACTION_NO_ACTION
    );

$installer->endSetup();
