<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$installer->getConnection()->addColumn($this->getTable('cms/page'), 'is_featured', 'int(1) not null');
$installer->getConnection()->addIndex(
    $this->getTable('cms/page'),
    $installer->getIdxName(
        $installer->getTable('cms/page'),
        array('is_featured'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
    ),
    'is_featured',
    Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
);
$installer->endSetup();
