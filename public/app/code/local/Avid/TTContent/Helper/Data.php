<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 20/03/14
 * Time: 3:49 PM
 */
class Avid_TTContent_Helper_Data extends Mage_Core_Helper_Data
{
    public function getCatalogCategorySelectOptions()
    {
        $options = array();
        $allCategories = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSort('path', 'ASC')
            ->getItems();
        foreach ($allCategories as $_category) {
            $options[$_category->getId()] = str_pad($_category->getName(), $_category->getLevel()+strlen($_category->getName()), '_', STR_PAD_LEFT);
        }
        return $options;
    }

    public function includeContentCategoryAssociation(Mage_Cms_Model_Resource_Page_Collection $collection)
    {
        return Mage::helper('avid_core/association')->includeBelongsToAssociation(
            $collection,
            'content_category',
            'ttcontent/content_category',
            'main_table.content_category_id = content_category.content_category_id'
        );
    }

}