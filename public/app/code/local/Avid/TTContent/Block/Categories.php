<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 24/03/14
 * Time: 2:53 PM
 */
class Avid_TTContent_Block_Categories extends Mage_Core_Block_Template
{
    public function getContentCategories()
    {
        return Mage::getModel('ttcontent/content_category')->getCollection();
    }
}