<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 25/03/14
 * Time: 1:19 PM
 */
class Avid_TTContent_Block_Rewrite_Cms_Page extends Mage_Cms_Block_Page
{
    /**
     * Prepare global layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $page = $this->getPage();

        // show breadcrumbs
        if (Mage::getStoreConfig('web/default/show_cms_breadcrumbs')
            && ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs'))
            && ($page->getIdentifier()!==Mage::getStoreConfig('web/default/cms_home_page'))
            && ($page->getIdentifier()!==Mage::getStoreConfig('web/default/cms_no_route'))) {
                $breadcrumbs->addCrumb('home', array('label'=>Mage::helper('cms')->__('Home'), 'title'=>Mage::helper('cms')->__('Go to Home Page'), 'link'=>Mage::getBaseUrl()));
                if (($id = $page->getContentCategoryId()) && ($cat = Mage::getModel('ttcontent/content_category')->load($id)) && $cat->getId()) {
                    $breadcrumbs->addCrumb('content_category', array(
                       'label' => $cat->getTitle(),
                       'title' => $cat->getTitle(),
                       'link' => $this->getUrl($cat->getUrlKey())
                    ));
                }
                $breadcrumbs->addCrumb('cms_page', array('label'=>$page->getTitle(), 'title'=>$page->getTitle()));
        }

        $root = $this->getLayout()->getBlock('root');
        if ($root) {
            $root->addBodyClass('cms-'.$page->getIdentifier());
            if (isset($cat) && $cat->getUrlKey()) {
                $root->addBodyClass($cat->getUrlKey());
            }
        }

        $head = $this->getLayout()->getBlock('head');
        if ($head) {
            $head->setTitle($page->getTitle());
            $head->setKeywords($page->getMetaKeywords());
            $head->setDescription($page->getMetaDescription());
        }

        return parent::_prepareLayout();
    }
}