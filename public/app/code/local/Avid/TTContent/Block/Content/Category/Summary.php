<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 24/03/14
 * Time: 3:09 PM
 */
class Avid_TTContent_Block_Content_Category_Summary extends Mage_Core_Block_Template
{
    /**
     * Prepare global layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        //It will only be set here if set in registry, setting in xml happens after this.
        if ($contentCategory = $this->getContentCategory()) {

            // show breadcrumbs
            if (Mage::getStoreConfig('web/default/show_cms_breadcrumbs')
                && ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs'))
            ) {
                $breadcrumbs->addCrumb('home', array('label' => Mage::helper('cms')->__('Home'), 'title' => Mage::helper('cms')->__('Go to Home Page'), 'link' => Mage::getBaseUrl()));
                $breadcrumbs->addCrumb('cms_page', array('label' => $contentCategory->getTitle(), 'title' => $contentCategory->getTitle()));
            }

            $root = $this->getLayout()->getBlock('root');
            if ($root) {
                $root->addBodyClass('ttcontent-category-' . $contentCategory->getIdentifier());
            }

            $head = $this->getLayout()->getBlock('head');
            if ($head) {
                $head->setTitle($contentCategory->getTitle());
            }
        }
        return parent::_prepareLayout();
    }

    public function getContentCategory()
    {
        if (!$this->getData('content_category')) {
            $category = $this->getData('content_category_code') ?
                Mage::getModel('ttcontent/content_category')->load($this->getData('content_category_code')) :
                Mage::registry('ttcontent.content_category');
            $this->setData('content_category', $category);
        }
        return $this->getData('content_category');
    }

    public function getCacheKeyInfo()
    {
        $info = parent::getCacheKeyInfo();
        $info[] = $this->getContentCategoryCode();
        return $info;
    }
}
