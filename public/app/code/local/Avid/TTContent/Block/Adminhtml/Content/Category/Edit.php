<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 20/03/14
 * Time: 4:18 PM
 */
class Avid_TTContent_Block_Adminhtml_Content_Category_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        $this->_objectId = 'cms_content_id';
        $this->_blockGroup = 'ttcontent';
        $this->_controller = 'adminhtml_content_category';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('ttcontent')->__('Save Category'));
        $this->_updateButton('delete', 'label', Mage::helper('ttcontent')->__('Delete Category'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('block_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'block_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'block_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * Get edit form container header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('ttcontent_content_category')->getId()) {
            return Mage::helper('cms')->__("Edit Content Category '%s'", $this->escapeHtml(Mage::registry('ttcontent_content_category')->getTitle()));
        } else {
            return Mage::helper('cms')->__('New Content Category');
        }
    }

    /**
     * Get form action URL
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        return $this->getUrl('*/*/save');
    }
}
