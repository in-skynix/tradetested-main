<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 20/03/14
 * Time: 5:01 PM
 */
class Avid_TTContent_Block_Adminhtml_Page_Edit_Tab_Categories
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('page_');
        $model = Mage::registry('cms_page');

        $fieldset = $form->addFieldset('meta_fieldset', array('legend' => Mage::helper('ttcontent')->__('Content Categories'), 'class' => 'fieldset-wide'));
        $fieldset->addField('short_description', 'editor', array(
            'name' => 'short_description',
            'label' => Mage::helper('ttcontent')->__('Short Description'),
            'title' => Mage::helper('ttcontent')->__('Short Description'),
        ));

        $fieldset->addField('content_category_id', 'select', array(
            'name' => 'content_category_id',
            'label' => Mage::helper('ttcontent')->__('Content Category'),
            'title' => Mage::helper('ttcontent')->__('Content Category'),
            'options' => Mage::getModel('ttcontent/content_category')->getCollection()->asOptionArray()
        ));
        $fieldset->addType('categories', 'Avid_Data_Form_Element_Categories');
        $fieldset->addField('catalog_category_ids', 'categories', array(
            'name' => 'catalog_category_ids',
            'label' => Mage::helper('ttcontent')->__('Catalog Category'),
            'title' => Mage::helper('ttcontent')->__('Catalog Category'),
            'options' => Mage::helper('ttcontent')->getCatalogCategorySelectOptions()
        ));

        $fieldset->addField('is_featured', 'select', array(
            'name' => 'is_featured',
            'label' => Mage::helper('ttcontent')->__('Featured?'),
            'title' => Mage::helper('ttcontent')->__('Featured?'),
            'options'	=> array('1' => Mage::helper('adminhtml')->__('Yes'), '0' => Mage::helper('adminhtml')->__('No')),
        ));

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('ttcontent')->__('Content Categories');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('ttcontent')->__('Categories');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }
}
