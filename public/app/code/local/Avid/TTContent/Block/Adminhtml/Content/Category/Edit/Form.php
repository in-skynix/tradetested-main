<?php
class Avid_TTContent_Block_Adminhtml_Content_Category_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Init form
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('ttcontent_content_category_form');
        $this->setTitle(Mage::helper('cms')->__('Content Category Details'));
    }

    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    protected function _prepareForm()
    {
        $model = Mage::registry('ttcontent_content_category');

        $form = new Varien_Data_Form(
            array('id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post')
        );

        $form->setHtmlIdPrefix('content_category_');

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('cms')->__('General Information'), 'class' => 'fieldset-wide'));

        if ($model->getContentCategoryId()) {
            $fieldset->addField('content_category_id', 'hidden', array(
                'name' => 'content_category_id',
            ));
        }

        $fieldset->addField('title', 'text', array(
            'name'      => 'title',
            'label'     => Mage::helper('cms')->__('Category Title'),
            'title'     => Mage::helper('cms')->__('Category Title'),
            'required'  => true,
        ));

        $fieldset->addField('url_key', 'text', array(
            'name'      => 'url_key',
            'label'     => Mage::helper('cms')->__('URL Key'),
            'title'     => Mage::helper('cms')->__('URL Key'),
            'required'  => true,
        ));

        $fieldset->addField('short_description', 'editor', array(
            'name'      => 'short_description',
            'label'     => Mage::helper('cms')->__('Short Description'),
            'title'     => Mage::helper('cms')->__('Short Description'),
            'style'     => 'height:36em',
            'required'  => true,
            'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig()
        ));

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
