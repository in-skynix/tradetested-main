<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 30/07/13
 * Time: 3:29 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_TTContent_Block_Adminhtml_Content_Categories_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Construct grid block
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('ttcontent_content_categories_grid');
        $this->setDefaultSort('title', 'asc');
        $this->setUseAjax(true);
    }

    /**
     * Prepare Collection
     *
     * @return this
     */
    protected function _prepareCollection()
    {
        $this->setCollection(Mage::getModel('ttcontent/content_category')->getCollection());
        return parent::_prepareCollection();
    }

    /**
     * Prepare Columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('content_category_id', array(
                'header'=> Mage::helper('ttcatalog')->__('ID'),
                'width' => '50px',
                'type'  => 'number',
                'index' => 'content_category_id',
        ));
        $this->addColumn('title', array(
            'header' => Mage::helper('ttcatalog')->__('Title'),
            'index' => 'title',
        ));
        $this->addColumn('url_key', array(
            'header' => Mage::helper('ttcatalog')->__('URL Key'),
            'index' => 'url_key',
        ));

        return parent::_prepareColumns();
    }

    /**
     * Get Grid URL for use in AJAX requests
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }


    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('content_category_id' => $row->getContentCategoryId()));
    }
}
