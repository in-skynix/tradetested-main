<?php
class Avid_TTContent_Block_Adminhtml_Content_Categories extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Construct Block
     *
     * Set Template, controller. Add buttons.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_content_categories';
        $this->_blockGroup = 'ttcontent';
        $this->_headerText = Mage::helper('ttcontent')->__('Content Categories');
        $this->_addButtonLabel = Mage::helper('ttcontent')->__('Add New Content Category');
        parent::__construct();
    }
}
