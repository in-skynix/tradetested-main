<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 11/04/14
 * Time: 1:18 PM
 */
class Avid_TTContent_Block_List_Catalog extends Mage_Core_Block_Template
{
    public function getProduct()
    {
        if (!Mage::registry('product') && $this->getProductId()) {
            $product = Mage::getModel('catalog/product')->load($this->getProductId());
            Mage::register('product', $product);
        }
        return Mage::registry('product');
    }

    public function getCategory()
    {
        if (!Mage::registry('current_category') && $this->getCategoryId()) {
            $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
            Mage::register('current_category', $category);
        }
        return Mage::registry('current_category');
    }

    public function getGuides()
    {
        $ids = array();
        if ($cat = $this->getCategory()) {
            $ids[] = $cat->getId();
            $parentIds = explode('/', $cat->getPath());
            foreach ($parentIds as $id) $ids[] = $id;
        }

        $collection = Mage::getModel('cms/page')->getCollection()
            ->join(array('pc'=>'ttcontent/cms_page_catalog_category'), 'main_table.page_id = pc.page_id')
            ->addStoreFilter(Mage::app()->getStore())
            ->addFieldToFilter('is_active', true)
            ->addFieldToFilter('catalog_category_id', array('in' => $ids));
        Mage::helper('ttcontent')->includeContentCategoryAssociation($collection);
        $collection->getSelect()->group('main_table.page_id');
        return $collection;
    }
}