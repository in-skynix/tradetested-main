<?php
ini_set('memory_limit', '1280M');
error_reporting(0);

class Wyomind_Datafeedmanager_Model_Datafeedmanager extends Mage_Core_Model_Abstract
{
    public $_indexPhp = '';
    protected $_filePath;
    public $_limit = false;
    public $_display = false;
    public $_rates = false;
    public $_chartset = false;
    public $_sqlSize = 1500;
    static $option = 0;

    public function cleanXml($xml, $x33 = true, $x34 = false)
    {
        if ($x34) {
            $x35       = preg_split("\57\x0a/", $xml);
            $x36 = $x35[0];
            $x35[0] = null;
            $xml = implode($x35, "");
        }
        $xml = str_replace('<?', utf8_encode('¤'), $xml);
        $xml = str_replace('?>', utf8_encode('¤'), $xml);
        preg_match_all(utf8_encode('/(¤(.[^¤]+)¤)/s'), $xml, $matches);
        if (isset($matches[1])) {
            foreach ($matches[1] as $x39 => $x3a) {
                if ($x33 == 1) {
                    if (@eval($matches[2][$x39] . '; ')) $xml = str_replace($x3a, eval($matches[2][$x39] . '; '), $xml);
                    else $xml = str_replace($x3a, '', $xml);
                } else {
                    if (@eval($this->unx11d($matches[2][$x39] . '; '))) $xml = str_replace($x3a, $this->x11d(eval($this->unx11d($matches[2][$x39]) . '; ')), $xml);
                    else $xml = str_replace($x3a, '', $xml);
                }
            }
        }
        if ($x34) {
            if ($x33 == 1)
                return $x36 . $xml;
            else
                return $x36;
        } else return $xml;
    }

    protected function _construct()
    {
        $this->_sqlSize = Mage::getStoreConfig("datafeedmanager/system/sqlsize");
        $this->_init('datafeedmanager/datafeedmanager');
    }

    protected function _beforeSave()
    {
        $file = new Varien_Io_File();
        $x3c = $file->getCleanPath(Mage::getBaseDir() . '/' . $this->getFeedPath());
        if (!$file->allowedPath($x3c, Mage::getBaseDir())) {
            Mage::throwException(Mage::helper('datafeedmanager')->__('Please define correct path'));
        }
        if (!$file->fileExists($x3c, false)) {
            Mage::throwException(Mage::helper('datafeedmanager')->__('Please create the specified folder "%s" before saving the data feed configuration.', Mage::helper('core')->htmlEscape($this->getFeedPath())));
        }
        if (!$file->isWriteable($x3c)) {
            Mage::throwException(Mage::helper('datafeedmanager')->__('Please make sure that "%s" is writable by web-server.', $this->getFeedPath()));
        }
        if (!preg_match('#^[a-zA-Z0-9_\.]+$#', $this->getFeedName())) {
            Mage::throwException(Mage::helper('datafeedmanager')->__('Please use only letters (a-z or A-Z), numbers (0-9) or underscore (_) in the filename. No spaces or other characters are allowed.'));
        }
        $this->setFeedPath(rtrim(str_replace(str_replace('\\', ' / ', Mage::getBaseDir()), '', $x3c), '/') . '/');
        return parent::_beforeSave();
    }

    protected function getPath()
    {
        if (is_null($this->_filePath)) {
            $this->_filePath = str_replace('//', '/', Mage::getBaseDir() . $this->getFeedPath());
        }
        return $this->_filePath;
    }

    protected function getFilename()
    {
        $x3d = array(1 => 'xml', 2 => 'txt', 3 => 'csv');
        return $this->getFeedName() . "." . $x3d[$this->getFeedType()];
    }

    public function getPreparedFilename()
    {
        return $this->getPath() . $this->getFilename();
    }

    public function x117($x3e, $x3f)
    {
        $x40 = $this->_currencies;
        if (isset($x40[$x3f])) {
            return $x3e * $x40[$x3f];
        } else {
            return $x3e;
        }
    }

    public function x118($x3e, $x41, $x42, $x43 = false)
    {
        $taxClasses = $this->_rates;
        if ($x43 === false) {
            if (!$x41 && isset($taxClasses[$x42])) {
                if (count($taxClasses[$x42]) > 1) {
                    return $x3e;
                } else {
                    return $x3e * ($taxClasses[$x42][0]['rate'] / 100 + 1);
                }
            } else {
                return $x3e;
            }
        } elseif ($x43 === "0") {
            if ($x41 && isset($taxClasses[$x42])) {
                if (count($taxClasses[$x42]) > 1) {
                    return $x3e;
                } else {
                    return 100 * $x3e / (100 + ($taxClasses[$x42][0]['rate']));
                }
            } else {
                return $x3e;
            }
        } else {
            if (is_numeric($x43)) {
                if ($x42 != 0) {
                    return $x3e * ($x43 / 100 + 1);
                } elseif ($x42 == 0) {
                    return $x3e;
                }
            } else {
                $x43 = explode('/', $x43);
                $x45 = 0;
                $x46 = false;
                if (substr($x43[0], 0, 1) == "-") {
                    $x43[0] = substr($x43[0], 1);
                    $x46 = true;
                }
                if ($taxClasses[$x42]) {
                    foreach ($taxClasses[$x42] as $x47) {
                        if ($x47['country'] == $x43[0]) {
                            if (!isset($x43[1]) || $x47['code'] == $x43[1]) {
                                $x45 = $x47['rate'];
                                break;
                            }
                        }
                    }
                    if (!$x46) return $x3e * ($x45 / 100 + 1);
                    else {
                        return 100 * $x3e / (100 + ($x45));
                    }
                } else {
                    return $x3e;
                }
            }
        }
    }

    public function x119($x48, $x49 = true)
    {
        $x37 = '/(<[^>^\/]+>)([^<]*)(<\/[^>]+>)/s';
        preg_match_all($x37, $x48, $matches);
        foreach ($matches[1] as $x39 => $value) {
            $x4a = trim($matches[2][$x39]);
            if (empty($x4a) && !is_numeric($x4a)) $x48 = str_replace($matches[0][$x39], '', $x48);
            else {
                if ($x49) $x48 = str_replace($matches[0][$x39], ($matches[1][$x39]) . '<![CDATA[' . $x4a . ']]>' . ($matches[3][$x39]), $x48);
                else {
                    $x48 = str_replace($matches[0][$x39], ($matches[1][$x39]) . $x4a . ($matches[3][$x39]), $x48);
                }
            }
        }
        $x4b = preg_split("\x2f\x0a/\163", $x48);
        $x4c = '';
        foreach ($x4b as $x4d) {
            (strlen(trim($x4d)) > 0) ? $x4c .= $x4d . "\n" : false;
        }
        $x48 = $x4c;
        return $x48;
    }

    public function x11a($x48)
    {
        if ($this->_display) return ($x48);
        else {
            if ($this->_chartset == 'ISO') return utf8_decode($x48);
            else {
                return ($x48);
            }
        }
    }

    public function x11b($x37, $x34 = false)
    {
        $x37  = preg_replace('/(\r\n|\n|\r|\r\n)/s', '', $x37);
        $x4e = 'padding:2px;  border:1px solid grey;text-align:center; padding:5px;  min-width:10px; min-height:10px; ';
        $x4f = json_decode($x37);
        if (isset($x4f->header)) $x4f = $x4f->header;
        else {
            $x4f = $x4f->product;
        }
        if ($x34) $x50 = "<tr style='background-color:grey; color:white; font-weight:bold'>";
        else {
            $x50 = "<tr>";
        }
        foreach ($x4f as $x39 => $value) {
            ($value != null) ? $x51 = $value : $x51 = "<span style='font-size:px;color:grey'>(empty)</span>";
            $x50 .= "<td style='" . $x4e . "'>" . $x51 . "</td>";
        }
        $x50 .= "</tr>";
        return $x50;
    }

    public function x11c($x37, $feedSeparator, $feedProtector)
    {
        $x37  = preg_replace('/(\r\n|\n|\r|\r\n)/s', '', $x37);
        $x4f = json_decode($x37);
        if (isset($x4f->header)) $x4f = $x4f->header;
        else {
            if (!json_decode($x37)) return "";
            $x4f = $x4f->product;
        }
        $x4d = '';
        $x54 = 0;
        foreach ($x4f as $x39 => $value) {
            if ($feedSeparator == '\t') $feedSeparator = "\t";
            if ($x54 > 0) $x4d .= $feedSeparator;
            if ($feedProtector != "") $x4d .= $feedProtector . $this->x11d($value, $feedProtector) . $feedProtector;
            else {
                $x4d .= $this->x11d($value, $feedSeparator);
            }
            $x54++;
        }
        if ($feedSeparator == "[|]") $x4d .= "[:]";
        return $x4d;
    }

    public function x11d($x37, $x55 = '"')
    {
        $x37 = str_replace($x55, '\\' . $x55, $x37);
        return $x37;
    }

    public function unx11d($x37, $x55 = '"')
    {
        $x37 = str_replace('\\' . $x55, $x55, $x37);
        return $x37;
    }

    public function x11f($feedHeader)
    {
        if (!stristr($feedHeader, 'encoding="utf - 8"') === FALSE)
            $this->_chartset = 'UTF8';
        if (!stristr($feedHeader, 'encoding="ISO-8859-1"') === FALSE)
            $this->_chartset = 'ISO';
    }

    public function checkReference($x57, $product)
    {
        if (($x57 == "parent" || $x57 == "configurable") && isset($this->configurable[$product->getId()]))
            return $this->configurable[$product->getId()];
        elseif (($x57 == "parent" || $x57 == "grouped") && isset($this->grouped[$product->getId()]))
            return $this->grouped[$product->getId()];
        elseif (($x57 == "parent" || $x57 == "bundle") && isset($this->bundle[$product->getId()]))
            return $this->bundle[$product->getId()];
        else {
            return $product;
        }
    }

    public function skipOptions($x58)
    {
        $this->option = $this->option + $x58;
    }

    public function generateFile()
    {
        is_string(Mage::app()->getRequest()->getParam("store_id")) ? $storeId = Mage::app()->getRequest()->getParam("store_id") : $storeId = $this->getStoreId();
        $x5f = Mage::getStoreConfig("catalog/placeholder/image_placeholder", $storeId);
        $storeBaseCurrency = Mage::getStoreConfig("currency/options/base", $storeId);
        $x61 = Mage::getStoreConfig("cataloginventory/item_options/manage_stock", $storeId);
        $storeBaseWebUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB, false);
        $storeBaseUrl = Mage::getModel('core/store')->load($storeId)->getBaseUrl();
        $storeMediaUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, false);
        $x65 = Mage::getStoreConfig(Mage_Tax_Model_Config::CONFIG_XML_PATH_PRICE_INCLUDES_TAX, $storeId);
        $rootId = Mage::app()->getStore($storeId)->getRootCategoryId();
        is_string(Mage::app()->getRequest()->getParam("feed_product")) ? $feedProduct = Mage::app()->getRequest()->getParam("feed_product") : $feedProduct = $this->getFeed_product();
        is_string(Mage::app()->getRequest()->getParam("feed_header")) ? $feedHeader = Mage::app()->getRequest()->getParam("feed_header") : $feedHeader = $this->getFeed_header();
        is_string(Mage::app()->getRequest()->getParam("feed_footer")) ? $feedFooter = Mage::app()->getRequest()->getParam("feed_footer") : $feedFooter = $this->getFeed_footer();
        is_string(Mage::app()->getRequest()->getParam("feed_type")) ? $feedType = Mage::app()->getRequest()->getParam("feed_type") : $feedType = $this->getFeed_type();
        is_string(Mage::app()->getRequest()->getParam("feed_extraheader")) ? $feedExtraHeader = Mage::app()->getRequest()->getParam("feed_extraheader") : $feedExtraHeader = $this->getFeed_extraheader();
        is_string(Mage::app()->getRequest()->getParam("feed_include_header")) ? $feedIncludeHeader = Mage::app()->getRequest()->getParam("feed_include_header") : $feedIncludeHeader = $this->getFeed_include_header();
        is_string(Mage::app()->getRequest()->getParam("feed_separator")) ? $feedSeparator = Mage::app()->getRequest()->getParam("feed_separator") : $feedSeparator = $this->getFeed_separator();
        is_string(Mage::app()->getRequest()->getParam("feed_protector")) ? $feedProtector = Mage::app()->getRequest()->getParam("feed_protector") : $feedProtector = $this->getFeed_protector();
        is_string(Mage::app()->getRequest()->getParam("feed_satus")) ? $feedStatus = Mage::app()->getRequest()->getParam("feed_satus") : $feedStatus = $this->getFeed_status();
        is_string(Mage::app()->getRequest()->getParam("feed_enclose_data")) ? $feedEncloseData = Mage::app()->getRequest()->getParam("feed_enclose_data") : $feedEncloseData = $this->getFeed_enclose_data();
        is_string(Mage::app()->getRequest()->getParam("datafeedmanager_categories")) ? $dfmCategories = json_decode(Mage::app()->getRequest()->getParam("datafeedmanager_categories")) : $dfmCategories = json_decode($this->getDatafeedmanagerCategories());
        is_string(Mage::app()->getRequest()->getParam("datafeedmanager_category_filter")) ? $dfmCategoryFilter = Mage::app()->getRequest()->getParam("datafeedmanager_category_filter") : $dfmCategoryFilter = $this->getDatafeedmanagerCategoryFilter();
        is_string(Mage::app()->getRequest()->getParam("datafeedmanager_categories")) ? $categories = Mage::app()->getRequest()->getParam("datafeedmanager_categories") : $categories = $this->getDatafeedmanagerCategories();
        $selectedFilterCategoryPaths = Array();
        $categoryMappings = Array();
        if ($categories != '*' && is_array($dfmCategories)) {
            foreach ($dfmCategories as $_categoryDataObject) {
                if ($_categoryDataObject->checked) {
                    $selectedFilterCategoryPaths[] = $_categoryDataObject->line;
                }
                if ($_categoryDataObject->mapping != "") {
                    $categoryMappings[$_categoryDataObject->line] = $_categoryDataObject->mapping;
                }
            }
        }
        if (count($selectedFilterCategoryPaths) < 1) {
            $selectedFilterCategoryPaths[] = '*';
        }
        is_string(Mage::app()->getRequest()->getParam("datafeedmanager_type_ids")) ? $dfmTypeIds = explode(',', Mage::app()->getRequest()->getParam("datafeedmanager_type_ids")) : $dfmTypeIds = explode(',', $this->getDatafeedmanagerTypeIds());
        is_string(Mage::app()->getRequest()->getParam("datafeedmanager_visibility")) ? $dfmVisibility = explode(',', Mage::app()->getRequest()->getParam("datafeedmanager_visibility")) : $dfmVisibility = explode(',', $this->getDatafeedmanagerVisibility());
        is_string(Mage::app()->getRequest()->getParam("datafeedmanager_attributes")) ? $dfmAttributes = json_decode(Mage::app()->getRequest()->getParam("datafeedmanager_attributes")) : $dfmAttributes = json_decode($this->getDatafeedmanagerAttributes());
        if (!$feedStatus && !$this->_display)
            Mage::throwException(Mage::helper("datafeedmanager")->__("Thedatafeedconfigurationmustbeenabledinordertogenerateafile . "));
        $file = new Varien_Io_File();
        $file->setAllowCreateFolders(true);
        if (!$this->_display) {
            $file->open(array(
                'path' => $this->getPath()
            ));
            if ($file->fileExists($this->getFilename()) && !$file->isWriteable($this->getFilename())) {
                Mage::throwException(Mage::helper('datafeedmanager')->__('File " % s" cannot be saved. Please, make sure the directory " % s" is writeable by web server.', $this->getFilename(), $this->getPath()));
            }
            $file->streamOpen($this->getFilename());
        }

        $xml = '';
        header("Content-Type:text/html;charset=utf-8");
        $this->x11f($feedHeader);
        $feedHeader = $this->cleanXml($feedHeader, $feedType, true);
        if ($feedType == 1 || ($feedType != 1 && !$this->_display))
            $x48 = $this->x11a($feedHeader);
        if ($this->_display) {
            if ($feedType == 1) {
                $xml = $this->x119($feedHeader, $feedEncloseData) . "";
            } else {
                $xml = $feedExtraHeader . '<br>';
                $xml .= "<table style='border:2px solid grey;font-family:arial;font-size:12px' cellspacing=0 cellpadding=0 width='100%'>";
                if ($feedIncludeHeader) {
                    $xml .= $this->x11b($feedHeader, true);
                }
            }
        } else {
            if ($feedType == 1) {
                $file->streamWrite($this->x119($feedHeader, $feedEncloseData) . "");
            } else {
                if ($feedExtraHeader != '')
                    $file->streamWrite($feedExtraHeader . "\x0d\x0a");
                if ($feedIncludeHeader) {
                    $file->streamWrite($this->x11c($feedHeader, $feedSeparator, $feedProtector) . "\x0d\x0a");
                }
            }
        }
        $x37 = '/{([a-zA-Z_0-9:]+)(\sparent|\sgrouped|\sconfigurable|\sbundle)?([^}]*)}/';
        preg_match_all($x37, $feedProduct, $matches);
        $matches[0][] = "{categories, [1], [1], [1]}";
        $matches[1][] = "categories";
        $matches[2][] = "";
        $matches[3][] = ", [1], [1], [1]";
        $x79 = array();
        $x7a = array();
        foreach ($matches[1] as $x39 => $x7b) {
            $x79[$x39]['methodName'] = "get" . str_replace(' ', '', ucwords(trim($x7b)) . '()');
            $x79[$x39]['pattern'] = "{".trim($x7b)."}";
            $x79[$x39]['fullpattern'] = $matches[0][$x39];
            $x79[$x39]['name'] = trim($x7b);
            $x79[$x39]['reference'] = trim($matches[2][$x39]);
            if (empty($x79[$x39]['reference']))
                $x79[$x39]['reference'] = 'self';
            switch ($x79[$x39]['name']) {
                case 'url':
                    array_push($x7a, 'url_key');
                    break;
                case 'uri':
                    array_push($x7a, 'url_key');
                    break;
                case 'G:IMAGE_LINK':
                    array_push($x7a, 'image');
                    array_push($x7a, 'small_image');
                    array_push($x7a, 'thumbnail');
                    break;
                case 'SC:IMAGES':
                    array_push($x7a, 'image');
                    array_push($x7a, 'small_image');
                    array_push($x7a, 'thumbnail');
                    break;
                case 'SC:DESCRIPTION':
                    array_push($x7a, 'description');
                    array_push($x7a, 'short_description');
                    array_push($x7a, 'manufacturer');
                    array_push($x7a, 'name');
                    array_push($x7a, 'sku');
                    break;
                case 'SC:EAN':
                    array_push($x7a, 'ean');
                    break;
                case 'SC:URL':
                    array_push($x7a, 'url_key');
                    array_push($x7a, 'url');
                    break;
                case 'sc:images':
                    array_push($x7a, 'image');
                    array_push($x7a, 'small_image');
                    array_push($x7a, 'thumbnail');
                    break;
                case 'sc:description':
                    array_push($x7a, 'description');
                    array_push($x7a, 'short_description');
                    array_push($x7a, 'manufacturer');
                    array_push($x7a, 'name');
                    array_push($x7a, 'sku');
                    break;
                case 'sc:ean':
                    array_push($x7a, 'ean');
                    break;
                case 'sc:url':
                    array_push($x7a, 'url_key');
                    array_push($x7a, 'url');
                    break;
                default:
                    array_push($x7a, $x79[$x39]['name']);
            }
            $x79[$x39]["value"] = '$product->get' . $x79[$x39]['name'] . "() ";
            $x79[$x39]["getText"] = 'getAttributeText(\'' . trim($x7b) . '\')';
            $x7c = '/\[([^\]]+)\]/';
            preg_match_all($x7c, $matches[3][$x39], $x7d);
            $x79[$x39]["options"] = $x7d[1];
        }
        $x7e = Mage::getModel('catalog/category')->getCollection()->setStoreId($storeId)->addAttributeToSelect('name')->addAttributeToSelect('is_active')->addAttributeToSelect('include_in_menu');
        $categories = array();
        foreach ($x7e as $_category) {
            $categories[$_category->getId()]['name'] = $_category->getName();
            $categories[$_category->getId()]['path'] = $_category->getPath();
            $categories[$_category->getId()]['level'] = $_category->getLevel();
            if ((Mage::getVersion() < 1.6))
                $categories[$_category->getId()]['include_in_menu'] = true;
            else
                $categories[$_category->getId()]['include_in_menu'] = $_category->getIncludeInMenu();
        }
        $x80 = Mage::getSingleton('core/resource');
        $x81 = $x80->getConnection('core_read');
        $x82 = $x80->getTableName('eav_entity_type');
        $x83 = $x81->select()->from($x82)->where('entity_type_code=\'catalog_product\'');
        $x4f = $x81->fetchAll($x83);
        $x84 = $x4f[0]['entity_type_id'];
        $x80 = Mage::getSingleton('core/resource');
        $x81 = $x80->getConnection('core_read');
        $x85 = $x80->getTableName('directory_currency_rate');
        $x83 = $x81->select()->from($x85)->where('currency_from=\'' . $storeBaseCurrency . '\'');
        $x40 = $x81->fetchAll($x83);
        $currencies = array();
        foreach ($x40 as $x3f) {
            $currencies[$x3f['currency_to']] = $x3f['rate'];
        }
        $this->_currencies = $currencies;
        $x87 = Mage::getResourceModel('eav/entity_attribute_collection')->setEntityTypeFilter($x84)->addSetInfo()->getData();
        $attributes = array();
        $x89 = array();
        foreach ($x87 as $x39 => $x7b) {
            if (in_array($x7b['attribute_code'], $x7a)) {
                array_push($attributes, $x7b['attribute_code']);
                $x89[$x7b['attribute_code']] = $x7b['frontend_input'];
            }
        }
        if (!in_array('special_price', $attributes))
            $attributes[] = 'special_price';
        if (!in_array('special_from_date', $attributes))
            $attributes[] = 'special_from_date';
        if (!in_array('special_to_date', $attributes))
            $attributes[] = 'special_to_date';
        if (!in_array('price_type', $attributes))
            $attributes[] = 'price_type';
        if (!in_array('price', $attributes))
            $attributes[] = 'price';
        $attributes[] = 'tax_class_id';
        foreach ($dfmAttributes as $x8a) {
            if (!in_array($x8a->code, $attributes) && $x8a->checked)
                $attributes[] = $x8a->code;
        }
        $x80 = Mage::getSingleton('core/resource');
        $x81 = $x80->getConnection('core_read');
        $x8b = $x80->getTableName('eav_attribute_option_value');
        $x83 = $x81->select();
        $x83->from($x8b);
        $x83->where("store_id = " . $storeId . ' OR store_id=0');
        $x83->order(array(
            'option_id',
            'store_id'
        ));
        $x8c = $x81->fetchAll($x83);
        foreach ($x8c as $x8d) {
            $attributesLabel[$x8d['option_id']][$x8d['store_id']] = $x8d['value'];
        }
        $x8f = $x80->getTableName('tax_class');
        $x90 = $x80->getTableName('tax_calculation');
        $x91 = $x80->getTableName('tax_calculation_rate');
        $x92 = $x80->getTableName('directory_country_region');
        $x83 = $x81->select();
        $x83->from($x8f)->order(array(
            'class_id',
            'tax_calculation_rate_id'
        ));
        $x83->joinleft(array(
            'tc' => $x90
        ), 'tc.product_tax_class_id = ' . $x8f . '.class_id', 'tc.tax_calculation_rate_id');
        $x83->joinleft(array(
            'tcr' => $x91
        ), 'tcr.tax_calculation_rate_id = tc.tax_calculation_rate_id', array(
            'tcr.rate',
            'tax_country_id',
            'tax_region_id'
        ));
        $x83->joinleft(array(
            'dcr' => $x92
        ), 'dcr.region_id=tcr.tax_region_id', 'code');
        $x93 = $x81->fetchAll($x83);
        $taxClasses = array();
        $x94 = '';
        foreach ($x93 as $x95) {
            if ($x94 != $x95['class_id'])
                $x96 = 0;
            else {
                $x96++;
            }
            $x94 = $x95['class_id'];
            $taxClasses[$x95['class_id']][$x96]['rate'] = $x95['rate'];
            $taxClasses[$x95['class_id']][$x96]['code'] = $x95['code'];
            $taxClasses[$x95['class_id']][$x96]['country'] = $x95['tax_country_id'];
        }
        $this->_rates = $taxClasses;
        $x97 = $x80->getTableName('review');
        $x98 = $x80->getTableName('review_store');
        $x99 = $x80->getTableName('rating_option_vote');
        $x9a = $x81->select()->distinct('review_id');
        $x9a->from(array("r" => $x97), array("COUNT(DISTINCT r.review_id) AS count", 'entity_pk_value'));
        $x9a->joinleft(array(
            'rs' => $x98
        ), 'rs.review_id=r.review_id', 'rs.store_id');
        $x9a->joinleft(array(
            'rov' => $x99
        ), 'rov.review_id=r.review_id', 'AVG(rov.percent) AS score');
        $x9a->where("status_id = 1 and entity_id = 1");
        $x9a->group(array(
            'r.entity_pk_value',
            'rs.store_id'
        ));
        $x9b = $x81->select();
        $x9b->from(array(
            "r" => $x97
        ), array(
            "COUNT(DISTINCT r.review_id) AS count",
            'entity_pk_value',
            "(SELECT 0) AS store_id"
        ));
        $x9b->joinleft(array(
            'rs' => $x98
        ), 'rs.review_id=r.review_id', array());
        $x9b->joinleft(array(
            'rov' => $x99
        ), 'rov.review_id=r.review_id', 'AVG(rov.percent) AS score');
        $x9b->where("status_id = 1 and entity_id = 1");
        $x9b->group(array(
            'r.entity_pk_value'
        ));
        $x83 = $x81->select()->union(array(
            $x9a,
            $x9b
        ));
        $x83->order(array(
            'entity_pk_value',
            'store_id'
        ));
        $x9c = $x81->fetchAll($x83);
        $x9d = array();
        foreach ($x9c as $x9e) {
            $x9d[$x9e['entity_pk_value']][$x9e['store_id']]["count"] = $x9e["count"];
            $x9d[$x9e['entity_pk_value']][$x9e['store_id']]['score'] = $x9e['score'];
        }
        $x80 = Mage::getSingleton('core/resource');
        $x81 = $x80->getConnection('core_read');
        $x9f = $x80->getTableName('catalog_product_entity_media_gallery');
        $xa0 = $x80->getTableName('catalog_product_entity_media_gallery_value');
        $x83 = $x81->select();
        $x83->from($x9f);
        $x83->joinleft(array(
            'cpemgv' => $xa0
        ), 'cpemgv.value_id = ' . $x9f . '.value_id', array(
            'cpemgv.position',
            'cpemgv.disabled'
        ));
        $x83->where("value <> TRIM('') AND (store_id = " . $storeId . ' OR store_id=0)');
        $x83->order(array(
            'position',
            'value_id'
        ));
        $xa1 = $x81->fetchAll($x83);
        foreach ($xa1 as $xa2) {
            $images[$xa2['entity_id']]['src'][] = $xa2['value'];
            $images[$xa2['entity_id']]['disabled'][] = $xa2['disabled'];
        }
        $xa4 = $x80->getTableName("cataloginventory_stock_item");
        $xa5 = $x80->getTableName("core_url_rewrite");
        $x85 = $x80->getTableName('catalog_category_product');
        $xa6 = $x80->getTableName('catalog_category_product_index');
        $xa7 = $x80->getTableName('catalog_product_index_price');
        $xa8 = $x80->getTableName('catalog_product_super_link');
        $xa9 = $x80->getTableName('catalog_product_link');
        $xaa = $x80->getTableName('catalog_product_bundle_selection');
        (Mage::getVersion() < 1.6) ? $x7d = "options = ''" : $x7d = "ISNULL(options) ";
        $select = Mage::getModel('catalog/product')->getCollection()->addStoreFilter($storeId);
        if (Mage::getStoreConfig("datafeedmanager / system / disabled"))
            $select->addFieldToFilter("status", array(
                'gteq' => 1
            ));
        else
            $select->addFieldToFilter("status", 1);
        $select->addAttributeToFilter('type_id', array(
            "in" => "configurable"
        ));
        $select->addAttributeToFilter('visibility', array(
            "nin" => 1
        ));
        $select->addAttributeToSelect($attributes);
        $select->getSelect()->joinLeft($xa8 . ' AS cpsl', 'cpsl.parent_id=e.entity_id ', array(
            'child_ids' => 'GROUP_CONCAT( DISTINCT cpsl.product_id)'
        ));
        $select->getSelect()->joinLeft($xa4 . ' AS stock', 'stock.product_id=e.entity_id', array(
            'qty' => 'qty',
            'is_in_stock' => 'is_in_stock',
            'manage_stock' => 'manage_stock',
            'use_config_manage_stock' => 'use_config_manage_stock'
        ));
        //[dane][avid] request_path is set in the URL model after getting highest category
//        $select->getSelect()->joinLeft($xa5 . ' AS url', 'url.product_id=e.entity_id AND url.category_id IS NULL AND is_system=1 AND ' . $x7d . ' AND url.store_id=' . $storeId, array(
//            'request_path' => 'request_path'
//        ));
        Mage::helper('tradetested_catalog/url')->addRequestPathsToCollection($select, $storeId);

        $select->getSelect()->joinLeft($x85 . ' AS categories', 'categories.product_id=e.entity_id');
        $select->getSelect()->joinLeft($xa6 . ' AS categories_index', 'categories_index.category_id=categories.category_id AND categories_index.product_id=categories.product_id AND categories_index.store_id=' . $storeId, array(
            'categories_ids' => 'GROUP_CONCAT( DISTINCT categories_index.category_id)'
        ));
        $select->getSelect()->group(array(
            'cpsl.parent_id'
        ));
        $xac = array();
        foreach ($select as $xad) {
            foreach (explode(", ", $xad->getChildIds()) as $xae) {
                $xac[$xae] = $xad;
                $configurables[$xae]['categories_ids'] = $xad->getCategories_ids();
                $configurables[$xae]['parent_id'] = $xad->getId();
                $configurables[$xae]['parent_sku'] = $xad->getSku();
            }
        }
        $this->configurable = $xac;
        $select = Mage::getModel('catalog/product')->getCollection()->addStoreFilter($storeId);
        if (Mage::getStoreConfig("datafeedmanager / system / disabled"))
            $select->addFieldToFilter("status", array(
                'gteq' => 1
            ));
        else
            $select->addFieldToFilter("status", 1);
        $select->addAttributeToFilter('type_id', array(
            "in" => "configurable"
        ));
        $select->addAttributeToFilter('visibility', array(
            "nin" => 1
        ));
        $select->getSelect()->joinLeft($xa8 . ' AS cpsl', 'cpsl.parent_id=e.entity_id ');
        $select->getSelect()->joinLeft($xa4 . ' AS stock', 'stock.product_id=cpsl.product_id', array(
            'qty' => 'SUM(qty)'
        ));
        $select->getSelect()->group(array(
            'cpsl.parent_id'
        ));
        $configurablesQuantities = array();
        foreach ($select as $xb1) {
            $configurablesQuantities[$xb1->getId()] = $xb1->getQty();
        }
        $this->configurableQty = $configurablesQuantities;

        $select = Mage::getModel('catalog/product')->getCollection()->addStoreFilter($storeId);
        if (Mage::getStoreConfig("datafeedmanager / system / disabled"))
            $select->addFieldToFilter("status", array(
                'gteq' => 1
            ));
        else
            $select->addFieldToFilter("status", 1);
        $select->addAttributeToFilter('type_id', array(
            "in" => "grouped"
        ));
        $select->addAttributeToFilter('visibility', array(
            "nin" => 1
        ));
        $select->addAttributeToSelect($attributes);
        $select->getSelect()->joinLeft($xa9 . ' AS cpl', 'cpl.product_id=e.entity_id AND cpl.link_type_id=3', array(
            'child_ids' => 'GROUP_CONCAT( DISTINCT cpl.linked_product_id)'
        ));
        $select->getSelect()->joinLeft($xa4 . ' AS stock', 'stock.product_id=e.entity_id', array(
            'qty' => 'qty',
            'is_in_stock' => 'is_in_stock',
            'manage_stock' => 'manage_stock',
            'use_config_manage_stock' => 'use_config_manage_stock'
        ));
        //[dane][avid] request_path is set in the URL model after getting highest category
//        $select->getSelect()->joinLeft($xa5 . ' AS url', 'url.product_id=e.entity_id AND url.category_id IS NULL AND is_system=1 AND ' . $x7d . ' AND url.store_id=' . $storeId, array(
//            'request_path' => 'request_path'
//        ));
        Mage::helper('tradetested_catalog/url')->addRequestPathsToCollection($select, $storeId);

        $select->getSelect()->joinLeft($x85 . ' AS categories', 'categories.product_id=e.entity_id');
        $select->getSelect()->joinLeft($xa6 . ' AS categories_index', 'categories_index.category_id=categories.category_id AND categories_index.product_id=categories.product_id AND categories_index.store_id=' . $storeId, array(
            'categories_ids' => 'GROUP_CONCAT( DISTINCT categories_index.category_id)'
        ));
        $select->getSelect()->group(array(
            'cpl.product_id'
        ));
        $xb2 = array();
        foreach ($select as $xad) {
            foreach (explode(", ", $xad->getChildIds()) as $xae) {
                $xb2[$xae] = $xad;
                $grouped[$xae]['categories_ids'] = $xad->getCategories_ids();
                $grouped[$xae]['parent_id'] = $xad->getId();
                $grouped[$xae]['parent_sku'] = $xad->getSku();
            }
        }
        $this->grouped = $xb2;

        $select = Mage::getModel('catalog/product')->getCollection()->addStoreFilter($storeId);
        if (Mage::getStoreConfig("datafeedmanager / system / disabled"))
            $select->addFieldToFilter("status", array(
                'gteq' => 1
            ));
        else
            $select->addFieldToFilter("status", 1);
        $select->addAttributeToFilter('type_id', array(
            "in" => "bundle"
        ));
        $select->addAttributeToFilter('visibility', array(
            "nin" => 1
        ));
        $select->addAttributeToSelect($attributes);
        $select->getSelect()->joinLeft($xaa . ' AS cpbs', 'cpbs.parent_product_id=e.entity_id', array(
            'child_ids' => 'GROUP_CONCAT( DISTINCT cpbs.product_id)'
        ));
        $select->getSelect()->joinLeft($xa4 . ' AS stock', 'stock.product_id=e.entity_id', array(
            'qty' => 'qty',
            'is_in_stock' => 'is_in_stock',
            'manage_stock' => 'manage_stock',
            'use_config_manage_stock' => 'use_config_manage_stock'
        ));
        //[dane][avid] request_path is set in the URL model after getting highest category
//        $select->getSelect()->joinLeft($xa5 . ' AS url', 'url.product_id=e.entity_id AND url.category_id IS NULL AND is_system=1 AND ' . $x7d . ' AND url.store_id=' . $storeId, array(
//            'request_path' => 'request_path'
//        ));
        Mage::helper('tradetested_catalog/url')->addRequestPathsToCollection($select, $storeId);

        $select->getSelect()->joinLeft($x85 . ' AS categories', 'categories.product_id=e.entity_id');
        $select->getSelect()->joinLeft($xa6 . ' AS categories_index', 'categories_index.category_id=categories.category_id AND categories_index.product_id=categories.product_id AND categories_index.store_id=' . $storeId, array(
            'categories_ids' => 'GROUP_CONCAT( DISTINCT categories_index.category_id)'
        ));
        $select->getSelect()->group(array(
            'e.entity_id'
        ));

        $xb4 = array();
        foreach ($select as $xad) {
            foreach (explode(", ", $xad->getChildIds()) as $xae) {
                $xb4[$xae] = $xad;
                $bundle[$xae]['parent_id'] = $xad->getId();
                $bundle[$xae]['parent_sku'] = $xad->getSku();
                $bundle[$xae]['parent_request_path'] = $xad->getRequestPath();
                $bundle[$xae]['categories_ids'] = $xad->getCategories_ids();
            }
        }
        $this->bundle = $xb4;

        $xb6 = 0;
        $select = Mage::getModel('catalog/product')->getCollection()->addStoreFilter($storeId);
        if (Mage::getStoreConfig("datafeedmanager / system / disabled"))
            $select->addFieldToFilter("status", array(
                'gteq' => 1
            ));
        else
            $select->addFieldToFilter("status", 1);
        $select->addAttributeToFilter('type_id', array(
            "in" => $dfmTypeIds
        ));
        $select->addAttributeToFilter('visibility', array(
            "in" => $dfmVisibility
        ));
        $select->getSelect()->columns("COUNT(DISTINCT e . entity_id) As total")->group(array(
            'status'
        ));
        $xb7 = $select->getFirstItem()->getTotal();
        $xb8 = round($xb7 / $this->_sqlSize) + 1;
        $x77 = '';
        $xb9 = 1;
        while ($xb6 < $xb8) {
            $select = Mage::getModel('catalog/product')->getCollection()->addStoreFilter($storeId);
            if (Mage::getStoreConfig("datafeedmanager / system / disabled"))
                $select->addFieldToFilter("status", array(
                    'gteq' => 1
                ));
            else
                $select->addFieldToFilter("status", 1);
            $select->addAttributeToFilter("type_id", array(
                "in" => $dfmTypeIds
            ));
            $select->addAttributeToFilter("visibility", array(
                "in" => $dfmVisibility
            ));
            $select->addAttributeToSelect($attributes);
            $xba = array(
                "eq" => " = '%s'",
                "neq" => " != '%s'",
                "gteq" => " >= '%s'",
                "lteq" => " <= '%s'",
                "gt" => " > '%s'",
                "lt" => " < '%s'",
                "like" => "like '%s'",
                "nlike" => "not like'%s'",
                "null" => "is null",
                "notnull" => "is not null",
                "in" => "in (%s) ",
                "nin" => "not in (%s) "
            );
            $xbb = '';
            $x4b = 0;
            foreach ($dfmAttributes as $x8a) {
                if ($x8a->checked) {
                    if ($x8a->condition == 'in' || $x8a->condition == 'nin') {
                        if ($x8a->code == 'qty' || $x8a->code == 'is_in_stock') {
                            $xbc = explode(',', $x8a->value);
                            $x8a->value = "'" . implode($xbc, "', '") . "'";
                        } else {
                            $x8a->value = explode(',', $x8a->value);
                        }
                    }
                    switch ($x8a->code) {
                        case 'qty':
                            if ($x4b > 0)
                                $xbb .= ' AND ';
                            $xbb .= "qty" . sprintf($xba[$x8a->condition], $x8a->value);
                            $x4b++;
                            break;
                        case 'is_in_stock':
                            if ($x4b > 0)
                                $xbb .= ' AND ';
                            $xbb .= "(is_in_stock" . sprintf($xba[$x8a->condition], $x8a->value);
                            $xbb .= " OR (manage_stock" . sprintf($xba[$x8a->condition], (int)!$x8a->value);
                            $xbb .= " AND use_config_manage_stock" . sprintf($xba[$x8a->condition], (int)!$x8a->value) . ')';
                            $xbb .= " OR (use_config_manage_stock" . sprintf($xba[$x8a->condition], $x8a->value) . ' AND ' . $x61 . '=' . (int)$x8a->value . ' AND is_in_stock = ' . $x8a->value . ' )';
                            $xbb .= ") ";
                            $x4b++;
                            break;
                        default:
                            //[dane][avid] Attributes are joined by inner join by default. meaning nlike wasn't working for the stock description filter where it was a null value
                            if ($x8a->condition == 'nlike') {
                                $select->addAttributeToFilter(
                                    $x8a->code,
                                    array(
                                        array($x8a->condition => $x8a->value),
                                        array('null' => true),
                                    ),
                                    'left');
                            } else {
                                $select->addAttributeToFilter($x8a->code, array(
                                    $x8a->condition => $x8a->value
                                ));
                            }
                            break;
                    }
                }
            };
            $select->getSelect()->joinLeft($xa4 . ' AS stock', 'stock.product_id=e.entity_id', array(
                'qty' => 'qty',
                'is_in_stock' => 'is_in_stock',
                'manage_stock' => 'manage_stock',
                'use_config_manage_stock' => 'use_config_manage_stock'
            ));
            //[dane][avid] request_path is set in the URL model after getting highest category
//            $select->getSelect()->joinLeft($xa5 . ' AS url', 'url.product_id=e.entity_id AND url.category_id IS NULL AND is_system=1 AND ' . $x7d . ' AND url.store_id=' . $storeId, array(
//                'request_path' => 'request_path'
//            ));
            Mage::helper('tradetested_catalog/url')->addRequestPathsToCollection($select, $storeId);

            $select->getSelect()->joinLeft($x85 . ' AS categories', 'categories.product_id=e.entity_id');
            if ($selectedFilterCategoryPaths[0] != '*') {
                $x51 = 0;
                $selectedCategoryIdsString = null;
                foreach ($selectedFilterCategoryPaths as $_categoryDataObject) {
                    if ($x51 > 0)
                        $selectedCategoryIdsString .= ',';
                    $arr = explode('/', $_categoryDataObject);
                    $selectedCategoryIdsString .= array_pop($arr);
                    $x51++;
                }
                ($dfmCategoryFilter) ? $xbe = "IN" : $xbe = "NOT IN";
                $selectedCategoryIdsString = " AND categories_index.category_id " . $xbe . "(" . $selectedCategoryIdsString . ") ";
                $select->getSelect()->joinInner($xa6 . ' AS categories_index', 'categories_index.category_id=categories.category_id AND categories_index.product_id=categories.product_id AND categories_index.store_id=' . $storeId . ' ' . $selectedCategoryIdsString, array(
                    'categories_ids' => 'GROUP_CONCAT(categories_index.category_id)'
                ));
            } else
                $select->getSelect()->joinLeft($xa6 . ' AS categories_index', 'categories_index.category_id=categories.category_id AND categories_index.product_id=categories.product_id AND categories_index.store_id=' . $storeId, array(
                    'categories_ids' => 'GROUP_CONCAT(categories_index.category_id)'
                ));
            if (Mage::getVersion() >= 1.4)
                $select->getSelect()->joinLeft($xa7 . ' AS price_index', 'price_index.entity_id=e.entity_id AND customer_group_id=0 AND price_index.website_id=' . Mage::getModel('core/store')->load($storeId)->getWebsiteId(), array(
                    'min_price' => 'min_price',
                    'max_price' => 'max_price',
                    'tier_price' => 'tier_price',
                    'final_price' => 'final_price'
                ));
            if (!empty($xbb))
                $select->getSelect()->where($xbb);
            $select->getSelect()->group(array(
                'e.entity_id'
            ));
            $select->getSelect()->group(array(
                'e.entity_id'
            ));
            $select->getSelect()->limit($this->_sqlSize, ($this->_sqlSize * $xb6));

            $xb6++;
            $xbf = 1;
            $customOptionsObject = new MyCustomOptions;
            $customAttributesObject = new MyCustomAttributes;
            foreach ($select as $product) {

                //[dane][avid] set highest category on product, and store id
                $product->setStoreId($storeId);

                if (!ini_get('safe_mode')) {
                    set_time_limit(60);
                }
                $x48 = $feedProduct;
                foreach ($x79 as $x39 => $exp) {
                    $value = "";
                    $this->option = 0;
                    switch ($exp['pattern']) {
                        case '{id}':
                            $product = $this->checkReference($exp['reference'], $product);
                            $value = $product->getId();
                            break;
                        case '{inc}':
                            $value = $xb9;
                            break;
                        case '{final_price}':
                            $product = $this->checkReference($exp['reference'], $product);
                            $x3e = $product->getFinalPrice();
                            (!isset($exp['options'][1])) ? $x43 = false : $x43 = $exp['options'][1];
                            $value = $this->x118($x3e, $x65, $product->getTaxClassId(), $x43);
                            (!isset($exp['options'][0])) ? $x3f = $storeBaseCurrency : $x3f = $exp['options'][0];
                            $value = $this->x117($value, $x3f);
                            $value = number_format($value, 2, '.', '');
                            $this->skipOptions(2);
                            break;
                        case '{tier_price}':
                            $product = $this->checkReference($exp['reference'], $product);
                            $x3e = $product->getTierPrice();
                            (!isset($exp['options'][1])) ? $x43 = false : $x43 = $exp['options'][1];
                            $value = $this->x118($x3e, $x65, $product->getTaxClassId(), $x43);
                            (!isset($exp['options'][0])) ? $x3f = $storeBaseCurrency : $x3f = $exp['options'][0];
                            $value = $this->x117($value, $x3f);
                            $value = number_format($value, 2, '.', '');
                            $this->skipOptions(2);
                            break;
                        case '{min_price}':
                            $product = $this->checkReference($exp['reference'], $product);
                            $x3e = $product->getMinPrice();
                            (!isset($exp['options'][1])) ? $x43 = false : $x43 = $exp['options'][1];
                            $value = $this->x118($x3e, $x65, $product->getTaxClassId(), $x43);
                            (!isset($exp['options'][0])) ? $x3f = $storeBaseCurrency : $x3f = $exp['options'][0];
                            $value = $this->x117($value, $x3f);
                            $value = number_format($value, 2, '.', '');
                            $this->skipOptions(2);
                            break;
                        case '{max_price}':
                            $product = $this->checkReference($exp['reference'], $product);
                            $x3e = $product->getMaxPrice();
                            (!isset($exp['options'][1])) ? $x43 = false : $x43 = $exp['options'][1];
                            $value = $this->x118($x3e, $x65, $product->getTaxClassId(), $x43);
                            (!isset($exp['options'][0])) ? $x3f = $storeBaseCurrency : $x3f = $exp['options'][0];
                            $value = $this->x117($value, $x3f);
                            $value = number_format($value, 2, '.', '');
                            $this->skipOptions(2);
                            break;
                        case '{normal_price}':
                            $product = $this->checkReference($exp['reference'], $product);
                            if ($product->type_id == 'bundle')
                                $x3e = $product->price;
                            else {
                                $x3e = $product->getPrice();
                            }
                            (!isset($exp['options'][1])) ? $x43 = false : $x43 = $exp['options'][1];
                            $value = $this->x118($x3e, $x65, $product->getTaxClassId(), $x43);
                            (!isset($exp['options'][0])) ? $x3f = $storeBaseCurrency : $x3f = $exp['options'][0];
                            $value = $this->x117($value, $x3f);
                            $value = number_format($value, 2, '.', '');
                            $this->skipOptions(2);
                            break;
                        case '{price}':
                            $product = $this->checkReference($exp['reference'], $product);
                            if ($product->getSpecialFromDate() && !$product->getSpecialToDate()) {
                                if ($product->getSpecialFromDate() <= date("Y-m-d H:i:s")) {
                                    if ($product->type_id == "bundle") {
                                        if (($product->price_type || (!$product->price_type && $product->special_price < $product->price)) && $product->special_price > 0) {
                                            if ($product->price_type)
                                                $x3e = number_format($product->price * $product->special_price / 100, 2, ".", "");
                                            else {
                                                $x3e = $product->special_price;
                                            }
                                        } else {
                                            $x3e = $product->price;
                                        }
                                    } else {
                                        ($product->getSpecial_price() && $product->getSpecial_price() < $product->getPrice()) ? $x3e = $product->getSpecialPrice() : $x3e = $product->getPrice();
                                    }
                                } else {
                                    if ($product->type_id == "bundle")
                                        $x3e = $product->price;
                                    else {
                                        $x3e = $product->getPrice();
                                    }
                                }
                            } elseif ($product->getSpecialFromDate() && $product->getSpecialToDate()) {
                                if (($product->getSpecialFromDate() <= date("Y-m-d H:i:s")) && (date("Y-m-d H:i:s") < $product->getSpecialToDate())) {
                                    if ($product->type_id == "bundle") {
                                        if (($product->price_type || (!$product->price_type && $product->special_price < $product->price)) && $product->special_price > 0) {
                                            if ($product->price_type)
                                                $x3e = number_format($product->price * $product->special_price / 100, 2, ".", "");
                                            else {
                                                $x3e = $product->special_price;
                                            }
                                        } else {
                                            $x3e = $product->price;
                                        }
                                    } else {
                                        ($product->getSpecial_price() && $product->getSpecial_price() < $product->getPrice()) ? $x3e = $product->getSpecialPrice() : $x3e = $product->getPrice();
                                    }
                                } else {
                                    if ($product->type_id == "bundle")
                                        $x3e = $product->price;
                                    else {
                                        $x3e = $product->getPrice();
                                    }
                                }
                            } else {
                                if ($product->type_id == "bundle") {
                                    if (($product->price_type || (!$product->price_type && $product->special_price < $product->price)) && $product->special_price > 0) {
                                        if ($product->price_type)
                                            $x3e = number_format($product->price * $product->special_price / 100, 2, ".", "");
                                        else {
                                            $x3e = $product->special_price;
                                        }
                                    } else {
                                        $x3e = $product->price;
                                    }
                                } else {
                                    ($product->getSpecial_price() && $product->getSpecial_price() < $product->getPrice()) ? $x3e = $product->getSpecialPrice() : $x3e = $product->getPrice();
                                }
                            }
                            (!isset($exp['options'][1])) ? $x43 = false : $x43 = $exp['options'][1];
                            $value = $this->x118($x3e, $x65, $product->getTaxClassId(), $x43);
                            (!isset($exp["options"][0])) ? $x3f = $storeBaseCurrency : $x3f = $exp["options"][0];
                            $value = $this->x117($value, $x3f);
                            $value = number_format($value, 2, ".", "");
                            $this->skipOptions(2);
                            break;
                        case "{is_special_price}":
                            $product = $this->checkReference($exp['reference'], $product);
                            $valueIfTrue = (isset($exp["options"][0])) ? $exp["options"][0] : 1;
                            $valueIfFalse = (isset($exp["options"][1])) ? $exp["options"][1] : 0;
                            if ($product->getSpecialFromDate() && !$product->getSpecialToDate()) {
                                if ($product->getSpecialFromDate() <= date("Y-m-d H:i:s")) {
                                    if ($product->type_id == "bundle")
                                        $value = (($product->price_type || (!$product->price_type && $product->special_price < $product->price)) && $product->special_price > 0) ? $valueIfTrue : $valueIfFalse;
                                    else {
                                        $value = ($product->getSpecial_price() && $product->getSpecial_price() < $product->getPrice()) ? $valueIfTrue : $valueIfFalse;
                                    }
                                } else {
                                    $value = $valueIfFalse;
                                }
                            } elseif ($product->getSpecialFromDate() && $product->getSpecialToDate()) {
                                if ($product->getSpecialFromDate() <= date("Y-m-d H:i:s") && date("Y-m-d H:i:s") < $product->getSpecialToDate()) {
                                    if ($product->type_id == "bundle")
                                        $value = (($product->price_type || (!$product->price_type && $product->special_price < $product->price)) && $product->special_price > 0) ? $valueIfTrue : $valueIfFalse;
                                    else {
                                        $value = ($product->getSpecial_price() && $product->getSpecial_price() < $product->getPrice()) ? $valueIfTrue : $valueIfFalse;
                                    }
                                } else {
                                    $value = $valueIfFalse;
                                }
                            } else {
                                if ($product->type_id == "bundle")
                                    $value = (($product->price_type || (!$product->price_type && $product->special_price < $product->price)) && $product->special_price > 0) ? $valueIfTrue : $valueIfFalse;
                                else {
                                    $value = ($product->getSpecial_price() && $product->getSpecial_price() < $product->getPrice()) ? $valueIfTrue : $valueIfFalse;
                                }
                            }
                            $this->skipOptions(2);
                            break;
                        case "{special_price}":
                            $product = $this->checkReference($exp['reference'], $product);
                            $x3e = null;
                            if ($product->getSpecialFromDate() && !$product->getSpecialToDate()) {
                                if ($product->getSpecialFromDate() <= date("Y-m-d H:i:s")) {
                                    if ($product->type_id == 'bundle') {
                                        if ($product->price_type)
                                            $x3e = number_format($product->price * $product->special_price / 100, 2, ".", "");
                                        else {
                                            $x3e = $product->special_price;
                                        }
                                    } else {
                                        $x3e = $product->getSpecial_price();
                                    }
                                }
                            } elseif ($product->getSpecialFromDate() && $product->getSpecialToDate()) {
                                if ($product->getSpecialFromDate() <= date("Y-m-d H:i:s") && date("Y-m-d H:i:s") < $product->getSpecialToDate()) {
                                    if ($product->type_id == 'bundle') {
                                        if ($product->price_type)
                                            $x3e = number_format($product->price * $product->special_price / 100, 2, ".", "");
                                        else {
                                            $x3e = $product->special_price;
                                        }
                                    } else {
                                        $x3e = $product->getSpecial_price();
                                    }
                                }
                            } else {
                                if ($product->type_id == 'bundle') {
                                    if ($product->price_type)
                                        $x3e = number_format($product->price * $product->special_price / 100, 2, ".", "");
                                    else {
                                        $x3e = $product->special_price;
                                    }
                                } else {
                                    $x3e = $product->getSpecial_price();
                                }
                            }
                            (!isset($exp['options'][1])) ? $x43 = false : $x43 = $exp['options'][1];
                            if ($x3e > 0) {
                                $value = $this->x118($x3e, $x65, $product->getTaxClassId(), $x43);
                                (!isset($exp['options'][0])) ? $x3f = $storeBaseCurrency : $x3f = $exp['options'][0];
                                $value = $this->x117($value, $x3f);
                                $value = number_format($value, 2, '.', '');;
                            } else {
                                $value = "";
                            }
                            $this->skipOptions(2);
                            break;
                        case '{price_rules}':
                            $product = $this->checkReference($exp['reference'], $product);
                            $storeId = $this->getStoreId();
                            $xc5 = Mage::getResourceModel('catalogrule/rule');
                            $xc6 = Mage::app()->getLocale()->storeTimeStamp($storeId);
                            $xc7 = Mage::app()->getStore($storeId);
                            $xc8 = $xc7->getWebsiteId();
                            $xc9 = Mage::getSingleton('customer/session')->getCustomerGroupId();
                            $xca = $xc5->getRulePrice($xc6, $xc8, $xc9, $product->getId());
                            if ($xca !== false)
                                $price = sprintf('%.2f', round($xca, 2));
                            else
                                $price = $product->getPrice();
                            if ($product->getSpecialFromDate() && !$product->getSpecialToDate()) {
                                if ($product->getSpecialFromDate() <= date("Y-m-d H:i:s")) {
                                    if ($product->type_id == "bundle") {
                                        if (($product->price_type || (!$product->price_type && $product->special_price < $product->price)) && $product->special_price > 0) {
                                            if ($product->price_type)
                                                $x3e = number_format($product->price * $product->special_price / 100, 2, ".", "");
                                            else {
                                                $x3e = $product->special_price;
                                            }
                                        } else {
                                            $x3e = $product->price;
                                        }
                                    } else {
                                        ($product->getSpecial_price() && $product->getSpecial_price() < $product->getPrice()) ? $x3e = $product->getSpecialPrice() : $x3e = $price;
                                    }
                                } else {
                                    if ($product->type_id == "bundle")
                                        $x3e = $product->price;
                                    else {
                                        $x3e = $price;
                                    }
                                }
                            } elseif ($product->getSpecialFromDate() && $product->getSpecialToDate()) {
                                if ($product->getSpecialFromDate() <= date("Y-m-d H:i:s") && date("Y-m-d H:i:s") < $product->getSpecialToDate()) {
                                    if ($product->type_id == "bundle") {
                                        if (($product->price_type || (!$product->price_type && $product->special_price < $product->price)) && $product->special_price > 0) {
                                            if ($product->price_type)
                                                $x3e = number_format($product->price * $product->special_price / 100, 2, ".", "");
                                            else {
                                                $x3e = $product->special_price;
                                            }
                                        } else {
                                            $x3e = $product->price;
                                        }
                                    } else {
                                        ($product->getSpecial_price() && $product->getSpecial_price() < $product->getPrice()) ? $x3e = $product->getSpecialPrice() : $x3e = $price;
                                    }
                                } else {
                                    if ($product->type_id == "bundle")
                                        $x3e = $product->price;
                                    else {
                                        $x3e = $price;
                                    }
                                }
                            } else {
                                if ($product->type_id == "bundle") {
                                    if (($product->price_type || (!$product->price_type && $product->special_price < $product->price)) && $product->special_price > 0) {
                                        if ($product->price_type)
                                            $x3e = number_format($product->price * $product->special_price / 100, 2, ".", "");
                                        else {
                                            $x3e = $product->special_price;
                                        }
                                    } else {
                                        $x3e = $product->price;
                                    }
                                } else {
                                    ($product->getSpecial_price() && $product->getSpecial_price() < $product->getPrice()) ? $x3e = $product->getSpecialPrice() : $x3e = $price;
                                }
                            }
                            (!isset($exp['options'][1])) ? $x43 = false : $x43 = $exp['options'][1];
                            $value = $this->x118($x3e, $x65, $product->getTaxClassId(), $x43);
                            (!isset($exp["options"][0])) ? $x3f = $storeBaseCurrency : $x3f = $exp["options"][0];
                            $value = $this->x117($value, $x3f);
                            $value = number_format($value, 2, ".", "");
                            $this->skipOptions(2);
                            break;
                        case "{G:SALE_PRICE}":
                            $product = $this->checkReference($exp['reference'], $product);
                            $xcc = str_replace(' ', 'T', $product->getSpecialFromDate());
                            $xcd = str_replace(' ', 'T', $product->getSpecialToDate());
                            if ($product->type_id == 'bundle' && $product->special_price) {
                                if ($product->price_type)
                                    $x3e = number_format($product->price * $product->special_price / 100, 2, ".", "");
                                else {
                                    $x3e = $product->special_price;
                                }
                            } else {
                                $x3e = $product->getSpecial_price();
                            }
                            (!isset($exp['options'][1])) ? $x43 = false : $x43 = $exp['options'][1];
                            if ($x3e > 0) {
                                $x3e = $this->x118($x3e, $x65, $product->getTaxClassId(), $x43);
                                (!isset($exp['options'][0])) ? $x3f = $storeBaseCurrency : $x3f = $exp['options'][0];
                                $x3e = $this->x117($x3e, $x3f);
                                $x3e = number_format($x3e, 2, '.', '');;
                            }
                            if ($x3e > 0)
                                $value = "<g:sale_price><![CDATA[".$x3e."]]></g:sale_price>";
                            if ($x3e > 0 && $xcd)
                                $value .= "\n<g:sale_price_effective_date><![CDATA[".$xcc."/".$xcd."]]></g:sale_price_effective_date>";
                            $this->skipOptions(2);
                            break;
                        case "{image}":
                            $product = $this->checkReference($exp['reference'], $product);
                            $xce = $product->getImage();
                            if (!isset($exp['options'][0]) || $exp['options'][0] == 0) {
                                if ($product->getImage() && $product->getImage() != 'no_selection') {
                                    $xcf = 'catalog/product/' . $product->getImage();
                                    $value = $storeMediaUrl . str_replace('//', '/', $xcf);
                                } else {
                                    $value = $storeMediaUrl . '/catalog/product/placeholder/' . $x5f;
                                }
                            } elseif (isset($images[$product->getId()]['src'][$exp['options'][0] - 1]) && $exp['options'][0] > 0) {
                                if ($images[$product->getId()]['src'][$exp['options'][0] - 1] != $xce) {
                                    $xcf = 'catalog/product/' . $images[$product->getId()]['src'][$exp['options'][0] - 1];
                                    $value = $storeMediaUrl . str_replace('//', '/', $xcf);
                                }
                            }
                            $this->skipOptions(1);
                            break;
                        case "{thumbnail}":
                            $product = $this->checkReference($exp['reference'], $product);
                            $xce = $product->getThumbnail();
                            if (!isset($exp['options'][0]) || $exp['options'][0] == 0) {
                                if ($product->getThumbnail() && $product->getThumbnail() != 'no_selection') {
                                    $xcf = 'catalog/product/' . $product->getThumbnail();
                                    $value = $storeMediaUrl . str_replace('//', '/', $xcf);
                                } else {
                                    $value = $storeMediaUrl . '/catalog/product/placeholder/' . $x5f;
                                }
                            } elseif (isset($images[$product->getId()]['src'][$exp['options'][0] - 1]) && $exp['options'][0] > 0) {
                                if ($images[$product->getId()]['src'][$exp['options'][0] - 1] != $xce) {
                                    $xcf = 'catalog/product/' . $images[$product->getId()]['src'][$exp['options'][0] - 1];
                                    $value = $storeMediaUrl . str_replace('//', '/', $xcf);
                                }
                            }
                            $this->skipOptions(1);
                            break;
                        case "{G:IMAGE_LINK}":
                            $product = $this->checkReference($exp['reference'], $product);
                            $xce = $product->getImage();
                            $xd0 = array(
                                $product->getSmall_image(),
                                $product->getThumbnail()
                            );
                            $xd1 = '';
                            $xd2 = 0;
                            if ($product->getImage() && $product->getImage() != 'no_selection') {
                                $xcf = 'catalog/product/' . $product->getImage();
                                $value = $storeMediaUrl . str_replace('//', '/', $xcf);
                                $xd1 .= "<g:image_link><![CDATA[".$value."]]></g:image_link>";
                                $xd2++;
                            }
                            $xd3 = 0;
                            while (isset($images[$product->getId()]['src'][$xd3]) && $xd2 < 10) {
                                if ($images[$product->getId()]['src'][$xd3] != $xce) {
                                    if (in_array($images[$product->getId()]['src'][$xd3], $xd0) || $images[$product->getId()]['disabled'][$xd3] != 1) {
                                        $xcf = 'catalog/product/' . $images[$product->getId()]['src'][$xd3];
                                        $value = $storeMediaUrl . str_replace('//', '/', $xcf);
                                        $xd1 .= "\n<g:additional_image_link><![CDATA[".$value."]]></g:additional_image_link>";
                                        $xd2++;
                                    }
                                }
                                $xd3++;
                            }
                            $value = $xd1;
                            break;
                        case "{url}":
                            //[dane][avid] use URL with highest category
                            $appEmulation = Mage::getSingleton('core/app_emulation');
                            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
                            $product = $this->checkReference($exp['reference'], $product);
                            $value = $product->getProductUrl();
                            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
                            break;
                        case "{host}":
                            $value = $storeBaseUrl;
                            break;
                        case " {uri}":
                            (isset($exp['options'][0])) ? $xd4 = $exp['options'][0] : $xd4 = "";
                            (isset($exp['options'][1])) ? $xd5 = $exp['options'][1] : $xd5 = "";
                            $product = $this->checkReference($exp['reference'], $product);
                            if ($product->getRequest_path()) {
                                $value = $xd5 . '' . $product->getRequest_path() . $xd4;
                            } else {
                                $value = str_replace($storeBaseUrl, '', $product->getProductUrl());
                            }
                            break;
                        case '{is_in_stock}':
                            $product = $this->checkReference($exp['reference'], $product);
                            (!isset($exp['options'][0])) ? $valueIfTrue = 1 : $valueIfTrue = $exp['options'][0];
                            (!isset($exp['options'][1])) ? $valueIfFalse = 0 : $valueIfFalse = $exp['options'][1];
                            if ($product->getManageStock() || ($product->getUseConfigManageStock() && $x61)) {
                                ($product->getIs_in_stock() > 0) ? $value = $valueIfTrue : $value = $valueIfFalse;
                            } else {
                                $value = $valueIfTrue;
                            }
                            $this->skipOptions(2);
                            break;
                        case '{stock_status}':
                            $product = $this->checkReference($exp['reference'], $product);
                            ($product->getIs_in_stock() > 0) ? $value = Mage::helper('datafeedmanager')->__('in stock') : $value = Mage::helper('datafeedmanager')->__('out of stock');
                            break;
                        case '{qty}':
                            $product = $this->checkReference($exp['reference'], $product);
                            (!isset($exp['options'][0])) ? $xd6 = 0 : $xd6 = $exp['options'][0];
                            if ($product->type_id == "configurable") {
                                $value = $configurablesQuantities[$product->getId()];
                                $value = number_format($value, $xd6, '.', '');
                            } else if ($exp['reference'] == "configurable") {
                                $value = number_format($configurablesQuantities[$product->getId()], $xd6, '.', '');
                            } else {
                                $value = number_format($product->getQty(), $xd6, '.', '');
                            }
                            $this->skipOptions(1);
                            break;
                        case "{categories}":
                            $product = $this->checkReference($exp['reference'], $product);
                            (!isset($exp['options'][0]) || !$exp['options'][0] || $exp['options'][0] == 'INF') ? $limit = INF : $limit = $exp['options'][0];
                            (!isset($exp['options'][1])) ? $fromCategoryLevel = 1 : $fromCategoryLevel = $exp['options'][1];
                            (!isset($exp['options'][2]) || !$exp['options'][2] || $exp['options'][2] == 'INF') ? $maxPerPath = INF : $maxPerPath = $exp['options'][2];
                            $xda = 0;
                            $value = '';
                            $xdb = '';
                            foreach (explode(',', $product->getCategoriesIds()) as $x39 => $_category) {
                                ($dfmCategoryFilter) ? $xdc = in_array($categories[$_category]["path"], $selectedFilterCategoryPaths) : $xdc = !in_array($categories[$_category]["path"], $selectedFilterCategoryPaths);
                                if (isset($categories[$_category]) && $xda < $limit && ($xdc || $selectedFilterCategoryPaths[0] == "*")) {
                                    $x = 0;
                                    $pathParts = explode('/', $categories[$_category]["path"]);
                                    if (in_array($rootId, $pathParts)) {
                                        $xdf = "";
                                        if ($xda > 0)
                                            $xdb = ",";
                                        foreach ($pathParts as $_categoryId) {
                                            if (isset($categories[$_categoryId])) {
                                                if ($categories[$_categoryId]['level'] > $fromCategoryLevel && $x < $maxPerPath) {
                                                    if ($x > 0)
                                                        $xdf .= ' > ';
                                                    $xdf .= ($categories[$_categoryId]['name']);
                                                    $x++;
                                                }
                                            }
                                        }
                                        $xe1 = "";
                                        if (!empty($xdf)) {
                                            $value .= $xdb . $xdf . $xe1;
                                            $xda++;
                                        }
                                    }
                                }
                            };
                            $this->skipOptions(3);
                            break;
                        case "{G:PRODUCT_TYPE}":
                            //[dane][avid] customised to remove parent categories of another.
                            $product = $this->checkReference($exp['reference'], $product);

                            $limit = (!isset($exp['options'][0]) || !$exp['options'][0] || $exp['options'][0] == 'INF') ? 0 : $exp['options'][0];
                            $fromCategoryLevel = (!isset($exp['options'][1])) ? 1 : $exp['options'][1];
                            $maxPerPath = (!isset($exp['options'][2]) || !$exp['options'][2] || $exp['options'][2] == 'INF') ? 0 : $exp['options'][2];

                            $outputCategories = array();
                            foreach (explode(',', $product->getCategoriesIds()) as $_category) {
                                $includedByCategoryFilter = ($dfmCategoryFilter) ? in_array($categories[$_category]["path"], $selectedFilterCategoryPaths) : !in_array($categories[$_category]["path"], $selectedFilterCategoryPaths);
                                if (
                                    isset($categories[$_category])
                                    && $categories[$_category]["include_in_menu"]
                                    && ($includedByCategoryFilter || $selectedFilterCategoryPaths[0] == "*")
                                ) {
                                    $pathParts = explode('/', $categories[$_category]["path"]);
                                    if (in_array($rootId, $pathParts)) {
                                        $outputCategories[$categories[$_category]["path"]] = $pathParts;
                                    }
                                }
                            };
                            foreach ($outputCategories as $_path => $_parts) {
                                foreach ($outputCategories as $_removalCandidatePath => $_removalParts) {
                                    if (
                                        (strpos($_path, $_removalCandidatePath) === 0)
                                        && ($_path != $_removalCandidatePath)
                                    ) {
                                        unset($outputCategories[$_removalCandidatePath]);
                                    }
                                }
                            }
                            foreach ($outputCategories as $_path => $_pathParts) {
                                $pathNames = array();
                                foreach ($_pathParts as $_categoryId) {
                                    if (
                                        isset($categories[$_categoryId])
                                        && ($categories[$_categoryId]['level'] > $fromCategoryLevel)
                                    ) {
                                        $pathNames[] = ($categories[$_categoryId]['name']);
                                    }
                                }
                                $pathNames = $maxPerPath ? array_slice($pathNames, 0, $maxPerPath) : $pathNames;
                                $outputCategories[$_path] = "<g:product_type><![CDATA[".implode(' > ', $pathNames)."]]></g:product_type>";
                            }
                            $outputCategories = $limit ? array_slice($outputCategories, 0, $limit) : $outputCategories;
                            $value = implode("\n", $outputCategories)."\n";
                            $this->skipOptions(3);
                            break;
                        case "{G:GOOGLE_PRODUCT_CATEGORY}":
                            (isset($exp["options"][0])) ? $xe2 = $exp["options"][0] : $xe2 = 0;
                            $value = "";
                            $xe3 = 0;
                            $product = $this->checkReference($exp['reference'], $product);
                            foreach (explode(',', $product->getCategoriesIds()) as $x39 => $_category) {
                                if (isset($categories[$_category]["path"]) && isset($categoryMappings[$categories[$_category]["path"]])) {
                                    if ($xe3 == $xe2) {
                                        $value .= "<g:google_product_category><![CDATA[".$categoryMappings[$categories[$_category]["path"]]."]]></g:google_product_category>";
                                        break;
                                    }
                                    $xe3++;
                                }
                            }
                            $this->skipOptions(1);
                            break;
                        case "{category_mapping}":
                            (isset($exp["options"][0])) ? $xe2 = $exp["options"][0] : $xe2 = 0;
                            $value = "";
                            $product = $this->checkReference($exp['reference'], $product);
                            $xe3 = 0;
                            foreach (explode(',', $product->getCategoriesIds()) as $x39 => $_category) {
                                if (isset($categoryMappings[$categories[$_category]["path"]])) {
                                    if ($xe3 == $xe2) {
                                        $value .= $categoryMappings[$categories[$_category]["path"]];
                                        break;
                                    }
                                    $xe3++;
                                }
                            }
                            $this->skipOptions(1);
                            break;
                        case "{review_count}":
                            $product = $this->checkReference($exp['reference'], $product);
                            $value = "";
                            (isset($exp["options"][0]) && $exp["options"][0] == "*") ? $xe4 = 0 : $xe4 = $storeId;
                            if (isset($x9d[$product->getId()][$xe4]["count"])) {
                                $xe5 = $x9d[$product->getId()][$xe4]["count"];
                                if (isset($xe5))
                                    $value .= $xe5;
                            }
                            $this->skipOptions(1);
                            break;
                        case "{review_average}":
                            $product = $this->checkReference($exp['reference'], $product);
                            $value = "";
                            (isset($exp["options"][0]) && $exp["options"][0] == "*") ? $xe4 = 0 : $xe4 = $storeId;
                            (!isset($exp["options"][1]) || !$exp["options"][1]) ? $xe6 = 5 : $xe6 = $exp["options"][1];
                            if (isset($x9d[$product->getId()][$xe4]["score"])) {
                                $xe7 = number_format($x9d[$product->getId()][$xe4]["score"] * $xe6 / 100, 2, ".", "");
                                if (isset($xe7))
                                    $value .= $xe7;
                            }
                            $this->skipOptions(2);
                            break;
                        case "{G:PRODUCT_REVIEW}":
                            $product = $this->checkReference($exp['reference'], $product);
                            (isset($exp["options"][0]) && $exp["options"][0] == "*") ? $xe4 = 0 : $xe4 = $storeId;
                            (!isset($exp["options"][1]) || !$exp["options"][1]) ? $xe6 = 5 : $xe6 = $exp["options"][1];
                            $value = "";
                            if (isset($x9d[$product->getId()][$xe4]["count"])) {
                                $xe5 = $x9d[$product->getId()][$xe4]["count"];
                                $xe7 = number_format($x9d[$product->getId()][$xe4]["score"] * $xe6 / 100, 2, ".", "");
                            }
                            if (isset($xe7) && $xe7 > 0) {
                                $value .= "<g:product_review_average><![CDATA[".$xe7."]]></g:product_review_average>";
                            }
                            if (isset($xe5) && $xe5 > 0) {
                                $value .= "<g:product_review_count><![CDATA[".$xe5."]]></g:product_review_count>";
                            }
                            unset($xe7);
                            unset($xe5);
                            break;
                        case "{G:ITEM_GROUP_ID}":
                            if (isset($this->configurable[$product->getId()])) {
                                $product = $this->checkReference('configurable', $product);
                                $value = "<g:item_group_id><![CDATA[".$product->getSku()."]]></g:item_group_id>";
                            }
                            break;
                        case "{SC:EAN}":
                            (is_numeric($exp['options'][0]) && $exp['options'][0] > 0) ? $xe8 = $exp['options'][0] : $xe8 = 0;
                            $product = $this->checkReference($exp['reference'], $product);
                            $value = explode(',', $product->getEan());
                            $value = "<g:ean><![CDATA[".$value[$xe8]."]]></g:ean>";
                            break;
                        case "{sc:ean}":
                            (is_numeric($exp['options'][0]) && $exp['options'][0] > 0) ? $xe8 = $exp['options'][0] : $xe8 = 0;
                            $product = $this->checkReference($exp['reference'], $product);
                            $value = explode(',', $product->getEan());
                            $value = $value[$xe8];
                            break;
                        case "{SC:IMAGES}":
                            $product = $this->checkReference($exp['reference'], $product);
                            $xce = $product->getSmall_image();
                            $xd0 = array(
                                $product->getImage(),
                                $product->getThumbnail()
                            );
                            $xd1 = '';
                            $xd2 = 0;
                            if ($product->getSmall_image() && $product->getSmall_image() != 'no_selection') {
                                $xcf = $product->getSmall_image();
                                $value = $xcf;
                                $xd1 .= "<g:image_link><![CDATA[".$value."]]></g:image_link>";
                                $xd2++;
                            }
                            $xd3 = 0;
                            while (isset($images[$product->getId()]['src'][$xd3]) && $xd2 < 10) {
                                if ($images[$product->getId()]['src'][$xd3] != $xce) {
                                    if (in_array($images[$product->getId()]['src'][$xd3], $xd0) || $images[$product->getId()]['disabled'][$xd3] != 1) {
                                        $xcf = $images[$product->getId()]['src'][$xd3];
                                        $value = $xcf;
                                        $xd1 .= "<g:additional_image_link><![CDATA[".$value."]]></g:additional_image_link>";
                                        $xd2++;
                                    }
                                }
                                $xd3++;
                            }
                            $value = $xd1;
                            break;
                        case "{sc:images}":
                            $product = $this->checkReference($exp['reference'], $product);
                            $xce = $product->getSmall_image();
                            if (!isset($exp['options'][0]) || $exp['options'][0] == 0) {
                                if ($product->getSmall_image() && $product->getSmall_image() != 'no_selection') {
                                    $xcf = $product->getSmall_image();
                                    $value = $xcf;
                                } else {
                                    $value = $storeMediaUrl . '/catalog/product/placeholder/' . $x5f;
                                }
                            } elseif (isset($images[$product->getId()]['src'][$exp['options'][0] - 1]) && $exp['options'][0] > 0) {
                                if ($images[$product->getId()]['src'][$exp['options'][0] - 1] != $xce) {
                                    $xcf = 'catalog/product/' . $images[$product->getId()]['src'][$exp['options'][0] - 1];
                                    $value = $storeMediaUrl . str_replace('//', '/', $xcf);
                                }
                            }
                            $this->skipOptions(1);
                            break;
                        case "{SC:DESCRIPTION}":
                            $product = $this->checkReference($exp['reference'], $product);
                            $xe9 = $product->getDescription() . $product->getShortDescription();
                            $xea = " | < iframe( . *) < / iframe > | U";
                            preg_match($xea, $xe9, $xeb);
                            if ($xeb) {
                                $xe9 = $product->getAttributeText('manufacturer') . "" . $product->getName() . " - Partnumber:" . $product->getSku() . " - Category: {categories, [1], [1], [1]}";
                            } else {
                                if (in_array("strip_tags", $exp['options'])) {
                                    $xe9 = preg_replace('!\<br /\>!isU', "", $xe9);
                                    $xe9 = preg_replace('!\<br/\>!isU', "", $xe9);
                                    $xe9 = preg_replace('!\<br>!isU', "", $xe9);
                                    $xe9 = strip_tags($xe9);
                                }
                                if (in_array("html_entity_decode", $exp['options'])) {
                                    $xe9 = html_entity_decode($xe9, ENT_QUOTES, 'UTF-8');
                                }
                                if (in_array("htmlentities", $exp['options'])) {
                                    $xe9 = htmlspecialchars(($xe9));
                                }
                                if (strlen($xe9) > 900) {
                                    $xe9 = substr($xe9, 0, 900 - 3);
                                    $xec = strrpos($xe9, "");
                                    $xe9 = substr($xe9, 0, $xec) . '...';
                                }
                            }
                            if ($xe9 == null)
                                $xe9 = $product->getAttributeText('manufacturer') . "" . $product->getName() . " - Partnumber:" . $product->getSku() . " - Category: {categories, [1], [1], [1]}";
                            $xe9 = preg_replace('/' . '[-]' . '|[-][-]+' . '|([]|[-])[-]*' . '|[-]((?![-])|[-]{2,})' . '|[-](([-](?![-]))|' . '(?![-]{2})|[-]{3,})' . '/S', ' ', $xe9);
                            $xe9 = str_replace('', '', $xe9);
                            $value = "<description><![CDATA[".$xe9."]]></description>";
                            $exp['options'] = array();
                            break;
                        case "{sc:description}":
                            $product = $this->checkReference($exp['reference'], $product);
                            $xe9 = $product->getDescription() . $product->getShortDescription();
                            $xea = " | < iframe( . *) < / iframe > | U";
                            preg_match($xea, $xe9, $xeb);
                            if ($xeb) {
                                $xe9 = $product->getAttributeText('manufacturer') . "" . $product->getName() . " - Partnumber:" . $product->getSku() . " - Category: {categories, [1], [1], [1]}";
                            } else {
                                if (in_array("strip_tags", $exp['options'])) {
                                    $xe9 = preg_replace('!\<br /\>!isU', "", $xe9);
                                    $xe9 = preg_replace('!\<br/\>!isU', "", $xe9);
                                    $xe9 = preg_replace('!\<br>!isU', "", $xe9);
                                    $xe9 = strip_tags($xe9);
                                }
                                if (in_array("html_entity_decode", $exp['options'])) {
                                    $xe9 = html_entity_decode($xe9, ENT_QUOTES, 'UTF-8');
                                }
                                if (in_array("htmlentities", $exp['options'])) {
                                    $xe9 = htmlspecialchars(($xe9));
                                }
                                if (strlen($xe9) > 900) {
                                    $xe9 = substr($xe9, 0, 900 - 3);
                                    $xec = strrpos($xe9, "");
                                    $xe9 = substr($xe9, 0, $xec) . '...';
                                }
                            }
                            if ($xe9 == null)
                                $xe9 = $product->getAttributeText('manufacturer') . "" . $product->getName() . " - Partnumber:" . $product->getSku() . " - Category: {categories, [1], [1], [1]}";
                            $xe9 = preg_replace('/' . '[-]' . '|[-][-]+' . '|([]|[-])[-]*' . '|[-]((?![-])|[-]{2,})' . '|[-](([-](?![-]))|' . '(?![-]{2})|[-]{3,})' . '/S', ' ', $xe9);
                            $xe9 = str_replace('', '', $xe9);
                            $value = $xe9;
                            $exp['options'] = array();
                            break;
                        case "{SC:URL}":
                            $product = $this->checkReference($exp['reference'], $product);
                            if ($product->getRequest_path()) {
                                $value = "<link><![CDATA[".$storeBaseUrl.$product->getRequest_path()."]]></link>";
                            } else {
                                $value = "<link><![CDATA[".$product->getProductUrl()."]]></link>";
                            }
                            break;
                        case "{sc:url}":
                            (isset($exp['options'][0])) ? $xd4 = $exp['options'][0] : $xd4 = "";
                            (isset($exp['options'][1])) ? $xd5 = $exp['options'][1] : $xd5 = "";
                            $product = $this->checkReference($exp['reference'], $product);
                            if ($product->getUrlKey()) {
                                $value = $storeBaseUrl . $xd5 . $product->getRequest_path() . $xd4;
                            } else {
                                $value = $product->getProductUrl();
                            }
                            break;
                        case "{SC:CONDITION}":
                            $product = $this->checkReference($exp['reference'], $product);
                            (stristr($product->getName(), "refurbished")) ? $xba = 'refurbished' : $xba = 'new';
                            $value = " <g:condition><![CDATA[".$xba."]]></g:condition>";
                            break;
                        case "{sc:condition}":
                            $product = $this->checkReference($exp['reference'], $product);
                            (stristr($product->getName(), "refurbished")) ? $xba = 'refurbished' : $xba = 'new';
                            $value = $xba;
                            break;
                        default:
                            $product = $this->checkReference($exp['reference'], $product);
                            if (in_array($exp['name'], $attributes)) {
                                if (isset($x89[$exp['name']]) && in_array($x89[$exp['name']], array(
                                    'select',
                                    'multiselect'
                                ))) {
                                    eval('$xee =($product->' . $exp['methodName'] . ");
                                                                                                                                                                                                                                                                                                                    ");
                                    $xed = explode(',', $xee);
                                    if (count($xed) > 1) {
                                        $value = array();
                                        foreach ($xed as $x51) {
                                            if (isset($attributesLabel[$x51][$storeId]))
                                                $value[] = $attributesLabel[$x51][$storeId];
                                            else {
                                                if (isset($attributesLabel[$x51][0]))
                                                    $value[] = $attributesLabel[$x51][0];
                                            }
                                        }
                                    } else {
                                        if (isset($attributesLabel[$xed[0]][$storeId])) {
                                            $value = $attributesLabel[$xed[0]][$storeId];
                                        } else {
                                            if (isset($attributesLabel[$xed[0]][0]))
                                                $value = $attributesLabel[$xed[0]][0];
                                        }
                                    }
                                } else {
                                    eval('$value =($product->' . $exp['methodName'] . ");
                                                                                                                                                                                                                                                                                                                    ");
                                }
                            }
                            if (in_array(@$currencies[$exp['name']], $currencies)) {
                                $value = $currencies[$exp['name']];
                            }
                            $value = $customAttributesObject->_eval($product, $exp, $value);
                            if (is_bool($value) && !$value)
                                continue 3;
                            break;
                    }
                    if (count($exp['options']) > 0) {
                        foreach ($exp['options'] as $x39 => $option) {
                            if ($x39 >= $this->option) {
                                switch ($exp['options'][$this->option]) {
                                    case "substr":
                                        if (isset($exp['options'][$this->option + 1]) && strlen($value) > $exp['options'][$this->option + 1]) {
                                            $value = substr($value, 0, ($exp['options'][$this->option + 1] - 3));
                                            $xec = strrpos($value, " ");
                                            $value = substr($value, 0, $xec) . $exp['options'][$this->option + 2];
                                        }
                                        $this->skipOptions(3);
                                        break;
                                    case "strip_tags":
                                        $xef = "";
                                        $value = preg_replace('!\<br /\>!isU', $xef, $value);
                                        $value = preg_replace('!\<br/\>!isU', $xef, $value);
                                        $value = preg_replace('!\<br>!isU', $xef, $value);
                                        $value = strip_tags($value);
                                        $this->skipOptions(1);
                                        break;
                                    case "htmlentities":
                                        $value = htmlspecialchars(($value));
                                        $this->skipOptions(1);
                                        break;
                                    case "implode":
                                        $value = (is_array($value)) ? implode($exp['options'][$this->option + 1], $value) : $value;
                                        $this->skipOptions(2);
                                        break;
                                    case "float":
                                        $value = number_format($value, $exp['options'][$this->option + 1], '.', '');
                                        $this->skipOptions(2);
                                        break;
                                    case "html_entity_decode":
                                        $value = html_entity_decode($value, ENT_QUOTES, 'UTF-8');
                                        $this->skipOptions(1);
                                        break;
                                    case "inline":
                                        $value = preg_replace('/(\r\n|\n|\r|\r\n\t)/s', ' ', $value);
                                        $this->skipOptions(1);
                                        break;
                                    case "strtolower":
                                        $value = mb_strtolower($value, "UTF8");
                                        $this->skipOptions(1);
                                        break;
                                    case "strtoupper":
                                        $value = mb_strtoupper($value, "UTF8");
                                        $this->skipOptions(1);
                                        break;
                                    case "cleaner":
                                        $value = preg_replace('/' . '[-]' . '|[-][-]+' . '|([]|[-])[-]*' . '|[-]((?![-])|[-]{2,})' . '|[-](([-](?![-]))|' . '(?![-]{2})|[-]{3,})' . '/S', ' ', $value);
                                        $value = str_replace('', '', $value);
                                        $this->skipOptions(1);
                                        break;
                                    default:
                                        $customOptionsObject->option = $this->option;
                                        $value = $customOptionsObject->_eval($exp, $value);
                                        $this->option = $customOptionsObject->option;
                                        if (is_bool($value) && !$value)
                                            continue 3;
                                        break;
                                }
                            }
                        }
                    }
                    if ($feedType > 1)
                        $value = $this->x11d($value);
                    $value = str_replace(array("<", ">"), array("{([", "])}"), $value);

                    //reject overly long 2 byte sequences, as well as characters above U+10000 and replace with ?
                    $value = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]'.
                        '|[\x00-\x7F][\x80-\xBF]+'.
                        '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
                        '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})'.
                        '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
                        '?', $value );
                    //reject overly long 3 byte sequences and UTF-16 surrogates and replace with ?
                    $value = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.
                        '|\xED[\xA0-\xBF][\x80-\xBF]/S','?', $value );

                    $x48 = str_replace($exp['fullpattern'], $value, $x48);
                }
                $x48 = $this->cleanXml($x48, $feedType);
                if ($feedType == 1 || ($feedType != 1 && !$this->_display))
                    $x48 = $this->x11a($x48);
                if ($feedType == 1)
                    $x48 = $this->x119($x48, $feedEncloseData);
                else {
                    if (!$this->_display)
                        $x48 = $this->x11c($x48, $feedSeparator, $feedProtector);
                    else {
                        $x48 = $this->x11b(($x48), false);
                    }
                }
                $x48 = str_replace(array("{([", "])}"), array("<", ">"), $x48);
                if (!empty($x48)) {
                    if ($feedType == 1)
                        $x77 .= $x48 . "";
                    else
                        $x77 .= $x48 . "\x0d\x0a";
                    if ($this->_display) {
                        $xml .= $x77;
                        $x77 = '';
                    } else {
                        if ($xbf % Mage::getStoreConfig("datafeedmanager/system/buffer") == 0) {
                            $file->streamWrite($x77);
                            unset($x77);
                            $x77 = '';
                        }
                    }
                    if ($this->_limit && $xbf >= $this->_limit)
                        break 2;
                    $xbf++;
                    $xb9++;
                }
            }
        }
        if (!$this->_display) {
            $file->streamWrite($x77);
            if (strlen(trim($feedFooter)) > 1)
                $file->streamWrite($feedFooter . "\x0a");
        } else {
            $xml .= $x77;
            $xml .= $feedFooter . "\x0a";
            if ($feedType > 1)
                $xml .= "</table>";
        }
        unset($select);
        if ($this->_display) {
            ($feedType == 1 && !Mage::app()->getRequest()->getParam("real_time_preview")) ? $xe9 = " <textarea id='CodeMirror' class='CodeMirror'>".($xml)."</textarea>" : $xe9 = $xml;
            if ((!Mage::app()->getRequest()->getParam("real_time_preview"))) {
                return (" <html><head><title>" . $this->getFeedName() . "</title><link rel='stylesheet' href='".$storeBaseWebUrl."skin/adminhtml/default/default/CodeMirror2/lib/codemirror.css'><link rel='stylesheet' href='".$storeBaseWebUrl."skin/adminhtml/default/default/CodeMirror2/theme/default.css'><script type='text/javascript' src='".$storeBaseWebUrl."skin/adminhtml/default/default/CodeMirror2/lib/codemirror.js'></script>
                <script type='text/javascript' src='" . $storeBaseWebUrl . "skin/adminhtml/default/default/CodeMirror2/mode/xml/xml.js'></script> 
          </head>
                <body>
    " . $xe9 . "
                <script language='javascript'>
    sHtml=document.getElementById('CodeMirror');
                    if(typeof sHtml != 'undefined'){
        var myCodeMirror = CodeMirror(function(elt) {
            sHtml.parentNode.replaceChild(elt, sHtml)}, {
                        value: sHtml.value,
                       mode:  'xml',                            readOnly: true 

                      }) 
                  }
              </script>
            </body> 
           </html>");
            } else
                return $xe9;
        } else {
            $file->streamClose();
            $this->setFeedUpdatedAt(Mage::getSingleton('core/date')->gmtDate('Y-m-d H:i:s'));
            $this->save();
        }
        return $this;
    }
}

include(Mage::getBaseDir() . "/app/code/local/Wyomind/Datafeedmanager/myCustomAttributes.php");
include(Mage::getBaseDir() . "/app/code/local/Wyomind/Datafeedmanager/myCustomOptions.php");;