<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dane
 * Date: 8/05/12
 * Time: 4:10 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_NotFound_Model_Observer
{
    public function controllerFrontSendResponseBefore($observer)
    {
        $event = $observer->getEvent();
        $front = $event->getFront();
        $response = $front->getResponse();
        $headers = $response->getHeaders();
        foreach ($headers as $header) if ($header['name'] == 'Status' && strpos($header['value'], '404') === 0) {
//            echo "Its a 404!"; die();
            $rootId = Mage::app()->getStore()->getRootCategoryId();

            //Get the last slug in the URL structure.
            $requestString = rtrim($front->getRequest()->getRequestString(), '/');
            $requestString = preg_replace('/(.html)+$/', '' , $requestString);
            $slugs = explode('/', $requestString);
            //We want to work backwards thorugh the slugs from lst to first
            $slugs = array_reverse($slugs);
            if (!isset($slugs[0])) return;

            //Find a matching Category
            $categoryCollection = Mage::getModel('catalog/category')->getCollection()
                ->addPathsFilter("1/{$rootId}")
                ->addAttributeToFilter('url_key', array('eq' => $slugs[0]))
				->addIsActiveFilter()
                ->addUrlRewriteToResult();

            foreach ($categoryCollection as $category) if ($url = $category->getUrl()) return $this->_lateRedirect($response, $url);

            //Find a matching product
            $productCollection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToFilter('url_key', array('eq' => $slugs[0]))
                ->addUrlRewrite();
            foreach ($productCollection as $product) if ($url = $product->getProductUrl()) return $this->_lateRedirect($response, $url);

            //Loop through earlier slugs for a category match.
            foreach ($slugs as $slug) {
                $categoryCollection = Mage::getModel('catalog/category')->getCollection()
                    ->addPathsFilter("1/{$rootId}")
                    ->addAttributeToFilter('url_key', array('eq' => $slug))
                    ->addUrlRewriteToResult()
					->addIsActiveFilter()
					->load();
                foreach ($categoryCollection as $category) if ($url = $category->getUrl()) return $this->_lateRedirect($response, $url);
            }
            //Pick keywords out of the slug(s) to find a product.    - Not working yet.
            /*if ($collection = Mage::getModel('senseportbase/product_collection')) {
                $query = str_replace('-', ' ', $slugs[0]);
                $collection->setSearchQuery($query)->load();
                foreach ($collection as $product) if ($url = $product->getCanonicalUrl()) return $this->_lateRedirect($response, $url);
            }*/
        }
    }
    protected function _lateRedirect($response, $url)
    {
        //Prevent multiple redirects.
        $params = parse_url($url);
        if ($params['path'] == Mage::app()->getRequest()->getRequestString()) {
            return;
        }
        $response->setHeader('HTTP/1.1','301 Moved Permanently');
        $response->setHeader('Status', '301 Moved Permanently');
        $response->setHeader('Location',$url);
    }
}