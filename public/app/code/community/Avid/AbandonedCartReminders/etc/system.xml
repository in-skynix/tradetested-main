<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* @package     Avid_AbandonedCartReminders
* @author      Dane Lowe <dane@avidonline.co.nz>.
* @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
* @license     Commercial - see LICENSE.txt for details
*/
-->
<config>
    <sections>
        <avid_abandoned_cart_reminders translate="label">
            <label>Abandoned Cart Reminders</label>
            <tab>sales</tab>
            <sort_order>1000</sort_order>
            <show_in_default>1</show_in_default>
            <show_in_website>1</show_in_website>
            <show_in_store>1</show_in_store>
            <groups>
                <general translate="label">
                    <label>General</label>
                    <sort_order>10</sort_order>
                    <show_in_default>1</show_in_default>
                    <show_in_website>1</show_in_website>
                    <show_in_store>1</show_in_store>
                    <fields>
                        <minimum_interval_minutes translate="label">
                            <label>Minimum Interval (Minutes)</label>
                            <comment>Will ensure that any email sent is at least this many minutes since the last one was sent. In case sending is paused.</comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>20</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </minimum_interval_minutes>
                        <end_sequence_event_type translate="label">
                            <label>End Sequence On</label>
                            <comment>When this event occurs, the sequence will be ended so that customer no longer receives email reminders.</comment>
                            <frontend_type>select</frontend_type>
                            <source_model>avid_abandoned_cart_reminders/source_event_type</source_model>
                            <sort_order>10</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </end_sequence_event_type>
                        <email_sender translate="label">
                            <label>Email Sender</label>
                            <frontend_type>select</frontend_type>
                            <source_model>adminhtml/system_config_source_email_identity</source_model>
                            <sort_order>30</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </email_sender>
                        <email_bcc>
                            <label>BCC all emails to:</label>
                            <frontend_type>text</frontend_type>
                            <sort_order>40</sort_order>
                            <show_in_default>1</show_in_default>
                        </email_bcc>
                        <allow_copy_quote_details translate="label,comment">
                            <label>Allow Copying Quote Details</label>
                            <comment><![CDATA[If set to No, recovering a cart will only copy products to the cart, it will not allow saved address data etc. to be copied into the customer session.]]></comment>
                            <frontend_type>select</frontend_type>
                            <source_model>adminhtml/system_config_source_yesno</source_model>
                            <sort_order>80</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </allow_copy_quote_details>
                        <require_login translate="label,comment">
                            <label>Require Login</label>
                            <comment><![CDATA[If set to No, recovering a cart will automatically log in a customer that the cart is for.]]></comment>
                            <frontend_type>select</frontend_type>
                            <source_model>adminhtml/system_config_source_yesno</source_model>
                            <sort_order>90</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </require_login>
                    </fields>
                </general>
                <background>
                    <label>Background Processes</label>
                    <sort_order>50</sort_order>
                    <show_in_default>1</show_in_default>
                    <show_in_website>1</show_in_website>
                    <show_in_store>1</show_in_store>
                    <fields>
                        <update_sequence_frontend translate="label,comment">
                            <label>Create / Update Sequences on Frontend</label>
                            <comment><![CDATA[If disabled, sequences will not be created or updated whenever a customer adds or changes an email on their quote.]]></comment>
                            <frontend_type>select</frontend_type>
                            <source_model>adminhtml/system_config_source_yesno</source_model>
                            <sort_order>50</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </update_sequence_frontend>
                        <create_sequence_cron translate="label,comment">
                            <label>Create Reminders via Cron</label>
                            <comment><![CDATA[If enabled, runs a scheduled task to find any quotes that have not already had sequences created for it.]]></comment>
                            <frontend_type>select</frontend_type>
                            <source_model>adminhtml/system_config_source_yesno</source_model>
                            <sort_order>40</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </create_sequence_cron>
                        <create_sequence_cron_days>
                            <label>Days to search back when creating reminders via cron</label>
                            <comment><![CDATA[]]></comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>30</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </create_sequence_cron_days>
                        <cron_batch_size>
                            <label>Cron Batch Size</label>
                            <comment><![CDATA[]]></comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>20</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </cron_batch_size>
                        <send_sequence_cron translate="label,comment">
                            <label>Send Emails via Cron</label>
                            <comment><![CDATA[If enabled, will automatically send emails on a regular basis. If disabled, emails will only be sent manually.]]></comment>
                            <frontend_type>select</frontend_type>
                            <source_model>adminhtml/system_config_source_yesno</source_model>
                            <sort_order>10</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </send_sequence_cron>
                    </fields>
                </background>
                <sequencing>
                    <label>Sequencing</label>
                    <sort_order>20</sort_order>
                    <show_in_default>1</show_in_default>
                    <show_in_website>1</show_in_website>
                    <show_in_store>1</show_in_store>
                    <fields>
                        <steps translate="label,comment">
                            <label>Cart Reminder Steps</label>
                            <comment><![CDATA[]]></comment>
                            <frontend_type>textarea</frontend_type>
                            <frontend_model>avid_abandoned_cart_reminders/adminhtml_field_steps</frontend_model> <!-- Even though it is called frontend_model it really is a block. -->
                            <sort_order>1</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </steps>
                        <steps_wishlist translate="label,comment">
                            <label>Wishlist Reminder Steps</label>
                            <comment><![CDATA[]]></comment>
                            <frontend_type>textarea</frontend_type>
                            <frontend_model>avid_abandoned_cart_reminders/adminhtml_field_steps_wishlist</frontend_model> <!-- Even though it is called frontend_model it really is a block. -->
                            <sort_order>100</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </steps_wishlist>
                        <steps_conditional translate="label,comment">
                            <label>Conditional Cart Reminder Steps</label>
                            <comment><![CDATA[See Conditional Sequence Setup]]></comment>
                            <frontend_type>textarea</frontend_type>
                            <template_type>quote</template_type>
                            <frontend_model>avid_abandoned_cart_reminders/adminhtml_field_steps</frontend_model> <!-- Even though it is called frontend_model it really is a block. -->
                            <sort_order>200</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </steps_conditional>
                    </fields>
                </sequencing>
                <conditional_sequence>
                    <label>Conditional Sequence</label>
                    <sort_order>30</sort_order>
                    <show_in_default>1</show_in_default>
                    <fields>
                        <enable>
                            <label>Enabled</label>
                            <comment><![CDATA[If enabled, any abandoned cart matching the conditions here will be added to the Conditional Cart Reminder Sequence <strong>instead</strong> of the standard Cart Reminder Sequence ]]></comment>
                            <frontend_type>select</frontend_type>
                            <source_model>adminhtml/system_config_source_yesno</source_model>
                            <sort_order>10</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </enable>
                        <require_fields>
                            <label>Require Fields Specified</label>
                            <frontend_type>select</frontend_type>
                            <source_model>avid_abandoned_cart_reminders/system_config_source_requirefields</source_model>
                            <sort_order>20</sort_order>
                            <show_in_default>1</show_in_default>
                        </require_fields>
                        <min_quote_subtotal>
                            <label>Minimum Quote Subtotal</label>
                            <comment><![CDATA[Decimal value representing minimum value of quote, e.g. 250.50]]></comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>30</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </min_quote_subtotal>
                    </fields>
                </conditional_sequence>
                <reports>
                    <label>Reports</label>
                    <sort_order>40</sort_order>
                    <show_in_default>1</show_in_default>
                    <fields>
                        <default_graph_statistics>
                            <label>Default Statistics for Graph</label>
                            <frontend_type>multiselect</frontend_type>
                            <source_model>avid_abandoned_cart_reminders/system_config_source_statistics_scoped</source_model>
                            <sort_order>50</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </default_graph_statistics>
                        <utm_source>
                            <label>Google Analytics UTM Source</label>
                            <comment><![CDATA[Used for tagging email links for Google Analytics]]></comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>40</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </utm_source>
                        <utm_medium>
                            <label>Google Analytics UTM Medium</label>
                            <comment><![CDATA[Used for tagging email links for Google Analytics]]></comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>30</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </utm_medium>
                        <utm_campaign>
                            <label>Google Analytics UTM Campaign</label>
                            <frontend_type>label</frontend_type>
                            <sort_order>20</sort_order>
                            <show_in_default>1</show_in_default>
                        </utm_campaign>
                        <utm_term>
                            <label>Google Analytics UTM Term</label>
                            <frontend_type>label</frontend_type>
                            <sort_order>10</sort_order>
                            <show_in_default>1</show_in_default>
                        </utm_term>
                    </fields>
                </reports>
                <coupons>
                    <label>Coupons</label>
                    <sort_order>30</sort_order>
                    <show_in_default>1</show_in_default>
                    <fields>
                        <length>
                            <label>Coupon Code Length</label>
                            <comment><![CDATA[Number of digits in a generated coupon code.]]></comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>1</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </length>
                        <expire_days>
                            <label>Coupon Expiry (days)</label>
                            <comment><![CDATA[Coupon will expire this many days after being sent.]]></comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>1</sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                        </expire_days>
                    </fields>
                </coupons>
                <test_mode>
                    <label>Test Mode</label>
                    <sort_order>100</sort_order>
                    <show_in_default>1</show_in_default>
                    <fields>
                        <enabled translate="label,comment">
                            <label>Test Mode Enabled</label>
                            <comment><![CDATA[If enabled, all reminder emails will be sent to the address below, and not to the customer.]]></comment>
                            <frontend_type>select</frontend_type>
                            <source_model>adminhtml/system_config_source_yesno</source_model>
                            <sort_order>10</sort_order>
                            <show_in_default>1</show_in_default>
                        </enabled>
                        <email>
                            <label>Email address to send all reminders to</label>
                            <comment><![CDATA[]]></comment>
                            <frontend_type>text</frontend_type>
                            <sort_order>30</sort_order>
                            <show_in_default>1</show_in_default>
                        </email>
                    </fields>
                </test_mode>
            </groups>
        </avid_abandoned_cart_reminders>
    </sections>
</config>