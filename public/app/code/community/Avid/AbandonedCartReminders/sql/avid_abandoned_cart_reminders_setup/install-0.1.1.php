<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

//Sequences Table
$table = $installer->getConnection()->newTable($installer->getTable('avid_abandoned_cart_reminders/reminder'))
    ->addColumn('reminder_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Reminder ID')
    ->addColumn('quote_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array('unsigned'  => true), 'Quote ID')
    ->addColumn('wishlist_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array('unsigned'  => true), 'Wishlist ID')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array('unsigned'  => true), 'Store ID')
    ->addColumn('type', Varien_Db_Ddl_Table::TYPE_CHAR, 255, array(), 'Type')
    ->addColumn('email', Varien_Db_Ddl_Table::TYPE_CHAR, 255, array(), 'Email')
    ->addColumn('abandoned_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Quote Last Edited Time')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Creation Time')
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Update Time')
    ->addColumn('last_sent_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Last Sent Time')
    ->addColumn('retrieval_code', Varien_Db_Ddl_Table::TYPE_CHAR, 255, array(), 'Retrieval Code')
    ->addColumn('retrieve_count', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array('unsigned'  => true), 'Retrieve Count')
    ->addColumn('is_converted', Varien_Db_Ddl_Table::TYPE_INTEGER, 1, array('unsigned'  => true), 'Is converted')
    ->addColumn('scheduled_step_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 8, array('unsigned'  => true), 'Scheduled Step')
    ->addColumn('scheduled_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Last Sent Time')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_CHAR, 32, array('default' => 'ready'), 'Status')
    ->addColumn('flagged_events', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(), 'Flagged Events')
    ->addColumn('promo_data', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(), 'Promo Data')
    ->addIndex($installer->getIdxName('avid_abandoned_cart_reminders/reminder', array('quote_id')), 'quote_id', array('type' => 'UNIQUE'))
    ->addForeignKey($installer->getFkName('avid_abandoned_cart_reminders/reminder', 'quote_id', 'sales/quote', 'entity_id'), 'quote_id', $this->getTable('sales/quote'), 'entity_id', Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('avid_abandoned_cart_reminders/reminder', 'wishlist_id', 'wishlist/wishlist', 'wishlist_id'), 'wishlist_id',$this->getTable('wishlist/wishlist'), 'wishlist_id', Varien_Db_Ddl_Table::ACTION_CASCADE);
$installer->getConnection()->createTable($table);

//Sequence Events Table
$table = $installer->getConnection()->newTable($installer->getTable('avid_abandoned_cart_reminders/reminder_event'))
    ->addColumn('event_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Event ID')
    ->addColumn('reminder_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array('unsigned'  => true,), 'Reminder ID')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Creation Time')
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Update Time')
    ->addColumn('event_type', Varien_Db_Ddl_Table::TYPE_CHAR, 32, array(), 'Event Type')
    ->addColumn('step_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array('unsigned'  => true,), 'Step ID')
    ->addColumn('is_first_for_step', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array('unsigned'  => true,), 'Is First Event of Type For Step')
    ->addColumn('is_first_for_reminder', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array('unsigned'  => true,), 'Is First Event of Type For Reminder')
    ->addColumn('value_number', Varien_Db_Ddl_Table::TYPE_DOUBLE, 16, array('unsigned'  => true,), 'Value Number')
    ->addColumn('value_currency', Varien_Db_Ddl_Table::TYPE_DOUBLE, 16, array('unsigned'  => true,), 'Value Currency')
    ->addColumn('event_data', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(), 'Event Data')
    ->addForeignKey($installer->getFkName('avid_abandoned_cart_reminders/reminder_event', 'reminder_id', 'avid_abandoned_cart_reminders/reminder', 'reminder_id'), 'reminder_id', $this->getTable('avid_abandoned_cart_reminders/reminder'), 'reminder_id', Varien_Db_Ddl_Table::ACTION_CASCADE);
$installer->getConnection()->createTable($table);

$table = $installer->getConnection()->newTable($installer->getTable('avid_abandoned_cart_reminders/statistic_bucket'))
    ->addColumn('bucket_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 16, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Event ID')
    ->addColumn('end_date', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'End Date')
    ->addColumn('bucket_size', Varien_Db_Ddl_Table::TYPE_CHAR, 32, array(), 'Bucket Size')
    ->addColumn('statistic_code', Varien_Db_Ddl_Table::TYPE_CHAR, 32, array(), 'Statistic Code')
    ->addColumn('uniqueness_scope', Varien_Db_Ddl_Table::TYPE_CHAR, 32, array(), 'Uniqueness Code')
    ->addColumn('value', Varien_Db_Ddl_Table::TYPE_DOUBLE, 16, array('unsigned'  => true,), 'Value')
    ->addIndex($installer->getIdxName('avid_abandoned_cart_reminders/reminder', array('end_date', 'bucket_size', 'statistic_code', 'uniqueness_scope')), array('end_date', 'bucket_size', 'statistic_code', 'uniqueness_scope'), array('type' => 'UNIQUE'));
$installer->getConnection()->createTable($table);

$installer->endSetup();

$this->getConnection()->resetDdlCache();


