<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Adminhtml_CartReminders_SendController
 *
 * Adminhtml Controller for viewing recently sent reminder emails.
 */
class Avid_AbandonedCartReminders_Adminhtml_CartReminders_HistoryController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Init actions
     *
     * @return Mage_Adminhtml_Cms_BlockController
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
            ->_setActiveMenu('avid_abandoned_cart_reminders/index')
            ->_addBreadcrumb(Mage::helper('cms')->__('Sales'), Mage::helper('cms')->__('Sales'))
            ->_addBreadcrumb(Mage::helper('cms')->__('Abandoned Cart Reminders'), Mage::helper('cms')->__('Abandoned Cart Reminders'))
        ;
        return $this;
    }

    /**
     * Display List (AJAX)
     */
    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Display List and Actions
     */
    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Clear out old reminder that have been ended/cleaned and are old enough to not get recreated.
     */
    public function cleanAction()
    {
        $count = Mage::helper('avid_abandoned_cart_reminders')->cleanOldReminders();
        $this->_getSession()->addSuccess("Cleaned {$count} old reminders");
        $this->_redirect('*/*/index');
    }
}