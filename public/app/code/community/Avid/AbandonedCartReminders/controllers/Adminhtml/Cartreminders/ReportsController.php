<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Adminhtml_CartReminders_ReportsController
 *
 * Admin Controller for viewing reports/
 */
class Avid_AbandonedCartReminders_Adminhtml_CartReminders_ReportsController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Init actions
     *
     * @return Mage_Adminhtml_Cms_BlockController
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
            ->_setActiveMenu('avid_abandoned_cart_reminders/index')
            ->_addBreadcrumb(Mage::helper('cms')->__('Sales'), Mage::helper('cms')->__('Sales'))
            ->_addBreadcrumb(Mage::helper('cms')->__('Abandoned Cart Reminders'), Mage::helper('cms')->__('Abandoned Cart Reminders'))
        ;
        return $this;
    }

    /**
     * Display Dashboard.
     */
    public function indexAction() {
        if ($date = $this->getRequest()->getParam('from'))
            Mage::helper('avid_abandoned_cart_reminders')->setStatisticFromDate($date);
        if ($date = $this->getRequest()->getParam('to'))
            Mage::helper('avid_abandoned_cart_reminders')->setStatisticToDate($date);
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Display details on chosen statistic.
     */
    public function detailsAction()
    {
        $request = $this->getRequest();
        $statistic = Mage::helper('avid_abandoned_cart_reminders')->getStatistic($request->getParam('statistic_code'));
        foreach (array('uniqueness_scope', 'from_date', 'to_date') as $_scope)
            if ($value = $request->getParam($_scope))
                $statistic->setData($_scope, $value);

        //Default to a single month for histogram.
        if (!$request->getParam('from_date')) {
            $yesterday =  date("Y-m-d", strtotime('-1 day'));
            $start = date("Y-m-d", strtotime("{$yesterday} -1 month"));
            $statistic->setFromDate($start)->setToDate($yesterday);
        }
//        Mage::register('avid_abandoned_cart_reminders.details.statistic', $statistic);
        $this->loadLayout();

        $block = $this->getLayout()->createBlock('avid_abandoned_cart_reminders/adminhtml_reports_statistic_detail')->setStatistic($statistic);
        $this->getLayout()->getBlock('content')->append($block, 'detail');

        $this->renderLayout();
    }

    /**
     * Graph JSON action, based on chosen statistics.
     */
    public function jsonAction()
    {
        $tqx = array();
        $tqData = explode(";",$this->getRequest()->getParam('tqx'));
        foreach ($tqData as $_tqData) if ($_tqData) {
            list ($_k, $_v) = explode(":", $_tqData);
            $tqx[$_k] = $_v;
        }

        $params = $this->getRequest()->getParams();
        $block = $this->getLayout()->createBlock('avid_abandoned_cart_reminders/adminhtml_reports_statistic_graph');
        foreach(explode(',', $params['statistics']) as $_statisticCode) if ($_statisticCode) {
            list($_code,$_scope) = explode(':', $_statisticCode);
            $_statistic = Mage::helper('avid_abandoned_cart_reminders')->getStatistic($_code)
                ->setFromDate($params['from'])
                ->setToDate($params['to'])
                ->setUniquenessScope($_scope);
            $block->addStatistic($_statistic);
        }
        try {
            $response = array(
                'reqId' => $tqx['reqId'],
                'status' => 'ok',
                'table' => $block->getTableArray()
            );
        } catch (Exception $e) {
            $response = array(
                'reqId' => $tqx['reqId'],
                'status' => 'error',
                'errors' => array(array('reason' => 'internal_error'))
            );
        }
        echo json_encode($response);
    }

    /**
     * Save Default Statistic Options for Graph.
     */
    public function setDefaultStatisticsAction()
    {
        Mage::getModel('core/config')->saveConfig('avid_abandoned_cart_reminders/reports/default_graph_statistics', implode(',', $this->getRequest()->getParam('statistics')));
    }
}