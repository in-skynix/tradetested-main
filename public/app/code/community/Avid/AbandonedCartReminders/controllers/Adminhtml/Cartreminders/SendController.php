<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Adminhtml_CartReminders_SendController
 *
 * Adminhtml Controller for viewing and sending reminder emails.
 */
class Avid_AbandonedCartReminders_Adminhtml_CartReminders_SendController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Init actions
     *
     * @return Mage_Adminhtml_Cms_BlockController
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
            ->_setActiveMenu('avid_abandoned_cart_reminders/index')
            ->_addBreadcrumb(Mage::helper('cms')->__('Sales'), Mage::helper('cms')->__('Sales'))
            ->_addBreadcrumb(Mage::helper('cms')->__('Abandoned Cart Reminders'), Mage::helper('cms')->__('Abandoned Cart Reminders'))
        ;
        return $this;
    }

    /**
     * Display List (AJAX)
     */
    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Display List and Actions
     */
    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Get Current Processing Status from Background Process.
     */
    public function statusAction() {
        echo Mage::helper('avid_abandoned_cart_reminders/queue')->getMsg();
        die();
    }

    /**
     * View Reminder Email Preview.
     */
    public function viewAction() {
        $reminder = Mage::getModel('avid_abandoned_cart_reminders/reminder')->load($this->getRequest()->getParam('reminder_id'));
        Mage::register('avid_abandoned_cart_reminders.reminder', $reminder);
        $this->loadLayout();
        $this->renderLayout();
    }

    public function deleteAction()
    {
        Mage::getModel('avid_abandoned_cart_reminders/reminder')->load($this->getRequest()->getParam('reminder_id'))->clean();
        $this->_getSession()->addSuccess('Removed Reminder from Queue');
        $this->_redirect('*/*/index');
    }

    /**
     * Send All Emails that are Ready / Scheduled.
     */
    public function sendReadyAction() {
        Mage::helper('avid_abandoned_cart_reminders/queue')->spawn();
        $this->_getSession()->addSuccess('Send Process Started');
        $this->_redirect('*/*/index');
    }

    public function massDeleteAction()
    {
        Mage::getModel('avid_abandoned_cart_reminders/reminder')->getResource()->updateAttributes($this->getRequest()->getParam('reminder_ids'), array('status' => 'cleaned'));
        $this->_getSession()->addSuccess('Removed Reminders from Queue');
        $this->_redirect('*/*/index');
    }

    /**
     * Send Selected Emails now
     *
     * regardless of current scheduled time.
     */
    public function massSendNowAction()
    {
        $data = array(
            'scheduled_at' => date('Y-m-d H:i:s', strtotime('-2 minutes'))
        );
        Mage::getModel('avid_abandoned_cart_reminders/reminder')->getResource()->updateAttributes($this->getRequest()->getParam('reminder_ids'), $data);
        Mage::helper('avid_abandoned_cart_reminders/queue')->spawn();
        $this->_getSession()->addSuccess('Send Process Started');
        $this->_redirect('*/*/index');
    }

    /**
     * Send single Email now
     *
     * regardless of time it has been scheduled for.
     */
    public function sendNowAction()
    {
        $reminder = Mage::getModel('avid_abandoned_cart_reminders/reminder')->load($this->getRequest()->getParam('reminder_id'));
        $reminder->setScheduledAt(date('Y-m-d H:i:s', strtotime('-2 minutes')))->send();
        $this->_getSession()->addSuccess('Reminder Email Sent');
        $this->_redirect('*/*/index');
    }

    /**
     * Create Random Test Reminder
     */
    public function createTestAction()
    {
//        $helper = Mage::helper('avid_abandoned_cart_reminders');
//        $quoteCollection = Mage::getModel('sales/quote')->getCollection();
//        $quoteCollection->getSelect()->order('rand()')->limit(1);
//        $quote = $quoteCollection->fetchItem()->setCustomerEmail($helper->getTestModeEmail());
//        $reminder = $helper->saveReminderForObject($quote);
//        $this->_getSession()->addSuccess('Test Reminder Created. ID:'.$reminder->getId());
        $this->_getSession()->addSuccess('Temporarily Disabled');
        $this->_redirect('*/*/index');
    }
}