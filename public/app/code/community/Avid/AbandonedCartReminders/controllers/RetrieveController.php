<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_RetrieveController
 *
 * Controller for retrieving an abandoned cart.
 */
class Avid_AbandonedCartReminders_RetrieveController extends Mage_Core_Controller_Front_Action
{
    /**
     * Retrieve a quote by reminder retrieval_code and ID.
     *
     * Let reminder model handle retrieval of items, and then redirect to cart if login is not necessary.
     */
    public function quoteAction()
    {
        $reminder = Mage::getModel('avid_abandoned_cart_reminders/reminder')
            ->loadByCode($this->getRequest()->getParam('code'), $this->getRequest()->getParam('id'));
        $reminder->retrieveToCart();
        if($reminder->getIsRequiredLogin()) {
            $customerSession = Mage::getModel('customer/session');
            if ($customerSession->getCustomerId()) {
                Mage::getSingleton('core/session')->addError("Sorry, you need to be logged in as {$reminder->getCustomer()->getEmail()} in order to retrieve that cart. Please log out and try again.");
                $this->_redirectToCart();
            } else {
                Mage::getSingleton('core/session')->addWarning("Please log in in as {$reminder->getCustomer()->getEmail()} in order to retrieve that cart.");
                if (!Mage::getSingleton('customer/session')->authenticate($this))
                    $this->setFlag('', 'no-dispatch', true);
            }
        } else {
            $this->_redirectToCart();
        }
    }

    /**
     * Redirect to cart.
     */
    protected function _redirectToCart()
    {
        $this->_redirect('checkout/cart');
    }
}