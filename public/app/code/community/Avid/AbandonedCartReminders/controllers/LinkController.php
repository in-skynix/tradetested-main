<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_LinkController
 *
 * All links in emails should go to this controller, which records the click event and adds Google Analytics tags to URL
 * before redirecting to it.
 */
class Avid_AbandonedCartReminders_LinkController extends Mage_Core_Controller_Front_Action
{
    /**
     * Record, tag and redirect to URL (l).
     */
    public function redirectAction()
    {
        $linkData = Mage::helper('avid_abandoned_cart_reminders/link')->extractLinkData($this->getRequest()->getParam('l'));
        $reminder = Mage::getModel('avid_abandoned_cart_reminders/reminder')->load($linkData['reminder_id'])->setLastStepId($linkData['step_id']);
        $reminder->processReminderEvent('click', array('url' => $linkData['url']), $linkData['step_id']);
        Mage::helper('avid_abandoned_cart_reminders')->setActiveReminder($reminder);
        if (!$linkData['url'])
            $this->_forward('defaultNoRoute');
        $this->_redirectUrl($linkData['tagged_url']);
    }
}