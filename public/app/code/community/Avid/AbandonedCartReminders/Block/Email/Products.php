<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Block_Email_Products
 *
 * Block for displaying products in email.
 */
class Avid_AbandonedCartReminders_Block_Email_Products extends Mage_Catalog_Block_Product_Abstract
{

}