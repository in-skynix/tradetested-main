<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 24/07/13
 * Time: 3:00 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_AbandonedCartReminders_Block_Adminhtml_Field_Steps_Wishlist extends Avid_AbandonedCartReminders_Block_Adminhtml_Field_Steps
{
    //@todo: remove the requirement to overload this block. Some sort of XML setting?
    /**
     * Get Element HTML
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        return $this->setTemplate('avid/abandoned_cart_reminders/field/steps.phtml')
            ->setTemplateOptions(Mage::getSingleton('avid_abandoned_cart_reminders/source_email_template_wishlist')->toOptionArray())
            ->setElement($element)
            ->toHtml();
    }
}