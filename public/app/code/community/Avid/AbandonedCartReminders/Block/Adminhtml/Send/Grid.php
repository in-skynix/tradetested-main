<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Block_Adminhtml_Send_Grid
 *
 * Grid for showing all emails scheduled to be sent in the future.
 */
class Avid_AbandonedCartReminders_Block_Adminhtml_Send_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Construct grid block
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('avid_abandoned_cart_reminders_step_grid');
        $this->setDefaultSort('scheduled_at');
        $this->setUseAjax(true);
    }

    /**
     * Prepare Collection
     *
     * @return this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::helper('avid_abandoned_cart_reminders/scheduler')->getAllIncompleteReminders();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare Columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $storeOptions = array();
        $storeOptionArrays = Mage::getSingleton('adminhtml/system_config_source_store')->toOptionArray();
        foreach ($storeOptionArrays as $_storeOption)
            $storeOptions[$_storeOption['value']] = $_storeOption['label'];

        $stepOptions = array();
        foreach (Mage::app()->getStores() as $_store) {
            $storeId = $_store->getId();
            $steps = json_decode(Mage::getStoreConfig('avid_abandoned_cart_reminders/sequencing/steps', $storeId), true);
            foreach ($steps as $_stepData)
                $stepOptions[$_stepData['step_id']] = $_stepData['label'];
        }

        $this->addColumn('reminder_id',
            array(
                'header'=> Mage::helper('avid_abandoned_cart_reminders')->__('ID'),
                'width' => '50px',
                'type'  => 'number',
                'index' => 'reminder_id',
            ));
        $this->addColumn('scheduled_at', array(
            'header'    => Mage::helper('avid_abandoned_cart_reminders')->__('Scheduled'),
            'index'     => 'scheduled_at',
            'type'      => 'datetime',
        ));
        $this->addColumn('abandoned_at', array(
            'header'    => Mage::helper('avid_abandoned_cart_reminders')->__('Abandoned'),
            'index'     => 'abandoned_at',
            'type'      => 'datetime',
        ));
        $this->addColumn('email', array(
            'header'    => Mage::helper('avid_abandoned_cart_reminders')->__('Email'),
            'index'     => 'email'
        ));
        $this->addColumn('scheduled_step_id', array(
            'header'    => Mage::helper('avid_abandoned_cart_reminders')->__('Scheduled Step'),
            'index'     => 'scheduled_step_id',
            'renderer'  => 'Avid_AbandonedCartReminders_Block_Adminhtml_Renderer_StepLabel',
            'type'      => 'options',
            'options'   => $stepOptions,
        ));
        $this->addColumn('store_id', array(
            'header'    => Mage::helper('avid_abandoned_cart_reminders')->__('Store'),
            'index'     => 'store_id',
            'renderer'  => 'Avid_AbandonedCartReminders_Block_Adminhtml_Renderer_StoreLabel',
            'type'      => 'options',
            'options'   => $storeOptions,
        ));
        $this->addColumn('last_sent_at', array(
            'header'    => Mage::helper('avid_abandoned_cart_reminders')->__('Last Sent'),
            'index'     => 'last_sent_at',
            'type'      => 'datetimeloc',
        ));
        $this->addColumn('view_action',
            array(
                'header'    => Mage::helper('avid_abandoned_cart_reminders')->__('Action'),
                'width'     => '150px',
                'type'      => 'action',
                'getter'     => 'getId',
                'actions'   => array(
                    array(
                        'caption' => Mage::helper('catalog')->__('View Email'),
                        'url'     => array(
                            'base'=>'*/cartreminders_send/view',
                        ),
                        'field'   => 'reminder_id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
            )
        );
        $this->addColumn('delete_action',
            array(
                'header'    => Mage::helper('avid_abandoned_cart_reminders')->__('Action'),
                'width'     => '150px',
                'type'      => 'action',
                'getter'     => 'getId',
                'actions'   => array(
                    array(
                        'caption' => Mage::helper('catalog')->__('Cancel Reminder'),
                        'url'     => array(
                            'base'=>'*/cartreminders_send/delete',
                        ),
                        'field'   => 'reminder_id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
            )
        );
        $this->addColumn('send_action',
            array(
                'header'    => Mage::helper('avid_abandoned_cart_reminders')->__('Action'),
                'width'     => '150px',
                'type'      => 'action',
                'getter'     => 'getId',
                'actions'   => array(
                    array(
                        'caption' => Mage::helper('catalog')->__('Send Now'),
                        'url'     => array(
                            'base'=>'*/cartreminders_send/sendNow',
                        ),
                        'field'   => 'reminder_id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
            )
        );
        return parent::_prepareColumns();
    }

    /**
     * Get Grid URL for use in AJAX requests
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->_getData('grid_url') ? $this->_getData('grid_url') : $this->getUrl('*/cartreminders_send/grid', array('_current'=>true));
    }

    /**
     * Get Reminder edit URL for use when clicking on a row.
     *
     * @param Avid_AbandonedCartReminders_Reminder $item
     * @return string
     */
    public function getRowUrl($item)
    {
        return $this->getUrl('*/cartreminders_send/view', array('reminder_id' => $item->getId()));
    }

    /**
     * Prepare Mass Actions.
     *
     * Including send all selected now.
     *
     * @return $this;
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('reminder_id');
        $this->getMassactionBlock()->setFormFieldName('reminder_ids');
        $this->getMassactionBlock()->addItem('send_now', array(
            'label'=> Mage::helper('catalog')->__('Schedule and Send NOW'),
            'url'  => $this->getUrl('*/*/massSendNow'),
            'confirm' => Mage::helper('catalog')->__('This action will reschedule all selected reminders to send now. Are you sure you want to do this?')
        ));
        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('catalog')->__('Cancel Reminders'),
            'url'  => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('catalog')->__('This action will clear out all selected reminders. Are you sure you want to do this?')
        ));

        return $this;
    }

}