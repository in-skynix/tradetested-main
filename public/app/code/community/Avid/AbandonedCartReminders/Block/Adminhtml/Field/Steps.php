<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Block_Adminhtml_Field_Steps
 *
 * Block for the step configuration field type.
 */
class Avid_AbandonedCartReminders_Block_Adminhtml_Field_Steps extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /**
     * Get Element HTML
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        return $this->setTemplate('avid/abandoned_cart_reminders/field/steps.phtml')
            ->setTemplateOptions(Mage::getSingleton('avid_abandoned_cart_reminders/source_email_template_quote')->toOptionArray())
            ->setElement($element)
            ->toHtml();
    }
}