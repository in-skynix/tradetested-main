<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Block_Adminhtml_Renderer_StoreLabel
 *
 * Grid column renderer for store label.
 */
class Avid_AbandonedCartReminders_Block_Adminhtml_Renderer_StoreLabel extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Render
     *
     * @param Varien_Object $row
     * @return null|string
     */
    public function render(Varien_Object $row)
    {
        $id =  $row->getData($this->getColumn()->getIndex());
        return Mage::app()->getStore($id)->getName();
    }
}