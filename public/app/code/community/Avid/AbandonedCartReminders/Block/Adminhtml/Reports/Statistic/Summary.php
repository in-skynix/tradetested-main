<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Block_Adminhtml_Reports_Statistic_Summary
 *
 * Block for displaying summary (totals) of statistic over chosen time period.
 */
class Avid_AbandonedCartReminders_Block_Adminhtml_Reports_Statistic_Summary extends Avid_AbandonedCartReminders_Block_Adminhtml_Reports_Dashboard
{
    /**
     * Set Statistic Code
     *
     * @param string $code
     * @return $this
     */
    public function setStatisticCode($code)
    {
        $this->setStatistic(
            Mage::helper('avid_abandoned_cart_reminders')->getStatistic($code)
                ->setFromDate($this->getfromDate())
                ->setToDate($this->getToDate())
        );
        return $this;
    }
}