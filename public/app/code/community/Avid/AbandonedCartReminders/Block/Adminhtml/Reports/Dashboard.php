<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Block_Adminhtml_Reports_Dashboard
 *
 * Block for displaying numerous statistic summaries.
 */
class Avid_AbandonedCartReminders_Block_Adminhtml_Reports_Dashboard extends Mage_Adminhtml_Block_Template
{
    //Default statistics displayed in graph.
    protected $_defaultStatisticCodes;

    /**
     * Get All statistics available to be shown.
     *
     * @return array
     */
    public function getAllStatistics()
    {
        return Mage::helper('avid_abandoned_cart_reminders')->getAllStatistics();
    }

    /**
     * Get From Date
     *
     * @return string
     */
    public function getFromDate()
    {
        return Mage::helper('avid_abandoned_cart_reminders')->getStatisticFromDate();
    }

    /**
     * Get To Date
     *
     * @return string
     */
    public function getToDate()
    {
        return Mage::helper('avid_abandoned_cart_reminders')->getStatisticToDate();
    }

    /**
     * Check if Statistic Code is for a Default Graph Statistic
     *
     * @param string $code
     * @return bool
     */
    public function isDefaultStatistic($code)
    {
        if (!$this->_defaultStatisticCodes)
            $this->_defaultStatisticCodes = explode(',', Mage::getStoreConfig('avid_abandoned_cart_reminders/reports/default_graph_statistics'));
        foreach($this->_defaultStatisticCodes as $_code)
            if ($_code == $code)
                return true;
        return false;
    }

    /**
     * GEt HTMl for a statistic summary box.
     *
     * @param string $code
     * @return string HTMl Content.
     */
    public function getStatisticSummaryHtml($code)
    {
        return $this->getLayout()->createBlock('avid_abandoned_cart_reminders/adminhtml_reports_statistic_summary')
            ->setTemplate('avid/abandoned_cart_reminders/reports/statistic/summary.phtml')
            ->setStatisticCode($code)
            ->toHtml();
    }
}