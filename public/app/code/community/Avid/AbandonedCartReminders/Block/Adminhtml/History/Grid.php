<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Block_Adminhtml_Send_Grid
 *
 * Grid for showing all emails scheduled to be sent in the future.
 */
class Avid_AbandonedCartReminders_Block_Adminhtml_History_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Construct grid block
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('avid_abandoned_cart_reminders_history_grid');
        $this->setDefaultSort('sent_at', 'desc');
        $this->setUseAjax(true);
    }

    /**
     * Prepare Collection
     *
     * @return this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('avid_abandoned_cart_reminders/reminder')->getCollection()
            ->addhistoryFilter();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare Columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $storeOptions = array();
        $storeOptionArrays = Mage::getSingleton('adminhtml/system_config_source_store')->toOptionArray();
        foreach ($storeOptionArrays as $_storeOption)
            $storeOptions[$_storeOption['value']] = $_storeOption['label'];

        $this->addColumn('reminder_id',
            array(
                'header'=> Mage::helper('avid_abandoned_cart_reminders')->__('ID'),
                'width' => '50px',
                'type'  => 'number',
                'index' => 'reminder_id',
            ));
        $this->addColumn('abandoned_at', array(
            'header'    => Mage::helper('avid_abandoned_cart_reminders')->__('Abandoned'),
            'index'     => 'abandoned_at',
            'type'      => 'datetime',
        ));
        $this->addColumn('email', array(
            'header'    => Mage::helper('avid_abandoned_cart_reminders')->__('Email'),
            'index'     => 'email'
        ));
        $this->addColumn('store_id', array(
            'header'    => Mage::helper('avid_abandoned_cart_reminders')->__('Store'),
            'index'     => 'store_id',
            'renderer'  => 'Avid_AbandonedCartReminders_Block_Adminhtml_Renderer_StoreLabel',
            'type'      => 'options',
            'options'   => $storeOptions,
        ));
        $this->addColumn('last_sent_at', array(
            'header'    => Mage::helper('avid_abandoned_cart_reminders')->__('Last Sent'),
            'index'     => 'last_sent_at',
            'type'      => 'datetimeloc',
        ));
        return parent::_prepareColumns();
    }

    /**
     * Get Grid URL for use in AJAX requests
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->_getData('grid_url') ? $this->_getData('grid_url') : $this->getUrl('*/cartreminders_history/grid', array('_current'=>true));
    }

    /**
     * Get Reminder edit URL for use when clicking on a row.
     *
     * @param Avid_AbandonedCartReminders_Reminder $item
     * @return string
     */
    public function getRowUrl($item)
    {
        //return $this->getUrl('*/cartreminders_history/view', array('reminder_id' => $item->getId()));
    }
}