<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Block_Adminhtml_Reports_Statistic_Graph
 *
 * Block for rendering statistics graphs and JSON data to insert into existing graphs.
 */
class Avid_AbandonedCartReminders_Block_Adminhtml_Reports_Statistic_Graph extends Mage_Core_Block_Template
{
    //Array of statistic objects to be rendered in the graph
    protected $_statistics = array();

    //Whether the data has been collated into headers and rows.
    protected $_collated = false;

    /**
     * Set Template on construct.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('avid/abandoned_cart_reminders/reports/statistic/graph.phtml');
    }

    /**
     * Add Statistic to be displayed.
     *
     * @param Avid_AbandonedCartReminders_Model_Statistic_Abstract $statistic
     * @return $this
     */
    public function addStatistic($statistic)
    {
        $this->_statistics[] = $statistic;
        return $this;
    }

    /**
     * Get Array of Statistic Objects to Display
     * @return array
     */
    public function getStatistics()
    {
        return $this->_statistics;
    }

    /**
     * Collate the statistic objects' data into header, rows and columns for Google DataTable
     * @return $this
     */
    public function collate()
    {
        if (!$this->_collated) {
            $rows = array();
            $headers = array();
            $_x = 0;
            foreach ($this->getStatistics() as $_statistic) {
                $headers[$_statistic->getScopeKey()] = $_statistic;
                foreach ($_statistic->getValues() as $_key => $_value)
                    $rows[$_key][$_x] = array(
                        'v' => (float)$_value,
                        'f' => $_statistic->formatValue($_value),
                    );
                $_x++;
            }
            $this->setHeaders($headers);
            $this->setRows($rows);
        }
        return $this;
    }

    /**
     * Get JSON representation of Google DataTable
     *
     * @return string
     */
    public function getJSON()
    {
        return json_encode($this->getTableArray());
    }

    /**
     * Get Array representation of DataTable
     *
     * @return array
     */
    public function getTableArray()
    {
        $this->collate();
        $table = array(
            'cols' => array(
                array('id' => 'date', 'label' => 'Day', 'type' => 'date')
            ),
            'rows' => array(),
            'p' => array('vizOptions' => array()),
        );
        $series = array();
        $_x = 0;
        foreach ($this->getHeaders() as $_code => $_statistic) {
            $table['cols'][] = array('id' => $_code, 'label' => $_statistic->getLabel(), 'type' => 'number');
            $series[$_x] = array('targetAxisIndex' => (int)($_statistic->getValueType() == 'currency'));
            $_x++;
        }
        foreach ($this->getRows() as $_key => $_values) {
            $_columnData = array($this->_getTimeColumnArray($_key));
            foreach ($_values as $_valueArray)
                $_columnData[] = $_valueArray;
            $table['rows'][] = array(
                'c' => $_columnData
            );
        }
        $table['p']['vizOptions']['series'] = $series;
        return $table;
    }

    /**
     * Get Time Column Array
     *
     * Gets array data of value, formatted value for a time series axis, with Google API representation of JS date
     * object
     *
     * @param string $timeString
     * @return array
     */
    protected function _getTimeColumnArray($timeString)
    {
        $date = getdate(strtotime($timeString));
        $month = (int)$date['mon']-1;
        $value = "Date({$date['year']}, {$month}, {$date['mday']})";
        $label = Mage::helper('core')->formatDate($timeString);
        return array(
            'v' => $value,
            'f' => $label,
        );
    }
}