<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Block_Adminhtml_Send
 *
 * Blcok that contains the grid of all reminders to send, as well as key action buttons.
 */
class Avid_AbandonedCartReminders_Block_Adminhtml_History extends Mage_Adminhtml_Block_Widget_Container
{
    /**
     * Construct Block
     *
     * Set Template, controller. Add buttons.
     */
    public function __construct()
    {
        $days = (int)Mage::getStoreConfig('avid_abandoned_cart_reminders/background/create_sequence_cron_days')+2;
        $cleanCount = Mage::getModel('avid_abandoned_cart_reminders/reminder')->getCollection()->addToCleanFilter()->count();
        $this->_controller = 'cartreminders_send';
        $this->_addButton('clean', array(
            'label'   => Mage::helper('catalog')->__('Clean out old history'),
            'onclick' => "setLocation('{$this->getUrl('*/*/clean')}')",
            'class'   => 'delete',
            'before_html' => "<span style=\"margin-right:18px\">{$cleanCount} Reminders to clean from database, up to {$days} ago. Statistics will be collected and saved first</span>"
        ));
        $this->setTemplate('widget/grid/container.phtml');
        parent::__construct();
    }

    /**
     * Prepare Layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->setChild( 'grid', $this->getLayout()->createBlock('avid_abandoned_cart_reminders/adminhtml_history_grid',$this->_controller . '.grid')->setSaveParametersInSession(true) );
        return Mage_Adminhtml_Block_Widget_Container::_prepareLayout();
    }

    /**
     * Get Header Text
     *
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper('avid_abandoned_cart_reminders')->__('View Reminder Email Send history');
    }

    /**
     * Render grid
     *
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getChildHtml('grid');
    }
}