<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Block_Adminhtml_Renderer_StepLabel
 *
 * Grid column renderer for step label.
 */
class Avid_AbandonedCartReminders_Block_Adminhtml_Renderer_StepLabel extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Render
     *
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        $id =  $row->getData($this->getColumn()->getIndex());
        $storeId = $row->getData('store_id');
        $step = $row->getScheduler()->getStep($id, $storeId);
        return $step['label'];
    }
}