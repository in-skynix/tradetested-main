<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Block_Adminhtml_Reports_Statistic_Detail
 *
 * Block for rendering details on a specific statistic.
 */
class Avid_AbandonedCartReminders_Block_Adminhtml_Reports_Statistic_Detail extends Mage_Core_Block_Template
{
    /**
     * Set Template on construct.
     */
    public function __construct()
    {
        if (!$this->getTemplate())
            $this->setTemplate('avid/abandoned_cart_reminders/reports/statistic/detail.phtml');
    }
}