<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Block_Adminhtml_Reminder_View
 *
 * Block for previewing reminder email before send.
 */
class Avid_AbandonedCartReminders_Block_Adminhtml_Reminder_View extends Mage_Core_Block_Template
{
    /**
     * @var Mage_Core_Model_Email_Template
     */
    protected $_emailTemplate;
    /**
     * @var Avid_AbandonedCartReminders_Model_Reminder
     */
    protected $_reminder;

    /**
     * Get Reminder
     *
     * The reminder that we will be displaying the email for.
     *
     * @return Avid_AbandonedCartReminders_Model_Reminder|mixed
     */
    public function getReminder()
    {
        if (!$this->_reminder)
            $this->_reminder = Mage::registry('avid_abandoned_cart_reminders.reminder');
        return $this->_reminder;
    }

    /**
     * Get Email Content
     *
     * The body HTML for the reminder email.
     *
     * @return string HTML Email content.
     */
    public function getEmailContent()
    {
        $reminder = $this->getReminder();
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($reminder->getStoreId());
        $emailTemplate = $this->_getEmailTemplate();
        $html = $emailTemplate->setDesignConfig(array('area'=>'frontend', 'store' => $reminder->getStoreId()))
            ->getProcessedTemplate($reminder->getEmailVariables());
        $this->getReminder()->save(); //save promo data into it.
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        return $html;
    }

    /**
     * Get Email Subject
     *
     * @return string
     */
    public function getEmailSubject()
    {
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($this->getReminder()->getStoreId());
        $emailTemplate = $this->_getEmailTemplate();
        $subject = $emailTemplate->setDesignConfig(array('area'=>'frontend', 'store' => $this->getReminder()->getStoreId()))->getProcessedTemplateSubject($this->getReminder()->getEmailVariables());
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        return $subject;
    }

    /**
     * Get Email Template Object
     *
     * @return Mage_Core_Model_Email_Template
     */
    protected function _getEmailTemplate()
    {
        if (!$this->_emailTemplate) {
            $storeId = $this->getReminder()->getStoreId();
            $templateId = $this->getReminder()->getStepTemplate();
            $emailTemplate = Mage::getModel('core/email_template')
                ->setTemplateFilter(Mage::getModel('avid_abandoned_cart_reminders/reminder_email_template_filter')->setReminder($this->getReminder()))
                ->setDesignConfig(array('area'=>'frontend', 'store' => $storeId));
            if (is_numeric($templateId)) {
                $emailTemplate = $emailTemplate->load($templateId);
            } else {
                $localeCode = Mage::getStoreConfig('general/locale/code', $storeId);
                $emailTemplate = $emailTemplate->loadDefault($templateId, $localeCode);
            }
            $this->_emailTemplate = $emailTemplate;
        }
        return $this->_emailTemplate;
    }
}