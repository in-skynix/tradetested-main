<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Block_Adminhtml_Send
 *
 * Blcok that contains the grid of all reminders to send, as well as key action buttons.
 */
class Avid_AbandonedCartReminders_Block_Adminhtml_Send extends Mage_Adminhtml_Block_Widget_Container
{
    /**
     * Construct Block
     *
     * Set Template, controller. Add buttons.
     */
    public function __construct()
    {
        $this->_controller = 'cartreminders_send';
        $this->_addButton('send_now', array(
            'label'   => Mage::helper('catalog')->__('Send All Reminders that are ready'),
            'onclick' => "setLocation('{$this->getUrl('*/*/sendReady')}')",
            'class'   => 'go'
        ));
        $this->_addButton('create_test', array(
            'label'   => Mage::helper('catalog')->__('Create Test Reminder'),
            'onclick' => "setLocation('{$this->getUrl('*/*/createTest')}')",
        ));
        $this->setTemplate('widget/grid/container.phtml');
        parent::__construct();
    }

    /**
     * Prepare Layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->setChild( 'grid', $this->getLayout()->createBlock('avid_abandoned_cart_reminders/adminhtml_send_grid',$this->_controller . '.grid')->setSaveParametersInSession(true) );
        return Mage_Adminhtml_Block_Widget_Container::_prepareLayout();
    }

    /**
     * Get Header Text
     *
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper('avid_abandoned_cart_reminders')->__('Send/View reminder emails');
    }

    /**
     * Render grid
     *
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getChildHtml('grid');
    }
}