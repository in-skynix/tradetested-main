<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Helper_Link
 *
 * Helper for encoding, decoding, and tagging reminder email links. Google Analytics Tags
 */
class Avid_AbandonedCartReminders_Helper_Link extends Mage_Core_Helper_Abstract
{
    /**
     * Extract Link Data from encoded string
     * @param string $hash The encoded string.
     * @return array Link Data
     */
    public function extractLinkData($hash)
    {
        $data = (array)json_decode(base64_decode($hash));
        $data['tagged_url'] = $this->_tagUrl($data);
        return $data;
    }

    /**
     * Create Link with encoded string.
     *
     * @param string $url The URL this link will redirect to.
     * @param Avid_AbandonedCartReminders_Model_Reminder $reminder The reminder to tag the link for.
     * @return Mage_Core_Model_Url The link URL
     */
    public function createLink($url, $reminder)
    {
        if (!$url)
            die('no url');
        $hash =  base64_encode(json_encode(array(
            'url' => $url,
            'reminder_id' => $reminder->getId(),
            'step_id' => $reminder->getScheduledStepId(),
            'reminder_key' => Mage::getModel('catalog/product_url')->formatUrlKey($reminder->getLabel())
        )));
        return Mage::getModel('core/url')->setStore($reminder->getStoreId())->getUrl('avid_abandoned_cart_reminders/link/redirect', array('l' => $hash));
    }

    /**
     * Tag URL
     * @param Array $data Link Data
     * @return string URL to redirect to with Google Analytics Tags.
     */
    protected function _tagUrl($data)
    {
        try{
            $tags = array(
                'utm_source' => Mage::getStoreConfig('avid_abandoned_cart_reminders/reports/utm_source'),
                'utm_medium' => Mage::getStoreConfig('avid_abandoned_cart_reminders/reports/utm_medium'),
                'utm_campaign' => $data['reminder_key'],
                'utm_term' => $data['reminder_id'],
            );
            $join = parse_url($data['url'], PHP_URL_QUERY) ? '&' : '?';
            return $data['url'].$join.http_build_query($tags);
        } catch (Exception $e) {
            return $data['url'];
        }
    }
}