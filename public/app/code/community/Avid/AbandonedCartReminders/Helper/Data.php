<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Helper_Data
 *
 * Core Helper for Avid_AbandonedCartReminders
 */
class Avid_AbandonedCartReminders_Helper_Data extends Mage_Core_Helper_Data
{
    public function saveReminderForObject($object)
    {
        if ($reminder = $this->getReminderForObject($object)) {
            return $reminder
                ->setObjectDetails($object)
                ->save();
        }
    }

    public function getReminderForObject($object)
    {
        return Mage::getModel('avid_abandoned_cart_reminders/reminder')->loadByObject($object);
    }

    /**
     * Get Code for the event type which should end the sequence.
     * @return string
     */
    public function getEventTypeToEndSequence()
    {
        return Mage::getStoreConfig('avid_abandoned_cart_reminders/general/end_sequence_event_type');
    }

    /**
     * Is login required if the reminder has a customer account.
     *
     * @return bool
     */
    public function requireLogin()
    {
        return Mage::getStoreConfig('avid_abandoned_cart_reminders/general/require_login');
    }

    /**
     * Is it permitted to copy entire quote data?
     *
     * @return bool
     */
    public function allowCopyQuoteDetails()
    {
        return Mage::getStoreConfig('avid_abandoned_cart_reminders/general/allow_copy_quote_details');
    }

    /**
     * Get Statistic Model from Code
     *
     * @param string $code
     * @return Avid_AbandonedCartReminders_Model_Statistic_Abstract
     */
    public function getStatistic($code)
    {
        return Mage::getModel(Mage::getConfig()->getNode("global/avid/abandoned_cart_reminders/statistics/{$code}/model"));
    }

    /**
     * Get All Registered Statistic Models.
     *
     * @return array
     */
    public function getAllStatistics()
    {
        $statistics = array();
        foreach ($this->getAllStatisticCodes() as $_code)
            $statistics[] = $this->getStatistic($_code);
        return $statistics;
    }

    /**
     * Get All Registered Statistic Codes
     *
     * @return array
     */
    public function getAllStatisticCodes()
    {
       return array_keys(Mage::getConfig()->getNode("global/avid/abandoned_cart_reminders/statistics")->asArray());
    }

    /**
     * Register the From date for statistics on this page
     *
     * @param string $date
     * @return $this
     */
    public function setStatisticFromDate($date)
    {
        Mage::register('avid_abandoned_cart_reminders.statistics.from_date', strtotime($date));
        return $this;
    }

    /**
     * Register the To date for statistics on this page
     *
     * @param $date
     * @return $this
     */
    public function setStatisticToDate($date)
    {
        Mage::register('avid_abandoned_cart_reminders.statistics.to_date', strtotime($date));
        return $this;
    }

    /**
     * Get the From date for statistics on this page.
     *
     * @return string
     */
    public function getStatisticFromDate()
    {
        if (!($date = Mage::registry('avid_abandoned_cart_reminders.statistics.from_date')))
            $date = strtotime("-31 days");
        return date("Y-m-d", $date);
    }

    /**
     * Get the to date for statistics on this page.
     *
     * @return string
     */
    public function getStatisticToDate()
    {
        if (!($date = Mage::registry('avid_abandoned_cart_reminders.statistics.to_date')))
            $date = strtotime("-1 day");
        return date("Y-m-d", $date);
    }

    /**
     * Set Active Reminder when an email link is clicked.
     *
     * @param Avid_AbandonedCartReminders_Model_Reminder $reminder
     * @return $this
     */
    public function setActiveReminder($reminder)
    {
        $reminder->stickPromo();
        Mage::getSingleton('core/session')->setAvidAbandonedCartRemindersActiveReminderId($reminder->getId());
        return $this;
    }

    /**
     * Get Active Reminder (the last one clicked on for the session)
     *
     * @return Avid_AbandonedCartReminders_Model_Reminder
     */
    public function getActiveReminder()
    {
        if ($reminderId = Mage::getSingleton('core/session')->getAvidAbandonedCartRemindersActiveReminderId())
            return Mage::getModel('avid_abandoned_cart_reminders/reminder')->load($reminderId)->setIsActive(true);
        return Mage::getModel('avid_abandoned_cart_reminders/reminder');
    }

    /**
     * Get Test Mode Email.
     *
     * If test mode is enabled, return the configured email. REturns false if test mode is off.
     */
    public function getTestModeEmail()
    {
        if (!Mage::getStoreConfig('avid_abandoned_cart_reminders/test_mode/enabled'))
            return false;
        $email =  ($v = Mage::getStoreConfig('avid_abandoned_cart_reminders/test_mode/email')) ? $v :
            Mage::getStoreConfig('contacts/email/recipient_email');
        if (!$email)
            Mage::throwException('No Test email address configured to send to.');
        return $email;

    }

    /**
     * Clean Old reminders
     *
     * Get Reminders already ended/cleaned and old enough to not be recreated, ensure their statistics buckets are
     * collected, and then delete them.
     *
     * @return $this
     */
    public function cleanOldReminders()
    {
        $collection = Mage::getModel('avid_abandoned_cart_reminders/reminder')->getCollection()
            ->addToCleanFilter();
        $earliest = $collection->getEarliestCreatedAt();
        if ($count = $collection->count())
            foreach ($this->getAllStatistics() as $_statistic)
                $_statistic->setFromDate($earliest)
                    ->setToDate(date("Y-m-d", strtotime('-1 day'))) //All reminders to clean will be at least 2 days old.
                    ->getBucketCollection();
        $collection->delete();
        return $count;
    }
}