<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Helper_Queue
 *
 * Helper for updating statuses of backgorund jobs.
 */
class Avid_AbandonedCartReminders_Helper_Queue extends Mage_Core_Helper_Abstract
{
    //File to read/write status to.
    protected $_file;

    //message to add.
    protected $_msg = null;

    /**
     * Initialise Queue Helper
     *
     * Get a lock on the status update file. Only allow one helper process at a time.
     *
     * @return $this
     */
    public function init()
    {
        if(!$this->_file) {
            $this->_file = fopen(Mage::getBaseDir('var').DS."avid_abandoned_cart_reminders_status", "w");
            if (!flock($this->_file, LOCK_EX | LOCK_NB))
                die("Process is already running\n");
        }
        return $this;
    }

    /**
     * Start the process
     */
    public function start()
    {
        //@todo: set running flag
        $this->init();

    }

    /**
     * End the process
     *
     * @return $this
     */
    public function end()
    {
        if(is_resource($this->_file))
            fclose($this->_file);
        //@todo unset running flag
        return $this;
    }

    /**
     * Set Latest Message
     *
     * @param string $msg
     * @return $this
     */
    public function setMsg($msg)
    {
        //@todo: persist message across requests
        $msg = $this->_getDate()." : ".$msg;
        $this->_msg = $msg;
        fwrite($this->_file,  $msg."\n");
        return $this;
    }

    /**
     * Get Latest Message
     *
     * @return string Message
     */
    public function getMsg()
    {
        if(!$this->_msg)
            $this->_msg = $this->_tail();
        return $this->_msg;
    }

    /**
     * Get current Date
     *
     * @return string
     */
    protected function _getDate()
    {
        return Mage::getSingleton('core/date')->date();
    }

    /**
     * Tail the status file
     *
     * @param int $n Lines to tail
     * @return string
     */
    protected function _tail($n = 1) {
        $file = escapeshellarg(Mage::getBaseDir('var').DS."avid_abandoned_cart_reminders_status");
        return `tail -n $n $file`;
    }

    /**
     * Start Background process to send emails.
     *
     * @return $this
     */
    public function spawn()
    {
        $foo = '';
        $cmd = 'php '.Mage::getBaseDir().DS.'shell'.DS.'abandoned_carts.php --send_reminders &';
        proc_close(proc_open($cmd, Array (), $foo));
        return $this;
    }

}