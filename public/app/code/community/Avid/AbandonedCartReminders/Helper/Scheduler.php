<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Helper_Scheduler
 *
 * Helper for scheduling reminder emails.
 */
class Avid_AbandonedCartReminders_Helper_Scheduler extends Mage_Core_Helper_Abstract
{
    //Array of step data used in schedules.
    protected $_steps = array();

    protected $_reminder;

    /**
     * Get All Step Data
     *
     * @param null|int $storeId
     * @return Array Step Data
     */
    protected function _getSteps($storeId = null)
    {
        if (!(isset($this->_steps[$storeId][$this->getScheduleCode()]) && $this->_steps[$storeId][$this->getScheduleCode()]))
            $this->_steps[$storeId][$this->getScheduleCode()] = Mage::registry('avid_abandoned_cart_reminders.steps.store'.$storeId.'.type'.$this->getScheduleCode());
        if (!$this->_steps[$storeId][$this->getScheduleCode()]) {
            $fieldName = ($this->getScheduleCode() == 'quote') ? 'steps' : 'steps_'.$this->getScheduleCode();
            $config = json_decode(Mage::getStoreConfig('avid_abandoned_cart_reminders/sequencing/'.$fieldName, $storeId), true);
            $wait = array();
            if (!is_array($config))
                Mage::throwException('Cannot find steps config for this sequence: '.$this->getScheduleCode());
            foreach ($config as $key => $row)
                $wait[$key] = $row['wait_minutes'];
            array_multisort($wait, SORT_ASC, $config);
            $this->_steps[$storeId][$this->getScheduleCode()] = $config;
            Mage::register('avid_abandoned_cart_reminders.steps.store'.$storeId.'.type'.$this->getScheduleCode(), $config);
        }
        return $this->_steps[$storeId][$this->getScheduleCode()];
    }

    /**
     * Get Schedule Code
     *
     * e.g. quote, wishlist
     * @return string
     */
    public function getScheduleCode()
    {
        return $this->getReminder()->getType();
    }

    /**
     * Set Reminder
     *
     * With the reminder set, we can get schedule data for this reminder. Different types of reminder may have different
     * schedules.
     *
     * @param $reminder
     * @return $this
     */
    public function setReminder($reminder)
    {
        $this->_reminder = $reminder;
        return $this;
    }

    /**
     * Get Reminder
     *
     * @return Avid_AbandonedCartReminders_Model_Reminder
     */
    public function getReminder()
    {
        if (!$this->_reminder)
            Mage::logException(new Exception('Reminder not set in Scheduler yet'));

        return $this->_reminder;
    }

    /**
     * Get Data for Single Step
     *
     * @param int $stepId
     * @param null|int $storeId
     * @return Array|null
     */
    public function getStep($stepId, $storeId = null)
    {
        $steps = $this->_getSteps($storeId);
        foreach ($steps as $_step)
            if ($_step['step_id'] == $stepId)
                return $_step;
        return null;
    }

    /**
     * Get Next Available Step For Reminder
     *
     * @param Avid_AbandonedCartReminders_Model_Reminder $reminder
     * @return null|Array
     */
    protected function _getNextAvailableStepFor($reminder)
    {
        //@todo: Refactor all steps in here to use set reminder, rather than pass reminder around, as there are multiple types now.
        $this->setReminder($reminder);

        $steps = $this->_getSteps($reminder->getStoreId());
        //if the scheduled_step_id still exists, simply assign the scheduled_at accordingly.
        foreach($steps as $_step)
            if ($_step['step_id'] == $reminder->getScheduledStepId())
                return $_step;

        //if the scheduled_step_id no longer exists, find the step after the last one we can verify it has been sent to
        $sentStepIds = $reminder->getSentStepIds();
        $lastSentId = 0;
        foreach ($steps as $_step)
            if (in_array($_step['step_id'], $sentStepIds))
                $lastSentId = $_step['step_id'];
        return $this->getStepAfter($lastSentId);
    }

    /**
     * Get Next Step after Step ID
     *
     * @param $stepId
     * @param null $storeId
     * @return null
     */
    public function getStepAfter($stepId, $storeId = null)
    {
        $steps = $this->_getSteps($storeId);
        if ($stepId == 0)
            return $steps[0];

        $nextKey = 0;
        foreach ($steps as $_key => $_step)
            if ($_step['step_id'] == $stepId)
                $nextKey = (int)$_key+1;
        return isset($steps[$nextKey]) ? $steps[$nextKey] : null;
    }

    /**
     * Reschedule all pending reminder emails
     *
     * @return $this
     */
    public function reflowAll()
    {
        foreach ($this->getAllIncompleteReminders() as $_reminder)
            $_reminder->scheduleStep($this->_getNextAvailableStepFor($_reminder))->save();
        return $this;
    }

    /**
     * Send all emails that are ready to be sent (scheduled for past)
     */
    public function sendAllReady()
    {
        $remindersToSend = $this->getRemindersToSend();
        foreach ($remindersToSend as $_reminder)
            $_reminder->send();
    }

    /**
     * Get collection of all reminders that are ready or in progress.
     *
     * @return Avid_AbandonedCartReminders_Model_Resource_Reminder_Collection
     */
    public function getAllIncompleteReminders()
    {
        return Mage::getModel('avid_abandoned_cart_reminders/reminder')->getCollection()
            ->addIncompleteFilter();
    }

    /**
     * Get Collection of all reminders that are ready to send.
     *
     * @return Avid_AbandonedCartReminders_Model_Resource_Reminder_Collection
     */
    public function getRemindersToSend()
    {
        $limit = Mage::getStoreConfig('avid_abandoned_cart_reminders/background/cron_batch_size');
        $collection =  Mage::getModel('avid_abandoned_cart_reminders/reminder')->getCollection()
            ->addReadyToSendFilter();
        $collection->getSelect()->limit($limit);
        return $collection;
    }
}