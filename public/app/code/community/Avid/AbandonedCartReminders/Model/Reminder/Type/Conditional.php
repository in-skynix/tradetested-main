<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 24/07/13
 * Time: 11:58 AM
 * To change this template use File | Settings | File Templates.
 */
class Avid_AbandonedCartReminders_Model_Reminder_Type_Conditional extends Avid_AbandonedCartReminders_Model_Reminder_Type_Quote
{
    protected $_typeCode = 'conditional';

    public function canApplyTo($object)
    {
        if (Mage::getStoreConfig('avid_abandoned_cart_reminders/conditional_sequence/enable', $object->getStoreId()) && ($object instanceof Mage_Sales_Model_Quote)) {
            $quote = $object;
            if ($quote->getSubtotal() < floatval(preg_replace("[^-0-9\.]","", Mage::getStoreConfig('avid_abandoned_cart_reminders/conditional_sequence/min_quote_subtotal', $object->getStoreId()))))
                return false;

            $emailAddress = $object->getCustomerEmail();
            if (!$emailAddress && ($address = $object->getBillingAddress()))
                $emailAddress = $address->getEmail();

            $telephone = $object->getBillingAddress()->getTelephone();

            switch(Mage::getStoreConfig('avid_abandoned_cart_reminders/conditional_sequence/require_fields', $object->getStoreId())) {
                case 'telephone':
                    return (boolean)$telephone;
                case 'email':
                    return (boolean)$emailAddress;
                case 'or':
                    return (boolean)($telephone || $emailAddress);
                case 'and':
                    return (boolean)($telephone && $emailAddress);
            }
        }
        return false;
    }

    public function getCustomerName()
    {
        return $this->getReminder()->getQuote()->getBillingAddress()->getFirstname()." ".$this->getReminder()->getQuote()->getBillingAddress()->getLastname();
    }
}