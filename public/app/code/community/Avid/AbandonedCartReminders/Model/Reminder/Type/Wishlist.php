<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 8/07/13
 * Time: 6:18 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_AbandonedCartReminders_Model_Reminder_Type_Wishlist extends Avid_AbandonedCartReminders_Model_Reminder_Type_Abstract
{
    protected $_typeCode = 'wishlist';

    public function getEmailVariables()
    {
        $wishlist = $this->getWishlist()->setShared(true)->save();
        $vars = parent::getEmailVariables();
        $vars['customer_wishlist_url'] = Mage::helper('wishlist')->getListUrl($wishlist);
        $vars['shared_wishlist_url'] = Mage::getUrl('wishlist/shared/index', array('code' => $wishlist->getSharingCode()));
        return $vars;
    }

    public function getCustomerName()
    {
        return  $this->getCustomer()->getFirstname();
    }

    public function getCustomer()
    {
        return Mage::getModel('customer/customer')->load($this->getWishlist()->getCustomerId());
    }

    public function getProductCollection()
    {
        if (!$this->_productCollection) {
            $productIds = array();
            foreach ($this->getWishlist()->getItemCollection() as $_item)
                $productIds[] = $_item->getProductId();
            $this->_productCollection = $this->loadProductCollectionFromIds($productIds);
        }
        return $this->_productCollection;
    }

    public function getWishlist()
    {
        return $this->getReminder()->getObject();
    }

    public function getRetrievalUrl()
    {
        return Mage::getUrl('wishlist/shared/allcart', array('code' => $this->getWishlist()->getSharingCode()));
    }

    public function setObjectDetails($wishlist)
    {
        $customer = Mage::getModel('customer/customer')->load($wishlist->getCustomerId());
        return $this->getReminder()
            ->setWishlistId($wishlist->getId())
            ->setAbandonedAt($wishlist->getUpdatedAt())
            ->setStoreId($wishlist->getStore()->getId())
            ->setEmail($customer->getEmail())
            ->save();
    }

    public function getObject()
    {
        return Mage::getModel('wishlist/wishlist')->load($this->getWishlistId());
    }

    public function getObjectKey()
    {
        return 'wishlist_id';
    }

    public function canApplyTo($object)
    {
        return (boolean)($object instanceof Mage_Wishlist_Model_Wishlist);
    }
}