<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Reminder_Event
 *
 * Reminder Event Log
 */
class Avid_AbandonedCartReminders_Model_Reminder_Event extends Mage_Core_Model_Abstract
{
    const CACHE_TAG     = 'avid_abandoned_cart_reminders.reminder_event';
    protected $_cacheTag= 'avid_abandoned_cart_reminders.reminder_event';

    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('avid_abandoned_cart_reminders/reminder_event');
    }

    /**
     * Set Create/Update times on Save
     *
     * @return Mage_Core_Model_Abstract|void
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $this->setUpdatedAt(date('Y-m-d H:i:s'));
        if (!$this->getId())
            $this->setCreatedAt(date('Y-m-d H:i:s'));
    }

    /**
     * Set Extra Event Data
     * @param Array $data
     * @return $this
     */
    public function setEventData($data)
    {
        return parent::setEventData(serialize($data));
    }

    /**
     * Get Extra Event Data
     * @return Array
     */
    public function getEventData()
    {
        return unserialize(parent::getEventData());
    }
}