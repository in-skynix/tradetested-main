<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Reminder_Email_Template_Filter
 *
 * custom Email Template Filter for Processing Links in the email.
 */
class Avid_AbandonedCartReminders_Model_Reminder_Email_Template_Filter extends Mage_Core_Model_Email_Template_Filter
{
    /**
     * @var Avid_AbandonedCartReminders_Model_Reminder
     */
    protected $_reminder;
    /**
     * @var Avid_AbandonedCartReminders_Helper_Link
     */
    protected $_helper;

    /**
     * Process Template
     *
     * @param string $value
     * @return mixed|string
     */
    public function filter($value)
    {
        try {
            $this->_helper = Mage::helper('avid_abandoned_cart_reminders/link');
            $value = $this->_processLinks(parent::filter($value));
        } catch (Exception $e) {
            $value = '';
            Mage::logException($e);
        }
        return $value;
    }

    /**
     * Set reminder into this
     *
     * so that its data can be later used in processing
     *
     * @param Avid_AbandonedCartReminders_Model_Reminder $reminder
     * @return $this
     */
    public function setReminder($reminder)
    {
        $this->_reminder = $reminder;
        return $this;
    }

    /**
     * Find all HTML links in the email, and replace them with links to the Link Controller with Link Data in an
     * encoded string
     * @param string $html Email Content
     * @return string Email Content
     */
    protected function _processLinks($html)
    {
        return $html;
        return preg_replace_callback('/<a(.*)href="([^"]*)"(.*)>/',array($this, '_processLinkMatch'),$html);
    }

    /**
     * Get new HTML for regext match in _processLinks.
     *
     * @param Array $matches
     * @return string HTML Link
     */
    protected function _processLinkMatch($matches)
    {
        return ($matches[2]) ? '<a'.$matches[1].'href="'.$this->_helper->createLink($matches[2], $this->_reminder).'"'.$matches[3].'>' : $matches[0]; //Don't replace the tag if there is no URL.
    }
}