<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 8/07/13
 * Time: 6:10 PM
 * To change this template use File | Settings | File Templates.
 */
abstract class Avid_AbandonedCartReminders_Model_Reminder_Type_Abstract extends Mage_Core_Model_Abstract
{
    /**
     * Abstract Type Instance Class for Reminder
     *
     * @method Avid_AbandonedCartReminders_Model_Reminder getReminder()
     * @method $this setReminder(Avid_AbandonedCartReminders_Model_Reminder $reminder)
     */

    /**
     * @var Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $_productCollection;

    protected $_productHtmlTemplate = 'avid/abandoned_cart_reminders/email/products.phtml';

    protected $_typeCode = '';

    /**
     * Get Email Variables
     *
     * @return array
     */
    public function getEmailVariables()
    {
        $reminder = $this->getReminder();
        $productsHtml = Mage::app()->getLayout()->getBlockSingleton('avid_abandoned_cart_reminders/email_products')
            ->setArea('frontend')
            ->setTemplate($this->_productHtmlTemplate)
            ->setProducts($this->getProductCollection())
            ->toHtml();
        return array(
            'reminder'              => $reminder,
            'products'              => $this->getProductCollection(),
            'products_html'         => $productsHtml,
            'coupon_code'           => Mage::helper('core')->escapeHtml($reminder->getCouponCode()),
            'coupon_expire_days'    => Mage::helper('core')->escapeHtml($reminder->getCouponExpireDays()),
            'retrieve_url'          => $this->getRetrievalUrl(),
            'customer_name'         => Mage::helper('core')->escapeHtml($this->getCustomerName()),
            'has_sticky_promo'      => $reminder->hasStickyPromo(),
            'store_name'            => Mage::app()->getStore($this->getStoreId())->getFrontendName(),
        );
    }

    abstract function canApplyTo($object);
    abstract function setObjectDetails($object);
    abstract function getObjectKey();
    abstract function getObject();

    public function getTypeCode()
    {
        return $this->_typeCode;
    }

    public function getCustomerName()
    {
        $name = $this->getReminder()->getQuote()->getCustomerFirstname();
        return $name ? $name : $this->getReminder()->getQuote()->getBillingAddress()->getFirstname();
    }

    public function getProductCollection()
    {
        $reminder = $this->getReminder();
        if (!$this->_productCollection) {
            $productIds = array();
            foreach ($reminder->getQuote()->getAllItems() as $_item)
                $productIds[] = $_item->getProductId();
            $this->_productCollection = $this->loadProductCollectionFromIds($productIds);
        }
        return $this->_productCollection;
    }
    
    public function isValid()
    {
        return true;
    }

    public function loadProductCollectionFromIds($productIds)
    {
        $reminder = $this->getReminder();
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->setStore($reminder->getStoreId())
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addIdFilter($productIds)
        ;
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
        return $collection;
    }

    /**
     * Get Retrieval URL
     *
     * @return string
     */
    public function getRetrievalUrl()
    {
        $reminder = $this->getReminder();
        return Mage::app()->getStore($reminder->getStoreId())->getUrl('avid_abandoned_cart_reminders/retrieve/quote', array('id' => $reminder->getId(), 'code' => $reminder->getRetrievalCode()));
    }
}