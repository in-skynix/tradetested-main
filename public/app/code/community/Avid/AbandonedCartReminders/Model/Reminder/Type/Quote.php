<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 8/07/13
 * Time: 6:15 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_AbandonedCartReminders_Model_Reminder_Type_Quote extends Avid_AbandonedCartReminders_Model_Reminder_Type_Abstract
{
    protected $_typeCode = 'quote';
    protected $_quote;

    public function getEmailVariables()
    {
        $vars = parent::getEmailVariables();
        $vars['quote'] = $this->getReminder()->getQuote();
        return $vars;
    }

    public function setObjectDetails($quote)
    {
        $emailAddress = $quote->getCustomerEmail();
        if (!$emailAddress && ($address = $quote->getBillingAddress()))
            $emailAddress = $address->getEmail();
        if (!$emailAddress) return;

        return $this->getReminder()
            ->setQuoteId($quote->getId())
            ->setAbandonedAt($quote->getUpdatedAt())
            ->setStoreId($quote->getStoreId())
            ->setEmail($emailAddress); //If quote has an email copy it over, if not we can still create a sequence where the email comes from another source.
    }

    public function getObject()
    {
        if (!$this->_quote) {
            $this->_quote = Mage::getModel('sales/quote')->loadByIdWithoutStore($this->getReminder()->getQuoteId());
        }
        return $this->_quote;
    }

    public function getObjectKey()
    {
        return 'quote_id';
    }

    public function canApplyTo($object)
    {
        if ($object instanceof Mage_Sales_Model_Quote) {
            $emailAddress = $object->getCustomerEmail();
            if (!$emailAddress && ($address = $object->getBillingAddress()))
                $emailAddress = $address->getEmail();
            if ($emailAddress)
                return true;
        }
        return false;
    }
    
    public function isValid()
    {
        $order = Mage::getModel('sales/order')->load($this->getObject()->getId(), 'quote_id');
        if ($order && $order->getId()) {
            return false;
        }
        return parent::isValid();
    }
}