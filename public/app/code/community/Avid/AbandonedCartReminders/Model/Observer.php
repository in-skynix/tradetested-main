<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
class Avid_AbandonedCartReminders_Model_Observer
{
    /**
     * Check Quote Save
     *
     * If the quote has new/change email. create/save a reminder for it.
     *
     * @param $observer
     */
    public function checkQuoteSave($observer)
    {
        try {
            $event = $observer->getEvent();
            $quote = $event->getQuote();
            $reminder = Mage::helper('avid_abandoned_cart_reminders')->saveReminderForObject($quote);

            //Clean any existing reminders for same email address as quote.
            if ($reminder) {
                $emailAddress = $quote->getBillingAddress()->getEmail();
                $remindersToClean = Mage::getModel('avid_abandoned_cart_reminders/reminder')->getCollection()
                    ->addFieldToFilter('reminder_id', array('neq' => $reminder->getId()))
                    ->addFieldToFilter('email', $emailAddress)
                    ->addFieldToFilter('status', 'ready');

                foreach ($remindersToClean as $_reminder) {
                    $_reminder->clean();
                }
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * Check Order Submit
     *
     * Converting a quote to order, see if we should mark a conversion, and/or clean a reminder that should no longer
     * need to be sent.
     *
     * @param $observer
     */
    public function checkOrderSubmit($observer)
    {
        try {
            $event = $observer->getEvent();
            /** @var Mage_Sales_Model_Quote $quote */
            $quote = $event->getQuote();
            //Reminder attached to quote. If we've retrieved it, mark as converted, if not, we just clean it.
            $reminder = Mage::helper('avid_abandoned_cart_reminders')->getReminderForObject($quote);

            if (!$reminder)
                return;

            if ($reminder->getRetrieveCount() && !$reminder->getIsConverted()) {
                $reminder->processReminderEvent('convert', array('value_number' => $quote->getItemsQty(), 'value_currency' => $quote->getGrandTotal()), $reminder->getLastStepId());
            } else {
                $reminder->clean();
            }

            //Clean any reminders for same email address as quote.
            $emailAddress = $quote->getBillingAddress()->getEmail();
            $remindersToClean = Mage::getModel('avid_abandoned_cart_reminders/reminder')->getCollection()
                ->addFieldToFilter('email', $emailAddress)
                ->addFieldToFilter('status', 'ready');
            foreach ($remindersToClean as $_reminder) {
                $_reminder->clean();
            }

            //Reminder set as active by clikcing on a link in the reminder email. We mark it as converted if not already converted.
            if(($activeReminder = Mage::helper('avid_abandoned_cart_reminders')->getActiveReminder()) && ($activeReminder->getId() != $reminder->getId()) && !$activeReminder->getIsConverted() && $activeReminder->getId()) {
                $activeReminder->processReminderEvent('convert', array('value_number' => $quote->getItemsQty(), 'value_currency' => $quote->getGrandTotal()), $reminder->getLastStepId());
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * When steps are edited, we need to reschedule all pending emails as the next step for them may have changed.
     * @param $observer
     */
    public function checkNewSettings($observer)
    {
        Mage::helper('avid_abandoned_cart_reminders/scheduler')->reflowAll();
    }

    public function wishlistAddProduct($observer)
    {
        try {
            $event = $observer->getEvent();
            $wishlist = $event->getWishlist();
            Mage::helper('avid_abandoned_cart_reminders')->saveReminderForObject($wishlist);
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
}