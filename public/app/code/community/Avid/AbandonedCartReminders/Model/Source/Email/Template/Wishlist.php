<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 24/07/13
 * Time: 2:51 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_AbandonedCartReminders_Model_Source_Email_Template_Wishlist extends Avid_AbandonedCartReminders_Model_Source_Email_Template
{
    //@todo: set up template types by XML, remove the hardcoding here.
    public function toOptionArray()
    {
        $options = parent::toOptionArray();
        foreach ($options as $_k => $_v)
            if (!$_v['value'])
                unset($options[$_k]);
        array_unshift($options,
            array(
                'value' => 'avid_abandoned_cart_reminders_wishlist',
                'label' => 'Default Wishlist Reminder Template'
            )
        );
        return $options;
    }
}