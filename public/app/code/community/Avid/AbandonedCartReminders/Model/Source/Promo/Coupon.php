<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 31/05/13
 * Time: 12:06 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_AbandonedCartReminders_Model_Source_Promo_Coupon
{
    /**
     * To Option Array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $values = array();
        $promos = Mage::getModel('salesrule/rule')->getCollection()
            ->addFieldToFilter('coupon_type', array('gt' => 1));
        foreach ($promos as $_promo)
            $values[] = array(
                'label' => $_promo->getName(),
                'value' => $_promo->getId()
            );
        return $values;
    }
}