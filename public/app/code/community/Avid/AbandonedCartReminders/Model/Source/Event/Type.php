<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Source_Event_Type
 *
 * Form Element for for which to select the event to end reminder sequence.
 */
class Avid_AbandonedCartReminders_Model_Source_Event_Type
{
    /**
     * To Option Array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'label' => 'Click Link on Email',
                'value' => 'click',
            ),
            array(
                'label' => 'Retrieve Cart Contents',
                'value' => 'retrieve',
            ),
            array(
                'label' => 'Purchase',
                'value' => 'convert',
            ),
        );
    }
}