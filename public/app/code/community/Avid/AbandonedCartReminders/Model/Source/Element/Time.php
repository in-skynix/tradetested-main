<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Source_Element_Time
 *
 * Time Select Form Element
 */
class Avid_AbandonedCartReminders_Model_Source_Element_Time extends Varien_Data_Form_Element_Time
{
    /**
     * Get Element HTML
     *
     * @return string
     */
    public function getElementHtml()
    {
        $this->addClass('select');
        $value_days = 0;
        $value_hours = 0;
        $value_minutes = 0;
        if( $value = $this->getValue() ) {
            $value_days = floor ($value / 1440);
            $value_hours = floor (($value - $value_days * 1440) / 60);
            $value_minutes = $value - ($value_days * 1440) - ($value_hours * 60);
        }
        $html = '<input type="hidden" id="' . $this->getHtmlId() . '" />';
        $html .= '<select name="'. $this->getName() . '" '.$this->serialize($this->getHtmlAttributes()).' id="'.$this->getName().'_days" style="width:60px">'."\n";
        for( $i=0;$i<365;$i++ )
            $html.= '<option value="'.$i.'" '. ( ($value_days == $i) ? 'selected="selected"' : '' ) .'>' . $i . '</option>';
        $html.= '</select><label for="'.$this->getName().'_days"> days, </label>'."\n";
        $html.= '<select name="'. $this->getName() . '" '.$this->serialize($this->getHtmlAttributes()).' id="'.$this->getName().'_hours" style="width:50px">'."\n";
        for( $i=0;$i<24;$i++ )
            $html.= '<option value="'.$i.'" '. ( ($value_hours == $i) ? 'selected="selected"' : '' ) .'>' . $i . '</option>';
        $html.= '</select><label for="'.$this->getName().'_hours"> hours, </label>'."\n";
        $html.= '<select name="'. $this->getName() . '" '.$this->serialize($this->getHtmlAttributes()).' id="'.$this->getName().'_minutes" style="width:50px">'."\n";
        for( $i=0;$i<60;$i++ )
            $html.= '<option value="'.$i.'" '. ( ($value_minutes == $i) ? 'selected="selected"' : '' ) .'>' . $i . '</option>';
        $html.= '</select><label for="'.$this->getName().'_minutes"> minutes.</label>'."\n";
        $html.= $this->getAfterElementHtml();
        return $html;
    }
}