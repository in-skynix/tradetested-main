<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 31/05/13
 * Time: 12:06 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_AbandonedCartReminders_Model_Source_Promo_Sticky
{
    /**
     * To Option Array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $values = array();
        $promos = Mage::helper('core')->isModuleEnabled('Avid_StickyPromo') ?
            Mage::getModel('avid_sticky_promo/code')->getCollection() : array();
        foreach ($promos as $_promo)
            $values[] = array(
                'label' => $_promo->getCode(),
                'value' => $_promo->getId()
            );
        return $values;
    }
}