<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Source_Email_Template
 *
 * Email Template Select Form Element
 */
class Avid_AbandonedCartReminders_Model_Source_Email_Template extends Mage_Adminhtml_Model_System_Config_Source_Email_Template
{

}