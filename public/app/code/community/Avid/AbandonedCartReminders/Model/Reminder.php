<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Reminder
 *
 * Abandoned Cart Reminder. One Reminder per quote. Sent to numerous steps.
 */
class Avid_AbandonedCartReminders_Model_Reminder extends Mage_Core_Model_Abstract
{
    const CACHE_TAG     = 'avid_abandoned_cart_reminders.reminder';
    protected $_cacheTag= 'avid_abandoned_cart_reminders.reminder';

    /**
     * @var Avid_AbandonedCartReminders_Helper_Scheduler
     */
    protected $_scheduler;

    /**
     * @var Mage_Core_Model_Abstract
     */
    protected $_object;

    /**
     * @var Mage_Customer_Model_Customer
     */
    protected $_customer;

    protected $_coupon;

    protected $_typeInstance;

    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('avid_abandoned_cart_reminders/reminder');
    }

    /**
     * Get Scheduler
     *
     * Helper set to this reminder. Helper will use different schedules for different types of reminder.
     *
     * @return Avid_AbandonedCartReminders_Helper_Scheduler
     */
    public function getScheduler()
    {
        if (!$this->_scheduler)
            $this->_scheduler = Mage::helper('avid_abandoned_cart_reminders/scheduler')->setReminder($this);
        return $this->_scheduler;
    }

    public function getType()
    {
        if (!($type = parent::getType()))
            $this->setType($this->getTypeInstance()->getTypeCode());
        return $type;
    }

    protected function _getObjectKey()
    {
        return $this->getTypeInstance()->getObjectKey();
    }

    /**
     * Load By Object, e.g Quote, Wishlist
     * @param $object
     * @return $this|boolean
     */
    public function loadByObject($object)
    {
        $objectId = ($object instanceof Mage_Core_Model_Abstract) ? $object->getId() : $object;
        return ($instance = $this->_inferTypeInstance($object)) ? $this->load($objectId, $instance->getObjectKey())->setType($instance->getTypeCode()) : false; //setting the Type here to ensure it gets saved
    }

    /**
     * Scheudle a next step for this reminder.
     * @param $data
     * @return $this
     */
    public function scheduleStep($data)
    {
        if (!$data)
            return $this->complete();
        $time = date('Y-m-d H:i:s', strtotime($this->getAbandonedAt().' + '.$data['wait_minutes'].' minutes'));
        $this->setScheduledStepId($data['step_id'])
            ->setScheduledAt($time);
        return $this;
    }

    /**
     * Find and schedule the next step for this reminder.
     *
     * @return $this
     */
    public function scheduleNext()
    {
        $next = $this->getScheduler()->getStepAfter($this->getScheduledStepId(), $this->getStoreId());
        return $this->scheduleStep($next, $this->getStoreId());
    }

    /**
     * Load by retrieval code
     *
     * @param $code
     * @param $id
     * @return $this
     */
    public function loadByCode($code, $id)
    {
        $this->load($id);
        if ($this->getRetrievalCode() != $code)
            Mage::throwException('Reminder not available');
        return $this;
    }

    public function send()
    {
        // double-check the schedule time is passed.
        if (strtotime(now()) < strtotime($this->getScheduledAt()))
            return $this;
        //Ensure we haven't just sent an email to this person.
        $minInterval = Mage::getStoreConfig('avid_abandoned_cart_reminders/general/minimum_interval_minutes');
        if (strtotime($this->getLastSentAt()) > strtotime("-{$minInterval} minutes"))
            return $this;
        if (!$this->getTypeInstance()->isValid())
            return $this->clean();
        if (!$this->getTypeInstance()->getProductCollection()->count())
            return $this->clean();

        $step = $this->getScheduledStepData();


		//double check reminder has an ID, for integrity
		if(!isset($step['step_id']) || !$step['step_id'])
			return;


        $template = $step['template'] ? $step['template'] : 'avid_abandoned_cart_reminders_default';
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($this->getStoreId());
        $emailTemplate = Mage::getModel('core/email_template')
            ->setTemplateFilter(Mage::getModel('avid_abandoned_cart_reminders/reminder_email_template_filter')->setReminder($this))
            ->setDesignConfig(array('area'=>'frontend', 'store' => $this->getStoreId()))
            ->sendTransactional(
                $template,
                $this->getFrom(),
                $this->getEmailAddressToSendTo(),
                $this->getNameToSendTo(),
                $this->getEmailVariables()
            );
        $success = $emailTemplate->getSentSuccess();
        if ($emailBcc = Mage::getStoreConfig('avid_abandoned_cart_reminders/general/email_bcc')) {
            Mage::getModel('core/email_template') //Can't re-use template as causes erros with setting subject twice.
                ->setTemplateFilter(Mage::getModel('avid_abandoned_cart_reminders/reminder_email_template_filter')->setReminder($this))
                ->setDesignConfig(array('area'=>'frontend', 'store' => $this->getStoreId()))
                ->sendTransactional(
                    $template,
                    $this->getFrom(),
                    $emailBcc,
                    null,
                    $this->getEmailVariables()
                );
        }
        if ($success) {
            $this
                ->setLastSentAt(date('Y-m-d H:i:s'))
                ->scheduleNext()
                ->save();
            $this->processReminderEvent('send', array('step_id' => $step['step_id']), $step['step_id']);
        }
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
    }

    public function getEmailAddressToSendTo()
    {
        if ($v = Mage::helper('avid_abandoned_cart_reminders')->getTestModeEmail())
            return $v;

        $step = $this->getScheduledStepData();
        if (isset($step['send_to_override']) && $step['send_to_override'])
            return $step['send_to_override'];

        return $this->getEmail();
    }

    public function getNameToSendTo()
    {
        $step = $this->getScheduledStepData();
        if (isset($step['send_to_override']) && $step['send_to_override'])
            return null;
        return $this->getTypeInstance()->getCustomerName();
    }

    protected function _setPromo()
    {
        $step = $this->getScheduledStepData();
        if (isset($step['promo']) && $step['promo']) {
            $promoType = $step['promo'];
            if (strpos($promoType, ':') !== false)
                list($promoType, $id) = explode(':', $step['promo']);
            switch($promoType) {
                case 'sticky':
                    $this->_setStickyData($id);
                    break;
                case 'coupon':
                    $this->_createCoupon($id);
                break;
            }
        }
    }

    protected function _setStickyData($stickyId)
    {
        if ($stickyId) {
            //$sticky = Mage::getModel('avid_sticky_promo/code')->load($promo);
            $this->setData('promo_data', array(
                'sticky_id' => $stickyId,
                'sticky_time_attached' => Mage::getModel('core/date')->timestamp()
            ));
        }
    }

    protected function _createCoupon($ruleId)
    {
        $generator = Mage::getSingleton('salesrule/coupon_massgenerator')
            ->setLength(Mage::getStoreConfig('avid_abandoned_cart_reminders/coupons/length'));
        //Seems that magento will save coupon_type as coupon_type_specific, even when the rule is set up as auto.
        $rule = Mage::getModel('salesrule/rule')->load($ruleId)
            ->setCouponType(Mage_SalesRule_Model_Rule::COUPON_TYPE_AUTO);
        $rule->setCouponCodeGenerator($generator);
        $coupon = $rule->acquireCoupon(false);
        if (!$coupon) return;

        $coupon->setExpirationDate(date('Y-m-d', strtotime("+{$this->getCouponExpireDays()} days")))
            ->save();
        $this->setPromoData(array('coupon_id' => $coupon->getId()));
    }

    public function getCouponExpireDays()
    {
        return Mage::getStoreConfig('avid_abandoned_cart_reminders/coupons/expire_days');
    }

    public function getCouponCode()
    {
        return $this->getCoupon()->getCode();
    }

    public function getCoupon()
    {
        if (!$this->_coupon) {
            $data = $this->getPromoData();
            $id = isset($data['coupon_id']) ? $data['coupon_id'] : null;
            $this->_coupon = Mage::getModel('salesrule/coupon')->load($id);
        }
        return $this->_coupon;
    }

    public function stickPromo()
    {
        $data = $this->getPromoData();
        if (isset($data['sticky_id']) && isset($data['sticky_time_attached']) && ($stickyCode = Mage::getModel('avid_sticky_promo/code')->load($data['sticky_id']))) {
            $session = Mage::getModel('avid_sticky_promo/session');
            $session->addStickyCode($stickyCode);
            foreach ($session->getStickyCodes() as $_stickyCode)
                $_stickyCode->setData('time_stuck', $data['sticky_time_attached']);
        }
        return $this;
    }

    public function hasStickyPromo()
    {
        $data = $this->getPromoData();
        return (boolean)(isset($data['sticky_id']) && isset($data['sticky_time_attached']) && ($stickyCode = Mage::getModel('avid_sticky_promo/code')->load($data['sticky_id'])));
    }

    public function getTypeInstance()
    {
        if (!$this->_typeInstance) {
            if ($typeCode = $this->getData('type')) {
                //@todo: move from hardcoded to configuration of types in XML
                $this->_typeInstance = Mage::getModel('avid_abandoned_cart_reminders/reminder_type_'.$typeCode)->setReminder($this);
                return $this->_typeInstance;
            }
            Mage::throwException('Cannot find a type instance for this reminder');
        }
        return $this->_typeInstance;
    }

    protected function _inferTypeInstance($object)
    {
        $this->_typeInstance = null;
        $this->setType(null)->setObject($object);
        //@todo: move from hardcoded to configuration of types in XML
        foreach (array('wishlist', 'conditional', 'quote') as $_typeCode) {
            $instance = Mage::getModel('avid_abandoned_cart_reminders/reminder_type_'.$_typeCode);
            if ($instance->canApplyTo($this->_object)) {
                $this->_typeInstance = $instance->setReminder($this);
                return $this->_typeInstance;
            }
        }
    }

    public function getEmailVariables()
    {
        $this->_setPromo();
        $variables = new Varien_Object($this->getTypeInstance()->getEmailVariables());
        Mage::dispatchEvent('avid_abandoned_cart_reminders_get_variables', array('variables'=>$variables));
        return $variables->getData();
    }
    public function getFrom()
    {
        return Mage::getStoreConfig(Mage_Sales_Model_Order::XML_PATH_EMAIL_IDENTITY,Mage::app()->getStore()->getId());
    }

    public function processReminderEvent($eventType, $data = array(), $stepId = null)
    {
        if ($eventType == Mage::helper('avid_abandoned_cart_reminders')->getEventTypeToEndSequence())
            $this->complete()->setStatus('ended');
        if ($eventType == 'retrieve')
            $this->setRetrieveCount((int)$this->getRetrieveCount()+1)->save();
        if ($eventType == 'convert')
            $this->setIsConverted(true);

        Mage::getModel('avid_abandoned_cart_reminders/reminder_event')
            ->setData($data)
            ->setStepId($stepId)
            ->setReminderId($this->getId())
            ->setEventType($eventType)
            ->setIsFirstForReminder(!$this->_isEventFlagged($eventType))
            ->setIsFirstForStep(!$this->_isEventFlagged($eventType, $stepId))
            ->setEventData($data)
            ->save();
        $this->_flagEvent($eventType, $stepId);
        return $this->save();
    }

    //We keep a note of which event types this reminder has seen so that we can flag the events as the first, meaning
    //we can aggregate data that is unique to the reminder or step/reminder combo. Otherwise, we would not be able to
    //bucket the data for stats generation.
    protected function _flagEvent($eventType, $stepId)
    {
        $flaggedEvents = $this->getData('flagged_events') ? unserialize($this->getData('flagged_events')) : array();
        $flaggedEvents[$eventType][$stepId] = true;
        $this->setData('flagged_events', serialize($flaggedEvents));
        return $this;
    }

    protected function _isEventFlagged($eventType, $stepId = null)
    {
        $flaggedEvents = $this->getData('flagged_events') ? unserialize($this->getData('flagged_events')) : array();
        if (isset($flaggedEvents[$eventType]))
            return $stepId ? isset($flaggedEvents[$eventType][$stepId]): true;
        return false;
    }

    public function getScheduledStepData()
    {
        return $this->getScheduler()->getStep($this->getScheduledStepId());
    }

    public function complete()
    {
        return $this->setStatus('ended')->save();
    }

    /**
     * Retrieve To Cart.
     *
     * Try to automatically retrieve the cart. If the settings/situation requires the customer to either log in or
     * choose what to add to an existing cart, returns false. Otherwise ensures the cart has all the relevant products
     * so we could just redirect to cart.
     *
     * If login is required, will setIsRequiredLogin(true)
     *
     * @return bool Whether automatic retrieval could be completed or not.
     */
    public function retrieveToCart()
    {

        if ($quoteCustomerId = $this->getQuote()->getCustomerId()) {
            //The quote is attached ot a customer, so will be automatically available if the customer is logged in.
            $customerSession = Mage::getModel('customer/session');
            if($quoteCustomerId == $customerSession->getCustomerId()){
                //The customer is logged in.
            } elseif (Mage::helper('avid_abandoned_cart_reminders')->requireLogin()) {
                //The customer is not logged in and we need to request the customer logs in.
                $this->setIsRequiredLogin(true);
            } else {
                //The customer is not logged in, but the config allows us to automatically log the customer in.
                $customerSession->setCustomerAsLoggedIn($this->getQuote()->getCustomer());
            }
        } else {
            //The quote is not attached to a customer, we need to see if we already have the quote available or replace an empty one.
            $currentQuote = Mage::getSingleton('checkout/session')->getQuote();
            if ($this->getQuoteId() == $currentQuote->getId()) {
                //quote is already loaded.
            } elseif (!($currentQuote->getItemsCount()) && Mage::helper('avid_abandoned_cart_reminders')->allowCopyQuoteDetails()) {
                //There are no items in cart already. If config allows, we can just copy over the quote.
                Mage::getSingleton('checkout/session')->replaceQuote($this->getQuote());
            } else {
                $quote = $this->getQuote();
                //There are items in the current quote. we need to copy items over.
                //We're replacing the quote with the quote for this sequence so that conversion can be tracked / noted.
                foreach ($currentQuote->getAllItems() as $_item)
                    if(!$quote->hasProductId($_item->getProductId()))
                        $quote->addItem($_item->setId(null)->setQuoteId(null));
                $quote->collectTotals()->save();
                Mage::getSingleton('checkout/session')->replaceQuote($quote);
            }
        }
        if (!$this->getIsRequiredLogin())
            $this->processReminderEvent('retrieve');
    }

    /**
     * Clean this reminder when no longer needed.
     *
     * @return $this
     */
    public function clean()
    {
        $this->setStatus('cleaned')->save();
        return $this;
    }

    /**
     * Get Quote
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return $this->getObject();
    }

    public function setObject($object)
    {
        $this->_object = $object;
        return $this;
    }

    public function getObject()
    {
        if (!$this->_object)
            $this->_object = $this->getTypeInstance()->getObject();
        return $this->_object;
    }

    /**
     * Get Scheduled Step Label
     *
     * @return string
     */
    public function getStepLabel()
    {
        $step = $this->getScheduledStepData();
        return $step['label'];
    }

    /**
     * Get Scheduled Step Template Code
     *
     * @return int|string
     */
    public function getStepTemplate()
    {
        $step = $this->getScheduledStepData();
        return (isset($step['template']) && $step['template']) ? $step['template'] : 'avid_abandoned_cart_reminders_default';;
    }

    /**
     * Get Last Step ID
     *
     * @return int
     */
    public function getLastStepId()
    {
        if ($id = parent::getLastStepId())
            return $id;
        return (int)$this->getScheduledStepId()-1;
    }



    /**
     * Set Extra Data before Save.
     *
     * @return Mage_Core_Model_Abstract|void
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $this->setUpdatedAt(date('Y-m-d H:i:s'));
        if (!$this->getId()) {
            $this->scheduleNext();
            $this->setCreatedAt(date('Y-m-d H:i:s'));
            $this->setRetrievalCode(uniqid());
        }
        if (is_array($this->getData('promo_data')))
            $this->setData('promo_data', serialize($this->getData('promo_data')));
    }

    protected function _afterLoad()
    {
        parent::_afterLoad();
        if (!is_array($this->getData('promo_data')))
            $this->setData('promo_data', unserialize($this->getData('promo_data')));
    }

    public function getPromoData()
    {
        if (!is_array($this->getData('promo_data')))
            $this->setData('promo_data', unserialize($this->getData('promo_data')));
        return $this->getData('promo_data');
    }

    public function setObjectDetails($object)
    {
        $this->getTypeInstance()->setObjectDetails($object);
        return $this;
    }


}