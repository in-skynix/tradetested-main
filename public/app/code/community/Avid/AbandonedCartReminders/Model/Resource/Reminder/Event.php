<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Resource_Reminder_Event
 * Reminder Event Log Resource
 */
class Avid_AbandonedCartReminders_Model_Resource_Reminder_Event extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('avid_abandoned_cart_reminders/reminder_event', 'event_id');
    }

    /**
     * Get Read Adapter
     *
     * Used in Statistic Models for collection purposes.
     *
     * @return Varien_Db_Adapter_Interface
     */
    public function getReadAdapter()
    {
        return $this->_getReadAdapter();
    }
}