<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Resource_Statistic_Bucket
 *
 * Statistic Bucket Resource. Summary of value by time (day)
 */
class Avid_AbandonedCartReminders_Model_Resource_Statistic_Bucket extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('avid_abandoned_cart_reminders/statistic_bucket', 'bucket_id');
    }
}