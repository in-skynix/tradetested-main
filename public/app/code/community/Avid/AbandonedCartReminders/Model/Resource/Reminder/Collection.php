<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Resource_Reminder_Collection
 *
 * Collection of Reminders
 */
class Avid_AbandonedCartReminders_Model_Resource_Reminder_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected $_minCreatedAt;

    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('avid_abandoned_cart_reminders/reminder');
    }

    /**
     * Add filter for all sent to given step.
     *
     * @param bool $include
     * @param int $step
     * @return $this
     */
    public function addSentToFilter($include = true, $step)
    {
        if (!is_int($step)) $step = $step->getId();
        $cond = $include ? 'IS NOT NULL' : 'IS NULL';
        $this->getSelect()->joinLeft(array('event' => $this->getTable('avid_abandoned_carts/sequence_event')), "event.sequence_id = main_table.sequence_id AND event.step_id = {$step} AND event.event_type = 'send'", array())
            ->where("event.step_id {$cond}");
        return $this;
    }

    /**
     * Add filter to return only those reminders that are ready or in progress.
     * @return $this
     */
    public function addIncompleteFilter()
    {
        $this->addFieldToFilter('status', array('in' => array('ready', 'in_progress')));
        return $this;
    }

    /**
     * Add filter to return only those reminders that are ready or in progress.
     * @return $this
     */
    public function addHistoryFilter()
    {
        $this->addFieldToFilter('status', 'ended');
        return $this;
    }

    /**
     * Add filter to return only thos reminder that need cleaning
     *
     * Reminders must be ended or cleaned, and created long ago enough that they won't be recreated by a cron job.
     *
     * @return $this
     */
    public function addToCleanFilter()
    {
        $days = (int)Mage::getStoreConfig('avid_abandoned_cart_reminders/background/create_sequence_cron_days')+2;
        $this->addFieldToFilter('status', array('in' => array('ended', 'cleaned')))
            ->addFieldToFilter('created_at', array(
                'to'  => date("Y-m-d H:i:s", strtotime("-{$days} days")),
                'date'  => true
            ));
        return $this;
    }

    /**
     * Filter to return only emails that are ready to send (scheduled for past).
     * @return $this
     */
    public function addReadyToSendFilter()
    {
        $this->addIncompleteFilter()
            ->addFieldToFilter('scheduled_at', array('to' => date('Y-m-d H:i:s'), 'date' => true));
        return $this;
    }

    /**
     * Batch update attributes of collection
     *
     * @param Array $attrData
     * @return $this
     */
    public function updateAttributes($attrData)
    {
        $this->getResource()->updateAttributes($this->getAllIds(), $attrData);
        return $this;
    }

    /**
     * Delete all reminders in this collection.
     */
    public function delete()
    {
        $this->getResource()->deleteIds($this->getAllIds());
        return $this;
    }

    /**
     * Get Earliest Created At
     *
     * @return string
     */
    public function getEarliestCreatedAt()
    {
        if (!$this->_minCreatedAt) {
            $select = clone $this->getSelect();
            $select->reset(Zend_Db_Select::COLUMNS)->columns('MIN(created_at)');
            $row = $this->getConnection()->fetchRow($select, $this->_bindParams, Zend_Db::FETCH_NUM);
            $this->_minCreatedAt = date("Y-m-d", strtotime($row[0]));
        }
        return $this->_minCreatedAt;
    }
}