<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Resource_Statistic_Bucket_Collection
 *
 * Collection of statistics buckets (summary for time/day)
 */
class Avid_AbandonedCartReminders_Model_Resource_Statistic_Bucket_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected $_bucketsByDate = array();
    /**
     * @var Avid_AbandonedCartReminders_Model_Statistic_Abstract
     */
    protected $_statistic;

    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('avid_abandoned_cart_reminders/statistic_bucket');
    }

    /**
     * Load Collection.
     *
     * Ensure filter conditions are met, and ensure any missing buckets (missing dates) are collected.
     *
     * @param bool $printQuery
     * @param bool $logQuery
     * @return $this|Varien_Data_Collection_Db
     * @throws Mage_Core_Exception
     */
    public function load($printQuery = false, $logQuery = false)
    {
        if ($this->isLoaded())
            return $this;

        if (!($statistic = $this->getStatistic()))
            throw new Mage_Core_Exception('Cannot load a statistic bucket collection without a statistic Model');
        foreach (array('bucket_size', 'statistic_code', 'uniqueness_scope') as $_scope) {
            if (!($_val = $statistic->getData($_scope)))
                throw new Mage_Core_Exception('Required parameter missing for loading statistic bucket collection: '.$_scope);
            $this->addFieldToFilter($_scope, array('eq' => $_val));
        }
        $this->addFieldToFilter('end_date', array('from' => $statistic->getFromDate(), 'to' => $statistic->getToDate(), 'date' => true));
        parent::load($printQuery, $logQuery);
        $this->_collectEmptyBuckets();
        return $this;
    }

    /**
     * Set Statistic to Collect into Buckets
     *
     * Contains information on filters for type, date, uniqueness.
     *
     * @param Avid_AbandonedCartReminders_Model_Statistic_Abstract $statistic
     * @return $this
     */
    public function setStatistic($statistic)
    {
        $this->_statistic = $statistic;
        return $this;
    }

    /**
     * Get Statistic
     *
     * @return Avid_AbandonedCartReminders_Model_Statistic_Abstract
     */
    public function getStatistic()
    {
        return $this->_statistic;
    }

    /**
     * Get Total
     *
     * @return float|int
     */
    public function getTotal()
    {
        $total = 0;
        foreach ($this as $_bucket)
            $total += $_bucket->getValue();
        return $total;
    }

    /**
     * Get Bucket for given date
     *
     * @param string $date
     * @return Avid_AbandonedCartReminders_Model_Statistic_Bucket
     */
    public function getBucket($date)
    {
        //Check that date is within range. If it is not, throw exception as it is not intended to be loaded this way,
        //and we don't want to collect a bucket that we've not attempted to load already.
        if (!isset($this->_bucketsByDate[$date]))
            foreach ($this->getItems() as $_bucket)
                if (strtotime($_bucket->getEndDate()) == strtotime($date)) $this->_bucketsByDate[$date] = $_bucket;

        if (!isset($this->_bucketsByDate[$date]))
            $this->_bucketsByDate[$date] = $this->_createMissingBucket($date);

        return $this->_bucketsByDate[$date];
    }

    /**
     * Collect Missing buckets within the set dates.
     *
     * Iterates over dates within the range and gets the individual buckets. The getting of the bucket will take care of
     * ensuring it is collected and saved (if date is in the past).
     */
    protected function _collectEmptyBuckets()
    {
       foreach ($this->_getDateRangeArray() as $_date)
           $this->getBucket($_date);
    }

    /**
     * Get arary of dates within the set range.
     *
     * @return array
     * @throws Mage_Core_Exception
     */
    protected function _getDateRangeArray()
    {
        $statistic = $this->getStatistic();
        $dates = array();
        $current = strtotime($statistic->getFromDate());
        $last = strtotime($statistic->getToDate());
        if(!($current && $last))
            throw new Mage_Core_Exception('Could not determine a date range for statistic collection');
        while( $current <= $last ) {
            $dates[] = date('Y-m-d', $current);
            $current = strtotime('+1 day', $current);
        }
        return $dates;
    }

    /**
     * Create a missing bucket.
     *
     * The bucket's collect function will save it if the date is past.
     *
     * @param string $date
     * @return Avid_AbandonedCartReminders_Model_Statistic_Bucket
     */
    protected function _createMissingBucket($date)
    {
        $item = Mage::getModel('avid_abandoned_cart_reminders/statistic_bucket')
            ->setStatistic($this->getStatistic())
            ->setEndDate($date)
            ->collect();
        $this->_addItem($item);
        return $item;
    }
}