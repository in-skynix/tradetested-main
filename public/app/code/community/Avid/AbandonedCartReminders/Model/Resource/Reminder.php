<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Resource_Reminder
 *
 * Reminder Resource
 */
class Avid_AbandonedCartReminders_Model_Resource_Reminder extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('avid_abandoned_cart_reminders/reminder', 'reminder_id');
    }

    /**
     * Bulk Update Data on Multiple Entities
     *
     * @param Array $entityIds
     * @param Array $attrData
     * @return $this
     * @throws Exception
     */
    public function updateAttributes($entityIds, $attrData)
    {
        $connection = $this->_getWriteAdapter();
        $connection->beginTransaction();
        try {
            $where = $connection->quoteInto('reminder_id in (?)', $entityIds);
            $connection->update($this->getMainTable(), $attrData, $where);
            $connection->commit();
        } catch (Exception $e) {
            $this->_getWriteAdapter()->rollBack();
            throw $e;
        }

        return $this;
    }

    public function deleteIds($entityIds = array())
    {
        $connection = $this->_getWriteAdapter();
        $connection->beginTransaction();
        try {
            $where = $connection->quoteInto('reminder_id in (?)', $entityIds);
            $connection->delete($this->getMainTable(), $where);
            $connection->commit();
        } catch (Exception $e) {
            $this->_getWriteAdapter()->rollBack();
            throw $e;
        }
        return $this;
    }
}