<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Statistic_Retrieve
 * Abandoned Cart Retrievals Statistic
 */
class Avid_AbandonedCartReminders_Model_Statistic_Retrieve extends Avid_AbandonedCartReminders_Model_Statistic_Abstract
{
    protected $_eventType = 'retrieve';
    protected $_statisticCode = 'retrieve';
    protected $_label = 'Retrievals';
    protected $_uniquenessScopes = array(
        'event' => 'All Retrieval Actions',
        'reminder' => 'Number of Carts Retrieved',
    );
}