<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Statistic_Send
 *
 * Emails sent Statistic
 */
class Avid_AbandonedCartReminders_Model_Statistic_Send extends Avid_AbandonedCartReminders_Model_Statistic_Abstract
{
    protected $_eventType = 'send';
    protected $_statisticCode = 'send';
    protected $_label = 'Sends';
    protected $_uniquenessScopes = array(
        'event' => 'All Emails',
        'reminder' => 'Number of Carts for which a reminder was sent',
    );

    /**
     * Get Percentage Base
     *
     * This Statistic has no percentage display.
     *
     * @return null
     */
    protected function _getPercentageBase()
    {
        return null;
    }
}