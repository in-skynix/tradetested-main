<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Statistic_Abstract
 *
 * Statistic Model
 *
 * Takes care of collecting bucket, calculating statistics, and display.
 */
abstract class Avid_AbandonedCartReminders_Model_Statistic_Abstract extends Mage_Core_Model_Abstract
{
    //Memoized data for multiple scopes.
    protected $_memoData = array();

    //Collected values for multiple scopes.
    protected $_collectedValues = array();

    //Event type for the statistic to collect data from
    protected $_eventType = 'click';

    //Unique code for the statistic.
    protected $_statisticCode = 'click';

    //Defines display behaviour. e.g. count, currency
    protected $_valueType = 'count';

    //Column to sum data from, if null, will count the rows.
    protected $_valueColumn = null;

    //Display label for the statistic.
    protected $_label = 'Clicks';

    //Labels for uniqueness scopes. Uniqueness scope is used in collection form event table.
    protected $_uniquenessScopes = array(
        'event' => 'All',
        'reminder_step' => 'Number of reminder steps',
        'reminder' => 'Number of unique carts',
    );

    //SQL clauses for filtering for uniqueness scope when collecting from event table.
    protected $_uniquenessClauses = array(
        'event' => null,
        'average' => null,
        'reminder' => 'is_first_for_reminder = 1',
        'reminder_step' => 'is_first_for_step = 1',
    );

    /**
     * Construct
     */
    public function __construct()
    {
        parent::__construct();
        $this->setBucketSize('day')
            ->setStatisticCode($this->_statisticCode)
            ->setUniquenessScope('event');
    }

    /**
     * Get Label for this Statistic
     *
     * @param bool $withScope Include description of the current uniqueness scope.
     * @return string Label
     */
    public function getLabel($withScope = true)
    {
        return $withScope ? "$this->_label ({$this->_uniquenessScopes[$this->getUniquenessScope()]})" : $this->_label;
    }

    /**
     * Get Value Type
     *
     * e.g. count, currency
     *
     * @return string
     */
    public function getValueType()
    {
        return $this->_valueType;
    }

    /**
     * Get Array of All Available Uniqueness Scopes and Corresponding Labels.
     *
     * @return array
     */
    public function getAvailableUniquenessScopes()
    {
        return $this->_uniquenessScopes;
    }

    /**
     * Set Uniqueness Scope
     *
     * @param string $scope
     * @return $this
     * @throws Mage_Core_Exception
     */
    public function setUniquenessScope($scope)
    {
        if (!in_array($scope, array_keys($this->_uniquenessScopes)))
            throw new Mage_Core_Exception("Uniqueness scope '{$scope}' not available for statistic '{$this->_statisticCode}");
        return parent::setUniquenessScope($scope);
    }

    /**
     * Get Bucket Collection for this Scope
     *
     * @return Avid_AbandonedCartReminders_Model_Resource_Statistic_Bucket_Collection
     */
    public function getBucketCollection()
    {
        if (!$this->_hasMemo('bucket_collection')) {
            $bucketCollection = Mage::getModel('avid_abandoned_cart_reminders/statistic_bucket')->getCollection()
                ->setStatistic($this)
                ->load();
            $this->_setMemo('bucket_collection', $bucketCollection);
        }
        return $this->_getMemo('bucket_collection');
    }

    /**
     * Get unique identifier for the scope
     *
     * @return string
     */
    public function getScopeKey()
    {
        return "{$this->getStatisticCode()}.{$this->getBucketSize()}.{$this->getUniquenessScope()}.{$this->getFromDate()}.{$this->getToDate()}";
    }

    /**
     * Get Specific Histogram Value
     *
     * @param string $endDate
     * @return float|int
     */
    public function getValue($endDate)
    {
        return $this->collectValue($endDate);
    }

    /**
     * Collect specific Histogram Value
     *
     * @param $endDate
     * @return float|int
     */
    public function collectValue($endDate)
    {
        if (!(isset($this->_collectedValues[$this->getScopeKey()])))
            $this->_collectedValues[$this->getScopeKey()] = array();
        if (!(isset($this->_collectedValues[$this->getScopeKey()][$endDate]) && $this->_collectedValues[$this->getScopeKey()][$endDate]))
            $this->_collectedValues[$this->getScopeKey()][$endDate] = $this->_loadValue($endDate);
        return $this->_collectedValues[$this->getScopeKey()][$endDate];
    }

    /**
     * Get Total Value scoped to the Range
     *
     * @return float|int
     */
    public function getTotal()
    {
        if (!$this->_hasMemo('total'))
            $this->_setMemo('total', $this->getBucketCollection()->getTotal());
        return $this->_getMemo('total');
    }

    /**
     * Format a value according to the value type
     *
     * @param float|int $value
     * @return string
     */
    public function formatValue($value)
    {
        return ($this->_valueType == 'currency') ? Mage::helper('core')->formatCurrency($value, false) : $value;
    }

    /**
     * Get Percentage Text
     *
     * @return string
     */
    public function getPercentageText()
    {
        if ($base = $this->_getPercentageBase())
            return number_format(($this->getTotal()/$base)*100,2)."% of sends";
    }

    /**
     * Get Collection of Values
     *
     * @return Array
     */
    public function getValues()
    {
        //@todo: allow different x and y axes.
        if(!$this->_hasMemo('values')) {
            $values = array();
            foreach ($this->getBucketCollection() as $_bucket)
                $values[$_bucket->getEndDate()] = $_bucket->getValue();
            $this->_setMemo('values', $values);
        }
        return $this->_getMemo('values');
    }

    /**
     * Get Base to calculate percentages from
     *
     * @return int|float|null
     */
    protected function _getPercentageBase()
    {
        if (!$this->_hasMemo('percentage_base')) {
            $scope = ($this->getUniquenessScope() == 'reminder_step') || ($this->getUniquenessScope() == 'average')
                ? 'event' : $this->getUniquenessScope();
            $percentageBase =  Mage::getModel('avid_abandoned_cart_reminders/statistic_send')
                ->setFromDate($this->getFromDate())
                ->setToDate($this->getToDate())
                ->setUniquenessScope($scope)
                ->getTotal();
            $this->_setMemo('percentage_base', $percentageBase);
        }
        return $this->_getMemo('percentage_base');
    }

    /**
     * Load Specifc Value directly from Events Table.
     *
     * This is the one place where values are loaded from the events table.
     *
     * @param $endDate
     * @return mixed
     */
    protected function _loadValue($endDate)
    {
        $valueExpression = $this->_valueColumn ? "SUM({$this->_valueColumn})" : 'COUNT(event_id)';
        $valueExpression = ($this->getUniquenessScope() == 'average') ?  "AVG({$this->_valueColumn})" : $valueExpression;
        $resource = Mage::getResourceModel('avid_abandoned_cart_reminders/reminder_event');
        $query = $resource->getReadAdapter()->select()->from($resource->getMainTable())
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns(array('value' => $valueExpression))
            ->where('event_type = ?', $this->_eventType)
            ->where('DATE(created_at) = DATE(?)', $endDate);
        if ($uniquenessClause = $this->_uniquenessClauses[$this->getUniquenessScope()])
            $query->where($uniquenessClause);
        if ($stepId = $this->getStepId())
            $query->where('step_id = ?', $stepId);
        if ($reminderId = $this->getReminderId())
            $query->where('step_id = ?', $reminderId);
        return $resource->getReadAdapter()->fetchOne($query);
    }

    /**
     * Set Memoized value specific to the current scope
     *
     * @param $key
     * @param $value
     * @return $this
     */
    protected function _setMemo($key, $value)
    {
        if (!isset($this->_memoData[$this->getScopeKey()]))
            $this->_memoData[$this->getScopeKey()] = array();
        $this->_memoData[$this->getScopeKey()][$key] = $value;
        Mage::register('avid_abandoned_cart_reminders.statistics_memo'.$this->getScopeKey(), $this->_memoData[$this->getScopeKey()], true);
        return $this;
    }

    /**
     * Get Memoized value specific to the current scope
     *
     * @param $key
     * @return null
     */
    protected function _getMemo($key)
    {
        if (!isset($this->_memoData[$this->getScopeKey()]))
            $this->_memoData[$this->getScopeKey()] = ($memo = Mage::registry('avid_abandoned_cart_reminders.statistics_memo'.$this->getScopeKey())) ? $memo : array();
        if (isset($this->_memoData[$this->getScopeKey()][$key]))
            return $this->_memoData[$this->getScopeKey()][$key];
        return null;
    }

    /**
     * Is there memoized data for this scope and key?
     *
     * @param $key
     * @return bool
     */
    protected function _hasMemo($key)
    {
        return ($this->_getMemo($key) !== null);
    }
}