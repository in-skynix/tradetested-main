<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Statistic_Revenue
 *
 * Revenue Statistic
 */
class Avid_AbandonedCartReminders_Model_Statistic_Revenue extends Avid_AbandonedCartReminders_Model_Statistic_Abstract
{
    protected $_eventType = 'convert';
    protected $_statisticCode = 'revenue';
    protected $_label = 'Revenue';
    protected $_valueColumn = 'value_currency';
    protected $_valueType = 'currency';
    protected $_uniquenessScopes = array(
        'event' => 'Total',
        'average' => 'Average',
    );
    protected function _getPercentageBase()
    {
        return null;
    }
}