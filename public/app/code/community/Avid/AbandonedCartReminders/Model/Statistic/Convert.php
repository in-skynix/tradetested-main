<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Statistic_Convert
 *
 * Conversions Statistic
 */
class Avid_AbandonedCartReminders_Model_Statistic_Convert extends Avid_AbandonedCartReminders_Model_Statistic_Abstract
{
    protected $_eventType = 'convert';
    protected $_statisticCode = 'convert';
    protected $_label = 'Conversions';
    protected $_uniquenessScopes = array(
        'event' => 'All Conversions'
    );
}