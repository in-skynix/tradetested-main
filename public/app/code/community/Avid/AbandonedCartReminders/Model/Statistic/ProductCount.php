<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Statistic_ProductCount
 *
 * Product Counts Statistic
 */
class Avid_AbandonedCartReminders_Model_Statistic_ProductCount extends Avid_AbandonedCartReminders_Model_Statistic_Abstract
{
    protected $_eventType = 'convert';
    protected $_statisticCode = 'product_count';
    protected $_label = 'Products';
    protected $_valueColumn = 'value_number';
    protected $_uniquenessScopes = array(
        'event' => 'Total',
        'average' => 'Average',
    );

    /**
     * Get Percentage Base
     *
     * This statistic should not display a percentage.
     *
     * @return null
     */
    protected function _getPercentageBase()
    {
        return null;
    }
}