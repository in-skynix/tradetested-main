<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Statistic_Bucket
 *
 * Bucket to hold summary data for a specific date.
 */
class Avid_AbandonedCartReminders_Model_Statistic_Bucket extends Mage_Core_Model_Abstract
{
    const CACHE_TAG     = 'avid_abandoned_cart_reminders.statistic_bucket';
    protected $_cacheTag= 'avid_abandoned_cart_reminders.statistic_bucket';

    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('avid_abandoned_cart_reminders/statistic_bucket');
    }

    /**
     * Collect Data for this Bucket
     *
     * Get the data from the statistic set to this bucket's range, if the date is past, then save the bucket.
     *
     * @return $this
     */
    public function collect()
    {
        //@todo: check that one does not already exist for this scope.

        $statistic = $this->getStatistic();
        foreach (array('bucket_size', 'statistic_code', 'uniqueness_scope') as $_scope);
            $this->setData($_scope, $statistic->getData($_scope));

        $this->setValue($statistic->collectValue($this->getEndDate()));

        //if end date has passed, we can finalize the bucket
        if (strtotime($this->getEndDate()) < strtotime(date("Y-m-d",time())))
            $this->save();

        return $this;
    }

    /**
     * Set additional qualifying data from the statistic to the row to save.
     *
     * This data is necessary for retrieval of bucket collection with correct scope
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        if ($statistic = $this->getStatistic())
            foreach (array('bucket_size', 'statistic_code', 'uniqueness_scope') as $_scope)
                if (!$this->getData($_scope))
                    $this->setData($_scope, $statistic->getData($_scope));
        return parent::_beforeSave();
    }
}