<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Statistic_Click
 *
 * Clicks Statistic
 */
class Avid_AbandonedCartReminders_Model_Statistic_Click extends Avid_AbandonedCartReminders_Model_Statistic_Abstract
{
    protected $_eventType = 'click';
    protected $_statisticCode = 'click';
    protected $_label = 'Clicks';
    protected $_uniquenessScopes = array(
        'event' => 'All Clicks',
        'reminder_step' => 'Number of reminder emails clicked on',
        'reminder' => 'Number of carts that had a click on a reminder',
    );
}