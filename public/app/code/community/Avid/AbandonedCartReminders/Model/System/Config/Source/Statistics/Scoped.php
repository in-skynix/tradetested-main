<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_System_Config_Source_Statistics_Scoped
 *
 * Source Model to get all statistics and scopes labels and values. as options
 */
class Avid_AbandonedCartReminders_Model_System_Config_Source_Statistics_Scoped
{
    /**
     * To Option Array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = array();
        foreach (Mage::helper('avid_abandoned_cart_reminders')->getAllStatistics() as $_statistic)
            foreach($_statistic->getAvailableUniquenessScopes() as $_code => $_label)
                $options[] = array(
                    'label' => $_statistic->setUniquenessScope($_code)->getLabel(),
                    'value' => "{$_statistic->getStatisticCode()}:$_code"
                );
        return $options;
    }
}