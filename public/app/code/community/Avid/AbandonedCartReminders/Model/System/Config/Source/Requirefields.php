<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 24/07/13
 * Time: 11:33 AM
 * To change this template use File | Settings | File Templates.
 */
class Avid_AbandonedCartReminders_Model_System_Config_Source_Requirefields
{
    /**
     * To Option Array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'label' => 'Telephone',
                'value' => 'telephone'
            ),
            array(
                'label' => 'Email',
                'value' => 'email'
            ),
            array(
                'label' => 'Telephone OR Email',
                'value' => 'or'
            ),
            array(
                'label' => 'Telephone AND Email',
                'value' => 'and'
            ),
        );
    }
}