<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Model_Governor
 *
 * Model for managing cron jobs and background tasks.
 */
class Avid_AbandonedCartReminders_Model_Governor
{
    /**
     * @var Avid_AbandonedCartReminders_Helper_Data
     */
    protected $_helper;

    /**
     * @var Avid_AbandonedCartReminders_Helper_Scheduler
     */
    protected $_scheduler;

    /**
     * @var Avid_AbandonedCartReminders_Helper_Queue
     */
    protected $_queue;

    /**
     * Process cron job, check to see if we should run send process or sequencing process.
     */
    public function cron()
    {
        $this->_helper      = Mage::helper('avid_abandoned_cart_reminders');
        $this->_scheduler   = Mage::helper('avid_abandoned_cart_reminders/scheduler');
        $this->_queue       = Mage::helper('avid_abandoned_cart_reminders/queue');

        if (Mage::getStoreConfig('avid_abandoned_cart_reminders/background/create_sequence_cron'))
            $this->_create();

        if (Mage::getStoreConfig('avid_abandoned_cart_reminders/background/send_sequence_cron'))
            $this->_send();


    }

    /**
     * Create Reminders that have not been created.
     */
    protected function _create()
    {
        $days = Mage::getStoreConfig('avid_abandoned_cart_reminders/background/create_sequence_cron_days');
        $limit = Mage::getStoreConfig('avid_abandoned_cart_reminders/background/cron_batch_size');
        $quotes = Mage::getModel('sales/quote')->getCollection()
            ->addFieldToFilter('customer_email', array('notnull' => true))
            ->addFieldToFilter('main_table.updated_at', array(
                'from'  => date("Y-m-d H:i:s", strtotime("-{$days} days")),
                'date'  => true
            ));
        $quotes->getSelect()
            ->joinLeft(array('reminders' => $quotes->getTable('avid_abandoned_cart_reminders/reminder')), 'main_table.entity_id = reminders.quote_id', array())
            ->where('reminder_id IS NULL')
            ->limit($limit);
        foreach ($quotes as $quote)
            $this->_helper->saveReminderForObject($quote);
    }

    /**
     * Send reminders that are ready to send.
     */
    protected function _send()
    {
        $this->_queue->start();
        register_shutdown_function(array(&$this->_queue,'end'));
        $this->_setStatus('Sending. Finding reminders to send.');
        $remindersToSend = Mage::helper('avid_abandoned_cart_reminders/scheduler')->getRemindersToSend();
        $count = $remindersToSend->count();
        $x = 0;
        foreach ($remindersToSend as $_reminder) {
            $x ++;
            $this->_setStatus("Sending {$x}/{$count}");
            $_reminder->send();
        }
        $this->_setStatus("Finished Sending {$count} Reminders.");
        $this->_queue->end();
    }

    protected function _setStatus($msg)
    {
        $this->_queue->setMsg($msg);
        return $this;
    }
}