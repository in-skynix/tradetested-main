<?php
/**
 * @package     Avid_AbandonedCartReminders
 * @author      Dane Lowe <dane@avidonline.co.nz>.
 * @copyright   Copyright (c) 2013 Avid Online Enterprise Ltd. (New Zealand)
 * @license     Commercial - see LICENSE.txt for details
 */
/**
 * Class Avid_AbandonedCartReminders_Adminhtml_CartReminders_ReportsController
 *
 * Admin Controller for viewing reports/
 */
class Avid_Remarketer_Adminhtml_Remarketer_ReportsController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Init actions
     *
     * @return Mage_Adminhtml_Cms_BlockController
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
            ->_setActiveMenu('avid_remarketer/index')
            ->_addBreadcrumb(Mage::helper('cms')->__('Sales'), Mage::helper('cms')->__('Sales'))
            ->_addBreadcrumb(Mage::helper('cms')->__('Remarketing'), Mage::helper('cms')->__('Remarketing'))
        ;
        return $this;
    }

    protected function _initFile()
    {
        $file = Mage::getModel('avid_remarketer/file')->load($this->getRequest()->getParam('file_id'));
        Mage::register('avid_remarketer.current_file', $file);
    }

    /**
     * Display Dashboard.
     */
    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function viewAction() {
        $this->_initFile();
        $this->loadLayout();
        $this->renderLayout();
    }

    public function massContactedAction()
    {
        $ids = $this->getRequest()->getParam('file_ids');
        $record = Mage::getModel('avid_remarketer/record');
        foreach ($ids as $_id) {
            $record->find($_id)->addTag('contacted');
        }
        $this->_redirect('*/*/index');
    }
    public function contactedAction()
    {
        Mage::getModel('avid_remarketer/record')->find($this->getRequest()->getParam('file_id'))->addTag('contacted');
        $this->_redirect('*/*/index');
    }

    public function massIgnoreAction()
    {
        $ids = $this->getRequest()->getParam('file_ids');
        $record = Mage::getModel('avid_remarketer/record');
        foreach ($ids as $_id) {
            $record->find($_id)->addTag('ignored');
        }
        $this->_redirect('*/*/index');
    }
    public function ignoreAction()
    {
        Mage::getModel('avid_remarketer/record')->find($this->getRequest()->getParam('file_id'))->addTag('ignored');
        $this->_redirect('*/*/index');
    }

}