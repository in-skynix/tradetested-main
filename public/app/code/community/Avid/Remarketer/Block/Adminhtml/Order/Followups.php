<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 30/07/13
 * Time: 3:30 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_Remarketer_Block_Adminhtml_Order_Followups extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        if(is_null($this->_backButtonLabel)) {
            $this->_backButtonLabel = $this->__('Back');
        }
        Mage_Adminhtml_Block_Widget_Container::__construct();
        $this->setTemplate('widget/grid/container.phtml');
    }


    /**
     * Prepare Layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->setChild( 'grid', $this->getLayout()->createBlock('avid_remarketer/adminhtml_order_followups_grid',$this->_controller . '.grid') );  /////->setSaveParametersInSession(true) ?
        return Mage_Adminhtml_Block_Widget_Container::_prepareLayout();
    }

    /**
     * Get Header Text
     *
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper('avid_remarketer')->__('View Orders to Follow Up');
    }
}