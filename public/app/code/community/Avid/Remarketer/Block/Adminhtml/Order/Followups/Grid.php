<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 30/07/13
 * Time: 3:29 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_Remarketer_Block_Adminhtml_Order_Followups_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Construct grid block
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('avid_remarketer_followups_grid');
        $this->setDefaultSort('created_at', 'asc');
        $this->setUseAjax(true);
    }

    /**
     * Prepare Collection
     *
     * @return this
     */
    protected function _prepareCollection()
    {
        $this->setCollection(Mage::getSingleton('avid_remarketer/record')->getOrderGridCollection());
        return parent::_prepareCollection();
    }

    /**
     * Prepare Columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('file_id',
            array(
                'header'=> Mage::helper('avid_remarketer')->__('ID'),
                'width' => '50px',
                'type'  => 'number',
                'index' => 'file_id',
            ));
        $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('sales')->__('Order #'),
            'width' => '80px',
            'renderer' => 'Avid_Remarketer_Block_Adminhtml_Renderer_Column_Orderid',
            'index' => 'increment_id',
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'    => Mage::helper('sales')->__('Purchased From (Store)'),
                'index'     => 'store_id',
                'type'      => 'store',
                'store_view'=> true,
                'display_deleted' => true,
            ));
        }

        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
        ));

        $this->addColumn('billing_name', array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
        ));

        $this->addColumn('billing_telephone', array(
            'header' => Mage::helper('sales')->__('Phone'),
            'index' => 'billing_telephone',
        ));

        $this->addColumn('last_order_time', array(
            'header' => Mage::helper('sales')->__('Last Order Time'),
            'index' => 'last_order_time',
            'type' => 'datetime',
            'width' => '100px',
        ));

        $this->addColumn('last_product_viewed_time', array(
            'header' => Mage::helper('sales')->__('Last Product View Time'),
            'index' => 'last_view_time',
            'type' => 'datetime',
            'width' => '100px',
        ));

        $this->addColumn('grand_total', array(
            'header' => Mage::helper('sales')->__('Order Total'),
            'index' => 'grand_total',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
        ));

        $this->addColumn('products_viewed_since', array(
            'header' => Mage::helper('sales')->__('Products Viewed Since'),
            'index' => 'products_viewed_since',
            'renderer'  => 'Avid_Remarketer_Block_Adminhtml_Renderer_Column_Products'
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('sales')->__('Order Status'),
            'index' => 'status',
            'type'  => 'options',
            'width' => '70px',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));
        $this->addColumn('action',
            array(
                'header'    => Mage::helper('sales')->__('Action'),
                'width'     => '50px',
                'type'      => 'action',
                'getter'     => 'getId',
                'actions'   => array(
                    array(
                        'caption' => Mage::helper('sales')->__('View'),
                        'url'     => array('base'=>'*/*/view'),
                        'field'   => 'file_id'
                    ),
                    array(
                        'caption' => Mage::helper('sales')->__('Mark as Contacted'),
                        'url'     => array('base'=>'*/*/contacted'),
                        'field'   => 'file_id'
                    ),
                    array(
                        'caption' => Mage::helper('sales')->__('Ignore until next product view'),
                        'url'     => array('base'=>'*/*/ignore'),
                        'field'   => 'file_id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
            ));
        return parent::_prepareColumns();
    }

    /**
     * Get Grid URL for use in AJAX requests
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->_getData('grid_url') ? $this->_getData('grid_url') : $this->getUrl('*/remarketer_reports/grid', array('_current'=>true));
    }


    public function getRowUrl($row)
    {

        return $this->getUrl('*/*/view', array('file_id' => $row->getFileId()));
        return false;
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('file_id');
        $this->getMassactionBlock()->setFormFieldName('file_ids');
        $this->getMassactionBlock()->addItem('mark_as_contacted', array(
            'label'=> Mage::helper('catalog')->__('Mark As Contacted'),
            'url'  => $this->getUrl('*/*/massContacted')
        ));
        $this->getMassactionBlock()->addItem('ignore', array(
            'label'=> Mage::helper('catalog')->__('Ignore until next product view'),
            'url'  => $this->getUrl('*/*/massIgnore')
        ));
        return $this;
    }
}