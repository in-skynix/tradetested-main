<?php
class Avid_Remarketer_Block_Adminhtml_Renderer_Column_Orderid extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $id = $row->getData($this->getColumn()->getIndex());
        return '<a href="'.$this->getUrl('adminhtml/sales_order/view', array('order_id' => $row->getOrderId())).'">'.$id.'</a>';
    }
}