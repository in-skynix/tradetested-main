<?php
class Avid_Remarketer_Block_Adminhtml_Renderer_Column_Products extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $html = '';
        $data = $row->getData($this->getColumn()->getIndex());
        foreach ($data as $_product)
            $html .= $_product->getName().",";
        return $html;
    }
}