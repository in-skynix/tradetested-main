<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 30/07/13
 * Time: 1:40 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_Remarketer_Model_Visitor extends Mage_Core_Model_Abstract
{
    const CACHE_TAG     = 'avid_remarketer.visitor';
    protected $_cacheTag= 'avid_remarketer.visitor';

    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('avid_remarketer/visitor');
    }
}