<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 2/08/13
 * Time: 1:37 PM
 * To change this template use File | Settings | File Templates.
 */
class Avid_Remarketer_Model_File extends Mage_Core_Model_Abstract
{
    protected $_document;

    protected $_lastOrder;
    protected $_history;
    protected $_actionLabels = array(
        'product_view' => 'Viewed Product',
        'order_create' => 'Created Order',
        'cart_add_product' => 'Added Product to Cart',
        'contacted' => 'Customer Contacted',
        'ignored' => 'File Ignored'
    );

    public function load($id, $field = null)
    {
        $record = Mage::getSingleton('avid_remarketer/record');
        $this->_document = $record->findDocument($id);
        return $this;
    }

    public function getFileId()
    {
        return (string)$this->_document['_id'];
    }

    public function getLastOrder()
    {
        if (!$this->_lastOrder) {
            $this->_lastOrder = Mage::getModel('sales/order')->load($this->_document['latest_tags']['order_create']['value']);
        }
        return $this->_lastOrder;
    }

    public function getHistory($type = null)
    {
        if (!$this->_history) {
            $history = array();
            $historyIds = array();
            foreach ($this->_document['tags'] as $_tag) {
                if (!isset($historyIds[$_tag['name']]))
                    $historyIds[$_tag['name']] = array();
                $historyIds[$_tag['name']][$_tag['timestamp']] = $_tag['value'];
            }
            foreach ($historyIds as $_type => $_data) {
                $history[$_type] = array();
                switch ($_type) {
                   case 'order_create':
                        $collection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('entity_id', array('in' => $_data));
                        break;
                   case 'contacted':
                        $collection = null;
                        break;
                   case 'ignored':
                        $collection = null;
                        break;
                   default:
                        $collection = Mage::getModel('catalog/product')->getCollection()->addIdFilter($_data)->addAttributeToSelect(array('name'));
                }
                if ($collection) {
                    foreach ($collection as $_object) {
                        $timestamps = array_keys($_data, $_object->getId());
                        foreach ($timestamps as $timestamp)
                            if ($timestamp !== false)
                                $history[$_type][$timestamp] = array(
                                    'object' => $_object,
                                    'type' => $_type,
                                    'action_label' => $this->_actionLabels[$_type],
                                );
                    }
                } else {
                    foreach ($_data as $_timestamp => $_details)
                        $history[$_type][$_timestamp] = array(
                            'value' => $_details,
                            'type' => $_type,
                            'action_label' => $this->_actionLabels[$_type],
                        );
                }
            }
            $this->_history = $history;
        }
        if ($type)
            return $this->_history[$type];
        $h = array();
        foreach ($this->_history as $_typeHistory)
            foreach ($_typeHistory as $_timestamp => $_data)
            $h[$_timestamp] = $_data;
        ksort($h);
        return $h;
    }

}