<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 26/07/13
 * Time: 11:14 AM
 * To change this template use File | Settings | File Templates.
 */
class Avid_Remarketer_Model_Record extends Mage_Core_Model_Abstract
{
    protected $_document;
    protected $_newTags = array();
    protected $_toSet = array('latest_tags' => array());
    protected $_mongoCollection;

    public function __construct()
    {
        $this->_getDocument();
        //can get the created timestamp from the document's MongoId
    }

    public function getOrderGridCollection()
    {
        $data = $this->_getOrderData();
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('entity_id', array('in' => array_keys($data)));
        $collection->getSelect()
            ->join(array('a' => 'sales_flat_order_address'), 'a.entity_id=main_table.billing_address_id', array('billing_telephone' => 'telephone', 'billing_name' => new Zend_Db_Expr("CONCAT(firstname, ' ', lastname)")));
        //echo $collection->getSelect(); die();
        $this->_addProductCollectionsToOrderCollection($collection, $data);
        return $collection;
    }

    /**
     * Find Document
     *
     * Careful! - Will load the doc into this instance - used for admin actions only.
     * @param $id
     * @return array|null
     */
    public function findDocument($id)
    {
        return  $this->_getMongoCollection()->findOne(array('_id' => new MongoId($id)));
    }

    public function find($id)
    {
        $this->_document = $this->_getMongoCollection()->findOne(array('_id' => new MongoId($id)));
        return $this;
    }
    protected function _addProductCollectionsToOrderCollection($collection, $data)
    {
        $allProductIds = array();
        foreach ($data as $orderData)
            $allProductIds = array_unique(array_merge($allProductIds, $orderData['product_ids']));
        if (!count($allProductIds))
            return;
        $allProducts = Mage::getModel('catalog/product')->getCollection()->addIdFilter($allProductIds)
            ->addAttributeToSelect(array('name'))
            ;
        foreach ($collection as $_order) {
            $orderProductIds = $data[$_order->getId()]['product_ids'];
            $productsViewedSince = array();
            foreach ($allProducts as $_product)
                if(in_array($_product->getId(), $orderProductIds))
                    $productsViewedSince[] = $_product;
            $lastView = isset($data[$_order->getId()]['latest_tags']['product_view']) ? $data[$_order->getId()]['latest_tags']['product_view'] : array('timestamp' => '', 'value' => '');
            $lastOrder = isset($data[$_order->getId()]['latest_tags']['product_view']) ? $data[$_order->getId()]['latest_tags']['order_create'] : array('timestamp' => '', 'value' => '');
            $_order->setProductsViewedSince($productsViewedSince)
                ->setOrderId($_order->getId())
                ->setFileId($data[$_order->getId()]['file_id'])
                ->setId($data[$_order->getId()]['file_id'])
                ->setLastOrderTime(date('Y-m-d H:i:s',$lastOrder['timestamp']))
                ->setLastViewTime(date('Y-m-d H:i:s',$lastView['timestamp']))
                ;
        }
    }

//    protected function _addAllTagDataToCollection($collection, $data)
//    {
//        //@todo: delegate to own classes etc.
//        $productViews = array();
//        $orderCreates = array();
//        foreach ($data as $_tag) {
//            if ($_tag['name'] == 'product_view')
//                $productViews[$_tag['value']] = $_tag['timecode'];
//            if ($_tag['name'] == 'order_create')
//                $orderCreates[$_tag['value']] = $_tag['timecode'];
//        }
//        $orders = Mage::getResourceModel('sales/order_grid_collection')
//            ->addFieldToFilter('entity_id', array('in', array_keys($orderCreates)));
//        $products = Mage::getModel('catalog/product')->getCollection()
//            ->addIdFilter(array_keys($orderCreates));
//        foreach ($orders as $_order)
//            $_order->setRemarketTimecode($orderCreates[$_order->getId()]);
//        foreach ($products as $_product)
//            $_product->setRemarketTimecode($productViews[$_product->getId()]);
//    }

    protected function _getOrderData()
    {
        $orderData = array();
        $timediff = 24*60*60;
        $lastContact = strtotime('-1 month');
        //Haven't been able to get the match into the first pipeline yet. Nto sure why.
        $cursor = $this->_getMongoCollection()->aggregate(
            array(
                '$project' => array(
                    'has_viewed_since' => array(
                        '$gt' => array('$latest_tags.product_view.timestamp', array('$add' => array('$latest_tags.order_create.timestamp', $timediff))),
                    ),
                    'has_an_order' => array(
                        '$gt' => array('$latest_tags.order_create', 0),
                    ),
                    'has_not_been_contacted_recently' => array(
                        '$lt' => array('$latest_tags.contacted.timestamp', $lastContact),
                    ),
                    'has_not_been_ignored_since_last_product_view' => array(
                        '$lt' => array('$latest_tags.ignored.timestamp', '$latest_tags.product_view.timestamp')
                    ),
                    'has_not_been_contacted_since_last_product_view' => array(
                        '$lt' => array('$latest_tags.contacted.timestamp', '$latest_tags.product_view.timestamp')
                    ),
                    'latest_tags.product_view' => true,
                    'latest_tags.order_create' => true,
                    'tags' => true
                )
            ),
            array(
                '$match' => array('has_an_order' => true, 'has_viewed_since' => true, 'has_not_been_contacted_recently' => true, 'has_not_been_ignored_since_last_product_view' => true, 'has_not_been_contacted_since_last_product_view' => true),
            )
        );
        if (isset($cursor['result'])) foreach ($cursor['result'] as $_doc) {
            $productsSinceLastOrder = array();
            if (is_array($_doc['tags'])) foreach ($_doc['tags'] as $_tag)
                if (($_tag['name'] == 'product_view') && ($_tag['timestamp'] > $_doc['latest_tags']['order_create']['timestamp']))
                    $productsSinceLastOrder[] = $_tag['value'];
            $orderData[$_doc['latest_tags']['order_create']['value']] = array(
                'latest_tags' => $_doc['latest_tags'],
                'product_ids' => $productsSinceLastOrder,
                'file_id' => (string)$_doc['_id']
            );
        }
        //db.visitor_records.find({'latest_tags.order_create.timestamp': {$lt: 1375151888}, 'latest_tags.product_view.timestamp': {$gt: 1375151888}})
        return $orderData;
    }

    public function addTag($name, $value = true, $timestamp = null)
    {
        $timestamp = $timestamp ? $timestamp : time();
        $this->_newTags[] = array(
            'name'      => $name,
            'timestamp' => $timestamp,
            'value'     => $value
        );
        $this->_toSet['latest_tags'][$name] = array(
            'timestamp' => $timestamp,
            'value'     => $value
        );
        $this->_persist();

    }


    public function getTag($name)
    {
        return;
    }


    protected function _persist()
    {
        $collection = $this->_getMongoCollection();
        //If we already have a document, update it with the new data array.
        //If we do not have a document (is null), we insert the new data array, get the ID and set it in cookie.
        if ($this->_document) {
            //$collection->update(array('_id' => new MongoId($this->_getVisitorId())), array('$push' => array('tags' => array('$each' => $this->_newTags)))); //Mongo 2.4+
            $ritit = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->_toSet));
            $result = array();
            foreach ($ritit as $leafValue) {
                $keys = array();
                foreach (range(0, $ritit->getDepth()) as $depth) {
                    $keys[] = $ritit->getSubIterator($depth)->key();
                }
                $result[ join('.', $keys) ] = $leafValue;
            }
            $collection->update(array('_id' => $this->_document['_id']), array('$pushAll' => array('tags' => $this->_newTags), '$set' => $result));
        } else {
            //May need to convert the newTags array to an array with positions. e.g. tags.name.timestamp => value
            $data = array_merge($this->_toSet, array('tags' => $this->_newTags));
            $collection->insert($data);
            $this->_setVisitorId((string)$data['_id']);
        }


    }

    protected function _getDocument()
    {
        if (!$this->_document)
            $this->_document = $this->_getMongoCollection()->findOne(array('_id' => new MongoId($this->_getVisitorId())));
        return $this->_document;
    }

    protected function _getMongoCollection()
    {
        if (!$this->_mongoCollection) {
            $client = new MongoClient();
            $dbname = str_replace('.', '_', (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname'));
            $this->_mongoCollection = $client->$dbname->visitor_records;
        }
        return $this->_mongoCollection;
    }

    /**
     * Get Visitor ID
     *
     * Currently got from a value set in a long-term cookie.
     *
     * Alternate algorithms can be developed to e.g. get or infer an ID from an IP address, browser combo, link two
     * cookies together when a user proves to be the same as from another cookie (i.e. enters same email on second
     * computer, create a merge function?)
     *
     * @return string
     */
    protected function _getVisitorId()
    {
        return (isset($_COOKIE['avid_vid']) && $_COOKIE['avid_vid']) ? $_COOKIE['avid_vid'] : null;
    }

    protected function _setVisitorId($id)
    {
        setcookie('avid_vid', $id, time()+60*60*24*365*2, '/', (string)Mage::getStoreConfig(Mage_Core_Model_Cookie::XML_PATH_COOKIE_DOMAIN, Mage::app()->getStore())); //Set the visitor ID cookie again on each visit?
    }
}