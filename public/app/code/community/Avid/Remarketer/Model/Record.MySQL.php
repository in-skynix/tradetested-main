<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 26/07/13
 * Time: 11:14 AM
 * To change this template use File | Settings | File Templates.
 */
class Avid_Remarketer_Model_Record_MySQL extends Mage_Core_Model_Abstract
{
    protected $_visitorId;

    public function addTag($name, $value = true, $timestamp = null)
    {
        $timestamp = $timestamp ? $timestamp : time();
        $this->_newTags[] = array(
            'name'      => $name,
            'timestamp' => $timestamp,
            'value'     => $value
        );
        $this->_persist();

    }


    public function getTag($name)
    {
        return;
    }


    protected function _persist()
    {
        $collection = $this->_getMongoCollection();
        //If we already have a document, update it with the new data array.
        //If we do not have a document (is null), we insert the new data array, get the ID and set it in cookie.
        if ($this->_document) {
            //$collection->update(array('_id' => new MongoId($this->_getVisitorId())), array('$push' => array('tags' => array('$each' => $this->_newTags)))); //Mongo 2.4+
            $collection->update(array('_id' => new MongoId($this->_getVisitorId())), array('$pushAll' => array('tags' => $this->_newTags)));
        } else {
            //May need to convert the newTags array to an array with positions. e.g. tags.name.timestamp => value
            $data = array('tags' => $this->_newTags);
            $collection->insert($data);
            $this->_setVisitorId((string)$data['_id']);
        }


    }

    protected function _getDocument()
    {
        if (!$this->_document)
            $this->_document = $this->_getMongoCollection()->findOne(array('_id' => new MongoId($this->_getVisitorId())));
        return $this->_document;
    }

    protected function _getMongoCollection()
    {
        if (!$this->_mongoCollection) {
            $client = new MongoClient();
            $this->_mongoCollection = $client->magento->visitor_records;
        }
        return $this->_mongoCollection;
    }

    /**
     * Get Visitor ID
     *
     * Currently got from a value set in a long-term cookie.
     *
     * Alternate algorithms can be developed to e.g. get or infer an ID from an IP address, browser combo, link two
     * cookies together when a user proves to be the same as from another cookie (i.e. enters same email on second
     * computer, create a merge function?)
     *
     * @return string
     */
    protected function _getVisitorId()
    {
        if (!$this->_visitorId) {
            $string = (isset($_COOKIE['avid_mvid']) && $_COOKIE['avid_mvid']) ? $_COOKIE['avid_mvid'] : '';
            $parts = explode(':', $string);
            if ((count($parts) == 2) && ($this->_encodeId($parts[0]) == $parts[1])) {
                $this->_visitorId = $parts[0];
            } else {
                $this->_visitorId = $this->_createVisitor();
            }
        }
        return $this->_visitorId;
    }

    protected function _createVisitor()
    {
        $visitor = Mage::getModel('avid_remarketer/visitor')->save();
        $id = $visitor->getId();
        setcookie('avid_mvid', $id.":".$this->_encodeId($id), time()+60*60*24*365*2); //@todo: Set the visitor ID cookie again on each visit?
        return $id;
    }

    protected function _encodeId($id)
    {
        return md5($id); //@todo: Quick hack. need to add some sort of security to avoid cookie tampering.
    }
}