<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 26/07/13
 * Time: 10:46 AM
 * To change this template use File | Settings | File Templates.
 */
class Avid_Remarketer_Model_Observer
{
    public function catalogControllerProductView($observer)
    {
        try {
            Mage::getSingleton('avid_remarketer/record')->addTag('product_view', $observer->getEvent()->getProduct()->getId());
        } catch (Exception $e) { }
    }
    public function salesQuoteSaveAfter($observer)
    {
        try {
           // Mage::getSingleton('avid_remarketer/record')->addTag('quote_create', $observer->getEvent()->getQuote()->getId()); //Need to work out a strategy, e.g. only save for new quote.
        } catch (Exception $e) { }
    }
    public function wishlistAddProduct($observer)
    {
        try {
            Mage::getSingleton('avid_remarketer/record')->addTag('wishlist_add_product', $observer->getEvent()->getProduct()->getId());
        } catch (Exception $e) { }
    }
    public function checkoutCartProductAddAfter($observer)
    {
        try {
            Mage::getSingleton('avid_remarketer/record')->addTag('cart_add_product', $observer->getEvent()->getProduct()->getId());
        } catch (Exception $e) { }
    }
    public function checkoutSubmitAllAfter($observer)
    {
        try {
            if ($order = $observer->getEvent()->getOrder()) {
                Mage::getSingleton('avid_remarketer/record')->addTag('order_create', $order->getId());
            }
        } catch (Exception $e) { }
    }
}