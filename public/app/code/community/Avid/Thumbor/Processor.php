<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 15/09/14
 * Time: 1:59 PM
 */
class Avid_Thumbor_Processor extends Varien_Image_Adapter_Abstract
{
    /**
     * @param null $fileName
     * @param string $adapter
     */
    function __construct($fileName=null, $adapter=null)
    {
        $this->_fileName = $fileName;
    }

    public function open($fileName)
    {
        $this->_fileName = $fileName;
    }

    public function display()
    {
    }

    public function save($destination=null, $newFileName=null)
    {
    }

    public function rotate($angle)
    {
    }

    public function crop($top=0, $left=0, $right=0, $bottom=0)
    {
    }

    public function resize($width = null, $height = null)
    {
    }

    public function keepAspectRatio($value)
    {
    }

    public function keepFrame($value)
    {
    }

    public function keepTransparency($value)
    {
    }

    public function constrainOnly($value)
    {
    }

    public function backgroundColor($value)
    {
    }

    public function quality($value)
    {
    }

    public function watermark($watermarkImage, $positionX=0, $positionY=0, $watermarkImageOpacity=30, $repeat=false)
    {
    }

    public function process()
    {
    }

    public function instruction()
    {
    }
    
    public function checkDependencies()
    {
    }
}