<?php
require_once('Thumbor'.DS.'Url'.DS.'BuilderFactory.php');
require_once('Thumbor'.DS.'Url'.DS.'Builder.php');
require_once('Thumbor'.DS.'Url'.DS.'CommandSet.php');
require_once('Thumbor'.DS.'Url.php');
class Avid_Thumbor_Helper_Data extends Mage_Core_Helper_Data
{
    public function getUrl($image)
    {
        $urlFactory = Thumbor\Url\BuilderFactory::construct(Mage::getStoreConfig('avid_thumbor/setup/host'), Mage::getStoreConfig('avid_thumbor/setup/key'));
        return $urlFactory->url($image);
    }
}
