<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 4/08/16
 * Time: 2:03 PM
 */
class Avid_Thumbor_Model_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     * 
     * The event is dispatched in dev-tradetested branch of arkade/magento-s3. 
     * 
     * Generate thumbnail URLs in admin
     */
    public function wysiwygImagesGetThumbnailUrl(Varien_Event_Observer $observer)
    {
        /** @var Varien_Object $transport */
        $transport = $observer->getEvent()->getTransport();
        $url = str_replace(Mage::getBaseDir('media'), '', $transport->getFilePath());
        $url = str_replace('/home/tradetested/media', '', $url); //The symlink messes with it.
        $url = Mage::helper('avid_thumbor')->getUrl($url)->fitIn(100, 75);
        $transport->setThumbnailUrl($url->__toString());

    }
    
    public function wysiwygImagesUploadFile(Varien_Event_Observer $observer)
    {
        /** @var Varien_Object $transport */
        $transport = $observer->getEvent()->getTransport();
        $targetPath = $transport->getTargetPath();
        $type = $transport->getType();
        $storage = Mage::getModel('cms/wysiwyg_images_storage');
        
        $uploader = new Mage_Core_Model_File_Uploader('image');
        if ($allowed = $storage->getAllowedExtensions($type)) {
            $uploader->setAllowedExtensions($allowed);
        }
        $uploader->setAllowRenameFiles(true);
        $uploader->setFilesDispersion(false);
        $result = $uploader->save($targetPath);
        /** @var $storageHelper Mage_Core_Helper_File_Storage_Database */
        $storageHelper = Mage::helper('core/file_storage_database');
        $storageHelper->saveUploadedFile($result);

        if (!$result) {
            Mage::throwException( Mage::helper('cms')->__('Cannot upload file.') );
        }

        return $result;
    }
}