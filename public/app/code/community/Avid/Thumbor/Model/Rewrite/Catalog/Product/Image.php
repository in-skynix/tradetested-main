<?php
class Avid_Thumbor_Model_Rewrite_Catalog_Product_Image extends Mage_Catalog_Model_Product_Image
{
    /**
     * @return Varien_Image
     */
    public function getImageProcessor()
    {
        if (!$this->_isEnabled()) {
            return parent::getImageProcessor();
        }
        if( !$this->_processor ) {
            $this->_processor = new Avid_Thumbor_Processor($this->getBaseFile());
        }
        $this->_processor->keepAspectRatio($this->_keepAspectRatio);
        $this->_processor->keepFrame($this->_keepFrame);
        $this->_processor->keepTransparency($this->_keepTransparency);
        $this->_processor->constrainOnly($this->_constrainOnly);
        $this->_processor->backgroundColor($this->_backgroundColor);
        $this->_processor->quality($this->_quality);
        return $this->_processor;
    }

    /**
     * @return Mage_Catalog_Model_Product_Image
     */
    public function saveFile()
    {
        if (!$this->_isEnabled()) {
            return parent::saveFile();
        }
        return $this;
    }

    public function setBaseFile($file)
    {
        if (!$this->_isEnabled()) {
            return parent::setBaseFile($file);
        }
        $this->_isBaseFilePlaceholder = false;
        if (($file) && (0 !== strpos($file, '/', 0))) {
            $file = '/' . $file;
        }
        if ('/no_selection' == $file) {
            $file = null;
        }
        if ($file) {
            $this->_baseFile = 'catalog/product'.$file;
        } else {
            //@todo: setup thumbor to serve skin asset, upload a placeholder image to config and test.
            if ($isConfigPlaceholder = Mage::getStoreConfig("catalog/placeholder/{$this->getDestinationSubdir()}_placeholder")) {
                $this->_baseFile   = 'catalog/product/placeholder/' . $isConfigPlaceholder;
            } else {
                $this->_baseFile   = 'catalog/product/no_selection.png'; //Trade Tested has it here at the moment.
//                $this->_baseFile = "assets/catalog/product/placeholder/{$this->getDestinationSubdir()}.jpg";
            }
            $this->_isBaseFilePlaceholder = true;
        }
    }

    /**
     * Is Cached
     * 
     * Prevent checking filesystem or S3. When using thumbor, we don't need local cache.
     * 
     * @return bool
     */
    public function isCached()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        if (!$this->_isEnabled()) {
            return parent::getUrl();
        }
        //Not looking up mime type here to save disk access where its not needed.
        if (pathinfo($this->getBaseFile(), PATHINFO_EXTENSION) == 'svg') {
            return Mage::getBaseUrl('media').$this->getBaseFile();
        }
        if (!$this->getHeight()) {
            $this->setHeight($this->getWidth());
        }
        $url = Mage::helper('avid_thumbor')->getUrl($this->getBaseFile());
        if ($this->getWidth()) {
            $url = $url->fitIn($this->getWidth(), $this->getHeight())
                ->addFilter('no-upscale');
        }
        if ($this->_keepFrame) {
            $url = $url->addFilter('fill', $this->_getHtmlBackgroundColor());
        }
        return $url->__toString();

        /*
         * @todo
         *
         *  + Caching?
         *  + $this->getAngle()
         *  + $model->getQuality()
         *  + $model->getKeepFrame()
         *  + $model->getKeepAspectRatio()
         *  + $model->getConstrainOnly()
         *  + $model->getBackgroundColor()
         */
    }

    protected function _getHtmlBackgroundColor()
    {
        if (is_array($this->_backgroundColor) && sizeof($this->_backgroundColor) == 3)
            list($r, $g, $b) = $this->_backgroundColor;

        $r = intval($r); $g = intval($g);
        $b = intval($b);

        $r = dechex($r<0?0:($r>255?255:$r));
        $g = dechex($g<0?0:($g>255?255:$g));
        $b = dechex($b<0?0:($b>255?255:$b));

        $color = (strlen($r) < 2?'0':'').$r;
        $color .= (strlen($g) < 2?'0':'').$g;
        $color .= (strlen($b) < 2?'0':'').$b;
        return $color;
    }

    protected function _isEnabled()
    {
        return (boolean)Mage::getStoreConfig('avid_thumbor/setup/enabled');
    }
}