<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 4/08/16
 * Time: 3:07 PM
 */
class Avid_Thumbor_Model_Rewrite_Catalog_Product_Media_Config extends Mage_Catalog_Model_Product_Media_Config
{
    public function getMediaUrl($file)
    {
        $file = 'catalog/product/'.ltrim($this->_prepareFileForUrl($file), '/');
        return Mage::helper('avid_thumbor')->getUrl($file)->fitIn(100, 100)->__toString();
    }
}
