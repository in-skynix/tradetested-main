<?php
/**
 * @author Dane Lowe
 * @package Avid_Gravitator
 * @copyright Copyright (c) 2014 Avid Online Enterprises Limited
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Avid_Gravitator_Helper_Data extends Mage_Core_Helper_Data
{
    protected $_baseUrls = [
        'dev' => ['gvt' => 'http://localhost:3000/', 'jvt' => 'http://localhost:8443/'],
        'production' => ['gvt' => 'https://pxl.gravitator.io/', 'jvt' => 'https://pxl.gravitator.io:8443/tradetested'],
    ];
    
    public function getEmhCookie()
    {
        return Mage::getSingleton('core/cookie')->get('pxl.emh');
    }

    public function setEmhCookie($emh)
    {
        Mage::getSingleton('core/cookie')->set('pxl.emh', $emh ,time()+63072000,'/');
        return $this;
    }
    /**
     * @todo: get the Configured access token for extra API features.
     *
     * @return null
     */
    public function getAccessToken()
    {
        return '8d99bd1999932cfabf1e6113a93cd8782afa7fa731778754376c3948b6d27d44';
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return Mage::getStoreConfigFlag('avid_gravitator/general/enabled');
    }

    /**
     * @return bool
     */
    public function isTestMode($storeId = null)
    {
        return Mage::getStoreConfigFlag('avid_gravitator/general/test_mode', $storeId);
    }

    public function getBaseUrl($service, $storeId = null)
    {
        return $this->_getBaseDomain($service, $storeId).$this->_getDir($service, $storeId);
    }
    
    protected function _getDir($service, $storeId)
    {
        return ($service == 'jvt') ?  Mage::getStoreConfig('avid_gravitator/general/store_name', $storeId).'/' : '';
    }

    protected function _getBaseDomain($service, $storeId)
    {
        return $this->isTestMode($storeId) 
            ? $this->_baseUrls['dev'][$service] 
            : $this->_baseUrls['production'][$service];
    }
}