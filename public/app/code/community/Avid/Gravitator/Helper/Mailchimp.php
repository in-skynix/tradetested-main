<?php
class Avid_Gravitator_Helper_Mailchimp extends Mage_Core_Helper_Abstract
{
    protected $_json = '[{"email":null,"mc_eid":"f9e8258dce","skus":["891","1770","1771","1636","1309","1161","1261","1890","467","1213"]},{"email":null,"mc_eid":"0e45bbeda1","skus":["679","676","682"]},{"email":null,"mc_eid":"d857699ac9","skus":[]}]';
    protected $_data;
    protected $_allProducts;
    protected $_allHtml;
    protected $_block;


    public function getData()
    {
        if (!$this->_data){
            $token = Mage::helper('avid_gravitator')->getAccessToken();
            $this->_data = json_decode(file_get_contents("https://pxl.gravitator.io/api/user_recommendations.json?access_token={$token}"), true);
            $this->_data = json_decode($this->_json, true);
        }
        return $this->_data;
    }

    public function getProducts($skus = null)
    {
        if (!$this->_allProducts) {
            $skus = array();
            foreach ($this->getData() as $_row) {
                $skus = array_unique(array_merge($skus,$_row['skus']), SORT_REGULAR);
            }
            $collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToFilter('entity_id', array('in' => $skus));
            $collection
                ->setStoreId(1)
                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ;

            Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
            Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
            $this->_allProducts = $collection;
        }
        if ($skus) {
            $products = array();
            foreach ($this->_allProducts as $_product) {
                if (in_array($_product->getId(), $skus)) {
                    $products[] = $_product;
                }
            }
            return $products;
        } else {
            return $this->_allProducts;
        }
    }

    public function getAllHtml()
    {
        if (!$this->_allHtml) {
            $data = array();
            foreach ($this->getData() as $_row) {
                $data[$_row['mc_eid']] = (count($_row['skus']) > 1) ? $this->getHtml($this->getProducts($_row['skus'])) : '';
            }
            $this->_allHtml = $data;
        }
        return $this->_allHtml;
    }

    public function send()
    {
        $apiKey = 'b999bb0d51370d68dee76ca5c41c440c-us2';
        $listId = '3e38d9942c';
        $api = new Mailchimp($apiKey);


        $optin = true; //yes, send optin emails
        $up_exist = true; // yes, update currently subscribed users
        $replace_int = false; // no, add interest, don't replace
        $batch = array();

        foreach ($this->getAllHtml() as $eid => $html) {
            $batch[] = array('email' => array('euid' => $eid), 'email_type' => null, 'merge_vars' => array('RECOMMENDS' => $html));
        }
//        var_dump($batch); die();
        $vals = $api->lists->batchSubscribe($listId,$batch,$optin, $up_exist, $replace_int);
        var_dump($vals);
    }

    public function getHtml($products)
    {
        if (!$this->_block) {
            Mage::app()->loadArea('frontend');
            $layout = Mage::getSingleton('core/layout');
            $this->_block = $layout->createBlock('avid_gravitator/mailchimp_recommendations');
        }
        return $this->_block->setProducts($products)->toHtml();
    }
}