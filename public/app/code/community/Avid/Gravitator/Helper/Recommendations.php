<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 06/11/2015
 * Time: 13:52
 */
class Avid_Gravitator_Helper_Recommendations extends Mage_Core_Helper_Abstract
{
    public function getRecommendationsUrl($entityType, $entityId, $recoType)
    {
        $base = Mage::helper('avid_gravitator')->getBaseUrl('jvt');
        return "{$base}{$entityType}s/{$entityId}/recommendations/{$recoType}";
    }
}