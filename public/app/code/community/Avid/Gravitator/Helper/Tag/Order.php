<?php

/**
 * @author Dane Lowe
 * @package Avid_Gravitator
 * @copyright Copyright (c) 2014 Avid Online Enterprises Limited
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Avid_Gravitator_Helper_Tag_Order extends Avid_Gravitator_Helper_Tag_Default
{
    /** @var  Mage_Sales_Model_Order */
    protected $_order;

    /**
     * Get Order
     *
     * @return Mage_Core_Model_Abstract|Mage_Sales_Model_Order
     */
    protected function _getOrder()
    {
        if (!$this->_order) {
            if ($orderId = Mage::getSingleton('checkout/session')->getLastOrderId()) {
                $this->_order = Mage::getModel('sales/order')->load($orderId);
            } else {
                $this->_order = Mage::getModel('sales/order');
            }
        }

        return $this->_order;
    }

    /**
     * Get a hash representation of email address
     *
     * To conceal the fact that it is an email address in URL
     *
     * @return string
     */
    protected function _getEmailHash()
    {
        $emailAddress = $this->_getOrder()->getCustomerEmail();
        if (
            !$emailAddress 
            && ($address = $this->_getOrder()->getBillingAddress())
            && ($email = $address->getEmail())
        ) {
            return base64_encode($email);
        } else {
            return parent::_getEmailHash();
        }
    }

    /**
     * @return array
     */
    protected function _getTagData()
    {
        $itemData = [];
        $skus     = [];
        $order    = $this->_getOrder();


        /** @var Mage_Sales_Model_Order_Item $_item */
        foreach ($order->getAllItems() as $_item) {
            $product = $_item->getProduct();
            if ($product->getParentId()) {
                continue;
            }
            $skus[]     = $product->getId();
            $itemData[] = [
                'sku'       => $product->getId(),
                'name'      => $product->getName(),
                'url'       => $product->getProductUrl(),
                'image_url' => Mage::helper('catalog/image')->init($product, 'image'),
                'price'     => $product->getPrice(),
            ];

        }

        return [
            'jvt' => [
                'order_place' => [
                    'order_id'    => $order->getIncrementId(),
                    'grand_total' => $order->getBaseGrandTotal(),
                    'skus'        => $skus,
                ],
            ],
            'gvt' => [
                'order_place' => [
                    'order_id'    => $order->getIncrementId(),
                    'grand_total' => $order->getBaseGrandTotal(),
                    'products'    => $itemData,
                ],
            ],
        ];
    }
}