<?php
/**
 * @author Dane Lowe
 * @package Avid_Gravitator
 * @copyright Copyright (c) 2014 Avid Online Enterprises Limited
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Avid_Gravitator_Helper_Tag_Product extends Avid_Gravitator_Helper_Tag_Default
{
    /** @var  Mage_Catalog_Model_Product */
    protected $_product;

    /** @var  string */
    protected $_categoryName;

    /**
     * Get Product
     *
     * @return Mage_Catalog_Model_Product|mixed
     */
    protected function _getProduct()
    {
        if (!$this->_product)
            $this->_product = Mage::registry('product');
        return $this->_product;
    }

    /**
     * Get Category Name
     * @return string
     */
    protected function _getCategoryName()
    {
        if (!$this->_categoryName) {
            if ($cat = Mage::registry('current_category')) {
                $this->_categoryName = $cat->getName();
            } else {
                $ids = $this->_getProduct()->getCategoryIds();
                $this->_categoryName =  Mage::getModel('catalog/category')->load($ids[0])->getName();
            }
        }
        return $this->_categoryName;
    }

    /**
     * @return array
     */
    protected function _getTagData()
    {
       $product = $this->_getProduct(); 
        return [
            'jvt' => ['product_view' => ['sku' =>  $product->getId(), 'category_name' => $this->_getCategoryName()]],
            'gvt' => ['product_view' => [
                    'category_name' => $this->_getCategoryName(),
                    'product'       => [
                        'sku'       => $product->getId(),
                        'name'      => $product->getName(),
                        'url'       => $product->getProductUrl(),
                        'image_url' => Mage::helper('catalog/image')->init($product, 'image'),
                        'price'     => $product->getPrice(),
                    ]
            ]]
        ];  
    }
}
