<?php

/**
 * @author Dane Lowe
 * @package Avid_Gravitator
 * @copyright Copyright (c) 2014 Avid Online Enterprises Limited
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Avid_Gravitator_Helper_Tag_Category extends Avid_Gravitator_Helper_Tag_Default
{
    /**
     * @return array
     */
    protected function _getTagData()
    {
        $category = Mage::registry('current_category');

        return ['gvt' => [
                'category_view' => [
                    'category' => [
                        'category_id' => $category->getId(),
                        'name'        => $category->getName(),
                        'url'         => $category->getUrl(),
                        'image_url'   => $category->getImageUrl(),
                    ],
                ],
            ],
        ];
    }
}