<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 16/06/16
 * Time: 2:29 PM
 */
class Avid_Gravitator_Helper_Tag_Default extends Mage_Core_Helper_Abstract
{
    protected $_tagData;

    public function getPxlData(string $service)
    {
        $data = [
            't' => base64_encode(json_encode($this->getTagData($service))),
            'cb' => uniqid(),
        ];
        if ($em = $this->getEmailHash()) {
            $data['em'] = $em;
        }
        $paramMap = ['id' => 'gvt_id', 'mc_cid' => 'mc_cid', 'mc_eid' => 'mc_eid'];
        foreach ($paramMap as $key => $param) {
            if ($v = $this->_getRequest()->getParam($param)) {
                $data[$key] = $v;
            }
        }
        return $data;
    }
    
    public function getTagData(string $service)
    {
        if (!$this->_tagData) {
            $this->_tagData = $this->_getTagData();
        }
        return $this->_tagData[$service] ?? false;
    }
    
    public function getEmailHash()
    {
        $emh = $this->_getEmailHash(); 
        Mage::helper('avid_gravitator')->setEmhCookie($emh);
        return $emh;
    }
    
    protected function _getEmailHash()
    {
        if ($e = $this->_getRequest()->getParam('gvt_em')) {
            return base64_encode($e);
        } elseif (
            ($e = $this->_getRequest()->getParam('gvt_emh'))
            || ($e = Mage::helper('avid_gravitator')->getEmhCookie())
        ) {
            return $e;
        }
    }

    protected function _getTagData()
    {
        return [];
    }

    protected function _saveCookie()
    {

    }
    
    protected function _readCookie($v)
    {}
    
}