<?php

/**
 * @author Dane Lowe
 * @package Avid_Gravitator
 * @copyright Copyright (c) 2014 Avid Online Enterprises Limited
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Avid_Gravitator_Helper_Tag_Cart extends Avid_Gravitator_Helper_Tag_Default
{
    protected function _getTagData()
    {
        $productData = [];
        /** @var Mage_Sales_Model_Quote_Item $_item */
        foreach (Mage::getSingleton('checkout/cart')->getQuote()->getAllItems() as $_item) {
            $product = $_item->getProduct();
            if ($product->getParentId()) {
                continue;
            }
            $productData[] = [
                'sku'       => $product->getId(),
                'name'      => $product->getName(),
                'url'       => $product->getProductUrl(),
                'image_url' => Mage::helper('catalog/image')->init($product, 'image'),
                'price'     => $product->getPrice(),
                'qty'       => $_item->getQty(),
            ];
        }

        return [
            'gvt' => [
                'cart_view' => [
                    'products' => $productData,
                    'cart_url' => Mage::helper('checkout/cart')->getCartUrl(),
                ],
            ],
        ];
    }
}