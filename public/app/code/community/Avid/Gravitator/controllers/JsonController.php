<?php
/**
 * @author Dane Lowe
 * @package Avid_Gravitator
 * @copyright Copyright (c) 2014 Avid Online Enterprises Limited
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Class Avid_Gravitator_JsonController
 *
 * Allows Mailchimp to get some extra information on a set of products.
 */
class Avid_Gravitator_JsonController extends Mage_Core_Controller_Front_Action
{
    public function extradataAction()
    {
        $skus = json_decode(base64_decode($this->getRequest()->getParam('enc')));
        $products = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('entity_id', array('in' => $skus))
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addAttributeToSelect('small_image')
            ;
        $data = array();
        foreach ($products as $_product) {
            $data[$_product->getId()] = array(
                'price'         => Mage::helper('core')->currency($_product->getPrice(), true, false),
                'final_price'   => Mage::helper('core')->currency($_product->getFinalPrice(), true, false),
                'media_url'     => Mage::helper('catalog/image')->init($_product, 'small_image')->resize(163, 163)->__toString()
            );
        }
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(json_encode($data));
    }
}
