<?php
/**
 * @author Dane Lowe
 * @package Avid_Gravitator
 * @copyright Copyright (c) 2014 Avid Online Enterprises Limited
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Avid_Gravitator_Block_Tag extends Mage_Core_Block_Template
{
    /**
     * To HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        return Mage::helper('avid_gravitator')->isEnabled() ? parent::_toHtml() : '';
    }

    /**
     * @return Avid_Gravitator_Helper_Tag_Default
     */
    public function getTagHelper()
    {
        return Mage::helper($this->getData('tag_helper')??'avid_gravitator/tag_default');
    }
    
//    public function getPxlUrl($service)
//    {
//        $data = $this->getTagHelper()->getPxlData($service);
//        if (($service == 'gvt') || ($data['t']??false)) {
//            return Mage::helper('avid_gravitator')->getBaseUrl($service).'pxl.template.js?' .http_build_query($data);
//        }
//    }
    
    
}