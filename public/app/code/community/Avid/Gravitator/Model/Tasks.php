<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 17/12/2015
 * Time: 09:37
 */
class Avid_Gravitator_Model_Tasks
{
    public function reindexAll()
    {
        Mage::getModel('avid_gravitator/indexer_product')->reindexAll();
        Mage::getModel('avid_gravitator/indexer_relationships')->reindexAll();
    }
}