<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 10/11/2015
 * Time: 09:07
 */
class Avid_Gravitator_Model_Resource_Indexer_Relationships extends Mage_Core_Model_Abstract
{

    protected $_linkTypes = array(
        Mage_Catalog_Model_Product_Link::LINK_TYPE_CROSSSELL    => 'cross_sell',
        Mage_Catalog_Model_Product_Link::LINK_TYPE_UPSELL       => 'up_sell'
    );

    public function reindexAll()
    {
        $this->reindex();
    }

    public function reindex($productIds = null)
    {
        $links = Mage::getResourceModel('catalog/product_link_collection')
            ->addExpressionFieldToSelect('linked_ids', 'GROUP_CONCAT({{linked_product_id}})', 'linked_product_id')
            ->addFieldToFilter('link_type_id', array('in' => array(array_keys($this->_linkTypes))));
        $links->getSelect()->group(array('product_id', 'link_type_id'));

        if ($productIds) {
            $links->addFieldToFilter('product_id', array('in' => $productIds));
        }

        $data = array();
        foreach ($links as $_link) {
            $_id = $_link->getProductId();
            $productData = isset($data[$_id]) ? $data[$_id] : array();
            $productData[$this->_linkTypes[$_link->getLinkTypeId()]] = explode(',', $_link->getLinkedIds());
            $data[$_id] = $productData;
        }
        $json = json_encode($data);

        foreach (Mage::app()->getStores() as $_store) {
            if (Mage::getStoreConfig('avid_gravitator/general/store_name', $_store->getId())) {
                $this->_sendData($json, $_store->getId());
            }
        }
    }

    protected function _sendData($json, $storeId)
    {
        $url = Mage::helper('avid_gravitator')->getBaseUrl('jvt', $storeId);
        $ch = curl_init($url.'products/bulk/relationships');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json),
            'Authentication: '.Mage::getStoreConfig('avid_gravitator/general/api_key', $storeId)
        ));
        if (!curl_exec($ch)) {
            Mage::throwException('Could not update');
        }
    }
}
