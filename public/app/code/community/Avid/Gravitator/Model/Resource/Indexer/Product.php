<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 10/11/2015
 * Time: 09:07
 */
class Avid_Gravitator_Model_Resource_Indexer_Product extends Mage_Core_Model_Abstract
{

    public function reindex($productIds = null)
    {
        foreach (Mage::app()->getStores() as $_store) {
            if (Mage::getStoreConfig('avid_gravitator/general/store_name', $_store->getId())) {
                $this->_reindex($_store->getId(), $productIds);
            }
        }
    }

    public function reindexAll()
    {
        return $this->reindex(null);
    }

    protected function _reindex($storeId, $productIds = null)
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $products */
        $products = Mage::getResourceModel('catalog/product_collection')
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->setVisibility(array(
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
            ))
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addAttributeToSelect(
                array('name', 'price', 'thumbnail', 'small_image', 'special_from_date', 'special_to_date',
                    'special_price'),
                'left'
            )
//            ->setPageSize(10)
//            ->setCurPage(1)
        ;
        if ($productIds) {
            $products->addIdFilter($productIds);
        }
        if (!$products->count()) {
            return;
        }

        Mage::helper('tradetested_catalog/url')->addRequestPathsToCollection($products, $storeId);

        $initialEnvironmentInfo = Mage::getSingleton('core/app_emulation')->startEnvironmentEmulation($storeId);
        $data = array();
        $imageHelper = Mage::helper('catalog/image');
        $dateModel = Mage::getModel('core/date');
        /** @var Mage_Catalog_Model_Product $_product */
        foreach ($products as $_product) {
            $data[] = [
                'sku'             => $_product->getId(),
                'sku1'            => $_product->getId(),
                'name'            => $_product->getName(),
                'url'             => $_product->getProductUrl(),
                'add_to_cart_url' => Mage::getUrl('checkout/cart/add', ['product' => $_product->getId()]),
                'price'           => $_product->getPrice() ? doubleval($_product->getPrice()) : null,
                'special_price'   => $_product->getSpecialPrice() ? doubleval($_product->getSpecialPrice()) : null,
                'special_from'    => $_product->getSpecialFromDate()
                    ? $dateModel->gmtTimestamp($_product->getSpecialFromDate()) : null,
                'special_to'      => $_product->getSpecialToDate()
                    ? ($dateModel->gmtTimestamp($_product->getSpecialToDate())+3600*24) : null,
                'images'          => [
                    'recolist'               => $imageHelper->init($_product, 'small_image')->keepFrame(false)
                        ->resize(134)->__toString(),
                    'recolist_retina'        => $imageHelper->init($_product, 'small_image')->keepFrame(false)
                        ->resize(268)->__toString(),
                    'thumb'                  => $imageHelper->init($_product, 'thumbnail')->keepFrame(false)->resize(50)
                        ->__toString(),
                    'thumb_retina'           => $imageHelper->init($_product, 'thumbnail')->keepFrame(false)
                        ->resize(100)->__toString(),
                    'cross_sell'             => $imageHelper->init($_product, 'small_image')->keepFrame(false)
                        ->resize(140)->__toString(),
                    'cross_sell_retina'      => $imageHelper->init($_product, 'small_image')->keepFrame(false)
                        ->resize(280)->__toString(),
                    'cross_sell_ajax'        => $imageHelper->init($_product, 'small_image')->keepFrame(false)
                        ->resize(160)->__toString(),
                    'cross_sell_ajax_retina' => $imageHelper->init($_product, 'small_image')->keepFrame(false)
                        ->resize(320)->__toString(),
                ]
            ];
        }
        Mage::getSingleton('core/app_emulation')->stopEnvironmentEmulation($initialEnvironmentInfo);
        $json = json_encode($data);

        $url = Mage::helper('avid_gravitator')->getBaseUrl('jvt', $storeId);
        if ($productIds) {
            $ch = curl_init($url.'products');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
        } else {
            $ch = curl_init($url.'products/bulk');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json),
            'Authentication: '.Mage::getStoreConfig('avid_gravitator/general/api_key', $storeId)
        ));
        if (!curl_exec($ch)) {
            Mage::throwException('Could not update: '.curl_error($ch));
        }
    }
}
