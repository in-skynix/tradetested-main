<?php
/**
 * Created by IntelliJ IDEA.
 * User: Dane
 * Date: 10/11/2015
 * Time: 08:30
 */
class Avid_Gravitator_Model_Indexer_Relationships extends Mage_Index_Model_Indexer_Abstract
{
    /**
     * Data key for matching result to be saved in
     */
    const EVENT_MATCH_RESULT_KEY = 'avid_gvt_index_product_relationships';

    protected $_matchedEntities = array(
        Mage_Catalog_Model_Product::ENTITY => array(
            Mage_Index_Model_Event::TYPE_SAVE,
            Mage_Index_Model_Event::TYPE_DELETE,
            Mage_Index_Model_Event::TYPE_MASS_ACTION,
            Mage_Index_Model_Event::TYPE_REINDEX
        )
    );

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('avid_gravitator/indexer_relationships');
    }

    /**
     * Get Indexer name
     *
     * @return string
     */
    public function getName()
    {
        return Mage::helper('avid_gravitator')->__('Gravitator Product Relationships');
    }

    /**
     * Get Indexer description
     *
     * @return string
     */
    public function getDescription()
    {
        return Mage::helper('tradetested_search')->__('Reindex Product Relationships in Gravitator');
    }

    /**
     * Register indexer required data inside event object
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        $event->addNewData(self::EVENT_MATCH_RESULT_KEY, true);
        if ($event->getEntity() == Mage_Catalog_Model_Product::ENTITY) {
            $product = $event->getDataObject();
            $event->addNewData('avid_gvt_index_rel_product_id', $product->getId());
        }
    }

    /**
     * Process event
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        $data = $event->getNewData();
        $productIds = empty($data['avid_gvt_index_rel_product_id']) ?
            null : array($data['avid_gvt_index_rel_product_id']);
        $this->reindex($productIds);
    }

    /**
     * @param null $pageIds
     * @return $this
     */
    public function reindex($productIds = null)
    {
        if ($productIds) {
            return $this->_getResource()->reindex($productIds);
        } else {
            return $this->_getResource()->reindexAll();
        }
    }
}