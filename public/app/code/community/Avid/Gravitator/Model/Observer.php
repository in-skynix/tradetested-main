<?php
/**
 * @author Dane Lowe
 * @package Avid_Gravitator
 * @copyright Copyright (c) 2014 Avid Online Enterprises Limited
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Avid_Gravitator_Model_Observer
{
    /**
     * When a user enters an email into a quote,
     *
     * keep the data in a cookie that can be picked up by GVT js
     *
     * @param Varien_Event_Observer $observer
     */
    public function checkQuoteSave(Varien_Event_Observer $observer)
    {
        $quote = $observer->getEvent()->getQuote();
        $emailAddress = $quote->getCustomerEmail();
        if (!$emailAddress && ($address = $quote->getBillingAddress()))
            $emailAddress = $address->getEmail();
        if (!$emailAddress) return;
        Mage::helper('avid_gravitator')->setEmhCookie(base64_encode($emailAddress));
    }

}