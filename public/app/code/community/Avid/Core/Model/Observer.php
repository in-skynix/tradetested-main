<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 23/05/13
 * Time: 11:12 AM
 * To change this template use File | Settings | File Templates.
 */
class Avid_Core_Model_Observer
{
    public function coreBlockAbstractToHtmlAfter(Varien_Event_Observer $observer)
    {
        $event = $observer->getEvent();
        $block = $event->getBlock();
        $transport = $event->getTransport();
        $transport->setHtml($transport->getHtml().$block->getChildHtml('avid_extra_html'));
    }

    /**
     * After loading a collection, add any association data to the items
     *
     * Data will be in the item as avid_assocations.{assoc_name}.{assoc_data_key}
     *
     * @param Varien_Event_Observer $observer
     */
    public function coreCollectionAbstractLoadAfter(Varien_Event_Observer $observer)
    {
        $collection = $observer->getEvent()->getCollection();
        $associations = Mage::helper('avid_core/association')->getAssociations($collection);
        $prefix = Avid_Core_Helper_Association::ASSOC_FLAG_PREFIX;
        $prefixLength = strlen($prefix);
        if(count($associations)) {
            foreach($collection->getItems() as $_item) {
                $itemAssocData = array();
                foreach ($_item->getData() as $_key => $_value) { /** @var Varien_Object $_item */
                    if ((strlen($_key) > $prefixLength) && (strrpos($_key, $prefix, -$prefixLength) !== FALSE)) {
                        list($prefix, $name, $assocKey) = explode('.', $_key);
                        if (isset($itemAssocData[$name])) {
                            $itemAssocData[$name][$assocKey] = $_value;
                        } else {
                            $itemAssocData[$name] = array($assocKey => $_value);
                        }
                        $_item->unsetData($_key);
                    }
                }
                foreach ($itemAssocData as $assocName => $assocData) {
                    if (isset($associations[$assocName]) && !$_item->getData($assocName)) {
                        $_item->setData($assocName, Mage::getModel($associations[$assocName])->setData($assocData));
                    }
                }
            }
        }

    }
}