<?php
/**
 * Created by PhpStorm.
 * User: Dane
 * Date: 08/12/14
 * Time: 10:50
 */
class Avid_Core_Helper_Association extends Mage_Core_Helper_Abstract
{
    const ASSOC_FLAG_PREFIX = 'avid_assocations.';

    /**
     * Include a Belongs-to association when loading a collection
     *
     * @param Mage_Core_Model_Resource_Db_Collection_Abstract $collection
     * @param string $name
     * @param string $entityType
     * @param string $condition
     * @return Mage_Core_Model_Resource_Db_Collection_Abstract
     */
    public function includeBelongsToAssociation(Mage_Core_Model_Resource_Db_Collection_Abstract $collection, $name, $entityType, $condition)
    {
        if (!$this->_getAssociation($collection, $name)) {
            $aliases = array();
            foreach ($collection->getConnection()->describeTable($collection->getTable($entityType)) as $_column) {
                $aliases[self::ASSOC_FLAG_PREFIX."{$name}.{$_column['COLUMN_NAME']}"] = $_column['COLUMN_NAME'];
            }
            $collection->join(
                array($name => $entityType),
                $condition,
                $aliases
            );
            $this->_addAssociation($collection, $name, $entityType);
        }
        return $collection;
    }

    /**
     * Set the associations that the collection is already joined to
     *
     * @param Mage_Core_Model_Resource_Db_Collection_Abstract $collection
     * @return array
     */
    public function getAssociations(Mage_Core_Model_Resource_Db_Collection_Abstract $collection)
    {
        $assoc = $collection->getFlag(self::ASSOC_FLAG_PREFIX);
        return is_array($assoc) ? $assoc : array();
    }

    /**
     * Get the associations that the collection is already joined to
     *
     * @param Mage_Core_Model_Resource_Db_Collection_Abstract $collection
     * @param array $value
     * @return $this
     */
    public function _setAssociations(Mage_Core_Model_Resource_Db_Collection_Abstract $collection, $value = array())
    {
        $collection->setFlag(self::ASSOC_FLAG_PREFIX, $value);
        return $this;
    }

    /**
     * Add flag for association that collection is joined to
     * @param Mage_Core_Model_Resource_Db_Collection_Abstract $collection
     * @param string $name
     * @param string $value
     * @return $this
     */
    protected function _addAssociation(Mage_Core_Model_Resource_Db_Collection_Abstract $collection, $name, $value)
    {
        $assoc = $this->getAssociations($collection);
        $assoc[$name] = $value;
        return $this->_setAssociations($collection, $assoc);
    }

    /**
     * get flag for existing assocation
     *
     * @param Mage_Core_Model_Resource_Db_Collection_Abstract $collection
     * @param string $name
     * @return string|null
     */
    protected function _getAssociation(Mage_Core_Model_Resource_Db_Collection_Abstract $collection, $name)
    {
        $assoc = $this->getAssociations($collection);
        return isset($assoc[$name]) ? $assoc[$name] : null;
    }
}