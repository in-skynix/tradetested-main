<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dane
 * Date: 23/05/13
 * Time: 11:33 AM
 * To change this template use File | Settings | File Templates.
 */
class Avid_Core_Block_Html_Head extends Mage_Core_Block_Template
{
    protected $_items = array();
    public function addItem($type, $value)
    {
        if (!isset($this->_items[$type]))
            $this->_items[$type] = array();
        $this->_items[$type][] = $value;
        return $this;
    }
    public function getItems($type)
    {
        if ($type)
            return (isset($this->_items[$type])) ? $this->_items[$type] : array();
        return $this->_items;
    }
}