<?php

$installer = $this;

$installer->startSetup();
try {
    $installer->run("
        ALTER TABLE {$this->getTable('salesrule_coupon')} DROP INDEX UNQ_COUPON_CODE;
    ");
} catch (Exception $e) {
    Mage::logException($e);
}

$installer->endSetup();
