<?php

$installer = $this;
$installer->startSetup();
try {
    $installer->run("
        SET GLOBAL group_concat_max_len=9999999;
    ");
} catch (Exception $e) {
    Mage::logException($e);
}

$installer->endSetup();