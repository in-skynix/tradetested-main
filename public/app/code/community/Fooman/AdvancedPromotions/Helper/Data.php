<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Helper_Data extends Mage_Core_Helper_Abstract {

    const DEBUG_LEVEL = false;
    const LOG_FILE_NAME = 'fooman_advancedpromotions.log';
    const XML_PATH_ADVANCEDPROMO_SETTINGS = 'foomanadvancedpromotions/settings/';

    const RULE_MATCH_RETURNFORMAT_ITEMS = 'matched_items';
    const RULE_MATCH_RETURNFORMAT_WHAT_MATCHED ='items_and_number_unprocessed';
    const RULE_MATCH_RETURNFORMAT_DEBUG ='debug';

    //for items grouped by attribute A discount by items grouped by attribute A
    const ACTION_GROUP_MATCHED = 'fooman_advancedpromotions/salesRule_rule_action_groupMatched';
    //for items grouped by attribute A discount different items
    const ACTION_GROUP_INDEPENDANT = 'fooman_advancedpromotions/salesRule_rule_action_groupIndependant';
    //for items grouped
    const ACTION_GROUP_SIMPLE = 'fooman_advancedpromotions/salesRule_rule_action_groupSimple';
    //for auto adding items
    const ACTION_GROUP_AUTOADD = 'fooman_advancedpromotions/salesRule_rule_action_autoAdd';

    const CONDITION_PRODUCT_GROUP_INDEPENDANT = 'fooman_advancedpromotions/salesRule_rule_condition_product_groupIndependant';
    const CONDITION_PRODUCT_GROUP_SIMPLE = 'fooman_advancedpromotions/salesRule_rule_condition_product_groupSimple';
    const CONDITION_PRODUCT_COMBINE = 'fooman_advancedpromotions/salesRule_rule_condition_product_combine';

    const PROMO_ARRAY_PREFIX = 'VALUE-';
    const CACHE_TAG_PROMOENGINE_PRODUCT = 'promoengine_product';

    const KEY_ADD_LATER ='add_later';
    const KEY_ADD_RULES = 'add_rules';
    const KEY_ADD_CERT_LATER ='add_cert_later';
    const KEY_SUBTOTAL_RULES = 'subtotal_rules';
    const KEY_QTY_RULES = 'qty_rules';
    const KEY_FIXED_RULES = 'fixed_rules';

    const FOOMAN_ADVPROMO_SALESRULES_CACHETAG = 'fooman_advpromo_salesrule';

    public function saveFixedRule($ruleId, $value)
    {
        $this->_savedCalculations[self::KEY_FIXED_RULES][$ruleId] = $value;
    }

    public function loadFixedRules()
    {
        return $this->loadCalculation(self::KEY_FIXED_RULES);
    }

    public function saveAddLater($ruleId, $sku, $value)
    {
        $this->_savedCalculations[self::KEY_ADD_LATER][$ruleId][$sku] = $value;
    }

    public function loadAddLaters()
    {
        return $this->loadCalculation(self::KEY_ADD_LATER);
    }

    public function saveAddRule($ruleId, $key, $value)
    {
        $this->_savedCalculations[self::KEY_ADD_RULES][$ruleId][$key] = $value;
    }

    public function removeAddRule($ruleId, $key)
    {
        if (isset($this->_savedCalculations[self::KEY_ADD_RULES][$ruleId][$key])) {
            unset($this->_savedCalculations[self::KEY_ADD_RULES][$ruleId][$key]);
        }
    }

    public function loadAddRules()
    {
        return $this->loadCalculation(self::KEY_ADD_RULES);
    }

    public function saveAddCertLater($key, $value)
    {
        if(isset($this->_savedCalculations[self::KEY_ADD_CERT_LATER][$key]) ) {
            $this->_savedCalculations[self::KEY_ADD_CERT_LATER][$key] = max($this->_savedCalculations[self::KEY_ADD_CERT_LATER][$key],$value);
        } else {
            $this->_savedCalculations[self::KEY_ADD_CERT_LATER][$key] = $value;
        }
    }

    public function loadAddCertLaters()
    {
        return $this->loadCalculation(self::KEY_ADD_CERT_LATER);
    }

    public function saveSubtotalRule($ruleId, $value)
    {
        $this->_savedCalculations[self::KEY_SUBTOTAL_RULES][$ruleId] = $value;
    }

    public function loadSubtotalRules()
    {
        return $this->loadCalculation(self::KEY_SUBTOTAL_RULES);
    }

    public function saveQtyRule($ruleId, $value)
    {
        $this->_savedCalculations[self::KEY_QTY_RULES][$ruleId] = $value;
    }

    public function loadQtyRules()
    {
        return $this->loadCalculation(self::KEY_QTY_RULES);
    }

    private $_savedCalculations = array();

    public function saveCalculation($key, $calc)
    {
        $this->_savedCalculations[$key] = $calc;
    }

    public function loadCalculation($key)
    {
        if (isset($this->_savedCalculations[$key])) {
            return $this->_savedCalculations[$key];
        }
        return false;
    }

    public function resetCalculations()
    {
        $this->_savedCalculations = array();
    }

    public function formattedReturnValue($returnFormat,$ruleType,$ruleQty,$matches)
    {
        //$matches = $this->removeEmptyMatches($matches);
        switch ($returnFormat) {
            case self::RULE_MATCH_RETURNFORMAT_ITEMS:
                return $matches;
                break;
            case self::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED:
                return array('rule_type'=>$ruleType,'rule_qty'=>$ruleQty,'matches'=>$matches);
                break;
            case self::RULE_MATCH_RETURNFORMAT_DEBUG:
                $onlyItemNames = array();
                foreach ($matches as $key=>$match) {
                    if (is_object($match)){
                        $onlyItemNames[$key] = $match->getQty().' x '.$match->getName();
                    }else {
                        $onlyItemNames[$key] = $match;
                    }
                }
                return array('rule_type'=>$ruleType,'rule_qty'=>$ruleQty,'matches'=>$onlyItemNames);
                break;
        }
    }

    public function formattedQuoteItem(&$matches, $ruleMatchesXtimes, $quoteItem, $groupBy, $groupIdentifier, $multiplier=1, $attrValue = '', $debug=false){

            $itemKey = $this->getItemKey($quoteItem);
            if ($groupBy === true) {
                if(!isset($matches['group'][$groupIdentifier][$itemKey])){
                    $ruleMatchesXtimes += floor($quoteItem->getQty()/$multiplier);
                    $matches['group'][$groupIdentifier][$itemKey] = array(
                                'rule_qty'  => floor($quoteItem->getQty()/$multiplier),
                                'item' => $debug?$quoteItem->getName():$quoteItem
                                    );
                }
            }
            elseif ($groupBy) {
                if ($quoteItem->getOptionByCode('simple_product')) {
                    $product = $this->loadProduct($quoteItem->getOptionByCode('simple_product')->getValue());
                } else {
                    $product = $this->loadProduct($quoteItem->getProductId());
                }
                $productGroupAttributeValues = array();
                if($groupBy == 'category_ids') {
                    foreach ($product->getCategoryIds() as $productCategoryId) {
                        $productGroupAttributeValues[] = Fooman_AdvancedPromotions_Helper_Data::PROMO_ARRAY_PREFIX.$productCategoryId;
                    }
                }else {
                    $productGroupAttributeValues[] = Fooman_AdvancedPromotions_Helper_Data::PROMO_ARRAY_PREFIX.$product->getData($groupBy);
                }
                foreach ($productGroupAttributeValues as $productGroupAttributeValue) {
                    $arrayKey = $groupBy;
                    //filter out results with different attrValue
                    if ($attrValue == '' || $attrValue == $productGroupAttributeValue) {
                        if(strpos($groupIdentifier, 'simple') === false){
                            $arrayKey = $groupBy.'|'.$groupIdentifier;
                        }
                        $matches[$arrayKey][$productGroupAttributeValue][$itemKey] = array(
                            'rule_qty' => floor($quoteItem->getQty()/$multiplier),
                            'item' => $debug?$quoteItem->getName():$quoteItem
                                );
                    }
                    $ruleMatchesXtimes = $this->mergeQty(
                        $ruleMatchesXtimes,
                        array(
                            $arrayKey => array(
                                $productGroupAttributeValue => floor(
                                    $quoteItem->getQty() / $multiplier
                                )
                            )
                        ),
                        'max'
                    );
                }
            }else {
                $matches[$itemKey] = array(
                            'rule_qty' => floor($quoteItem->getQty()/$multiplier),
                            'item' => $debug?$quoteItem->getName():$quoteItem
                                );
                $ruleMatchesXtimes += floor($quoteItem->getQty()/$multiplier);
            }

            return $ruleMatchesXtimes;
    }


    public function loadProduct ($productId = null)
    {
        //using cache speeds up execution by 3
        //but also increases memory footprint in the unserialize directive
        if ($productId) {
            $cacheTags = array(
                Mage_Catalog_Model_Product::CACHE_TAG,
                Mage_Catalog_Model_Product::CACHE_TAG . "_" . $productId);
            $cacheKey = self::CACHE_TAG_PROMOENGINE_PRODUCT . '_' . $productId;
            $product = unserialize(Mage::app()->loadCache($cacheKey));
            if (!$product) {
                $product = Mage::getModel('catalog/product')->load($productId);
                Mage::app()->saveCache(serialize($product), $cacheKey, $cacheTags);
            }
            return $product;
        }
    }

    public function debug ($msg, $level, $returnFormat = self::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED, $identifier = null)
    {
        if (self::DEBUG_LEVEL === false || $returnFormat == self::RULE_MATCH_RETURNFORMAT_DEBUG){
            return false;
        }
        if (self::DEBUG_LEVEL >= $level) {
            if($identifier) {
                Mage::log('---------------------- '.$identifier.' START ----------------------',$level, self::LOG_FILE_NAME);
            }
            Mage::log($msg, $level, self::LOG_FILE_NAME);
            if($identifier) {
                Mage::log('---------------------- '.$identifier.' END ----------------------',$level, self::LOG_FILE_NAME);
            }
        }
    }

    public function isDebugMode() {
        return (bool)self::DEBUG_LEVEL;
    }

    protected function _onlyMatchedItemsSub($inputArray, $count, &$returnArray)
    {
        foreach ($inputArray as $attrKey => $attrCollected) {
            foreach ($attrCollected as $attrValue => $items) {
                if(is_array($items)){
                    foreach ($items as $quoteItemId => $item) {
                        if ($count) {
                            if (!isset($returnArray[$quoteItemId])) {
                                $returnArray[$quoteItemId] = 1;
                            } else {
                                $returnArray[$quoteItemId]++;
                            }
                        } else {
                            $returnArray[$quoteItemId] = $item;
                        }
                    }
                }
            }
        }
        if($count){
            $this->debug($returnArray, Zend_Log::EMERG);
        }
        return $returnArray;
    }
    public function onlyMatchedItems($inputArray = array(), $count = false, $global = false)
    {
        $returnArray = array();
        if (!empty($inputArray)) {
            if (isset($inputArray['matches']) && is_array($inputArray['matches'])) {
                $inputArray = $inputArray['matches'];
            }
            if ($global) {
                foreach ($inputArray as $inputArraySub) {
                    $this->_onlyMatchedItemsSub($inputArraySub['matches'], $count, $returnArray);
                }
            } else {
                $this->_onlyMatchedItemsSub($inputArray, $count, $returnArray);
            }
        }
        return $returnArray;
    }

    public function mergeTmpMatches ($inputArray = array(), $mode = 'min', $groupBy = false, $groupIdentifier = 'simple')
    {
        $ruleQty = false;
        $matches = array();
        $ruleType = false;
        foreach ($inputArray as $tmpMatch) {
            if (!$ruleType && $tmpMatch['rule_type']) {
                //take first encountered rule type
                $ruleType = $tmpMatch['rule_type'];
            }
            $ruleQty = $this->mergeQty($ruleQty, $tmpMatch['rule_qty'], $mode, $groupBy, $groupIdentifier);
            $matches = $this->mergeMatches($matches, $tmpMatch['matches'], $mode);
        }
        return $this->formattedReturnValue(self::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED, $ruleType, $ruleQty, $matches);
    }

    public function mergeMatches($existingMatches, $newMatches, $mode)
    {
        foreach ($newMatches as $groupKey=>$groups) {
            foreach ($groups as $attrKey=>$items){
                foreach ($items as $quoteItemId=>$item) {
                    $existingMatches[$groupKey][$attrKey][$quoteItemId] = $item;
                }
            }
        }
        return $existingMatches;
    }

    public function mergeQty($existingQty, $newQty, $mode, $groupBy = false, $groupIdentifier = 'simple')
    {
        if ($groupBy === false || $groupBy == 1 ||  $groupBy == '') {
            $groupBy = 'group';
        }
        if (is_array($newQty) || is_array($existingQty)) {
            if (!is_array($existingQty)) {
                if($existingQty){
                    $existingQty = array($groupBy => array($groupIdentifier => $existingQty));
                } else {
                    $existingQty = array();
                }
            }
            if (!is_array($newQty)) {
                $newQty = array($groupBy => array($groupIdentifier => $newQty));
            }
            foreach ($existingQty as $attrKey => $attrGroup) {
                foreach ($attrGroup as $attrValue => $existingValue) {
                    if(isset($existingQty[$attrKey][$attrValue]) && isset($newQty[$attrKey][$attrValue])) {
                        switch ($mode) {
                            case 'min':
                                $existingQty[$attrKey][$attrValue] = min($existingQty[$attrKey][$attrValue], $newQty[$attrKey][$attrValue]);
                                break;
                            case 'max':
                                $existingQty[$attrKey][$attrValue] = max($existingQty[$attrKey][$attrValue], $newQty[$attrKey][$attrValue]);
                                break;
                            case 'add':
                                $existingQty[$attrKey][$attrValue] += $newQty[$attrKey][$attrValue];
                                break;
                        }
                        unset($newQty[$attrKey][$attrValue]);
                    }
                }
            }
            return array_merge_recursive($existingQty, $newQty);
        } else {
            switch ($mode) {
                case 'min':
                    if($existingQty === false) {
                        return $newQty;
                    }
                    return min($existingQty, $newQty);
                    break;
                case 'max':
                    if($existingQty === false) {
                        return $newQty;
                    }
                    return max($existingQty, $newQty);
                    break;
                case 'add':
                    return $existingQty + $newQty;
                    break;
            }
        }
    }

    public function removeDoubles ($inputArray = array())
    {
        $processedItems = array();
        $returnArray = array('rule_type'=> array(), 'rule_qty'=> array(), 'matches'=> array());
        if (!empty($inputArray)) {
            if($this->hasMatches($inputArray)){
                $returnArray['rule_type']= $inputArray['rule_type'];
                $returnArray['rule_qty']= $inputArray['rule_qty'];
                if (is_array($inputArray['matches'])) {
                    foreach ($inputArray['matches'] as $attrKey => $attrCollected) {
                        foreach ($attrCollected as $attrValue => $items) {
                            foreach ($items as $quoteItemId => $item) {
                                if(!isset($processedItems[$quoteItemId])) {
                                    $returnArray['matches'][$attrKey][$attrValue][$quoteItemId] = $item;
                                    $processedItems[$quoteItemId] = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $returnArray;
    }
    
    public function getAutoAddToHistory()
    {
        $autoAddHistory = Mage::getSingleton('core/session')->getFoomanAutoAddToHistory();
        if(!$autoAddHistory) {
            $autoAddHistory = array();
        } else {
            $autoAddHistory = unserialize($autoAddHistory);
        }
        return $autoAddHistory;
    }
    
    public function getItemKey ($quoteItem)
    {
        if ($quoteItem->getId()) {
            return 'q' . $quoteItem->getId();
        } else {
            return 'q' . $quoteItem->getQuoteId() . $quoteItem->getProductId();
        }
    }
    
    public function doItemsMatch ($item1, $item2)
    {
        if($item2 instanceof Mage_Sales_Model_Quote_Address_Item) {
            $item2 = $item2->getQuoteItem();
        }
        return $this->getItemKey($item1) == $this->getItemKey($item2);
    }

    public function hasMatches($inputArray)
    {
        if(!isset($inputArray['rule_qty']) || empty($inputArray['rule_qty'])){
            return false;
        }
        if(!isset($inputArray['matches']) || empty($inputArray['matches']) || !is_array($inputArray['matches'])){
            return false;
        }
        return true;
    }

    public function reduceQty($input, $mode = 'min')
    {
        if(!is_array($input)){
            return $input;
        }

        $nrGroupFree = 0;
        //tally up all matches recursively
        $ruleMatchesIterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($input));
        foreach($ruleMatchesIterator as $value) {
            switch ($mode) {
            case 'min':
                if($nrGroupFree > 0) {
                    $nrGroupFree = min ($nrGroupFree,$value);
                } else {
                    $nrGroupFree = $value;
                }
                break;
            case 'max':
                $nrGroupFree = max($nrGroupFree, $value);
                break;
            case 'add':
                $nrGroupFree += $value;
                break;
            }

        }
        return $nrGroupFree;
    }

    public function addAppliedRuleId($object, $id, $fooman=false)
    {
        $field = 'applied_rule_ids';
        if($fooman){
            $field = 'fooman_'.$field;
        }
        if($object->getData($field)){
            $current = explode(',',$object->getData($field));
        } else {
            $current = array();
        }
        $key = array_search($id, $current);
        if (false === $key && $id) {
            $current[] = $id;
        }
        $object->setData($field, implode(',',$current));
    }

    public function removeAppliedRuleId($object, $id, $fooman=false)
    {
        $field = 'applied_rule_ids';
        if($fooman){
            $field = 'fooman_'.$field;
        }
        $current = explode(',',$object->getData($field));
        $key = array_search($id, $current);

        if (false !== $key) {
            unset($current[$key]);
        }

        $object->setData($field, implode(',',$current));
    }

    public function isRuleSuperRule($rule)
    {
        if (is_int($rule)) {
            $ruleId = $rule;
        } else {
            $ruleId = $rule->getId();
        }
        $cacheId = sprintf('%s_%s', self::FOOMAN_ADVPROMO_SALESRULES_CACHETAG, $ruleId);
        $ruleSimpleAction = Mage::app()->loadCache($cacheId);
        if (!$ruleSimpleAction) {
            if (is_int($rule)) {
                $rule = Mage::getModel('salesrule/rule')->load($rule);
            }
            if ($rule->getSimpleAction()) {
                $ruleSimpleAction = $rule->getSimpleAction();
                Mage::app()->saveCache(
                    $ruleSimpleAction,
                    $cacheId,
                    array(
                        self::FOOMAN_ADVPROMO_SALESRULES_CACHETAG
                    )
                );
            }
        }
        return substr($ruleSimpleAction, 0, 6) == 'super_';
    }

    public function removeInsufficientMatches($inputArray, $nrConditions)
    {
        $returnArray = array();
        $counted = $this->onlyMatchedItems($inputArray, true, true);
        foreach ($inputArray as $tmpMatch) {
            $oneMatchFound = false;
            foreach ($tmpMatch['matches'] as $attrKey => $attrCollected) {
                foreach ($attrCollected as $attrValue => $items) {
                    foreach ($items as $quoteItemId => $item) {
                        if ($counted[$quoteItemId] < $nrConditions) {
                            unset($tmpMatch['matches'][$attrKey][$attrValue][$quoteItemId]);
                        } else {
                            $oneMatchFound = true;
                        }
                    }
                }
            }
            if ($oneMatchFound) {
                $returnArray[] = $tmpMatch;
            }
        }
        return $returnArray;
    }

    protected function tallyUpMatches($input)
    {
        $matches = 0;
        $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($input));
        foreach ($iterator as $value) {
            $matches += $value;
        }
        return $matches;
    }

    public function getItemPrice($item, $base = true)
    {
        if ($base) {
            if (Mage::getStoreConfigFlag(Fooman_AdvancedPromotions_Helper_Data::XML_PATH_ADVANCEDPROMO_SETTINGS . 'original_price')
            ) {
                $itemPrice = $item->getProduct()->getBasePrice();
                if($itemPrice == 0 && $item->getProduct()->getPrice() > 0) {
                    //Mage bug - base price can be zero even if it shouldn't be
                    $itemPrice = $item->getStore()->convertPrice($item->getProduct()->getPrice());
                }
            } else {
                $itemPrice = ($item->getBaseDiscountCalculationPrice() !== null)
                    ? $item->getBaseDiscountCalculationPrice() : $item->getBaseCalculationPrice();
            }
        } else {
            if (Mage::getStoreConfigFlag(Fooman_AdvancedPromotions_Helper_Data::XML_PATH_ADVANCEDPROMO_SETTINGS . 'original_price')
            ) {
                $itemPrice = $item->getProduct()->getPrice();
            } else {
                $itemPrice  = ($item->getDiscountCalculationPrice()!==null)?$item->getDiscountCalculationPrice():$item->getCalculationPrice();
            }
        }
        return $itemPrice;
    }
}
