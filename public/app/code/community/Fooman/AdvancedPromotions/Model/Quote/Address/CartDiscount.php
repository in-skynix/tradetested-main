<?php
class Fooman_AdvancedPromotions_Model_Quote_Address_CartDiscount extends Mage_Sales_Model_Quote_Address_Total_Abstract
{

    public function __construct()
    {
        $this->setCode('fooman_cartdiscount');
    }

    /**
     * delayed processing for cart discounts
     *
     * @param Mage_Sales_Model_Quote_Address $address
     *
     * @return Fooman_AdvancedPromotions_Model_Quote_Address_AdvancedPromo
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        return $this;
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        return $this;
    }

}
