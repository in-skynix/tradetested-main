<?php
class Fooman_AdvancedPromotions_Model_Quote_Address_AdvancedPromo extends Mage_Sales_Model_Quote_Address_Total_Abstract
{

    const DISCOUNT_DISTRIBUTION_ON_PRODUCT = 'on-product';
    const DISCOUNT_DISTRIBUTION_PER_ITEM = 'per-item';
    const DISCOUNT_DISTRIBUTION_PER_CURRENCY_UNIT = 'per-currency-unit';

    public function __construct()
    {
        $this->setCode('fooman_advancedpromo');
    }

    /**
     * since Magento sets applied_rule_ids to all items that validate
     * we clean up here all items that were validated by an advanced rule
     * but did not play a rule in either a condition or action
     *
     * also trigger discount redistribution
     *
     * @param Mage_Sales_Model_Quote_Address $address
     *
     * @return Fooman_AdvancedPromotions_Model_Quote_Address_AdvancedPromo
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        if (method_exists($this, '_getAddressItems')) {
            $items = $this->_getAddressItems($address);
        } else {
            $items = $address->getAllItems();
        }

        if (!count($items)) {
            return $this;
        }
        $redistributeDiscountItems = array();
        $quoteRuleIds = array();
        foreach ($items as $item) {
            $foomanAppliedRuleIds = explode(',', $item->getFoomanAppliedRuleIds());
            foreach (explode(',', $item->getAppliedRuleIds()) as $ruleId) {
                $isAdvanced = Mage::helper('fooman_advancedpromotions')->isRuleSuperRule((int)$ruleId);
                if ($isAdvanced) {
                    $quoteRuleIds[$ruleId] = true;
                }
                if (!isset($redistributeDiscountItems[$ruleId])) {
                    $redistributeDiscountItems[$ruleId]['base_discount_amount'] = 0;
                    $redistributeDiscountItems[$ruleId]['discount_amount'] = 0;
                    $redistributeDiscountItems[$ruleId]['base_total'] = 0;
                    $redistributeDiscountItems[$ruleId]['total'] = 0;
                    $redistributeDiscountItems[$ruleId]['auto_added_qty'] = 0;
                }

                $removed = false;
                if (array_search($ruleId, $foomanAppliedRuleIds) === false && $isAdvanced) {
                    Mage::helper('fooman_advancedpromotions')->removeAppliedRuleId($item, $ruleId);
                    $removed = true;
                }
                if (!$removed && $item->getFoomanActiveInGroup()) {
                    $redistributeDiscountItems[$ruleId]['items'][] = $item;
                    $redistributeDiscountItems[$ruleId]['base_discount_amount'] += $item->getBaseDiscountAmount();
                    $redistributeDiscountItems[$ruleId]['discount_amount'] += $item->getDiscountAmount();
                    $redistributeDiscountItems[$ruleId]['base_total'] += $item->getBaseRowTotal();
                    $redistributeDiscountItems[$ruleId]['total'] += $item->getRowTotal();
                    $redistributeDiscountItems[$ruleId]['auto_added_qty'] += $item->getFoomanAutoAddedQty();
                }
            }
        }

        //check for advanced matched rules that do not have any items
        $quoteAppliedRuleIds = explode(',', $address->getQuote()->getAppliedRuleIds());
        if($quoteAppliedRuleIds){
            foreach ($quoteAppliedRuleIds as $quoteAppliedRuleId) {
                if(!isset($quoteRuleIds[$quoteAppliedRuleId])
                    && Mage::helper('fooman_advancedpromotions')->isRuleSuperRule((int)$quoteAppliedRuleId)){
                    $quoteRuleIds[$quoteAppliedRuleId] = $quoteAppliedRuleId;
                }
            }
        }

        //TODO confirm behaviour for items in multiple rules
        if (!empty($redistributeDiscountItems)) {
            foreach ($redistributeDiscountItems as $rule) {
                if (isset($rule['items'])) {
                    $this->_redistributeDiscount($rule);
                }
            }
        }

        foreach ($quoteRuleIds as $quoteRuleId => $value) {
            if (!isset($redistributeDiscountItems[$quoteRuleId])
                || ($redistributeDiscountItems[$quoteRuleId]['base_discount_amount'] == 0
                    && $redistributeDiscountItems[$quoteRuleId]['auto_added_qty'] == 0
                )
            ) {
                $this->removeLabel($address, $quoteRuleId);
            }
        }
        return $this;
    }

    public function removeLabel(Mage_Sales_Model_Quote_Address $address, $quoteRuleId)
    {
        Mage::helper('fooman_advancedpromotions')->removeAppliedRuleId($address->getQuote(), $quoteRuleId);
        $rule = Mage::getModel('salesrule/rule')->load($quoteRuleId);
        $label = $rule->getStoreLabel($address->getQuote()->getStore());
        $description = $address->getDiscountDescriptionArray();

        if (is_array($description)) {
            $key = array_search($label, $description);
            if ($key !== false) {
                unset($description[$key]);
                $address->setDiscountDescriptionArray(array_unique($description));
                $address->setDiscountDescription(implode(',', array_unique($description)));
            }
        }
    }

    /**
     * @param Mage_Sales_Model_Quote_Address $address
     *
     * @return Fooman_AdvancedPromotions_Model_Quote_Address_AdvancedPromo
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        return $this;
    }

    protected function _redistributeDiscount($rule)
    {
        $mode = Mage::getStoreConfig(
            Fooman_AdvancedPromotions_Helper_Data::XML_PATH_ADVANCEDPROMO_SETTINGS . 'discount_distribution'
        );

        switch ($mode) {
            case self::DISCOUNT_DISTRIBUTION_ON_PRODUCT:
                return;
            case self::DISCOUNT_DISTRIBUTION_PER_ITEM:
                $numberOfItems = count($rule['items']);
                if ($numberOfItems) {
                    $discountPerItem = $rule['discount_amount'] / $numberOfItems;
                    $baseDiscountPerItem = $rule['base_discount_amount'] / $numberOfItems;
                    $maxPricedItem = false;
                    foreach ($rule['items'] as $item) {
                        $item->setBaseDiscountAmount(
                            Mage::app()->getStore()->roundPrice(
                                min($item->getBaseRowTotal(), $baseDiscountPerItem)
                            )
                        );
                        $rule['base_discount_amount'] -= $item->getBaseDiscountAmount();

                        $item->setDiscountAmount(
                            Mage::app()->getStore()->roundPrice(
                                min($item->getRowTotal(), $discountPerItem)
                            )
                        );
                        $rule['discount_amount'] -= $item->getDiscountAmount();

                        if (!$maxPricedItem) {
                            $maxPricedItem = $item;
                        } else {
                            if ($item->getBaseRowTotal() > $maxPricedItem->getBaseRowTotal()) {
                                $maxPricedItem = $item;
                            }
                        }
                    }

                    //if any amount is left over add it to the last item
                    $maxPricedItem->setBaseDiscountAmount(
                        $maxPricedItem->getBaseDiscountAmount() + $rule['base_discount_amount']
                    );
                    $maxPricedItem->setDiscountAmount($maxPricedItem->getDiscountAmount() + $rule['discount_amount']);
                }
                break;
            case self::DISCOUNT_DISTRIBUTION_PER_CURRENCY_UNIT:
                if ($rule['total']) {
                    $maxPricedItem = false;
                    $discountPerCurrencyUnit = $rule['discount_amount'] / $rule['total'];
                    $baseDiscountPerCurrencyUnit = $rule['base_discount_amount'] / $rule['base_total'];
                    foreach ($rule['items'] as $item) {
                        $item->setDiscountAmount(
                            Mage::app()->getStore()->roundPrice(
                                $item->getRowTotal() * $discountPerCurrencyUnit
                            )
                        );
                        $rule['discount_amount'] -= $item->getDiscountAmount();

                        $item->setBaseDiscountAmount(
                            Mage::app()->getStore()->roundPrice(
                                $item->getBaseRowTotal() * $baseDiscountPerCurrencyUnit
                            )
                        );
                        $rule['base_discount_amount'] -= $item->getBaseDiscountAmount();
                        if (!$maxPricedItem) {
                            $maxPricedItem = $item;
                        } else {
                            if ($item->getRowTotal() > $maxPricedItem->getRowTotal()) {
                                $maxPricedItem = $item;
                            }
                        }
                    }

                    //if any amount is left over add it to the last item
                    $maxPricedItem->setBaseDiscountAmount(
                        $maxPricedItem->getBaseDiscountAmount() + $rule['base_discount_amount']
                    );
                    $maxPricedItem->setDiscountAmount($maxPricedItem->getDiscountAmount() + $rule['discount_amount']);
                }
                break;
        }
    }
}
