<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule extends Mage_SalesRule_Model_Rule
{

    public function getActionsInstance ()
    {
        return Mage::getModel('fooman_advancedpromotions/salesRule_rule_condition_product_combine');
    }

}