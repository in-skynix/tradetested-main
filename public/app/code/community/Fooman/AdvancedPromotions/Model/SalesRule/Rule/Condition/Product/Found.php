<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_Found
    extends Mage_SalesRule_Model_Rule_Condition_Product_Found
{

    public function validateItems(
        Varien_Object $quote,
        $address,
        $returnFormat = Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED,
        $groupBy = false,
        $multiplier = 1,
        $groupIdentifier = 'simple',
        $conditionsMatched = array(),
        $attrValue = ''
    ) {
        $matches = array();
        $all = $this->getAggregator() === 'all';
        $true = (bool)$this->getValue();
        $nrConditions = 0;
        $found = false;
        $ruleMatchesXtimes = 0;
        $ruleType = '';

        $debug = $returnFormat == Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG;

        $matches = array();

        if ($groupIdentifier == 'simple') {
            $groupIdentifier .= $this->getRule()->getRuleId();
        }

        foreach ($this->getConditions() as $cond) {
            if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                Mage::helper('fooman_advancedpromotions')->debug(
                    $cond->validateItems(
                        $quote, $address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG, true,
                        $multiplier, $groupIdentifier
                    ), Zend_Log::ERR, $returnFormat, 'GROUP SIMPLE CONDITION'
                );
            }
            $tmpMatches[] = $cond->validateItems($quote, $address, $returnFormat, true, $multiplier, $groupIdentifier);
            $nrConditions++;
        }
        if ($nrConditions == 0) {
            $ruleMatchesXtimes = array('group' => array($groupIdentifier => 1));
        } else {

            $tmpMatches = Mage::helper('fooman_advancedpromotions')->removeInsufficientMatches(
                $tmpMatches,
                $nrConditions
            );
            if (sizeof($tmpMatches)) {
                $ruleMatchesXtimes = array('group' => array($groupIdentifier => 1));
            } else {
                $ruleMatchesXtimes = array('group' => array($groupIdentifier => 0));
            }
        }

        if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
            Mage::helper('fooman_advancedpromotions')->debug(
                get_class($this) . ' rule matches X times: ' . $ruleMatchesXtimes, Zend_Log::NOTICE, $returnFormat
            );
        }
        return Mage::helper('fooman_advancedpromotions')->formattedReturnValue(
            $returnFormat, $this->getType(), $ruleMatchesXtimes, $matches
        );
    }

}