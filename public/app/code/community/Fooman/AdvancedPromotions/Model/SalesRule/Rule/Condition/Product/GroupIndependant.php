<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_GroupIndependant
    extends Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_Group_Abstract
{
    const TYPE = 'fooman_advancedpromotions/salesRule_rule_condition_product_groupIndependant';

    public function asHtml()
    {
        $html = $this->getTypeElement()->getHtml().
           Mage::helper('fooman_advancedpromotions')->__("Apply discount one time each for a group consisting of ALL these items with the same %s",$this->getValueElement()->getHtml());
           if ($this->getId()!='1') {
               $html.= $this->getRemoveLinkHtml();
           }
        return $html;
    }

    public function getType()
    {
        return self::TYPE;
    }

    public function validateItems (
            Varien_Object $quote,
            $address,            
            $returnFormat = Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED,
            $groupBy = false,
            $multiplier = 1,
            $groupIdentifier = 'simple',
            $conditionsMatched = array(),
            $attrValue = ''
    )
    {
        $matches = array();
        $tmpMatches = array();
        $ruleMatchesXtimes = array();
        $nrConditions = 0;
        $ruleType=$this->getGroupMatchType();
        $debug = $returnFormat == Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG;

        foreach ($this->getConditions() as $cond) {
            if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                Mage::helper('fooman_advancedpromotions')->debug($cond->validateItems($quote, $address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG, $this->getValue(), $multiplier, $groupIdentifier.'-'.$cond->getRule()->getId()), Zend_Log::ERR, $returnFormat,'GROUP INDEPENDANT CONDITION');
            }
            $tmpMatches[]=$cond->validateItems($quote, $address, $returnFormat, $this->getValue(), $multiplier, $groupIdentifier.'-'.$cond->getRule()->getId());
            $nrConditions ++;
        }

        if ($nrConditions == 0) {
            foreach ($quote->getAllVisibleItems() as $quoteItem) {
                $ruleMatchesXtimes = Mage::helper('fooman_advancedpromotions')->formattedQuoteItem($matches, $ruleMatchesXtimes, $quoteItem, $groupBy, $groupIdentifier.'-'.$cond->getRule()->getId(), $multiplier, $attrValue, $debug);
            }
        } else {
            $groupsCombined = array();
            $nrGroupMatched = 0;
            $insufficientMatch = false;
            //combine the different matches into 1 group
            foreach ($tmpMatches as $groupMatched) {
                if (isset($groupMatched['matches'])) {
                    $groupsCombined = Mage::helper('fooman_advancedpromotions')->mergeMatches($groupsCombined, $groupMatched['matches'], 'min');
                }
                if (isset($groupMatched['rule_qty']) && is_array($groupMatched['rule_qty'])) {
                    foreach ($groupMatched['rule_qty'] as $attrKey => $tmpGroupMatches) {
                        foreach ($tmpGroupMatches as $attrValue => $nrMatches) {
                            if (isset($ruleMatchesXtimes[$attrKey][$attrValue])) {
                                $ruleMatchesXtimes[$attrKey][$attrValue] = min($ruleMatchesXtimes[$attrKey][$attrValue], $nrMatches);
                            } else {
                                $ruleMatchesXtimes[$attrKey][$attrValue] = $nrMatches;
                            }
                        }
                    }
                } else {
                    $ruleMatchesXtimes = $groupMatched['rule_qty'];
                }

            }
            //work out where we have matched all conditions making up the logical group
            foreach ($groupsCombined as $attrKey => $groupMatched) {
                foreach ($groupMatched as $attrValue => $items) {
                    if (sizeof($items) >= $nrConditions) {
                        foreach ($items as $key => $item) {
                            $matches[$attrKey][$attrValue][$key] = $item;
                        }
                    } else {
                        $ruleMatchesXtimes[$attrKey][$attrValue] = 0;
                    }

                }
            }

        }
        return Mage::helper('fooman_advancedpromotions')->formattedReturnValue($returnFormat, $ruleType, $ruleMatchesXtimes, $matches);
    }

}
