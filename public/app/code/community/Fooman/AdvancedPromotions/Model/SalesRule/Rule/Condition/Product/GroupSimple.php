<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_GroupSimple
    extends Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_Group_Abstract
{
    const TYPE = 'fooman_advancedpromotions/salesRule_rule_condition_product_groupSimple';

    public function loadValueOptions()
    {
        $this->setValueOption(array(
            1=>'ALL'
        ));
        return $this;
    }

    public function getType()
    {
        return self::TYPE;
    } 

    public function asHtml()
    {
        $html = $this->getTypeElement()->getHtml().
           Mage::helper('fooman_advancedpromotions')->__("Apply discount one time each for a group consisting of %s these items",$this->getValueElement()->getHtml());
           if ($this->getId()!='1') {
               $html.= $this->getRemoveLinkHtml();
           }
        return $html;
    }

    public function validateItems (
            Varien_Object $quote,
            $address,            
            $returnFormat = Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED,
            $groupBy = false,
            $multiplier = 1,
            $groupIdentifier = 'simple',
            $conditionsMatched = array(),
            $attrValue = ''
    )
    {
        $matches = array();
        $tmpMatches = array();
        $ruleMatchesXtimes = false;
        $nrConditions = 0;
        $ruleType=$this->getGroupMatchType();
        $debug = $returnFormat == Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG;

        if($groupIdentifier=='simple'){
            $groupIdentifier.= $this->getRule()->getRuleId();
        }

        foreach ($this->getConditions() as $cond) {
            if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                Mage::helper('fooman_advancedpromotions')->debug($cond->validateItems($quote, $address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG, true, $multiplier, $groupIdentifier), Zend_Log::ERR, $returnFormat,'GROUP SIMPLE CONDITION');
            }
            $tmpMatches[]=$cond->validateItems($quote, $address, $returnFormat, true, $multiplier, $groupIdentifier);
            $nrConditions ++;
        }

        if ($nrConditions == 0) {
            foreach ($quote->getAllVisibleItems() as $quoteItem) {
                $ruleMatchesXtimes = Mage::helper('fooman_advancedpromotions')->formattedQuoteItem($matches, $ruleMatchesXtimes, $quoteItem, $groupBy, $groupIdentifier.'-'.$cond->getRule()->getId(), $multiplier, $attrValue, $debug);
            }
        } else {
            $insufficientMatch = false;
            foreach ($tmpMatches as $groupMatched) {
                if ($ruleMatchesXtimes === false) {
                    $ruleMatchesXtimes = $groupMatched['rule_qty'];
                    $matches = $groupMatched['matches'];
                } else {
                    if(is_array($ruleMatchesXtimes)) {
                        $tmpTotalMatches1 = 0;
                        $tmpTotalMatches2 = 0;
                        //tally up all matches recursively
                        $groupMatchedIterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($groupMatched['rule_qty']));
                        foreach($groupMatchedIterator as $value) {
                            $tmpTotalMatches1 += $value;
                        }
                        //tally up all matches recursively
                        $ruleMatchesIterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($ruleMatchesXtimes));
                        foreach($ruleMatchesIterator as $value) {
                            $tmpTotalMatches2 += $value;
                        }

                        if($tmpTotalMatches1 < $tmpTotalMatches2) {
                            $ruleMatchesXtimes = min($ruleMatchesXtimes, $groupMatched['rule_qty']);
                            $matches = array_merge_recursive($matches, $groupMatched['matches']);
                        }
                    } else {
                        if (isset($groupMatched['matches'])) {
                            $ruleMatchesXtimes = min($ruleMatchesXtimes, $groupMatched['rule_qty']);
                            $matches = array_merge_recursive($matches, $groupMatched['matches']);
                        }
                    }
                }
                if ($groupMatched['rule_qty'] == 0) {
                    $insufficientMatch = true;
                }
            }
            if($insufficientMatch) {
                $ruleMatchesXtimes = 0;
                $matches = array();
            }
        }

        if(is_array($ruleMatchesXtimes)){
            foreach ($ruleMatchesXtimes as $groupMatched) {
                foreach ($groupMatched as $attrValue => $value) {
                    if($value){
                        Mage::helper('fooman_advancedpromotions')->saveAddRule(
                            $this->getRule()->getId(),
                            $attrValue,
                            $value
                        );
                    }
                }
            }
        } elseif($ruleMatchesXtimes) {
            Mage::helper('fooman_advancedpromotions')->saveAddRule(
                $this->getRule()->getId(),
                $groupBy . $groupIdentifier,
                $ruleMatchesXtimes
            );
        }

        if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
            Mage::helper('fooman_advancedpromotions')->debug(get_class($this).' rule matches X times: '.$ruleMatchesXtimes, Zend_Log::NOTICE, $returnFormat);
        }
        return Mage::helper('fooman_advancedpromotions')->formattedReturnValue($returnFormat, $ruleType, $ruleMatchesXtimes, $matches);
    }

}