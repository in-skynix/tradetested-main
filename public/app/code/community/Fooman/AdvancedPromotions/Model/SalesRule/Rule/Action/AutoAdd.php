<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Action_AutoAdd
    extends Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_Group_Abstract
{
    const TYPE='fooman_advancedpromotions/salesRule_rule_action_autoAdd';

    public function getType()
    {
        return self::TYPE;
    }

    public function getNewChildSelectOptions()
    {
        $choices = array(
            array('value' => 'fooman_advancedpromotions/salesRule_rule_condition_productAutoAdd|sku', 'label' => Mage::helper('fooman_advancedpromotions')->__('Simple Product by SKU'))
        );
        return $choices;
    }

    public function asHtml ()
    {
        $html = $this->getTypeElement()->getHtml() .
                Mage::helper('fooman_advancedpromotions')->__("Automatically add this product (onepage checkout only)");
        if ($this->getId() != '1') {
            $html.= $this->getRemoveLinkHtml();
        }
        return $html;
    }

    public function validateItems (
            Varien_Object $quote,
            $address,
            $returnFormat = Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED,
            $groupBy = false,
            $multiplier = 1,
            $groupIdentifier = 'simple',
            $conditionsMatched = array(),
            $attrValue = ''
    )
    {
        $matches = array();
        $tmpMatches = array();
        $groupMatchLookup = array();
        $ruleMatchesXtimes = 0;
        $nrConditions = 0;
        $ruleType=$this->getGroupMatchType();
        $debug = $returnFormat == Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG;

        foreach ($this->getActions() as $action) {
            if($action->getValue()){
                if (is_array($action->getValue())) {
                    $skus = $action->getValue();
                } else {
                    $skus = explode(',', $action->getValue());
                }
                foreach ( $skus as $sku) {
                    $sku = trim($sku);
                    $min = (int)$this->getRule()->getDiscountQty();
                    if (isset($conditionsMatched['rule_qty']['group'][$groupIdentifier])) {
                        if($min > 0 ){
                            $min = min($min, $conditionsMatched['rule_qty']['group'][$groupIdentifier]);
                        } else {
                            $min = $conditionsMatched['rule_qty']['group'][$groupIdentifier];
                        }
                    }
                    Mage::helper('fooman_advancedpromotions')->saveAddLater(
                        $this->getRule()->getId(),
                        $sku,
                        $min
                    );
                }
            }

            $groupIdentifier = $groupIdentifier.'-'.$action->getId();
            if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                Mage::helper('fooman_advancedpromotions')->debug($action->validateItems($quote, $address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG, true, $multiplier, $groupIdentifier), Zend_Log::ERR, $returnFormat,'AUTO ADD ACTION 1');
            }
            $nrConditions ++;
        }


        return Mage::helper('fooman_advancedpromotions')->formattedReturnValue($returnFormat, $ruleType, $ruleMatchesXtimes, $matches);
    }


}