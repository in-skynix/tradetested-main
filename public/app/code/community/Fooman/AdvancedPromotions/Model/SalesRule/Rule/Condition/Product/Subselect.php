<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_Subselect extends Mage_SalesRule_Model_Rule_Condition_Product_Subselect {

    public function validateItems (
            Varien_Object $quote,
            $address,            
            $returnFormat = Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED,
            $groupBy = false,
            $multiplier = 1,
            $groupIdentifier = 'simple',
            $conditionsMatched = array(),
            $attrValue = ''
    )
    {
        $debug = $returnFormat == Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG;
        $ruleType = $this->getGroupMatchType();
        $ruleMatchesXtimes = 0;
        $matches = array();

        if (parent::validate($address)) {
            foreach ($quote->getAllVisibleItems() as $quoteItem) {
                $ruleMatchesXtimes = Mage::helper('fooman_advancedpromotions')->formattedQuoteItem($matches, $ruleMatchesXtimes, $quoteItem, $groupBy, $groupIdentifier, $multiplier, $attrValue, $debug);
            }
            $ruleMatchesXtimes = array('group' => array($groupIdentifier => 1));
        } else {
            $ruleMatchesXtimes = array('group' => array($groupIdentifier => 0));
        }

        if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
            Mage::helper('fooman_advancedpromotions')->debug(get_class($this).' rule matches X times: '.$ruleMatchesXtimes, Zend_Log::NOTICE, $returnFormat);
        }
        return Mage::helper('fooman_advancedpromotions')->formattedReturnValue($returnFormat, $this->getType(), $ruleMatchesXtimes, $matches);

    }

    /**
     * validate
     *
     * @param Varien_Object $object Quote
     * @return boolean
     */
    public function validate(Varien_Object $object)
    {
        if (substr($this->getRule()->getSimpleAction(), 0, 6) == 'super_') {
            return true;
        } else {
            return parent::validate($object);
        }
    }

}