<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_Combine extends Mage_SalesRule_Model_Rule_Condition_Product_Combine
{

    const TYPE = 'fooman_advancedpromotions/salesRule_rule_condition_product_combine';

    public function __construct()
    {
        parent::__construct();
        $this->setType($this->getType());
    }

    public function getType()
    {
        return self::TYPE;
    }

    public function getOrigNewChildSelectOptions()
    {
        return parent::getNewChildSelectOptions();
    }

    public function getNewChildSelectOptions()
    {
        $conditions = parent::getNewChildSelectOptions();

        $choices = array(
            array('value'=>'fooman_advancedpromotions/salesRule_rule_action_groupSimple', 'label'=>Mage::helper('fooman_advancedpromotions')->__('Discount in group')),
            array('value'=>'fooman_advancedpromotions/salesRule_rule_action_groupIndependant', 'label'=>Mage::helper('fooman_advancedpromotions')->__('Discount in group defined by attribute Z')),
            array('value'=>'fooman_advancedpromotions/salesRule_rule_action_autoAdd', 'label'=>Mage::helper('fooman_advancedpromotions')->__('Automatically add this simple product'))
        );
        if ((string) Mage::getConfig()->getModuleConfig('Unirgy_Giftcert')->active == 'true') {
            $choices[] = array('value'=>'fooman_advancedpromotions/salesRule_rule_action_giftCertAutoAdd', 'label'=>Mage::helper('fooman_advancedpromotions')->__('Automatically add this gift certificate'));
        }

        $conditions = array_merge_recursive($conditions, array(
            array('value'=>$choices, 'label'=>Mage::helper('fooman_advancedpromotions')->__('AdvancedPromotions'))
        ));
        return $conditions;
    }

    /**
     * need to validate all items since they are later needed for potential effects on other items
     *
     * @param Varien_Object $object
     *
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        if (substr($this->getRule()->getSimpleAction(), 0, 6) == 'super_') {
            return true;
        } else {
            return parent::validate($object);
        }

        /*
        $itemValidated = parent::validate($object);
        if ($itemValidated) {
            return true;
        }

        if ($object instanceof Mage_Sales_Model_Quote_Item && $object->getProductType() == 'configurable') {
            $childrenItems = $object->getChildren();
            foreach ($childrenItems as $childrenItem) {
                //should be only 1 child
                return parent::validate($childrenItem);
            }
        }
        return false;*/
    }

    public function validateItems (
            Varien_Object $quote,
            $address,            
            $returnFormat = Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED,
            $groupBy = false,
            $multiplier = 1,
            $groupIdentifier = 'simple',
            $conditionsMatched = array(),
            $attrValue = ''
    )
    {
        $matches = array();
        $tmpMatches = array();
        $all = $this->getAggregator()==='all';
        $true = (bool)$this->getValue();
        $nrConditions = 0;
        $ruleMatchesXtimes = 0;
        $ruleType= $this->getGroupMatchType();
        $debug = $returnFormat == Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG;

        if ($this->getAttribute() == 'qty') {
            $multiplier = $this->getValue();
        }
        if($groupIdentifier=='simple'){
            $groupIdentifier.= $this->getRule()->getRuleId();
        }
        foreach ($this->getConditions() as $cond) {
            if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                Mage::helper('fooman_advancedpromotions')->debug($cond->validateItems($quote, $address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG, $groupBy, $multiplier, $groupIdentifier, $conditionsMatched), Zend_Log::ERR, $returnFormat,'COMBINE');
            }
            $tmpMatches[] = $cond->validateItems($quote, $address, $returnFormat, $groupBy, $multiplier, $groupIdentifier, $conditionsMatched);
            $nrConditions++;
        }

        if ($nrConditions == 0) {
            foreach ($quote->getAllVisibleItems() as $quoteItem) {
                $ruleMatchesXtimes = Mage::helper('fooman_advancedpromotions')->formattedQuoteItem($matches, $ruleMatchesXtimes, $quoteItem, $groupBy, $groupIdentifier, $multiplier, $attrValue, $debug);
            }
        } else {
            if ($all) {
                $tmpMatches = Mage::helper('fooman_advancedpromotions')->removeInsufficientMatches(
                    $tmpMatches,
                    $nrConditions
                );
            }

            $matches = Mage::helper('fooman_advancedpromotions')->mergeTmpMatches($tmpMatches);
            if(!empty($matches['rule_type'])){
                $ruleType = $matches['rule_type'];
            }
            
            if($matches['rule_type'] != Fooman_AdvancedPromotions_Helper_Data::ACTION_GROUP_INDEPENDANT) {
                $matches = Mage::helper('fooman_advancedpromotions')->removeDoubles($matches);
            }

            $ruleMatchesXtimes = $matches['rule_qty'];
            $matches = $matches['matches'];

        }

        return Mage::helper('fooman_advancedpromotions')->formattedReturnValue($returnFormat, $ruleType, $ruleMatchesXtimes, $matches);
    }

    public function getGroupMatchType() {
        return $this->getType();
    }

}