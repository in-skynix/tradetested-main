<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_GroupSubselectAll 
    extends Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_GroupSubselectQty
{
    const TYPE = 'fooman_advancedpromotions/salesRule_rule_condition_product_groupSubselectAll';

    public function getType()
    {
        return self::TYPE;
    }

}