<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_ProductAutoAdd extends Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product
{

    public function loadOperatorOptions()
    {
        parent::loadOperatorOptions();
        $extraOption = array('==' => Mage::helper('rule')->__('is'),'()'  => Mage::helper('rule')->__('is one of'));
        $this->setOperatorOption($extraOption);
        return $this;
    }

    public function getValueElementChooserUrl()
    {
        $url = parent::getValueElementChooserUrl();
        if($url) {
            $url = str_replace('/attribute/sku', '/attribute/sku/std/true', $url);
        }
        return $url;
    }

    public function validateItems (
            Varien_Object $quote,
            $address,            
            $returnFormat = Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED,
            $groupBy = false,
            $multiplier = 1,
            $groupIdentifier = 'simple',
            $conditionsMatched = array(),
            $attrValue = '',
            $reverseAutoAdded = false
    ){
        return parent::validateItems($quote, $address, $returnFormat, $groupBy, $multiplier, $groupIdentifier, $conditionsMatched, $attrValue, true);
    }
    
}