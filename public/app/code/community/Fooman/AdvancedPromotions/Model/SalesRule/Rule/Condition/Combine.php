<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Combine extends Mage_SalesRule_Model_Rule_Condition_Combine {

    public function validate(Varien_Object $object)
    {
        //Mage::getSingleton('core/session')->addSuccess('buy more');
        return parent::validate($object);
    }

    public function validateItems (
            Varien_Object $quote,
            $address,            
            $returnFormat = Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED,
            $groupBy = false,
            $multiplier = 1,
            $groupIdentifier = 'simple',
            $conditionsMatched = array(),
            $attrValue = ''
    )
    {
        $matches = array();
        $all = $this->getAggregator()==='all';
        $true = (bool)$this->getValue();
        $nrConditions = 0;
        $found = false;
        $ruleMatchesXtimes = 0;
        $ruleType='';
        $debug = $returnFormat == Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG;
        $tmpMatches = array();

        foreach ($this->getConditions() as $cond) {
            if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                Mage::helper('fooman_advancedpromotions')->debug($cond->validateItems($quote,$address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG,$groupBy,$multiplier),Zend_Log::EMERG, null, 'MASTER CONDITIONS MATCHED');
            }
            $tmpMatches[] = $cond->validateItems($quote, $address, $returnFormat, $groupBy, $multiplier);
            $nrConditions++;
        }
        foreach ($tmpMatches as $groupedMatches) {
            if ($true) {
                //if(is_array($groupedMatches)) {
                    $matches = array_merge($matches, $groupedMatches['matches']);
                    if (is_array($groupedMatches['rule_qty'])) {
                        $ruleMatchesXtimes = $groupedMatches['rule_qty'];
                    } else {
                        if ($all) {
                            if ($ruleMatchesXtimes == 0) {
                                 $ruleMatchesXtimes = floor($groupedMatches['rule_qty']/$multiplier);
                            } else {
                                $ruleMatchesXtimes = min(floor($groupedMatches['rule_qty']/$multiplier), $ruleMatchesXtimes);
                            }
                        } else {
                            if ($ruleMatchesXtimes == 0) {
                                 $ruleMatchesXtimes = floor($groupedMatches['rule_qty']/$multiplier);
                            } else {
                                $ruleMatchesXtimes = min(floor($groupedMatches['rule_qty']/$multiplier), $ruleMatchesXtimes);
                            }
                        }
                    }
                    if($groupedMatches['rule_type']){
                        $ruleType = $groupedMatches['rule_type'];
                    }
                /*} else {
                    if ($all) {
                        if ($ruleMatchesXtimes == 0) {
                            $ruleMatchesXtimes = floor($groupedMatches/$multiplier);
                        } else {
                            $ruleMatchesXtimes = min(floor($groupedMatches/$multiplier), $ruleMatchesXtimes);
                        }
                    } else {
                        $ruleMatchesXtimes = max(floor($tmpMatches/$multiplier), $ruleMatchesXtimes);
                    }
                    if(!is_int($tmpMatches)){
                        $ruleType = $tmpMatches;
                    }
                }*/
            }
        }
        $ruleMatchesXtimes == 0 ? $found=false:$found=true;
        // no item found and no conditions so all items match and rule matched once
        if(!$found && $nrConditions==0) {
            foreach ($quote->getAllVisibleItems() as $quoteItem) {
                $ruleMatchesXtimes = Mage::helper('fooman_advancedpromotions')->formattedQuoteItem($matches, $ruleMatchesXtimes, $quoteItem, $groupBy, $groupIdentifier, $multiplier, $attrValue, $debug);
            }
        }
        return Mage::helper('fooman_advancedpromotions')->formattedReturnValue($returnFormat, $ruleType, $ruleMatchesXtimes, $matches);
    }

}