<?php
class Fooman_AdvancedPromotions_Model_System_DiscountMode
{
    const CHEAPEST = 0;
    const PRICIEST = 1;
    const SMART_SHOPPER = 2;

    public function getOptions()
    {
        $returnArray = array(
            self::CHEAPEST=> Mage::helper('fooman_advancedpromotions')->__('Cheapest Item'),
            self::PRICIEST=> Mage::helper('fooman_advancedpromotions')->__('Most Expensive Item'),
            self::SMART_SHOPPER=> Mage::helper('fooman_advancedpromotions')->__('Smart Shopper')
        );
        return $returnArray;
    }
}