<?php
class Fooman_AdvancedPromotions_Model_System_DiscountDistribution
{

    public function toOptionArray()
    {
        $returnArray = array();
        $returnArray[] = array(
            'value' => Fooman_AdvancedPromotions_Model_Quote_Address_AdvancedPromo::DISCOUNT_DISTRIBUTION_ON_PRODUCT,
            'label' => Mage::helper('fooman_advancedpromotions')->__('None')
        );
        $returnArray[] = array(
            'value' => Fooman_AdvancedPromotions_Model_Quote_Address_AdvancedPromo::DISCOUNT_DISTRIBUTION_PER_ITEM,
            'label' => Mage::helper('fooman_advancedpromotions')->__('Evenly among products')
        );
        $returnArray[] = array(
            'value' => Fooman_AdvancedPromotions_Model_Quote_Address_AdvancedPromo::DISCOUNT_DISTRIBUTION_PER_CURRENCY_UNIT,
            'label' => Mage::helper('fooman_advancedpromotions')->__('Proportionally between products')
        );
        return $returnArray;
    }
}