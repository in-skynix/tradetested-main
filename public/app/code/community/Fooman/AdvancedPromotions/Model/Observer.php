<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_Observer
{

    private $_groupsSummedUp = array();
    private $_subItemsToDiscount = array();

    /**
     * grab search engine keywords and utm_campaign details from referrer
     *
     * @param   Varien_Event_Observer $observer
     */
    public function grabReferrerDetails ($observer)
    {

        $controllerAction = $observer->getEvent()->getControllerAction();
        $request = $controllerAction->getRequest();
        $referrerProcessed = Mage::getSingleton('customer/session')->getFoomanAdvancedPromoProcessed();
        if(!$referrerProcessed) {
            try {
                Mage::getSingleton('customer/session')->setFoomanAdvancedPromoProcessed(true);
                //http://code.google.com/apis/analytics/docs/tracking/gaTrackingTraffic.html#searchEngine
                $knownKeywordParameters = array('q', 'query', 'p', 'encquery', 'k', 'qs', 'qt', 'rdata',
                    'search_word', 'szukaj', 'terms', 'text', 'wd', 'words');

                if(isset($_SERVER['HTTP_REFERER'])){
                    $uri = Zend_Uri::factory($_SERVER['HTTP_REFERER']);
                    $queryOrFragment = $uri->getQuery();
                    if(!$queryOrFragment) {
                        $queryOrFragment = $uri->getFragment();
                    }
                    if($queryOrFragment) {
                        parse_str($queryOrFragment, $output);
                        $collectedKeywords = false;
                        foreach ($knownKeywordParameters as $param) {
                            $keywords = isset($output[$param])?$output[$param]:false;
                            if($keywords) {
                                $collectedKeywords = $keywords;
                                break;
                            }
                        }
                        if ($collectedKeywords) {
                            Mage::getSingleton('customer/session')->setFoomanAdvancedPromoKeywords($collectedKeywords);
                        }
                    }
                }

                $knownGAParameters = array('utm_source','utm_medium','utm_term','utm_content','utm_campaign');
                foreach ($knownGAParameters as $param) {
                    $GAParam = $request->getParam($param);
                    if ($GAParam) {
                        Mage::getSingleton('customer/session')->setData('fooman_advanced_promo_' . $param, $GAParam);
                        if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                            Mage::helper('fooman_advancedpromotions')->debug('campaign set to |' . Mage::getSingleton('customer/session')->getFoomanAdvancedPromoUtmCampaign() . '|', Zend_Log::EMERG);
                        }
                    }
                }

            } catch (Exception $e) {
                //drop the exception - referrer will stay empty
            }
            if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                Mage::helper('fooman_advancedpromotions')->debug('keywords set to |'.Mage::getSingleton('customer/session')->getFoomanAdvancedPromoKeywords().'|', Zend_Log::EMERG);
            }
        }
    }

    /**
     * when initiating a quote add saved utm_campaign as coupon code
     *
     * @param   Varien_Event_Observer $observer
     */
    public function addCampaignAsCoupon ($observer)
    {
        $utmCampaign = Mage::getSingleton('customer/session')->getFoomanAdvancedPromoUtmCampaign();
        if ($utmCampaign) {
            $quote = $observer->getEvent()->getQuote();
            if (!$quote->getCouponCode()) {
                if (Mage::getStoreConfig(Fooman_AdvancedPromotions_Helper_Data::XML_PATH_ADVANCEDPROMO_SETTINGS . 'autocouponenabled')) {
                    $quote->setCouponCode($utmCampaign);
                }
            }
        }
    }

    public function resetDiscounts ($observer)
    {
        $quote = $observer->getEvent()->getQuote();
        foreach ($quote->getAllItems() as $item){
            $item->setDiscountAmount(0);
            $item->setBaseDiscountAmount(0);
        }
    }



    public function autoAddFreeProducts ($observer)
    {
        Mage::helper('fooman_advancedpromotions')->debug('autoAddFreeProducts',Zend_Log::EMERG);

        $quote = $observer->getQuote();
        if ($quote) {
            $recollect = false;
            $subtotalRules = Mage::helper('fooman_advancedpromotions')->loadSubtotalRules();
            $addLater = Mage::helper('fooman_advancedpromotions')->loadAddLaters();
            $addRules = Mage::helper('fooman_advancedpromotions')->loadAddRules();
            $qtyRules = Mage::helper('fooman_advancedpromotions')->loadQtyRules();
            $giftCerts= Mage::helper('fooman_advancedpromotions')->loadAddCertLaters();

            Mage::helper('fooman_advancedpromotions')->debug('ADD LATER',Zend_Log::EMERG);
            Mage::helper('fooman_advancedpromotions')->debug($addLater,Zend_Log::EMERG);
            Mage::helper('fooman_advancedpromotions')->debug('SUBTOTAL RULES',Zend_Log::EMERG);
            Mage::helper('fooman_advancedpromotions')->debug($subtotalRules,Zend_Log::EMERG);
            Mage::helper('fooman_advancedpromotions')->debug('QTY RULES',Zend_Log::EMERG);
            Mage::helper('fooman_advancedpromotions')->debug($qtyRules,Zend_Log::EMERG);
            Mage::helper('fooman_advancedpromotions')->debug('ADD RULES',Zend_Log::EMERG);
            Mage::helper('fooman_advancedpromotions')->debug($addRules, Zend_Log::EMERG);

            if (($addLater || $giftCerts)) {
                if($subtotalRules) {
                    foreach ($subtotalRules as $ruleId=>$subtotalAmount){
                        $totals = $quote->getTotals();
                        if ($totals['subtotal']->getAddress()) {
                            $quoteTotal = $totals['subtotal']->getAddress()->getBaseSubtotalInclTax();
                        }
                        if (!$quoteTotal) {
                            if ($totals['subtotal']->getBaseValueInclTax()) {
                                $quoteTotal = $totals['subtotal']->getBaseValueInclTax();
                            } else {
                                $quoteTotal = $totals['subtotal']->getBaseValue();
                            }
                        }
                        if(isset($totals['discount'])){
                            $quoteTotal += $totals['discount']->getBaseValue() > 0 ? (-1 * $totals['discount']->getBaseValue()) : $totals['discount']->getBaseValue();
                        }
                        if(isset($totals['ugiftcert'])){
                            $quoteTotal -= $totals['ugiftcert']->getBaseValue();
                        }
                        $nrFree = floor($quoteTotal / $subtotalAmount);
                        if ($nrFree) {
                            if ($addLater && isset($addLater[$ruleId])) {
                                foreach ($addLater[$ruleId] as $sku => $max) {
                                    if ($max) {
                                        $giveAway = min($max, $nrFree);
                                    } else {
                                        $giveAway = $nrFree;
                                    }
                                    Mage::helper('fooman_advancedpromotions')->debug('$giveAway '.$giveAway,Zend_Log::EMERG);
                                    $this->_addFreeProduct($quote, $sku,
                                            $giveAway, $ruleId);
                                }
                            }
                            if($giftCerts){
                                foreach ($giftCerts as $sku=>$value) {
                                    $this->_addFreeGiftCertProduct($quote, $sku, $nrFree, $value, $ruleId);
                                }
                            }
                        }
                    }
                }
                if ($qtyRules) {
                    foreach ($qtyRules as $ruleId => $qty) {
                        $autoAdded = Mage::getModel('fooman_advancedpromotions/sales_mysql4_quote_item_collection')->getAutoAddedQtyForQuote($quote);
                        $nrFree = floor(($quote->getItemsQty() - $autoAdded) / $qty);
                                    Mage::helper('fooman_advancedpromotions')->debug('$nrFree '.$nrFree,Zend_Log::EMERG);
                        if ($nrFree) {
                            if ($addLater && isset($addLater[$ruleId])) {
                                foreach ($addLater[$ruleId] as $sku => $max) {
                                    if ($max) {
                                        $giveAway = min($max, $nrFree);
                                    } else {
                                        $giveAway = $nrFree;
                                    }
                                    Mage::helper('fooman_advancedpromotions')->debug('$giveAway '.$giveAway,Zend_Log::EMERG);
                                    $this->_addFreeProduct($quote, $sku,
                                            $giveAway, $ruleId);
                                }
                            }
                            if ($giftCerts) {
                                foreach ($giftCerts as $sku => $value) {
                                    $this->_addFreeGiftCertProduct($quote,
                                            $sku, $nrFree, $value, $ruleId);
                                }
                            }
                        }
                    }
                }
                if($addRules) {
                    foreach ($addRules as $ruleId => $addRule) {
                        foreach ($addRule as $nrFree) {
                            Mage::helper('fooman_advancedpromotions')->debug('$nrFree ' . $nrFree,
                                    Zend_Log::EMERG);
                            if ($nrFree) {
                                if ($addLater && isset($addLater[$ruleId])) {
                                    foreach ($addLater[$ruleId] as $sku => $max) {
                                        if ($max) {
                                            $giveAway = min($max, $nrFree);
                                        } else {
                                            $giveAway = $nrFree;
                                        }
                                        $this->_addFreeProduct($quote, $sku,
                                                $giveAway, $ruleId);
                                    }
                                }
                                if ($giftCerts) {
                                    foreach ($giftCerts as $sku => $value) {
                                        $this->_addFreeGiftCertProduct($quote,
                                                $sku, $nrFree, $value, $ruleId);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    protected function _addFreeProduct ($quote, $sku, $nrFree, $ruleId)
    {
        if(strpos($sku, Fooman_AdvancedPromotions_Helper_Data::PROMO_ARRAY_PREFIX) !==false){
            list($groupedBy, $value) = explode(Fooman_AdvancedPromotions_Helper_Data::PROMO_ARRAY_PREFIX, $sku);
            if($groupedBy == 'sku'){
                $sku = $value;
            }
        }
        $freeProduct = Mage::getModel('catalog/product');
        $freeProduct->load($freeProduct->getIdBySku($sku));
        $freeProduct->addCustomOption('fooman_advancedpromotions_auto_added', 1);
        if ($freeProduct->getId() && $freeProduct->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_ENABLED) {
            if ($nrFree > 0) {
                try {
                    $checkStock = $freeProduct->getStockItem()->checkQty($nrFree);
                    if(!$checkStock){
                        Mage::throwException(
                            Mage::helper('cataloginventory')->__('Sorry we don\'t have enough free "%s" available.', $freeProduct->getName())
                        );
                    }

                    $message = Mage::helper('fooman_advancedpromotions')->__('Your shopping cart qualifies for %sx free %s',
                        $nrFree, $freeProduct->getName());
                    $messageObject = Mage::getModel('core/message_notice', $message);
                    $messageObject->setIdentifier($freeProduct->getId().'-'.$ruleId);
                    $isCartPage = Mage::app()->getRequest()->getControllerName() == 'cart';
                    //only add message once
                    if ($isCartPage){
                        $existingMessages =  Mage::getSingleton('checkout/session')->getMessages(false);
                        if(!$existingMessages->getMessageByIdentifier($messageObject->getIdentifier())){
                            Mage::getSingleton('checkout/session')->addMessage($messageObject);
                        }
                    }
                    $item = $quote->getItemByProduct($freeProduct);

                    if(!$item) {
                        $item = $quote->addProduct($freeProduct, $nrFree);

                        if (is_string($item)) {
                            //error during adding product returns a string == the error message
                            Mage::throwException($item);
                        }
                        $item->setCustomPrice(0);
                        $item->setBaseOriginalPrice(0);
                        $item->setTaxPercent(0);
                        $item->setName(Mage::helper('fooman_advancedpromotions')->__('FREE') . ' ' . $item->getName());
                        $item->setFoomanAutoAddedQty($nrFree);
                        $item->setMessage(Mage::helper('fooman_advancedpromotions')->__('FREE'));
                        Mage::helper('fooman_advancedpromotions')->addAppliedRuleId($item, $ruleId, true);
                        if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                            Mage::helper('fooman_advancedpromotions')->debug('Auto adding '.$nrFree. 'x '.$freeProduct->getName(), Zend_Log::EMERG);
                        }
                    } else {
                        $item->setQty($item->getQty() + $nrFree);
                        $item->setFoomanAutoAddedQty($item->getFoomanAutoAddedQty() + $nrFree);
                        Mage::helper('fooman_advancedpromotions')->addAppliedRuleId($item, $ruleId, true);
                    }
                } catch (Exception $e) {
                    $message = $e->getMessage();
                    if ($isCartPage && strpos(Mage::getSingleton('checkout/session')->getMessages(false)->toString(),
                                    $message) === false) {
                        Mage::getSingleton('checkout/session')->addError($message);
                    }
                    Mage::logException($e);
                }
            }
        }
    }

    protected function _addFreeGiftCertProduct ($quote, $sku, $nrFree, $value, $ruleId)
    {
        $freeProduct = Mage::getModel('catalog/product');
        $freeProduct->load($freeProduct->getIdBySku($sku));
        $freeProduct->addCustomOption('fooman_advancedpromotions_auto_added', 1);
        $buyRequest = new Varien_Object;
        $buyRequest->setAmount($value);
        $buyRequest->setQty($nrFree);
        if ($freeProduct->getId() && $freeProduct->isSaleable()) {
            if ($nrFree > 0) {
                try {
                    $checkStock = $freeProduct->getStockItem()->checkQty($nrFree);
                    if(!$checkStock){
                        Mage::throwException(
                            Mage::helper('cataloginventory')->__('Sorry we don\'t have enough free "%s" available.', $freeProduct->getName())
                        );
                    }

                    $item = $quote->addProduct($freeProduct, $buyRequest);
                    $item->setCustomPrice(0);
                    $item->setName(Mage::helper('fooman_advancedpromotions')->__('FREE') . ' ' . $item->getName());
                    $item->setFoomanAutoAddedQty($nrFree);
                    //$rule = Mage::getModel('salesrule/rule')->load($ruleId);
                    //$label = $rule->getStoreLabel($quote->getStore());
                    //$item->setMessage($label);
                    $item->setMessage(Mage::helper('fooman_advancedpromotions')->__('FREE'));
                    Mage::helper('fooman_advancedpromotions')->addAppliedRuleId($item, $ruleId, true);
                    if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                        Mage::helper('fooman_advancedpromotions')->debug('Auto adding '.$nrFree. 'x '.$freeProduct->getName(), Zend_Log::EMERG);
                    }

                    $message = Mage::helper('fooman_advancedpromotions')->__('Your shopping cart qualifies for %sx free %s',
                        $nrFree, $freeProduct->getName());

                    $messageObject = Mage::getModel('core/message_notice', $message);
                    $messageObject->setIdentifier($freeProduct->getId().'-'.$ruleId);
                    $isCartPage = Mage::app()->getRequest()->getControllerName() == 'cart';

                    //only add message once
                    if ($isCartPage){
                        $existingMessages =  Mage::getSingleton('checkout/session')->getMessages(false);
                        if(!$existingMessages->getMessageByIdentifier($messageObject->getIdentifier())){
                            Mage::getSingleton('checkout/session')->addMessage($messageObject);
                        }
                    }
                } catch (Exception $e) {
                    $message = $e->getMessage();
                    if ($isCartPage && strpos(Mage::getSingleton('checkout/session')->getMessages(false)->toString(),
                                    $message) === false) {
                        Mage::getSingleton('checkout/session')->addError($message);
                    }
                }
            }
        }
    }

    public function resetFreeProducts ($observer)
    {
        Mage::helper('fooman_advancedpromotions')->resetCalculations();
        $quote = $observer->getQuote();
        if ($quote) {
            $quote->setFoomanTmpDiscountAmount(0);
            foreach ($quote->getItemsCollection() as $item) {
                if ($item) {
                    if ($item->getOptionByCode('fooman_advancedpromotions_auto_added')) {
                        $item->isDeleted(true);
                        $item->setData('qty_to_add', '0.0000');
                        $quote->removeItem($item->getId());
                    }
                }
            }
        }
    }


    public function resetFreeProductsOnUpdate ($observer)
    {
        $cart = $observer->getCart();
        $info = $observer->getInfo();
        $quote = $cart->getQuote();
        foreach ($info as $itemId => $itemInfo) {
            $item = $cart->getQuote()->getItemById($itemId);
            if ($item) {
                if ($item->getOptionByCode('fooman_advancedpromotions_auto_added')) {
                    if (isset($info[$itemId])) {
                        $info[$itemId]['remove'] = true;
                    }
                }
            }
        }
    }

    /**
     * add "List of Coupons" to dropdown for available coupon types
     * on Magento >= 1.4.1.0
     *
     * @param   Varien_Event_Observer $observer
     */
    public function enableAutoCouponType ($observer)
    {
        if (version_compare(Mage::getVersion(), '1.4.1.0','>=') && version_compare(Mage::getVersion(), '1.7.0.0','<')) {
            $transport = $observer->getEvent()->getTransport();
            //$transport->setIsCouponTypeAutoVisible(true);
            $types = $transport->getCouponTypes();
            $types[Mage_SalesRule_Model_Rule::COUPON_TYPE_AUTO] = Mage::helper('fooman_advancedpromotions')->__('List of Coupons');
            $types = $transport->setCouponTypes($types);
        }
    }

    /**
     * delete all subcoupons if we no longer are using List Of Coupons
     *
     * @param   Varien_Event_Observer $observer
     */
    public function salesruleRuleSaveBefore ($observer)
    {
        //restrict to admin/promo_quote controller
        if( Mage::app()->getRequest()->getModuleName()=='admin' &&
            Mage::app()->getRequest()->getControllerName() == 'promo_quote'){
            if (version_compare(Mage::getVersion(), '1.4.1.0','>=') && version_compare(Mage::getVersion(), '1.7.0.0','<')) {
                $rule = $observer->getEvent()->getRule();
                if ($rule->getCouponType() != Mage_SalesRule_Model_Rule::COUPON_TYPE_AUTO) {
                    //remove all old non-primary coupons
                    $couponsCodesToDelete = $rule->getCoupons();
                    foreach ($couponsCodesToDelete as $oldSubCoupon) {
                        if (!$oldSubCoupon->getIsPrimary()) {
                            $oldSubCoupon->delete();
                        }
                    }
                }
            }
        }
    }

    /**
     * routine to convert csv submitted list from CouponCodeAlternatives field
     * to individual coupons
     *
     * clear salesrule cache tag
     *
     * @param   Varien_Event_Observer $observer
     */
    public function salesruleRuleSaveAfter ($observer)
    {
        Mage::app()->cleanCache(Fooman_AdvancedPromotions_Helper_Data::FOOMAN_ADVPROMO_SALESRULES_CACHETAG);
        //restrict to admin controller
        //restrict to admin/promo_quote controller
        if( Mage::app()->getRequest()->getModuleName()=='admin' &&
            Mage::app()->getRequest()->getControllerName() == 'promo_quote'){
            $rule = $observer->getEvent()->getRule();
            if (
                ($rule->getUsesPerCouponAlternatives() < $rule->getUsesPerCustomer()
                && $rule->getUsesPerCouponAlternatives() > 0
                && $rule->getUsesPerCustomer() > 0 )
                ||
                ($rule->getUsesPerCoupon() < $rule->getUsesPerCustomer())
                && $rule->getUsesPerCoupon() > 0
                && $rule->getUsesPerCustomer() > 0) {

                Mage::getSingleton('adminhtml/session')->addNotice((Mage::helper('fooman_advancedpromotions')->__('Total Usage Limit for Coupon is lower than Customer Usage Limit')));
            }
            if (version_compare(Mage::getVersion(), '1.4.1.0','>=') && version_compare(Mage::getVersion(), '1.7.0.0','<')) {
                $altCouponCodes = $rule->getCouponCodeAlternatives();
                if ($rule->getCouponType() == Mage_SalesRule_Model_Rule::COUPON_TYPE_AUTO) {
                    if ($altCouponCodes) {
                        $altCouponCodes = explode(",",$altCouponCodes);

                        // delete subcoupons that were previously associated to this rule
                        $couponsCodesToDelete = $this->_getSubCouponsToDelete($altCouponCodes, $rule);
                        foreach ($couponsCodesToDelete as $oldSubCoupon) {
                            $oldSubCoupon->delete();
                        }

                        // create new subcoupons
                        $couponsCodesToModify = $this->_getSubCouponsToModify($altCouponCodes, $rule);
                        foreach ($couponsCodesToModify['create'] as $altCouponCode) {
                            $coupon = Mage::getModel('salesrule/coupon');
                            $coupon->setRule($rule)
                                ->setCode(trim($altCouponCode))
                                ->setIsPrimary(false)
                                ->setUsageLimit($rule->getUsesPerCouponAlternatives() ? $rule->getUsesPerCouponAlternatives() : null)
                                ->setUsagePerCustomer($rule->getUsesPerCustomer() ? $rule->getUsesPerCustomer() : null)
                                ->setExpirationDate($rule->getToDate())
                                ->save();
                        }
                        // update existing ones
                        foreach ($couponsCodesToModify['update'] as $altCouponCode) {
                            $coupon = Mage::getModel('salesrule/coupon')->load($altCouponCode, 'code');
                            $coupon ->setIsPrimary(false)
                                ->setUsageLimit($rule->getUsesPerCouponAlternatives() ? $rule->getUsesPerCouponAlternatives() : null)
                                ->setUsagePerCustomer($rule->getUsesPerCustomer() ? $rule->getUsesPerCustomer() : null)
                                ->setExpirationDate($rule->getToDate())
                                ->save();
                        }
                    } else {
                        //remove all old non-primary coupons
                        $couponsCodesToDelete = $rule->getCoupons();
                        foreach ($couponsCodesToDelete as $oldSubCoupon) {
                            if (!$oldSubCoupon->getIsPrimary()) {
                                $oldSubCoupon->delete();
                            }
                        }
                    }
                } else {
                    //remove all old non-primary coupons
                    $couponsCodesToDelete = $rule->getCoupons();
                    foreach ($couponsCodesToDelete as $oldSubCoupon) {
                        if (!$oldSubCoupon->getIsPrimary()) {
                            $oldSubCoupon->delete();
                        }
                    }
                }
            }
        }
    }

    /**
     * add input field for CouponCodeAlternatives to salesrule form
     *
     * @param   Varien_Event_Observer $observer
     */
    public function addAlternativeCoupons ($observer)
    {
        if (version_compare(Mage::getVersion(), '1.4.1.0','>=') && version_compare(Mage::getVersion(), '1.7.0.0','<')) {
            $form = $observer->getEvent()->getForm();
            $prefix = $form->getHtmlIdPrefix();
            $fieldset = $form->getElement('base_fieldset');
            $alternativeCouponCodesField = $fieldset->addField('coupon_code_alternatives', 'textarea', array(
                        'name' => 'coupon_code_alternatives',
                        'label' => Mage::helper('fooman_advancedpromotions')->__('List of Coupon Codes (separate with comma)'),
                        'required' => false,
                        'value' => $this->_getAlternativeCouponsFlat()
                    ));
            $usesPerCouponAlternativesField = $fieldset->addField('uses_per_coupon_alternatives', 'text', array(
                        'name' => 'uses_per_coupon_alternatives',
                        'label' => Mage::helper('fooman_advancedpromotions')->__('Uses per Coupon'),
                        'required' => false,
                        'value' => $this->_getAlternativeCouponUses()
                    ));

            //find current coupon_type element
            foreach($fieldset->getSortedElements() as $element) {
                if($element->getName() == 'coupon_type') {
                    $couponTypeElement = $element;
                }
            }
            //add dependent fields to form_after
            $dependenceWidget = $form->getParent()->getChild('form_after');
            $dependenceWidget
                        ->addFieldMap($alternativeCouponCodesField->getHtmlId(), $alternativeCouponCodesField->getName())
                        ->addFieldMap($usesPerCouponAlternativesField->getHtmlId(), $usesPerCouponAlternativesField->getName())
                        ->addFieldDependence(
                            $alternativeCouponCodesField->getName(),
                            $couponTypeElement->getName(),
                            Mage_SalesRule_Model_Rule::COUPON_TYPE_AUTO)
                        ->addFieldDependence(
                            $usesPerCouponAlternativesField->getName(),
                            $couponTypeElement->getName(),
                            Mage_SalesRule_Model_Rule::COUPON_TYPE_AUTO);
        }
    }

    /**
     * return alternative coupons (non-primary) as comma separated string
     *
     * @param   void
       @return  string
     */
    private function _getAlternativeCouponsFlat ()
    {
        $rule = Mage::registry('current_promo_quote_rule');
        $altCoupons = array();
        $collection = Mage::getResourceModel('salesrule/coupon_collection');
        $collection->addRuleToFilter($rule);
        $collection->addFieldToFilter('is_primary', array(array('eq' => 0), array('null' => 1)));
        foreach ($collection->getItems() as $coupon) {
            $altCoupons[] = $coupon->getCode();
        }
        return implode(",", $altCoupons);
    }

    /**
     * return alternative coupons (non-primary) as comma separated string
     *
     * @param   void
       @return  string
     */
    private function _getAlternativeCouponUses ()
    {
        $rule = Mage::registry('current_promo_quote_rule');
        $collection = Mage::getResourceModel('salesrule/coupon_collection');
        $collection->addRuleToFilter($rule);
        $collection->addFieldToFilter('is_primary', array(array('eq' => 0), array('null' => 1)));
        foreach ($collection->getItems() as $coupon) {
            return $coupon->getUsageLimit();
        }

    }

    /**
     * identify which SubCoupons don't exist anymore in the new list
     *
     * @param array $newCodes
     * @param Mage_SalesRule_Model_Rule $rule
     * @return array of Mage_SalesRule_Model_Rule
     */
    private function _getSubCouponsToDelete ($newCodes, $rule)
    {
        $collection = Mage::getResourceModel('salesrule/coupon_collection');
        $collection->addRuleToFilter($rule);
        $collection->addFieldToFilter('code', array('nin' => $newCodes));
        return $collection->getItems();
    }

    /**
     * identify which SubCoupons need to be updated and which need to be created
     *
     * @param array $toCreate
     * @param Mage_SalesRule_Model_Rule $rule
     * @return array of Mage_SalesRule_Model_Rule
     */
    private function _getSubCouponsToModify ($toCreate, $rule)
    {
        $toUpdate = array();
        $collection = Mage::getResourceModel('salesrule/coupon_collection');
        $collection->addRuleToFilter($rule);
        $collection->addFieldToFilter('code', array('in' => $toCreate));
        foreach ($collection->getItems() as $coupon) {
            $toUpdate[] = $toCreate[array_search($coupon->getCode(), $toCreate)];
            unset($toCreate[array_search($coupon->getCode(), $toCreate)]);
        }
        return array('create' => $toCreate, 'update' => $toUpdate);
    }

    /**
     * AdvancedPromotions discount calculation
     *
     * @param   Varien_Event_Observer $observer
     */
    public function applyDiscount ($observer)
    {
        $rule = $observer->getEvent()->getRule();
        $item = $observer->getEvent()->getItem();
        Mage::helper('fooman_advancedpromotions')->removeAppliedRuleId($item, $rule->getId(), true);
        $address = $observer->getEvent()->getAddress();
        $quote = $observer->getEvent()->getQuote();
        $qty = $observer->getEvent()->getQty();
        $result = $observer->getEvent()->getResult();
        $startDiscountAmount = $result->getDiscountAmount();
        $isMulti = ($item instanceof Mage_Sales_Model_Quote_Address_Item)?true:false;

        //keep track of discount being added during this calculation by other rules
        $quote->setFoomanTmpDiscountAmount($quote->getFoomanTmpDiscountAmount() + $result->getDiscountAmount());
        $quote->setBaseFoomanTmpDiscountAmount($quote->getBaseFoomanTmpDiscountAmount() + $result->getBaseDiscountAmount());

        $itemPrice = $this->getItemPrice($item, false);
        $baseItemPrice = $this->getItemPrice($item, true);

        $rulePercent = null;
        if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
            Mage::helper('fooman_advancedpromotions')->debug('************************* '.$rule->getSimpleAction().' ID '.$rule->getId().' *************************',Zend_Log::EMERG);
            Mage::helper('fooman_advancedpromotions')->debug($item->getName(),Zend_Log::EMERG);
        }

        Varien_Profiler::start('SUPER PROMO');
        switch ($rule->getSimpleAction()){
            case 'super_cart_fixed':
                //Mage::helper('fooman_advancedpromotions')->saveFixedRule($rule->getId(), $rule->getDiscountAmount());
                $items = $address->getAllVisibleItems();

                if (!count($items)) {
                    return $this;
                }
                $quote = $address->getQuote();
                $baseCartDiscount = $rule->getDiscountAmount();
                $cartDiscount = $quote->getStore()->convertPrice($baseCartDiscount);

                foreach ($items as $currentItem) {
                    //all items participate in cart discount
                    $currentItem->setFoomanActiveInGroup(true);
                    if (Mage::helper('fooman_advancedpromotions')->doItemsMatch($currentItem, $item)) {
                        if(!$currentItem->getFoomanFirstPass()){
                            $currentItem->setFoomanFirstPass(true);
                            $currentItem->setPriorDiscountAmount($currentItem->getDiscountAmount());
                            $currentItem->setBasePriorDiscountAmount($currentItem->getBaseDiscountAmount());
                        }
                    }
                    Mage::helper('fooman_advancedpromotions')->addAppliedRuleId($currentItem, $rule->getId(), true);

                    $itemPrice = Mage::helper('fooman_advancedpromotions')->getItemPrice($currentItem, false);
                    $baseItemPrice = Mage::helper('fooman_advancedpromotions')->getItemPrice($currentItem, true);

                    //apply discount
                    if ($cartDiscount > 0) {
                        $discountAmount =  min((($itemPrice*$currentItem->getQty()) - $currentItem->getPriorDiscountAmount()), $cartDiscount);
                        $baseDiscountAmount =  min((($baseItemPrice*$currentItem->getQty()) - $currentItem->getBasePriorDiscountAmount()), $baseCartDiscount);

                        $cartDiscount -= $discountAmount;
                        $baseCartDiscount -= $baseDiscountAmount;

                        if (Mage::helper('fooman_advancedpromotions')->doItemsMatch($currentItem, $item)) {
                            $currentItem->setDiscountAmount($discountAmount + $currentItem->getPriorDiscountAmount());
                            $currentItem->setBaseDiscountAmount($baseDiscountAmount + $currentItem->getBasePriorDiscountAmount());

                        }
                    } else {
                        //no more discount to distribute - we are done here
                        break;
                    }
                }
                break;
            case 'super_buy_x_get_y_fixed':
                break;
            case 'super_buy_x_get_y_off':
                break;
            case 'super_buy_x_get_y_at_discount':
                if (!$rule->getDiscountAmount()) {
                    break;
                }
                $rulePercentFactor = min(1, $rule->getDiscountAmount()/100);
                //no break
            case 'super_buy_x_get_y':
                if (!$rule->getDiscountAmount()) {
                    break;
                }
                if(!isset($rulePercentFactor)) {
                    $rulePercentFactor = 1;
                }

                $condRegistryKey = 'COND_'.$rule->getSimpleAction() . '_' . $rule->getId() . '_' . $quote->getId();
                $conditionsMatched = Mage::helper('fooman_advancedpromotions')->loadCalculation($condRegistryKey);

                if (!$conditionsMatched) {
                    if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                        Mage::helper('fooman_advancedpromotions')->debug($rule->getConditions()->validateItems($quote,$address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG),Zend_Log::EMERG, null, 'MASTER CONDITIONS MATCHED');
                    }
                    $conditionsMatched = $rule->getConditions()->validateItems($quote,$address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED);
                    Mage::helper('fooman_advancedpromotions')->saveCalculation($condRegistryKey, $conditionsMatched);
                }
                if(!is_array($conditionsMatched) && $conditionsMatched['rule_qty']) {
                    //nothing to do here
                    break;
                }

                $conditionsMatchedItems = Mage::helper('fooman_advancedpromotions')->onlyMatchedItems($conditionsMatched);
                foreach ($conditionsMatchedItems as $matchedItem) {
                    if (Mage::helper('fooman_advancedpromotions')->doItemsMatch($matchedItem['item'], $item)) {
                        Mage::helper('fooman_advancedpromotions')->addAppliedRuleId($item, $rule->getId(), true);
                        $item->setFoomanActiveInGroup(true);
                    }
                }

                $this->_groupsSummedUp = array();
                $this->_subItemsToDiscount = array();
                $registryKey = 'ACTION_'.$rule->getSimpleAction() . '_' . $rule->getId() . '_' . $quote->getId();
                $actionsMatched = Mage::helper('fooman_advancedpromotions')->loadCalculation($registryKey);

                if (!$actionsMatched) {
                    $actionsMatched = $rule->getActions()->validateItems($quote, $address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED, true, 1, 'simple', $conditionsMatched);
                    if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                        Mage::helper('fooman_advancedpromotions')->debug('__________________________________________________________________________________________________________________________________', Zend_Log::EMERG);
                        Mage::helper('fooman_advancedpromotions')->debug($rule->getActions()->validateItems($quote, $address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG, true, 1, 'simple', $conditionsMatched), Zend_Log::EMERG, null, 'MASTER ACTIONS MATCHED');
                    }
                    Mage::helper('fooman_advancedpromotions')->saveCalculation($registryKey, $actionsMatched);
                }

                if (!is_array($actionsMatched)) {
                    //nothing to do here - continue with next rule
                    break;
                }
                $this->_groupActionsMatched($actionsMatched);

                foreach ($this->_groupsSummedUp as $attrKey=>$attrCollected) {
                    foreach ($attrCollected as $attrValue=>$itemsToDiscount) {
                        switch ($actionsMatched['rule_type']) {
                                case Fooman_AdvancedPromotions_Helper_Data::ACTION_GROUP_INDEPENDANT:
                                    $nrGroupFree = Mage::helper('fooman_advancedpromotions')->reduceQty($conditionsMatched['rule_qty']);
                                    break;
                                case Fooman_AdvancedPromotions_Helper_Data::ACTION_GROUP_MATCHED:
                                case Fooman_AdvancedPromotions_Helper_Data::ACTION_GROUP_SIMPLE:
                                default:
                                    //remove the rule splitter
                                    if (strpos($attrKey,'|') > 0) {
                                        $tmpAttrKey = explode('|',$attrKey);
                                        $attrKey = $tmpAttrKey[0];
                                    }
                                    //remove the rule splitter
                                    if (strpos($attrValue,'|') > 0) {
                                        $tmpAttrKey = explode('|',$attrValue);
                                        $attrValue = $tmpAttrKey[0];
                                    }
                                    if (!isset($conditionsMatched['rule_qty'][$attrKey][$attrValue])) {
                                        $nrGroupFree = 0;
                                        continue;
                                    }
                                    $nrGroupFree = $conditionsMatched['rule_qty'][$attrKey][$attrValue];
                                    break;

                                    //need to reduce matches to 1 total
                                    $nrGroupFree = Mage::helper('fooman_advancedpromotions')->reduceQty($conditionsMatched['rule_qty']);
                                    break;
                        }
                        if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                            Mage::helper('fooman_advancedpromotions')->debug('$attrKey '.$attrKey, Zend_Log::EMERG);
                            Mage::helper('fooman_advancedpromotions')->debug('$attrValue '.$attrValue, Zend_Log::EMERG);
                            Mage::helper('fooman_advancedpromotions')->debug($conditionsMatched['rule_qty'], Zend_Log::EMERG);
                            Mage::helper('fooman_advancedpromotions')->debug('$nrGroupFree '.$nrGroupFree, Zend_Log::EMERG);
                        }

                        $itemsSorted = array();
                        $itemsToDiscount = $this->sortItemsToDiscount($itemsToDiscount, $rule->getFoomanDiscountSelect(), $rule->getFoomanDiscountGroupsize());
                        foreach($itemsToDiscount as $priceGroup) {
                            foreach ($priceGroup as $sortItem) {
                                $itemsSorted[] = $sortItem;
                            }
                        }
                        $subtotalModel = Mage::getModel(
                            'fooman_advancedpromotions/salesRule_rule_condition_product_subtotal'
                        );
                        if ($subtotalModel->shouldApplySubtotal($rule->getId())) {
                            $nrGroupFree = $subtotalModel->calcDiscounted($address, $rule, $itemsSorted);
                        }

                        $qtyStep = ($rule->getDiscountStep() > 0)?$rule->getDiscountStep():1;
                        if($rulePercentFactor < 1){
                            $totalItemsToDiscount = floor($nrGroupFree/$qtyStep);
                        } else {
                            $totalItemsToDiscount = floor($rule->getDiscountAmount() * $nrGroupFree/$qtyStep);
                        }
                        if ($rule->getDiscountQty() > 0) {
                            $totalItemsToDiscount = min($rule->getDiscountQty(),$totalItemsToDiscount);
                        }
                        if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                            Mage::helper('fooman_advancedpromotions')->debug('$totalItemsToDiscount '.$totalItemsToDiscount, Zend_Log::EMERG);
                        }
                        foreach ($itemsSorted as $currentItem) {
                            if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                                Mage::helper('fooman_advancedpromotions')->debug('IDs. '.$currentItem->getId().'/'.$item->getId().' $currentItem->getName() '.$currentItem->getName(), Zend_Log::EMERG);
                            }

                            //we are done - no more items to discount
                            if ($totalItemsToDiscount <= 0) {
                                break;
                            }
                            $currentItem->setFoomanActiveInGroup(true);

                            if ($currentItem->getFoomanQtyToDiscount() == 0) {
                                continue;
                            }

                            Mage::helper('fooman_advancedpromotions')->debug('$totalItemsToDiscount: ' .$totalItemsToDiscount, Zend_Log::EMERG);
                            //work out how many of the current items belong to the discount group
                            if ($totalItemsToDiscount >= $currentItem->getQty()) {
                                $multiplier = $currentItem->getQty();
                            }else {
                                $multiplier = $totalItemsToDiscount;
                            }
                            if ($multiplier > $currentItem->getFoomanQtyToDiscount()) {
                                $multiplier = $currentItem->getFoomanQtyToDiscount();
                            }

                            $itemPrice = $this->getItemPrice($item, false);
                            $baseItemPrice = $this->getItemPrice($item, true);
                            $totalItemsToDiscount -= $multiplier;
                            Mage::helper('fooman_advancedpromotions')->debug('$totalItemsToDiscount: ' .$totalItemsToDiscount .' $multiplier '.$multiplier. ' $rulePercentFactor '.$rulePercentFactor, Zend_Log::EMERG);
                            $discountAmount =  $itemPrice * $multiplier * $rulePercentFactor;
                            $baseDiscountAmount = $baseItemPrice * $multiplier * $rulePercentFactor;
                            Mage::helper('fooman_advancedpromotions')->debug('$discountAmount: ' .$discountAmount .' $itemPrice '.$itemPrice, Zend_Log::EMERG);
                            if(!$isMulti) {
                                $discountAmount -= $currentItem->getDiscountAmount();
                                $baseDiscountAmount -= $currentItem->getBaseDiscountAmount();
                            }

                            Mage::helper('fooman_advancedpromotions')->debug('discountAmount: ' .$discountAmount, Zend_Log::EMERG);
                            //if current item is a bundle - distribute the discount over the subitems
                            if ($currentItem->getProductType() == 'bundle') {

                                //need to adjust item price to parent since this is the total we distribute over the subitems below
                                $itemPrice = $this->getItemPrice($currentItem, false);
                                $baseItemPrice = $this->getItemPrice($currentItem, true);

                                $discountAmount =  $itemPrice * $multiplier * $rulePercentFactor;
                                $baseDiscountAmount = $baseItemPrice * $multiplier * $rulePercentFactor;

                                if(!$isMulti) {
                                    $discountAmount -= $currentItem->getDiscountAmount();
                                    $baseDiscountAmount -= $currentItem->getBaseDiscountAmount();
                                }

                                if ($discountAmount > 0) {
                                    $subitemDiscountLeftToDistribute = $discountAmount;
                                    $baseSubitemDiscountLeftToDistribute = $baseDiscountAmount;
                                    foreach ($currentItem->getChildren() as $currentSubitem) {
                                        $subitemDiscountAmount =  min($currentSubitem->getRowTotal(), $subitemDiscountLeftToDistribute);
                                        $baseSubitemDiscountAmount =  min($currentSubitem->getBaseRowTotal(), $baseSubitemDiscountLeftToDistribute);
                                        $subitemDiscountLeftToDistribute -= $subitemDiscountAmount;
                                        $baseSubitemDiscountLeftToDistribute -= $baseSubitemDiscountAmount;
                                        if(Mage::helper('fooman_advancedpromotions')->doItemsMatch($currentSubitem, $item)) {
                                            if($subitemDiscountAmount > $result->getDiscountAmount()) {
                                                if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                                                    Mage::helper('fooman_advancedpromotions')->debug('discounting subitem: ' .$multiplier.' x ' . $item->getName().' by '.$subitemDiscountAmount, Zend_Log::EMERG);
                                                }
                                                Mage::helper('fooman_advancedpromotions')->addAppliedRuleId($item, $rule->getId(), true);
                                                $result->setDiscountAmount($subitemDiscountAmount);
                                                $result->setBaseDiscountAmount($baseSubitemDiscountAmount);
                                            } else {
                                                if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                                                    Mage::helper('fooman_advancedpromotions')->debug('NOT discounting subitem: ' . $item->getName() .' since existing discount is higher '. $result->getDiscountAmount(), Zend_Log::EMERG);
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                            }

                            //if the current loop item is the item we query set the discount
                            if(Mage::helper('fooman_advancedpromotions')->doItemsMatch($currentItem, $item)) {
                                if($discountAmount > $result->getDiscountAmount()) {
                                    Mage::helper('fooman_advancedpromotions')->addAppliedRuleId($item, $rule->getId(), true);
                                    if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                                        Mage::helper('fooman_advancedpromotions')->debug('discounting item: '.$multiplier.' x ' . $item->getName().' by '.$discountAmount, Zend_Log::EMERG);
                                    }
                                    $result->setDiscountAmount($discountAmount);
                                    $result->setBaseDiscountAmount($baseDiscountAmount);
                                } else {
                                    if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                                        Mage::helper('fooman_advancedpromotions')->debug('NOT discounting item: '.$multiplier.' x ' . $item->getName().' by '.$discountAmount .' since existing discount is higher '. $result->getDiscountAmount(), Zend_Log::EMERG);
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
                Varien_Profiler::stop('SUPER PROMO');
                break;

            case 'super_buy_x_for_fixed_amount':
                if (!$rule->getDiscountStep()) {
                    break;
                }

                //work out which items in cart are covered under actions
                $itemsToDiscount = array();
                $subitemsToDiscount = array();

                $this->_groupsSummedUp = array();
                $this->_subItemsToDiscount = array();
                $registryKey = 'ACTION_'.$rule->getSimpleAction() . '_' . $rule->getId() . '_' . $quote->getId();

                $actionsMatched = Mage::helper('fooman_advancedpromotions')->loadCalculation($registryKey);
                if (!$actionsMatched) {
                    $actionsMatched = $rule->getActions()->validateItems($quote, $address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED, true, 1, 'simple');
                    if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                        Mage::helper('fooman_advancedpromotions')->debug($rule->getActions()->validateItems($quote, $address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG, true, 1, 'simple'), Zend_Log::EMERG, null, 'MASTER ACTIONS MATCHED');
                    }
                    Mage::helper('fooman_advancedpromotions')->saveCalculation($registryKey, $actionsMatched);
                }

                if (!is_array($actionsMatched)) {
                    //nothing to do here - continue with next rule
                    if ($rule->setStopRulesProcessing()) {
                        $rule->setStopRulesProcessing(false);
                    }
                    break;
                }

                if ($actionsMatched['rule_qty'] < $rule->getFoomanMinQty()) {
                    if ($rule->setStopRulesProcessing()) {
                        $rule->setStopRulesProcessing(false);
                    }
                    break;
                }

                //apply discount
                $applyRuleNTimes = floor(Mage::helper('fooman_advancedpromotions')->reduceQty($actionsMatched['rule_qty'])/$rule->getDiscountStep());
                if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                    Mage::helper('fooman_advancedpromotions')->debug('applyRuleNTimes '.$applyRuleNTimes, Zend_Log::EMERG);
                }
                if(!$applyRuleNTimes) {
                    //nothing to do here
                    if ($rule->setStopRulesProcessing()) {
                        $rule->setStopRulesProcessing(false);
                    }
                    break;
                }

                $this->_groupActionsMatched($actionsMatched);

                $itemsSorted = array();
                $totalQtyMatched = 0;
                foreach ($this->_groupsSummedUp as $attrKey=>$attrCollected) {
                    foreach ($attrCollected as $attrValue=>$itemsToDiscount) {
                        //reverse order since we grab payment first and discount everything after
                        $itemsToDiscount = $this->sortItemsForFixedAmount($itemsToDiscount, $rule->getFoomanDiscountSelect() , $rule->getFoomanDiscountGroupsize());
                        foreach($itemsToDiscount as $priceGroup) {
                            foreach ($priceGroup as $sortItem) {
                                $itemsSorted[] = $sortItem;
                                $totalQtyMatched += $sortItem->getQty();
                            }
                        }
                    }
                }

                $distributeDiscount=false;
                if($distributeDiscount) {
                    $fixedAmount = Mage::app()->getStore()->roundPrice($quote->getStore()->convertPrice($rule->getDiscountAmount()) / $rule->getDiscountStep());
                    $baseFixedAmount = Mage::app()->getStore()->roundPrice($rule->getDiscountAmount() / $rule->getDiscountStep());
                    $roundingAmount = ($quote->getStore()->convertPrice($rule->getDiscountAmount()) - ($fixedAmount * $rule->getDiscountStep()))* $applyRuleNTimes;
                } else {
                    $fixedAmount = $quote->getStore()->convertPrice($rule->getDiscountAmount()) * $applyRuleNTimes;
                    $baseFixedAmount = $rule->getDiscountAmount() * $applyRuleNTimes;
                }
                $baseRoundingAmount = ($rule->getDiscountAmount() - ($baseFixedAmount * $rule->getDiscountStep()))* $applyRuleNTimes;
                $totalItemsToDiscount = $rule->getDiscountStep() * $applyRuleNTimes;

                if ($rule->getDiscountQty() > 0) {
                    $totalItemsToDiscount = min($rule->getDiscountQty(),$totalItemsToDiscount);
                }

                foreach ($itemsSorted as $currentItem) {
                    //we are done - no more items to discount
                    if (!$totalItemsToDiscount) {
                        break;
                    }
                    $currentItem->setFoomanActiveInGroup(true);
                    Mage::helper('fooman_advancedpromotions')->addAppliedRuleId($currentItem, $rule->getId(), true);
                    //work out how many of the current items belong to the discount group
                    if ($totalItemsToDiscount >= $currentItem->getQty()) {
                        $multiplier = $currentItem->getQty();
                    }else {
                        $multiplier = $totalItemsToDiscount;
                    }
                    $totalItemsToDiscount -= $multiplier;

                    $itemPrice = $this->getItemPrice($currentItem, false);
                    $baseItemPrice = $this->getItemPrice($currentItem, true);

                    //get amount first
                    if ($fixedAmount > 0) {
                        $discountAmount =  $itemPrice * $multiplier - $fixedAmount;
                        $baseDiscountAmount = $baseItemPrice * $multiplier - $baseFixedAmount;

                        $fixedAmount -= $itemPrice * $multiplier;
                        $baseFixedAmount -= $baseItemPrice * $multiplier ;
                    } else {
                        //all paid discount the complete price
                        $discountAmount =  $itemPrice * $multiplier;
                        $baseDiscountAmount = $baseItemPrice * $multiplier;
                    }

                    if ($discountAmount <= 0) {
                        $discountAmount =  0;
                        $baseDiscountAmount = 0;
                    }

                    //if current item is a bundle - distribute the discount over the subitems
                    if ($discountAmount > 0 && $currentItem->getProductType() == 'bundle') {

                        $subitemDiscountLeftToDistribute = $discountAmount;
                        $baseSubitemDiscountLeftToDistribute = $baseDiscountAmount;
                        foreach ($currentItem->getChildren() as $currentSubitem){
                            $subitemDiscountAmount =  min($currentSubitem->getRowTotal(), $subitemDiscountLeftToDistribute);
                            $baseSubitemDiscountAmount =  min($currentSubitem->getBaseRowTotal(), $baseSubitemDiscountLeftToDistribute);
                            $subitemDiscountLeftToDistribute -= $subitemDiscountAmount;
                            $baseSubitemDiscountLeftToDistribute -= $baseSubitemDiscountAmount;
                            if(Mage::helper('fooman_advancedpromotions')->doItemsMatch($currentSubitem, $item)) {
                                if($subitemDiscountAmount > $result->getDiscountAmount()) {
                                    $result->setDiscountAmount($subitemDiscountAmount);
                                    $result->setBaseDiscountAmount($baseSubitemDiscountAmount);
                                    Mage::helper('fooman_advancedpromotions')->addAppliedRuleId($item, $rule->getId(), true);
                                }
                                break;
                            }
                        }
                    }

                    //if the current loop item is the item we query set the discount
                    if(Mage::helper('fooman_advancedpromotions')->doItemsMatch($currentItem, $item)) {
                        if($discountAmount > $result->getDiscountAmount()) {
                            if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                                Mage::helper('fooman_advancedpromotions')->debug('discounting item: '.$multiplier.' x ' . $item->getName().' by '.$discountAmount, Zend_Log::EMERG);
                            }
                            $result->setDiscountAmount($discountAmount);
                            $result->setBaseDiscountAmount($baseDiscountAmount);
                            Mage::helper('fooman_advancedpromotions')->addAppliedRuleId($item, $rule->getId(), true);
                        } else {
                            if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                                Mage::helper('fooman_advancedpromotions')->debug('NOT discounting item: '.$multiplier.' x ' . $item->getName().' by '.$discountAmount.' since existing discount is higher', Zend_Log::EMERG);
                            }
                        }
                        break;
                    }
                }
                Varien_Profiler::stop('SUPER PROMO');
                break;
        }

    }

    /**
     * Add AdvancedPromotions new actions to salesrule dropdown menu
     *
     * @param   Varien_Event_Observer $observer
     */
    public function addActions($observer)
    {
        $form = $observer->getEvent()->getForm();
        $prefix = $form->getHtmlIdPrefix();
        $fieldset = $form->getElement('action_fieldset');
        $fieldset->removeField('simple_action');
        $simpleActionElement = $fieldset->addField('simple_action', 'select', array(
            'label'     => Mage::helper('salesrule')->__('Apply'),
            'name'      => 'simple_action',
            'options'    => array(
                'by_percent' => Mage::helper('salesrule')->__('Percent of product price discount'),
                'by_fixed' => Mage::helper('salesrule')->__('Fixed amount discount'),
                'cart_fixed' => Mage::helper('salesrule')->__('Fixed amount discount for whole cart'),
                'buy_x_get_y' => Mage::helper('salesrule')->__('Buy X get Y free (discount amount is Y)'),
                'super_buy_x_for_fixed_amount' => Mage::helper('fooman_advancedpromotions')->__('ADVANCED Buy X for fixed amount (discount amount = tax excl.)'),
                'super_buy_x_get_y' => Mage::helper('fooman_advancedpromotions')->__('ADVANCED Buy Conditions get Y free (discount amount is Y)'),
                'super_buy_x_get_y_at_discount' => Mage::helper('fooman_advancedpromotions')->__('ADVANCED Buy Conditions get a discount'),
                'super_cart_fixed' => Mage::helper('fooman_advancedpromotions')->__('ADVANCED Fixed amount discount for whole cart'),
            ),
        ));

        $discountModeField = $fieldset->addField(
            'fooman_discount_select',
            'select',
            array(
                 'label'   => Mage::helper('fooman_advancedpromotions')->__('Discount'),
                 'name'    => 'fooman_discount_select',
                 'options' => Mage::getModel('fooman_advancedpromotions/system_discountMode')->getOptions()
            )
        );
        $groupsizeField = $fieldset->addField(
            'fooman_discount_groupsize',
            'text',
            array(
                 'label' => Mage::helper('fooman_advancedpromotions')->__('Group Size'),
                 'name'  => 'fooman_discount_groupsize'
            )
        );

        $minQtyField = $fieldset->addField(
            'fooman_min_qty',
            'text',
            array(
                 'label' => Mage::helper('fooman_advancedpromotions')->__('Minimum Qty'),
                 'name'  => 'fooman_min_qty'
            )
        );

        $dependenceWidget = Mage::app()->getLayout()->createBlock('adminhtml/widget_form_element_dependence');
        $dependenceWidget
            ->addFieldMap($simpleActionElement->getHtmlId(),$simpleActionElement->getName())
            ->addFieldMap($discountModeField->getHtmlId(), $discountModeField->getName())
            ->addFieldMap($groupsizeField->getHtmlId(), $groupsizeField->getName())
            ->addFieldMap($minQtyField->getHtmlId(), $minQtyField->getName())
            ->addFieldDependence(
                $groupsizeField->getName(),
                $discountModeField->getName(),
                Fooman_AdvancedPromotions_Model_System_DiscountMode::SMART_SHOPPER
            );
        if (version_compare(Mage::getVersion(), '1.7.0.0', '=>')) {
            $dependenceWidget->addFieldDependence(
                $discountModeField->getName(),
                $simpleActionElement->getName(),
                array('super_buy_x_for_fixed_amount', 'super_buy_x_get_y', 'super_buy_x_get_y_at_discount')
            )
                ->addFieldDependence(
                    $minQtyField->getName(),
                    $simpleActionElement->getName(),
                    array('super_buy_x_for_fixed_amount')
                )
                ->addFieldDependence(
                    $groupsizeField->getName(),
                    $simpleActionElement->getName(),
                    array('super_buy_x_for_fixed_amount', 'super_buy_x_get_y', 'super_buy_x_get_y_at_discount')
                );
        }
        Mage::app()->getLayout()->getBlock('promo_quote_edit_tab_actions')->setChild('form_after', $dependenceWidget);
    }

    /**
     * Add AdvancedPromotions group options to salesrule dropdown menu
     *
     * @param   Varien_Event_Observer $observer
     */
    public function addGroupFilter($observer)
    {
        $additional = $observer->getEvent()->getAdditional();
        $existingChoices = $additional->getConditions();
        $choices = array(
            array('value'=>'fooman_advancedpromotions/salesRule_rule_condition_product_groupSimple', 'label'=>Mage::helper('fooman_advancedpromotions')->__('For each group')),
            array('value'=>'fooman_advancedpromotions/salesRule_rule_condition_product_groupIndependant', 'label'=>Mage::helper('fooman_advancedpromotions')->__('For each group with attribute Z')),
            array('value'=>'fooman_advancedpromotions/salesRule_rule_condition_product_subtotal', 'label'=>Mage::helper('fooman_advancedpromotions')->__('Basket value')),
            array('value'=>'fooman_advancedpromotions/salesRule_rule_condition_product_groupSubselectQty', 'label'=>Mage::helper('fooman_advancedpromotions')->__('Number of items'))
            //array('value'=>'fooman_advancedpromotions/salesRule_rule_condition_product_groupMatched', 'label'=>Mage::helper('fooman_advancedpromotions')->__('ADVANCED matched Groups'))
                );
        if(is_array($existingChoices)) {
            $additional->setConditions(array_merge_recursive(
                $existingChoices,
                array(array('value'=>$choices, 'label'=>Mage::helper('fooman_advancedpromotions')->__('AdvancedPromotions')))

            ));
        } else  {
            $additional->setConditions(
                array(array('value'=>$choices, 'label'=>Mage::helper('fooman_advancedpromotions')->__('AdvancedPromotions')))

            );
        }
    }


    private function _loadProduct ($productId = null)
    {
        return Mage::helper('fooman_advancedpromotions')->loadProduct($productId);
    }

    private function _groupActionsMatched ($actionsMatched)
    {

        if(!isset($actionsMatched['rule_type'])){
            return false;
        }

        //default is a two level deep nested result [2][2]
        //however just using the default combine action can in some instances only have 1 level
        Mage::helper('fooman_advancedpromotions')->debug('$actionsMatched[rule_type] '.$actionsMatched['rule_type'],Zend_Log::DEBUG);
        /*if(isset($actionsMatched[2][2]) && is_array($actionsMatched[2][2])) {
            $actionsGroups = $actionsMatched[2][2];
        } else {
            //we only have one level of results - reformat
            $actionsGroups = array();
            $actionsGroups = $actionsMatched[2];
        }*/

        switch ($actionsMatched['rule_type']) {
            case Fooman_AdvancedPromotions_Helper_Data::ACTION_GROUP_MATCHED:
                foreach ($actionsMatched['matches'] as $attrKey => $actionsAttrGroups) {
                    foreach ($actionsAttrGroups as $attrValue => $actionsAttrItems) {
                        foreach ($actionsAttrItems as $actionsAttrItem) {
                            $price = $actionsAttrItem['item']->getCalculationPrice() * 100;
                            //only count composite items - not their subitems
                            $parentItemId = $actionsAttrItem['item']->getParentItemId();
                            if (!$price && $parentItemId) {
                                $price = $this->_loadProduct($parentItemId)->getCalculationPrice() * 100;
                            }
                            $actionItemKey = Mage::helper('fooman_advancedpromotions')->getItemKey($actionsAttrItem['item']);
                            $actionsAttrItem['item']->setFoomanQtyToDiscount($actionsAttrItem['item']->getQty());
                            if (!$parentItemId) {
                                $this->_groupsSummedUp[$attrKey][$attrValue][$price][$actionItemKey] = $actionsAttrItem['item'];
                            } else {
                                $this->_subItemsToDiscount[$parentItemId][$actionItemKey] = $actionsAttrItem['item'];
                            }
                        }
                    }
                }
                break;

            default:
            case Fooman_AdvancedPromotions_Helper_Data::ACTION_GROUP_INDEPENDANT:
                foreach ($actionsMatched['matches'] as $attrKey => $actionsAttrGroups) {
                    foreach ($actionsAttrGroups as $attrValue => $actionsAttrItems) {
                        foreach ($actionsAttrItems as $actionsAttrItem) {
                            $price = $actionsAttrItem['item']->getCalculationPrice() * 100;
                            //only count composite items - not their subitems
                            $parentItemId = $actionsAttrItem['item']->getParentItemId();
                            if (!$price && $parentItemId) {
                                $price = $this->_loadProduct($parentItemId)->getCalculationPrice() * 100;
                            }
                            $actionItemKey = Mage::helper('fooman_advancedpromotions')->getItemKey($actionsAttrItem['item']);
                            $actionsAttrItem['item']->setFoomanQtyToDiscount($actionsAttrItem['item']->getQty());
                            if (!$parentItemId) {
                                $this->_groupsSummedUp[$attrKey][$attrValue][$price][$actionItemKey] = $actionsAttrItem['item'];
                            } else {
                                $this->_subItemsToDiscount[$parentItemId][$actionItemKey] = $actionsAttrItem['item'];
                            }
                        }
                    }
                }
                break;


            case Fooman_AdvancedPromotions_Helper_Data::ACTION_GROUP_SIMPLE:
                foreach ($actionsMatched['matches'] as $attrKey => $actionsAttrGroups) {
                    foreach ($actionsAttrGroups as $groupKey => $actionsAttrItems) {
                        foreach ($actionsAttrItems as $actionsAttrItem) {
                            $price = $actionsAttrItem['item']->getCalculationPrice() * 100;
                            //only count composite items - not their subitems
                            $parentItemId = $actionsAttrItem['item']->getParentItemId();
                            if (!$price && $parentItemId) {
                                $price = $this->_loadProduct($parentItemId)->getCalculationPrice() * 100;
                            }
                            $actionItemKey = Mage::helper('fooman_advancedpromotions')->getItemKey($actionsAttrItem['item']);
                            $actionsAttrItem['item']->setFoomanQtyToDiscount($actionsAttrItem['item']->getQty());
                            if (!$parentItemId) {
                                $this->_groupsSummedUp['group'][$groupKey][$price][$actionItemKey] = $actionsAttrItem['item'];
                            } else {
                                $this->_subItemsToDiscount[$parentItemId][$actionItemKey] = $actionsAttrItem['item'];
                            }
                        }
                    }
                }
                break;
        }
    }

    public function sortItemsToDiscount($items, $mode, $groupSize = false)
    {
        if ($mode == Fooman_AdvancedPromotions_Model_System_DiscountMode::CHEAPEST) {
            ksort($items); //least expensive first
        } elseif ($mode == Fooman_AdvancedPromotions_Model_System_DiscountMode::PRICIEST) {
            krsort($items); //most expensive first
        } elseif ($mode == Fooman_AdvancedPromotions_Model_System_DiscountMode::SMART_SHOPPER) {
            krsort($items); //most expensive first
            $groupCounter = $groupSize;
            foreach($items as $priceGroup){
                foreach ($priceGroup as $item){
                    $discountQty = 0;
                    if($item->getQty() == $groupSize){
                        $discountQty++;
                    } else {
                        if($item->getQty() > $groupSize) {
                            $discountQty = floor($item->getQty()/$groupSize);
                            $remainder = $item->getQty()%$groupSize;
                            if ($remainder >= $groupCounter) {
                                $discountQty++;
                                $groupCounter = $groupSize;
                            } else {
                                $groupCounter -= $remainder;
                            }
                        } else {
                            if ($item->getQty() >= $groupCounter) {
                                $discountQty++;
                                $groupCounter = $groupSize -($item->getQty() - $groupCounter);
                            } else {
                                $groupCounter -= $item->getQty();
                            }
                        }
                    }
                    $item->setFoomanQtyToDiscount($discountQty);
                }
            }
        }
        $transport = new Varien_Object();
        $transport->setItems($items);
        Mage::dispatchEvent(
            'fooman_advancedpromotions_sorted_items_to_discount',
            array(
                'mode'      => $mode,
                'transport' => $transport
            )
        );
        return $transport->getItems();
    }

    public function sortItemsForFixedAmount($items, $mode, $groupSize = false)
    {
        if ($mode == Fooman_AdvancedPromotions_Model_System_DiscountMode::CHEAPEST) {
            ksort($items); //most expensive first
        } elseif ($mode == Fooman_AdvancedPromotions_Model_System_DiscountMode::PRICIEST) {
            krsort($items); //least expensive first
        } elseif ($mode == Fooman_AdvancedPromotions_Model_System_DiscountMode::SMART_SHOPPER) {
            krsort($items); //least expensive first
            $groupCounter = $groupSize;
            foreach($items as $priceGroup){
                foreach ($priceGroup as $item){
                    $discountQty = 0;
                    if (($item->getQty() % $groupSize) == 0) {
                        $discountQty = $item->getQty();
                    } else {
                        if($item->getQty() > $groupSize) {
                            $discountQty = floor($item->getQty()/$groupSize);
                            $remainder = $item->getQty()%$groupSize;
                            if ($remainder >= $groupCounter) {
                                $discountQty++;
                                $groupCounter = $groupSize;
                            } else {
                                $groupCounter -= $remainder;
                            }
                        } else {
                            if ($item->getQty() >= $groupCounter) {
                                $discountQty++;
                                $groupCounter = $groupSize -($item->getQty() - $groupCounter);
                            } else {
                                $groupCounter -= $item->getQty();
                            }
                        }
                    }
                    $item->setFoomanQtyToDiscount($discountQty);
                }
            }
        }
        $transport = new Varien_Object();
        $transport->setItems($items);
        Mage::dispatchEvent(
            'fooman_advancedpromotions_sorted_items_to_discount',
            array(
                 'mode'      => $mode,
                 'transport' => $transport
            )
        );
        return $transport->getItems();
    }

    public function getItemPrice($item, $base = true)
    {
        return Mage::helper('fooman_advancedpromotions')->getItemPrice($item, $base);
    }

}
