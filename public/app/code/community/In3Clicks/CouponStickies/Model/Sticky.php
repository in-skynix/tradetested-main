<?php
class In3Clicks_CouponStickies_Model_Sticky extends Mage_Core_Model_Abstract
{
    const CACHE_TAG     = 'i3c_coupon_stickies.sticky';
    protected $_cacheTag= 'i3c_coupon_stickies.sticky';

    protected function _construct()
    {
        $this->_init('i3c_coupon_stickies/sticky');
    }

    public function stick()
    {
        Mage::getSingleton('core/session', array('name' => 'frontend'))->setCouponSticky($this);
//		Mage::helper('varnishcache/cache')->setNoCacheHeader();		
//		Mage::helper('varnishcache/cache')->setNoCacheCookie();
        return $this;
    }

    public function isValid()
    {
        return true;
    }

    public static function getStuck()
    {
        if (($sticky = Mage::getSingleton('core/session')->getCouponSticky()) && $sticky->getId() && $sticky->isValid()) return $sticky;
    }

    public function apply()
    {
        if ($this->_getQuote()->getCouponCode()) return;
        $couponCode = $this->getCouponCode();

        try {
            $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
            $this->_getQuote()->setCouponCode(strlen($couponCode) ? $couponCode : '')
                ->collectTotals()
                ->save();

            if ($couponCode) {
                if ($couponCode == $this->_getQuote()->getCouponCode()) {
                    $this->_getSession()->addSuccess('Coupon code "'.Mage::helper('core')->htmlEscape($couponCode).'" was applied.');
                }
                else {
                    $this->_getSession()->addError('Coupon code "'.Mage::helper('core')->htmlEscape($couponCode).'" is not valid.');
                }
            } else {
                $this->_getSession()->addSuccess('Coupon code was canceled.');
            }

        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError('Cannot apply the coupon code.');
            Mage::logException($e);
        }
    }

    /**
     * Retrieve shopping cart model object
     *
     * @return Mage_Checkout_Model_Cart
     */
    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    /**
     * Get checkout session model instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Get current active quote instance
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote()
    {
        return $this->_getCart()->getQuote();
    }
}