<?php
class In3Clicks_CouponStickies_Model_Resource_Sticky_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('i3c_coupon_stickies/sticky');
    }
}
