<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dane
 * Date: 20/06/12
 * Time: 4:12 PM
 * To change this template use File | Settings | File Templates.
 */
class In3Clicks_CouponStickies_Model_Observer
{
    public function checkUrl($observer)
    {
        $event = $observer->getEvent();
        $front = $event->getFront();
        if (($code = $front->getRequest()->getParam('sticky')) && is_string($code)) {
			try {
	            if ($sticky = Mage::getModel('i3c_coupon_stickies/sticky')->load($code)) $sticky->stick();
			} catch (Exception $e) {}
        }
    }

    public function applyStuckCoupon($observer)
    {
        if ($sticky = Mage::getSingleton('i3c_coupon_stickies/sticky')->getStuck()) $sticky->apply();
    }
}