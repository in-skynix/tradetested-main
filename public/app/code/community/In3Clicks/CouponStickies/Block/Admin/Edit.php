<?php
class In3Clicks_CouponStickies_Block_Admin_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'sticky_id';
        $this->_controller = 'couponStickies';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('cms')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('cms')->__('Delete'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('block_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'block_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'block_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * Get edit form container header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('i3c_coupon_stickies.sticky')->getId()) {
            return Mage::helper('cms')->__("Edit sticky");
        }
        else {
            return Mage::helper('cms')->__('New sticky');
        }
    }

    protected function _prepareLayout()
    {
        if ($this->_blockGroup && $this->_controller && $this->_mode) {
            $this->setChild('form', $this->getLayout()->createBlock('i3c_coupon_stickies/admin_edit_form'));
        }
        return parent::_prepareLayout();
    }

}
