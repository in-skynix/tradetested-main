<?php
class In3Clicks_CouponStickies_Block_Admin_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('i3cCouponStickyGrid');
        $this->setDefaultSort('sticky_code');
        $this->setDefaultDir('ASC');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('i3c_coupon_stickies/sticky')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $baseUrl = $this->getUrl();

        $this->addColumn('sticky_code', array(
            'header'    => Mage::helper('cms')->__('Sticky Code'),
            'align'     => 'left',
            'index'     => 'sticky_code'
        ));

        $this->addColumn('coupon_code', array(
            'header'    => Mage::helper('cms')->__('Coupon Code'),
            'align'     => 'left',
            'index'     => 'coupon_code',
        ));

        $this->addColumn('sticky_content', array(
            'header'    => Mage::helper('cms')->__('Content'),
            'align'     => 'left',
            'index'     => 'sticky_content',
        ));
        return parent::_prepareColumns();
    }
    /**
     * Row click url
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('sticky_id' => $row->getId()));
    }

}
