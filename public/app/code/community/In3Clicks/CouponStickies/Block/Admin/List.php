<?php
class In3Clicks_CouponStickies_Block_Admin_List extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'admin';
        $this->_headerText = Mage::helper('cms')->__('CouponStickies');
        $this->_addButtonLabel = Mage::helper('cms')->__('Add New Sticky');
        parent::__construct();
    }

    protected function _prepareLayout()
    {
        $this->setChild( 'grid',
            $this->getLayout()->createBlock('i3c_coupon_stickies/admin_grid',
                $this->_controller . '.grid')->setSaveParametersInSession(true) );
        foreach ($this->_buttons as $level => $buttons) {
            foreach ($buttons as $id => $data) {
                $childId = $this->_prepareButtonBlockId($id);
                $this->_addButtonChildBlock($childId);
            }
        }
        return $this;
    }
}
