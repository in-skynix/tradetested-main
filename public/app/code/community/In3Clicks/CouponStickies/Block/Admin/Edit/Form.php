<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Adminhtml cms page edit form block
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class In3Clicks_CouponStickies_Block_Admin_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $model = Mage::registry('i3c_coupon_stickies.sticky');
        $form = new Varien_Data_Form(array('id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post'));
        $form->setHtmlIdPrefix('sticky_');
        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('cms')->__('General Information'), 'class' => 'fieldset-wide'));
        if ($model->getId()) {
            $fieldset->addField('sticky_id', 'hidden', array(
                'name' => 'sticky_id',
            ));
        }
        $fieldset->addField('sticky_code', 'text', array(
            'name'      => 'sticky_code',
            'label'     => Mage::helper('cms')->__('Sticky Code'),
            'title'     => Mage::helper('cms')->__('Sticky Code'),
            'required'  => true,
        ));
        $fieldset->addField('coupon_code', 'text', array(
            'name'      => 'coupon_code',
            'label'     => Mage::helper('cms')->__('Coupon Code'),
            'title'     => Mage::helper('cms')->__('Coupon Code'),
            'required'  => true,
        ));
        $fieldset->addField('sticky_content', 'textarea', array(
            'name'      => 'sticky_content',
            'label'     => Mage::helper('cms')->__('Content'),
            'title'     => Mage::helper('cms')->__('Content'),
            'required'  => true,
        ));
        $fieldset->addField('sticky_class', 'text', array(
            'name'      => 'sticky_class',
            'label'     => Mage::helper('cms')->__('Sticky Class'),
            'title'     => Mage::helper('cms')->__('Sticky Class'),
            'required'  => false,
        ));
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }

}
