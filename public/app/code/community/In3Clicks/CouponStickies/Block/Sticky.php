<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ?
 * Date: 26/06/12
 * Time: 1:15 PM
 * To change this template use File | Settings | File Templates.
 */
class In3Clicks_CouponStickies_Block_Sticky extends Mage_Core_Block_Template
{
    public function getSticky()
    {
        return Mage::getSingleton('i3c_coupon_stickies/sticky')->getStuck();
    }
}