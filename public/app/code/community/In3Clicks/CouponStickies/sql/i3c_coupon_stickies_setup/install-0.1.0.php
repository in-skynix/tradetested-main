<?php
/**
 * Avid Online Enterprises Limited.
 * User: Dane
 * Date: 17/12/11
 * Time: 10:31 PM
 * Copyright (c) 2011 Avid Online Enterprises Limited. All Rights Reserved.
 */
$installer = $this;
$installer->startSetup();
$installer->run("
CREATE TABLE `i3c_coupon_stickies` (
	`sticky_id` int AUTO_INCREMENT,
	`sticky_code` varchar(255),
	`coupon_code` varchar(255),
	`sticky_content` text,
	`sticky_class` varchar(255),
	PRIMARY KEY (`sticky_id`)
);
");
$installer->endSetup();