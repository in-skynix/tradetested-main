<?php
class In3Clicks_CouponStickies_Adminhtml_CouponStickiesController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Init actions
     *
     * @return Mage_Adminhtml_Cms_BlockController
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
            ->_setActiveMenu('stickies/index')
            ->_addBreadcrumb(Mage::helper('cms')->__('Promotions'), Mage::helper('cms')->__('Promotions'))
            ->_addBreadcrumb(Mage::helper('cms')->__('I3C CouponStickies'), Mage::helper('cms')->__('I3C CouponStickies'))
        ;
        return $this;
    }

    public function indexAction() {
        if ($this->getRequest()->isPost()) {

        }
        $this->loadLayout();
        $this->renderLayout();
    }
    /**
     * Grid for AJAX request
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    /**
     * Create new CMS block
     */
    public function newAction()
    {
        return $this->editAction();
        // the same form is used to create and edit
        $this->_forward('*/*/edit');
    }

    /**
     * Edit CMS block
     */
    public function editAction()
    {
        $this->_title($this->__('CMS'))->_title($this->__('Stickies'));

        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('sticky_id');
        $model = Mage::getModel('i3c_coupon_stickies/sticky');

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (! $model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cms')->__('This item no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }

        $this->_title($model->getId() ? $model->getTitle() : $this->__('New Sticky'));

        // 3. Set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) {
            $model->setData($data);
        }

        // 4. Register model to use later in blocks
        Mage::register('i3c_coupon_stickies.sticky', $model);

        // 5. Build edit form
        $this->_initAction()
            ->_addBreadcrumb($id ? Mage::helper('cms')->__('Edit Sticky') : Mage::helper('cms')->__('New Sticky'), $id ? Mage::helper('cms')->__('Edit Sticky') : Mage::helper('cms')->__('New Sticky'))
            ->renderLayout();
    }

    /**
     * Save action
     */
    public function saveAction()
    {
        // check if data sent
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('sticky_id');
            $model = Mage::getModel('i3c_coupon_stickies/sticky')->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cms')->__('This item no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
            $model->setData($data);
            // try to save it
            try {
                // save the data
                $model->save();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('cms')->__('The sticky has been saved.'));
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('sticky_id' => $model->getId()));
                    return;
                }
                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // save data in session
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                // redirect to edit form
                $this->_redirect('*/*/edit', array('sticky_id' => $this->getRequest()->getParam('sticky_id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        // check if we know what should be deleted
        if ($id = $this->getRequest()->getParam('sticky_id')) {
            $title = "";
            try {
                // init model and delete
                $model = Mage::getModel('i3c_coupon_stickies/sticky');
                $model->load($id);
                $title = $model->getTitle();
                $model->delete();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('cms')->__('The item has been deleted.'));
                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // go back to edit form
                $this->_redirect('*/*/edit', array('sticky_id' => $id));
                return;
            }
        }
        // display error message
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cms')->__('Unable to find a sticky to delete.'));
        // go to grid
        $this->_redirect('*/*/');
    }
}