<?php
namespace TradeTestedComposer;
use Composer\Installer\PackageEvent;
use Composer\Util\ProcessExecutor;
use Composer\Util\FileSystem;
use Composer\Util\Git;
class Scripts
{

    public static function postPackageInstall(PackageEvent $event)
    {
        $operation = $event->getOperation();
        /** @var Composer\Package $package */
        $package = ($operation->getJobType() == 'install') ? $operation->getPackage() : $operation->getTargetPackage();
        if (($package->getInstallationSource() == 'source') && ($package->getSourceType() == 'git')) {
            $path = $event->getComposer()->getInstallationManager()
                ->getInstaller($package->getType())
                ->getInstallPath($package);
            $io = $event->getIO();
            $process = new  ProcessExecutor($io);
            $output = '';
            $process->execute('git submodule update --init', $output, $path);
        }
    }
}