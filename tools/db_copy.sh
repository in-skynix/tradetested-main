#!/bin/bash
read -p "Enter JWT Private Key Password: " -s JWT_PRIVATE_KEY_PASSWORD

#n98-magerun db:dump --strip="m2epro_config @development @stripped @sales @search"

n98-magerun db:import --compression="gzip" "$1"
n98-magerun config:delete --all admin/url/custom
n98-magerun config:delete --all admin/url/use_custom
n98-magerun config:delete --all admin/url/use_custom_path
n98-magerun config:delete --all admin/url/custom_path
n98-magerun config:delete --all web/cookie/cookie_domain
n98-magerun config:delete --all payment/magebasedpspxpost/postusername
n98-magerun config:delete --all payment/magebasedpspxpost/postpassword
n98-magerun config:delete --all payment/magebasedpspxpay/pxpayuserid
n98-magerun config:delete --all payment/magebasedpspxpay/pxpaykey
n98-magerun config:delete --all payment/tradetested_px_post/postusername
n98-magerun config:delete --all payment/tradetested_px_post/postpassword
n98-magerun config:set payment/magebasedpspxpost/use_maxmind_fraud_detection 0
n98-magerun config:delete --all system/sessettings/aws_access_key
n98-magerun config:delete --all system/sessettings/aws_private_key

n98-magerun config:delete --all web/unsecure/base_url
n98-magerun config:delete --all web/secure/base_url
n98-magerun config:delete --all web/unsecure/base_skin_url
n98-magerun config:delete --all web/secure/base_skin_url
n98-magerun config:delete --all web/unsecure/base_media_url
n98-magerun config:delete --all web/secure/base_media_url

n98-magerun config:set web/unsecure/base_url "https://mage.${NZ_DOMAIN:=tradetested.dev}/"
n98-magerun config:set web/secure/base_url "https://mage.${NZ_DOMAIN:=tradetested.dev}/"
n98-magerun config:set web/unsecure/base_link_url "https://mage.${NZ_DOMAIN:=tradetested.dev}/"
n98-magerun config:set web/secure/base_link_url "https://mage.${NZ_DOMAIN:=tradetested.dev}/"
n98-magerun config:set --scope=websites --scope-id=1 web/unsecure/base_url "https://www.${NZ_DOMAIN:=tradetested.dev}/"
n98-magerun config:set --scope=websites --scope-id=1 web/secure/base_url "https://www.${NZ_DOMAIN:=tradetested.dev}/"
n98-magerun config:set --scope=websites --scope-id=1 web/unsecure/base_link_url "https://www.${NZ_DOMAIN:=tradetested.dev}/"
n98-magerun config:set --scope=websites --scope-id=1 web/secure/base_link_url "https://www.${NZ_DOMAIN:=tradetested.dev}/"
n98-magerun config:set --scope=websites --scope-id=2 web/unsecure/base_url "https://www.${AU_DOMAIN:=au.tradetested.dev}/"
n98-magerun config:set --scope=websites --scope-id=2 web/secure/base_url "https://www.${AU_DOMAIN:=au.tradetested.dev}/"
n98-magerun config:set --scope=websites --scope-id=2 web/unsecure/base_link_url "https://www.${AU_DOMAIN:=au.tradetested.dev}/"
n98-magerun config:set --scope=websites --scope-id=2 web/secure/base_link_url "https://www.${AU_DOMAIN:=au.tradetested.dev}/"

n98-magerun config:set web/unsecure/base_skin_url "https://assets.${NZ_DOMAIN:=tradetested.dev}/skin/"
n98-magerun config:set web/secure/base_skin_url "https://assets.${NZ_DOMAIN:=tradetested.dev}/skin/"
n98-magerun config:set web/unsecure/base_js_url "https://assets.${NZ_DOMAIN:=tradetested.dev}/js/"
n98-magerun config:set web/secure/base_js_url "https://assets.${NZ_DOMAIN:=tradetested.dev}/js/"
n98-magerun config:set --scope=websites --scope-id=1 web/unsecure/base_skin_url "https://assets.${NZ_DOMAIN:=tradetested.dev}/assets/"
n98-magerun config:set --scope=websites --scope-id=1 web/secure/base_skin_url "https://assets.${NZ_DOMAIN:=tradetested.dev}/assets/"
n98-magerun config:set --scope=websites --scope-id=2 web/unsecure/base_skin_url "https://assets.${AU_DOMAIN:=au.tradetested.dev}/assets/"
n98-magerun config:set --scope=websites --scope-id=2 web/secure/base_skin_url "https://assets.${AU_DOMAIN:=au.tradetested.dev}/assets/"
n98-magerun config:set web/unsecure/base_media_url "https://media.${NZ_DOMAIN:=tradetested.dev}/"
n98-magerun config:set web/secure/base_media_url "https://media.${NZ_DOMAIN:=tradetested.dev}/"
n98-magerun config:set --scope=websites --scope-id=2 web/unsecure/base_media_url "https://media.${AU_DOMAIN:=au.tradetested.dev}/"
n98-magerun config:set --scope=websites --scope-id=2 web/secure/base_media_url "https://media.${AU_DOMAIN:=au.tradetested.dev}/"

n98-magerun config:set tradetested_api/esb/jwt_private_key_password "${JWT_PRIVATE_KEY_PASSWORD}" --encrypt
n98-magerun config:set tradetested_api/esb/jwt_private_key_location "/etc/ssl/tradetested/api.tradetested.co.nz.key"
n98-magerun config:set tradetested_api/esb/jwt_public_key_location "/etc/ssl/tradetested/api.tradetested.co.nz.crt"
n98-magerun config:set tradetested_api/websockets/base_url "https://ws.${NZ_DOMAIN:=tradetested.dev}"
n98-magerun config:set tradetested_security/rate_limit/redis_host "redis_session"

n98-magerun config:set google/tag_manager/identifier magento_dev
n98-magerun config:set smtppro/general/option smtp
n98-magerun config:set smtppro/general/smtp_authentication crammd5
n98-magerun config:set smtppro/general/smtp_username 212784a683f925243
n98-magerun config:set smtppro/general/smtp_password fee82fddb66b69
n98-magerun config:set smtppro/general/smtp_host mailtrap.io
n98-magerun config:set smtppro/general/smtp_port 2525
n98-magerun config:set smtppro/general/smtp_ssl none
n98-magerun config:set smtppro/queue/usage never
n98-magerun config:set system/smtppro/option smtp
n98-magerun config:set system/smtpsettings/authentication crammd5
n98-magerun config:set system/smtpsettings/username 212784a683f925243
n98-magerun config:set system/smtpsettings/password fee82fddb66b69
n98-magerun config:set system/smtpsettings/host mailtrap.io
n98-magerun config:set system/smtpsettings/port 2525
n98-magerun config:set system/smtpsettings/ssl none
n98-magerun config:set messenger/rabbitmq/host rabbitmq
n98-magerun config:set messenger/rabbitmq/username user
n98-magerun config:set messenger/rabbitmq/pass pass
n98-magerun config:set logger/rsyslog/app_name magento_staging
n98-magerun config:set algoliasearch/credentials/index_prefix magestage_
n98-magerun config:set avid_thumbor/setup/host "https://thumbs.${NZ_DOMAIN:=tradetested.dev}"
n98-magerun config:set --scope=websites --scope-id=2 avid_thumbor/setup/host "https://thumbs.${AU_DOMAIN:=au.tradetested.dev}"
n98-magerun config:set trademe/settings/cron 0
n98-magerun config:set --scope=websites --scope-id=1 trademe/settings/cron 0
n98-magerun config:set --scope=websites --scope-id=2 trademe/settings/cron 0
n98-magerun config:delete --all trademe/account_1422515817_5533/oauth_token
n98-magerun config:delete --all trademe/account_1422515817_5533/oauth_token_secret
n98-magerun config:delete --scope=websites --scope-id=1 trademe/account_1422515817_5533/oauth_token
n98-magerun config:delete --scope=websites --scope-id=1 trademe/account_1422515817_5533/oauth_token_secret
n98-magerun config:delete --scope=websites --scope-id=2 trademe/account_1422515817_5533/oauth_token
n98-magerun config:delete --scope=websites --scope-id=2 trademe/account_1422515817_5533/oauth_token_secret

n98-magerun config:delete --all arkade_s3/general/access_key
n98-magerun config:delete --all arkade_s3/general/secret_key
n98-magerun config:set arkade_s3/general/bucket magento.media.staging.tradetested

n98-magerun cache:clean

php /magento/shell/cache.php --clean
