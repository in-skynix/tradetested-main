FROM alpine:edge
MAINTAINER Dane Lowe <dane.r.lowe@gmail.com>

# Create app user with app group. Specifically assign uid and gid 1000
# to app user and group so that it matches the host user and group id
# on some major host operating systems.
RUN mkdir /app && \
    /usr/sbin/addgroup -g 1000 app && \
    /usr/sbin/adduser -D -H -h /app -u 1000 -G app app && \
    /bin/chown -R app:app /app

# Add dependencies.
RUN apk --no-cache add \
        ca-certificates \
        libuuid \
        apr \
        apr-util \
        libjpeg-turbo \
        icu \
        icu-libs \
        pcre \
        zlib \
        libressl libressl-dev \
        nginx

# Separate the logs into their own volume to keep them out of the container.
VOLUME ["/var/log/nginx"]

# Expose the HTTP and HTTPS ports.
EXPOSE 80 443

# Set nginx directly as the entrypoint.
ENTRYPOINT ["nginx", "-g", "daemon off;"]

#
# Install packages
#
RUN echo "@testing http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
    echo "@community http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories && \
    echo "http://dl-cdn.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories && \
    apk add --update \
    curl \
    supervisor \
    git \
    ca-certificates

RUN apk add --update \
    php7-fpm@community \
    php7-phar@community \
    php7-openssl@community \
    php7-curl@community \
    php7-json@community \
    php7-xml@community \
    php7-mcrypt@community \
    php7-zlib@community \
    php7-ctype@community \
    php7-iconv@community \
    php7-session@community \
    php7-dom@community \
    php7-bcmath@community \
    php7-mbstring@community \
    php7-pcntl@community \
#    php7-xdebug@testing \
    mysql mysql-client php7-pdo_mysql@community \
    && \
    rm -fR /var/cache/apk/*


#
# PHP Configuration
#
ENV PHP_INI=/etc/php7/php.ini
RUN \
    sed 's,;always_populate_raw_post_data,always_populate_raw_post_data,g' -i $PHP_INI && \
    sed 's,memory_limit = 128M,memory_limit = 256M,g' -i $PHP_INI && \
    sed 's,upload_max_filesize = 2M,upload_max_filesize = 20M,g' -i $PHP_INI
RUN ln -s /usr/bin/php7 /usr/bin/php

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer
WORKDIR /
COPY composer.json composer.lock /
ADD ./tools/composer /tools/composer
# Install dependencies with Composer.
# --prefer-source fixes issues with download limits on Github.
# --no-interaction makes sure composer can run fully automated
RUN composer install --prefer-source --no-interaction
RUN mkdir -p /magento/app/etc/
RUN ln -s /etc/magento/local.xml /magento/app/etc/local.xml

ADD ./tools/docker/etc/nginx/htpasswd /etc/nginx/htpasswd
ADD ./tools/docker/etc/nginx/includes /etc/nginx/includes
ADD ./tools/docker/etc/nginx/sites /etc/nginx/conf.d
ADD ./tools/docker/etc/magento /etc/magento
ADD ./tools/docker/etc/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
ADD ./tools/n98-magerun-latest.phar /usr/bin/n98-magerun
#ADD ./tools/docker/etc/php-fpm.ini /private/etc/php-fpm.conf
ADD ./public /magento
ADD ./assets/public /assets

#http://txt.fliglio.com/2013/11/creating-a-mysql-docker-container/
RUN sed -i -e"s/^bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0/" /etc/mysql/my.cnf

RUN curl -sLo /usr/local/bin/ep https://github.com/kreuzwerker/envplate/releases/download/v0.0.8/ep-linux && chmod +x /usr/local/bin/ep
RUN mkdir -p /run/nginx
ENTRYPOINT [ "/usr/local/bin/ep", "-v", "/etc/nginx/conf.d/*.conf", "/etc/magento/*.xml", "--", "/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf" ]
