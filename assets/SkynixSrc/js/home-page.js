/**
 * Created by anastasia on 9/21/16.
 */

var tradeTestedHome = (function(){
    return {
        init: function() {

            /////Mobile menu start//////////////////////////////////
            var mobileNavOpen = jQuery('.show-mobile-menu'),
                showCart = jQuery('.header-minicart a'),
                navTopCategory = jQuery('.main-navigation ol>li.level0'),
                subNav = navTopCategory.find('>ul.level0'),
                mainContainer = jQuery('.main-container'),
                navTopCategoryLevel0 = navTopCategory.find('>a.level0'),
                windowWidth = jQuery(window).width(),
                navSubCategory = navTopCategory.find('>ul.level0>li.level1>a'),
                closeButton = jQuery('a.close'),
                signReg = jQuery('.sign-reg-window'),
                userModal = jQuery('.cd-user-modal'),
                userModalSignIn = userModal.find('.login'),
                userModalRegistr = userModal.find('.registration'),
                tab1 = userModal.find('.tab1'),
                tab2 = userModal.find('.tab2'),
                tabs = jQuery('.cd-switcher > li'),
                pageForMask = jQuery('.page'),
                closeModal = jQuery('.close-modal');


            navTopCategoryLevel0.off();
            navSubCategory.off();

            mobileNavOpen.click(function(){
                jQuery('.main-navigation').slideToggle();
                jQuery(this).toggleClass('active-menu');
            });

            ///mobile accordion/////////////////////////////////

            navTopCategoryLevel0.click(function(ev){
                ev.preventDefault();
                var thisClickEl = jQuery(this),
                    thisUl = thisClickEl.next('ul');


                var subNavLi = thisUl.find('li.level1:not(.view-all)');
                subNavLi.removeAttr('style');

                if(thisClickEl.hasClass('opened-submenu')){
                    thisClickEl.removeClass('opened-submenu');
                    thisUl.slideUp();
                } else{
                    navTopCategoryLevel0.each(function(){
                        var thisEl = jQuery(this);
                        if(thisEl.hasClass('opened-submenu')){
                            thisEl.removeClass('opened-submenu');
                            thisEl.next('ul').slideUp();
                        }
                    });
                    thisClickEl.addClass('opened-submenu');
                    thisUl.slideDown('medium', function(){
                        var selectedUl = jQuery(this);
                        if (selectedUl.is(':visible'))
                            selectedUl.css('display','inline-block');
                        if(windowWidth > 377){
                            var subNavLi = selectedUl.find('li.level1:not(.view-all)');
                            subNavLi.attr('style', 'height:'+ selectedUl.height()+ 'px!important');
                        }
                    });

                }

            });

            ////Show search block, if clicked search-icon

            var smSearchIcon = jQuery('.show-search-sm');
            smSearchIcon.click(function(ev){
                ev.preventDefault();
                jQuery(this).parent('li').toggleClass('openedSearch');
                var searchBlock = jQuery('.search-block');
                searchBlock.find('input').val('');
                searchBlock.slideToggle('medium', function(){
                    var thisBlock = jQuery(this);
                    thisBlock.find('input').focus();
                    if (thisBlock.is(':visible'))
                        thisBlock.css('display','inline-block');
                });
            });

            /////Mobile menu end//////////////////////////////////

            ////Desktop menu start///////////////////////////////

            //Set height for li element in navigation subcategories
            navTopCategory.each(function(){
                var thisLi = jQuery(this);
                thisLi.on('mouseover',function(){
                    mainContainer.addClass('navMask');
                });
                thisLi.on('mouseleave',function(){
                    mainContainer.removeClass('navMask');
                });
            });

            if(windowWidth > 769){
                subNav.each(function(){
                    var thisSubNav = jQuery(this);
                    var subNavHeight = thisSubNav.height() - 70;
                    var subNavLi = thisSubNav.find('li.level1:not(.view-all)');
                    subNavLi.css('height',subNavHeight);

                });
            }

            ////Desktop menu end///////////////////////////////

            ////Show and close modal window - cart///////////
            showCart.click(function(){
                jQuery('.page').toggleClass('mask');
                jQuery('.block-cart').slideToggle();
            });

            closeButton.click(function(ev){
                ev.preventDefault();
            });
            ////////////////////////////////////////////////

            function deleteCarouselClasses(){
                var widgetProducts = jQuery('.widget-new-products');
                widgetProducts.find('.product-carousel').removeClass('product-carousel');
                widgetProducts.find('.viewport').removeClass('viewport');
                widgetProducts.find('.overview').removeClass('overview');
            }

            if(windowWidth < 378){
                deleteCarouselClasses();
            }

            // jQuery('.btn.btn-cart').click(function(ev){
            // });

            /////Modal window for sign in/registration start///////

            // signReg.click(function(ev){
            //     ev.preventDefault();
            //     var clicked = jQuery(this);
            //     tabs.removeClass('active');
            //     userModal.find('.visible').removeClass('visible');
            //
            //     if(clicked.attr("data-target-element") == '#header-account'){
            //         tab1.addClass('active');
            //         userModalSignIn.addClass('visible');
            //     }
            //     else if(clicked.attr("data-target-element") == '#header-registration'){
            //         tab2.addClass('active');
            //         userModalRegistr.addClass('visible');
            //     }
            //     closeModal.css("display", "block");
            //     userModal.slideToggle();
            //
            // });
            //
            // closeModal.click(function(){
            //     userModal.slideToggle();
            //     closeModal.css("display", "none");
            // });
            //
            // tabs.click(function(ev){
            //     ev.preventDefault();
            //     tabs.removeClass('active');
            //     userModal.find('.visible').removeClass('visible');
            //     var clicked = jQuery(this);
            //     clicked.addClass('active');
            //     if(clicked.hasClass('tab1')){
            //         userModalSignIn.addClass('visible');
            //     }
            //     else if(clicked.hasClass('tab2')){
            //         userModalRegistr.addClass('visible');
            //     }
            // })

            /////Modal window for sign in/registration end///////
        }
    }
})();

jQuery(function($) {
    tradeTestedHome.init();
    if(jQuery(window).width() > 377){
        jQuery('.widget-products').tinycarousel();
    }
    jQuery(".review-block").tinycarousel();
    console.log('connect ....js');
});