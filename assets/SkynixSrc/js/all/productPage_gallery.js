if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}

(function($, window) {

    /*
     * Exported Constructors
     */

    var ImageGallery = function(el, imageData, config) {
        this.vm = new ViewModel(imageData, config);
        this.vm.element = el;
        this.vm.thumbnails = new ThumbnailsComponent(this.vm);
        this.vm.zoomComponent = new ZoomComponent(this.vm);
        this.vm.actions = {select: this.vm.zoom.bind(this.vm), zoom: this.vm.zoom.bind(this.vm)};
        this.vm.zoomOnly = true;
        m.mount(el, new GalleryComponent(this.vm));
    };

    var MediaGallery = function(config) {
        this.vm = new ViewModel(config.image_data, config.config);
        this.vm.element = document.getElementById('gallery');
        this.vm.thumbnails = new ThumbnailsComponent(this.vm);
        this.vm.zoomComponent = new ZoomComponent(this.vm);
        this.vm.actions = {select: this.vm.zoom.bind(this.vm), zoom: this.vm.zoom.bind(this.vm)};
        this.vm.zoomOnly = true;
        m.mount(this.vm.element, new ProductGalleryComponent(this.vm));
    };

    /*
     * View Model
     */

    var ViewModel = function() {this.initialize.apply(this, arguments);};
    ViewModel.prototype = {
        initialize: function(imageData, config) {
            this.list = ImageList.from(imageData);
            this.zoomEnabled = m.prop(false);
            new KeyControl(this); //only set up for zoom component
            this.left = 0;
            var mobile = document.querySelector('.show-mobile-menu');
            this.mobile = (mobile != null)  ? true : false;
            this.components = this.mobile ? 3 : 5;

        },
        selectedImage: function(value) {
            return (arguments.length) ? this.list.get(value) : this.list.get();
        },
        defaultViewArgs: function() {
            return {perPage: this.components, excludeSelected: true}
        },
        zoom: function(imageId) {
            this.list.getById(imageId);
            this.zoomEnabled(true);
        },
        closeZoom: function() {
            this.zoomEnabled(false);
        },
        getData: function() {
            this.box = document.getElementById('product-img-box');
            this.overview =  this.box.querySelector('.overview > ul');
            this.item = this.overview.querySelector('.gallery-item');
            this.allItems = this.overview.querySelectorAll('.gallery-item');
            this.mainImage = this.box.querySelector('.product-image-zoom');


            if(this.mobile){
                this.width = this.item.clientWidth;
                this.overviewWidth = (this.width)*this.allItems.length;
                this.maxLeft = this.overviewWidth - (this.components*(this.width));
                this.itemWidth = this.width;
                this.overview.style.width = this.overviewWidth+'px';
            } else{
                this.width = this.item.clientWidth;
                this.overviewWidth = (this.width)*this.allItems.length;
                this.maxLeft = this.overviewWidth - (this.components*(this.width));
                this.itemWidth = this.width;
                this.overview.style.width = this.overviewWidth+'px';
            }
        },
        next: function() {
            if(this.left != -this.maxLeft){
                this.left-=this.itemWidth;
            }
            Object.assign(this.overview.style, {left: this.left+'px'});
        },
        prev: function(){
            if(this.left != 0){
                this.left+=this.itemWidth;
            }
            Object.assign(this.overview.style, {left: this.left+'px'});
        },
        setAttributes: function(el, options){
            Object.keys(options).forEach(function(attr) {
                el.setAttribute(attr, options[attr]);
            })
        }
    };

    /*
     * Views
     */
    var Pager = function() {this.initialize.apply(this, arguments);};
    Pager.prototype = {
        initialize: function(items, perPage, startIndex) {
            this.items = items;
            this.perPage = perPage || this.items.length;
            this.startIndex = startIndex || 0;
        },
        isFirstPage: function() {
            return (this.startIndex == 0)
        },
        isLastPage: function() {
            return (this.startIndex >= (this.items.length - this.perPage ));
        },
        currentItems: function() {
            return this.items.slice(this.startIndex, this.startIndex+this.perPage);
        },
        prev: function() {
            this.startIndex -= this.perPage;
            if (this.startIndex < 0) {
                this.startIndex = 0;
            }
        },
        next: function() {
            this.startIndex += this.perPage;
            var maxStartIndex = (this.items.length - this.perPage);
            if (this.startIndex > maxStartIndex) {
                this.startIndex = maxStartIndex;
            }
        }
    };

    var ThumbnailsComponent = function(vm) {
        this.vm = vm;
        this.view = function(ctrl, args) {
            var vm = this.vm;
            var selectedImage = args.selectedImage || vm.selectedImage();
            if (!this.pager) {
                var list = args.excludeSelected ?
                    vm.list.filter(function(image){return image.data.id != selectedImage.data.id}) : vm.list;
                this.pager = new Pager(list, args.perPage, 0);
            }
            if (vm.list.length) {
                var items = this.pager.currentItems().map(function(image) {
                    return m('li.gallery-item', {className: (image.data.id == selectedImage.data.id) ? 'selected' : ''},m('a',{
                        onclick: function(el) {
                            var zoomImg = document.querySelector('.main_image');
                            vm.setAttributes(zoomImg,
                                {
                                    'src': image.loader(vm.element, 'zoom').src,
                                    'alt': image.data.label,
                                    'key': image.data.id
                                })

                    }},[
                        m('img', {
                            height: 68,
                            width: 68,
                            key: image.data.id,
                            src: image.loader(vm.element, 'thumbnail').src,
                            alt: image.data.label
                        })]
                    ))
                });
                return m('div.more-views', [
                    m('div.overview-wrap',[
                        m('div.overview',[
                            m('ul', items)
                        ]),
                        (vm.left!=0 ? m('div.img-button.first',[
                            m('a.prev-img',{'href':'#', onclick: function(ev){
                                ev.preventDefault();
                                vm.prev();
                            }})
                        ]) : null),
                        (vm.list.length > vm.components  ? m('div.img-button',[
                            m('a.next-img',{className:(vm.left!=-vm.maxLeft ? '' : 'hide'),'href':'#', onclick: function(ev){
                                ev.preventDefault();
                                vm.next();
                            }, config: function(){
                                vm.getData();
                            }
                            })
                        ]): null),

                    ])

                ]);
            } else {
                return m('span');
            }
        };
    };

    var ZoomComponent = function(vm){
        this.vm = vm;
        this.thumbnails = new ThumbnailsComponent(vm);

        this.view = function() {
            var vm = this.vm;
            if (!vm.zoomEnabled()) {
                return m('span');
            }
            var selectedImage = vm.selectedImage();
            if (!selectedImage.loader(vm.element, 'zoom').complete) {
                return {subtree: "retain"};
            }
            return m('');
        };
        this.nav = function() {
            if (this.vm.list.length > 1) {
                return m('div.gallery_nav',
                    m('div.icon.prev', {
                        onclick: function(){vm.list.getDelta(-1)}
                    }),
                    m('div.icon.next', {
                        onclick: function(){vm.list.getDelta(1)}
                    })
                );
            } else {
                return m('div.gallery_nav');
            }
        }
    };

    var GalleryComponent = function(vm) {
        this.vm = vm;
        this.thumbnails = new ThumbnailsComponent(vm);
        this.view = function() {
            return m('div', [
                m.component(this.thumbnails),
                m.component(vm.zoomComponent)
            ]);
        }
    };

    var ProductGalleryComponent = function(vm) {
        this.vm = vm;
        this.thumbnails = new ThumbnailsComponent(vm);
        this.view = function() {
            var selectedImage = vm.zoomOnly ? vm.list[0] : this.vm.selectedImage();
            var large_image = m('img.main_image', {
                src: selectedImage.loader(vm.element, vm.mobile ? 'large' :'zoom').src,
                alt: selectedImage.data.label,
                config: function(element, isInitialized) {
                    $(document.body).toggleClass('zoom', false);
                }
            });
            return m('div.product-img-box#product-img-box', [
                m('div.product-img-wrap',[
                    m('p.product-image.product-image-zoom ', large_image),
                    (vm.list.length > 1 ? m.component(this.thumbnails, { selectedImage: selectedImage}) : null),
                    m.component(vm.zoomComponent)
                ])

            ]);
        }
    };

    /*
     * Helpers and Data
     */

    var KeyControl = function() {this.initialize.apply(this, arguments);};
    KeyControl.prototype = {
        initialize: function (vm) {
            this.vm = vm;
            $(document).keydown(this.handleKeyDown.bind(this));
            $(document).keyup(this.handleKeyUp.bind(this));
        },
        handleKeyUp: function(e) {
            if (this.vm.zoomEnabled() && (e.keyCode == 27)) {
                this.vm.closeZoom();
                m.redraw();
            }
        },
        handleKeyDown: function(e) {
            if (this.vm.zoomEnabled() && (e.keyCode == 37)) {
                this.vm.list.getDelta(-1);
                m.redraw();
            } else if (this.vm.zoomEnabled() && (e.keyCode == 39)) {
                this.vm.list.getDelta(1);
                m.redraw();
            }
        }

    };

    var GalleryImage = function() {this.initialize.apply(this, arguments);};
    GalleryImage.prototype = {
        initialize: function(data) {
            this.data = data;
            this.preloaders = [];
        },
        srcKey: function() {
            if (window.devicePixelRatio >= 2) {
                return 'src_retina';
            } else {
                return 'src';
            }
        },
        loader: function(el, size) {
            var src =  this.data[this.srcKey()][size];
            var preloader;
            if (!el.spinner) {
                el.spinner = new Spinner({
                    lines: 15,
                    length: 6,
                    width: 5,
                    radius: 20,
                    color: '#ccc'
                });
            }
            if (!(preloader = this.preloaders[size])) {
                el.spinner.spin(el);
                preloader = this.preloaders[size] = new Image();
                preloader.onload = function() {
                    el.spinner.stop();
                    m.redraw();
                };
                preloader.src = src;
            }
            return preloader;
        }
    };

    var LoopList = function(){};
    LoopList.prototype = new Array;
    LoopList.prototype.get = function(i) {
        if (arguments.length) {
            this.activeIndex = i;
        } else if (!this.activeIndex) {
            this.activeIndex = 0;
        }
        return this[this.activeIndex];
    };
    LoopList.prototype.getById = function(id) {
        for (var x = 0, l = this.length; x < l; x++) {
            if (this[x].data.id == id) {
                return this.get(x);
            }
        }
        return this.get[0];
    };
    LoopList.prototype.getDelta = function(delta) {
        var i = this.activeIndex ? this.activeIndex : 0;
        if (arguments.length) {
            i += delta;
            if (!((i) in this)) { i = (delta) < 0 ? this.length-1 : 0 }
        }
        return this.get(i);
    };

    var ImageList = function(){};
    ImageList.prototype = new LoopList;
    ImageList.from = function(arr) {
        var list = new ImageList();
        for (var x = 0, l = arr.length; x < l; x++) {
            list.push(new GalleryImage(arr[x]));
        }
        return list;
    };
    ImageList.prototype.isSelected = function(image, selectedIndex) {
        if (selectedIndex == null) {
            selectedIndex = this.activeIndex;
        }
        return this[selectedIndex] == image;
    };

    window.TradeTested.ImageGallery = ImageGallery;
    window.TradeTested.MediaGallery = MediaGallery;
})(window.jQuery || window.ender || window.Zepto, window);