if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}

(function(window){
    var MithrilSign = {
        Controller: function(config) {
            this.vm = new MithrilSign.ViewModel(config);
        },
        ViewModel: function(config){
            this.activeTab = m.prop(false);
            this.config = config;
        },
        View: function(ctrl) {
            return m('div.header-sign', [
                m('a.sign-reg-window.modal-link', {onclick: m.withAttr("data-index", ctrl.vm.activeTab), "data-index": "sign_in"}, [
                        m('span', 'Sign in')
                    ]),
                m(SignComponent, {vm: ctrl.vm})
            ])
        }
    };

    var Tabs = {
        sign_in: {
            name: 'Sign in',
            view: function(ctrl, args) {
                return m('div.login', {
                    config: function(el,isInit, ctx){
                        var h = el.clientHeight;
                        el.classList.add('selected-tab')
                    }
                },[
                    m('form#login-form',{'method':'post', 'action': args.urls.action},[
                        m('div.fieldset', [
                            m('ul.form-list',[
                                m('li',[
                                    m('label.required',{'for':'email'},'Email',[
                                        m('em','*')
                                    ]),
                                    m('div.input-box',[
                                        m('input#email.input-text.required-entry.validate-email', {'type':'login', 'name':'login[username]'})
                                    ])

                                ]),
                                m('li',[
                                    m('label.required',{'for':'pass'},'Password',[
                                        m('em','*')
                                    ]),
                                    m('div.input-box',[
                                        m('input#pass.input-text.required-entry.validate-password', {'type':'password', 'name':'login[password]'})
                                    ])

                                ])

                            ]),
                            m('a.f-left', {'href': args.urls.forgot_password},'Forgotten password'),
                            m('div.submit-button',[
                                m('button.button#send2',{'type':'submit', 'name':'send', 'title':'Login'}, [
                                    m('span',[
                                        m('span','Sign in')
                                    ])
                                ])
                            ])
                        ])
                    ])
                ])
            }
        },
        create: {
            name: 'Create Account',
            view: function(ctrl, args) {
                return  m('div.registration', {
                    config: function(el,isInit, ctx){
                        var h = el.clientHeight;
                        el.classList.add('selected-tab')
                    }
                },[
                    m('form#form-validate', {'method':'post', 'action': args.urls.action}, [
                        m('div.fieldset', [
                            m('ul.form-list', [
                                m('li',[
                                    m('label.required',{'for':'email_address'},'Email Address',[
                                        m('em','*')
                                    ]),
                                    m('div.input-box',[
                                        m('input#email_address.input-text.required-entry.validate-email', {'type':'login', 'name':'login[username]'})
                                    ])
                                    ])

                                ]),
                            ]),
                            m('div.fieldset', [
                                m('ul.form-list', [
                                    m('li.fields', [
                                        m('label.required',{'for':'user_name'},'Full Name',[
                                            m('em','*')
                                        ]),
                                        m('div.input-box',[
                                            m('input#user_name.input-text.required-entry', {'type':'login', 'name':'login[username]'})
                                        ])

                                    ])
                                ])
                            ]),
                            m('div.fieldset', [
                                m('ul.form-list', [
                                    m('li.fields', [
                                        m('label.required',{'for':'password'},'Create Password',[
                                            m('em','*')
                                        ]),
                                        m('span.about-pass', 'Your password must be at least 6 characters'),
                                        m('div.input-box',[
                                            m('input#password.input-text.required-entry.validate-password', {'type':'password', 'name':'login[username]'})
                                        ])

                                    ])
                                ])
                            ]),
                            m('div.buttons-set', [
                                m('button.button', {'type':'submit', 'title': 'submit'}, [
                                    m('span', [
                                        m('span', 'Submit')
                                    ])
                                ])
                            ]),
                            m('p', 'By creating an account, you agree to Trade Tested\'s ', [
                                m('a', {'href':'#'}, 'Terms & Conditions'),
                                'and',
                                m('a', {'href': '#'}, 'Privacy Policy')
                            ])
                        ])
                    ])
            }
        }
    };

    var SignComponent = {
        view: function(ctrl, args){
            var vm = args.vm;
            var active = vm.activeTab();
            if (!active){
                return m('');
            }

            return m('div.overlay', {onclick: m.withValue(false, vm.activeTab)}, [
                m('div#header-sign',
                    {
                        onclick: function(ev) {
                            ev.stopPropagation();
                        }
                    },
                    m('div.cd-user-modal', {
                        config: function(el,isInit, ctx){
                            var h = el.clientHeight;
                            el.classList.add('open-modal')
                        }
                    }, [
                        m('div.cd-user-modal-container',[
                            m('div.mobile-close',  {onclick: m.withValue(false, vm.activeTab)}),
                            m('ul.cd-switcher', Object.keys(Tabs).map(function(tab){
                                var props = (active === tab) ? {className: 'active'} : {};
                                return m('li', props,
                                    m('a', { onclick: m.withAttr("data-index", vm.activeTab), "data-index":tab}, Tabs[tab].name)
                                )
                            })),
                            m('div.modal-content', m(Tabs[active], vm.config)
                            )
                        ])

                    ])

                )
            ] )
        }
    };

    window.TradeTested.SignRegModal = function(options){
        var controller = m.mount(
            document.getElementById('header-sign-link'),
            m.component({controller: MithrilSign.Controller, view: MithrilSign.View}, options)
        );
        document.getElementById('header-reg-link').getElementsByTagName('a')[0].addEventListener(
            'click',
            m.withValue('create', controller.vm.activeTab, true)
        );
    }
})(window);