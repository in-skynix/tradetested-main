if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}

(function(window) {
    var MithrilCPP = {
        Controller: function (config) {
            this.vm = new MithrilCPP.ViewModel(config);
        },
        ViewModel: function (config) {
            this.config = config;
             this.initialize.apply(this, arguments);
        },
        View: function(ctrl){
            return m('div.list-block',[
                m(CategoryPageProductListComponent, {vm: ctrl.vm}),
                ctrl.vm.config.data.product.length > ctrl.vm.end ?
                    m('a.view-more-button',{'href':'#', onclick: function(ev){
                        ev.preventDefault();
                        ctrl.vm.end+=16;
                        return CategoryPageProductListComponent;
                    }},'View More',[
                        m('span')
                    ]) : ''

            ])
        }
    };
    MithrilCPP.ViewModel.prototype = {
        initialize: function () {
            this.end = 16;
            this.show = true;
            this.currentMode = document.querySelector('.selected-view').getAttribute('title');
        }
    };

    var CategoryPageProductListComponent = {
        view: function(ctrl, args){
            var vm = args.vm;
            var data = args.vm.config.data;
            var currentMode = vm.currentMode=='List';

            return m(vm.currentMode!='List'?'ul.products-grid':'ol.products-list',[
                Object.keys(data.product).map(function(key){
                    if(data.product[key].is_salable){
                        var component = m('button.button.btn-cart', {'title':"Add to cart",

                            onclick: function(){
                                setLocation(data.product[key].add_to_cart);
                            }}, [
                            m('span', [
                                m('span', 'Add')
                            ])
                        ]);
                        var sale = currentMode? m('p',[
                            component
                        ]): component;

                        var inOutOfStock = m('span.in-stock', 'In stock');
                    }else {
                        var inOutOfStock = m('span.out-of-stock', 'Out of stock');
                    }
                    if(data.product[key].special_price != '0.00'){
                        var price =  m('div.price-box.',[
                            m('p#.old-price', [
                                m('span.price', '$'+data.product[key].product_price+'')
                            ]),
                            m('p.special-price', [
                                m('span.price', '$'+data.product[key].special_price)
                            ])
                        ])
                    } else{
                        var price =
                            m('div.price-box',[
                                m('p.regular-price', [
                                    m('span.price', '$'+data.product[key].product_price+'')])
                            ])
                    }

                    if(key < vm.end) {
                        return m('li.item',{config: function(){
                        }}, [
                            m('a.product-image', {
                                    'href': data.product[key].url,
                                    config: function (el, isInit, ctx) {
                                        if (data.product[key].top_seller) {
                                            el.classList.add('topseller');
                                        }
                                        if(data.product[key].free_ship){
                                            el.classList.add('free-shipping');
                                        }
                                    }
                                }, [
                                    m('img', {
                                        'src': data.product[key].image_url_small,
                                        'width': 200,
                                        'height': 200,
                                        'alt': data.product[key].name
                                    }),
                                ]
                            ),
                            currentMode ? m("div.product-shop", [
                                m('div.p-fix',[
                                    m('h2.product-name', [
                                        m('a', {
                                            'href': data.product[key].url,
                                            'title': data.product[key].name
                                        }, data.product[key].name)
                                    ]),
                                    inOutOfStock,
                                    price,
                                    m('div.actions', [
                                        sale
                                    ])
                                ])

                            ]) : m('div',[
                                m('h2.product-name', [
                                    m('a', {
                                        'href': data.product[key].url,
                                        'title': data.product[key].name
                                    }, data.product[key].name)
                                ]),
                                inOutOfStock,
                                price,
                                m('div.actions', [
                                    sale
                                ])
                            ])

                        ])
                    }
                })

            ])

        }
    }

    window.TradeTested.CategoryPageProducts = function(options) {
        m.mount(
            document.getElementById('products-list'),
            m.component({controller: MithrilCPP.Controller, view: MithrilCPP.View}, options)
        )
    };
})(window);





