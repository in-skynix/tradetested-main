/**
 * Created by anastasia on 9/21/16.
 */
if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}
(function($, window) {
    var Home = function () {this.init.apply(this, arguments);};
    Home.prototype = {
        init: function () {
            /////Mobile menu start//////////////////////////////////
            var mobileNavOpen = $('.show-mobile-menu'),
                navTopCategory = $('.main-navigation ol>li.level0'),
                subNav = navTopCategory.find('>ul.level0'),
                navTopCategoryLevel0 = navTopCategory.find('>a.level0'),
                windowWidth = $(window).width(),
                mainContainer = $('.wrapper'),
                navSubCategory = navTopCategory.find('>ul.level0>li.level1>a');

            // navTopCategoryLevel0.off();
            // navSubCategory.off();
            mobileNavOpen.click(function () {
                $('.main-navigation').slideToggle();
                $(this).toggleClass('active-menu');
                $('.menu-active').removeClass('menu-active');
            });

            ////Desktop menu start///////////////////////////////
            navTopCategory.each(function () {
                var thisLi = $(this);
                thisLi.on('mouseover', function () {
                    mainContainer.addClass('navMask');
                });
                thisLi.on('mouseleave', function () {
                    mainContainer.removeClass('navMask');
                });
            });

            ////Desktop menu end///////////////////////////////

            ////Mobile menu start/////////////////////////////
            if(mobileNavOpen.length != 0){
                navTopCategoryLevel0.click(function(ev){
                    ev.preventDefault();
                    var thisEl = $(this),
                        thisLiParent = thisEl.parent('li');
                    thisLiParent.toggleClass('menu-active');
                })
            }
            ////Mobile menu end//////////////////////////////

            
        }
    }
    window.TradeTested.Home = Home;
})(window.jQuery, window);
