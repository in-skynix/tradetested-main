if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}

(function(window) {
    var RecoForYou = {
        Controller: function (config) {
            this.vm = new RecoForYou.ViewModel(config);
        },
        ViewModel: function (config) {
            this.config = config;
            this.initialize.apply(this, arguments);
        },
        View: function(ctrl){
            return m('div.list-block',[
                m(RecoForYouComponent, {vm: ctrl.vm}),
                ctrl.vm.config.data.product.length > ctrl.vm.end ?
                    m('a.view-more-button',{'href':'#', onclick: function(ev){
                        ev.preventDefault();
                        ctrl.vm.end+=8;
                        return RecoForYouComponent;
                    }},'View More',[
                        m('span')
                    ]) : ''

            ])
        }
    };
    RecoForYou.ViewModel.prototype = {
        initialize: function () {
            this.end = 8;
            this.show = true;
        }
    };

    var RecoForYouComponent = {
        view: function(ctrl, args){
            var vm = args.vm;
            var data = args.vm.config.data;

            return m('ul.products-grid',[
                Object.keys(data.product).map(function(key){
                    if(data.product[key].is_salable){
                        var component = m('button.button.btn-cart', {'title':"Add to cart",

                            onclick: function(){
                                setLocation(data.product[key].add_to_cart);
                            }}, [
                            m('span', [
                                m('span', 'Add')
                            ])
                        ]);
                        var sale = component;

                        var inOutOfStock = m('span.in-stock', 'In stock');
                    }else {
                        var inOutOfStock = m('span.out-of-stock', 'Out of stock');
                    }
                    if(data.product[key].special_price != '0.00'){
                        var price =  m('div.price-box.',[
                            m('p#.old-price', [
                                m('span.price', '$'+data.product[key].product_price+'')
                            ]),
                            m('p.special-price', [
                                m('span.price', '$'+data.product[key].special_price)
                            ])
                        ])
                    } else{
                        var price =
                            m('div.price-box',[
                                m('p.regular-price', [
                                    m('span.price', '$'+data.product[key].product_price+'')])
                            ])
                    }

                    if(key < vm.end) {
                        return m('li.item',{config: function(){
                        }}, [
                            m('a.product-image', {
                                    'href': data.product[key].url,
                                    config: function (el, isInit, ctx) {
                                        if (data.product[key].top_seller) {
                                            el.classList.add('topseller');
                                        }
                                        if(data.product[key].free_ship){
                                            el.classList.add('free-shipping');
                                        }
                                    }
                                }, [
                                    m('img', {
                                        'src': data.product[key].image_url_small,
                                        'width': 200,
                                        'height': 200,
                                        'alt': data.product[key].name
                                    }),
                                ]
                            ),
                             m('div',[
                                m('h2.product-name', [
                                    m('a', {
                                        'href': data.product[key].url,
                                        'title': data.product[key].name
                                    }, data.product[key].name)
                                ]),
                                inOutOfStock,
                                price,
                                m('div.actions', [
                                    sale
                                ])
                            ])

                        ])
                    }
                })

            ])

        }
    }

    window.TradeTested.RecoForYou = function(options) {
        m.mount(
            document.getElementById('products-reco'),
            m.component({controller: RecoForYou.Controller, view: RecoForYou.View}, options)
        )
    };
})(window);





