if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}
(function(window) {
    var MithrilCart = {
        Controller: function(config) {
            this.vm = new MithrilCart.ViewModel(config);
        },
        ViewModel: function () {
            this.initialize.apply(this, arguments);
        },
        View: function(ctrl) {
            var data = ctrl.vm.cartData();
            return m('div.header-minicart', [
                m('a.skip-link.skip-cart.no-count.skip-active', {href: data.cartUrl, onclick: ctrl.vm.open.bind(ctrl.vm)}, [
                    m('span', 'View Cart')
                ]),
                m(CartComponent, {vm: ctrl.vm})
            ]);
        }
    };
    MithrilCart.ViewModel.prototype = {
        initialize: function(cartData) {
            this.isCartOpen = m.prop(false);
            this.cartData = m.prop(cartData||{});
        },
        hide: function() {
            this.isCartOpen(false);
        },
        open: function(e) {
            e.preventDefault();
            this.isCartOpen(true)
        },
        remove: function(id) {
            var items = this.cartData().items;
            for(var i = items.length - 1; i >= 0; i--) {
                if(items[i].id == id) {
                    items[i].className = 'removing';
                }
                this.cartData().items = items;
                m.redraw();
            }
            m.request({
                method: "POST",
                url: '/checkout/cart/ajaxDelete/id/'+id+'/form_key/'+FORM_KEY+'/ajax/true/'
            }).then(this.loadResult.bind(this));
        },
        loadResult: function(data) {
            if (data.success) {
                this.cartData(data.data);
            } else {
                Observable.trigger('messagesUpdated', data.messages||{});
            }
        }
    };

    var CartComponent = {
        view: function(ctrl, args) {
            var vm = args.vm;
            if (!vm.isCartOpen()) {
                return m('');
            }
            var data = vm.cartData();
            var src = (window.devicePixelRatio >= 2) ? '100px' : '50px';
            return m('div.overlay', {onclick: vm.hide.bind(vm)}, [
                m('div#header-cart',
                    {
                        config: function(el, isInit, ctx){
                            var h = el.clientHeight; //Force browser to render and attach element.
                            el.classList.add('open');
                        },
                        onclick: function(e) {e.stopPropagation()}
                    },
                    m('div.minicart-wrapper', [
                        m('p.block-subtitle', [
                            "Your shopping cart",
                            m('a.close.skip-link-close', {href: "#", title: "close", onclick: vm.hide.bind(vm)}, m('span', m('span')))
                        ]),
                        (data.items.length
                            ? m('ul', data.items.map(function(item){
                                return m('li', [
                                    m('img', {src: item.images[src]}),
                                    m('a', {href: item.product_url}, item.product_name),
                                    m('a.remove', {
                                        href: '/checkout/cart/ajaxDelete/id/'+item.id+'/form_key/'+FORM_KEY+'/',
                                        title: 'Remove this item',
                                        onclick: function(e){ vm.remove(item.id); e.preventDefault(); e.stopPropagation()}
                                    }, 'Remove')
                                ]);
                            }))
                            : m('p.empty', 'Your shopping cart is empty')
                        ),
                        Object.keys(data.totals).map(function(key){
                            return m('p', {className: key}, [
                                m('span.label', data.totals[key].label),
                                m('span.price', data.totals[key].value_formatted)
                            ])
                        }),
                        m('div.minicart-actions', m('ul.checkout-types.minicart', m('li',
                            m('a.button.empty.checkout-button', {href: '#'}, "Checkout")
                        )))
                    ])
                )
            ]);
        }
    };

    window.TradeTested.ShoppingCartOverlay = function(options) {
        m.mount(
            document.getElementById('header-cart-link'),
            m.component({controller: MithrilCart.Controller, view: MithrilCart.View}, options)
        )
    };
})(window);
