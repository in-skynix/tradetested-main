if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}

(function(window) {
    var states = {
        NI: 'North Island',
        SI: 'South Island'
    };
    var MithrilComponent = {
        ViewModel: function () {
            this.initialize.apply(this, arguments);
        },
        Controller: function (config) {
            this.vm = new MithrilComponent.ViewModel(config);
        },
        View: function(ctrl) {
            var vm = ctrl.vm;
            var rates = vm.rates().rates;
            if (vm.areaName()) {
                return m('div',
                    Object.keys(rates).map(function(type) {return vm.renderRates(type, rates[type]);}),
                    m('p.notes', [vm.rates().note, m('a', {href: '/shipping'}, 'View more shipping information')])
                );
            } else {
                var opts = {required: 'required', title: 'Please enter your postcode', type: 'text'};
                return m('div.estimator_blank', [
                    m('h3', 'Shipping'),
                    m('p', 'Enter your postcode to get prices and estimated delivery times.'),
                    m('form', {onsubmit: function (e) {e.preventDefault(); vm.setProductPostcode($('#estimator_postcode').val())}}, [
                        m('input.input-text#estimator_postcode', opts),
                        m('button.button', 'Show Shipping')
                    ])
                ]);

            }
        }
    };
    var CartMithrilComponent = {
        Controller: function (config) {
            this.vm = new MithrilComponent.ViewModel(config);
        },
        View: function(ctrl) {
            var vm = ctrl.vm;
            var opts = {placeholder: 'Enter postcode', required: 'required', title: 'Please enter your postcode', type: 'text'};
            if (vm.postcode()) {opts['value'] = vm.postcode();}
            var contents = [
                m('h3', 'Calculate Shipping'),
                m('form', {onsubmit: function (e) {e.preventDefault(); vm.postcode($('#estimator_postcode').val())}}, [
                    m('input.input-text#estimator_postcode', opts),
                    m('button.button', 'Calculate')
                ])
            ];
            if (vm.rates() && vm.showRates()) {
                var rates = vm.rates().rates;
                contents.push(m('div.rates',
                    Object.keys(rates).map(function(type) {return vm.renderRates(type, rates[type]);})
                ));
            }
            return m('div', contents);
        }
    };
    var RateComponents = {
        Default: {
            view: function(ctrl, args) {
                return m('span', args.rates.map(function(rate) {
                    var radioOpts = {id: 's_'+rate.code, type: 'radio', name: 'shipping_est_method'};
                    if (args.vm.selectedMethod() == rate.code) {
                        radioOpts.checked = 'checked';
                    }
                    var contents = [
                        m('input', radioOpts),
                        m('span.input-icon',[
                            m('span.icon')
                        ]),
                        m('span.title-deliver',  rate.title),
                        m('span.shipping_price', rate.price_formatted),
                    ];
                    if (rate.address) {
                        contents.push(m('br'));
                        contents.push(
                            args.vm.showStoreAddress()
                                ? [
                                m('span.link', {onclick: function(){args.vm.showStoreAddress(false)}},'- Hide store address'),
                                m('address', rate.address)
                            ]
                                : m('span.link', {onclick: function(){args.vm.showStoreAddress(true)}},'+ Show store address')
                        );

                    }
                    return m(
                        'label.radio-label',
                        {for: 's_'+rate.code, onclick: args.vm.getSelectFunction(rate.code)},
                        contents
                    );
                }));
            }
        },
        Depot: {
            view: function(ctrl, args) {
                var vm = args.vm;
                if(vm.rates().in_area == 'Auckland') { return m('span') }
                var selected = args.rates.find(function(r){return (r.code == vm.selectedMethod())})
                    || args.rates.find(function(r){return (r.code == vm.selectedDepot())})
                    || this.closest(args.rates);
                vm.selectedDepot(selected.code);
                var groupedRates = args.rates.groupBy('state');
                var radioOpts = {id: 's_depot', type: 'radio', name: 'shipping_est_method'};
                if (args.vm.selectedMethod() == selected.code) {
                    radioOpts.checked = 'checked';
                }
                var note = selected.time_estimate ? selected.time_estimate+'. ' : '';
                return m('span',[
                    m('label.radio-label', {for: 's_depot', onclick: vm.getSelectFunction(selected.code)}, [
                        m('input', radioOpts),
                        m('span.input-icon',[
                            m('span.icon')
                        ]),
                        m('span.title-deliver', 'Delivery to Nearest Depot'),
                        m('span.shipping_price', selected.price_formatted),
                        m('span.note', note+'Shipping to '+ selected.title),
                        m('span.location_changer', [
                            m('span','Change location'),
                            m(
                                'select',
                                {onchange: function(e){e.preventDefault(); vm.selectedMethod(this.value)}},
                                Object.keys(groupedRates).map(function(state){
                                        return m('optgroup', {label: states[state]||state}, groupedRates[state].map(function(rate){
                                            var title = rate.title;
                                            var priceDifference = rate.price - selected.price;
                                            if (priceDifference > 0) {
                                                title += " (+$"+priceDifference+")"
                                            } else if (priceDifference < 0) {
                                                title += " (-$"+Math.abs(priceDifference)+")"
                                            }
                                            var opts = {value: rate.code};
                                            if (rate.code == selected.code) {
                                                opts.selected = 'selected';
                                            }
                                            return m('option', opts, title);
                                        }));
                                    }
                                ))
                        ]),
                        m('br'),
                        vm.showDepot()
                            ? [
                            m('span.link', {onclick: function(){args.vm.showDepot(false)}},'- Hide depot address'),
                            m('address', selected.address)
                        ]
                            : m('span.link', {onclick: function(){args.vm.showDepot(true)}},'+ Show depot address')
                    ])
                ])
            },
            closest: function(rates) {
                var rate = null;
                for (var i = 0, len = rates.length; i < len; i++) {
                    if (!rate || (rates[i].distance < rate.distance)) {
                        rate = rates[i];
                    }
                }
                return rate;
            }
        },
        ProductMatrix: {
            view: function(ctrl, args) {
                var vm = args.vm;
                var view = this;
                if ((args.rates.length < 1) && !vm.showRates()) { //Don't show in cart
                    return m('span.no-delivery', [
                        m('strong', 'No delivery options to '+vm.areaName()),
                        this.locationChanger(vm)
                    ]);

                }
                return m('span', args.rates.map(function(rate) {
                    var radioOpts = {id: 's_'+rate.code, type: 'radio', name: 'shipping_est_method'};
                    if (rate.code == args.vm.selectedMethod()) {
                        radioOpts.checked = 'checked';
                    }
                    var note = rate.time_estimate ? rate.time_estimate+'. ' : '';
                    return m(
                        'label.radio-label',
                        {for: 's_'+rate.code, onclick: vm.getSelectFunction(rate.code)},
                        [
                            m('input', radioOpts),
                            m('span.input-icon',[
                                m('span.icon')
                            ]),
                            m('span.title-deliver',  rate.title),
                            m('span.shipping_price', rate.price_formatted),
                            vm.areaName() ? m('span.note', note+'Shipping to '+vm.areaName()) : m('span.note', note),
                            vm.showRates() ? null : view.locationChanger(vm)
                        ]
                    );
                }));
            },
            locationChanger: function(vm) {
                var options = vm.areaOptions();
                var areaCode = vm.areaCode();
                return options ? m('span.location_changer', [
                    m('span', 'Change location'),
                    m('select', {onchange: function(){vm.setArea(this.value)}}, [
                        Object.keys(options).map(
                            function(k){
                                var opts = {value: k};
                                if (k == areaCode) {
                                    opts.selected = 'selected';
                                }
                                return m('option', opts, options[k]);
                            }
                        ),
                        areaCode ? null : m('option', {selected: 'selected', disabled: 'disabled', hidden: 'hidden', style: 'display: none'}, 'Select a region')
                    ])
                ]) : m('span.location_changer', {onclick: vm.reset.bind(vm)}, 'Change location')
            }
        }
    };
    MithrilComponent.ViewModel.prototype = {
        initialize: function (config) {
            var vm = this;
            this.rates = m.prop();
            this.areaCode = m.prop();
            this.selectedDepot = m.prop();
            this.showDepot = m.prop(false);
            this.areaOptions = m.prop(config.area_options);
            this.productId = config.product_id;
            this.showRates = m.prop(false);
            this.showStoreAddress = m.prop(false);
            this.loadData(config);
            Observable.on(['blockLoader.success.cart_totals'], function(r){ vm.loadData(r.data.estimator);m.redraw();})
        },
        loadData: function(data) {
            this.rates(data.rates);
            this.areaName(data.area_name||null);
            this._postcode = data.postcode;
            if (data.selected_method) {
                this._selectedMethod = data.selected_method;
            }
            Observable.trigger('shippingEstimator.initMethod', this.selectedMethod());
            this.selectedDepot(null);
            this.areaCode(data.area_code);
            m.redraw();
        },
        setArea: function(areaCode) {
            m.request(
                {method: "GET", url: '/shipping/estimator/product/product_id/'+this.productId+'/area_code/'+areaCode}
            ).then(this.loadData.bind(this));
        },
        setProductPostcode: function(postcode) {
            m.request(
                {method: "GET", url: '/shipping/estimator/product/product_id/'+this.productId+'/postcode/'+postcode}
            ).then(this.loadData.bind(this));
        },
        reset: function() {
            m.request({method: "GET", url: '/shipping/estimator/reset'});
            this.areaName(null);
        },
        postcode: function(postcode) {
            if (postcode) {
                Observable.trigger('shippingEstimator.setPostcode', postcode);
                this.showRates(true);
                this._postcode = postcode;
            } else {
                return this._postcode;
            }
        },
        areaName: function(areaName) {
            if (areaName !== undefined) {
                this._areaName = areaName;
            } else {
                return (this.areaOptions() && !this._areaName) ? this.areaOptions()[this.areaCode()] : this._areaName;
            }
        },
        renderRates: function(type, data) {
            var componentMap = {
                productmatrix: RateComponents.ProductMatrix,
                tradetested_depot: RateComponents.Depot
            };
            var component = componentMap[type] ||  RateComponents.Default;
            return m.component(component, {vm: this, rates: data});
        },
        getSelectFunction: function(methodCode) {
            return function(){this.selectedMethod(methodCode)}.bind(this);
        },
        selectedMethod: function(methodCode) {
            if (!methodCode) {
                return this._selectedMethod;
            } else {
                if (methodCode != this._selectedMethod) {
                    Observable.trigger('shippingEstimator.selectedMethod', methodCode);
                }
                this._selectedMethod = methodCode;
            }
        }
    };

    window.TradeTested.ProductShippingEstimator = function(config) {
        if (config.disabled) {
            return;
        }
        Observable.on(['shippingEstimator.selectedMethod', 'shippingEstimator.initMethod'], function(v){$('#addtocart_shippingmethod').val(v)});
        var container = document.getElementById('product_shipping_estimator');
        this.component = m.component({controller: MithrilComponent.Controller, view: MithrilComponent.View}, config);
        this.mithril = m.mount(container, this.component);
    };

    window.TradeTested.CartShippingEstimator = function(config) {
        var container = document.getElementById('cart_shipping_estimator');
        this.component = m.component({controller: CartMithrilComponent.Controller, view: CartMithrilComponent.View}, config);
        this.mithril = m.mount(container, this.component);
    };
})(window);
