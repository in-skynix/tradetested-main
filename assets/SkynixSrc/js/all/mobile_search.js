/**
 * Created by nastya on 10/12/16.
 */
if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}

(function(window){
    var MithrilMobileSearch = {
        Controller: function(config) {
            this.vm = new MithrilMobileSearch.ViewModel(config);
        },
        ViewModel: function(config) {
            if(window.innerWidth<377){
                this.openSearch = m.prop(true);
            } else{
                this.openSearch = m.prop(false);
            }
            this.config = config;
        },
        View: function(ctrl) {
            return m('div.header-search-block',[
                m('a#show-search-sm', {onclick: ctrl.vm.open.bind(ctrl.vm) } ),
                m(SearchComponent, {vm: ctrl.vm})
            ])
        }
    };
    MithrilMobileSearch.ViewModel.prototype = {
        open: function(ev) {
            ev.preventDefault();
            this.openSearch(!this.openSearch());
        }
    };

    var SearchComponent = {
        view: function(ctrl, args){
            var vm = args.vm;
            var data = args.vm.config.data;
            if(window.innerWidth<378){
                vm.openSearch(true);
            }
            if(!vm.openSearch()){
                return m('');
            }

            return m('div', [
                m('div#header-search.search-block',
                    {
                        config: function(el, isInit, ctx){
                            var h = el.clientHeight;
                            el.classList.add('openedSearch');
                        },
                        onclick: function(ev){
                            ev.stopPropagation();
                        }
                    },
                    [m('form#search_form.search_form.new',  {'method':'get', 'action': data.action},[
                        m('label', {'for':"search"}, data.label),
                        m('input#search.input-text', {'type':'text', 'placeholder': data.placeholder, 'name': data.inputName, 'value':'', maxlength: data.maxlength,
                            config: function(element){
                                if(window.innerWidth > 377){
                                    element.focus();
                                }
                            }} ),
                        m('button.button', {'type':'submit', 'value': data.buttonText}, data.buttonText, [
                            m('span')
                        ])
                    ])
                    ])
            ])
        }
    }

    window.TradeTested.MobileSearch = function(options) {
        m.mount(
            document.getElementById('header-mobile-search'),
            m.component({controller: MithrilMobileSearch.Controller, view: MithrilMobileSearch.View}, options)
        );
    };
})(window)

