if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};

}

(function(window){
    var MithrilReviewCarousel = {
        Controller: function(config){
            this.vm = new MithrilReviewCarousel.ViewModel(config);
        },
        ViewModel: function(config){
            this.config = config;
            this.inizialise.apply(this, arguments);
        },
        View: function(ctrl){
            return m('div.customer-block', [
                m(ReviewCarouselComponent, {vm: ctrl.vm})
            ])
        }
    };

    MithrilReviewCarousel.ViewModel.prototype = {
        inizialise: function(){
            this.left = 0;
            var mobile = document.querySelector('.show-mobile-menu');
            this.mobile = (mobile != null)  ? true : false;
        },
        next: function(ev){
            function setAttributes(el, attrs) {
                for(var key in attrs) {
                    el.setAttribute(key, attrs[key]);
                }
            }
            this.widget = document.querySelector('#customer-reviwes');
            this.overview =  this.widget.querySelector('.overview');
            this.item = this.overview.querySelector('.item');
            this.allItems = this.overview.querySelectorAll('.item');
            this.width = this.item.clientWidth;
            var overviewWidth = this.width*this.allItems.length;
            var maxLeft = overviewWidth - this.width;
            if(this.left != -maxLeft){
                this.left-=this.width;
            } else{
                this.left = 0;
            }
            this.overview.setAttribute('style', 'left:'+ this.left+'px');
            m.redraw();
        }
    }

    var ReviewCarouselComponent = {
        view: function (ctrl, args) {
            var vm = args.vm;
            var data = args.vm.config.data;
            return  m('div.review-block', [
                m('h1.review-block-title', 'From our customers'),
                m('div.viewport', [
                    m('ul.overview',{
                        config: function(el){
                            var reviewCarousel = document.querySelector('#customer-reviwes');
                            var item = reviewCarousel.querySelector('.overview').querySelector('.item');
                            var itemAll = reviewCarousel.querySelector('.overview').querySelectorAll('.item');
                            var reviewBlock = reviewCarousel.querySelector('.review-block');
                            var reviewBlockWidth = reviewBlock.clientWidth;

                            if(vm.mobile){
                                for(var i =0; i<itemAll.length; i++){
                                    itemAll[i].style.width = reviewBlockWidth + 'px';
                                }
                            } else{

                            }

                            var width = item.clientWidth * data.total.length;
                            el.style.width = width +'px';
                        }
                    }, [
                        Object.keys(data.total).map(function(key){
                            return m('li.item', [
                                    m('div.review', [
                                        m('div.review-custom-title',[
                                            m('span.customer-name', data.total[key].customer_name),
                                            m('a.review-product-title', {'href': data.total[key].product_url},[
                                                m('span.review-product-title', '- Reviewing '+ data.total[key].product_name )
                                            ]),

                                        ]),
                                        m('div.review-rate',{
                                            config: function(el, isInit, ctx){
                                                el.classList.add('stars'+data.total[key].review_rate);
                                            }
                                        },[
                                            m('span'),
                                            m('span'),
                                            m('span'),
                                            m('span'),
                                            m('span')
                                        ]),
                                        m('p.review-title',  data.total[key].review_title),
                                        m('span.review-date',  data.total[key].review_date),
                                        m('p.customer-review',  data.total[key].review_text)
                                    ])
                            ])
                        })
                    ])

                ]),
                m('a.view-all-review', {'href':data.total.product_url}, 'View all testimonials'),
                m('button.main-trade-button',{onclick: vm.next.bind(vm)},[
                    m('span')
                ])
            ])

        }
    }

    window.TradeTested.ReviewCarouselBlock = function(options) {
        m.mount(
            document.getElementById('customer-reviwes'),
            m.component({controller: MithrilReviewCarousel.Controller, view: MithrilReviewCarousel.View}, options)
        )
    }
})(window)

