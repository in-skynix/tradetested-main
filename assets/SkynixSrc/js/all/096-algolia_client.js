if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}

(function(window) {

    var AlgoliaClient = function() {this.initialize.apply(this, arguments);}
    AlgoliaClient.prototype = {
        initialize: function(config) {
            this.appId = config.appId;
            this.apiKey = config.apiKey;

            /*
            * indexConfig e.g.
            * {magento_default_products: {params: {hitsPerPage: 3}},
            * magento_default_suggestion: {params: {hitsPerPage: 5}}}
            */
            this.indexConfig = config.indexConfig;
            this.queryCache = new TradeTested.LruCache();
        },
        //Convert data into URL params for Algolia
        param: function(data) {
            var ret = [];
            for (var d in data)
                ret.push(encodeURIComponent(d) + "=" + encodeURIComponent(data[d]));
            return ret.join("&");
        },
        search: function(queryString, callback) { //callback is called with results as param. callback to be pre-bound
            var client = this;
            if (cached = this.queryCache.get(queryString)) {
                callback(cached);
            } else {
                var data = Object.keys(this.indexConfig).map(function(k, i) {
                    p = client.indexConfig[k].params;
                    p.query = queryString;
                    return {indexName: k, params: client.param(p)}
                });
                if(this.transport) {
                    this.transport.abort();
                }
                m.request(
                    {
                        method: "POST",
                        url: 'https://'+this.appId+'-dsn.algolia.net/1/indexes/*/queries',
                        data: {requests: data},
                        config: function(xhr) {
                            xhr.setRequestHeader('X-Algolia-API-Key', client.apiKey);
                            xhr.setRequestHeader('X-Algolia-Application-Id', client.appId);
                            client.transport = xhr;
                        }
                    }
                ).then(function(r){
                        client.queryCache.set(queryString, r.results);
                        callback(r.results);
                    });
            }
        }
    };

    window.TradeTested.AlgoliaClient = AlgoliaClient;

})(window);
