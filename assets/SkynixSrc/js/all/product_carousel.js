
if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}

(function(window){
    var MithrilProductCarousel = {
        Controller: function(config){
            this.vm = new MithrilProductCarousel.ViewModel(config);
        },
        ViewModel: function(config){
            this.config = config;
            this.inizialise.apply(this, arguments);
        },
        View: function(ctrl){
            return m('div.widget', [
                m('div.widget-title', [
                    m('h2', 'Popular Products')
                ]),
                m(ProductCarouselComponent, {vm: ctrl.vm})
            ])
        }
    };

    MithrilProductCarousel.ViewModel.prototype = {
        inizialise: function(){
            this.left = 0;
            var mobile = document.querySelector('.show-mobile-menu');
            this.mobile = (mobile != null)  ? true : false;
        },
        next: function(ev){
            function setAttributes(el, attrs) {
                for(var key in attrs) {
                    el.setAttribute(key, attrs[key]);
                }
            }
            this.widget = document.querySelector('#products-catalog');
            this.overview =  this.widget.querySelector('.overview');
            this.item = this.overview.querySelector('.item');
            this.allItems = this.overview.querySelectorAll('.item');


            if(this.mobile && window.innerWidth > 377){
                var overviewWidth = (this.width+10)*this.allItems.length;
                var maxLeft = overviewWidth - (3*(this.width+10));
                var itemWidth = this.width + 10;
            } else{
                this.width = this.item.clientWidth;
                var overviewWidth = (this.width+22)*this.allItems.length;
                var maxLeft = overviewWidth - (4*(this.width+22));
                var itemWidth = this.width + 22;
            }

            if(this.left != -maxLeft){
                this.left-=itemWidth;
            } else{
                this.left = 0;
            }
            Object.assign(this.overview.style, {left: this.left+'px'});
        }
    }

    var ProductCarouselComponent = {
        view: function (ctrl, args) {
            var vm = args.vm;
            var data = args.vm.config.data;

            return m('div.widget-products.product-carousel', [
                m('div.viewport', [
                    m('ul.products-grid.overview',{
                        config: function(el){
                            var productCatalog = document.querySelector('#products-catalog');
                            var overview = productCatalog.querySelector('.overview');
                            var item = overview.querySelector('.item');

                            if(!vm.mobile && !window.innerWidth < 378){
                                var width = (item.clientWidth+22) * data.total.length;
                                el.style.width = width +'px';
                            } else if(vm.mobile && window.innerWidth > 377){
                                var viewport = productCatalog.querySelector('.viewport'),
                                    mainWidth = viewport.clientWidth,
                                    itemWidth = (mainWidth-40)/3,
                                    itemAll = overview.querySelectorAll('.item');
                                vm.width = itemWidth;
                                // viewport.style.width = mainWidth + 'px';
                                for(var i=0; i<itemAll.length; i++){
                                    itemAll[i].style.width = itemWidth + 'px';
                                }
                                var width = ((itemWidth+10) * data.total.length)+10;
                                el.style.width = width + 'px';
                                // viewport.style.width = (itemWidth+12)*3 + 'px';
                            }

                        }},[
                        Object.keys(data.total).map(function(key){

                            if(data.total[key].is_in_stock){
                                var sale = m('button.button.btn-cart', {'title':"Add to cart",

                                    onclick: function(){
                                        setLocation(data.total[key].add_to_cart);
                                    }}, [
                                    m('span', [
                                        m('span', 'Add')
                                    ])
                                ]);
                                var inOutOfStock = m('span.in-stock', 'In stock');
                            }else {
                                // var sale = m('p.availability.out-of-stock', [
                                //     m('span', 'Out of stock')
                                // ]);
                                var inOutOfStock = m('span.out-of-stock', 'Out of stock');
                            }

                            if(data.total[key].special_price != '0.00'){
                               var price =  m('div.price-box.special',[
                                    m('span#product-price-3350-widget-new-grid.regular-price', [
                                        m('span.price.old-price', '$'+data.total[key].product_price+''),
                                        m('span.special-price.new-price', '$'+data.total[key].special_price)
                                    ])
                                ])
                            } else{
                                var price =
                                    m('div.price-box',[
                                        m('span#product-price-3350-widget-new-grid.regular-price', [
                                            m('span.price.old-price', '$'+data.total[key].product_price+'')])
                                    ])
                            }
                            return m('li.item',[
                                m('a.product-image', {'href': data.total[key].product_url, 'title': data.total[key].name,
                                    config: function(el, isInit, ctx){
                                        var h = el.clientHeight;
                                        if(data.total[key].top_seller){
                                            el.classList.add('topseller');
                                        }
                                    }
                                }, [
                                    m('img', {'src': data.total[key].image_url_small, 'alt': data.total[key].product_name})]),

                                m('div', [
                                    m('h3.product-name', [
                                        m('a', {'href': data.total[key].product_url, 'title': data.total[key].name}, data.total[key].name)
                                    ]),
                                    inOutOfStock,
                                    price,
                                    m('div.action', [
                                        sale,
                                        m('ul.add-to-links'),
                                        m('span.price', [
                                            m('p.ammount', {onclick: function(){

                                            }})
                                        ])
                                    ])
                                ])

                            ])
                        })
                    ]),
                    m('button.main-trade-button.next',{onclick: vm.next.bind(vm)

                    }, [
                        m('span')
                    ])
                ])
            ])
        }
    }

    window.TradeTested.ProductCarouselBlock = function(options) {
        m.mount(
            document.getElementById('products-catalog'),
            m.component({controller: MithrilProductCarousel.Controller, view: MithrilProductCarousel.View}, options)
        )
    }
})(window)

