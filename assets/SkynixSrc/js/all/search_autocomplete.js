if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}

(function(window) {
    /*
     * Contains the lists and the HitList.
     *
     * Lists are the details returned from each index.
     *
     * pass empty array or null to load to clear the ResultSet
     */
    var ResultSet = function(config){
        this.load = function(algoliaResults) {
            this.lists = [];
            var results = algoliaResults ? algoliaResults : [];
            //Hash techically isn't ordered, so sort them just in case.
            results.sort(this.sortCompare.bind(this));
            this.hits = new TradeTested.HitList();
            for (var i = 0, len = results.length; i < len; i++) {
                var list = results[i];
                var config = this.config[list.index];

                //if there are no hits, or too many hits for the config, don't use this list at all.
                if((list.hits.length >= 1) && (!config.hideIfPage || (list.nbPages <= 1))) {
                    this.lists.push(list);
                    for (var x = 0, l = list.hits.length; x < l; x++) {
                        var hit = list.hits[x];
                        //Quick fix for the fact that cms index has title instead of name.
                        //The name is not just used in rendering
                        if (!hit.name) {
                            hit.name = hit.title;
                        }
                        this.hits.push(hit);
                    }
                }
            }
        };
        this.isEmpty = function () {
            return !this.lists || (this.lists.length < 1);
        },
        this.sortCompare = function(a, b) {
            return (this.config[a.index].sortOrder > this.config[b.index].sortOrder)
        };
        this.config = config;
        this.load();
    };

    var components = {

        /*
         * Generic Component for rendering a list
         */
        ResultListComponent: {
            controller: function() {},
            view: function(ctrl, args) {
                return m("li", [
                    m('strong', args.header),
                    args.hits.map(function(hit, i){
                        return m('a', {href: hit.url, onmouseover: function(){args.hitlist.activate(hit)}, className: hit.active ? 'active' : ''}, hit.path||hit.name);
                    })
                ]);
            }
        },

        /*
         * Component for rendering a list of suggestions
         */
        SuggestionListComponent: {
            controller: function() {},
            view: function(ctrl, args) {
                var origin = window.location.origin ? window.location.origin : '';
                return m("li", [
                    m('strong', args.header),
                    args.hits.map(function(hit, i){
                        hit.name = hit.query_text.toLowerCase();
                        hit.url = origin + '/catalogsearch/result/?q=' + hit.name;
                        return m('a', {href: hit.url, onmouseover: function(){args.hitlist.activate(hit)}, className: hit.active ? 'active' : ''}, '"'+hit.name+'"');
                    })
                ]);
            }
        },


        /*
         * Component for rendering a list of products
         */
        ProductListComponent: {
            controller: function() {},
            view: function(ctrl, args) {
                return m("li", [
                    m('strong', args.header),
                    args.hits.map(function(hit, i){
                        return m('a', {href: hit.url, onmouseover: function(){args.hitlist.activate(hit)}, className: hit.active ? 'active' : ''}, [
                            m('div', {className: 'product_result'}, [
                                m('img', {src: hit.thumbnail_url}),
                                m('div', {className: 'product_name'}, hit.name)
                            ])
                        ]);
                    })
                ]);
            }
        }

    };

    /*
     * A Standard constructor that mounts the Mithril App
     */
    var Autocomplete = function(config){
        config.container = document.getElementById('search_form');
        m.mount(
            config.container,
            m.component(
                {controller: TradeTested.SearchAutocomplete.Controller, view: TradeTested.SearchAutocomplete.View},
                config
            )
        );
    };

    /*
     * View Model.
     *
     * Manages state of the app. Is a bit of a dumping ground for everything.
     */
    Autocomplete.ViewModel = function(){ this.initialize.apply(this, arguments); };
    Autocomplete.ViewModel.prototype = {
        initialize: function(config) {
            this.placeholder = config.placeholder; //Placeholder attribute on input
            this.buttonText = config.buttonText||'Go'
            /*
            * Key is the index name in Algolia.
            * Header is used for rendering component.
            * params are sent to algolia
            * hideIfPage indicates that it should not be displayed if more than one page of hits.
            * component is used to render the list.
            */
            this.indexConfig = config.indexConfig;
            for(var i in this.indexConfig) {
                this.indexConfig[i].component = components[this.indexConfig[i].component];
            }
            this.algoliaClient = new TradeTested.AlgoliaClient({appId: config.appId, apiKey: config.apiKey, indexConfig: this.indexConfig})
            this.results = new ResultSet(this.indexConfig);

            // Value of search input
            this.searchString = m.prop(config.inputValue);

            //Is the dropdown open?
            this.isOpen = false;

        },
        search: function(searchString) {
            this.searchString(searchString);
            this.results.hits.activate(); //Clear active hit immediately, so quickly hitting enter goes to what we're typing, not previously active hit.
            this.open(true);
            if (this.searchString() && this.searchString().length >= 3) {
                this.algoliaClient.search(this.searchString(), this.results.load.bind(this.results));
            } else {
                this.results.load();
            }
        },
        //when arrow up/down over hits, fill the input.
        fillSuggestion: function() {
            var h;
            if (h = this.results.hits.activeHit()) {
                this.searchString(h.path||h.name);
            }
        },
        //Close the dropdown on leaving input
        blur: function(e) {
            this.open(false);
            this.results.load();
        },
        open: function() {
            if (arguments.length) {
                this.isOpen = arguments[0];
            }
            return this.isOpen && !this.results.isEmpty();
        },
        toggleOverlay: function() {
            $('body>.wrapper').toggleClass('navMask', this.open())
        },
        //Handle up/down/enter
        checkKey: function(e) {
            if (!this.searchString()) {
                // quick fix for browsers not supporting oninput., don't clear/redraw
                return m.redraw.strategy('none');
            } else if (e.keyCode == '38') { //up
                this.results.hits.prev();
                this.fillSuggestion();
                e.preventDefault();
            } else if (e.keyCode == '40') { //down
                this.results.hits.next();
                this.fillSuggestion();
                e.preventDefault();
            } else if (e.which == 13 || e.keyCode == 13) { //Enter
                if (this.results.hits.activeHit()) {
                    window.location = this.results.hits.activeHit().url;
                    e.preventDefault();
                }
                //If there is no active hit, allow form to submit as normal
            } else {
                m.redraw.strategy('none');
            }
        },
        //Button and input CSS class, if empty, will have icon on left.
        elClass: function(orig) {
            return this.searchString() ? orig : orig+' empty'
        }
    };

    Autocomplete.Controller = function(config) {
        this.vm = new Autocomplete.ViewModel(config);
    };

    Autocomplete.View = function(ctrl, config) {
        els = [
            m("input", {
                placeholder: config.placeholder,
                onkeydown: ctrl.vm.checkKey.bind(ctrl.vm),
                oninput: m.withAttr("value", ctrl.vm.search.bind(ctrl.vm)),
                onblur: ctrl.vm.blur.bind(ctrl.vm),
                value: ctrl.vm.searchString(),
                name: config.inputName,
                id: 'search',
                className: ctrl.vm.elClass('search-input input-text'),
                autocomplete: 'off',
                spellcheck: 'false',
                autocorrect: 'off',
                autocapitalize: 'off',
                type: 'text'
            }),
            m('button', {
                type: 'submit',
                title: 'search',
                className: ctrl.vm.elClass('search-button button btn-primary')
            }, m.trust(ctrl.vm.buttonText))
        ];
        if (ctrl.vm.open()) {
            els.push(
                m("ul", {
                    className: 'typeahead',
                    onmouseout: function(){ ctrl.vm.results.hits.activate() }, //clear active hits when mouse leaving area
                    onmousedown: function(e){ e.preventDefault(); } // Prevent blurring input when clicking in suggestions area
                }, [
                    ctrl.vm.results.lists.map(function(result, index) {
                        var listConfig = ctrl.vm.indexConfig[result.index];
                        return m.component(listConfig.component, {header: listConfig.header, hits: result.hits, hitlist: ctrl.vm.results.hits});
                    })
                ])
            );
        }
        return m("div", {config: ctrl.vm.toggleOverlay.bind(ctrl.vm)}, els);
    };

    window.TradeTested.SearchAutocomplete = Autocomplete;

})(window);
