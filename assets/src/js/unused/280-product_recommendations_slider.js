//Reverse engineered from ES6 https://github.com/ArthurClemens/mithril-slider
if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}

(function(window) {
    var ProductRecommendationsSlider = function(el, url) {
        var getPageData = function() {
            return m.request({
                method: "GET",
                url: url,
                data: {
                    page: 1,
                    perPage: 8,
                    include: ['images.recolist', 'images.recolist_retina']
                }
            });
        };
        var pageView = function(opts) {
            var product = opts.data;
            return m('div.item', [
                m('a.product-image', {href: product.url},
                    m('img', {src: product.images.recolist, 'data-retina': product.images.recolist_retina})
                ),
                m('h4.product-name', m('a', {href: product.url}, product.name)),
                m.component(PriceBox, {product: product})
            ]);
        };
        m.mount(el, new TradeTested.MithrilSlider({pageData: getPageData, pageView: pageView, class: 'up-sell', groupBy: 4}));
    };

    var PriceBox = {
        view: function (ctrl, args) {
            if (args.product.special_price && args.product.price) {
                return m('div.price_box', [
                    m('p.old-price', m('span.price', this.formatPrice(args.product.price))),
                    m('p.special-price', m('span.price', this.formatPrice(args.product.special_price)))
                ]);
            }
            if (args.product.price) {
                return m('div.price_box', [
                    m('span.regular-price', m('span.price', this.formatPrice(args.product.price)))
                ]);
            } else {
                return m('div.price_box');
            }
        },
        formatPrice: function(price) {
            return '$'+price.toFixed(2);
        }
    };

    window.TradeTested.ProductRecommendationsSlider = ProductRecommendationsSlider;

})(window);
