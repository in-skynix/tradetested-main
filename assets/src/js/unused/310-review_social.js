/*
Gets permission to post to user's wall and stores signed request in cookie
 */
if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}
(function($, window) {
    ReviewSocial = function(){ this.initialize.apply(this, arguments); };
    ReviewSocial.prototype = {
        initialize: function(config) {
            this.config = $.extend(
                {checkbox: '#social_field'},
                config
            );
            $(this.config.checkbox).change(function() {
                if (this.checked) {
                    FB.login(function() {}, {
                        scope: 'publish_actions'
                    });
                }
            });
        }
    };
    window.TradeTested.ReviewSocial = ReviewSocial;

})(window.jQuery, window);

