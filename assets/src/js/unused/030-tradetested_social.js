/*
 Initialize facebook.
 */
if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}
(function(window) {
    Social = function(){ this.initialize.apply(this, arguments); };
    Social.prototype = {
        initialize: function(config) {
            this.config = config;
            this.initFacebook();
        },
        initFacebook: function() {
            var appId = this.config.facebook.appId;
            window.fbAsyncInit = function () {
                FB.init({
                    appId: appId,
                    cookie: true,
                    xfbml: true,
                    version: 'v2.4'
                });
            };
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        }
    };
    window.TradeTested.Social = Social;

})(window);

