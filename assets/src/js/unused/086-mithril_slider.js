//Reverse engineered from ES6 https://github.com/ArthurClemens/mithril-slider
if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}

(function(window) {

    var MithrilSlider = function(config) {
        this.vm = new ViewModel(config);
        return new Component(this.vm);
    };

    var Component = function(vm) {
        this.vm = vm;
        this.view = function(ctrl, opts) {
            var vm = this.vm;
            var contentEl = vm.contentEl();
            var list = vm.list();
            var currentIndex = vm.index();
            // sizes need to be set each redraw because of screen resizes
            vm.groupBy(opts.groupBy || 1);
            if (contentEl) {
                vm.updateContentSize(contentEl);
            }
            return m('div', {class: ['slider', vm.class || ''].join(' ')},
                opts.before ? m('.before', opts.before) : null,
                m(
                    '.content',
                    {
                        config: function(el, inited, context) {
                            if (inited) {return;}
                            vm.setContentEl(el);
                            var mc = new Hammer.Manager(el, {});
                            mc.add(new Hammer.Pan({
                                orientation: (opts.orientation === 'vertical') ? Hammer.DIRECTION_VERTICAL : Hammer.DIRECTION_HORIZONTAL,
                                threshold: 0
                            }));
                            mc.on('panmove', vm.handleDrag.bind(vm));
                            mc.on('panend', vm.handleDragEnd.bind(vm));
                            mc.on('panstart', vm.handleDragStart.bind(vm));
                            context.onunload = function() {
                                mc.off('panmove', vm.handleDrag.bind(vm));
                                mc.off('panend', vm.handleDragEnd.bind(vm));
                                mc.off('panstart', vm.handleDragStart.bind(vm));
                            };
                        }
                    },
                    list.map(function(data, listIndex) {
                        return vm.page({
                            data: data,
                            listIndex: listIndex,
                            currentIndex: currentIndex
                        });
                    })
                ),
                opts.after ? m('.after', opts.after) : null);
        }
    };

    var ViewModel = function() {
        var e = arguments.length <= 0 || void 0 === arguments[0] ? {} : arguments[0];
        this.defaultDuration = parseInt(e.duration, 10) || 250;
        this.index = m.prop(e.index || 0);
        this.list = e.pageData();
        this.page = e.pageView;
        this.contentEl = m.prop();
        this.pageSize = 0;
        this.groupBy = m.prop(e.groupBy || 1);
        this.cancelDragFactor = e.cancelDragFactor || .2;
        this.isVertical = ("vertical" === e.orientation);
        this.dir = e.rtl ? -1 : 1;
        this.class = e.class;
    };
    ViewModel.prototype = {
        setIndex: function (idx) {
            this.index(idx);
        },
        getPageEl: function (el, idx) {
            return el.childNodes[idx];
        },
        setTransitionStyle: function(el, value) {
            var style = el.style;
            var createAttrs = function () {
                var x = this.isVertical ? "0" : value + "px";
                var y = this.isVertical ? value + "px" : "0";
                var z = "0";
                var attrs = [x, y ,z].join(", ");
                return "translate3d(" + attrs + ")";
            };
            style.transform = style["-webkit-transform"] = style["-moz-transform"] = style["-ms-transform"] = createAttrs();
        },
        setTransitionDurationStyle: function(duration) {
            this.contentEl().style["-webkit-transition-duration"] = this.contentEl().style["transition-duration"] = duration + "ms";
        },
        goTo: function(idx, duration) {
            if (idx < 0 || idx > this.list().length - 1) {
                return;
            }
            if (duration !== undefined) {
                this.setTransitionDurationStyle(duration);
            }
            this.setTransitionStyle(this.contentEl(), -this.dir * idx * this.pageSize);
            this.setIndex(idx);
        },
        normalizedStep: function(orientation) {
            var idx = this.index();
            var size = this.groupBy();
            var min = 0;
            var max = this.list().length;
            var next = idx + (orientation * size);
            // make sure that last item aligns at the right
            if ((next + size) > max) {
                return max - size;
            }
            if (next < min) {
                return min;
            }
            return next;
        },
        updateContentSize: function(el) {
            var page = el.childNodes[0], prop = this.isVertical ? "height" : "width";
            this.pageSize = page.getBoundingClientRect()[prop];
            el.style[prop] = this.list().length * this.pageSize + "px";
        },
        goCurrent: function() {
            var duration = arguments.length <= 0 || void 0 === arguments[0] ? 0 : arguments[0];
            this.updateContentSize(this.contentEl());
            this.setTransitionDurationStyle(duration);
            this.goTo(this.normalizedStep(0));
        },
        goNext: function () {
            var duration = arguments.length <= 0 || void 0 === arguments[0] ? this.defaultDuration : arguments[0];
            this.setTransitionDurationStyle(duration);
            this.index() < this.list().length ? this.goTo(this.normalizedStep(1)) : this.goTo(this.normalizedStep(0))
        },
        goPrev: function () {
            var duration = arguments.length <= 0 || void 0 === arguments[0] ? this.defaultDuration : arguments[0];
            this.setTransitionDurationStyle(duration);
            this.index() < this.list().length ? this.goTo(this.normalizedStep(-1)) : this.goTo(this.normalizedStep(0))
        },
        hasNext: function () {
            return this.index() + this.groupBy() < this.list().length
        },
        hasPrevious: function () {
            return this.index() > 0
        },
        setContentEl: function (el) {
            this.contentEl(el);
            this.updateContentSize(el);
            this.goCurrent(0);
        },
        handleDragStart: function () {
            return this.setTransitionDurationStyle(0);
        },
        handleDrag: function (e) {
            var el = this.contentEl();
            var page = this.getPageEl(el, this.index());
            var delta = this.isVertical ? e.deltaY : e.deltaX;
            var origin = this.isVertical
                ? page.offsetTop
                : (this.dir === -1) ? (page.offsetLeft - page.parentNode.clientWidth + page.clientWidth) : page.offsetLeft;
            this.setTransitionStyle(el, delta - origin);
            e.preventDefault();
        },
        calculateTransitionDuration: function (velocity) {
            var speed = Math.abs(velocity), duration = 1 / speed * 360;
            80 > duration && (duration = 80);
            duration > this.defaultDuration && (duration = this.defaultDuration);
            return duration;
        },
        handleDragEnd: function (e) {
            var duration = this.calculateTransitionDuration(e.velocity), delta = this.isVertical ? e.deltaY : e.deltaX;
            if (Math.abs(delta) > this.pageSize * this.groupBy() * this.cancelDragFactor) {
                if (this.dir * delta < 0) {
                    this.goNext(duration);
                } else {
                    this.goPrev(duration);
                }
            } else {
                this.goCurrent(duration);
            }
        }
    };

    window.TradeTested.MithrilSlider = MithrilSlider;

})(window);
