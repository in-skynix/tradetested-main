(function($, window) {
  var AjaxBLockLoader;
  AjaxBLockLoader = (function() {
    AjaxBLockLoader.prototype.defaults = {
      queued: true,
      updateIfQueued: true
    };

    function AjaxBLockLoader(options) {
      var me;
      this.options = $.extend({}, this.defaults, options);
      this.blocks = {};
      me = this;
      $('[data-ajax_block]').each(function() {
        if ($(this).is(':visible')) {
          return me.addBlock($(this));
        }
      });
      this.queued = this.options.queued;
      this.loadBlocks();
    }

    AjaxBLockLoader.prototype.refresh = function() {
      this.queued = true;
      return this.loadBlocks();
    };

    AjaxBLockLoader.prototype.loadBlocks = function() {
      var me;
      me = this;
      if (this.canLoad()) {
        this.loading = true;
        this.queued = false;
        return $.ajax({
          url: '/ajax?' + this.hash(),
          complete: function() {
            me.loading = false;
            return me.loadBlocks();
          },
          success: function(r) {
            return me.renderResponse(r);
          }
        });
      }
    };

    AjaxBLockLoader.prototype.renderResponse = function(r) {
      var me;
      if (this.options.updateIfQueued || !this.queued) {
        me = this;
        return $.each(r, function(_location, _hash) {
          Observable.trigger('blockLoader.success.'+_location, _hash);
          var _html = _hash.html;
          if (_html && ((typeof _html === 'string') || (_html instanceof String))) {
            me.blocks[_location].html(_html);
            me.blocks[_location].trigger('ajax_block_replaced', _html);
          }
          return me.blocks[_location].trigger('ajax_block_loaded', _html);
        });
      }
    };

    AjaxBLockLoader.prototype.canLoad = function() {
      return (!!this.queued) && (!this.loading) && (!$.isEmptyObject(this.blocks));
    };

    AjaxBLockLoader.prototype.addBlock = function(container, reference) {
      reference || (reference = container.data('ajax_block'));
      this.blocks[reference] = container;
      return this;
    };

    AjaxBLockLoader.prototype.hash = function() {
      var data;
      data = {
        store_code: STORE_CODE
      };
      $.each(this.blocks, function(_location, _el) {
        return data[_location] = _el.data('ajax_block_data');
      });
      return $.param(data);
    };

    return AjaxBLockLoader;

  })();
  window.AjaxBLockLoader = AjaxBLockLoader;
  return $(function() {
    return window.ajax_block_loader = new AjaxBLockLoader();
  });
})(window.jQuery || window.ender || window.Zepto, window);