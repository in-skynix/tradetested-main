$(function() {
  return $('[data-ajax="popup"]').click(function(event) {
    var content, el;
    el = $(this);
    if (el.data('target')) {
      el.data('popup_content', $(el.data('target')));
    }
    if ((content = el.data('popup_content'))) {
      content.show();
      $(content).find('.container>.content').css('top', $(window).scrollTop()+'px');
    } else {
      el.data('spinner', new Spinner({
        lines: 10,
        length: 6,
        width: 5,
        radius: 10,
        color: '#ccc'
      }));
      var location = (el.attr('href') && el.attr('href') != '#') ? el.attr('href') : el.data('popup-url');
      if (location) {
        el.data('spinner').spin(el.get(0));
        $.get(location, function(response) {
          el.data('spinner').stop();
          el.data('popup_content', $(response));
          var popup = $(document.body).append(el.data('popup_content'));
          $(popup).find('.container>.content').css('top', $(window).scrollTop()+'px');
            $(document).trigger('ajax_block_replaced', response);
        });
      }
    }
    return event.preventDefault();
  });
});