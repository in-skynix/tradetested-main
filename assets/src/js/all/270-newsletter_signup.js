$(function(){
    $('.newsletter_subscribe').unbind('submit').submit(function(e) {
        var result = $(this).find('.result');
        var data = $(this).serialize();
        var form = $(this);
        form.find(':input').prop("disabled", true);
        var spinner = new Spinner({lines: 7,length: 5,width: 5,radius: 10, color: '#ccc'});
        spinner.spin(form.get(0));
        Observable.trigger('emailCaptured', form.find('[name="EMAIL"]').val());
        $.ajax({
            url: $(this).attr('action').replace('/post', '/post-json')+'&c=?',
            data: data,
            dataType: 'jsonp',
            error       : function(err) {
                spinner.stop();
                result.html("Could not connect to the registration server.").addClass('error-msg');
                form.find(':input').prop("disabled", false);
            },
            success     : function(data) {
                spinner.stop();
                if ((data.result == "success") || (data.msg.indexOf('already subscribed to list') > -1)) {
                    result.html('Thanks for subscribing').removeClass('error-msg').addClass('success-msg');
                    form.find(':input, label').hide();
                    window.setTimeout(function(){ form.closest('.ajax_popup').hide(); }, 3000);
                } else {
                    result.html(data.msg).addClass('error-msg');
                    form.find(':input').prop("disabled", false);
                }
            }
        });
        e.preventDefault();
        return false;
    });
});
