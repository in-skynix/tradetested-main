(function( $ ){
    $.fn.mageRetina = function() {
        if(window.devicePixelRatio >= 2) {
            this.each(function(index, element) {
                if(!$(element).attr('src') ||  ($(element).attr('src') == $(element).attr('data-retina'))) return;
                var new_image_src = $(element).attr('data-retina');
                $.ajax({url: new_image_src, type: "HEAD", success: function() {
                    $(element).attr('src', new_image_src);
                }});
            });
        }
        return this;
    }
})( jQuery );