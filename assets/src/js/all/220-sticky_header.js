$(function(){
    $(window).on('scroll resize', function(){
        $('#header_container').toggleClass('fixed', (($(window).scrollTop() > 90) && ($(window).width() > 1024)));
    });
});