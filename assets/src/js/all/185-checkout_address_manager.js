if (typeof window.TradeTested === 'undefined') {
  window.TradeTested = {};
}

(function(window) {
  CheckoutAddressManager = function(){ this.initialize.apply(this, arguments); };
  CheckoutAddressManager.prototype = {
    initialize: function(config) {
      this.oldAddressData = config.oldAddressData;
      this.addressBook = config.addressBook;
      this.currentArea = 'shipping';
    },
    undo: function() {
      var manager = this;
      $.each(['billing', 'shipping'], function(k, area){
        if (data = manager.oldAddressData[area]) {
          manager.prefillData(area, data);
        }
      });
      var same = !!(manager.oldAddressData.shipping && (manager.oldAddressData.shipping['same_as_billing'] == '1'));
      $('#different_billing').prop('checked', (!same && manager.oldAddressData.billing)).change();
    },
    hide: function() {
      $('#address_manager_popup').hide();
    },
    selectAddress: function(id) {
      this.prefillData(this.currentArea, this.addressBook[id]);
      this.hide();
    },
    prefillData: function(area, data) {
      $.each(data, function(k, v) {
        $('[name="'+area+'['+k+']"]').val(v);
      });
    }

  };
  window.TradeTested.CheckoutAddressManager = CheckoutAddressManager;

})(window);
