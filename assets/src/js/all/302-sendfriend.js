(function(window) {
    window.TradeTested.SendFriend = {
        recipientIDCounter: 1, 
        recipientCount: 1, 
        maxRecip: 5, 
        removeRecipient: function(i){
            $('#recipient'+i).remove();
            this.recipientCount--;
            if(this.recipientCount<this.maxRecip && this.maxRecip != 0) {
                $('#add_recipient_button').show();
                $('#max_recipient_message').hide();
            }
            return false;
        }, 
        addRecipient: function(){
        var items = $('#recipient0').find('.input-box').clone();
        items.find("[id^='recipient0']").each(function(){
            $(this).attr('id', $(this).attr('id').replace('recipient0','recipient'+this.recipientIDCounter));
        });
        $('#recipients_options').append(
            $('<li id="recipient'+this.recipientIDCounter+'" />').append(items).append('<a href="#" class="remove_recipient_button" onclick="window.TradeTested.SendFriend.removeRecipient('+this.recipientIDCounter+')">Remove</a>')
        );
        this.recipientIDCounter++;
        this.recipientCount++;
        if(this.recipientCount>=this.maxRecip && this.maxRecip != 0) {
            $('#add_recipient_button').hide();
            $('#max_recipient_message').show();
        }
    }
        
    };
})(window);
