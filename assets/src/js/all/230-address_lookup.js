if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}

(function(window) {
    var Client = function() {this.initialize.apply(this, arguments);};
    Client.prototype = {
        initialize: function(config) {
            this.baseUrl = config.baseUrl;
            this.jwt = config.jwt;
            this.queryCache = new TradeTested.LruCache();
        },
        search: function(queryString, lineNo, callback) { //callback is called with results as param. callback to be pre-bound
            var client = this;
            var cached;
            if(this.transport) {
                this.transport.abort();
            }
            if (cached = this.queryCache.get(queryString)) {
                callback(cached);
                m.redraw();
            } else {
                m.request(
                    {
                        method: "GET",
                        url: client.baseUrl+'suggest',
                        data: {q: queryString, l: lineNo},
                        config: function(xhr) {
                            xhr.setRequestHeader('Authorization', 'JWT '+client.jwt);
                            client.transport = xhr;
                        }
                    }
                ).then(function(r){
                    client.queryCache.set(queryString, r);
                    callback(r);
                });
            }
        },
        fetch: function(addressId, filteredCustomerInput, supplementaryData, callback) {
            var client = this;
            m.request(
                {
                    method: "GET",
                    url: client.baseUrl+'fetch',
                    data: {i: addressId, fci: filteredCustomerInput, suppl: supplementaryData},
                    config: function(xhr) {
                        xhr.setRequestHeader('Authorization', 'JWT '+client.jwt);
                        client.transport = xhr;
                    }
                }
            ).then(callback);

        }
    };

    /*
     * Contains the lists and the HitList.
     *
     * pass empty array or null to load to clear the ResultSet
     */
    var ResultSet = function(){
        this.load = function(results) {
            this.lists = [];
            results = results ? results : [];
            //Hash techically isn't ordered, so sort them just in case.
            this.hits = new TradeTested.HitList();
            if(results.length >= 1) {
                for (var x = 0, l = results.length; x < l; x++) {
                    var hit = results[x];
                    this.hits.push(hit);
                }
            }
        };
        this.load();
    };

    /*
     * A Standard constructor that mounts the Mithril App
     */
    var AddressLookup = function(config){
        this.component = m.component(
            {controller: Autocomplete.Controller, view: Autocomplete.View},
            config
        );
        this.mithril = m.mount(
            config.container,
            this.component
        );
        var input1 = config.fields.street_1;
        var input2 = config.fields.street_2;
        var vm = this.mithril.vm;
        input1.addEventListener('input', function() {
            vm.search(input1.value+"\n"+input2.value, 1);
        });
        input1.addEventListener('keydown', vm.checkKey.bind(vm));
        input1.addEventListener('blur', function(){vm.results.load();m.redraw();});
        input2.addEventListener('input', function() {
            vm.search(input1.value+"\n"+input2.value, 2);
        });
        input2.addEventListener('keydown', vm.checkKey.bind(vm));
        input2.addEventListener('blur', function(){vm.results.load();m.redraw();});
    };

    Autocomplete = {
        ViewModel: function () {
            this.initialize.apply(this, arguments);
        },
        Controller: function(config) {
            this.vm = new Autocomplete.ViewModel(config);
        },
        View: function(ctrl, config) {
            els = [];
            var hits = ctrl.vm.results.hits;
            if (ctrl.vm.open()) {
                els.push(
                    m("div",
                        {
                            className: 'typeahead line'+ctrl.vm.line(),
                            onmouseout: function(){ hits.activate() }, //clear active hits when mouse leaving area
                            onmousedown: function(e){ e.preventDefault(); } // Prevent blurring input when clicking in suggestions area
                        },
                        m("ul", [
                            hits.map(function(hit, i){
                                return m('li', {
                                    onmouseover: function(){hits.activate(hit)},
                                    onclick: function(){ctrl.vm.fetch(hit)},
                                    className: hit.active ? 'active' : ''
                                }, hit.label);
                            })
                        ])
                    )
                );
            }
            return m("div", els);
        }
    };

    /*
     * View Model.
     *
     * Manages state of the app. Is a bit of a dumping ground for everything.
     */
    Autocomplete.ViewModel.prototype = {
        initialize: function(config) {
            this.fields = config.fields;
            this.client = new Client({jwt: config.jwt, baseUrl: config.baseUrl});
            this.results = new ResultSet();
            this.searchString = m.prop('');
            this.line = m.prop(1);
            this.open = m.prop(false);
        },
        search: function(searchString, line) {
            this.searchString(searchString);
            this.line(line);
            this.results.hits.activate(); //Clear active hit immediately, so quickly hitting enter goes to what we're typing, not previously active hit.
            this.open(true);
            if (
                this.searchString()
                && this.searchString().length >= 3
                && /(\d+)(.*)([a-zA-Z]+)/.test(this.searchString())
            ) {
                this.client.search(this.searchString(), line, this.results.load.bind(this.results));
            } else {
                this.results.load();
            }
        },

        setAddress: function(value) {
            for(var index in this.fields) {
                // Prevent fields being filled with 'null'
                var fieldValue = value[index] ? value[index] : '';
                this.fields[index].value = fieldValue;
            }
            this.results.load();
        },
        fetch: function(hit) {
            this.client.fetch(
                hit.address_id, hit.filtered_customer_input, hit.supplementary_data, this.setAddress.bind(this)
            );
        },
        //Close the dropdown on leaving input
        blur: function(e) {
            this.open(false);
            this.results.load();
        },
        //Handle up/down/enter
        checkKey: function(e) {
            if (!this.searchString()) {
                // quick fix for browsers not supporting oninput., don't clear/redraw
                return m.redraw.strategy('none');
            } else if (e.keyCode == '38') { //up
                this.results.hits.prev();
                m.redraw();
                e.preventDefault();
            } else if (e.keyCode == '40') { //down
                this.results.hits.next();
                m.redraw();
                e.preventDefault();
            } else if (e.which == 13 || e.keyCode == 13) { //Enter
                if (this.results.hits.activeHit()) {
                    this.fetch(this.results.hits.activeHit());
                    e.preventDefault();
                }
            } else {
                m.redraw.strategy('none');
            }
        },
        //Button and input CSS class, if empty, will have icon on left.
        elClass: function(orig) {
            return this.searchString() ? orig : orig+' empty'
        }
    };

    window.TradeTested.AddressLookup = AddressLookup;

})(window);

