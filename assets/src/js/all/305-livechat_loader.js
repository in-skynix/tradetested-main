(function(window) {
    window.TradeTested.LiveChatLoader = function (config) {
        if (!config) {
            return;
        }
        window.__lc = config;
        var lc = document.createElement('script');
        lc.type = 'text/javascript';
        lc.async = true;
        lc.src = 'https://cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(lc, s);
    };
})(window);
