(function($, window) {
    window.TradeTested.CheckoutForm = function(){
        window.checkout_spinner = new Spinner({lines: 15,length: 20,width: 20,radius: 60, color: '#ccc'});
        $('.checkout_form').validate({
            onfocusout: false,
            onkeyup: false,
            submitHandler: function(form){
                form.submit();
                $(form).find(':input').prop('readonly', true);
                $(form).find(':radio:not(:checked), button').attr('disabled', true);
                window.checkout_spinner.spin(form)
            }
        });
    };
})(window.jQuery || window.ender || window.Zepto, window);