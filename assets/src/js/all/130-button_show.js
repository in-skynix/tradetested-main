$(function() {
  $('[data-show]').each(function() {
    return $($(this).data('show')).hide();
  }).click(function() {
    $($(this).data('show')).toggle();
    if ($(this).attr('data-show-hide') === 'true') {
      return $(this).hide();
    }
  });
  return $('[data-open]').click(function() {
    return $($(this).data('open')).toggleClass('open');
  });
});