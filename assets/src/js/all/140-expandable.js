// Elements become activatable, by changing the class to 'active'
// :single - only one in the set (containing element) can be activated at a time
$(function() {
  return $('[data-expandable-type]').each(function() {
    var expander, hit;
    expander = $(this);
    hit = $(this).data('expandable-hit') ? $(this).find($(this).data('expandable-hit')) : $(this);
    return hit.click(function() {
      if (expander.data('expandable-type') === 'single') {
        expander.siblings().removeClass('active');
      }
      return expander.toggleClass('active');
    });
  });
});