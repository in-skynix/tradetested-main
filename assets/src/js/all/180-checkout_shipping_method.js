if (typeof window.TradeTested === 'undefined') {
  window.TradeTested = {};
}

(function($, window) {
  var CheckoutShippingEstimator;
  CheckoutShippingEstimator = (function() {
    function CheckoutShippingEstimator() {
      var me;
      me = this;
      this.data = {};
      this.summary_block = $('#order_summary');
      this.block_loader = new AjaxBLockLoader({
        queued: false,
        updateIfQueued: false
      });
      this.refreshDepotPrices();
      this.block_loader.addBlock(this.summary_block, 'order_summary');
      $(document).on('change', 'input[name="shipping_method"],#depot_select', function() {
          var method = $('input[name="shipping_method"]:checked').val();
          method = (method == 'depot') ? $('#depot_select').val() : method;
          me.refreshDepotPrices();
          return me.setMethod(method);
      });
    }

    CheckoutShippingEstimator.prototype.setMethod = function(method_code) {
        if (this.data.shipping_method != method_code) {
            this.data.shipping_method = method_code;
            return this.update();
        }
    };

    CheckoutShippingEstimator.prototype.update = function() {
      this.summary_block.find('tfoot').find('td').addClass('blur');
      this.summary_block.data('ajax_block_data', {
        shipping_method: this.data.shipping_method
      });
      return this.block_loader.refresh();
    };
      
    CheckoutShippingEstimator.prototype.refreshDepotPrices = function() {
        var currentPrice = parseFloat($('#depot_select option:selected').data('price'));
        $('#depot_price').html('$'+currentPrice.toFixed(2));
        $('#depot_select option').each(function(){
            var title = $(this).data('title')
            var priceDifference = parseFloat($(this).data('price')) - currentPrice;
            if (priceDifference > 0) {
                title += " (+$"+priceDifference+")"
            } else if (priceDifference < 0) {
                title += " (-$"+Math.abs(priceDifference)+")"
            }
            $(this).html(title);
        });
    };

    return CheckoutShippingEstimator;

  })();
  return window.TradeTested.CheckoutShippingEstimator = CheckoutShippingEstimator;
})(window.jQuery || window.ender || window.Zepto, window);