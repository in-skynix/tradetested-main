window.TradeTested.CheckoutSteps = {};
(function($, window) {
    window.TradeTested.CheckoutSteps.Addresses = function(config){
        $('#shipping_region_id').val(config.shipping_region);
        $('#billing_region_id').val(config.billing_region);
        $('#checkout_billing_form').toggle(Boolean($('#different_billing').is(':checked')));
        $('#billing_postcode,#shipping_postcode').on('input', function(){
            $(this).val($(this).val().replace(/[^0-9]/gi, ''));
        });
        $('#checkout_address_form').change(function(){
            $('#prefill_callout').hide();
        });
        $('#register').change(function(){
            $('#register_password_box').toggle(!!($(this).is(':checked')));
        }).change();
        $('#login_form').validate({
            submitHandler: function(form){
                $('#login_email').val($('#billing_email').val());
                $('#login_address_data').val($('.checkout_form').serialize());
                form.submit();
                $(form).find(':input').prop('readonly', true);
                $(form).find(':radio:not(:checked), button').attr('disabled', true);
                window.checkout_spinner.spin(form)
            }
        });
        new TradeTested.CheckoutLogin({input: document.getElementById('billing_email'), jwt: config.login_jwt});
        window.addressManager = new TradeTested.CheckoutAddressManager(config.address_manager);
        if (config.store_code == 'default') {
            new TradeTested.AddressLookup({
                jwt: config.address_lookup.jwt,
                baseUrl: config.address_lookup.baseUrl,
                container: document.getElementById('shipping_autocomplete'),
                fields: {
                    "street_1": document.getElementById('shipping_street1'),
                    "street_2": document.getElementById('shipping_street2'),
                    "suburb":   document.getElementById('shipping_street3'),
                    "city":     document.getElementById('shipping_city'),
                    "postcode": document.getElementById('shipping_postcode'),
                    "signature":document.getElementById('shipping_signature')
                }
            });
            new TradeTested.AddressLookup({
                jwt: config.address_lookup.jwt,
                baseUrl: config.address_lookup.baseUrl,
                container: document.getElementById('billing_autocomplete'),
                fields: {
                    "company": document.getElementById('billing_company'),
                    "street_1": document.getElementById('billing_street1'),
                    "street_2": document.getElementById('billing_street2'),
                    "suburb":   document.getElementById('billing_street3'),
                    "city":     document.getElementById('billing_city'),
                    "postcode": document.getElementById('billing_postcode'),
                    "signature":document.getElementById('billing_signature')
                }
            });
        }
    };
    
    window.TradeTested.CheckoutSteps.ShippingMethod = function(config) {
        window.checkout_shipping_estimator = new TradeTested.CheckoutShippingEstimator();
        $('#shipping_method_form input').change(function(){
            $('#shipping_method_form input').each(function(){
                if (this.checked) {
                    $('#container_shipping_method_'+$(this).val()).show();
                } else {
                    $('#container_shipping_method_'+$(this).val()).hide();
                }
            })
        }).change();
        $('#shipping_methods').find('input[type="radio"]').change(function(){
            $('.shipping_details').hide();
            $('#sd_'+$(this).val()).toggle($(this).is(':checked'));
            $('#sd_header').toggle(!!$('#sd_'+$(this).val()).length)
        }).filter(':checked').change();
        
        if (config.depot_info) {
            var depotData = config.depot_info;
            $('#depot_select').change(function(){
                var method = $(this).val().replace('tradetested_depot_', '');
                var data = depotData[method];
                $('#container_shipping_method_depot').html("<strong>Address:</strong><br/><address>"+data.address+"</address>");
                $('.sd_depot_estimate').html(data.shipping_time_estimate);
            }).change();
        }
    };
    
    window.TradeTested.CheckoutSteps.Payment = function() {
        $('#payment_method_form .radio').change(function(){
            $('#payment_method_form .radio').each(function(){
                if (this.checked) {
                    $('#container_payment_method_'+$(this).val()).show();
                    $('#payment_form_'+$(this).val()).show();
                } else {
                    $('#container_payment_method_'+$(this).val()).hide();
                    $('#payment_form_'+$(this).val()).hide();
                }
            })
        }).change();

        var rebillForm = $('#payment_form_tradetested_px_token')
        var rebillCvvFields = rebillForm.find('.cvc_field');
        rebillForm.find('input[type="radio"]').change(function(){
            rebillCvvFields.hide();
            $(this).parent('.radio-label').next('.cvc_field').toggle($(this).is(':checked'));
        }).filter(':checked').change();
    }
})(window.jQuery || window.ender || window.Zepto, window);