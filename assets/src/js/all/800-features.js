/*
# Feature Applier - Applies a bunch of jQuery plugins to HTML elements.
# This should be run once the page loads, or after a section of HTML is loaded.
# A quick fix to use old jQ plugins.
*/

if (typeof window.TradeTested === 'undefined') {
  window.TradeTested = {};
}

(function($, window) {
  var FeatureApplier;
  FeatureApplier = (function() {
    function FeatureApplier(container) {
      container.find('[data-validate]').validate();
      container.find('[data-retina]').mageRetina();
      container.find('.m-scooch').scooch();
      container.find('a.btn-cart').each(function(){
        $(this).attr('href', $(this).attr('href')+'form_key/'+FORM_KEY+'/');
      });
    }
    return FeatureApplier;
  })();
  window.TradeTested.FeatureApplier = FeatureApplier;
  $(function() {
    return new FeatureApplier($(document));
  });
  return $(document).on('ajax_block_replaced', function(event, html) {
    return new FeatureApplier($(event.target));
  });
})(window.jQuery || window.ender || window.Zepto, window);