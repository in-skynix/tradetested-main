if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}
(function(window) {
    var MithrilAjaxCart = {
        ViewModel: function () {
            this.initialize.apply(this, arguments);
        },
        Controller: function (config) {
            this.vm = new MithrilAjaxCart.ViewModel(config);
        },
        View: function (ctrl, config) {
            var vm = ctrl.vm;
            if (vm.showCart()) {
                var recommendations = vm.recommendations();
                var products = vm.products();
                var src = (window.devicePixelRatio >= 2) ? 'cross_sell_ajax_retina' : 'cross_sell_ajax';
                var header = (products.length > 1) 
                    ? 'These items have been added to your cart' : 'This item has been added to your cart';
                header = vm.isLoading() ? (vm.loadingHeader||'Adding to cart ...') : (vm.header||header);
                var itemNote = (products.length > 1) ? 'Items' : 'Item';
                itemNote = vm.isLoading() ? itemNote+':' : itemNote+' added:';
                return m('div.ajax_popup', m('div.overlay', {onclick: function(e){
                        if (['overlay', 'container'].includes(e.target.className)) {vm.hide()}
                    }},
                    m('div.container', m('div.content.addtocart_notification', {config: vm.setContentHeight.bind(vm)}, [
                        m('div', [
                            m('h2', header),
                            m('span.close.icon', {onclick: vm.hide.bind(ctrl.vm)}),
                            (vm.isLoading())
                                ? ''
                                : m('div.actions', [
                                m('a.continue_shopping', {href: "#", onclick: vm.hide.bind(vm)}, 'Continue shopping'),
                                m('a.button.btn-primary.btn-cart', {href: "/checkout/cart"}, 'Go To Cart')
                            ])
                            ,
                            m('p.items_added', itemNote),
                            m('ul.items', products.map(function(product){
                                return m('li', m('a', {href: product.product_url}, product.name));
                            })),
                            m('div.clear')
                        ]),
                        (recommendations.length > 0) 
                            ? m('div.cross_sell.product_recommendations', [
                                m('h3', 'You might also need...'),
                                m('div', m('div', recommendations.map(function(product){
                                    return m('div.item', {'data-href': product.url}, [
                                        m('a.product-image', {href: product.url},
                                            m('img', {src: product.images[src]})
                                        ),
                                        m('div.product_details', [
                                            m('h4.product-name', m('a', {href: product.url}, product.name)),
                                            m.component(
                                                TradeTested.ProductRecommendations.PriceBox, 
                                                {product: product}
                                            ),
                                            m('a.addtocart', {
                                                href: '/checkout/cart/add/product/'+ product.sku
                                                    + '/form_key/'+FORM_KEY+'/',
                                                onclick: function(e){ 
                                                    vm.addToCart([{name: product.name, id: product.sku, product_url: product.url}]); e.preventDefault(); e.stopPropagation()
                                                }
                                            }, 'Add to cart')
                                        ])
                                    ]);
                                })))
                            ])
                            : ''
                        ])
                    )));
            }
            return m("div");
        }
    };
    MithrilAjaxCart.ViewModel.prototype = {
        initialize: function (config) {
            this.form = config.form;
            this.crossSellUrl = config.crossSellUrl;
            this.lastAddedProductId = m.prop();
            this.products = m.prop({});
            this.recommendations = m.prop([]);
            this.showCart = m.prop(false);
            this.isLoading = m.prop(false);
            this.mainProduct = config.mainProduct;
            this.header = config.header;
            this.loadingHeader = config.loadingHeader;
            Observable.on(['addToCart'], this.addToCart.bind(this));
            Observable.on(['addMainProductToCart'], this.addMainProductToCart.bind(this));
        },
        hide: function() {
            this.showCart(false);
        },
        addMainProductToCart: function(e) {
            this.addToCart([this.mainProduct]);
            e.preventDefault();
        },
        addToCart: function(productsData) {
            this.isLoading(true);
            this.showCart(true);
            this.products(productsData.slice(0)); //clones the array;
            var mainProduct = productsData.shift();
            var url = '/checkout/cart/add/product/'+mainProduct.id+'/form_key/'+FORM_KEY+'/'+'ajax/true/';
            var relatedUrl = this.crossSellUrl+'?page=1&perPage=5&include=images.recolist'
                +'&include=images.recolist_retina&skus%5B%5D='+mainProduct.id;
            if (productsData.length > 0) {
                url += 'related_product/'+productsData.map(function(p){return p.id}).join(',')+'/';
            }
            if (this.mainProduct && (mainProduct.id == this.mainProduct.id)) {
                var data = window.jQuery(this.form).find(':input').serialize();
            }
            
            m.redraw();
            m.request({
                method: "POST", 
                url: url, 
                data: data, 
                serialize: function(d){return d},
                config: function(xhr){xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded")}
            }).then(this.loadResult.bind(this));
            m.request({method: "GET", url: relatedUrl}).then(this.recommendations);
        },
        loadResult: function (data) {
            Observable.trigger('messagesUpdated', data.messages||{});
            this.lastAddedProductId(data.lastAddedProductId||null);
            if (!data.success) {
                this.showCart(false);
            }
            if (data.items) {
                this.isLoading(false);
                Observable.trigger('cartUpdated', data.items);
            }
        },
        setContentHeight: function(el, isInit, context) {
            var windowHeight = window.innerHeight;
            var boxHeight = el.offsetHeight;
            var margin = ((windowHeight - boxHeight)/2);
            margin = (margin < 200) ? margin : 200;
            margin = (margin > 20) ? margin : 20;
            el.style.marginTop = margin+'px';
            if (margin == 20) {
                el.className = el.className + ' over_height';
            }
        }
    };
    
    TradeTested.AjaxCart = function (config) {
        var container = document.createElement('div');
        document.body.appendChild(container);
        config.form = document.getElementById('product_addtocart_form');
        this.component = m.component(
            {controller: MithrilAjaxCart.Controller, view: MithrilAjaxCart.View},
            config
        );
        this.mithril = m.mount(
            container,
            this.component
        );
        var vm = this.mithril.vm;
        if (config.form) {
            config.form.addEventListener('submit', vm.addMainProductToCart.bind(vm));
        }
    };

})(window);
