/*
Links can have a PJAX url, which loads a block into the parent 'anchor'
 */
$(function(){
    $(document).on('click', '[data-pjax-url]', function(e){
        var anchor = $(this).closest('[data-pjax-anchor]');
        var spinner;
        if (!(spinner = anchor.data('spinner'))) {
            spinner = new Spinner({
                lines: 7,
                length: 5,
                width: 5,
                radius: 10,
                color: '#ccc'
            });
            anchor.data('spinner', spinner);
        }
        spinner.spin(anchor.get(0));
        $.get($(this).data('pjax-url'), function(r){
            spinner.stop();
            anchor.replaceWith($(r));
            anchor.trigger('ajax_block_replaced', r)
        });
        e.preventDefault();
    });
});