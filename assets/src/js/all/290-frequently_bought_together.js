if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}

(function(window) {
    var MithrilFBT = {
        ViewModel: function () {
            this.initialize.apply(this, arguments);
        },
        Controller: function (config) {
            this.vm = new MithrilFBT.ViewModel(config);
        },
        View: function(ctrl) {
            var vm = ctrl.vm;
            var products = vm.products();
            var selectedProducts = products.filter(function(p){return p.is_selected});
            var src = (window.devicePixelRatio >= 2) ? '240px' : '120px';
            var count = selectedProducts.length;
            var total = selectedProducts.reduce(function(p,c){ return p+c.price }, 0);
            var plural = (count > 1) ? 's' : '';
            return [
                m('div.fbt_addto', [
                    m('ol.fbt_elements', products.map(function(product){
                        var id = 'fbt_checkbox_'+product.id;
                        var opts = {type: 'checkbox', id: id, onchange: function(){vm.toggleProduct(product)}};
                        if (product.is_selected) {
                            opts.checked = 'checked'
                        }
                        var className = product.is_selected ? 'selected-radio-label' : null;
                        return m('li', m('label.radio-label.checkbox', {className: className, for: id}, [
                            m('input.checkbox', opts),
                            m('span.icon'),
                            (product.is_main ? [m('strong', 'This item:'), ' '] : ''),
                            (product.is_main ? product.name : m('a', {href: product.product_url}, product.name)),
                            m('span.price', '$'+product.price.toFixed(2))
                        ]));
                    })),
                    m('span.fbt_price', [
                        'Price for ',
                        m('span.fbt_count', count),
                        ' item'+plural+':',
                        m('span.price', '$'+total.toFixed(2))
                    ]),
                    m(
                        'button.button.btn-primary.btn-cart', 
                        {title: 'Buy Selected Products', onclick: vm.buySelected.bind(vm)}, 'Add All To Cart'
                    )
                ]),
                m('h2', 'Frequently bought together'),
                m('ol.fbt_thumbs', selectedProducts.map(function(product){
                    return m('li.item', m('a.product-image', {href: product.product_url, title: product.name},
                        m('img', {height: 90, width: 120, src: product.images[src], alt: product.name})
                    ))
                }))
            ];
        }
    };
    MithrilFBT.ViewModel.prototype = {
        initialize: function (config) {
            this.products = m.prop(config.products||{});
        },
        toggleProduct: function(product) {
            product.is_selected = !product.is_selected;
            this.products(this.products());
        },
        buySelected: function() {
            var data = this.products()
                .filter(function(product){return product.is_selected})
                .map(function(product){return {name: product.name, id: product.id, product_url: product.product_url}});
            Observable.trigger('addToCart', data);
        }
    };

    window.TradeTested.FrequentlyBoughtTogether = function(config) {
        if (config.container) {
            config.container = document.getElementById(config.container);
        } else {
            return;
        }
        this.component = m.component({controller: MithrilFBT.Controller, view: MithrilFBT.View}, config);
        this.mithril = m.mount(config.container, this.component);
    };
})(window);
