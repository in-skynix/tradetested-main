(function(window) {
    window.TradeTested.Digicert = function() {
        window.__dcid = window.__dcid || [];
        window.__dcid.push(["DigiCertClickID_m1IU3aey", "11", "m", "black", "m1IU3aey"]);
        (function () {
            var cid = document.createElement("script");
            cid.async = true;
            cid.src = "//seal.digicert.com/seals/cascade/seal.min.js";
            var s = document.getElementsByTagName("script");
            var ls = s[(s.length - 1)];
            ls.parentNode.insertBefore(cid, ls.nextSibling);
        }());
    };
})(window);
