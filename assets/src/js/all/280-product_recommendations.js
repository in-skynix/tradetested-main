if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}

(function(window) {
    var ProductRecommendations = function(config) {
        if(!window.requestAnimationFrame){
            return; // Don't bother trying X-Domain requests on IE9 or less.
        }
        var el = document.getElementById(config.containerId);
        var defaultConfig = {
            component: 'scooch',
            perPage: 4,
            numberOfPages: 2
        };
        config = $.extend(defaultConfig, config);
        var componentsMap = {
            scooch:     ScoochComponent,
            grid:       GridComponent,
            simple:     SimpleComponent,
            cross_sell: CrossSellComponent
        };
        this.vm = new ViewModel(config);
        m.mount(el, new componentsMap[config.component](this.vm));
    };

    var ViewModel = function(config) {
        this.config = config;
        this.products = m.request({
            method: "GET", url: this.config.url, data: this.requestParams(), config: function(xhr){
                xhr.withCredentials = true;
            }
        })
    };
    ViewModel.prototype = {
        requestParams: function() {
            var params = {
                page: 1,
                perPage: this.config.perPage*this.config.numberOfPages,
                include: ['images.recolist', 'images.recolist_retina']
            };
            if (this.config.additionalRequestParams) {
                for (var i in this.config.additionalRequestParams) {
                    if (this.config.additionalRequestParams.hasOwnProperty(i)) {
                        params[i] = this.config.additionalRequestParams[i];
                    }
                }
            }
            return params;
        },
        pages: function() {
            var pages = [];
            try {
                if (this.products()) {
                    var products = this.products().slice();
                    while (products.length > 0) {
                        pages.push(products.splice(0, this.config.perPage));
                    }
                }
            } catch (err) {}
            return pages;
        }
    };

    var PriceBox = {
        view: function (ctrl, args) {
            if (args.product.special_price && args.product.price) {
                return m('div.price-box', [
                    m('p.old-price', m('span.price', this.formatPrice(args.product.price))),
                    m('p.special-price', m('span.price', this.formatPrice(args.product.special_price)))
                ]);
            }
            if (args.product.price || (args.product.price === 0)) {
                return m('div.price-box', [
                    m('span.regular-price', m('span.price', this.formatPrice(args.product.price)))
                ]);
            } else {
                return m('div.price-box');
            }
        },
        formatPrice: function(price) {
            return '$'+price.toFixed(2);
        }
    };

    var SimpleComponent = function(vm) {
        this.vm = vm;
        this.view = function(ctrl, args) {
            var src = (window.devicePixelRatio >= 2) ? 'thumb_retina' : 'thumb';
            if (!vm.pages().length) {
                return m('div');
            }
            return m(
                '.block.block-recent', [
                    m('.block-title', m('h3', vm.config.title)),
                    vm.pages().map(function(page){
                        return m('.block-content',
                            m('ol.recently_viewed_items',
                                page.slice(0,3).map(function(product){
                                    return m('li', {'data-href': product.url}, [
                                        m('a.product_image', {href: product.url},
                                            m('img', {src: product.images[src]})
                                        ),
                                        m('a.product_name', {href: product.url}, product.name)
                                    ]);
                                })
                            ),
                            m(
                                'a.action',
                                {href: 'javascript:void(0)', onClick: "$('#extra_recently_viewed').show();$(this).hide()"},
                                'See more'
                            ),
                            m('ol.recently_viewed_items#extra_recently_viewed', {style: "display:none;"},
                                page.slice(3).map(function(product){
                                    return m('li', {'data-href': product.url}, [
                                        m('a.product_image', {href: product.url},
                                            m('img', {src: product.images[src]})
                                        ),
                                        m('a.product_name', {href: product.url}, product.name)
                                    ]);
                                })
                            )
                        )
                    })
                ]
            );
        };
    };

    var CrossSellComponent = function(vm) {
        this.vm = vm;
        this.view = function(ctrl, args) {
            var src = (window.devicePixelRatio >= 2) ? 'cross_sell_retina' : 'cross_sell';
            if (!vm.pages().length) {
                return m('div');
            }
            return m(
                '.crosssell.hide-phone', [
                    m('.block-title', m('h3', vm.config.title)),
                    vm.pages().map(function(page){
                        return m('ul',
                            page.map(function(product){
                                return m('li', {'data-href': product.url}, [
                                    m('a.product-image', {href: product.url},
                                        m('img', {src: product.images[src]})
                                    ),
                                    m('.product-shop', [
                                        m('h4.product-name',
                                            m('a', {href: product.url}, product.name)
                                        ),
                                        m.component(PriceBox, {product: product}),
                                        m('p',
                                            m(
                                                'a.button.btn-primary',
                                                {href: product.add_to_cart_url+'form_key/'+FORM_KEY},
                                                'Add To Cart'
                                            )
                                        )
                                    ])
                                ]);
                            })
                        );
                    })
                ]
            );
        };
    };

    var GridComponent = function(vm) {
        this.vm = vm;
        this.view = function(ctrl, args) {
            var src = (window.devicePixelRatio >= 2) ? 'recolist_retina' : 'recolist';
            if (!vm.pages().length) {
                return m('div');
            }
            return m(
                'div.product_recommendations.up-sell',
                m('h3', vm.config.title),
                vm.pages().map(function(page){
                    return m('div.reco_row',
                        page.map(function(product){
                            return m('div.item', {'data-href': product.url}, [
                                m('a.product-image', {href: product.url},
                                    m('img', {src: product.images[src]})
                                ),
                                m('h4.product-name', m('a', {href: product.url}, product.name)),
                                m.component(PriceBox, {product: product})
                            ]);
                        })
                    );
                })
            );
        };
    };

    var ScoochComponent = function(vm) {
        this.vm = vm;
        this.view = function(ctrl, args) {
            var src = (window.devicePixelRatio >= 2) ? 'recolist_retina' : 'recolist';
            if (!vm.pages().length) {
                return m('div');
            }
            return m(
                'div.product_recommendations.up-sell.m-scooch.m-fluid',
                {config: function(el, isInit){
                    if (!isInit) {
                        $(el).scooch();
                    }
                }},
                m('h3', vm.config.title),
                m('div.m-scooch-inner',
                    vm.pages().map(function(page){
                        return m('div.m-item',
                            page.map(function(product){
                                return m('div.item', {'data-href': product.url}, [
                                    m('a.product-image', {href: product.url},
                                        m('img', {src: product.images[src]})
                                    ),
                                    m('h4.product-name', m('a', {href: product.url}, product.name)),
                                    m.component(PriceBox, {product: product})
                                ]);
                            })
                        );
                    })
                ),
                m(
                    '.m-scooch-controls.m-scooch-bulleted',
                    vm.pages().map(function(page, i){
                        return m('a', {href: '#', 'data-m-slide': i+1})
                    })
                ),
                m('a.icon.m-prev', {href: '#', 'data-m-slide': 'prev'}),
                m('a.icon.m-next', {href: '#', 'data-m-slide': 'next'})
            );
        };
    };

    window.TradeTested.ProductRecommendations = ProductRecommendations;
    window.TradeTested.ProductRecommendations.PriceBox = PriceBox;
})(window);
