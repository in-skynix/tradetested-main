if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}

(function($, window) {

    function HeaderMenu(params) {
        if ((params.container == '#catalog_navigation_top') && $('#left_navigation_surrogate').length) {
            return $('#left_navigation_surrogate').css('height', $('#top-nav').innerHeight()+'px');
        }
        this.containerElement = $(params.container);
        this.openMenuTimer = null;
        this.ignoreSubmenus = params.ignoreSubmenus;
        this.initialize();
    }

    HeaderMenu.prototype = {

        initialize: function(){
            var me = this;
            if (!this.ignoreSubmenus) {
                this.containerElement.find('>ul').menuAim({
                    tolerance: 150,
                    activate: function(el) {
                        $(el).addClass('is-active');
                        me.matchVisibleHeights();
                    },
                    deactivate: function(el) {  $(el).siblings().removeClass('is-active') },
                    exitMenu: this.closeMenu.bind(this)
                });
            }

            this.containerElement.click(this.toggleMenu.bind(this)).hover(
                function(){
                    if(me.openMenuTimer) {
                        window.clearTimeout(me.openMenuTimer);
                        me.openMenuTimer = null;
                    }
                    me.openMenuTimer = window.setTimeout(me.openMenu.bind(me), 300);
                },
                function() {window.clearTimeout(me.openMenuTimer); me.closeMenu();}
            );

            // Prevent menu closing when clicking a link.
            this.containerElement.find('a').click(function(e){e.stopPropagation()});

        },

        openMenu: function() {
            var me = this;
            this.containerElement.find('[data-default]').add(this.containerElement).addClass('is-active');
            this.justOpened = true;
            window.setTimeout(function(){me.justOpened = null;}, 350);
            this.matchVisibleHeights();
        },

        closeMenu: function() {
            this.containerElement.find('li').add(this.containerElement).removeClass('is-active');
        },

        toggleMenu: function() {
            if (this.justOpened) {
                return;
            } else if (this.containerElement.hasClass('is-active')) {
                this.closeMenu();
            } else {
                this.openMenu();
            }
        },

        matchVisibleHeights: function() {
            if (this.ignoreSubmenus) { return; }
            var lists = this.containerElement.find('ul:visible');
            var height = 0;
            lists.css('height', '');
            $.each(lists, function(i, list){
                listHeight = $(list).innerHeight();
                if (listHeight > height) {
                    height = listHeight;
                }
            });
            lists.css('height', height+'px');
        }

    };
    TradeTested.HeaderMenu = HeaderMenu;

})(window.jQuery, window);
