(function(window) {
    $('[data-anchor-scroll]').click(function(e){
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top - 150
        }, 500);
        e.preventDefault();
    });
})(window);
