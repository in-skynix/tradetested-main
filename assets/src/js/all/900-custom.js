$(function() {
    if (!Modernizr.input.placeholder) {
        $('[placeholder]').each(function() {
            if ($(this).val() == '') {
                $(this).val($(this).attr('placeholder'));
                $(this).addClass('placeholder');
            }
            $(this).focus(function() {
                if ($(this).val() == $(this).attr('placeholder') && $(this).hasClass('placeholder')) {
                    $(this).val('');
                    $(this).removeClass('placeholder');
                }
            }).blur(function() {
                    if($(this).val() == '') {
                        $(this).val($(this).attr('placeholder'));
                        $(this).addClass('placeholder');
                    }
                });
        });
        $('[placeholder]').parents('form').submit(function() {
            $(this).find('[placeholder]').each(function() {
                if ($(this).val() == $(this).attr('placeholder') && $(this).hasClass('placeholder')) {
                    $(this).val('');
                }
            });
        });
        window.onbeforeunload = function() {
            $('[placeholder]').each(function() {
                if ($(this).val() == $(this).attr('placeholder') && $(this).hasClass('placeholder')) {
                    $(this).val('');
                }
            });
        };
    }

    $("input[type='radio'], input[type='checkbox']").change(function(){
        $("input[type='radio'], input[type='checkbox']").each(function(){
            $(this).parent('label').toggleClass('selected-radio-label', Boolean(this.checked))
        });
    });
    $("input[type='radio'], input[type='checkbox']").each(function(){
        $(this).parent('label').toggleClass('selected-radio-label', Boolean(this.checked))
    });

    $('#btn-navbar').click(function(){
        $('body').toggleClass('drawer_open');
        if ($('body').hasClass('drawer_open')) {
            window.setTimeout(function(){$('body').addClass('drawer_opened')}, 500);
        } else {
            $('body').removeClass('drawer_opened')
        }
    });
    $('#drawer').click(function(){
        $('body').removeClass('drawer_open');
        if ($('body').hasClass('drawer_open')) {
            window.setTimeout(function(){$('body').addClass('drawer_opened')}, 500);
        } else {
            $('body').removeClass('drawer_opened')
        }
    });

    if (navigator.appVersion.indexOf("Mac")!=-1) {
        $(document.body).addClass('mac');
    }


    $('.footer dl').click(function(){ $(this).toggleClass('open') });
    // viewport stuff
    var targetWidth = 980;
    var deviceWidth = 'device-width';
    var viewport = $('meta[name="viewport"]');
    var showFullSite = function(){
        viewport.attr('content', 'width=' + targetWidth);
        if(!$('.rwd-display-options #view-responsive').length){
            $('.rwd-display-options').append('<span id="view-responsive">View Mobile Optimized</span>');
        }
        $.cookie('full-site', 'active', {expires: 365, path: '/'});
    };

    var showMobileOptimized = function(){
        $.cookie('full-site', 'inactive', {expires: 365, path: '/'});
        viewport.attr('content', 'width=' + deviceWidth);
    };

// if the user previously chose to view full site, change the viewport
    if($.cookie('full-site') == 'active') {
        showFullSite();
    }

    $("#view-full").on("click", function(){
        showFullSite();
    });

    $('.rwd-display-options').on("click", "#view-responsive", function(){
        showMobileOptimized();
    });

    $(document).on('page:fetch', function(){
        $('html').addClass('wait');
    });
    $(document).on('page:change', function(){
        $('html').removeClass('wait');
    });

    $('.m-scooch').scooch();
});
if ((typeof cxApi !== 'undefined') && ($('#is-desktop').is(':visible'))) {
    var cxIds = ['CFxB0QRpQqKD89sJeVn_zA', 'oa2Y3GTeQYOgRzBz0mb6rA'];
    for (var i = 0, len = cxIds.length; i < len; i++) {
        $('body').addClass('variation-'+cxIds[i]+''+cxApi.getChosenVariation(cxIds[i]));
    }
}

