(function(window) {
    var old = window._components || {q: []};
    old = old.q || [];
    var oldCalls = window._call || {q: []};
    oldCalls = oldCalls.q || []
    window._components = function(components) {
        for (var i=0; i < components.length; i++) {
            var func = _getFunction(components[i]['name'], window.TradeTested, false);
            if (func) {
                //http://www.2ality.com/2011/08/spreading.html
                var result = new (Function.prototype.bind.apply(func, [null].concat(components[i]['args'])));
                if (components[i]['ref']) {
                    window[components[i]['ref']] = result;
                }
            } else {
                throw new Error(components[i]['name']+' could not be loaded');
            }
        }
    };
    window._call = function() {
        var args = Array.prototype.slice.call(arguments);
        var func = _getFunction(args.shift(), window, true);
        if (func) {
            func.apply(args);
            m.redraw();
        }
    };
    for (var i=0; i < old.length; i++) {
        var args = Array.prototype.slice.call(old[i]);
        _components.apply(window, args);
    }
    for (var x=0; x < oldCalls.length; x++) {
        var callArgs = Array.prototype.slice.call(oldCalls[x]);
        _call.apply(window, callArgs);
    }

    /**
     * 
     * @param objectString - function name with full path from currentObject
     * @param currentObject - object to start from 
     * @param bind boolean - whether to bind function to parent
     * @returns {*}
     * @private
     */
    function _getFunction(objectString, currentObject, bind) {
        var props = objectString.split(".");
        var bindObj = window;
        for (var i = 0; i < props.length; ++i) {
            bindObj = currentObject;
            currentObject = currentObject[props[i]];
            if (!currentObject) {
                return false;
            }
        }
        return bind ? currentObject.bind(bindObj) : currentObject;
    }
})(window);
