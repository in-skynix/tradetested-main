$(function() {
  $(document).on('click', '[data-href]', function(e) {
    if (e.ctrlKey || e.metaKey) {
      window.open($(this).attr('data-href'));
    } else if (typeof Turbolinks === 'undefined') {
      window.location = $(this).attr('data-href');
    } else {
      Turbolinks.visit($(this).attr('data-href'));
    }
    return e.stopPropagation();
  });
  return $('[data-href]').find('a').click(function(e) {
    return e.stopPropagation();
  });
});