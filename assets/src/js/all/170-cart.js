if (typeof window.TradeTested === 'undefined') {
  window.TradeTested = {};
}

(function($, window) {
  var Cart = (function() {
    Cart.prototype.defaults = {
      paramB: 'bar'
    };

    function Cart(options) {
      $('.header-container a:not(#logo), .footer-container a').attr('target','_blank');
      var element = $('#cart');
        
      var me;
      this.options = $.extend({}, this.defaults, options);
      this.data = options.data;
      this.element = element;
        
      Observable.on(['shippingEstimator.selectedMethod'], this.setShippingMethod.bind(this));
      Observable.on(['shippingEstimator.setPostcode'], this.setPostcode.bind(this));
        
      me = this;
      $(this.element).on('click', 'button.qty_update', function() {
        var input, qty;
        input = $(this).parent('div').find('input.qty');
        qty = parseInt(input.val()) + parseInt($(this).attr('data-value'));
        if (qty >= 0) {
          input.val(qty);
          return input.change();
        }
      });
      $(this.element).on('change', '.qty', function() {
        var item_id, qty;
        item_id = $(this).attr('data-item');
        qty = parseInt($(this).val());
        me.data.items[item_id].quantity = qty;
        me.recalculate();
        return me.render();
      });
      $(this.element).on('click', 'a.cart_remove', function(e) {
        var input;
        input = $(this).closest('.cart_item').find('input.qty');
        input.val(0);
        input.change();
        return e.preventDefault();
      });
      this.totals_block = $(this.element).find('#shopping-cart-totals-table');
      this.items_block = $(this.element).find('#cart_items');
      this.block_loader = new AjaxBLockLoader({
        queued: false,
        updateIfQueued: false
      });
      this.block_loader.addBlock(this.totals_block, 'cart_totals');
      this.block_loader.addBlock(this.items_block, 'cart_items');
    }
      
      Cart.prototype.setShippingMethod = function(method_code) {
          dataLayer.push({
              event: 'cart_estimator_set_method',
              estimatorMethod: method_code
          });
          this.data.shipping_method = method_code;
          return this.update();
      };
      Cart.prototype.setPostcode = function(postcode) {
          dataLayer.push({
              event: 'cart_estimator_set_postcode',
              estimatorPostcode: postcode
          });
          this.data.postcode = postcode;
          return this.update();
      };

    Cart.prototype.recalculate = function() {
      var difference, me, subtotal;
      me = this;
      difference = this.data.totals.grand_total.value - this.data.totals.subtotal.value;
      subtotal = 0;
      $.each(this.data.items, function(id, item) {
        item.subtotal = item.price * item.quantity;
        item.subtotal_formatted = me.formatCurrency(item.subtotal);
        return subtotal += item.subtotal;
      });
      this.data.totals.subtotal.value = subtotal;
      this.data.totals.grand_total.value = subtotal + difference;
      return $.each(this.data.totals, function(id, total) {
        return total.value_formatted = me.formatCurrency(total.value);
      });
    };

    Cart.prototype.render = function() {
      var me;
      me = this;
      $(this.element).find('[data-cartref]').each(function() {
        var val;
        val = me.recurseNodes(me.data, $(this).data('cartref').split(':'));
        if (val !== false) {
          return $(this).html(val);
        }
      });
      return this.update();
    };

    Cart.prototype.update = function() {
      this.totals_block.find('td').addClass('blur');
      this.totals_block.data('ajax_block_data', {
        form_data: this.getUpdateData()
      });
      return this.block_loader.refresh();
    };

    Cart.prototype.getUpdateData = function() {
      var updateData;
      updateData = {
        items: {}
      };
      $.each(this.data.items, function(k, row) {
        return updateData.items[k] = {
          quantity: row.quantity
        };
      });
      updateData.shipping_method = this.data.shipping_method;
      updateData.postcode = this.data.postcode;
      return updateData;
    };

    Cart.prototype.recurseNodes = function(data, nodes) {
      var node;
      if (nodes.length <= 0) {
        return data;
      }
      node = nodes.shift();
      if (data[node] != null) {
        return this.recurseNodes(data[node], nodes);
      }
      return false;
    };

    Cart.prototype.formatCurrency = function(num) {
      var cents, ref, sign;
      num = num.toString().replace(/\$|\,/g, '');
      if (isNaN(num)) {
        num = "0";
      }
      sign = num >= 0 ? '' : '-';
      num = Math.floor(num * 100 + 0.50000000001);
      cents = num % 100;
      num = Math.floor(num / 100).toString();
      if (cents < 10) {
        cents = "0" + cents;
      }
      for (i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));;
      return ((ref = sign) != null ? ref : {
          '': '-'
        }) + '$' + num + '.' + cents;
    };

    return Cart;

  })();
  return window.TradeTested.Cart = Cart;
})(window.jQuery || window.ender || window.Zepto, window);