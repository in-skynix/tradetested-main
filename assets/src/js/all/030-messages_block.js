if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}
(function(window) {
    var MithrilMessagesBlock = {
        Controller: function(config) {
            this.vm = new MithrilMessagesBlock.ViewModel(config);
        },
        ViewModel: function(config) {
            this.messages = m.prop(config.messages||{});
            Observable.on(['messagesUpdated'], this.messages.bind(this));
        },
        View: function(ctrl) {
            var messages = ctrl.vm.messages();
            if (Object.keys(messages).length === 0) {
                return m('span');
            } else {
                return m('ul.messages', Object.keys(messages).map(function(type){
                    return m('li', {className: type+"-msg"}, m('ul', messages[type].map(function(message){
                        return m('li', m('span', m.trust(message)));
                    })));
                }));
            }
        }
    };

    window.TradeTested.MessagesBlock = function(config) {
        var component = m.component(
            {controller: MithrilMessagesBlock.Controller, view: MithrilMessagesBlock.View}, 
            config
        );
        this.mithril = m.mount(config.container, component);
    };
})(window);
