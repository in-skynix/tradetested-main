if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}
(function(window) {
    /*
     * HitList
     *
     * An array-like contstruct that holds all of the hits from all lists combined.
     *
     * Has methods to control which on is active (highlighted)
     *
     * @constructor
     */
    var HitList = function(){};
    HitList.prototype = new Array;

    /*
     * Activate the hit after the one that is currently active
     */
    HitList.prototype.next = function(){
        var i = this.activeIndex() + 1;
        if (!((i) in this)) { i = 0 }
        this.activate(this[i]);
    };

    /*
     * Activate the hit prior to the one that is currently active
     */
    HitList.prototype.prev = function(){
        var i = this.activeIndex() - 1;
        if (!((i) in this)) { i = this.length-1 }
        this.activate(this[i]);
    };

    /*
     * Return the index within the array of the active hit.
     *
     * @returns Integer
     */
    HitList.prototype.activeIndex = function() {
        var activeHit;
        this.forEach(function(h, i) {
            if (h.active) {
                activeHit = i;
            }
        }, this);
        if (typeof activeHit == 'undefined') {
            activeHit = -1;
        }
        return activeHit;
    };

    /*
     *  Return the currently active hit.
     * @returns {*}
     */
    HitList.prototype.activeHit = function() {
        var activeHit;
        this.forEach(function(h, i) {
            if (h.active) {
                activeHit = h;
            }
        }, this);
        return activeHit;
    };

    /*
     * Activate the hit passed in.
     *
     * @param hit
     */
    HitList.prototype.activate = function(hit){
        this.forEach(function(h, i) {
            if (h == hit) {
                hit.active = true;
            } else {
                h.active = false;
            }
        }, this);
    };
    if (!HitList.prototype.forEach)
    {
        HitList.prototype.forEach = function(fun /*, thisp*/)
        {
            var len = this.length >>> 0;
            if (typeof fun != "function")
                throw new TypeError();
            var thisp = arguments[1];
            for (var i = 0; i < len; i++)
            {
                if (i in this)
                    fun.call(thisp, this[i], i, this);
            }
        };
    }

    window.TradeTested.HitList = HitList;
})(window);
