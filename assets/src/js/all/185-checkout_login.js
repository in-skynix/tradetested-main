if (typeof window.TradeTested === 'undefined') {
  window.TradeTested = {};
}

(function(window) {
  CheckoutLogin = function(){ this.initialize.apply(this, arguments); };
  CheckoutLogin.prototype = {
    initialize: function(config) {
      this.input = config.input;
      this.jwt = config.jwt;
      $(this.input).on('input blur', this.update.bind(this));
    },
    update: function() {
      var input = $(this.input);
      var jwt = this.jwt;
      var validator = $('#checkout_address_form').validate();
      if (this.request) {
        input.removeClass('loading');
        this.request.abort();
      }

      if (validator.check(this.input)) {
        input.addClass('loading');
        this.request = $.ajax({
          url: '/ajax',
          type: 'GET',
          data: {store_code: STORE_CODE, skip_mage: true, checkout_login: {email: input.val()}},
          beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', 'JWT '+jwt);
          },
          success: function (r) {
            input.removeClass('loading');
            $('#address_form_container').toggleClass('show_login', r.checkout_login.data.hasAccount);
          },
          error: function() {
            input.removeClass('loading');
            $('#address_form_container').removeClass('show_login');
          }
      })
      } else {
        $('#address_form_container').removeClass('show_login');
      }
    }
  };
  window.TradeTested.CheckoutLogin = CheckoutLogin;

})(window);
