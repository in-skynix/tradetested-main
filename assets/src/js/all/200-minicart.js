if (typeof window.TradeTested === 'undefined') {
    window.TradeTested = {};
}
(function($, window) {
    
    var MithrilMiniCart = {
        Controller: function(config) {
            this.vm = new MithrilMiniCart.ViewModel(config);
        },
        ViewModel: function () {
            this.initialize.apply(this, arguments);
        },
        View: function(ctrl) {
            var vm = ctrl.vm;
            var src = (window.devicePixelRatio >= 2) ? '100px' : '50px';
            var count = vm.items().map(function(p){return p.quantity}).reduce(function(p,c) {return p+c;}, 0);
            var els = [
                m('a.dropdown_title', {href: vm.cartUrl, onclick: vm.show.bind(vm)}, [
                    m('span.count', {className: (count >= 10) ? 'shrink' : ''}, (count>0) ? count : m.trust('&nbsp;')),
                    m('span', 'Cart')
                ])
            ];
            if (vm.isOpen() && (count > 0)) {
                els.push(
                    m('div#minicart_dropdown', m('div.minicart-wrapper', [
                        m('ul#cart-sidebar.mini-products-list', vm.items().map(function(item){
                            return m('li.item', {className: (item.className || '')}, [
                                m('a.product-image', {title: item.product_name, href: item.product_url}, 
                                    m('img', {src: item.images[src], height: 50, width: 50, alt: item.product_name})
                                ),
                                m('div.product-details', [
                                    m('p.product-name', m('a', {href: item.product_url}, item.product_name)),
                                    m('a.remove', {
                                        href: '/checkout/cart/ajaxDelete/id/'+item.id+'/form_key/'+FORM_KEY+'/', 
                                        title: 'Remove this item',
                                        onclick: function(e){ vm.remove(item.id); e.preventDefault(); e.stopPropagation()}
                                    }, 'Remove')
                                ])
                            ])
                        })),
                        m('a.button.btn-primary', {href: vm.cartUrl}, 'View Cart')
                    ]))
                )
            } else if (vm.isOpen()) {
                els.push(
                    m('div#minicart_dropdown', 
                        m('div.minicart-wrapper', m('p.empty', 'You have no items in your shopping cart.'))
                    )
                )
            }
            return m('div', {onmouseenter: vm.mouseover.bind(vm), onmouseleave: vm.mouseout.bind(vm)}, els);
        }
    };
    
    MithrilMiniCart.ViewModel.prototype = {
        initialize: function(config) {
            config = config || {};
            this.cartUrl = config.cartUrl;
            this.items = m.prop(config.items||[]);
            Observable.on(['cartUpdated'], this.items);
            this.isOpen = m.prop(false);
        },
        show: function() {
            this.isOpen(true);
            m.redraw();
        },
        remove: function(id) {
            var items = this.items();
            for(var i = items.length - 1; i >= 0; i--) {
                if(items[i].id == id) {
                    items[i].className = 'removing';
                }
                this.items(items);
                m.redraw();
            }
            m.request({
                method: "POST",
                url: '/checkout/cart/ajaxDelete/id/'+id+'/form_key/'+FORM_KEY+'/ajax/true/'
            }).then(this.loadResult.bind(this));
        },
        loadResult: function(data) {
            if (data.success) {
                var items = this.items();
                for(var i = items.length - 1; i >= 0; i--) {
                    if(items[i].id == data.id) {
                        items.splice(i, 1);
                    }
                }
                this.items(items);
            } else {
                Observable.trigger('messagesUpdated', data.messages||{});
            }
        },
        mouseover: function() {
            if(!this.openMenuTimer) {
                this.openMenuTimer = window.setTimeout(this.show.bind(this), 300);
            }
        }, 
        mouseout: function() {
            window.clearTimeout(this.openMenuTimer); 
            this.openMenuTimer = null;
            this.isOpen(false);
        }
    };
    
    var MithrilMicroCart = {
        Controller: function(config) {
            this.items = m.prop(config.items||[]);
            Observable.on(['cartUpdated'], this.items);
        },
        View: function(ctrl) {
            var count = ctrl.items().map(function (p) {return p.quantity}).reduce(function (p, c) {return p + c;}, 0);
            return (count > 0) ? m('span.header-cart-count', count) : m('span');
        }
    };

    TradeTested.MiniCart = function(options) {
        m.mount(
            document.getElementById('minicart'),
            m.component({controller: MithrilMiniCart.Controller, view: MithrilMiniCart.View}, options)
        )
    };

    TradeTested.MicroCart = function(options) {
        m.mount(
            document.getElementById('microcart'),
            m.component({controller: MithrilMicroCart.Controller, view: MithrilMicroCart.View}, options)
        )
    };

})(window.jQuery, window);
