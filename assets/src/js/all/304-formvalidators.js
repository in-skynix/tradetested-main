(function(window) {
    window.TradeTested.FormValidators = {};
    window.TradeTested.FormValidators.ChangePassword = function() {
        $('#personal_details_form').validate({
            rules: {
                password: {required: true, minlength: 6},
                confirmation: {equalTo: "#password"}
            }
        });
    };
    window.TradeTested.FormValidators.ReviewForm = function() {
        $('#review-form').validate({
            ignore: '',
            errorPlacement: function(error, element) {
                switch (error[0].htmlFor) {
                    case 'recommend':
                        error.insertAfter($('.review-recommend #recommend_no'));
                        break;
                    default:
                        error.insertAfter(element);
                }
            }
        });
    }
})(window);
