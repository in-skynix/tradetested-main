$(function(){
    $('.password_reveal').change(function(){
        var type = $(this).is(':checked') ? 'text' : 'password';
        $(this).siblings('input').attr('type', type);
    }).change();
});