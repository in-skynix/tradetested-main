(function(window) {
    window.TradeTested.AccountComponents = {};
    window.TradeTested.AccountComponents.ChangeEmail = function() {
        var origEmail = $('#email').val();
        $('#personal_details_form').validate();
        $('#email').on('input', function(){
            $('#require_password').toggle($(this).val() != origEmail);
        }).change(function(){
            $('#require_password').toggle($(this).val() != origEmail);
        }).change();
    };
})(window);
