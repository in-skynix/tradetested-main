var gulp = require('gulp');
var babel = require('gulp-babel');
var sass = require("gulp-sass");
var nodeSass = require('node-sass');
var autoPrefixer = require("gulp-autoprefixer");
var sourceMaps = require("gulp-sourcemaps");
var liveReload = require("gulp-livereload");
var rename = require("gulp-rename");
var watch = require('gulp-watch');
var plumber = require('gulp-plumber');
var RevAll = require('gulp-rev-all');
var del = require('del');
var sizeOf = require('image-size');
var favicons = require('gulp-favicons');
var glyphs = {};
var revAll = new RevAll({
    fileNameManifest: 'manifest.json'
});
var uglify             = require('gulp-uglify');
var cssmin             = require('gulp-minify-css');

var _ = require('lodash');

var settings = {
    source_dir: '/gulp/SkynixSrc',
    build_dir: '/assets',
    public_dir: '/assets',
    production: false
};


gulp.task('watch', function() {
    liveReload.listen();
    gulp.watch(settings.source_dir+'/sass/**/*.sass', gulp.series('css'));
    gulp.watch(settings.source_dir+'/js/**/*', gulp.series('js'));
    //gulp.watch(settings.source_dir+'/icons.sketch', gulp.series('icons'));
    gulp.watch(settings.source_dir+'/icons/**/*', gulp.series('icons'));
    gulp.watch(settings.source_dir+'/images/**/*', gulp.series('static'));
    gulp.watch(settings.source_dir+'/fonts/**/*', gulp.series('static'));
});

gulp.task('static', function(){
    return gulp.src([settings.source_dir+"/images/**/*", settings.source_dir+"/fonts/**/*"], {base: settings.source_dir})
        .pipe(gulp.dest(settings.public_dir));
});

gulp.task('css', function(){
    var fs = require('fs');
    var obj = {};
    if (!_.isEmpty(glyphs)) {
        _.each(glyphs, function(g){
            obj[g.name] = g.codepoint.toString(16).toUpperCase();
        })
    } else {
        try {
            obj = JSON.parse(fs.readFileSync(settings.build_dir+'/icons.json', 'utf8'));
        } catch (e) {
            obj = {}
        }
    }
    stream = gulp.src(settings.source_dir+"/sass/**/[^_]*.sass")
        //.pipe(sourceMaps.init())
        .pipe(plumber())
        .pipe(sass({
            indentedSyntax: true,
            includePaths: "path/to/code",
            sourceComments: true,
            errLogToConsole: true,
            functions: {
                'glyph($code)': function(code) {
                    return new nodeSass.types.String("'\\"+obj[code.getValue()]+"'");
                },
                'image_url($uri)': function(uri) {
                    return new nodeSass.types.String("url('/images/"+uri.getValue()+"')");
                },
                'image_height($path)': function(path) {
                    var dimensions = sizeOf(path.getValue());
                    return new nodeSass.types.String(dimensions.height+'px');
                },
                'image_width($path)': function(path) {
                    var dimensions = sizeOf(path.getValue());
                    return new nodeSass.types.String(dimensions.width+'px');
                }
            }
        }));
    if (settings.production) {
        stream.pipe(autoPrefixer())
            .pipe(cssmin({ keepBreaks: false }))
            //.pipe(sourceMaps.write('.'))
        ;
    }
    return stream.pipe(gulp.dest(settings.public_dir+'/css')).pipe(liveReload());
});

var include = require('gulp-include');
var closureCompiler = require('gulp-closure-compiler');

gulp.task('js', function() {
    stream = gulp.src(settings.source_dir+"/js/*.js")
        .pipe(babel())
        .pipe(plumber())
        .pipe(include());

    if (settings.production) {
        stream
            //.pipe(sourceMaps.init())
            .pipe(uglify())
            //.pipe(sourceMaps.write('.'))
        ;
        //.pipe(closureCompiler({compilerPath: 'bower_components/closure-compiler/compiler.jar', fileName: 'build.js'}))
    }

    return stream.pipe(gulp.dest(settings.public_dir+'/js')).pipe(liveReload());
});

var iconfont    = require('gulp-iconfont');
var consolidate = require('gulp-consolidate');
var sketch = require("gulp-sketch");
var gulpImagemin = require('gulp-imagemin');

gulp.task('icons', function(){
    var fontName = 'icons';
    var stream = gulp.src(settings.source_dir+'/icons/*.svg')
    //var stream = gulp.src(settings.source_dir+"/icons.sketch")
    //    .pipe(sketch({
    //        export: 'artboards',
    //        autohint: true,
    //        formats: 'svg'
    //    }))
        .pipe(gulpImagemin()) // pipe image-min before you pass it to the iconfont task
        .pipe(iconfont({
            fontName: fontName,
            normalize: true,
            appendCodepoints: false,
            fontHeight: 560
        }))
        .on('codepoints', function(codepoints, options) {
            glyphs = codepoints;
        })
        .pipe(gulp.dest(settings.public_dir+'/fonts'));
    stream.on('end', function() {
        var options = {glyphs: glyphs};
        gulp.src(settings.source_dir+'/icons/templates/**/*')
            .pipe(consolidate('lodash', options))
            .pipe(gulp.dest(settings.build_dir));
    });
    return stream;
});

gulp.task('favicons', function(){
    return gulp.src(settings.source_dir+"/favicon.png").pipe(favicons({
        appName: "Trade Tested",
        appDescription: "Trade Tested",
        background: "#ffffff",
        path: "favicons/",
        url: "https://www.tradetested.co.nz/",
        display: "standalone",
        orientation: "portrait",
        version: 1.0,
        logging: false,
        online: false,
        html: "index.html",
        pipeHTML: true,
replace: true
}))
.pipe(gulp.dest(settings.build_dir+"/images/favicons"));
});

gulp.task('svg', function () {
    var svgstore = require('gulp-svgstore');
    var svgmin = require('gulp-svgmin');
    var path = require('path');
    return gulp
        .src(settings.source_dir+'/icons/*.svg')
        .pipe(svgmin(function (file) {
            var prefix = path.basename(file.relative, path.extname(file.relative));
            return {
                plugins: [{
                    cleanupIDs: {
                        prefix: prefix + '-',
                        minify: true
                    }
                }]
            }
        }))
        .pipe(svgstore())
        .pipe(gulp.dest(settings.build_dir+"/images"));
});

function clean() {
    settings.production = true;
    return del([settings.build_dir+"/*", settings.public_dir+"/*"], {force: true});
}
gulp.task(clean);

function revision() {
    return gulp.src([settings.public_dir+"/**/*"])
        .pipe(revAll.revision())
        .pipe(gulp.dest(settings.public_dir))
        .pipe(revAll.manifestFile())
        .pipe(gulp.dest(settings.public_dir));
}

gulp.task("default", gulp.series(
    // gulp.parallel('icons', 'static', 'svg')
    gulp.parallel('icons', 'static', 'svg'),
    gulp.parallel('css', 'js')
));
gulp.task('production', gulp.series(clean, 'default', 'favicons', revision));
