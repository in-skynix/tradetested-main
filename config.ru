require 'bundler'
require 'rack/livereload'
require 'rack/rewrite'

Bundler.setup :default, (ENV['RACK_ENV'] || 'development')
#use Rack::ShowExceptions
ENV['MAGE_IS_DEVELOPER_MODE'] = 'true'

Bundler.require(:dev_server)

# use Rack::LiveReload
use Rack::Magento::SetOrigUri

use Rack::Rewrite do
  rewrite   %r{^/api/([a-z][0-9a-z_]+)/([^\?]*)\??(.*)$},  '/api.php?type=$1&$3'
  rewrite   %r{^/ajax(.*)$},  '/ajax.php$1'
  rewrite   %r{^/address/([^\?]*)\??(.*)$},  '/address/router.php?uri=$1&$2'
end
use Rack::Magento::Cgi, Dir.getwd+'/public'
run Rack::File.new 'public'
