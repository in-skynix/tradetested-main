# Provisioning and Configuration Management
To provision a new server in dev, run ```vagrant up``` from this directory (config/playbook)

This will automatically run the ansible playbook on the new vagrant box. To run the playbook again after the server has 
been provisioned, run ```vagrant provision``` from the directory.

Provisioning will setup the server as required, but will not install the code or database to the machine. 

You will then need to deploy to the vagrant stage, setup the database, and forward to port 80 on the vagrant box in 
order to see the site. For deployment and database setup, see the main README.

## Run playbook on remote server(s)
First, you need to define the server groups in your ansible inventory
```ini
;/etc/ansible/hosts
[tradetested_monolith]
stage.tradetested.co.nz
```

Run the playbook, and specify the user to ssh to in order to run it. 
```ansible-playbook playbook.yml -u root```


TODOs:

* Work out what is causing iptables to block 80
* Rsyslog setup for logstash/papertrailapp
* Set default UFW outgoing policy to deny, after working out why git ls-remote is being blocked. 
* Run ansible as another user after root logon has been disabled
* Change SSH port to 8322
* Setup my.cnf, currently not set up as the config depends on the amount of RAM available
* Only restart services if their config has been changed
* Random passwords for mysql users.
* setup zsh for root and deploy users
* Configuration Management for the production machine. 
* Logstash forwarder repo has moved?
* php.ini, set post_max_size, upload_max_filesize, 
* php opcache settings.
* new cron jobs
* RabbitMQ setup
* Oggetto Messenger upstart script
* Sensu setup.
* Thumbor with OpenCV
