set :deploy_to,   "/home/tradetested/deployed/staging"
set :branch, "staging"
set :ssh_options, {forward_agent: true, port: 8322}

role :app, %w{tradetested@tradetested.co.nz}
role :web, %w{tradetested@tradetested.co.nz}
role :db,  %w{tradetested@tradetested.co.nz}

