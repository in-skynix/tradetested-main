set :deploy_to,   "/var/www/magento"
set :branch, "master"
set :ssh_options, {forward_agent: true, port: 22}

role :app, %w{deploy@stage.tradetested.co.nz}
role :web, %w{deploy@stage.tradetested.co.nz}
role :db,  %w{deploy@stage.tradetested.co.nz}
