set :deploy_to,   "/var/www/magento"
set :branch, "staging"
set :ssh_options, {forward_agent: true, port: 22}

role :app, %w{deploy@192.168.33.10}
role :web, %w{deploy@192.168.33.10}
role :db,  %w{deploy@192.168.33.10}
