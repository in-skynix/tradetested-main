require 'magecap/notifier'
set :deploy_to,   "/home/tradetested/deployed/production"
set :branch, "master"
set :ssh_options, {forward_agent: true, port: 8322}

role :app, %w{tradetested@tradetested.co.nz}
role :web, %w{tradetested@tradetested.co.nz}
role :db,  %w{tradetested@tradetested.co.nz}


after "deploy:restart", "newrelic:notice_deployment"
