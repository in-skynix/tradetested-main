# require 'capistrano/bundler'
require 'capistrano/newrelic'
require 'magecap'
require 'magecap/nginx'
# require 'magecap/sprockets'
set :stages, %w[production staging vagrant stage]
set :compile, false
set :app_webroot, '/public'
set :linked_files, %w[public/app/etc/local.xml]
set :linked_dirs, %w[public/media public/var public/sitemaps public/blog public/blog_au public/feeds assets/node_modules]
# set :bundle_bins, %w{compass gem rake rails}
# set :bundle_roles, :all
# set :bundle_binstubs, -> { shared_path.join('bin') }
# set :bundle_path, -> { shared_path.join('bundle') }
# set :bundle_without, %w{development test}.join(' ')
# set :bundle_flags, '--deployment --quiet'
lock '3.4.0'
set :application, 'tradetested_magento'
set :repo_url,  "git@github.com:messagerichard/tt-magento.git"
set :deploy_via, :remote_cache
set :application, "tradetested_magento"
set :use_sudo, true
set :keep_releases,  3
set :normalize_asset_timestamps, false

set :newrelic_license_key, 'f34f0a1285b1f1d6cb621d062a270fbeb3f15c6b'
set :app_name, 'PHP Application'
set :notifier_mail_options, {
  :method => :smtp,
  :from   => 'deploy@tradetested.co.nz',
  :to     => ['dane@avidonline.co.nz', 'richard@tradetested.co.nz'],
  :github => 'messagerichard/tt-magento',
  :smtp_settings => {
    address: "email-smtp.us-east-1.amazonaws.com",
    port: 587,
    enable_starttls_auto: true,
    user_name: 'AKIAI5CD2WQEYRDYOM7A',
    password: 'Ao0IAvKqLfrjuhNhybsISuX1a4eJwKeMgpIJzc7P5X79'
  }
}
#aftet 'deploy:updated', 'bundler:install'
namespace :npm do
  task :install do
    on roles :app do
      within "#{release_path}/assets" do
        execute :npm, :install
      end
    end
  end
end


namespace :gulp do
  task :build do
    on roles :app do
      within "#{release_path}/assets" do
        with path: "#{release_path}/assets/node_modules/.bin:$PATH" do
          execute :gulp, :production
        end
      end
    end
  end
end

namespace :composer do
  task :fix do
    on roles :app do
      within "#{release_path}" do
				execute :sed, '-i "s|\\"magento-root-dir\\": \\"magento\\",|\\"magento-root-dir\\": \\"public\\",|g" composer.json'
      end
    end
  end
end

# after "deploy:updating", "npm:install"
after "deploy:updating", "gulp:build"
after "deploy:updating", "composer:fix"
after "composer:install", "change_permissions" do
  on roles :app do
    execute :chmod, "-R +x #{release_path}/public/shell/messenger/bin"
  end
end

namespace :deploy do
  task :restart do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      execute :sudo, :service, "tradetested_messaging_receiver restart"
      execute :sudo, :service, "tradetested_messaging_receiver_staging restart"
    end
  end
end

